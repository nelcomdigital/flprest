@extends('frontend.layouts.app')

@section('content')
    @include('frontend.layouts.partials.header')

    <div class="row" style="padding: 50px 20px">

    <div class="p-5 w-100 bg-white" >
     {!! $page->description !!}
        </div>

    </div>
    @include('frontend.layouts.partials.footer')

@endsection

@push('js')

@endpush
