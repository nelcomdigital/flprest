@extends('frontend.layouts.app')

@section('content')
    <div class="cart-container" id="cart-container">
        <div class="cart-text-header"><h1 style="padding: 10px 0;font-size: 2rem">{!! __('front.review_cart') !!}</h1></div>
        <div class="d-none d-sm-block">
            <div class="cart-table">
                <table style="width:100%">
                    <tr class="border-transparent-right">
                        <th class="w-25 text-left border-transparent">{!!__('front.item_label')!!}</th>
                        <th class="w-25 border-transparent">{!!__('front.unit_price')!!}</th>
                        <th class="w-25 border-transparent">{!!__('front.quantity')!!}</th>
                        <th class="w-25 border-transparent" style="display: none">{!!__('front.total')!!}</th>
                    </tr>
                    @isset($products)
                        @foreach ($products as $product)

                            <tr class="border-transparent-right" id="{!!$product['price']->ref!!}">
                                <th class="w-20 border-transparent">
                                    <div class="inline-flex w-100">
                                        <img alt="" class="w-25" src="https://distrib.foreverliving.fr/450-450/media/image/produits/produit/{{$product['price']->ref}}.png">
                                        <div>
                                            <div class="cart-item-name w-100" style="margin: auto 10px!important;">
                                                {!!html_entity_decode($product['detail']->label->{session()->get('current_locale')}) !!}</div>
                                            <div class="item-currency-modal w-100" style="
                                            font-weight: 300!important ;
                                            margin: auto 10px!important;
                                            text-align: left!important;
                                            text-transform: capitalize;">
                                                Ref: {!!$product['price']->ref!!}</div>
                                        </div>
                                    </div>
                                </th>
                                <th class="w-20 border-transparent">
                                    <div class="inline-flex w-100 m-auto">
                                        <div class="m-auto text-center font-16 inline-flex">
                                            <div class="unit-price" id="unit-price-{{$product['price']->ref}}">
                                                {{number_format((float)($product['price']->prixTTC), 2)}}
                                            </div>
                                            <div class="unit-currency" id="unit-currency-{{$product['price']->ref}}">
                                                €
                                            </div>
                                            /
                                            <div class="unit-price-cc" id="unit-price-cc-{{$product['price']->ref}}">
                                                {{number_format((float)($product['price']->CC), 3)}}
                                            </div>
                                            <div class="unit-currency-cc" id="unit-currency-cc-{{$product['price']->ref}}">
                                                CC
                                            </div>
                                        </div>
                                    </div>
                                </th>
                                <th class="w-20 border-transparent">
                                    <div class="inline-flex w-100 m-auto">
                                        <input class="input-border-yellow mt-auto text-center mb-auto w-25 p-0"
                                               type="text"
                                               style="width: 100% !important;
                                                    margin: 0 50px;
                                                    background-color: transparent!important;
                                                    border: 0!important;"
                                               id="quantity-{{$product['price']->ref}}"
                                               name="quantity[]" readonly
                                               value="{{getCurrentQuantity($product['price']->ref)}}">
                                    </div>
                                </th>
                                <th class="w-20 border-transparent" style="display: none;">
                                    <div class="inline-flex w-100 m-auto">
                                        <div class="m-auto text-center font-16 inline-flex">
                                            <div class="unit-price" id="unit-price-total-{{$product['price']->ref}}">
                                                {{number_format((float)(getCurrentQuantity($product['price']->ref)  *
                                    $product['price']->prixTTC), 2)}}

                                            </div>
                                            <div class="unit-currency" id="unit-currency-total-{{$product['price']->ref}}">
                                                €
                                            </div>
                                            /
                                            <div class="unit-price-cc" id="unit-price-cc-total-{{$product['price']->ref}}">
                                                {{number_format((float)(getCurrentQuantity($product['price']->ref)  *
                                       ((float)$product['price']->CC)), 3)}}
                                            </div>
                                            <div class="unit-currency-cc" id="unit-currency-cc-total-{{$product['price']->ref}}">
                                                CC
                                            </div>
                                        </div>
                                    </div>
                                </th>
                            </tr>
                        @endforeach
                    @endisset
                    @if ( session()->get('process.shipping_reference' , '') != '')
                        <tr class="border-transparent-right">
                            <th class="w-20 border-transparent">
                                <div class="inline-flex w-100">
                                    <div>
                                        <div class="cart-item-name w-100" style="margin: auto 10px!important;">
                                            {!!session()->get('process.shipping_name' , '')!!}</div>
                                        <div class="item-currency-modal w-100" style="
                                            font-weight: 300!important ;
                                            margin: auto 10px!important;
                                            text-align: left!important;
                                            text-transform: capitalize;">
                                            Ref: {{session()->get('process.shipping_reference' , '')}}</div>
                                    </div>
                                </div>
                            </th>
                            <th class="w-20 border-transparent">
                                <div class="inline-flex w-100 m-auto">
                                    <div class="m-auto text-center font-16 inline-flex">
                                        <div class="unit-price" >
                                            {{session()->get('process.shipping_amount' , '')}}
                                        </div>
                                        <div class="unit-currency">
                                            €
                                        </div>
                                        /
                                        <div class="unit-price-cc">
                                           0.000
                                        </div>
                                        <div class="unit-currency-cc" >
                                            CC
                                        </div>
                                    </div>
                                </div>
                            </th>
                            <th class="w-20 border-transparent">
                                <div class="inline-flex w-100 m-auto">
                                    <input class="input-border-yellow mt-auto text-center mb-auto w-25 p-0"
                                           type="text"
                                           style="width: 100% !important;
                                                    margin: 0 50px;
                                                    background-color: transparent!important;
                                                    border: 0!important;"
                                           name="quantity[]" readonly
                                           value="1">
                                </div>
                            </th>
                            <th class="w-20 border-transparent" style="display: none;">
                                <div class="inline-flex w-100 m-auto">
                                    <div class="m-auto text-center font-16 inline-flex">
                                        <div class="unit-price" >
                                            {{session()->get('process.shipping_amount' , '')}}

                                        </div>
                                        <div class="unit-currency" >
                                            €
                                        </div>
                                        /
                                        <div class="unit-price-cc">
                                            0.000
                                        </div>
                                        <div class="unit-currency-cc">
                                            CC
                                        </div>
                                    </div>
                                </div>
                            </th>
                        </tr>
                    @endif
                </table>
            </div>

            <div class="border-transparent-right w-100 p-3 text-right">
                <div class="row">
                    <div class="col-md-4" style="text-align: left">
                        <div class="payment-label-head">{{__('front.p_enter_pin')}}*</div>
                        <div class="w-100"><input class="input-round-corners-top-left-right" type="text" id="pin"
                                                  name="pin" placeholder="{{__('front.p_pin')}}*"></div>
                        <div class="payment-label m-auto">
                            <input class="m-auto" type="checkbox" id="policy" name="policy" >
                            <label class="m-auto pl-2" for="policy"
                                   style="font-family: 'Roboto';font-size: 14px;">{{__('front.i_agree_to')}}
                                <a href="{{asset('pdf/cgu.pdf')}}"
                                   target="_blank"
                                   style="color: #0056b3;text-decoration: underline;">{{__('front.privacy_notice')}}</a>
                            </label>
                        </div>
                        @if ((isset($pack) and $pack ) or (isset($totalCC) and $totalCC >= 2))
                            <div class="payment-label m-auto">
                                <input class="m-auto" type="checkbox" id="procedure" name="procedure">
                                <label class="m-auto pl-2" for="procedure" style="font-family: 'Roboto';font-size: 14px;">{{__('front.i_agree_to')}}
                                    <a
                                        href="{{asset('pdf/cgv.pdf')}}"
                                        target="_blank"
                                        style="color: #0056b3;text-decoration: underline;"> {{__('front.prod_policies')}}</a></label>
                            </div>
                        @endif
                        <label class=m-auto">
                            <span class="help-block" id="help-terms-and-policy"></span>
                        </label>

                        <label class=m-auto">
                            <span class="help-block help-pin" style="display: none">{!!__('front.wrong_pin')!!}</span>
                            <span class="help-block help-pin1" style="display: none">{!!__('front.req_pin')!!}</span>
                        </label>
                    </div>
                    <div class="col-md-3"></div>
                    <div class="col-md-5 " style="display: grid">
                        <table style="border: 0">

                            <tr>
                                <td>
                                    <div class="review-total-text">
                                        {{__('front.total')}}
                                    </div>
                                </td>
                                <td>
                                    <div class="inline-flex review-cart-total">
                                        <div class="unit-price" id="unit-price-grand-total">
                                            @isset($total)
                                                {{number_format((float)($total), 2)}}

                                            @else
                                                0
                                            @endisset
                                        </div>
                                        <div class="unit-currency" id="unit-currency-grand-total">
                                            €
                                        </div>
                                        /
                                        <div class="unit-price-cc" id="unit-price-cc-grand-total">
                                            @isset($totalCC)
                                                {{number_format((float)($totalCC), 3)}}
                                            @else
                                                0
                                            @endisset
                                        </div>
                                        <div class="unit-currency-cc" id="unit-currency-cc-grand-total">
                                            CC
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>

                    </div>
                </div>
            </div>
        </div>


        {{--Mobile--}}
        <div class="d-block d-sm-none">
            @isset($products)
                @foreach ($products as $product)
                    <div class="inline-flex w-100" id="m{{$product['price']->ref}}" style="padding: 0 0 20px 0;!important;">
                        <div class="cart-img">
                            <img src="https://distrib.foreverliving.fr/450-450/media/image/produits/produit/{{$product['price']->ref}}.png">
                        </div>
                        <div class="cart-details">
                            <div class="cart-details-info">
                                <div
                                    class="mobile-cart-product-name">{!!html_entity_decode($product['detail']->label->{session()->get('current_locale')}) !!}</div>
                                <div class="mobile-cart-reference">item# {{$product['price']->ref}}</div>

                                <div class="inline-flex" style="visibility: hidden">
                                    <div class="unit-price"
                                         id="mobile-unit-price-{{$product['price']->ref}}">{{$product['price']->prixTTC}}</div>
                                    <div class="unit-currency"
                                         id="mobile-unit-currency-{{$product['price']->ref}}"> € </div>
                                </div>
                            </div>
                            <div class="mobile-cart-price inline-flex" style="display: none;">
                                <div class="unit-price" id="mobile-unit-price-total-{{$product['price']->ref}}">
                                    {{number_format((float)(getCurrentQuantity($product['price']->ref)  *
                                        $product['price']->prixTTC), 2)}}
                                </div>
                                <div class="unit-currency" id="mobile-unit-currency-total-{{$product['price']->ref}}">
                                    €
                                </div>
                                =
                                <div class="unit-price-cc" id="mobile-unit-price-cc-total-{{$product['price']->ref}}">
                                    {{number_format((float)(getCurrentQuantity($product['price']->ref)  *
                                        ((float)$product['price']->CC)), 3)}}
                                </div>
                                <div class="unit-currency-cc" id="mobile-unit-currency-cc-total-{{$product['price']->ref}}">
                                    CC
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endisset

            @if ( session()->get('process.shipping_reference' , '') != '')
                    <div class="inline-flex w-100" style="padding: 0 0 20px 0;!important;">
                        <div class="cart-img">

                        </div>
                        <div class="cart-details">
                            <div class="cart-details-info">
                                <div
                                    class="mobile-cart-product-name">{!!session()->get('process.shipping_name' , '')!!}</div>
                                <div class="mobile-cart-reference">item# {{session()->get('process.shipping_reference')}}</div>

                                <div class="inline-flex" style="visibility: hidden">
                                    <div class="unit-price">{{session()->get('process.amount')}}</div>
                                    <div class="unit-currency"> € </div>
                                </div>
                            </div>
                            <div class="mobile-cart-price inline-flex" style="display: none;">
                                <div class="unit-price" >{{session()->get('process.amount')}}</div>
                                <div class="unit-currency">€</div>
                                =
                                <div class="unit-price-cc"> 0.000</div>
                                <div class="unit-currency-cc"> CC</div>
                            </div>
                        </div>
                    </div>

            @endif

            <div class="border-transparent-right w-100 p-2 text-center" style="background-color: black">
                <div class="row">
                    <div class="col-md-5" style="display: grid">
                        <table style="border: 0">
                            <tr>
                                <td>
                                    <div class="review-total-text">
                                        {{__('front.total')}}
                                    </div>
                                </td>
                                <td class="text-right">
                                    <div class="inline-flex cart-total">
                                        <div class="unit-price" id="unit-price-grand-total">
                                            @isset($total)
                                                {{number_format((float)($total), 2)}}

                                            @else
                                                0
                                            @endisset
                                        </div>
                                        <div class="unit-currency" id="unit-currency-grand-total">
                                            €
                                        </div>
                                        /
                                        <div class="unit-price-cc" id="unit-price-cc-grand-total">
                                            @isset($totalCC)
                                                {{number_format((float)($totalCC), 3)}}
                                            @else
                                                0
                                            @endisset
                                        </div>
                                        <div class="unit-currency-cc" id="unit-currency-cc-grand-total">
                                            CC
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>

                    </div>
                </div>
            </div>
        </div>

        <div class="d-none d-sm-block">
            <div class="btn-group-payment">

                <div class="text-right">
                    <button type="button"  class="btn-submit-yellow review-btn-cart" id="btn-cart">
                        {{__('front.proceed_to_payment')}}
                    </button>
                    <a class="m-3" id="continue_shopping" href="{{route('cart')}}">{{__('front.return_to_cart')}}</a>
                </div>
            </div>
        </div>
        <div class="d-block d-sm-none" style="min-height: inherit">
            <div class="btn-group-payment">
                <div class="text-center">
                    <div class="col-md-12 p-0">
                        <div class="payment-label-head">{{__('front.p_enter_pin')}}*</div>
                        <div class="w-100"><input class="input-round-corners-top-left-right" type="text" id="mpin"
                                                  name="pin" placeholder="{{__('front.p_pin')}}*"></div>

                        <div class="payment-label m-auto">
                            <input class="m-auto" type="checkbox" id="mpolicy" name="mpolicy" >
                            <label class="m-auto pl-2" for="mpolicy"
                                   style="font-family: 'Roboto';font-size: 14px;">{{__('front.i_agree_to')}}
                                <a href="{{asset('pdf/cgu.pdf')}}"
                                   target="_blank"
                                   style="color: #0056b3;text-decoration: underline;">{{__('front.privacy_notice')}}</a>
                            </label>
                        </div>
                        @if ((isset($pack) and $pack ) or (isset($totalCC) and $totalCC >= 2))
                            <div class="payment-label m-auto">
                                <input class="m-auto" type="checkbox" id="mprocedure" name="mprocedure">
                                <label class="m-auto pl-2" for="mprocedure" style="font-family: 'Roboto';font-size: 14px;">{{__('front.i_agree_to')}}
                                    <a
                                        href="{{asset('pdf/cgv.pdf')}}"
                                        target="_blank"
                                        style="color: #0056b3;text-decoration: underline;"> {{__('front.prod_policies')}}</a></label>
                            </div>
                        @endif
                        <label class=m-auto">
                            <span class="help-block" id="help-terms-and-policy"></span>
                            <span class="help-block" id="help-terms-and-policy-m"></span>
                        </label>

                        <label class=m-auto">
                            <span class="help-block help-pin" style="display: none">{!!__('front.wrong_pin')!!}</span>
                            <span class="help-block help-pin1" style="display: none">{!!__('front.req_pin')!!}</span>

                        </label>
                    </div>
                    {{--                    <a class="m-3" id="continue_shopping" href="{{route('products')}}">{{__('front.continue_shopping')}}</a>--}}

                    {{--                    <button class="btn-submit w-100 mt-3 " @isset($order) style="cursor: none; border: 0" @else onclick="laterBuy()" @endisset> @isset($order)--}}
                    {{--                            {{__('front.you_still_have')}} {{\Carbon\Carbon::parse($order->created_at)->addDays(3)->diffInHours(\Carbon\Carbon::now())}}--}}

                    {{--                        @else {{__('front.buy_within')}} 72 @endisset {{__('front.hours')}} @isset($order) {{__('front.to_buy_product')}} @endisset</button>--}}
                    <button type="button" class="btn-submit-yellow w-100 mt-3 review-btn-cart" id="btn-cart">
                        {{__('front.proceed_to_payment')}}
                    </button>
                    <a class="m-3" href="{{route('cart')}}">{{__('front.return_to_cart')}}</a>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        $(function () {

            $('.review-btn-cart').on('click', function () {
                if ( $('#pin').val() ==='' && $('#mpin').val() ===''){
                    $('.help-pin1').show()
                    return;
                }else{
                    $('.help-pin1').hide()
                }
                let policy = false;
                let procedure = false;
                 policy = $("#policy").prop("checked");
                 procedure = true;
                if ( $("#procedure").length != 0)
                    procedure = $("#procedure").prop("checked");

                let mpolicy = false;
                let mprocedure = false;
                mpolicy = $("#mpolicy").prop("checked");
                mprocedure = true;
                let pinn =  $('#pin').val();
                if ( pinn == '')
                    pinn = $('#mpin').val();
                if ( $("#mprocedure").length != 0)
                    mprocedure = $("#mprocedure").prop("checked");

                if ((policy && procedure) ||(mpolicy && mprocedure) ) {
                    $("#help-terms-and-policy").empty();
                    $("#help-terms-and-policy-m").empty();
                    // let url = window.location.protocol + '//' + window.location.hostname + '/thank-you';
                    // window.location.href = url;
                } else {
                    $("#help-terms-and-policy").html("{{html_entity_decode(__('front.terms_and_policy_aggreement'))}}");
                    $("#help-terms-and-policy-m").html("{{html_entity_decode(__('front.terms_and_policy_aggreement'))}}");
                    return;
                }


                    $.ajax({
                        type: 'POST',
                        url: '{{route('check.pin')}}',
                        data: {
                            pin: pinn,
                            _token: "{{ csrf_token() }}",
                        },
                        complete: function () {
                            // var $this = $('.load-btn-login');
                            // var data = $this.attr('data-loading-text');
                            // $this.attr('data-loading-text', $this.html());
                            // $this.html(data);
                        }
                    }).done(function (result) {
                        if (result === "success") {
                            $('.help-pin').hide()
                            let pin = $('#pin').val()
                            if ( pin === '')
                                pin = $('#mpin').val()
                            window.location.href = window.location.protocol + '//' + window.location.hostname + '/payment/'+ pin;
                        }
                        else{
                            $('.help-pin').show()
                            return;
                        }
                    });



            });
        });
    </script>

@endpush
