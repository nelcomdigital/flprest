@extends('frontend.layouts.app')

@section('content')
    <div class="w-100 bg-white" id="shopping-address">
        @if ( is_array(session()->get('process.country_detail.delivery'))  and sizeof(session()->get('process.country_detail.delivery'))==1 )
            @php($fixedDelivery = session()->get('process.country_detail.delivery')['fixed'])
        @endif
        <div class="row">
            <div class="col-md-12 w-100">
                <div class="cart-text-header">
                    @isset($fixedDelivery)
                        <h1 style="padding: 10px 0;font-size: 2rem;text-align: center">Adresse de retrait</h1>
                    @else
                        <h1 style="padding: 10px 0;font-size: 2rem;text-align: center">{{__('front.shipping_address')}}</h1>
                    @endisset
                </div>

            </div>
            <div class="col-md-6">
                    @csrf
                    <div class="address-container inline-flex">
                        <div class="w-100">
                            <div class="address-info">
                                <table class="w-100">


                                    <tr>
                                        <th class="padding-1" style="font-family: 'Roboto'; font-weight: 500">{{__('front.name')}}*</th>
                                        <th class="shipping"><input id="shipping-name"
                                                                    value="{{session()->get('process.first_name')}} {{session()->get('process.last_name')}}"
                                                                    class="shipping-input-text shipping-input"
                                                                    name="shipping_name" type="text" ></th>
                                    </tr>
                                    <tr>
                                        <th class="padding-1" style="font-family: 'Roboto'; font-weight: 500">{{__('front.country')}}*</th>
                                        <th class="shipping"><input id="shipping-country"
                                                                    value="{{session()->get('process.country_name')}}"
                                                                    data-id="{{session()->get('process.country')}}"
                                                                    class="shipping-input-text shipping-input"
                                                                    name="shipping-country" type="text" disabled></th>
                                    </tr>
                                    <tr>
                                        <th class="padding-1" style="font-family: 'Roboto'; font-weight: 500">{{__('front.zip_code')}}*</th>
                                        <th class="shipping"><input id="shipping-zip-code"
                                                                    @isset($fixedDelivery)
                                                                    value="{{$fixedDelivery['zip_code']}}"
                                                                    disabled
                                                                        @else
                                                                    value="{{session()->get('process.zip_code')}}"
                                                                    @endisset

                                                                    class="shipping-input-text shipping-input"
                                                                    name="shipping-zip-code" type="text"></th>
                                    </tr>
                                    <tr>
                                        <th class="padding-1" style="font-family: 'Roboto'; font-weight: 500">{{__('front.city')}}*</th>
                                        <th class="shipping">
                                            @isset($fixedDelivery)
                                                <input id="shipping-city"
                                                       value="{{$fixedDelivery['city']}}"
                                                       class="shipping-input-text shipping-input" type="text">
                                            @else
                                            <select class="select2 city-select w-100" id="shipping-city" placeholder="{{__('front.cities_label')}}">

                                            </select>
                                            @endisset
                                        </th>



                                    </tr>
                                    <tr>
                                        <th class="padding-1" style="font-family: 'Roboto'; font-weight: 500"></th>
                                        <th class="shipping"><span class="help-block" id="false-city"></span></th>
                                    </tr>
                                    <tr>
                                        <th class="padding-1" style="font-family: 'Roboto'; font-weight: 500">{{__('front.street_address')}}*</th>
                                        <th class="shipping"><input id="shipping-street-Address"
                                                                    @isset($fixedDelivery)
                                                                    value="{{$fixedDelivery['street']}}"
                                                                    disabled
                                                                    @else
                                                                    value="{{session()->get('process.street_address')}}"
                                                                    @endisset
                                                                    class="shipping-input-text shipping-input"
                                                                    name="shipping-street-address" type="text">
                                        </th>
                                    </tr>
                                    <tr>
                                        <th class="padding-1" style="font-family: 'Roboto'; font-weight: 500">{{__('front.apartment')}}</th>
                                        <th class="shipping"><input id="shipping-additional-address"
                                                                    @isset($fixedDelivery)
                                                                    value="{{$fixedDelivery['additional_street']}}"
                                                                    disabled
                                                                    @else
                                                                    value="{{session()->get('process.additional_address')}}"
                                                                    @endisset
                                                                    class="shipping-input-text shipping-input"
                                                                    name="shipping-additional-address" type="text">
                                        </th>
                                    </tr>

                                    <tr>
                                        <th class="padding-1" style="font-family: 'Roboto'; font-weight: 500">{{__('front.phone_number')}}*</th>
                                        <th class="shipping"><input id="shipping-phone-number"
                                                                    class="shipping-input-text shipping-input"
                                                                    value="{{session()->get('process.phone_number')}}"
                                                                    name="shipping-phone-number" type="text">
                                            <span class="help-block" id="help-phone"></span>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th class="padding-1" style="font-family: 'Roboto'; font-weight: 500">{{__('front.email')}}*</th>
                                        <th class="shipping"><input id="shipping-email"
                                                                    class="shipping-input-text shipping-input"
                                                                    value="{{session()->get('process.email')}}"
                                                                    name="shipping-email" type="text"></th>
                                    </tr>

                                    @if ( is_array(session()->get('process.country_detail.delivery'))  )
                                        @if ( sizeof(session()->get('process.country_detail.delivery'))>1)
                                            @php($delivery = session()->get('process.country_detail.delivery'))
                                            <tr >
                                                <th class="padding-1" style="font-family: 'Roboto'; font-weight: 500">{{__('front.shipping_type')}}*</th>
                                                <th class="shipping">
                                                    <select class="select2 city-select w-100" id="shipping-type" placeholder="{{__('front.shipping_type')}}" onchange="changeIframe()">
                                                        <option value="">{{__('front.shipping_type')}}</option>
                                                        @foreach($delivery as $k=>$d)
                                                            @if ( $k == 'home')
                                                                <option value="DOM">{{__('front.from_home')}}</option>
                                                            @endif
                                                            @if ( $k =='pick_up' and  session()->get('process.country_detail.delivery_code') == 'FRA')
                                                                    <option value="RELAIS">{{__('front.from_check_point')}}</option>
                                                            @endif
                                                        @endforeach
                                                    </select></th>

                                            </tr>
                                            @elseif( isset($fixedDelivery))
                                            <tr >

                                                <th class="padding-1" style="font-family: 'Roboto'; font-weight: 500">{{__('front.shipping_type')}}*</th>
                                                <th class="shipping" id="shipping_name">{!! $fixedDelivery['mode']  !!}</th>

                                            </tr>
                                            @else
                                            @php($direct = true)
                                        @endif
                                    @endif

                                    <tr >

                                        <th class="padding-1" style="font-family: 'Roboto'; font-weight: 500">{{__('front.shipping_way')}}</th>
                                        <th class="shipping" id="shipping_cost"></th>

                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="payment-labels">
                        <label class="inline-flex m-auto">
                            <span class="help-block" id="help-address"></span>
                        </label>
                    </div>
                    <div class="address-button">
                        {{--                    <button class="btn-submit" id="btn-add-address">+ Add another address</button>--}}
                        <button type="submit" style="margin-bottom: 15px" class="btn-submit-yellow-address" id="btn-proceed-payment">{{__('front.review_order')}}</button>
                        <a class="m-3" id="continue_shopping" href="{{route('cart')}}">{{__('front.return_to_cart')}}</a>
                    </div>
            </div>
            <div class="col-md-6" id="iframe-shipping-box" style="display: none">
                    <div class="row">
                        <iframe  id="iframe-shipping"  width="100%" height="500px">

                        </iframe>
                    </div>
                    <div class="row">
                        <button type="button" class="btn-submit-yellow w-100 mt-3 review-btn-cart" onclick="changeAddress()">
                            {{__('front.change_address')}}
                        </button>
                    </div>


            </div>
        </div>

    </div>

@endsection
@push('js')
    <script type="application/javascript">
        $(function () {
            $('#shipping-zip-code').on('keyup', function () {
                getCitiesByZipCode()
            });
            $('#btn-proceed-payment').on('click', function (e) {
                if ( $('#shipping-type').val() === ''){
                    $('#help-address').text('{{__('front.all_fields_required')}}');
                    return ;
                }
                if ( $('#shipping-type').val()=== 'RELAIS'){
                    $.ajax({
                        type: 'GET',
                        url: '{{route('check.checkpoint')}}',
                    }).done(function (result) {
                        if ( result == '' ){
                            $('#help-address').text('{{__('front.all_fields_required_2')}}');
                        }else{
                            e.preventDefault();
                            goToreview()
                        }
                    });
                }else{
                    e.preventDefault();
                    goToreview()
                }
            });

        });

        function goToreview(){
            let shippingName = $("#shipping-name");
            let shippingCountry = $("#shipping-country");
            let shippingCity = $("#shipping-city");
            let shippingStreetAddress = $("#shipping-street-Address");
            let shippingAdditionalAddress = $("#shipping-additional-address");
            let shippingZipCode = $("#shipping-zip-code");
            let shippingPhoneNumber = $("#shipping-phone-number");
            let first_name, last_name, country, city, street_address, additional_address, zip_code, phone_number;
            if (shippingName.val() && shippingCountry.val() && shippingCity.val() && shippingStreetAddress.val()
                && shippingZipCode.val() && shippingPhoneNumber.val()  ) {
                [first_name, last_name] = shippingName.val().split(" ");
                last_name = shippingName.val().replace( first_name , '');
                country = shippingCountry.val();
                city = shippingCity.val();
                street_address = shippingStreetAddress.val();
                additional_address = shippingAdditionalAddress.val();
                zip_code = shippingZipCode.val();
                phone_number = shippingPhoneNumber.val();

                if ( phone_number.match(/[a-z]/i)) {
                    $('#help-phone').html("{{html_entity_decode(__('front.phone_number_required'))}}" );
                    return;
                }else{
                    $('#help-phone').html("");
                }
                if ( $('#shipping_reference').length > 0){
                    $.ajax({
                        type: 'POST',
                        url: '{{route('session.put')}}',
                        data: {
                            key: ['shipping_first_name', 'shipping_last_name', 'shipping_country',
                                'shipping_city', 'shipping_street_address', 'shipping_additional_address',
                                'shipping_zip_code', 'shipping_phone_number','shipping_email','shipping_reference'
                                ,'shipping_amount','shipping_name','shipping_title'],
                            value: [first_name, last_name, country, city, street_address, additional_address, zip_code, phone_number,
                                $('#shipping-email').val()
                                ,$('#shipping_reference').val(),$('#shipping_amount').val(),$('#shipping_nom').val(),
                                $('#shipping-type').length > 0? $('#shipping-type').find(':selected').val():'DOM'],
                            _token: "{{ csrf_token() }}",
                        },
                    }).done(function (result) {
                        $('#help-address').text('');
                        let url = window.location.protocol + '//' + window.location.hostname + '/review/'+'{{$order_id}}';
                        window.location.href = url;
                    });
                }else{
                    $.ajax({
                        type: 'POST',
                        url: '{{route('session.put')}}',
                        data: {
                            key: ['shipping_first_name', 'shipping_last_name', 'shipping_country',
                                'shipping_city', 'shipping_street_address', 'shipping_additional_address',
                                'shipping_zip_code', 'shipping_phone_number','shipping_email','shipping_title'],
                            value: [first_name, last_name, country, city, street_address, additional_address,
                                zip_code, phone_number,
                                $('#shipping-email').val(),$('#shipping-type').length > 0? $('#shipping-type').find(':selected').val():'DOM'],
                            _token: "{{ csrf_token() }}",
                        },
                    }).done(function (result) {
                        $('#help-address').text('');
                        let url = window.location.protocol + '//' + window.location.hostname + '/review/'+'{{$order_id}}';
                        window.location.href = url;
                    });
                }


            } else {
                $('#help-address').text('{{__('front.all_fields_required')}}');
            }
        }

        function getCitiesByZipCode(){

            let zip = $('#shipping-zip-code').val();
            $.ajax({
                type: 'get',
                dataType: 'json',
                url: '{{route('select-city-review')}}/'+'{{session()->get('process.country','-1')}}/'+ zip,
            }).done(function (result) {
                $("#shipping-city").empty();
                if (result.status){
                    if ( result.response.length > 0 ) {
                        let i = 0;
                        for (i = 0; i < result.response.length; i++) {
                            let ob = result.response[i];
                            let id='';
                            @if( session()->get('process.city', '') != '')
                                id = '{{session()->get('process.city','')}}';
                            @endif
                            if (result.response.length === 1){
                                var option = '<option value="' + ob._id + '" data-zip="' + ob.zip_code + '" selected>' + ob.name + '</option>';
                            } else {
                                if (id === ob._id)
                                    var option = '<option value="' + ob._id + '" data-zip="' + ob.zip_code + '" selected>' + ob.name + '</option>';
                                else
                                    var option = '<option value="' + ob._id + '" data-zip="' + ob.zip_code + '">' + ob.name + '</option>';
                            }

                            $("#shipping-city").append(option);
                        }
                        $("#shipping-city").prop('disabled',false);
                        $('#false-city').text('').hide();
                    } else{
                        $("#shipping-city").prop('disabled',true);
                        $('#false-city').text('{{__('front.no_city_available_in_this_region')}}').show();
                    }
                } else {
                    $("#shipping-city").prop('disabled',true);
                    $('#false-city').text('{{__('front.wrong_pin_code')}}').show();
                }
            }).fail(function (data) {
                console.log(data);
            });
        }

        function getShipmentCost(){
            var obj = new Object();
            obj.action = "getFraisLivraison";
            obj.env = "{{session()->get('process.country_detail.products_code')}}";
            obj.region = "{{session()->get('process.country_detail.region_code')}}";
            obj.typliv = "{{session()->get('process.country_detail.product_type')}}";
            if ( $('#shipping-type').is(':visible')){
                obj.modeLiv= $('#shipping-type').find(':selected').val()
                if ( $('#shipping-type').find(':selected').val() == "DOM"){
                    obj.expediteur='FEDEX';
                }else{
                    obj.expediteur='CHRONOPOST';
                }
            }else{
                @isset($direct)
                    obj.modeLiv='DOM';
                @else
                    obj.modeLiv='';
                @endisset

                obj.expediteur='';
            }
            {{--obj.modeLiv="{{session()->get('process.group_code')}}";--}}
            obj.paysLiv="{{session()->get('process.country_detail.delivery_code','FRA')}}";
            obj.totalTTC="{{$total}}";
            obj.totalCC="{{$totalCC}}";
            obj.option="1";
            obj.cpLiv=$('#shipping-zip-code').val();

            var json = JSON.stringify(obj);

            $.ajax({
                url: 'https://webservice.foreverliving.fr/wsinscription.php' ,
                data: json,
                dataType:'json',
                type: "POST",
                contentType: "application/json",

            }).done(function (result)
            {
                if ( result.success){
                    if ( result.listeDetail.length > 0 ) {
                        let i = 0;
                        {{--if (result.response.length !== 1){--}}
                        {{--    var option = '<option value="" >{{__('front.city_label')}}</option>';--}}
                        {{--}--}}

                        // $("#city-select").append(option);
                        for (i = 0; i < result.listeDetail.length; i++) {
                            let ob = result.listeDetail[i];
                            let id='';
                            @if( session()->get('process.shipping_reference', '') != '')
                                id = '{{session()->get('process.shipping_reference','')}}';
                            @endif
                            if (result.listeDetail.length === 1){
                                var div = '<input type="hidden" id="shipping_reference" value="'+ob.ref+'"/>';
                                 div += '<input type="hidden" id="shipping_amount" value="'+Number(ob.prixTTC).toFixed(2)+'"/>';
                                 div += '<input  type="hidden" id="shipping_nom" value="'+ob.designation+'"/>';
                                 div += '<div>'+Number(ob.prixTTC).toFixed(2)+'€ </div>';
                                $("#shipping_cost").html(div);
                             }


                        }
                    }
                }


            }).fail(function (data) {
            });
        }


        function changeIframe() {
            if ( $('#shipping-type').val()== "RELAIS") {
                let url = 'https://posfra.flpbis.com/chronopost/index.php?bckUrl={{route('select.checkpoint')}}?' +
                    '&adresse='+$('#shipping-street-Address').val()+'&cp='+$('#shipping-zip-code').val()+'&ville='+$("#shipping-city option:selected").text()+'&key=5352876b-9490-42a2-aade-d36c9b6d92ca';
                $('#iframe-shipping-box').show()
                $('#iframe-shipping').attr('src', url);
            }else{
                $('#iframe-shipping-box').hide();
            }
            getShipmentCost()
        }

        function changeAddress() {
            if ( $('#shipping-type').val()== "RELAIS") {
                let url = 'https://posfra.flpbis.com/chronopost/index.php?bckUrl={{route('select.checkpoint')}}?' +
                    '&adresse='+$('#shipping-street-Address').val()+'&cp='+$('#shipping-zip-code').val()+'&ville='+$("#shipping-city option:selected").text()+'&key=5352876b-9490-42a2-aade-d36c9b6d92ca';
                $('#iframe-shipping-box').show()
                $('#iframe-shipping').attr('src', url);
            }else{
                $('#iframe-shipping-box').hide();
            }
        }

        @isset($fixedDelivery)
            getShipmentCost()
            @else
            getCitiesByZipCode()
        @endisset
        @isset($direct)
            getShipmentCost()
        @endisset
    </script>
@endpush
