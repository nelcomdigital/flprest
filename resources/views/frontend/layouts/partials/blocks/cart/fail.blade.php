@extends('frontend.layouts.app')

@section('content')
    <div class="d-none d-sm-block">
        <div class="p-5 w-100 bg-white" id="thank-you">
            <div class="thank-you-title text-lg-left" style="margin-top: 20px;"><h1><b>{{__('front.congrats')}}</b></h1></div>
            <div class="thank-you-label inline-flex w-100">
                <div class="visa-logos mr-auto"></div>
            </div>
            <div class="thank-you-container inline-flex">
                <div class="p-2 w-100" style="margin-top: 30px">
                    <p>{{__('front.we_have_sent_your')}}<b> {{__('front.distributor_number_by_email')}}</b>
                        {{__('front.you_can_also_check_it_here_below')}}</p>
                    <p style="text-transform: uppercase"><b>{{__('front.your_distributor_nmumber')}}*</b></p>
                    <input type="text" class="input-text" name="distributor"
                           value="{{$user->reference}}"
                           id="thank-you-distributor" readonly>
                    <p style="text-transform: uppercase"><b>{{__('front.final_name')}}</b></p>
                    <input type="text" class="input-text" name="name"
                           value="{{$user->name}}"
                           id="thank-you-name" readonly>
                    <br/>
                    <p>{{__('front.join_on_mobile')}}</p>
                    <img alt="" class="w-50" src="{{asset('images/downloads.png')}}">
                </div>
            </div>
        </div>
        <div class="w-100">
            <img class="w-100" src="{{asset('images/forever_proud.jpg')}}" alt="">
        </div>
    </div>
    <div class="d-block d-sm-none">
        <div class="w-100">
            <img class="w-100" src="{{asset('images/forever_proud.jpg')}}" style="margin-top: 10%;" alt="">
        </div>
        <div class="p-3 w-100 bg-white" id="thank-you">
            @isset($page)
                {!! $page->description !!}
            @endisset
            <div class="thank-you-container inline-flex">
                <div class="p-2 w-100" style="margin-top: 30px">
                                        <p>We have sent your distributor number by email, you can also check it here below</p>
                                        <p style="text-transform: uppercase"><b>YOUR DISTRIBUTOR NUMBER*</b></p>
                                        <input type="text" class="input-text" name="distributor"
                                               value="{{session()->get('process.distributor_number','')}}"
                                               id="thank-you-distributor" readonly>
                                        <p style="text-transform: uppercase"><b>Name</b></p>
                                        <input type="text" class="input-text" name="name"
                                               value="{{session()->get('process.first_name','')}} {{session()->get('process.last_name','')}}"
                                               id="thank-you-name" readonly>
                                        <br/>
                    <p>{{__('front.Join_us_on_mobile_application')}}</p>
                    <img alt="" class="w-50" src="{{asset('images/play-stor.png')}}">
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        $(function () {

        });
    </script>
@endpush
