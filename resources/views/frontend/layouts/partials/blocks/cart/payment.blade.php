<html lang="{{session()->get('current_locale')}}">
    <body>
    <form id="formPayment" action="https://paiement.systempay.fr/vads-payment/" name="formPayment"  method="POST">
        <input type="hidden" name="vads_action_mode" value="INTERACTIVE">
        <input type="hidden" name="vads_amount" value="{{$total*100}}">
        <input type="hidden" name="vads_available_languages" value="fr;en;de;es;it;pt;nl;sv">
        <input type="hidden" name="vads_ctx_mode" value="PRODUCTION">
        <input type="hidden" name="vads_currency" value="978">
        <input type="hidden" name="vads_cust_id" value="330000000000">
        <input type="hidden" name="vads_order_id" value="{{$order_id}}">
        <input type="hidden" name="vads_order_info" value="CMD">
        <input type="hidden" name="vads_page_action" value="PAYMENT">
        <input type="hidden" name="vads_payment_config" value="SINGLE">
        <input type="hidden" name="vads_return_mode" value="GET">
        <input type="hidden" name="vads_site_id" value="55754771">
        <input type="hidden" name="vads_trans_date" value="{{$time}}">
        <input type="hidden" name="vads_trans_id" value="{{substr($order_id,-6)}}">
        <input type="hidden" name="vads_url_cancel" value="{{route('payment.cancel')}}">
        <input type="hidden" name="vads_url_error" value="{{route('payment.error')}}">
        <input type="hidden" name="vads_url_referral" value="{{route('payment.process')}}">
        <input type="hidden" name="vads_url_refused" value="{{route('payment.refused')}}">
        <input type="hidden" name="vads_url_success" value="{{route('payment.success')}}">
        <input type="hidden" name="vads_shop_url" value="https://distrib.foreverliving.fr?origin=join">
        <input type="hidden" name="vads_validation_mode" value="0">
        <input type="hidden" name="vads_version" value="V2">
        <input type="hidden" name="signature" value="{{$signature}}">

    </form>
    <script type="application/javascript">
        //Once the page is loaded, submit the form
        setTimeout(function(){ document.getElementById('formPayment').submit(); }, 200);

    </script>
    </body>

</html>


