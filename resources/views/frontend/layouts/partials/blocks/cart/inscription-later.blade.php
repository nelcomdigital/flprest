@extends('frontend.layouts.app')

@section('content')
    <div class="d-none d-sm-block">
        <div class="p-5 w-100 bg-white" id="thank-you2">
            <div class="thank-you-title text-lg-left" style="margin-top: 30px"><h1><b>{{__('front.thank_you')}}</b></h1></div>
            <div class="thank-you-label inline-flex w-100">
                <div class="visa-logos mr-auto"></div>
            </div>
            <div class="thank-you-container inline-flex">
                <div class="p-2 w-100">
                    @isset($page)
                        {!! $page->description !!}
                    @endisset
                    {{--                <img alt="" style="margin: 20px 0" class="w-50" src="{{asset('images/downloads.png')}}">--}}
                </div>
            </div>
        </div>
        <div class="w-100">
            <img class="w-100" src="{{asset('images/72h.jpg')}}" alt="">
        </div>
    </div>
    <div class="d-block d-sm-none">
        <div class="w-100">
            <img class="w-100" src="{{asset('images/72h.jpg')}}" alt="" style="margin-top: 10%">
        </div>
        <div class="p-3 w-100 bg-white" id="thank-you2">
            <div class="thank-you-title text-lg-left" style="margin-top: 30px"><h1><b>{{__('front.thank_you')}}</b></h1></div>
            <div class="thank-you-label inline-flex w-100">
                <div class="visa-logos mr-auto"></div>
            </div>
            <div class="thank-you-container inline-flex">
                <div class="p-2 w-100">
                    @isset($page)
                        {!! $page->description !!}
                    @endisset
                    {{--                <img alt="" style="margin: 20px 0" class="w-50" src="{{asset('images/downloads.png')}}">--}}
                </div>
            </div>
        </div>
    </div>
@endsection
