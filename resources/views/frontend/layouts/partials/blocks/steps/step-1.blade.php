<div class="register-text">
    <p class="pdo-number" style="padding-right: 5px">{{__('front.welcome_to')}}
    <b class="roboto-bold">{{__('front.foreverliving')}}</b></p>
</div>
@isset($welcome)
    <div class="register-text">
        <p class="register-text1">{!! $welcome->description !!}</p>
    </div>
@else
    <div class="register-text">
        <p class="register-text1">{{__('front.welcome_label')}}</p>
    </div>
@endisset

<div class="phone-number">
    <label for="phone_number"></label><input type="text" class="dist-input-text" name="distributor_number"
                                             value="@if( $preId != null ){{$preId}}@else{{session()->get('process.distributor_number','')}}@endif"
                                             id="distributor_number" placeholder="{{__('front.distributor_number')}}">
    <span class="help-block" id="help-register_distributor_number"></span>
</div>
<div class="form-submit">
    <div class="submit-container">

        <span onclick="steps(2, this)"
              class="btn-submit-yellow cursor"> <i class="fa fa-spinner fa-spin" id="check-submit" style="display: none"></i> {{__('front.submit')}}</span>
    </div>
    <div class="remember-container">
{{--                <div class="remember-me-label">--}}
{{--                    <input class="m-auto" type="checkbox" id="remember_me" value="{{session()->get('process.distributor_name','')}}">--}}
{{--                    <label class="m-auto pl-2" for="remember_me">{{__('front.remember_me')}}</label>--}}
{{--                </div>--}}
    </div>
</div>
{{--<div class="contact-support">--}}
{{--    <a href="#"><img class="cursor" src="{{asset('images/Support.svg')}}" alt=""></a>--}}
{{--    <div class="support-text">{{__('front.support')}}</div>--}}
{{--</div>--}}
