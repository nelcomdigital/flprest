@if (session()->get('current_locale','fr'))
    <div class="register-text">
        <p class="pdo-number" style="padding-right: 5px">{{__('front.address')}}
            <b class="roboto-bold">{!!__('front.address-label2')!!}</b></p>
    </div>
@else
    <div class="register-text">
        <p class="pdo-number" style="padding-right: 5px">{!!__('front.address-label2')!!}
            <b class="roboto-bold">{!!__('front.address')!!}</b></p>
    </div>
@endif
<div class="w-100">
    <select class="select2 city-select w-100" id="country-select" placeholder="Country">
                <option value="">{{__('front.country-label')}}*</option>
        @foreach( $groupCountries as $group)
            <optgroup
                    label="@if ( \Illuminate\Support\Arr::has($group->name , session()->get('current_locale')))
                    {{$group->name[session()->get('current_locale')]}}
                    @else {{$group->name['en']}} @endif" data-zip="{{$group->tree}}">
{{--                @foreach( $group->countries as $country)--}}
{{--                @if ($country->name[session()->get('current_locale')] == "Métropole")--}}
{{--                    <option value="{{$country->_id}}"--}}
{{--                            @if( session()->get('process.country','') == $country->_id) selected @endif>--}}
{{--                        @if ( \Illuminate\Support\Arr::has($country->name , session()->get('current_locale')))--}}
{{--                            {{$country->name[session()->get('current_locale')]}}--}}
{{--                        @else {{$country->name['en']}} @endif--}}
{{--                    </option>--}}
{{--                @endif--}}
{{--                @endforeach--}}
                @foreach( $group->countries as $country)
{{--                    @if ($country->name[session()->get('current_locale')] != "Métropole")--}}
                        <option value="{{$country->_id}}"
                                @if( session()->get('process.country','') == $country->_id) selected @endif>
                            @if ( \Illuminate\Support\Arr::has($country->name , session()->get('current_locale')))
                                {{$country->name[session()->get('current_locale')]}}
                            @else {{$country->name['en']}} @endif
                        </option>
{{--                    @endif--}}
                @endforeach
            </optgroup>

        @endforeach
    </select>
    <span class="help-block" id="help-country"></span>

    <div id="region-select-div" style="display: none">
        <select class="select2 city-select w-100" id="region-select" placeholder="Regions">

        </select>
        <span class="help-block" id="help-region"></span>
    </div>
    <div id="department-select-div" style="display: none">
        <select class="select2 city-select w-100" id="department-select" placeholder="Departments">

        </select>
        <span class="help-block" id="help-departments"></span>
    </div>

    <input type="text" class="input-text ml-auto" name="zip_code" id="zip_code"
           value="{{session()->get('process.zip_code','')}}" style="display: none"
           placeholder="{{__('front.zip_code')}}*">
    <span class="help-block" id="help-zip-code"></span>

    <div id="city-select-div" style="display: none">

        <select class="select2 city-select w-100" id="city-select" placeholder="{{__('front.cities_label')}}">

        </select>
        <span class="help-block" id="help-city"></span>
    </div>

    <input type="text" class="input-text ml-auto" name="street_address" id="street_address"
           value="{{session()->get('process.street_address','')}}"
           placeholder="{{__('front.address')}}*">
    <span class="help-block" id="help-street-address"></span>

    <input type="text" class="input-text ml-auto" name="additional_address" id="additional_address"
           value="{{session()->get('process.additional_address','')}}"
           placeholder="{{__('front.additional_address')}}">
    <span class="help-block" id="help-additional-address"></span>
    <div class="payment-label m-auto">
        <input class="m-auto" type="checkbox" id="rgpd" name="rgpd">
        <label class="m-auto pl-2" for="rgpd" style="font-family: 'Roboto';font-size: 14px;">{{__('front.rgpd')}}
            <br>{{__('front.rgpd2')}}
            <br>{{__('front.rgpd3')}}
            <br>{{__('front.rgpd4')}} <a href="mailto:compliance.france@foreverliving.fr">compliance.france@foreverliving.fr</a>
        </label>
    </div>
    <span class="help-block padding-23 " id="help-rgpd"></span>
</div>

<div class="steps inline-flex w-100 mt-5 text-right">
    <div class="circle-arrow">
        <span onclick="steps(2)" class="cursor btn-submit-list">{{__('front.previous_step')}}</span>
        <span onclick="steps(4)" class="cursor btn-submit-green-list">{{__('front.next_step')}}</span>
    </div>
</div>

@push('js')
    <script>
        let id_number_needed = false;
        let postal_code_needed = false;

        function getRegions(id) {
            $('#region-select-div').hide()
            $('#region-select').empty()
            $('#department-select-div').hide()
            $('#department-select').empty()
            $('#city-select-div').hide()
            $('#city-select').empty()
            $.ajax({
                type: 'GET',
                dataType: 'json',
                url: '{{route('select.country')}}/' + id,
            }).done(function (result) {
                console.log(result)
                id_number_needed = result.need_id_number;
                postal_code_needed = result.need_postal_code;
                if (result.regions.length > 1) {
                    var option = new Option("Region", "");

                    $("#region-select").append(option);
                    let i = 0;
                    for (i = 0; i < result.regions.length; i++) {
                        let ob = result.regions[i];
                        var option = new Option(ob.name, ob._id);

                        $("#region-select").append(option);
                    }
                    $('#region-select-div').show()
                } else if (result.regions.length === 1) {
                    $('#region-select-div').hide()
                    getDepartments(result.regions[0]['_id']);
                } else {
                    $('#region-select-div').hide()
                }
            }).fail(function (data) {

            });
        }

        function getDepartments(id) {
            $('#department-select-div').hide()
            $('#department-select').empty()
            $('#city-select-div').hide()
            $('#city-select').empty()
            $.ajax({
                type: 'GET',
                dataType: 'json',
                url: '{{route('select.region')}}/' + id,
            }).done(function (result) {
                console.log(result)
                if (result.departments.length > 1) {
                    var option = new Option("Departments", "");

                    $("#department-select").append(option);
                    let i = 0;
                    for (i = 0; i < result.departments.length; i++) {
                        let ob = result.departments[i];
                        var option = new Option(ob.name, ob._id);

                        $("#department-select").append(option);
                    }
                    $('#department-select-div').show()
                } else if (result.departments.length === 1) {
                    $('#department-select-div').hide()
                    getCities(result.departments[0]['_id']);
                } else {
                    $('#department-select-div').hide()
                }
            }).fail(function (data) {

            });
        }

        function getCities(id) {
            $('#city-select-div').hide()
            $('#city-select').empty();
            $.ajax({
                type: 'GET',
                dataType: 'json',
                url: '{{route('select.department')}}/' + id,
            }).done(function (result) {
                console.log(result)
                try {
                    if (result.cities.length > 0) {
                        let i = 0;
                        // var option = '<option value="" >Cities</option>';
                        //
                        // $("#city-select").append(option);
                        for (i = 0; i < result.cities.length; i++) {
                            let ob = result.cities[i];
                            var option = '<option value="' + ob._id + '" data-zip="' + ob.zip_code + '">' + ob.name + '</option>';

                            $("#city-select").append(option);
                        }
                        $('#city-select-div').show();
                        if (postal_code_needed) {
                            $("#city-select").prop('disabled', true);
                            $("#zip_code").show();
                        } else {
                            $("#city-select").prop('disabled', false);
                            $("#zip_code").hide();
                        }
                        if (id_number_needed) {
                            $("#id_number").show();
                        } else {
                            $("#id_number").hide();
                        }
                    } else {
                        $('#city-select-div').hide()
                    }
                } catch (e) {

                }

            }).fail(function (data) {

            });
        }

        $('#country-select').on('change', function () {
            if ($('#country-select :selected').parent().attr('data-zip') === "0") {
                $('#city-select-div').show();
                $('#city-select').empty();
                $('#zip_code').show();
                $('#zip_code').val('');
            } else {
                $('#city-select-div').hide();
                $('#city-select').empty();
                $('#zip_code').hide();
                getRegions(this.value);
            }
        });
        $('#region-select').on('change', function () {
            getDepartments(this.value);
        });
        $('#department-select').on('change', function () {
            getCities(this.value);
        });

        if( $('#zip_code').val() != ''){
        if ($('#country-select :selected').parent().attr('data-zip') === "0") {
            $('#city-select-div').show();
            $('#zip_code').show();
            getCitiesByZipCode();
        } else {
            $('#city-select-div').hide();
            $('#city-select').empty();
            $('#zip_code').hide();
            getRegions(this.value);
        }
        }
        @if(session()->get('process.country','') != '')
                $('#zip_code').show();
        @endif
        $('#zip_code').on('keyup', function () {
            if ($('#zip_code').val().length > 3) {
                getCitiesByZipCode();
            }
            // $("#city-select").prop('disabled',true);
            // let selected = 0;
            // let i = 0;
            // $("#city-select option").each(function () {
            //     if ($('#zip_code').val() === $(this).attr('data-zip')) {
            //         i++;
            //         if (i >= 2){
            //             $("#city-select").prop('disabled',false);
            //             $("#city-select").children('option').hide();
            //             $("#city-select").children("option[value^=" + $(this).val() + "]").show()
            //         } else {
            //             $("#city-select").prop('disabled',true);
            //             selected = $(this).val();
            //         }
            //     }
            // });
            // $('#city-select').val(selected).trigger('change');
        });

        function getCitiesByZipCode() {
            $('#city-select-div').show();
            $('#zip_code').show();
            let id = $('#country-select').val();
            let zip_code = $('#zip_code').val();
            $.ajax({
                type: 'get',
                dataType: 'json',
                data: {
                    id: id,
                    zip_code: zip_code
                },
                url: '{{route('select.city')}}',
            }).done(function (result) {
                $("#city-select").empty();
                if (result.status) {
                    if (result.response.length > 0) {
                        let i = 0;
                        {{--if (result.response.length !== 1){--}}
                        {{--    var option = '<option value="" >{{__('front.city_label')}}</option>';--}}
                        {{--}--}}

                        // $("#city-select").append(option);
                        for (i = 0; i < result.response.length; i++) {
                            let ob = result.response[i];
                            let id = '';
                            console.log(ob);
                            @if( session()->get('process.city', '') != '')
                                id = '{{session()->get('process.city','')}}';
                            @endif
                            if (result.response.length === 1) {
                                var option = '<option value="' + ob._id + '" data-zip="' + ob.zip_code + '" selected>' + ob.name + '</option>';
                            } else {
                                if (id === ob._id)
                                    var option = '<option value="' + ob._id + '" data-zip="' + ob.zip_code + '" selected>' + ob.name + '</option>';
                                else
                                    var option = '<option value="' + ob._id + '" data-zip="' + ob.zip_code + '">' + ob.name + '</option>';
                            }

                            $("#city-select").append(option);
                        }
                        $("#city-select").prop('disabled', false);
                        $('#help-city').text('').hide();
                    } else {
                        $("#city-select").prop('disabled', true);
                        $('#help-city').text('{{html_entity_decode(__('front.no_city_available_in_this_region'))}}').show();
                    }
                } else {
                    $("#city-select").prop('disabled', true);
                    $('#help-city').text('{{html_entity_decode(__('front.wrong_pin_code'))}}').show();
                }
            }).fail(function (data) {
                console.log(data);
            });
        }


    </script>
@endpush
