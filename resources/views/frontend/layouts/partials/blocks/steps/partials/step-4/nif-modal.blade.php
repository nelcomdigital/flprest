<div id="proceed-to-payment-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="content-modal nif-code">
            <div class="m-auto w-100">
                <form method="get" action="{{route('address')}}">
                    <h3 class="m-auto pb-3 text-center">Please enter your NIF code</h3>
                    <div class="m-auto text-center pb-2">
                        <input type="text" placeholder="XXXXXXXXXXXXX" id="nif-number"
                               class="text-center w-100  border-0 p-2 bg-whites">
                    </div>
                    <div class="pb-3">
                        <div class="nif-label m-auto text-center inline-flex">
                            <input class="m-auto" type="radio" id="nif-code">
                            <label class="m-auto pl-2" for="nif-code">I don't have a NIF code</label>
                        </div>
                    </div>
                    <div class="m-auto text-center">
                        <button class="btn-submit" type="submit" id="submit-nif" >Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
