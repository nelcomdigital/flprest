<div class="register-text">
    <p class="roboto-bold" style="padding-right: 5px">{{__('front.documents')}}
        <b>{{__('front.to_provide')}}</b></p>
</div>
<br>
<div class="register-text">
    <p style="padding-right: 5px; font-size: 17px!important;">{{__('front.documents_text')}}</p>
</div>

<div class="phone-number">
    <label for="id_number"></label><input type="number" class="dist-input-text" name="id_number"
                                             value="{{session()->get('process.id_number','')}}"
                                             id="id_number" placeholder="{{__('front.id_number')}}">
    <span class="help-block" id="help-id_number"></span>
</div>

<div class="img-browse">
    <div class="yes">
        <span class="btn_upload" style="font-family: 'Roboto'; font-weight: 500; cursor: pointer">
            <input type="file" name="id_image" id="imag" title="" class="input-img" accept="image/*"/>
            Choose Image
        </span>
        <div style="position: relative">
            <img id="ImgPreview" src="" class="preview1" alt=""/>
            <div class="w-100 h-100" style="position: absolute; bottom: 0; right: 0;">
                <span id="removeImage1" class="btn-rmv1"></span>
            </div>
        </div>
    </div>
</div>

<div class="steps inline-flex w-100 mt-5 text-right">
    <div class="circle-arrow">
        <span onclick="steps(3)" class="cursor btn-submit-list">{{__('front.previous_step')}}</span>
        <span onclick="steps(5)" class="cursor btn-submit-green-list">{{__('front.next_step')}}</span>
    </div>
</div>
{{--<div class="steps inline-flex w-100 mt-5 text-right">--}}
{{--    <div class="circle-arrow">--}}
{{--            <span onclick="steps(3)" class="cursor mr-2">--}}
{{--                <img class="arrow-width" src="{{asset('images/arrow_left.svg')}}" alt="">--}}
{{--            </span>--}}
{{--        <span--}}
{{--            onclick="steps(5)"--}}
{{--            class="cursor">--}}
{{--                    <img class="arrow-width" src="{{asset('images/arrow_right.svg')}}" alt="">--}}
{{--                </span>--}}
{{--    </div>--}}
{{--</div>--}}

