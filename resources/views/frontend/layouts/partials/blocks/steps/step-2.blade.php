@if (session()->get('current_locale', 'fr'))
    <div class="register-text">
        <p class="pdo-number" style="padding-right: 5px">{{__('front.information')}}
            <b class="roboto-bold">{{__('front.personal')}}</b></p>
    </div>
@else
    <div class="register-text">
        <p class="pdo-number" style="padding-right: 5px">{{__('front.personal')}}
            <b class="roboto-bold">{{__('front.information')}}</b></p>
    </div>
@endif
<div class="gender">
    <div>
{{--        <p class="mt-4">{{__('front.gender')}}</p>--}}
    </div>
    <div class="gender-types inline-flex pright-20">
        <input class="m-auto" type="radio" name="gender" value="male" id="male"
               @if ( session()->get('process.genderType','') =='' || session()->get('process.genderType','') =='male')checked @endif>
        <label class="m-auto pl-2" for="male">{{__('front.male')}}</label>
    </div>
    <div class="gender-types inline-flex pright-20">
        <input class="m-auto" type="radio" name="gender" value="female" id="female"
               @if (  session()->get('process.genderType','') =='female')checked @endif>
        <label class="m-auto pl-2" for="female">{{__('front.female')}}</label>
    </div>
</div>
<div>
    <span class="help-block" id="help-gender"></span>
</div>

<div class="personal-info-div">
    <div class="inline-flex w-100">
        <label for="first_name"></label><input type="text" class="input-text mright-20"
                                               name="first_name" id="first_name"
                                               value="{{session()->get('process.first_name','')}}"
                                               placeholder="{{__('front.first_name')}}*">

        <label for="last_name"></label><input type="text" class="input-text ml-auto"
                                              name="last_name" id="last_name"
                                              value="{{session()->get('process.last_name','')}}"
                                              placeholder="{{__('front.last_name')}}*">
    </div>
</div>
<span class="help-block" id="help-name"></span>
<div class="w-100">
    <input type="email" class="input-text ml-auto" name="email" id="email"
           value="{{session()->get('process.email','')}}" placeholder="{{__('front.email')}}*">
    <span class="help-block" >
        <i class="fa fa-spinner fa-spin" id="check-email" style="display: none"></i>
    </span>
    <span class="help-block" id="help-email"></span>
    <input type="date" class="input-text ml-auto" name="birth_date" id="birth_date"
           @if ( session()->get('process.date_of_birth','') != '' )value="{{session()->get('process.date_of_birth','')}}" @endif
           placeholder="{{__('front.birth_date')}}*"
           data-placeholder="{{__('front.birth_date')}}*" required aria-required="true" max="2999-12-31">
    <span class="help-block" id="help-date-of-birth"></span>
    <input type="text" class="input-text ml-auto" name="reg_phone_number" id="reg_phone_number"
           value="{{session()->get('process.phone_number','')}}"
           placeholder="{{__('front.phone_number')}}*">
    <span class="help-block" id="help-phone-number"></span>
</div>

<div class="steps inline-flex w-100 mt-5 text-right">
    <div class="circle-arrow">
        <span onclick="steps(1)" class="cursor btn-submit-list">{{__('front.previous_step')}}</span>
        <span onclick="steps(3)" class="cursor btn-submit-green-list">{{__('front.next_step')}}</span>
    </div>
</div>

{{--<div class="steps inline-flex w-100 mt-5 text-right">--}}
{{--    <div class="circle-arrow">--}}
{{--        <span onclick="steps(1)" class="cursor mr-2"><img class="arrow-width" src="{{asset('images/arrow_left.svg')}}" alt=""></span>--}}
{{--        <span onclick="steps(3)" class="cursor"><img class="arrow-width" src="{{asset('images/arrow_right.svg')}}" alt=""></span>--}}
{{--    </div>--}}
{{--</div>--}}
