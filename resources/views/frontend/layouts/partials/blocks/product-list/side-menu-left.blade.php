{{--<div class="products-text">--}}
{{--</div>--}}
{{--<div class="d-none d-sm-block">--}}
<div id="accordion" style="margin-bottom: 5px;">
    <div class="" id="headingOne">
        <div class="d-none d-sm-block">
            <button class="products-text w-100 text-left inline-flex" data-toggle="collapse" data-target="#collapseOne"
                    aria-expanded="true" aria-controls="collapseOne">
                <b>{{__('front.products')}}</b>
                <i class="fa fa-arrow-circle-o-down text-right w-100 m-auto fa-lg"></i>
            </button>
        </div>

        <div class="d-block d-sm-none">
            <button class="products-text active w-100 text-left inline-flex " data-toggle="collapse" data-target="#collapseOne"
                    aria-expanded="true" aria-controls="collapseOne">
{{--                <b class="d-sm-none w-100">{{$category->name[session()->get('current_locale')]}}</b>--}}
                <i class="fa fa-arrow-circle-o-down text-right m-auto fa-lg"></i>
            </button>
        </div>

    </div>
    <div class="d-none d-sm-block">
        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
            @foreach( $categories as $category)
                @if ( $category->is_tool != "1")
                <div style="background-color: white;">
                    <a href="{{route('products',$category->id)}}">
                        <div id="headingOne" class="side-menu-cat">
                            <button
                                class="btn btn-collapse-custom @if(isset($slug) and $category->id == $slug) active @endif btn-products collapsed"
                                type="button">
                                {!!html_entity_decode( $category->label->{session()->get('current_locale')} ) !!}
                            </button>
                        </div>
                    </a>
                </div>
                @endif
            @endforeach
        </div>
    </div>
    <div class="d-block d-sm-none">
        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
            @foreach( $categories as $category)
                @if ( $category->is_tool != "1")
                <div style="background-color: white;">
                    <a href="{{route('products',$category->id)}}">
                        <div id="headingOne" class="side-menu-cat">
                            <button
                                class="btn btn-collapse-custom @if(isset($slug) and $category->id == $slug) active @endif btn-products collapsed"
                                type="button">
                                {{html_entity_decode($category->label->{session()->get('current_locale')}) }}
                            </button>
                        </div>
                    </a>
                </div>
                @endif
            @endforeach
        </div>
    </div>
</div>
