<!-- Modal -->
<div id="myModal-{{$product->_id}}" class="item-modal modal fade p-0" role="dialog" style="padding-right:0!important;">
    <div class="modal-dialog w-100 modal-dialog-container m-0 h-100">
        <!-- Modal content-->
        <div class="content-modal p-30">
            <div class="modal-img">
                <div class="row bg-white m-0">
                    <div class="col-md-5">
                        <img class="m-auto w-100" src="{{asset($product->image)}}" alt="">
                    </div>
                    <div class="col-md-6">
                        <div class="item-name-modal"><b>{{$product->name[session()->get('current_locale','es')]}}</b>
                        </div>
                        <div class="item-currency-modal">Ref: {{$product->reference}}</div>
                        <div class="item-details-modal">
                            {!! $product->short_description[session()->get('current_locale','es')] !!}
                        </div>
                        <div class="item-currency-modal-price">
                            <b> {{$product->getPrice()}}{{$product->price_label}}  </b></div>
                        <div class="add-to-cart-modal">
                            <button class="btn-shop outline-0" id="bag" onclick="addItem('{{$product->_id}}')"
                                    style="width: 20%;">
                                <i class="fa fa-shopping-bag"
                                   id="i-{{$product->_id}}">{{$product->getCurrentQuantity()}}</i>
                            </button>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <span class="fa fa-2x fa-arrow-circle-o-left cursor" data-dismiss="modal"
                              style="padding: 17px;" id="back-to-products"></span>
                    </div>
                </div>
            </div>
            <div>
                <div>
                    <div class="business-profile-box top-5">
                        <div class="business-profile-tabs">
                            <div class="business-profile-tab business-profile-tab-{{$product->_id}} active"
                                 id="{{$product->_id}}" target="packs-presentation-{{$product->_id}}">Presentation of
                                Packs
                            </div>
                            <div class="business-profile-tab business-profile-tab-{{$product->_id}}"
                                 id="{{$product->_id}}" target="description-{{$product->_id}}">Description
                            </div>
                            <div class="business-profile-tab business-profile-tab-{{$product->_id}}"
                                 id="{{$product->_id}}" target="advantages-{{$product->_id}}">Advantages
                            </div>
                            <div class="business-profile-tab business-profile-tab-{{$product->_id}}"
                                 id="{{$product->_id}}" target="ingredients-{{$product->_id}}">Ingredients
                            </div>
                        </div>
                    </div>
                </div>
                <div class="business-profile-tab-content business-profile-tab-content-{{$product->_id}} active"
                     id="packs-presentation-{{$product->_id}}" style="">
                    @isset($product->links)
                        <div class="row" id="dynamic-links">
                            @foreach($product->links[session()->get('current_locale', 'es')] as $links)
                                <div class="col-md-3">
                                    <iframe id="ytplayer" type="text/html" width="100%" height="200px"
                                            src="{{str_replace("watch?v=", "embed/", $links)}}/controls=2"
                                            frameborder="0" allowfullscreen="allowfullscreen"></iframe>
                                    <div><b>{{$product->getVideoData(str_replace("https://www.youtube.com/watch?v=","",$links))['title']}}</b></div>
                                    <div class="inline-flex w-100">
                                        <div style="color: rgba(210, 210, 210, 1);">{{substr($product->getVideoData(str_replace("https://www.youtube.com/watch?v=","",$links))['published'],0,10)}}</div>
                                        <div style="padding: 0 5px; color: rgba(210, 210, 210, 1);"> . </div>
                                        <div style="color: rgba(210, 210, 210, 1);">{{$product->getVideoData(str_replace("https://www.youtube.com/watch?v=","",$links))['views']}} Views</div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endisset
                </div>
                <div class="business-profile-tab-content business-profile-tab-content-{{$product->_id}}"
                     id="description-{{$product->_id}}" style="">
                    <p style="text-align: center;">
                        @isset($product->description)
                            {!!  $product->description[session()->get('current_locale','es')]!!}
                        @endisset
                    </p>
                </div>

                <div class="business-profile-tab-content business-profile-tab-content-{{$product->_id}}"
                     id="advantages-{{$product->_id}}" style="">
                    <p style="text-align: center;">
                        @isset($product->advantages)
                            {!!$product->advantages[session()->get('current_locale','es')]!!}
                        @endisset
                    </p>
                </div>

                <div class="business-profile-tab-content business-profile-tab-content-{{$product->_id}}"
                     id="ingredients-{{$product->_id}}" style="">
                    <p style="text-align: center;">
                        @isset($product->ingredients)
                            {!!$product->ingredients[session()->get('current_locale','es')]!!}
                        @endisset
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
