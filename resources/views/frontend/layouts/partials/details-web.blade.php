@extends('frontend.layouts.app')


@push('css')
    <style>
        html, body {
            background-color: #ececec;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            min-height: 100vh;
            margin: 0;
        }

        td {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        th {
            background-color: #9f9f9f40;
            text-align: left;
            padding: 8px;
        }

        .num-client-head {
            background-color: #808080b5;
            font-size: 15px;
            text-align: center;
            color: #a01919;
            font-weight: 700;
            padding: 10px;
            margin: 10px;
        }

        .document-info {
            padding: 0;
            background-color: #ececec;
        }

        input.form-control {
            margin-bottom: 5px;
        }

        .price-orange {
            color: #F9BF11;
        }

        .price-strikethrough {
            color: #636b6f;
            text-decoration: line-through;
        }

        .info-title {
            color: white;
            background-color: #808080b5;
            padding: 5px;
            margin-bottom: 5px;
        }

        .total-info {
            background-color: orange;
            color: white;
            display: inline-flex;
            width: 100%;
            padding: 5px;
            font-weight: 600;
            margin: 10px 0 0 0;
        }

        .total-num {
            text-align: end;
            padding: 5px;
            width: 20%;
        }

        .total-name {
            padding: 5px;
            width: 80%;
        }

        textarea#comment {
            min-height: 100px;
        }

        .blue-btn {
            background-color: #5064d3;
            color: white;
            padding: 5px;
            text-align: center;
            margin: 5px 0;
            font-weight: 600;
            cursor: pointer;
        }

        a.blue-a {
            color: #2196F3 !important;
            margin-right: 2px;
        }

        .frais-port {
            display: inline-flex;
            width: 100%;
            position: relative;
            margin-top: 10px;
        }

        .frais-num {
            position: absolute;
            right: -10px;
        }

        .totla-details {
            border: 1px solid #dddddd;
            padding: 5px;
            margin-bottom: 5px;
        }

        .payment-title {
            margin: 5px 0;
            background-color: #d9d9d9;
            padding: 3px;
        }

        .payment-sub-title {
            font-weight: bold;
            margin: 5px 0;
        }

        .payment-detail {
            margin: 5px 0;
        }
    </style>
@endpush

@section('content')
<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <form method="POST" action="{{route('vente.setComment')}}" enctype="multipart/form-data">
        @method('POST')
        @csrf
        {{-- <div>{{$result->infosFBO->firstName}}</div> --}}
        <div class="num-client-head">{{"N°"}}{{$result->listCommande[0]->idVente}}</div>
        <div class="col-xs-12">
            <div class="info-details">{{__('web.document-not-a-contract')}}</div>
            <div class="info-details">{{__('web.it-does-not-commit')}}.</div>
            <div class="info-details">{{__('web.purpose-of-signing')}}
                <a class="blue-a">{{__('web.entirety')}}</a>
                {{__('web.of-this-document')}}</div>
            {{-- <div class="blue-btn">Lire le document</div> --}}
        </div>

        <div class="col-xs-12 info-title">{{__('web.Client')}}</div>
        <div class="document-info col-xs-12">
            <div class="col-xs-6">
                <input type="text" class="form-control" readonly
                       value="@if (isset($result->listCommande[0]->infosClient->numClient))
                       {{$result->listCommande[0]->infosClient->numClient}}
                       @endif ">
            </div>
            <div class="col-xs-6">
                <input type="text" class="form-control" readonly
                       value="@if (isset($result->listCommande[0]->infosClient->numClient))
                       {{$result->listCommande[0]->infosClient->numClient}}
                       @endif">
            </div>
            <div class="col-xs-12">
                <input type="text" name="firstName" class="form-control " readonly placeholder="firstName"
                       value="@if (isset($result->listCommande[0]->infosClient->firstName))
                       {{$result->listCommande[0]->infosClient->firstName}}
                       @endif">
            </div>
            <div class="col-xs-12">
                <input type="text" name="lastName" class="form-control " readonly placeholder="lastName"
                       value="@if (isset($result->listCommande[0]->infosClient->lastName))
                       {{$result->listCommande[0]->infosClient->lastName}}
                       @endif">
            </div>
            <div class="col-xs-3">
                <input type="number" name="zipCode" class="form-control " readonly placeholder="zipCode"
                       value="@if (isset($result->listCommande[0]->infosClient->zipCode))
                       {{$result->listCommande[0]->infosClient->zipCode}}
                       @endif">
            </div>
            <div class="col-xs-9">
                <input type="text" name="country" class="form-control " readonly placeholder="country"
                       value="@if (isset($result->listCommande[0]->infosClient->country))
                       {{$result->listCommande[0]->infosClient->country}}
                       @endif">
            </div>
            <div class="col-xs-4">
                <input type="number" name="phoneNumber" class="form-control " readonly placeholder="phoneNumber"
                       value="@if (isset($result->listCommande[0]->infosClient->phoneNumbe))
                       {{$result->listCommande[0]->infosClient->phoneNumber}}
                       @endif">
            </div>
            <div class="col-xs-8">
                <input type="text" name="email" class="form-control " readonly placeholder="email"
                       value="@if (isset($result->listCommande[0]->infosClient->email))
                       {{$result->listCommande[0]->infosClient->email}}
                       @endif">
            </div>
            <div class="col-xs-12">
                <input type="text" name="address" class="form-control" readonly placeholder="address"
                       value="
                       @if (isset($result->listCommande[0]->infosClient->address))
                       {{$result->listCommande[0]->infosClient->address}}
                       @endif
                           ">
            </div>
        </div>

        <div class="col-xs-12 info-title">{{__('web.Distributer')}}</div>
        <div class="document-info col-xs-12">
            <div class="info-details">{{$result->infosFBO->firstName}}{{' '}}{{$result->infosFBO->lastName}}</div>
            <div class="col-xs-12">
                <input type="text" name="address" class="form-control" readonly placeholder="address"
                       value="{{$result->infosFBO->address}}">
            </div>
            <div class="col-xs-3">
                <input type="number" name="fbozipCode" class="form-control " readonly placeholder="zipCode"
                       value="{{$result->infosFBO->zipCode}}">
            </div>
            <div class="col-xs-9">
                <input type="text" name="fboCountry" class="form-control " readonly placeholder="country"
                       value="{{$result->infosFBO->country}}">
            </div>
            <div class="col-xs-4">
                <input type="number" name="fboPhoneNumber" class="form-control " readonly placeholder="phoneNumber"
                       value="{{$result->infosFBO->phoneNumber}}">
            </div>
            <div class="col-xs-8">
                <input type="text" name="fboEmail" class="form-control " readonly placeholder="email"
                       value="{{$result->infosFBO->email}}">
            </div>
        </div>
        <div class="col-xs-12 info-title">{{__('web.Products')}}</div>
        <div class="col-xs-12">
            <table style="width: 100%;">
                <tr>
                    <th>{{"Produits"}}</th>
                    <th>{{"Quantité"}}</th>
                    <th>{{"Prix"}}</th>
                </tr>
                @foreach ( $result->listCommande[0]->panierDetail as $commande )
                    <tr>
                        <td>{{$commande->designation}}</td>
                        <td>{{$commande->qte}}</td>
                        @if($commande->prixTTC == $commande->prixTTCRemise)
                            <td>{{$commande->prixTTCRemise}}{{"€"}}</td>
                        @else
                            <td>
                                <span class="price-strikethrough">{{$commande->prixTTC}}{{"€"}}</span>
                                <span class="price-orange">{{$commande->prixTTCRemise}}{{"€"}}</span>
                            </td>
                        @endif
                    </tr>
                @endforeach
            </table>
            <div class="frais-port">
                <div class="frais-name">{{__('web.Shiping-cost')}}</div>
                <div class="frais-num">
                    @if (isset($result->listCommande[0]->panierHead) and $result->listCommande[0]->panierHead != null)
                        {{$result->listCommande[0]->panierHead->FraisPortFBO}}{{"€"}}
                    @endif
                </div>
            </div>
            <div class="frais-port">
                <div class="frais-name">{{__('web.total-discount')}}</div>
                <div class="frais-num">
                    @if (isset($result->listCommande[0]->panierHead) and $result->listCommande[0]->panierHead != null)
                        {{$result->listCommande[0]->panierHead->montantRemiseTTC}}{{"€"}}
                    @endif

                </div>
            </div>
            <div class="total-info">
                <div class="total-name">{{__('web.Total')}}</div>

                <div class="total-num">
                    @if (isset($result->listCommande[0]->panierHead) and $result->listCommande[0]->panierHead != null)

                        @if($result->listCommande[0]->panierHead->totalBrutTTC == $result->listCommande[0]->panierHead->totalNetTTC)
                            {{$result->listCommande[0]->panierHead->totalNetTTC}}{{"€"}}
                        @else
                            <span
                                class="price-strikethrough">{{$result->listCommande[0]->panierHead->totalBrutTTC}}{{"€"}}</span>
                            <span> {{$result->listCommande[0]->panierHead->totalNetTTC}}{{"€"}}</span>
                        @endif
                    @endif
                </div>
            </div>
            <div class="totla-details">{{__('web.total-cost-detail')}}.</div>
            <div class="payment-title">{{__('web.payment-terms')}}</div>
            <div class="payment-sub-title">

                @if (isset($result->listCommande[0]) and $result->listCommande[0] != null)
                    @if (isset($result->listCommande[0]->panierHead) and $result->listCommande[0]->panierHead != null)
                      @if (isset($result->listCommande[0]->panierHead->LB_ModePaiement) and $result->listCommande[0]->panierHead->LB_ModePaiement != null)
                            {{$result->listCommande[0]->panierHead->LB_ModePaiement}}

                        @endif
                    @endif
                @endif
                {{--                    {{__('web.Species')}}--}}
            </div>
            @if (isset($result->listCommande[0]) and $result->listCommande[0] != null)
            @if (isset($result->listCommande[0]->panierHead) and $result->listCommande[0]->panierHead != null)
             @if (isset($result->listCommande[0]->panierHead->ModeLivraison) and $result->listCommande[0]->panierHead->ModeLivraison != null)
                <div class="payment-detail">{{__('web.payment-will-take-place')}}</div>
                <div class="payment-title">{{__('web.delivery-terms')}}</div>
                <div class="payment-sub-title">
                        {{$result->listCommande[0]->panierHead->ModeLivraison}}
                </div>
            @endif
            @endif
        @endif
        </div>

        {{-- <div class="col-xs-12 info-title">{{__('web.Commentaire')}}</div>
        <div class="document-info col-xs-12">
            <div class="col-xs-12">
                <textarea id="comment" name="comment" @if($comment != 1) readonly @endif
                class="form-control col-md-12 col-xs-12 ckeditor"
                          placeholder="{{__('web.Commentaire')}}">@if(isset($result->listCommande[0]->panierHead->commentaire)){!! $result->listCommande[0]->panierHead->commentaire !!}@endif</textarea>
                <input type="hidden" name="env" value="{{$env}}"/>
                <input type="hidden" name="idVente" value="{{$result->listCommande[0]->idVente}}"/>
            </div>
        </div>
        <div class="col-xs-12" @if($comment != 1) style="display: none" @endif>
            <button type="submit" class="btn btn-success btn-save"
                    data-loading-text="<i class='fa fa-spinner fa-spin '></i> Submitting"> {{__('cms.submit')}} </button>
        </div> --}}

    </form>
@endsection
