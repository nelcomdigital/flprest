<div class="header options-cartner" id="header">
    <div class="text-left" style="width: 30%; margin: auto 0 auto 5%;">
        <div class="header-label-lang m-auto inline-flex" id="cantre" style="margin-right: 2px;">

        </div>
    </div>
    <div class="text-center m-auto" style="width: 40%;">
        <div class="header-icon"><a href="{{route('welcome')}}"><img class="header-a-logo"
                                                                 src="{{asset('images/icons/Forever_TMblack.png')}}"></a></div>
    </div>
    <div class="text-right" style="width: 30%;padding-bottom: 1px; margin: auto 5% auto 0;">
            @if(Str::contains(request()->url(),'/product-list') or Str::contains(request()->url(),'/product'))
                <div class="header-label cursor w-50px inline-flex"  id="shopping-cart" onclick="goToCart()">
                    <img aria-hidden="true" style="width: 25px;" src="{{asset('images/Cart.svg')}}" alt="">
                    <span class="total-header-cart">{{$cart}}</span>
                </div>
            @else
            <div class="m-auto">

                    <span aria-hidden="true" id="sponsor-name" class="hidden-sm" style="display: none">
                        @if(session()->get('process.distributor_name','')!= "")
                            {{__('front.sponsor_name')}}
                        @endif
                    </span>
                <span aria-hidden="true" id="cus-name" style="display: none">{{session()->get('process.distributor_name','')}}</span>
            </div>
            @endif
    </div>

</div>
