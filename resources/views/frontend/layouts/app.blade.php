<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>For Ever Living</title>

    <link rel="shortcut icon" type="image/png" href="{{asset('images/favicon.ico')}}"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" />

    <!-- Bootstrap -->
    <link href="{{asset('vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,200,300,400,500,600,700&display=swap"
          rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="icon" href="{{asset('images/favicon.ico')}}" type="image/ico" />
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
{{--    <link href="{{asset('vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">--}}
    <link rel="stylesheet" href={{asset('css/main.css')}}>
    <link rel="stylesheet" href={{asset('bootstrap-4.3.1-dist/css/bootstrap.css')}}>
    <link rel="stylesheet" href="{{asset('font-awesome-4.7.0/css/font-awesome.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />



    {{--<link rel="stylesheet" href='../../../public/css/main.css'>--}}
    <head>
        <title>For Ever Living</title>
    </head>

    @stack('css')
</head>
<body>
<div id="app">
    <main>
        @yield('content')
    </main>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
{{--<script src="{{ asset('js/app.js') }}" defer></script>--}}
<script type="application/javascript">
    function putSession(keys, values, negative = null) {
        if (negative == null) {
            $.ajax({
                type: 'POST',
                url: '{{route('session.put')}}',
                data: {
                    key: keys,
                    value: values,
                    _token: "{{ csrf_token() }}",
                },
            }).done(function (result) {
            });
        } else {
            $.ajax({
                type: 'POST',
                url: '{{route('session.put')}}',
                data: {
                    key: keys,
                    value: values,
                    negative: negative,
                    _token: "{{ csrf_token() }}",
                },
            }).done(function (result) {
            });
        }
    }

</script>
<script>
    $(document).ready(function() {
        let sel = $('.select2');
        sel.select2();

    });

</script>
<script src="{{asset('bootstrap-4.3.1-dist/js/bootstrap.js')}}"></script>
@stack('js')

</body>
</html>
