@extends('frontend.layouts.app')
@section('content')

    <div id="main-content" style="margin-top: -5px">
        <div class="packs-container" id="packs-container">
            <div class="content-container" id="content-container">
                <div class="p-20 w-100">
                    <div class="page-title-description">
                        @isset($page)
                            {!! $page->description !!}
                        @endisset
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="choose-category-text d-block d-sm-none">
                        {{__('front.choose_a_product_category')}}
                    </div>
                    @include('frontend.layouts.partials.blocks.product-list.side-menu-left', ['categories'=>$categories])
                </div>
                <div class="col-md-9 packs-items">
                    <div class="d-none d-sm-block">
                        <div class="products-text">
                            <b>{{html_entity_decode($category->label->{session()->get('current_locale')}) }}</b>
                        </div>
                    </div>
                    <div class="items-container">
                        <div class="row">
                            @foreach($products as $k=> $product)
                                <div class="col-md-4">
                                    <div class="item-div">
                                        <div class="img-container cursor" >
                                            <div style="cursor: pointer" onclick="showProduct({{$k}})">
                                                <img class="item-img"
                                                     src="https://distrib.foreverliving.fr/450-450/media/image/produits/produit/{{$k}}.png"
                                                     alt=""/></div>
                                        </div>
                                        <div class="item-info">
                                            <div class="item-name">
                                                <b>{!! html_entity_decode($product['detail']->label->{session()->get('current_locale')}) !!}</b></div>
                                            <div class="item-details" style="text-align: center">
                                                Ref: {!! $k!!}
                                            </div>
                                            <form action="{{route('product.detail')}}" id="form-form-{{$k}}" method="POST">
                                                @csrf
                                                <input type="hidden" name="product" value="{{json_encode($product)}}">
                                                <input type="hidden" name="category" value="{{json_encode($category)}}">
                                            </form>
                                            <div class="item-currency">
                                                <b>{{number_format((float)($product['price']->prixTTC), 2)}} € / {{number_format((float)($product['price']->CC),3)}} CC </b></div>
                                            <div class="item-options">
                                                <button class="btn-shop outline-0 inline-flex" onclick="addItem('{{$k}}')">
                                                    <img aria-hidden="true" style="width: 20px;margin: auto 0 auto auto;" src="{{asset('images/Cart.svg')}}" alt="">
                                                    <span class="item-count-font" style="margin: auto auto auto 0;font-family: Roboto" id="i-{{$k}}">
                                                        {{getCurrentQuantity($k)}}</span>
                                                </button>
                                                <div onclick="showProduct({{$k}})"class="btn-shop outline-0 "style="text-align: center;cursor: pointer">
                                                    <button class="btn-product-search" style="border: 0; background: transparent"><i
                                                            class="fa fa-search"></i>
                                                    </button>
                                                </div>

                                            </div>
                                            <span id="out-stock-{{$k}}" style="display: none ;color: green;">{{__('front.out_of_stock')}}</span>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        {{--                        <div class="d-none d-sm-block">--}}
                        {{--                            <div class="steps inline-flex w-100 mt-5 text-center">--}}
                        {{--                                <div class="circle-arrow">--}}
                        {{--                                    <span class="cursor mr-2"><img class="arrow-width" onclick="location.href='{{route('welcome')}}'" src="{{asset('images/arrow_left.svg')}}" alt=""></span>--}}
                        {{--                                    <span onclick="steps(4)" class="cursor"><img class="arrow-width" src="{{asset('images/arrow_right.svg')}}" alt=""></span>--}}
                        {{--                                </div>--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}
                    </div>
                </div>
            </div>
            <div class="steps inline-flex w-100">
                <div class="circle-arrow">
                    <span onclick="location.href='{{route('welcome')}}#3'" class="cursor btn-submit-list">{{__('front.previous_step')}}</span>
                    <span onclick="goToCart()" class="cursor btn-submit-green-list" id="go_to_cart"> {{__('front.proceed_to_cart')}}</span>
                </div>
            </div>
            <div class="help-product-error">
                <span class="help-block product-error" id="product-alert">{{__('front.product_alert')}}</span>
            </div>

        </div>
    </div>
@endsection
@push('js')
    <script type="application/javascript">
        $(function () {
            $('.business-profile-tab').on('click', function () {
                let id = $(this).attr('id');
                let target = $(this).attr('target');
                location.href = "#" + target;
                profileTab(target, id);
            });
            function profileTab(ref, id) {

                let tab = $('.business-profile-tab[target="' + ref + '"]');
                let target = $(tab).attr('target');

                $('.business-profile-tab-' + id).each(function (key, value) {
                    $(value).removeClass('active');
                });
                $('.business-profile-tab-content-' + id).each(function (key, value) {
                    $(value).removeClass('active');
                });

                $(tab).addClass('active');
                $('.business-profile-tab-content#' + target).addClass('active');
            }
        });
        function goToCart() {
            if ( parseInt($('.total-header-cart').html()) > 0 ){
                location.href='{{route('cart')}}';
                $('#product-alert').hide();
            } else {
                $('#product-alert').show();
            }
        }

        function showProduct(id){
            $('#form-form-'+id).submit();
        }
        function addItem(id) {
// console.log($('#i-' + id).html());
            var obj = new Object();
            obj.action = "verifStockRef";
            obj.env = "{{session()->get('process.country_detail.products_code')}}";
            obj.region = "{{session()->get('process.country_detail.region_code')}}";
            obj.typliv = "{{session()->get('process.country_detail.product_type')}}";
            obj.ref = id;
            let tempi = parseInt($('#i-' + id).html());
            if ( isNaN(tempi)){
                tempi = 0;
            }
            tempi++;
            obj.qte = tempi;

            var json = JSON.stringify(obj);

            $.ajax({
                url: 'https://webservice.foreverliving.fr/wsinscription.php' ,
                data: json,
                dataType:'json',
                type: "POST",
                contentType: "application/json",

            }).done(function (result)
            {
                if ( result.success){
                    $('#out-stock-'+id).hide()
                    putSession(['item'], [id]);
                    $('#product-alert').hide();
                    if ($('#i-' + id).html() == ' '||$('#i-' + id).html().trim() == '') {
                        $('#i-' + id).html(1);
                    } else {

                        let iNum = parseInt($('#i-' + id).html());
                        $('#i-' + id).html(++iNum);
                    }
                    let iNum = 0;
                    let temp = parseInt($('.total-header-cart').html());
                    if ( !isNaN(temp)){
                        iNum = temp;
                    }
                    $('.total-header-cart').html(++iNum);
                }else{
                    $('#out-stock-'+id).show()
                }


            }).fail(function (data) {
            });


        }
    </script>
@endpush
