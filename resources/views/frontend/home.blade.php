@extends('frontend.layouts.app')
@section('content')
    <div class="content">
        <div class="w-100 m-auto">
            <div class="col-md-12 body-container">
                <div class="" style="width: 100%;text-align: center;">
                    <img style="margin: auto;" class="header-a-logo"
                         src="{{asset('images/icons/Forever_TMblack.png')}}">
                </div>
                <div style="display: none" class="col-md-6 m-auto ">
                    <div class="body-image-container">
                        <div class="body-image" id="random-image-change"
                             style="background-image: url('{{asset('images/Picture.jpg')}}');">
                            @if(session()->get('current_locale', 'fr'))
                                <img id="main-img-border" src="{{asset('images/France.svg')}}" alt="wallpaper">
                            @else
                                <img id="main-img-border" src="{{asset('images/Portugal.svg')}}" alt="wallpaper">
                            @endif
                        </div>
                    </div>
                </div>
                <div style="display: none" class="col-md-6 m-auto ">
                    <div class="welcome-body">
                        <form action="{{route('welcome')}}" id="regForm">
                            tab 1
                            <div id="tab1">
                                @include('frontend.layouts.partials.blocks.steps.step-1')
                            </div>
                            <div id="tab2" style="display:none; width: 100%">
                                @include('frontend.layouts.partials.blocks.steps.step-2')
                            </div>
                            <div id="tab3" style="display:none;">
                                @include('frontend.layouts.partials.blocks.steps.step-3')
                            </div>
                            <div id="tab4" style="display:none;">
                                @include('frontend.layouts.partials.blocks.steps.step-4')
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <style>
        input[type="date"]::before {
            content: attr(data-placeholder);
            width: 100%;
        }

        input[type="date"]:focus::before,
        input[type="date"]:valid::before {
            display: none
        }
    </style>
    <script type="text/javascript">
        var datefield = document.createElement("input")
        datefield.setAttribute("type", "date")
        if (datefield.type != "date") { //if browser doesn't support input type="date", load files for jQuery UI Date Picker
            document.write('<link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />\n')
            document.write('<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"><\/script>\n')
            document.write('<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"><\/script>\n')
        }
    </script>

    <script>
        if (datefield.type != "date") { //if browser doesn't support input type="date", initialize date picker widget:
            jQuery.noConflict();
            jQuery(function ($) { //on document.ready
                $('#birth_date').datepicker();
            })
        }
    </script>

@endpush

@push('js')
    {{--    <script>--}}
    {{--        $(function () {--}}
    {{--            $("#city-select").change(function () { //this occurs when select 1 changes--}}
    {{--                $("#zip_code").val($(this).find(':selected').attr("data-zip"));--}}
    {{--            });--}}
    {{--        });--}}
    {{--    </script>--}}


    <script type="application/javascript">

        function underAgeValidate(birthday) {
            // it will accept two types of format yyyy-mm-dd and yyyy/mm/dd
            var optimizedBirthday = birthday.replace(/-/g, "/");

            //set date based on birthday at 01:00:00 hours GMT+0100 (CET)
            var myBirthday = new Date(optimizedBirthday);

            // set current day on 01:00:00 hours GMT+0100 (CET)
            var currentDate = new Date().toJSON().slice(0, 10) + ' 01:00:00';


            // calculate age comparing current date and borthday
            var myAge = ~~((Date.now(currentDate) - myBirthday) / (31557600000));

            var check = 19;
            if (date_of_birth.includes('/')) {
                check = date_of_birth.slice(6, 8);
            } else {
                check = date_of_birth.slice(0, 2);
            }
            if (check != "19" && check != "20")
                return false;
            if (myAge < 18 || myAge > 100) {
                return false;
            } else {
                return true;
            }

        }

        function checkDis() {
            $('#check-submit').show();
            var obj = new Object();
            obj.action = "droitDeParrainer";
            obj.id_parrain = distributor_number;
            var json = JSON.stringify(obj);

            $.ajax({
                url: 'https://webservice.foreverliving.fr/wsinscription.php',
                data: json,
                dataType: 'json',
                type: "POST",
                contentType: "application/json",

            }).done(function (result) {
                console.log(result);
                $('#check-submit').hide();
                if (result.success) {
                    putSession(['distributor_number'], [distributor_number]);
                    putSession(['distributor_name'], [result.parrain.prenom + ' ' + result.parrain.nom]);

                    var pre = '{{__('front.sponsor_name')}}';
                    manager = result.manager;
                    setTimeout(function () {
                        document.getElementById("help-register_distributor_number").innerText = '';
                        document.getElementById("cus-name").innerText = result.parrain.prenom + ' ' + result.parrain.nom;
                        document.getElementById("sponsor-name").innerText = pre;
                        document.getElementById("tab1").style.display = 'none';
                        document.getElementById("tab2").style.display = 'block';
                        document.getElementById("tab3").style.display = 'none';
                        document.getElementById("tab4").style.display = 'none';
                        document.getElementById("main-img-border").src = "{{asset('images/p2.svg')}}";
                        $('.header-right').show();
                    }, 1000);


                } else {
                    document.getElementById("cus-name").style.display = 'none';
                    document.getElementById("sponsor-name").style.display = 'none';
                    document.getElementById("help-register_distributor_number").innerText = "{!!__('front.blocked_or_not_exist')!!}";
                }

            }).fail(function (data) {
                // temp
                document.getElementById("cus-name").style.display = 'none';
                document.getElementById("sponsor-name").style.display = 'none';
                document.getElementById("help-register_distributor_number").innerText = "{!!__('front.blocked_or_not_exist')!!}";
            });

        }

        function checkEmail() {

            $('#check-email').show();

            $.ajax({
                url: '{{route('check.internal.user')}}/' + email,
                dataType: 'json',
                type: "GET",
                contentType: "application/json",

            }).done(function (result) {
                console.log(result);

                // if (result) {
                var obj = new Object();
                obj.action = "verifFBO";
                obj.nom = last_name;
                obj.prenom = first_name;
                var arr = date_of_birth.split("-");
                obj.dateNaissance = arr[2] + "/" + arr[1] + "/" + arr[0];
                obj.email = email;

                var json = JSON.stringify(obj);

                $.ajax({
                    url: 'https://webservice.foreverliving.fr/wsinscription.php',
                    data: json,
                    dataType: 'json',
                    type: "POST",
                    contentType: "application/json",

                }).done(function (result) {
                    console.log(result);
                    $('#check-email').hide();
                    if (result.success) {
                        putSession(['first_name', 'last_name', 'email', 'date_of_birth', 'phone_number', 'genderType'],
                            [first_name, last_name, email, date_of_birth, phone_number, genderType]);
                        document.getElementById("tab1").style.display = 'none';
                        document.getElementById("tab2").style.display = 'none';
                        document.getElementById("tab3").style.display = 'block';
                        document.getElementById("tab4").style.display = 'none';
                        document.getElementById("main-img-border").src = "{{asset('images/p3.svg')}}";
                    } else {
                        switch (result.numErreur) {
                            case 201 :
                                document.getElementById("help-email").innerText = "{{html_entity_decode(__('front.error.201'))}}";
                                break;
                            case 202 :
                                document.getElementById("help-email").innerText = "{{html_entity_decode(__('front.error.202'))}}";
                                break;
                            case 203 :
                                document.getElementById("help-email").innerText = "{{html_entity_decode(__('front.error.203'))}}";
                                break;
                            default :
                                document.getElementById("help-email").innerText = "{{html_entity_decode(__('front.email_address_exists'))}}";
                        }

                    }

                }).fail(function (data) {
                    $('#check-email').hide();
                    document.getElementById("help-email").innerText = "{{html_entity_decode(__('front.email_address_exists'))}}";
                });
                {{--} else {--}}
                {{--    $('#check-email').hide();--}}
                {{--    document.getElementById("help-email").innerText = "{{html_entity_decode(__('front.email_address_exists'))}}";--}}
                {{--}--}}

            }).fail(function (data) {
                $('#check-email').hide();
                document.getElementById("help-email").innerText = "{{html_entity_decode(__('front.email_address_exists'))}}";
            });


        }

        function validateEmail(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(email).toLowerCase());
        }

        let email = '', genderType = '', first_name = '', last_name = '', phone_number = '', date_of_birth = '',
            distributor_number = '', city = '', street_address = '', additional_address = '', zip_code = '',
            remember_me = false, cart_access = false, cantre_access = false, country = '', department = '', region = '',
            id_number = '', manager = '';

        let images_pages = @json($pages);
        let image_counter = 0;
        try {
            let bkimg = images_pages[0];
            $("#random-image-change").css('background-image', 'url("{{asset('')}}' + bkimg.image + '")');
        } catch (e) {

        }

        function steps(n) {


            if (n === 1) {
                document.getElementById("tab1").style.display = 'block';
                document.getElementById("tab2").style.display = 'none';
                document.getElementById("tab3").style.display = 'none';
                document.getElementById("cus-name").style.display = 'none';
                document.getElementById("sponsor-name").style.display = 'none';
                @if(session()->get('current_locale', 'fr'))
                document.getElementById("main-img-border").src = "{{asset('images/France.svg')}}";
                @else
                document.getElementById("main-img-border").src = "{{asset('images/Portugal.svg')}}";
                @endif
                document.getElementById("tab4").style.display = 'none';
                try {
                    let bkimg = images_pages[0];
                    $("#random-image-change").css('background-image', 'url("{{asset('')}}' + bkimg['image'] + '")');
                } catch (e) {

                }
            } else if (n === 2) {
                try {
                    let bkimg = images_pages[1];
                    $("#random-image-change").css('background-image', 'url("{{asset('')}}' + bkimg['image'] + '")');
                } catch (e) {

                }
                distributor_number = document.getElementById("distributor_number").value;
                if (distributor_number === '') {
                    document.getElementById("help-register_distributor_number").innerText = "{{html_entity_decode(__('front.distributor_number_required'))}}";
                } else {
                    checkDis();
                }
                document.getElementById("cus-name").style.display = 'block';
                document.getElementById("sponsor-name").style.display = 'block';
            } else if (n === 3) {
                try {
                    let bkimg = images_pages[2];
                    $("#random-image-change").css('background-image', 'url("{{asset('')}}' + bkimg['image'] + '")');
                } catch (e) {

                }
                let error = 0;
                first_name = document.getElementById("first_name").value;
                last_name = document.getElementById("last_name").value;
                if (first_name === '' || last_name === '') {
                    document.getElementById("help-name").innerText = "{{html_entity_decode(__('front.name_required'))}}";
                    error++;
                } else {
                    document.getElementById("help-name").innerText = '';

                }

                email = document.getElementById("email").value;
                if (email === '') {
                    document.getElementById("help-email").innerText = "{{html_entity_decode(__('front.email_address'))}}";
                    error++;
                } else if (!validateEmail(email)) {
                    document.getElementById("help-email").innerText = "{{html_entity_decode(__('front.email_address'))}}";
                    error++;
                } else {
                    document.getElementById("help-email").innerText = '';

                }

                date_of_birth = document.getElementById("birth_date").value;
                if (date_of_birth === '') {
                    document.getElementById("help-date-of-birth").innerText = "{{html_entity_decode(__('front.date_required'))}}";
                    error++;
                } else if (!underAgeValidate(date_of_birth)) {
                    document.getElementById("help-date-of-birth").innerText = "{{html_entity_decode(__('front.older_18'))}}";
                    error++;
                } else {
                    document.getElementById("help-date-of-birth").innerText = "";

                }

                phone_number = document.getElementById("reg_phone_number").value;
                if (phone_number === '') {
                    document.getElementById("help-phone-number").innerText = "{{html_entity_decode(__('front.phone_number_required'))}}";
                    error++;
                } else if (phone_number.match(/[a-z]/i)) {
                    document.getElementById("help-phone-number").innerText = "{{html_entity_decode(__('front.phone_number_required'))}}";
                    error++;
                } else {
                    document.getElementById("help-phone-number").innerText = '';
                }

                let ele = document.getElementsByName("gender");
                for (let i = 0; i < ele.length; i++) {
                    if (ele[i].checked) {
                        genderType = ele[i].value;
                    }
                }
                if (genderType === '') {
                    document.getElementById("help-gender").innerText = '{{html_entity_decode(__('front.gender_required'))}}';
                    error++;
                } else {
                    document.getElementById("help-gender").innerText = '';
                }

                if (error == 0) {
                    checkEmail()
                }
                document.getElementById("cus-name").style.display = 'block';
                document.getElementById("sponsor-name").style.display = 'block';
            } else if (n === 4) {
                try {
                    let bkimg = images_pages[3];
                    $("#random-image-change").css('background-image', 'url("{{asset('')}}' + bkimg['image'] + '")');
                } catch (e) {

                }
                let addressError = 0;

                country = $('#country-select').find(':selected').val();
                if (country === '') {
                    document.getElementById("help-country").innerText = "{{html_entity_decode(__('front.country_required'))}}";
                    addressError++;
                } else {
                    document.getElementById("help-country").innerText = "";
                }

                city = $('#city-select').find(':selected').val();
                if (city === '') {
                    document.getElementById("help-city").innerText = "{{html_entity_decode(__('front.city_required'))}}";
                    addressError++;
                } else {
                    document.getElementById("help-city").innerText = "";
                }
                street_address = document.getElementById("street_address").value;
                if (street_address === '') {
                    document.getElementById("help-street-address").innerText =
                        "{!! html_entity_decode(__('front.street_address_required')) !!}";
                    addressError++;
                } else {
                    document.getElementById("help-street-address").innerText = "";
                }

                additional_address = document.getElementById("additional_address").value;

                zip_code = document.getElementById("zip_code").value;
                if (zip_code === '' && $('#zip_code').is(":visible") == true) {
                    document.getElementById("help-zip-code").innerText = "{{html_entity_decode(__('front.zipcode_required'))}}";
                    addressError++;
                } else {
                    document.getElementById("help-zip-code").innerText = "";
                }

                if ($("#city-select")[0].selectedIndex < 0) {
                    document.getElementById('help-city').innerText = "{{html_entity_decode(__('front.city_required'))}}";
                    addressError++;
                } else {
                    document.getElementById('help-city').innerText = "";
                }

                if ($('#rgpd').prop("checked") === false) {
                    document.getElementById('help-rgpd').innerText = "{{html_entity_decode(__('front.rgpd_required'))}}";
                    addressError++;
                } else {
                    document.getElementById('help-rgpd').innerText = "";
                }

                if (addressError == 0) {
                    if (!isNaN(id_number_needed) && id_number_needed) {
                        document.getElementById("tab1").style.display = 'none';
                        document.getElementById("tab2").style.display = 'none';
                        document.getElementById("tab3").style.display = 'none';
                        document.getElementById("tab4").style.display = 'block';
                        putSession(['city', 'country', 'street_address', 'additional_address', 'zip_code'],
                            [city, country, street_address, additional_address, zip_code]);
                    } else {
                        putSession(['city', 'country', 'street_address', 'additional_address', 'zip_code'],
                            [city, country, street_address, additional_address, zip_code]);
                        setTimeout(function () {
                            window.location.href = '{{route('products')}}';
                        }, 1000);

                    }
                }
                document.getElementById("cus-name").style.display = 'block';
                document.getElementById("sponsor-name").style.display = 'block';
            }
            if (n === 5) {
                id_number = document.getElementById('id_number').value;
                if (id_number === '') {
                    document.getElementById("help-id_number").innerText = "{{__('front.id_number_required')}}";
                } else {
                    document.getElementById("help-id_number").innerText = "";
                    putSession(['id_number'], [id_number]);
                    cart_access = true;
                    let url = window.location.protocol + '//' + window.location.hostname + '/product-list';
                    setInterval(function () {
                        window.location.href = url;{{--document.getElementById("main-img-border").src = "{{asset('images/p4.svg')}}";--}}
                    }, 500)
                }
                document.getElementById("cus-name").style.display = 'block';
                document.getElementById("sponsor-name").style.display = 'block';
            }

        }

        document.getElementById("cantre").addEventListener('click', function () {
            if (cantre_access) {
                let url = window.location.protocol + '//' + window.location.hostname + '/product-list';
                setInterval(function () {
                    window.location.href = url;{{--document.getElementById("main-img-border").src = "{{asset('images/p4.svg')}}";--}}
                }, 500)
            }
        });

        // document.getElementById('buy-within-72').addEventListener('click', function () {
        //     document.getElementById('thank-you2').style.display = 'block';
        //     document.getElementById('shopping-payment').style.display = 'none';
        //     document.getElementById("thank-you72-distributor").value = distributor_number;
        //     document.getElementById("thank-you72-name").value = first_name + " " + last_name;
        // });
        function readURL(input, imgControlName) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $(imgControlName).attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imag").change(function () {
            // add your logic to decide which image control you'll use
            var imgControlName = "#ImgPreview";
            readURL(this, imgControlName);
            $('.preview1').addClass('it');
            $('.btn-rmv1').addClass('rmv');
        });

        $("#removeImage1").click(function (e) {
            e.preventDefault();
            $("#imag").val("");
            $("#ImgPreview").attr("src", "");
            $('.preview1').removeClass('it');
            $('.btn-rmv1').removeClass('rmv');
        });
        $("#removeImage1").on('mouseenter', function (e) {
            $(this).addClass("fa fa-trash fa-2x");
        });
        $("#removeImage1").mouseleave(function (e) {
            $(this).removeClass("fa fa-trash fa-2x");
        });
        $(function () {
            if (window.location.href.split('/').pop() == '#3') {
                steps(3);
            }
        });

        @if ( $preId != null )
        steps(2)
        @endif
    </script>


@endpush
