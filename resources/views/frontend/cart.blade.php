@extends('frontend.layouts.app')

@section('content')

    <div id="amount-modal" class="modal" role="dialog" style="background-color: #8080806b;">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="nif-content-modal nif-code">
                <div class="m-auto w-100">
                    <h3 class="m-auto pb-3 text-center">{{__('front.amount_error_message')}}</h3>

                    <div class="m-auto text-center">
                        <button class="btn-submit w-100" onclick="hideAmountDialog()">{{__('front.dismiss')}}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="cart-container" id="cart-container">
        <div class="cart-text-header"><h1 style="padding: 10px 0;font-size: 2rem">{{__('front.your_cart')}}</h1></div>
        <div class="d-none d-sm-block">
            <div class="cart-table">
                <table style="width:100%">
                    <tr class="border-transparent-right">
                        <th class="w-25 text-left border-transparent">{{__('front.item_label')}}</th>
                        <th class="w-25 border-transparent">{{__('front.unit_price')}}</th>
                        <th class="w-25 border-transparent">{{__('front.quantity')}}</th>
                        <th class="w-25 border-transparent"></th>
                        <th class="w-25 border-transparent" style="display: none">{{__('front.total')}}</th>
                    </tr>
                    @isset($products)
                        @foreach ($products as $product)
                            <tr class="border-transparent-right" id="{{$product['price']->ref}}">
                                <th class="w-20 border-transparent">
                                    <div class="inline-flex w-100">
                                        <img alt="" style="width: 30%" src="https://distrib.foreverliving.fr/450-450/media/image/produits/produit/{{$product['price']->ref}}.png">
                                        <div>
                                            <div class="cart-item-name w-100" style="margin: auto 10px!important;">
                                                {!!html_entity_decode($product['detail']->label->{session()->get('current_locale')}) !!}</div>
                                            <div class="item-currency-modal w-100" style="
                                            font-weight: 300!important ;
                                            margin: auto 10px!important;
                                            text-align: left!important;
                                            text-transform: capitalize;">
                                                Ref: {{$product['price']->ref}}</div>
                                            <span class="help-block-red error out-stock-{{$product['price']->ref}}" style="display: none">{{__('front.out_of_stock')}}</span>

                                        </div>
                                    </div>
                                </th>
                                <th class="w-20 border-transparent">
                                    <div class="inline-flex w-100 m-auto">
                                        <div class="m-auto text-center font-16 inline-flex">
                                            <div class="unit-price" id="unit-price-{{$product['price']->ref}}">
                                                {{number_format((float)($product['price']->prixTTC), 2)}}
                                            </div>
                                            <div class="unit-currency" id="unit-currency-{{$product['price']->ref}}">
                                                €
                                            </div>
                                            /
                                            <div class="unit-price-cc" id="unit-price-cc-{{$product['price']->ref}}">

                                                {{number_format((float)($product['price']->CC), 3)}}

                                            </div>
                                            <div class="unit-currency-cc" id="unit-currency-cc-{{$product['price']->ref}}">
                                                CC
                                            </div>
                                        </div>
                                    </div>
                                </th>
                                <th class="w-20 border-transparent">
                                    <div class="inline-flex w-100 m-auto">
                                        <div class="fa fa-minus cursor m-auto sub"
                                             data-target="{{$product['price']->ref}}"></div>
                                        <input class="input-border-yellow mt-auto text-center mb-auto w-25 p-0"
                                               type="text"
                                               id="quantity-{{$product['price']->ref}}"
                                               name="quantity[]" readonly
                                               value="{{getCurrentQuantity($product['price']->ref)}}">
                                        <div class="fa fa-plus cursor m-auto add"
                                             data-target="{{$product['price']->ref}}"></div>
                                    </div>
                                    {{--                                    @endif--}}
                                </th>
                                <th class="w-10 border-transparent">
                                    <div class="inline-flex w-100 m-auto">
                                        <span class="pl-3 pr-3 fa fa-lg fa-trash m-auto cursor del"
                                              data-target="{{$product['price']->ref}}"></span>
                                    </div>
                                </th>
                                <th class="w-20 border-transparent" style="display: none;">
                                    <div class="inline-flex w-100 m-auto">
                                        <div class="m-auto text-center font-16 inline-flex">
                                            <div class="unit-price" id="unit-price-total-{{$product['price']->ref}}">
                                                {{number_format( (float)( (float) getCurrentQuantity($product['price']->ref) * (float) $product['price']->prixTTC ), 2)}}

                                            </div>
                                            <div class="unit-currency" id="unit-currency-total-{{$product['price']->ref}}">
                                                €
                                            </div>
                                            /
                                            <div class="unit-price-cc" id="unit-price-cc-total-{{$product['price']->ref}}">
                                                    {{number_format( (float)( (float) getCurrentQuantity($product['price']->ref) * (float) $product['price']->CC ), 3)}}

                                            </div>
                                            <div class="unit-currency-cc" id="unit-currency-cc-total-{{$product['price']->ref}}">
                                                CC
                                            </div>
                                        </div>
                                    </div>
                                </th>
                            </tr>
                        @endforeach
                    @endisset
                </table>
            </div>

            <div class="border-transparent-right w-100 p-3 text-right">
                <div class="text-right font-weight-bold inline-flex">
                    <div class="total-text">
                        {{__('front.total')}}
                    </div>
                    <div class="inline-flex cart-total">
                        <div class="unit-price" id="unit-price-grand-total">
                            @isset($total)
{{--                                {{$total}}--}}
                                {{number_format((float)($total), 2)}}
                            @else
                                0
                            @endisset
                        </div>
                        <div class="unit-currency" id="unit-currency-grand-total">
                            €
                        </div>
                        /
                        <div class="unit-price-cc" id="unit-price-cc-grand-total">
                            @isset($totalCC)
                                {{number_format((float)($totalCC), 3)}}
                            @else
                                0
                            @endisset
                        </div>
                        <div class="unit-currency-cc" id="unit-currency-cc-grand-total">
                            CC
                        </div>
                    </div>
                </div>
            </div>
        </div>


        {{--Mobile--}}
        <div class="d-block d-sm-none">
            @isset($products)
                @foreach ($products as $product)
                    <div class="inline-flex w-100" id="m{{$product['price']->ref}}" style="padding: 0 0 20px 0;!important;">
                        <div class="cart-img">
                            <img src="https://distrib.foreverliving.fr/450-450/media/image/produits/produit/{{$product['price']->ref}}.png">
                        </div>
                        <div class="cart-details">
                            <div class="cart-details-info">
                                <div
                                    class="mobile-cart-product-name">{!!html_entity_decode($product['detail']->label->{session()->get('current_locale')}) !!}</div>
                                <div class="mobile-cart-reference">item# {{$product['price']->ref}}</div>
                                <span class="help-block-red error out-stock-{{$product['price']->ref}}" style="display: none">{{__('front.out_of_stock')}}</span>
                                <div class="inline-flex" style="display: none">
                                    <div class="unit-price" id="mobile-unit-price-{{$product['price']->ref}}">{{$product['price']->prixTTC}}</div>
                                    /<div class="unit-price" id="mobile-unit-price-cc-{{$product['price']->ref}}">{{$product['price']->CC}}</div>
                                    <div class="unit-currency"
                                         id="mobile-unit-currency-{{$product['price']->ref}}"> € </div>
                                </div>
                                <div>

                                </div>
                                <div class="mobile-cart-add-sub w-100">
                                    {{--                                        @if ( $product->is_pack == null or $product->is_pack == 0 )--}}
                                    <div class="add-sub-div inline-flex">
                                        <span class="fa fa-minus-circle fa-lg mobile-sub"
                                              data-target="{{$product['price']->ref}}"
                                              style="margin: auto 0"></span>
                                        <input class="mobile-cart-quantity w-50" type="text"
                                               id="mobile-quantity-{{$product['price']->ref}}"
                                               name="mobile-quantity[]" readonly
                                               value="{{getCurrentQuantity($product['price']->ref)}}">
                                        <span class="fa fa-plus-circle fa-lg mobile-add"
                                              data-target="{{$product['price']->ref}}"
                                              style="margin: auto 0"></span>
                                    </div>
                                    {{--                                        @endif--}}
                                </div>
                            </div>
                            {{--                                <div class="m-auto"><span style="visibility: hidden">...</span></div>--}}
                            <div class="mobile-cart-price inline-flex" style="display: none;">
                                <div class="unit-price" id="mobile-unit-price-total-{{$product['price']->ref}}">
                                        {{number_format((float)(getCurrentQuantity($product['price']->ref)  *
                                        $product['price']->prixTTC), 2)}}
                                </div>
                                <div class="unit-currency" id="mobile-unit-currency-total-{{$product['price']->ref}}">
                                    €
                                </div>
                                =
                                <div class="unit-price-cc" id="mobile-unit-price-cc-total-{{$product['price']->ref}}">
                                        {{number_format((float)(getCurrentQuantity($product['price']->ref)  *
                                        ((float)$product['price']->CC)), 3)}}
                                </div>
                                <div class="unit-currency-cc" id="mobile-unit-currency-cc-total-{{$product['price']->ref}}">
                                    CC
                                </div>
                            </div>
                        </div>
                        <div class="cart-favorites m-auto">
                            <div class="mobile-cart-favorite">
                                <span class="fa fa-trash fa-lg mobile-del" data-target="{{$product['price']->ref}}"></span>
                            </div>
                        </div>

                    </div>
                @endforeach
            @endisset
            <div class="border-transparent-right w-100 p-2 text-center" style="background-color: black">
                <div class="text-right font-weight-bold inline-flex m-auto w-100">
                    <div class="total-text w-100 text-left">
                        {{__('front.total')}}
                    </div>
                    <div class="inline-flex cart-total w-100 text-right">
                        <div class="unit-price" id="mobile-unit-price-grand-total">
                            @isset($total)
                                {{number_format((float)$total, 2)}}
                            @else
                                0
                            @endisset
                        </div>
                        <div class="unit-currency" id="mobile-unit-currency-grand-total">
                            €
                        </div>
                        /
                        <div class="unit-price-cc" id="mobile-unit-price-cc-grand-total">
                            @isset($totalCC)
                                {{number_format((float)$totalCC, 3)}}
                            @else
                                0
                            @endisset
                        </div>
                        <div class="unit-currency-cc" id="mobile-unit-currency-cc-grand-total">
                            CC
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="cart-info">
            @if (isset($order) or session()->get('process.done',false))
            @else
                @isset($page)
                    {!! $page->description !!}
                @endisset
            @endif
        </div>
        <div class="d-none d-sm-block">
            <div class="btn-group-payment">
                <div class="text-right">
                    <a class="mr-5" id="continue_shopping" href="{{route('products')}}">{{__('front.continue_shopping')}}</a>
                    <button class="btn-submit" id="btn-buy-later"  @if (isset($order) and session()->get('process.done',false)) style="cursor: none; border: 0" @else onclick="laterBuy()" @endif>
                        @if (isset($order) and session()->get('process.done',false))
                            {{__('front.you_still_have')}} {{\Carbon\Carbon::parse($order->created_at)->addDays(3)->diffInHours(\Carbon\Carbon::now())}}
                        @else {{__('front.buy_within')}} 72 @endif {{__('front.hours')}}  @if (isset($order) and session()->get('process.done',false))  {{__('front.to_buy_product')}} @endif</button>
                    <button type="button" class="btn-submit-yellow" id="btn-cart-xs" >
                        {{__('front.checkout')}}
                    </button>
                </div>
            </div>
            <span class="help-block help-stock-error" style="text-align: right;width: 100%;float: right;margin-bottom: 10px;display: none">{{__('front.stock_error')}}</span>
        </div>
        <div class="d-block d-sm-none" style="min-height: inherit">
            <div class="btn-group-payment">
                <div class="text-center">
                    <a class="m-3" id="continue_shopping" href="{{route('products')}}">{{__('front.continue_shopping')}}</a>

                    <button class="btn-submit w-100 mt-3 " id="btn-buy-later"
                            @if (isset($order) and session()->get('process.done',false)) style="cursor: none; border: 0"
                            @else onclick="laterBuy()" @endif>  @if (isset($order) and session()->get('process.done',false))
                            {{__('front.you_still_have')}} {{\Carbon\Carbon::parse($order->created_at)->addDays(3)->diffInHours(\Carbon\Carbon::now())}}

                        @else {{__('front.buy_within')}} 72 @endif {{__('front.hours')}}  @if (isset($order) and session()->get('process.done',false))  {{__('front.to_buy_product')}} @endif</button>
                    <button type="button" class="btn-submit-yellow w-100 mt-3 " id="btn-cart" >
                        {{__('front.checkout')}}
                    </button>
                </div>
            </div>
            <span class="help-block help-stock-error" style="text-align: center;width: 100%;float:right;margin-bottom: 10px;display: none">{{__('front.stock_error')}}</span>
        </div>
    </div>
@endsection

@push('js')
    <script>
        function hideAmountDialog(){
            $('#amount-modal').hide();
        }
        $(function () {


            $("#continue_shopping").on('click', function () {
                location.href = "{{route('products')}}";
            });
            $("#btn-cart").on('click', function () {

                var check = true
                $('.error').each(function () {
                    if($(this).css('display') !== 'none' ){
                        check = false;
                    }
                });

                {{--if (parseFloat($("#unit-price-cc-grand-total").text().replace(',', '')) >= {{session()->get('process.country_detail.min_cc',1)}}) {--}}
                if (parseFloat($("#unit-price-grand-total").text().replace(',', '')) >=150) {
                    if (!check){
                        $('.help-stock-error').show();
                    } else {
                        $('.help-stock-error').hide();
                        location.href = '{{route('address',$order_id)}}';
                    }
                } else {
                    if(!check){
                        $('.help-stock-error').show();
                    } else {
                        $('.help-stock-error').hide();
                        $('#amount-modal').show();
                    }
                }

            });
            $("#btn-cart-xs").on('click', function () {
                var check = true
                $('.error').each(function () {
                    if($(this).css('display') !== 'none' ){
                        check = false;
                    }
                });
                {{--if (parseFloat($("#mobile-unit-price-cc-grand-total").text().replace(',', '')) >= {{session()->get('process.country_detail.min_cc',1)}}) {--}}
                if (parseFloat($("#mobile-unit-price-grand-total").text().replace(',', '')) >= 150) {
                    if (!check){
                        $('.help-stock-error').show();
                    } else {
                        $('.help-stock-error').hide();
                        location.href = '{{route('address',$order_id)}}';
                    }
                } else {
                    if(!check){
                        $('.help-stock-error').show();
                    } else {
                        $('.help-stock-error').hide();
                        $('#amount-modal').show();
                    }
                }
            });


            $('.add').on('click', function () {
                let target = $(this).attr('data-target');
                var obj = new Object();
                obj.action = "verifStockRef";
                obj.env = "{{session()->get('process.country_detail.products_code')}}";
                obj.region = "{{session()->get('process.country_detail.region_code')}}";
                obj.typliv = "{{session()->get('process.country_detail.product_type')}}";
                obj.ref = target;
                let tempi = Number($('#quantity-' + target).val()) ;
                if ( isNaN(tempi)){
                    tempi = 0;
                }
                tempi++;
                obj.qte = tempi;

                var json = JSON.stringify(obj);

                $.ajax({
                    url: 'https://webservice.foreverliving.fr/wsinscription.php' ,
                    data: json,
                    dataType:'json',
                    type: "POST",
                    contentType: "application/json",

                }).done(function (result) {
                    if ( result.success){

                        putSessionQty(target,tempi);
                        $('#quantity-' + target).val(Number($('#quantity-' + target).val()) + 1);
                        $('#mobile-quantity-' + target).val(Number($('#mobile-quantity-' + target).val()) + 1);
                        let AddUnitPrice = Number($("#unit-price-" + target).text());
                        let AddUnitPriceCC = Number($("#unit-price-cc-" + target).text());
                        let AddQuantity = Number($("#quantity-" + target).val());
                        let AddPrice_multiple = Number(AddUnitPrice) * Number(AddQuantity);
                        let AddPrice_multipleCC = Number(AddUnitPriceCC) * Number(AddQuantity);
                        $("#unit-price-total-" + target).text(Number(AddPrice_multiple).toFixed(2));
                        $("#unit-price-cc-total-" + target).text(Number(AddPrice_multipleCC).toFixed(3));
                        $("#mobile-unit-price-total-" + target).text(Number(AddPrice_multiple).toFixed(2));
                        $("#mobile-unit-price-cc-total-" + target).text((Number(AddPrice_multipleCC)).toFixed(3));
                        let total = Number($("#unit-price-grand-total").text().replace(',',''));
                        let totalCC = Number($("#unit-price-cc-grand-total").text().replace(',',''));
                        if ( total == NaN )
                            total = 0;
                        if ( totalCC == NaN )
                            totalCC = 0;
                        total += AddUnitPrice;
                        totalCC += AddUnitPriceCC;
                        let iNum = 0;
                        let temp = parseInt($('.total-header-cart').html());
                        if ( !isNaN(temp)){
                            iNum = temp;
                        }
                        $('.total-header-cart').html(++iNum);
                        $('#mobile-unit-price-cc-grand-total').text(Number(totalCC).toFixed(3));
                        $("#unit-price-cc-grand-total").text(Number(totalCC).toFixed(3));

                        $('.out-stock-'+target).hide();

                    }else{

                        $('.out-stock-'+target).show();
                    }


                }).fail(function (data) {
                });

            });

            $('.mobile-add').on('click', function () {



                let target = $(this).attr('data-target');
                var obj = new Object();
                obj.action = "verifStockRef";
                obj.env = "{{session()->get('process.country_detail.products_code')}}";
                obj.region = "{{session()->get('process.country_detail.region_code')}}";
                obj.typliv = "{{session()->get('process.country_detail.product_type')}}";
                obj.ref = target;
                let tempi = Number($('#quantity-' + target).val()) ;
                if ( isNaN(tempi)){
                    tempi = 0;
                }
                tempi++;
                obj.qte = tempi;

                var json = JSON.stringify(obj);

                $.ajax({
                    url: 'https://webservice.foreverliving.fr/wsinscription.php' ,
                    data: json,
                    dataType:'json',
                    type: "POST",
                    contentType: "application/json",

                }).done(function (result) {
                    if ( result.success){

                        putSessionQty(target,tempi);
                        $('#quantity-' + target).val(Number($('#quantity-' + target).val()) + 1);
                        $('#mobile-quantity-' + target).val(Number($('#mobile-quantity-' + target).val()) + 1);
                        let AddUnitPrice = Number($("#mobile-unit-price-" + target).text());
                        let AddUnitPriceCC = Number($("#mobile-unit-price-cc-" + target).text());
                        let AddQuantity = Number($("#mobile-quantity-" + target).val());
                        let AddPrice_multiple = Number(AddUnitPrice) * Number(AddQuantity);
                        let AddPrice_multipleCC = Number(AddUnitPriceCC) * Number(AddQuantity);
                        $("#unit-price-total-" + target).text(Number(AddPrice_multiple).toFixed(2));
                        $("#unit-price-cc-total-" + target).text(Number(AddPrice_multipleCC).toFixed(3));
                        $("#mobile-unit-price-total-" + target).text(Number(AddPrice_multiple).toFixed(2));
                        $("#mobile-unit-price-cc-total-" + target).text((Number(AddPrice_multipleCC)).toFixed(3));
                        let total = Number($("#mobile-unit-price-grand-total").text().replace(',',''));
                        let totalCC = Number($("#mobile-unit-price-cc-grand-total").text().replace(',',''));

                        if ( total == NaN )
                            total = 0;
                        if ( totalCC == NaN )
                            totalCC = 0;
                        total += AddUnitPrice;
                        totalCC += AddUnitPriceCC;
                        let iNum = 0;
                        let temp = parseInt($('.total-header-cart').html());
                        if ( !isNaN(temp)){
                            iNum = temp;
                        }
                        $('.total-header-cart').html(++iNum);
                        $('#mobile-unit-price-cc-grand-total').text(Number(totalCC).toFixed(3));
                        $("#unit-price-cc-grand-total").text(Number(totalCC).toFixed(3));

                        $('.out-stock-'+target).hide();

                    }else{

                        $('.out-stock-'+target).show();
                    }


                }).fail(function (data) {
                });
            });

            $('.sub').on('click', function () {

                let target = $(this).attr('data-target');
                var obj = new Object();
                obj.action = "verifStockRef";
                obj.env = "{{session()->get('process.country_detail.products_code')}}";
                obj.region = "{{session()->get('process.country_detail.region_code')}}";
                obj.typliv = "{{session()->get('process.country_detail.product_type')}}";
                obj.ref = target;
                let tempi = Number($('#quantity-' + target).val()) ;
                if ( isNaN(tempi)){
                    tempi = 0;
                }
                tempi--;
                obj.qte = tempi;

                var json = JSON.stringify(obj);

                $.ajax({
                    url: 'https://webservice.foreverliving.fr/wsinscription.php' ,
                    data: json,
                    dataType:'json',
                    type: "POST",
                    contentType: "application/json",

                }).done(function (result) {
                    if (Number($('#quantity-' + target).val()) > 1) {
                        putSessionQty(target,tempi,1);
                        $('#quantity-' + target).val(Number($('#quantity-' + target).val()) - 1);
                        $('#mobile-quantity-' + target).val(Number($('#mobile-quantity-' + target).val()) - 1);
                        let SubUnitPrice = Number($("#unit-price-" + target).text());
                        let SubUnitPriceCC = Number($("#unit-price-cc-" + target).text());
                        let SubQuantity = Number($("#quantity-" + target).val());
                        let SubPrice_multiple = Number(SubUnitPrice) * Number(SubQuantity);
                        let SubPrice_multipleCC = Number(SubUnitPriceCC) * Number(SubQuantity);
                        $("#unit-price-total-" + target).text(Number(SubPrice_multiple).toFixed(2));
                        $("#unit-price-cc-total-" + target).text(Number(SubPrice_multipleCC).toFixed(3));
                        $("#mobile-unit-price-total-" + target).text(Number(SubPrice_multiple).toFixed(2));
                        $("#mobile-unit-price-cc-total-" + target).text((Number(SubPrice_multipleCC)).toFixed(3));

                        let total = Number($("#unit-price-grand-total").text().replace(',',''));
                        let totalCC = Number($("#unit-price-cc-grand-total").text().replace(',',''));
                        if ( total == NaN )
                            total = 0;
                        if ( totalCC == NaN )
                            totalCC = 0;
                        total -= SubUnitPrice;
                        totalCC -= SubUnitPriceCC;
                        $('#mobile-unit-price-cc-grand-total').text(Number(totalCC).toFixed(3));
                        $("#unit-price-cc-grand-total").text(Number(totalCC).toFixed(3));
                        let iNum = 0;
                        let temp = parseInt($('.total-header-cart').html());
                        if ( !isNaN(temp)){
                            iNum = temp;
                        }
                        $('.total-header-cart').html(--iNum);

                    } else
                    {
                        putSessionQty(target,tempi,2);
                        let SubUnitPrice = Number($("#unit-price-" + target).text());
                        let SubUnitPriceCC = Number($("#unit-price-cc-" + target).text());
                        let SubQuantity = Number($("#quantity-" + target).val());
                        let SubPrice_multiple = Number(SubUnitPrice) * Number(SubQuantity);
                        let SubPrice_multipleCC = Number(SubUnitPriceCC) * Number(SubQuantity);
                        $("#unit-price-total-" + target).text(Number(SubPrice_multiple).toFixed(2));
                        $("#unit-price-cc-total-" + target).text(Number(SubPrice_multipleCC).toFixed(3));
                        $("#mobile-unit-price-total-" + target).text(Number(SubPrice_multiple).toFixed(2));
                        $("#mobile-unit-price-cc-total-" + target).text((Number(SubPrice_multipleCC)).toFixed(3));
                        let total = Number($("#unit-price-grand-total").text().replace(',',''));
                        let totalCC = Number($("#unit-price-cc-grand-total").text().replace(',',''));
                        if ( total == NaN )
                            total = 0;
                        if ( totalCC == NaN )
                            totalCC = 0;
                        total -= SubUnitPrice;
                        totalCC -= SubUnitPriceCC;
                        $('#mobile-unit-price-cc-grand-total').text(Number(totalCC).toFixed(3));
                        $("#unit-price-cc-grand-total").text(Number(totalCC).toFixed(3));
                        let iNum = 0;
                        let temp = parseInt($('.total-header-cart').html());
                        if ( !isNaN(temp)){
                            iNum = temp;
                        }
                        $('.total-header-cart').html(--iNum);
                        $('#' + target).remove();
                        $('#m' + target).remove();
                    }

                    if ( result.success){


                        $('.out-stock-'+target).hide();

                    }else{

                        $('.out-stock-'+target).show();
                    }


                }).fail(function (data) {
                });

            });

            $('.mobile-sub').on('click', function () {

                let target = $(this).attr('data-target');
                var obj = new Object();
                obj.action = "verifStockRef";
                obj.env = "{{session()->get('process.country_detail.products_code')}}";
                obj.region = "{{session()->get('process.country_detail.region_code')}}";
                obj.typliv = "{{session()->get('process.country_detail.product_type')}}";
                obj.ref = target;
                let tempi = Number($('#quantity-' + target).val()) ;
                if ( isNaN(tempi)){
                    tempi = 0;
                }
                tempi--;
                obj.qte = tempi;

                var json = JSON.stringify(obj);

                $.ajax({
                    url: 'https://webservice.foreverliving.fr/wsinscription.php' ,
                    data: json,
                    dataType:'json',
                    type: "POST",
                    contentType: "application/json",

                }).done(function (result) {
                    if (Number($('#mobile-quantity-' + target).val()) > 1) {
                        putSessionQty(target,tempi,1);
                        $('#quantity-' + target).val(Number($('#mobile-quantity-' + target).val()) - 1);
                        $('#mobile-quantity-' + target).val(Number($('#mobile-quantity-' + target).val()) - 1);
                        let SubUnitPrice = Number($("#mobile-unit-price-" + target).text());
                        let SubUnitPriceCC = Number($("#mobile-unit-price-cc-" + target).text());
                        let SubQuantity = Number($("#mobile-quantity-" + target).val());
                        let SubPrice_multiple = Number(SubUnitPrice) * Number(SubQuantity);
                        let SubPrice_multipleCC = Number(SubUnitPriceCC) * Number(SubQuantity);
                        $("#unit-price-total-" + target).text(Number(SubPrice_multiple).toFixed(2));
                        $("#unit-price-cc-total-" + target).text(Number(SubPrice_multipleCC).toFixed(3));
                        $("#mobile-unit-price-total-" + target).text(Number(SubPrice_multiple).toFixed(2));
                        $("#mobile-unit-price-cc-total-" + target).text((Number(SubPrice_multipleCC)).toFixed(3));

                        let total = Number($("#mobile-unit-price-grand-total").text().replace(',', ''));
                        let totalCC = Number($("#mobile-unit-price-cc-grand-total").text().replace(',', ''));
                        if (total == NaN)
                            total = 0;
                        if (totalCC == NaN)
                            totalCC = 0;
                        total -= SubUnitPrice;
                        totalCC -= SubUnitPriceCC;
                        $('#mobile-unit-price-cc-grand-total').text(Number(totalCC).toFixed(3));
                        $("#unit-price-cc-grand-total").text(Number(totalCC).toFixed(3));
                        let iNum = 0;
                        let temp = parseInt($('.total-header-cart').html());
                        if (!isNaN(temp)) {
                            iNum = temp;
                        }
                        $('.total-header-cart').html(--iNum);

                    } else
                    {
                        putSessionQty(target,tempi,2);
                        let SubUnitPrice = Number($("#mobile-unit-price-" + target).text());
                        let SubUnitPriceCC = Number($("#mobile-unit-price-cc-" + target).text());
                        let SubQuantity = Number($("#mobile-quantity-" + target).val());
                        let SubPrice_multiple = Number(SubUnitPrice) * Number(SubQuantity);
                        let SubPrice_multipleCC = Number(SubUnitPriceCC) * Number(SubQuantity);
                        $("#unit-price-total-" + target).text(Number(SubPrice_multiple).toFixed(2));
                        $("#unit-price-cc-total-" + target).text(Number(SubPrice_multipleCC).toFixed(3));
                        $("#mobile-unit-price-total-" + target).text(Number(SubPrice_multiple).toFixed(2));
                        $("#mobile-unit-price-cc-total-" + target).text((Number(SubPrice_multipleCC)).toFixed(3));
                        let total = Number($("#mobile-unit-price-grand-total").text().replace(',', ''));
                        let totalCC = Number($("#mobile-unit-price-cc-grand-total").text().replace(',', ''));
                        if (total == NaN)
                            total = 0;
                        if (totalCC == NaN)
                            totalCC = 0;
                        total -= SubUnitPrice;
                        totalCC -= SubUnitPriceCC;
                        let iNum = 0;
                        let temp = parseInt($('.total-header-cart').html());
                        if (!isNaN(temp)) {
                            iNum = temp;
                        }
                        $('.total-header-cart').html(--iNum);
                        $("#mobile-unit-price-cc-grand-total").text(Number(totalCC).toFixed(3));
                        $("#unit-price-cc-grand-total").text(Number(totalCC).toFixed(3));
                        if ($('#mstock-' + target) != null) {
                            $('#btn-cart-xs').attr('disabled', false);
                            $('#btn-cart').attr('disabled', false);
                            $('#btn-buy-later').attr('disabled', false);
                        }
                        $('#' + target).remove();
                        $('#m' + target).remove();
                        $('.help-stock-error').hide();
                    }
                    if ( result.success){



                        $('.out-stock-'+target).hide();

                    }else{

                        $('.out-stock-'+target).show();
                    }


                }).fail(function (data) {
                });

            });

            $('.del').on('click', function () {
                let target = $(this).attr('data-target');
                putSessionQty(target,0,2);
                let SubUnitPrice = Number($("#unit-price-total-" + target).text().replace(',', ''));
                let SubUnitPriceCC = Number($("#unit-price-cc-total-" + target).text());
                let total = Number($("#unit-price-grand-total").text().replace(',',''));
                let totalCC = Number($("#unit-price-cc-grand-total").text().replace(',',''));
                if ( total == NaN )
                    total = 0;
                if ( totalCC == NaN )
                    totalCC = 0;
                // console.log(total)
                // console.log(totalCC)
                total -= SubUnitPrice;
                totalCC -= SubUnitPriceCC;

                $("#mobile-unit-price-cc-grand-total").text(Number(totalCC).toFixed(3));
                $("#unit-price-cc-grand-total").text(Number(totalCC).toFixed(3));

                $('#' + target).remove();
            });

            $('.mobile-del').on('click', function () {
                let target = $(this).attr('data-target');
                putSessionQty(target,0,2);
                let SubUnitPrice = Number($("#mobile-unit-price-total-" + target).text().replace(',', ''));
                let SubUnitPriceCC = Number($("#mobile-unit-price-cc-total-" + target).text());
                let total = Number($("#mobile-unit-price-grand-total").text().replace(',',''));
                let totalCC = Number($("#mobile-unit-price-cc-grand-total").text().replace(',',''));
                if ( total == NaN )
                    total = 0;
                if ( totalCC == NaN )
                    totalCC = 0;
                totalCC -= SubUnitPriceCC;
                $("#mobile-unit-price-cc-grand-total").text(Number(totalCC).toFixed(3));
                $("#unit-price-cc-grand-total").text(Number(totalCC).toFixed(3));

                $('#m' + target).remove();
            });

        });
    </script>
    <script type="application/javascript">
        function laterBuy() {

            var check = true
            $('.error').each(function () {
                if($(this).css('display') !== 'none' ){
                    check = false;
                }
            });
            {{--if (parseFloat($("#unit-price-cc-grand-total").text().replace(',', '')) >= {{session()->get('process.country_detail.min_cc',1)}} ) { --}}
                if (parseFloat($("#unit-price-grand-total").text().replace(',', '')) >= 150 ) {
                if (!check){
                    $('.help-stock-error').show();
                } else {
                    $('.help-stock-error').hide();
                    location.href = "{{route('later-buy')}}";
                }
            } else {
                if(!check){
                    $('.help-stock-error').show();
                } else {
                    $('.help-stock-error').hide();
                    $('#amount-modal').show();
                }
            }
        }

        function calculateTotal(){
            $.ajax({
                type: 'GET',
                url: '{{route('ajax.calculate.total')}}',
                data: {
                    _token: "{{ csrf_token() }}",
                },
            }).done(function (result) {
                $("#mobile-unit-price-grand-total").text(Number(result).toFixed(2));
                $("#unit-price-grand-total").text(Number(result).toFixed(2));
            }).fail( function ( result){
                console.log(result)
            });
        }

    </script>

    <script type="application/javascript">
        function putSessionQty(keys, values, negative = null) {
            $("#mobile-unit-price-grand-total").text('');
            $("#unit-price-grand-total").text('');
            if (negative == null) {
                $.ajax({
                    type: 'POST',
                    url: '{{route('session.put.qty')}}',
                    data: {
                        key: keys,
                        value: values,
                        _token: "{{csrf_token()}}",
                    },
                }).done(function (result) {
                    $("#mobile-unit-price-grand-total").text(Number(result).toFixed(2));
                    $("#unit-price-grand-total").text(Number(result).toFixed(2));
                });
            } else {
                $.ajax({
                    type: 'POST',
                    url: '{{route('session.put.qty')}}',
                    data: {
                        key: keys,
                        value: values,
                        negative: negative,
                        _token: "{{csrf_token()}}",
                    },
                }).done(function (result) {
                    $("#mobile-unit-price-grand-total").text(Number(result).toFixed(2));
                    $("#unit-price-grand-total").text(Number(result).toFixed(2));
                });
            }
        }

    </script>
@endpush
