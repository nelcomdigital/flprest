@extends('frontend.layouts.app')

@section('content')
    @include('frontend.layouts.partials.header')

    <div class="row" >
        <div class="p-5 w-100 bg-white" id="thank-you">
            <div class="thank-you-title text-lg-left" style="margin-top: 50px;"><h1><b>{{__('front.congrats-customer')}}</b></h1>
            </div>
            <div class="thank-you-label inline-flex w-100">
                <div class="visa-logos mr-auto"></div>
            </div>
            <div class="thank-you-container inline-flex">
                <div class="p-2 w-100" style="margin-top: 30px">
                    <p style="font-size: 16px;">{!!__('front.customer-success-payment')!!}{{' '.$id.'. '}}{!!__('front.customer-success-payment-text-2')!!}  </p>
                    <p style="font-size: 16px;"><b>{!!__('front.customer-success-payment-text')!!}</b></p>
                    <p style="font-size: 16px;"><b>{!!__('front.customer-success-payment-text-1')!!}</b></p>
                </div>
            </div>
            <div class="button-yellow">
                <a href="https://direct.foreverliving.fr/" style="color: black">
                    {{'Accéder à mon compte'}}
                </a>
            </div>
        </div>

    </div>
    @include('frontend.layouts.partials.footer')

@endsection

@push('js')

@endpush
