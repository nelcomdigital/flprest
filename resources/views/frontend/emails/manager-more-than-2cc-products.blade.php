
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Forever Living</title>
    <style type="stylesheet">
        p {
            font-family: Arial, Helvetica, sans-serif;
        }

        strong {
            font-family: Arial, Helvetica, sans-serif;
        }

        button {
            font-family: Arial, Helvetica, sans-serif;
        }
    </style>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 10px;">
<div style="width: 200px">
    <a target="_blank"><img style="width: 100%; cursor: pointer"
                            src="https://join.foreverliving.fr/images/icons/socials/header.jpg"/></a>
</div>
<div style="margin-bottom: 30px">
    <p>{{__('front.email.inscription_client_privilégié_m.00001')}}</p>
</div>
<div style="">
    <p>{{__('front.email.inscription_client_privilégié_m.00002')}} {{ucwords($s_name)}} ( {{$reference}} ) {{__('front.email.inscription_client_distributeur_m.00001')}}

    </p>
</div>
<div style="margin: 10px 0 30px 0">
    <p>{{__('front.email.inscription_client_distributeur_m.00002')}}</p>
</div>
<div style="margin: 20px 0 ">
    <p>{{__('front.email.inscription_client_privilégié_m.00005')}}</p>
</div>

<div >
    <p style="margin: 0;"><strong>{{__('front.email.inscription_client_privilégié_p.00007')}}: {{$s_first_name}}</strong></p>
    <p style="margin: 0;"><strong>{{__('front.email.inscription_client_privilégié_p.00008')}}: {{$s_last_name}}</strong></p>
    <p style="margin: 0;"><strong>{{__('front.email.inscription_client_privilégié_p.00009')}}: {{$s_address}}<br/>{{$s_zip_code}}<br/>{{$s_city}}</strong></p>
    <p style="margin: 0;"><strong>{{__('front.email.inscription_client_privilégié_p.00010')}}: {{$s_phone}}</strong></p>
    <p style="margin: 0 0 50px 0;"><strong>{{__('front.email.inscription_client_privilégié_p.00011')}}: {{$s_mail}}</strong></p>

    <p style="margin: 0;"><strong>{{__('front.email.inscription_client_privilégié_m.00006')}}: {{$d_first_name}}</strong></p>
    <p style="margin: 0;"><strong>{{__('front.email.inscription_client_privilégié_m.00007')}}: {{$d_last_name}}</strong></p>
</div>

<div style="text-align: center">
    <p>{{__('front.email.inscription_client_privilégié_p.00013')}}</p>
</div>
{{--@include('frontend.emails.email-footer')--}}
</body>
</html>
