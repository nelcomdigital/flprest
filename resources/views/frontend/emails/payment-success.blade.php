<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Forever Living</title>
    <style type="stylesheet">
        p {
            font-family: Arial, Helvetica, sans-serif;
        }

        strong {
            font-family: Arial, Helvetica, sans-serif;
        }

        button {
            font-family: Arial, Helvetica, sans-serif;
        }
    </style>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 10px;">
<div style="width: 200px">
    <a target="_blank"><img style="width: 100%; cursor: pointer"
                            src="https://join.foreverliving.fr/images/later.jpg"/></a>
</div>
<div style="margin-bottom: 30px">
    <p>{{__('front.email.label.10023')}},</p>
</div>

<div style="margin: 10px 0 30px 0">
    <p>{{__('front.email.label.10024')}}</p>
</div>


<div>
    <p>{{__('front.email.label.10025')}}</p>
</div>


<div >
    <p style="margin: 0;">{{$first_name}} {{$last_name}}, {{__('front.email.label.10026')}} !</p>
    <p style="margin: 0;">{{__('front.email.label.10027')}}{{$num_order}}.</p>
    <p style="margin: 0;">{{__('front.email.label.10028')}}</p>
    <p style="margin: 0;">{{__('front.email.label.10029')}}</p>
    <p style="margin: 10px 0;">{{__('front.email.label.10030')}}</p>
    <p style="margin: 0;">{{__('front.email.label.10031')}}</p>
    <p style="margin: 0;">{{__('front.email.label.10032')}}</p>
    <p style="margin: 20px 0 0 0;">{{__('front.email.label.10033')}}<a href="https://www.foreverliving.fr." target="_blank">www.foreverliving.fr</a></p>
    <p style="margin: 0;">{{__('front.email.label.10034')}}</p>

</div>


</body>
</html>
