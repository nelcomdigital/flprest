<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Forever Living</title>
    <style type="stylesheet">
        p {
            font-family: Arial, Helvetica, sans-serif;
        }

        strong {
            font-family: Arial, Helvetica, sans-serif;
        }

        button {
            font-family: Arial, Helvetica, sans-serif;
        }
    </style>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 10px;">
<div style="width: 200px">
    <a target="_blank"><img style="width: 100%; cursor: pointer"
                            src="https://join.foreverliving.fr/images/icons/socials/header.jpg"/></a>
</div>
<div style="">
  <p>{{__('front.email.inscription_client_privilégié.00001')}},</p>
</div>
<div style="">
    <p>{{__('front.email.inscription_client_privilégié.00002')}}
        <strong>{{__('front.email.inscription_client_privilégié.00003')}}</strong></p>
</div>
<div style="margin: 30px 0 ">
    <p style="margin: 0 "><strong>{{__('front.email.inscription_client_privilégié.00004')}} </strong></p>
    <p style="margin: 0 "><strong>{{__('front.email.inscription_client_privilégié.00005')}}: {{$reference}}</strong></p>
    <p style="margin: 0 "><strong>{{__('front.email.inscription_client_privilégié.00006')}}: {{$password}} </strong></p>
</div>
<div style="margin: 15px 0 ">
    <p>{{__('front.email.inscription_client_privilégié.00007')}}
        <strong>{{__('front.email.inscription_client_privilégié.00008')}} </strong> {{__('front.email.inscription_client_privilégié.00009')}}
    </p>
</div>
<div>
    <ul>
        <li>{{__('front.email.sponsee.label00021')}}</li>
        <li>{{__('front.email.sponsee.label00022')}}</li>
        <li>{{__('front.email.sponsee.label00023')}}</li>
        <li>{{__('front.email.sponsee.label00024')}}</li>
    </ul>
</div>

<div style="margin:30px 0 ">
    <p>{{__('front.email.inscription_client_privilégié.00010')}}</p>
</div>


<div style="margin: 30px 0 0 0">
    <p  style="margin: 0 "><strong>{{__('front.email.inscription_client_privilégié.00016')}} : {{$d_first_name}} </strong></p>
    <p  style="margin: 0 "><strong>{{__('front.email.inscription_client_privilégié.00017')}} : {{$d_last_name}} </strong></p>
    <p  style="margin: 0 "><strong>{{__('front.email.inscription_client_privilégié.00018')}} : {{$d_phone}} </strong></p>
    <p style="margin:0 0 50px 0"><strong>{{__('front.email.inscription_client_privilégié.00019')}} : {{$d_mail}}</strong></p>
</div>
<div style="margin:30px 0 ">
    <p>{{__('front.email.inscription_client_privilégié.00011')}}</p>
    <p>*{{__('front.email.inscription_client_privilégié.00012')}}</p>
</div>
<div style="margin:30px 0 ">
    <p>{{__('front.email.inscription_client_privilégié.00013')}}</p>
</div>
<div style="margin:30px 0 ">
    <p>{{__('front.email.inscription_client_privilégié.00014')}}</p>
</div>
<div style="display: inline-flex; margin-bottom: 20px ;text-align: center">
    <a href=" https://play.google.com/store/apps/details?id=net.thenetgroup.foreverliving.FLPMobil&hl=fr" target="_blank">
        <img src="{{asset('images/play-stor.png')}}" alt="" style="width: 100px;height: 55px;cursor: pointer"/>
    </a>
    <a href="https://apps.apple.com/fr/app/flpmobil/id1289138967" target="_blank">
        <img src="{{asset('images/apple-store.png')}}" alt="" style="width: 100px;height: 55px;cursor: pointer"/>
    </a>
</div>
<div style="margin:30px 0;text-align: center ">
    <p>{{__('front.email.inscription_client_privilégié.00015')}}</p>
</div>
</body>
</html>

