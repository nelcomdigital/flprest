<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Forever Living</title>
    <style type="stylesheet">
        p {
            font-family: Arial, Helvetica, sans-serif;
        }

        strong {
            font-family: Arial, Helvetica, sans-serif;
        }

        button {
            font-family: Arial, Helvetica, sans-serif;
        }
    </style>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 10px;">
<div style="width: 200px">
    <a  target="_blank"><img style="width: 100%; cursor: pointer" src="https://join.foreverliving.fr/images/icons/socials/Forever-living-new.png"/></a>
</div>
<div style="margin-bottom: 30px">
    <p>{{__('front.Hello')}}</p>
</div>
<div style="margin-bottom: 30px">
    <p>{{__('front.email.sponsee.label00010')}}</p>
</div>
<table style="width: 100%; border-collapse: collapse;">
    <tr style="border: 1px solid rgba(215, 215, 215, 1);">
        <th class="border-transparent" style="font-size:12px;width: 25%; text-align: left;padding: 12px;
                    text-transform: uppercase;border: 1px solid rgba(215, 215, 215, 1);">{{__('front.item_label')}}</th>
        <th class="border-transparent" style="width: 25%;
                    padding: 12px;font-size:12px;border: 1px solid rgba(215, 215, 215, 1);
                    text-transform: uppercase;
                    text-align: center;
                    ">{{__('front.unit_price')}}</th>
        <th class="border-transparent" style="width: 25%;
                    padding: 12px;font-size:12px;border: 1px solid rgba(215, 215, 215, 1);
                    text-transform: uppercase;
                    text-align: center;
                    ">{{__('front.quantity')}}</th>
        <th class="border-transparent" style="width: 25%;
                    padding: 12px;border: 1px solid rgba(215, 215, 215, 1);
                    text-transform: uppercase;font-size:12px;
                    text-align: center;
                    ">{{__('front.total')}}</th>
    </tr>
    @foreach ($order['details'] as $product)

            <tr style="border: 1px solid rgba(215, 215, 215, 1);">
                <th class="border-transparent" style="width: 20%;
                        padding: 5px 0;
                        text-transform: uppercase;
                        text-align: center;border: 1px solid rgba(215, 215, 215, 1);
                        ">
                    <div style="display: inline-flex; width: 100%">
{{--                        <img alt="" src="{{asset($product->image)}}" style="width: 25%">--}}
                        <div>
                            <div style="margin: auto 10px!important; width: 100%;font-size:12px; text-align: left">
                                {{$product['product_name']}}</div>
                            <div style="font-weight: 300!important ;
                                            width: 100%;
                                            margin: auto 10px!important;
                                            text-align: left!important;
                                            text-transform: capitalize;font-size: 14px;
                                            color: #888888;font-size:12px;
                                            font-family: 'Roboto';">
                                Ref: {{$product['product_reference']}}</div>
                        </div>
                    </div>
                </th>
                <th style="
                            width: 20%;
                            padding: 5px 0;border: 1px solid rgba(215, 215, 215, 1);
                            text-transform: uppercase;
                            text-align: center;
                            ">
                    <div style="margin: auto !important; display: inline-flex">
                        <div style="display: inline-flex;  text-align: center; margin: auto">
                            <div style="padding: 0 4px;font-size:12px;">
                                {{number_format((float)($product['price'] + $product['price_tax']), 2)}}
                            </div>
                            <div style="padding: 0 4px;font-size:12px;">
                                €
                            </div>
                            /
                            <div style="padding: 0 4px;font-size:12px;">
                                {{number_format((float)($product['price_point']), 3)}}
                            </div>
                            <div style="padding: 0 4px;font-size:12px;">
                                CC
                            </div>
                        </div>
                    </div>
                </th>
                <th style="width: 20%;
                    padding: 5px 0;font-size:12px;
                    text-transform: uppercase;
                    text-align: center;border: 1px solid rgba(215, 215, 215, 1);
                    ">
                    <div style="width: 100%; margin: auto; display: inline-flex">
                        <input type="text"
                               style="margin: auto;font-size:10px;
                               padding: 5px;
                               text-align: center;
                                border: 0;"
                               name="quantity[]" readonly
                               value="{{$product['quantity']}}">
                    </div>
                </th>
                <th style="
                    width: 20%;
                    padding: 5px 0;
                    text-transform: uppercase;
                    text-align: center;border: 1px solid rgba(215, 215, 215, 1);
                    ">
                    <div style="display: inline-flex; width: 100%; margin: auto">
                        <div style="display: inline-flex;text-align: center; margin: auto">
                            <div style="padding: 0 4px;font-size:12px;">
                                @if($product['quantity'] > 0)
                                    {{number_format((float)($product['total']), 2)}}
                                @endif
                            </div>
                            <div style="padding: 0 4px;font-size:12px;" >
                                €
                            </div>
                            <div style="font-size: 10px">/</div>
                            <div style="padding: 0 4px;font-size:12px;" >
                                @if($product['quantity'] > 0)
                                    {{number_format((float)($product['quantity'] * ($product['price_point']) ), 3)}}
                                @endif
                            </div>
                            <div style="padding: 0 4px;font-size:12px;">
                                CC
                            </div>
                        </div>
                    </div>
                </th>
            </tr>
    @endforeach

    <tr style="border: 1px solid rgba(215, 215, 215, 1);">
        <th class="border-transparent" style="font-size:12px;width: 25%; text-align: left;padding: 12px;
                    text-transform: uppercase;border: 1px solid rgba(215, 215, 215, 1);"></th>
        <th class="border-transparent" style="width: 25%;
                    padding: 12px;font-size:12px;border: 1px solid rgba(215, 215, 215, 1);
                    text-transform: uppercase;
                    text-align: center;
                    "></th>
        <th class="border-transparent" style="width: 25%;
                    padding: 12px;font-size:12px;border: 1px solid rgba(215, 215, 215, 1);
                    text-transform: uppercase;
                    text-align: center;
                    ">{{__('front.total')}}</th>
        <th class="border-transparent" style="width: 25%;
                    padding: 12px;border: 1px solid rgba(215, 215, 215, 1);
                    text-transform: uppercase;font-size:12px;
                    text-align: center;
                    ">{{$order->total}} €/ {{$order->points}} CC</th>
    </tr>
</table>
{{--<div style="margin: 15px 0 ">--}}
{{--    <p>{{__('front.email.sponsee.label00011')}} <strong>{{__('front.email.sponsee.label00012')}}</strong></p>--}}
{{--</div>--}}

{{--<div style="margin: 30px 0 ">--}}
{{--    <a href="https://foreverliving.com/?store=ESP&language=es"--}}
{{--            style="    border-radius: 50px;--}}
{{--    background-color: #ffc600;--}}
{{--    padding: 10px 90px;--}}
{{--    border: 0;--}}
{{--    cursor: pointer;--}}
{{--    font-weight: 700;">{{__('front.email.sponsee.label00013')}}--}}
{{--    </a>--}}
{{--</div>--}}
<div style="margin:30px 0 ">
    <p>{{__('front.email_footer_mobile_new')}}</p>
</div>

<div style="display: inline-flex; margin-bottom: 20px">
    <a href=" https://play.google.com/store/apps/details?id=net.thenetgroup.foreverliving.FLPMobil&hl=fr" target="_blank">
        <img src="{{asset('images/play-stor.png')}}" alt="" style="width: 150px;height: 55px;cursor: pointer"/>
    </a>
</div>
<div style="margin:30px 0 ">
    <p>{{__('front.email_footer_new')}}<a href="https://foreverliving.com/?store=FRA&language=fr">
            <strong> {{__('front.email.click_here')}}</strong></a> </p>

</div>
@include('frontend.emails.email-footer')
</body>
</html>
