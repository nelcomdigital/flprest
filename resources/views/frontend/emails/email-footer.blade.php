<div style="width: 200px; margin: 10px 0">
    <a  target="_blank" ><img style="width: 100%; cursor: pointer" src="https://join.foreverliving.fr/images/icons/socials/Forever-living-new.png"/></a>
</div>
<div class="w-100 inline-flex" style="margin: 10px 0;">
    <div class="social-media">
        <span style="display: inline-flex;">
            <a href="https://www.facebook.com/ForeverFranceHQ/"><img style="width: 35px; cursor: pointer" src="https://join.foreverliving.fr/images/icons/socials/FB.png"/></a></span>
        <span style="display: inline-flex;">
            <a href="https://twitter.com/ForeverFranceHQ" ><img style="width: 35px; cursor: pointer" src="https://join.foreverliving.fr/images/icons/socials/TT.png"/></a></span>
        <span style="display: inline-flex;">
            <a href="https://www.linkedin.com/company/18297255/" ><img style="width: 35px; cursor: pointer" src="https://join.foreverliving.fr/images/icons/socials/LIN.png"/></a></span>
        <span style="display: inline-flex;">
            <a href="https://www.instagram.com/foreverfrancehq/" ><img style="width: 35px; cursor: pointer" src="https://join.foreverliving.fr/images/icons/socials/IN.png"/></a></span>
        <span style="display: inline-flex;">
            <a href="https://www.youtube.com/channel/UCMzGVmSB60PTYyWFZFQunig/videos"><img style="width: 35px; cursor: pointer" src="https://join.foreverliving.fr/images/icons/socials/YT.png"/></a></span>
        <span style="display: inline-flex;">
            <a href="https://www.pinterest.fr/foreverfranceHQ/"><img style="width: 35px; cursor: pointer" src="https://join.foreverliving.fr/images/icons/socials/pinterest.jpg"/></a></span>
    </div>
</div>
