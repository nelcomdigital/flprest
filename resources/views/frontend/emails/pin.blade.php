<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Forever Living</title>
    <style type="stylesheet">
        p {
            font-family: Arial, Helvetica, sans-serif;
        }

        strong {
            font-family: Arial, Helvetica, sans-serif;
        }

        button {
            font-family: Arial, Helvetica, sans-serif;
        }
    </style>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 10px;">
<div style="width: 200px">
    <a  target="_blank"><img style="width: 100%; cursor: pointer" src="https://join.foreverliving.fr/images/icons/socials/Forever-living-new.png"/></a>
</div>
<div style="margin-bottom: 30px">
    <p>{{__('front.email_Hello')}}</p>
</div>
<div style="margin-bottom: 30px">
    <p>{{__('front.email.pin_proceed_to_checkout_and_validate')}} <strong>{{__('front.email.pin_please_enter_this_PIN_code :')}} {{$pin}}</strong> </p>
</div>
<div style="margin-bottom: 50px">
    <p>{{__('front.email_see_you_soon')}}</p>
</div>
@include('frontend.emails.email-footer')
</body>
</html>
