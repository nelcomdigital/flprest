@extends('frontend.layouts.app')

@section('content')
    @include('frontend.layouts.partials.header')
    <div class="w-100 bg-white" id="shopping-address">

        <div class="row">

            <div class="col-md-12 p-0">
                <div class="col-md-12">
                    <svg xmlns="http://www.w3.org/2000/svg" width="50" height="40" fill="currentColor"
                         class="bi bi-truck" viewBox="0 0 16 16">
                        <path
                            d="M0 3.5A1.5 1.5 0 0 1 1.5 2h9A1.5 1.5 0 0 1 12 3.5V5h1.02a1.5 1.5 0 0 1 1.17.563l1.481 1.85a1.5 1.5 0 0 1 .329.938V10.5a1.5 1.5 0 0 1-1.5 1.5H14a2 2 0 1 1-4 0H5a2 2 0 1 1-3.998-.085A1.5 1.5 0 0 1 0 10.5v-7zm1.294 7.456A1.999 1.999 0 0 1 4.732 11h5.536a2.01 2.01 0 0 1 .732-.732V3.5a.5.5 0 0 0-.5-.5h-9a.5.5 0 0 0-.5.5v7a.5.5 0 0 0 .294.456zM12 10a2 2 0 0 1 1.732 1h.768a.5.5 0 0 0 .5-.5V8.35a.5.5 0 0 0-.11-.312l-1.48-1.85A.5.5 0 0 0 13.02 6H12v4zm-9 1a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm9 0a1 1 0 1 0 0 2 1 1 0 0 0 0-2z"/>
                    </svg>
                    <span class="shipping-title">{{__('front.livraison')}}</span>
                </div>
                <div class="col-md-6">
                    <div class="shipping-box-options">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="option-shipping-label">{{__('front.shipping_type')}}*</div>
                            </div>
                            <div class="col-md-6">


                                <input type="radio" id="RELAIS" name="shipping_type" value="RELAIS">
                                  <label for="RELAIS">{{__('front.from_check_point')}}</label><br>


                                <input type="radio" id="DOM" name="shipping_type" value="DOM">
                                  <label for="DOM">{{__('front.from_home')}}</label><br>


                            </div>
                        </div>

                        <div class="row" style="display: none" id="shipping_cost_value">
                            <div class="col-md-4">
                                <div class="option-shipping-label">{{__('front.shipping_cost')}}*</div>
                            </div>
                            <div class="col-md-6" id="shipping_cost"></div>
                        </div>
                        <div class="row" style="display: none" id="free-shipping-cost">
                            <div class="col-md-6">{{__('front.shipping_free_cost')}}</div>
                        </div>

                    </div>
                </div>
            </div>
            <div id="shipping-form-title" style="display:none;" class="col-md-12 mt-5">
                <div class="col-md-12">
                    <svg xmlns="http://www.w3.org/2000/svg" width="50" height="40" fill="currentColor"
                         class="bi bi-house" viewBox="0 0 16 16">
                        <path fill-rule="evenodd"
                              d="M2 13.5V7h1v6.5a.5.5 0 0 0 .5.5h9a.5.5 0 0 0 .5-.5V7h1v6.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5zm11-11V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z"/>
                        <path fill-rule="evenodd"
                              d="M7.293 1.5a1 1 0 0 1 1.414 0l6.647 6.646a.5.5 0 0 1-.708.708L8 2.207 1.354 8.854a.5.5 0 1 1-.708-.708L7.293 1.5z"/>
                    </svg>
                    <span class="shipping-title">
                            {{__('front.shipping_address')}}
                    </span>


                </div>
            </div>

            <div id="shipping-form-box" style="display: none" class="col-lg-6 col-md-12">
                @csrf
                <div class="address-container inline-flex">
                    <div class="w-100">
                        <div class="shipping-box-options">
                            <table class="col-md-12">


                                <tr>
                                    <th class="padding-1"
                                        style="font-family: 'Roboto'; font-weight: 500">{{__('front.name')}}*
                                    </th>
                                    <th class="shipping"><input id="shipping-name"
                                                                value="{{$client['firstName']}} {{$client['lastName']}}"
                                                                class="shipping-input-text shipping-input"
                                                                name="shipping_name" type="text"></th>
                                </tr>
                                <tr>
                                    <th class="padding-1"
                                        style="font-family: 'Roboto'; font-weight: 500">{{__('front.first-name')}}*
                                    </th>
                                    <th class="shipping"><input id="shipping-first-name"
                                                                value="{{$client['firstName']}}"
                                                                class="shipping-input-text shipping-input"
                                                                name="shipping_name" type="text"></th>
                                </tr>
                                <tr>
                                    <th class="padding-1"
                                        style="font-family: 'Roboto'; font-weight: 500">{{__('front.last-name')}}*
                                    </th>
                                    <th class="shipping"><input id="shipping-last-name"
                                                                value="{{$client['lastName']}}"
                                                                class="shipping-input-text shipping-input"
                                                                name="shipping_name" type="text"></th>
                                </tr>
                                <tr>
                                    <th class="padding-1"
                                        style="font-family: 'Roboto'; font-weight: 500">{{__('front.country')}}*
                                    </th>
                                    <th class="shipping">
                                        <select id="shipping-country" name="shipping-country"
                                                class="shipping-input-text shipping-input">
                                            @foreach($countries as $country)
                                                <option value="{{$country->country_code}}"
                                                        @if ($country->country_code == $client['country'])
                                                            selected
                                                    @endif>
                                                    {{$country->name['fr']}}
                                                </option>
                                            @endforeach

                                        </select>

                                </tr>

                                <tr>
                                    <th class="padding-1"
                                        style="font-family: 'Roboto'; font-weight: 500">{{__('front.zip_code')}}*
                                    </th>
                                    <th class="shipping"><input id="shipping-zip-code"
                                                                value="{{$client['zipCode']}}"
                                                                class="shipping-input-text shipping-input"
                                                                name="shipping-zip-code" type="text"></th>
                                </tr>
                                <tr>
                                    <th class="padding-1"
                                        style="font-family: 'Roboto'; font-weight: 500">{{__('front.city')}}*
                                    </th>

                                    <th class="shipping">
                                        <input id="city_name"
                                               value="{{$client['city']}}"
                                               class="shipping-input-text shipping-input"
                                               name="city_name" type="text">
                                    </th>


                                </tr>


                                <tr>
                                    <th class="padding-1"
                                        style="font-family: 'Roboto'; font-weight: 500">{{__('front.street_address')}}*
                                    </th>
                                    <th class="shipping"><input id="shipping-street-Address"

                                                                value="{{$client['address']}}"

                                                                class="shipping-input-text shipping-input"
                                                                name="shipping-street-address" type="text">
                                    </th>
                                </tr>
                                <tr>
                                    <th class="padding-1"
                                        style="font-family: 'Roboto'; font-weight: 500">{{__('front.apartment')}}</th>
                                    <th class="shipping"><input id="shipping-additional-address"

                                                                value="{{$client['additionalAddress']}}"

                                                                class="shipping-input-text shipping-input"
                                                                name="shipping-additional-address" type="text">
                                    </th>
                                </tr>
                                <tr>
                                    <th class="padding-1"
                                        style="font-family: 'Roboto'; font-weight: 500">{{__('front.phone_number')}}*
                                    </th>
                                    <th class="shipping"><input id="shipping-phone-number"
                                                                class="shipping-input-text shipping-input"
                                                                value="{{$client['phoneNumber']}}"
                                                                name="shipping-phone-number" type="text">
                                        <span class="help-block" id="help-phone"></span>
                                    </th>
                                </tr>
                                <tr>
                                    <th class="padding-1"
                                        style="font-family: 'Roboto'; font-weight: 500">{{__('front.email')}}*
                                    </th>
                                    <th class="shipping"><input id="shipping-email"
                                                                class="shipping-input-text shipping-input"
                                                                value="{{$client['email']}}"
                                                                name="shipping-email" type="text"></th>
                                </tr>


                            </table>
                        </div>
                    </div>
                </div>


            </div>
            <div class="col-md-12 mt-2 p-0" id="iframe-shipping-box" style="display: none">
                <div class="col-md-6">
                    <iframe id="iframe-shipping" width="100%" height="500px">

                    </iframe>
                </div>
                <div class="col-md-12">
                </div>
                <div class="col-md-6">
                    <button type="button" class="btn-submit-yellow w-100 mt-3 review-btn-cart"
                            onclick="changeAddress()">
                        {{__('front.change_address')}}
                    </button>
                </div>


            </div>
            <div class="col-md-12" id="shipping-form-btns">
                <div class="payment-labels">
                    <label class="inline-flex m-auto">
                        <span class="help-block" id="help-address"></span>
                    </label>
                </div>
                <div class="address-button">
                    <button type="submit" style="margin-bottom: 15px" class="btn-submit-yellow-address"
                            id="btn-proceed-payment">{{__('front.review_order')}}</button>
                </div>
            </div>
        </div>

    </div>

@endsection
@push('js')
    <script type="application/javascript">


        $(document).ready(function () {
            setTimeout(function () {
                $('input[name="shipping_type"]')[0].click()
                $('input[name="shipping_type"]').trigger('change')
            }, 500)

        })
        $(document).ready(function () {
            $('input[name="shipping_type"]').on('change', function () {
                changeIframeFromRadio()
                $('#help-address').text('');

            })
        })

        function validateEmail(email) {
            var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
            return reg.test(email);
        }

        $(function () {

            $("#shipping-phone-number").keypress(function (e) {
                var chr = String.fromCharCode(e.which);
                if ("0123456789".indexOf(chr) < 0) {
                    return false;
                }
                if ($("#shipping-phone-number").val().length > 12) {
                    return false;
                }
            });
            $('#shipping-phone-number').on('paste', function (e) {
                var check = true;
                var content = e.originalEvent.clipboardData.getData('text/plain');
                for (var i = 0; i < content.length; i++) {
                    if ("0123456789".indexOf(content[i]) < 0) {
                        return false
                    }
                }
                if (content.length > 12) {
                    return false

                }
                $('#shipping-phone-number').val(content)
            });
            $('#btn-proceed-payment').on('click', function (e) {
                let error = 0;
                if ($('input[name="shipping_type"]:checked').val() !== 'RELAIS') {
                    email = document.getElementById("shipping-email").value;

if (email === '') {
    document.getElementById("help-address").innerText = '{{__('front.email_address')}}';
    error++;
} else if (!validateEmail(email)) {
    document.getElementById("help-address").innerText = "{!!__('front.email_auth')!!}";
    error++;
} else {
    document.getElementById("help-address").innerText = '';
}
                }
         
                if (error === 0) {

                    if (!$('input[name="shipping_type"]:checked').val()) {
                        $('#help-address').text('{{__('front.all_fields_required')}}');
                        return;
                    }
                    if ($('input[name="shipping_type"]:checked').val() === 'RELAIS') {

                        if ($('#iframe-shipping').contents().find('#parcelShopId')
                            && $('#iframe-shipping').contents().find('#parcelShopId').val()
                            && $('#iframe-shipping').contents().find('#parcelShopId').val() != '') {
                            goToreview()
                        } else {
                            $('#help-address').text('{{__('front.all_fields_required_2')}}');
                        }


                    } else {
                        e.preventDefault();
                        goToreview()
                    }
                }

            });
        });

        function noSemiColon(id) {
            $('#' + id).keypress(function (e) {
                if (e.which != 59) {
                } else {
                    return false;
                }
            });
        }

        $(document).ready(function () {
            noSemiColon('shipping-name')
            noSemiColon('shipping-street-Address')
            noSemiColon('shipping-additional-address')
            noSemiColon('shipping-phone-number')
        })

        function goToreview() {

            if ($('input[name="shipping_type"]:checked').val() === 'RELAIS') {
                $('#help-address').text('');
                let url = window.location.protocol + '//' + window.location.hostname + '/customer-review-checkout/' + '{{$customerReview->_id}}';
                window.location.href = url;
            } else {
                let shippingName = $("#shipping-name").val();
                let shippingFirstName = $("#shipping-first-name").val();
                let shippingLastName = $("#shipping-last-name").val();
                let shippingCountry = $("#shipping-country").val();
                let shippingCityName = $("#city_name").val();
                let shippingStreetAddress = $("#shipping-street-Address").val();
                let shippingAdditionalAddress = $("#shipping-additional-address").val();
                let shippingZipCode = $("#shipping-zip-code").val();
                let shippingPhoneNumber = $("#shipping-phone-number").val();
                let shippingemail = $("#shipping-email").val();
                if (
                    shippingName != '' &&
                    shippingFirstName != '' &&
                    shippingLastName != '' &&
                    shippingCountry != '' &&
                    shippingCityName != '' &&
                    shippingStreetAddress != '' &&
                    shippingZipCode != '' &&
                    shippingPhoneNumber != '' && shippingemail != ''
                ) {

                    $.ajax({
                        type: 'POST',
                        url: '{{route('save-address',['id' => $customerReview->_id])}}',
                        data: {
                            'shipping_first_name': shippingFirstName,
                            'shipping_last_name': shippingLastName,
                            'shipping_country': shippingCountry,
                            'shipping_city': shippingCityName,
                            'shipping_street_address': shippingStreetAddress,
                            'shipping_additional_address': shippingAdditionalAddress,
                            'shipping_zip_code': shippingZipCode,
                            'shipping_phone_number': shippingPhoneNumber,
                            'shipping_email': shippingemail,
                            'shipping_title': shippingName,
                            _token: "{{ csrf_token() }}",
                        },
                    }).done(function (result) {
                        $('#help-address').text('');

                        let url = window.location.protocol + '//' + window.location.hostname + '/customer-review-checkout/' + '{{$customerReview->_id}}';
                        window.location.href = url;
                    });

                } else {
                    $('#help-address').text('{{__('front.fill_all_required')}}');
                }

            }

        }

        function changeIframe() {
            console.log('change iframe')
            $('#shipping-city').empty()
            $('#fixed-address-mode').hide()
            $('#shipping-zip-code').prop('disabled', false)
            $('#shipping-city').prop('disabled', false)
            $('#shipping-street-Address').prop('disabled', false)
            $('#shipping-additional-address').prop('disabled', false)
            $('#shipping-phone-number').prop('disabled', false)
            $('#shipping_name').prop('disabled', false)

            $('#shipping-form-btns').show()
            if ($('#shipping-type').val() == "RELAIS") {
                $('#shipping-zip-code').val('{{session()->get('process.zip_code')}}')
                $('#shipping-street-Address').val("{!! session()->get('process.street_address') !!}")
                $('#shipping-additional-address').val("{!! session()->get('process.additional_address')  !!}")
                $('#shipping-phone-number').val('{{session()->get('process.phone_number')}}')

                @if(session()->get('process.city_name','') != '')
                $('#city_name').val("{!! session()->get('process.city_name') !!}")
                @endif

                setTimeout(function () {
                    var cp = $('#shipping-zip-code').val();
                    var ville = $("#city_name").val();
                    var env = '{{$client['country']}}';
                    if (env === '' || env === null) {
                        env = 'FRA'
                    }

                    let url = 'https://posfra.foreverliving.fr/chronopost/index.php?' +
                        'bckUrl={{route('select.checkpoint', ['id' => $customerReview->_id])}}?' +
                        '&paysLiv=' + env
                        + '&adresse=' + $('#shipping-street-Address').val()
                        + '&cp=' + cp
                        + '&ville=' + ville
                        + '&key=5352876b-9490-42a2-aade-d36c9b6d92ca';
                    $('#iframe-shipping-box').show()
                    $('#shipping-form-box').hide()
                    $('#shipping-form-title').hide()

                    $('#iframe-shipping').attr('src', url);
                }, 500);
            } else {
                $('#shipping-zip-code').val('{{session()->get('process.zip_code')}}')
                $('#shipping-street-Address').val("{!! session()->get('process.street_address') !!}")
                $('#shipping-additional-address').val("{!! session()->get('process.additional_address')  !!}")
                $('#shipping-phone-number').val('{{session()->get('process.phone_number')}}')
                $('#city_name').val("{!! session()->get('process.city_name') !!}")

                $('#iframe-shipping-box').hide();
                $('#shipping-form-box').show();
                $('#shipping-form-title').show()
            }
            // getShipmentCost()
        }

        function changeIframeFromRadio() {

            $('#fixed-address-mode').hide()
            $('#shipping-zip-code').prop('disabled', false)
            $('#shipping-city').prop('disabled', false)
            $('#shipping-street-Address').prop('disabled', false)
            $('#shipping-additional-address').prop('disabled', false)
            $('#shipping-phone-number').prop('disabled', false)
            $('#shipping_name').prop('disabled', false)
            $('#shipping-form-btns').show()

            if ($('input[name="shipping_type"]:checked').val() == "RELAIS") {


                setTimeout(function () {
                    var cp = $('#shipping-zip-code').val();
                    var ville = $("#city_name").val();
                    var env = '{{$client['country']}}';
                    if (env === '' || env === null) {
                        env = 'FRA'
                    }

                    let url = 'https://posfra.foreverliving.fr/chronopost/index.php?' +
                        'bckUrl={{route('select.checkpoint', ['id' => $customerReview->_id])}}?' +
                        '&paysLiv=' + env
                        + '&adresse=' + $('#shipping-street-Address').val()
                        + '&cp=' + cp
                        + '&ville=' + ville
                        + '&key=5352876b-9490-42a2-aade-d36c9b6d92ca';
                    $('#iframe-shipping-box').show()
                    $('#shipping-form-box').hide();
                    $('#shipping-form-title').hide()


                    $('#iframe-shipping').attr('src', url);
                }, 500);
            } else {
                $('#iframe-shipping-box').hide();
                $('#shipping-form-box').show();
                $('#shipping-form-title').show()
            }
        }

        function changeAddress() {
            // if ($('#shipping-type').val() == "RELAIS") {
            if ($('input[name="shipping_type"]:checked').val() == "RELAIS") {
                var cp = $('#shipping-zip-code').val();
                var ville = $("#city_name").val();
                var env = '{{$client['country']}}';
                if (env === '' || env === null) {
                    env = 'FRA'
                }

                let url = 'https://posfra.foreverliving.fr/chronopost/index.php?' +
                    'bckUrl={{route('select.checkpoint', ['id' => $customerReview->_id])}}?' +
                    '&paysLiv=' + env
                    + '&adresse=' + $('#shipping-street-Address').val() +
                    '&cp=' + cp +
                    '&ville=' + ville +
                    '&key=5352876b-9490-42a2-aade-d36c9b6d92ca';
                $('#iframe-shipping-box').show()
                $('#shipping-form-box').hide();
                $('#shipping-form-title').hide()


                $('#iframe-shipping').attr('src', url);
            } else {
                $('#iframe-shipping-box').hide();
                $('#shipping-form-box').show();
                $('#shipping-form-title').show()

            }
        }

        $(document).ready(function () {
            $('iframe#iframe-shipping').on('load', function () {
                {{--if ($('#iframe-shipping').contents().find('.chrono-inputs').length > 0) {--}}

                {{--    var keys = [];--}}
                {{--    var values = [];--}}
                {{--    $('#iframe-shipping').contents().find('.chrono-inputs').each(function (key, value) {--}}
                {{--        keys.push($(value).attr('name'))--}}
                {{--        values.push($(value).val())--}}
                {{--    })--}}
                {{--    $.ajax({--}}
                {{--        type: 'POST',--}}
                {{--        url: '{{route('session-chrono-put')}}',--}}
                {{--        data: {--}}
                {{--            key: keys,--}}
                {{--            value: values,--}}
                {{--            _token: "{{ csrf_token() }}",--}}
                {{--        },--}}
                {{--    }).done(function (result) {--}}
                {{--        $('#help-address').text('');--}}
                {{--    });--}}
                {{--    $('#help-address').text('');--}}

                {{--}--}}
            });
        })

    </script>
@endpush
