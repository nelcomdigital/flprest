@extends('frontend.layouts.app')

@section('content')
    @include('frontend.layouts.partials.header')

    @if (isset($customerReview) && $customerReview != null)
        <div class="cart-container" id="cart-container">
            <div class="cart-text-header"><h1 style="padding: 10px 0;font-size: 2rem">
                </h1></div>
            <div class="row " style="display: none" id="error-message-hash">
                <div class="col-lg-8 col-md-8 mb-5" style="display: grid">
                    <div class="error-message">
                        {{__('front.payment-error-message')}}
                    </div>
                </div>
            </div>
            <div class="d-none d-sm-block">
                <div class="cart-table">
                    <table style="width:100%">
                        <tr class="border-transparent-right">
                            <th class="w-25 text-left border-transparent">{!!__('front.item_label')!!}</th>
                            <th class="w-25 border-transparent">{!!__('front.unit_price')!!}</th>
                            <th class="w-25 border-transparent">{!!__('front.quantity')!!}</th>
                            <th class="w-25 border-transparent" style="display: none">{!!__('front.total')!!}</th>
                        </tr>
                        {{-- @isset($customerReview)
                            @if (isset($customerReview->products) && $customerReview->products != null)
                                @foreach ($customerReview->products as $product)

                                    <tr class="border-transparent-right" id="{!!$product['price']['ref']!!}">
                                        <th class="w-20 border-transparent">
                                            <div class="inline-flex w-100">
                                                <img alt="" class="w-25"
                                                     src="https://distrib.foreverliving.fr/450-450/media/image/produits/produit/{{$product['price']['ref']}}.png">
                                                <div>
                                                    <div class="cart-item-name w-100"
                                                         style="margin: auto 10px!important;">
                                                        {!!html_entity_decode($product['detail']['label']['fr']) !!}</div>
                                                    <div class="item-currency-modal w-100" style="
                                            font-weight: 300!important ;
                                            margin: auto 10px!important;
                                            text-align: left!important;
                                            text-transform: capitalize;">
                                                        Ref: {!!$product['price']['ref']!!}</div>
                                                </div>
                                            </div>
                                        </th>
                                        <th class="w-20 border-transparent">
                                            <div class="inline-flex w-100 m-auto">
                                                <div class="m-auto text-center font-16 inline-flex">
                                                    <div class="unit-price"
                                                         id="unit-price-{{$product['price']['ref']}}">
                                                        {{number_format((float)($product['price']['prixTTC']), 2)}}
                                                    </div>
                                                    <div class="unit-currency"
                                                         id="unit-currency-{{$product['price']['ref']}}">
                                                        €
                                                    </div>

                                                </div>
                                            </div>
                                        </th>
                                        <th class="w-20 border-transparent">
                                            <div class="inline-flex w-100 m-auto">
                                                <input class="input-border-yellow mt-auto text-center mb-auto w-25 p-0"
                                                       type="text"
                                                       style="width: 100% !important;
                                                    margin: 0 50px;
                                                    background-color: transparent!important;
                                                    border: 0!important;"
                                                       id="quantity-{{$product['price']['ref']}}"
                                                       name="quantity[]" readonly
                                                       value="{{$product['qte']}}">
                                            </div>
                                        </th>
                                        <th class="w-20 border-transparent" style="display: none;">
                                            <div class="inline-flex w-100 m-auto">
                                                <div class="m-auto text-center font-16 inline-flex">
                                                    <div class="unit-price"
                                                         id="unit-price-total-{{$product['price']['ref']}}">
                                                        {{number_format((float)($product['qte']  *
                                            $product['price']['prixTTC']), 2)}}

                                                    </div>
                                                    <div class="unit-currency"
                                                         id="unit-currency-total-{{$product['price']['ref']}}">
                                                        €
                                                    </div>

                                                </div>
                                            </div>
                                        </th>
                                    </tr>
                                @endforeach

                            @endif
                        @endisset --}}
         @isset($order)
                            @if (isset($order->panierDetail) && $order->panierDetail != null)
                                @foreach ($order->panierDetail as $product)

                                    <tr class="border-transparent-right" id="{!! $product->ref !!}">
                                        <th class="w-20 border-transparent">
                                            <div class="inline-flex w-100">
                                                @if ($product->typeProduit != 'R')
                                                    <img alt="" class="w-25"
                                                    src="https://distrib.foreverliving.fr/450-450/media/image/produits/produit/{{$product->ref}}.png">
                                                
                                                    @else
                                                    <img alt="" class="w-25"
                                                    src="{{asset('images/remise.svg')}}">
                                                
                                                @endif
                                               <div>
                                                    <div class="cart-item-name w-100"
                                                         style="margin: auto 10px!important;">
                                                        {!!html_entity_decode($product->designation) !!}</div>
                                                    <div class="item-currency-modal w-100" style="
                                            font-weight: 300!important ;
                                            margin: auto 10px!important;
                                            text-align: left!important;
                                            text-transform: capitalize;">
                                                        Ref: {!! $product->ref !!}</div>
                                                </div>
                                            </div>
                                        </th>
                                        <th class="w-20 border-transparent">
                                            <div class="inline-flex w-100 m-auto">
                                                <div class="m-auto text-center font-16 inline-flex">
                                                    <div class="unit-price"
                                                         id="unit-price-{{$product->ref}}">
                                                        {{number_format((float)($product->prixTTC), 2)}}
                                                    </div>
                                                    <div class="unit-currency"
                                                         id="unit-currency-{{$product->ref}}">
                                                        €
                                                    </div>

                                                </div>
                                            </div>
                                        </th>
                                        <th class="w-20 border-transparent">
                                            <div class="inline-flex w-100 m-auto">
                                              

                                                       @if ($product->typeProduit != 'R')
                                                       <input class="input-border-yellow mt-auto text-center mb-auto w-25 p-0"
                                                       type="text"
                                                       style="width: 100% !important;
                                                    margin: 0 50px;
                                                    background-color: transparent!important;
                                                    border: 0!important;"
                                                       id="quantity-{{$product->ref}}"
                                                       name="quantity[]" readonly
                                                       value="{{$product->qte}}">
                                                       @else
                                                       <input class="input-border-yellow mt-auto text-center mb-auto w-25 p-0"
                                                       type="text"
                                                       style="width: 100% !important;
                                                    margin: 0 50px;
                                                    background-color: transparent!important;
                                                    border: 0!important;"
                                                       id="quantity-{{$product->ref}}"
                                                       name="quantity[]" readonly
                                                       value="1">
                                                       @endif
                                            </div>
                                        </th>
                                        <th class="w-20 border-transparent" style="display: none;">
                                            <div class="inline-flex w-100 m-auto">
                                                <div class="m-auto text-center font-16 inline-flex">
                                                    <div class="unit-price"
                                                         id="unit-price-total-{{$product->ref}}">
                                                        {{number_format((float)($product->qte  *
                                           $product->prixTTC), 2)}}

                                                    </div>
                                                    <div class="unit-currency"
                                                         id="unit-currency-total-{{$product->ref}}">
                                                        €
                                                    </div>

                                                </div>
                                            </div>
                                        </th>
                                    </tr>
                                @endforeach

                            @endif
                        @endisset
                    </table>
                </div>

                @if ($redirect != 'shipping')
                <div class="border-transparent-right w-100 p-3 text-right">
                    <div class="row">
                        <div class="col-md-4" style="text-align: left">
                            @isset($customerReview)
                                @if (isset($customerReview->shipping_address) && $customerReview->shipping_address != null)
                                    <div class=border-transparent">
                                        <div class="inline-flex w-100">
                                            <div>
                                                <div class="item-currency-modal w-100" style="
                                            font-weight: 300!important ;
                                            margin: auto 10px!important;
                                            text-align: left!important;
                                                           color: black;
                                            text-transform: capitalize;">
                                                    {!!__('front.shipping_address')!!}:
                                                </div>
                                                @if (isset($customerReview->shipping_address['reference']) and $customerReview->shipping_address['reference'] != '')
                                                    <div class="item-currency-modal w-100" style="
                                            font-weight: 300!important ;
                                            margin: auto 10px!important;
                                            text-align: left!important;
                                                           color: black;
                                            text-transform: capitalize;">
                                                        {!!$customerReview->shipping_address['nameLiv']!!}
                                                    </div>
                                                @else
                                                    <div class="item-currency-modal w-100" style="
                                            font-weight: 300!important ;
                                            margin: auto 10px!important;
                                            text-align: left!important;
                                                           color: black;
                                            text-transform: capitalize;">
                                                        {!!$customerReview->shipping_address['nameLiv']!!}

                                                    </div>
                                                @endif
                                                <div class="item-currency-modal w-100" style="
                                            font-weight: 300!important ;
                                            margin: auto 10px!important;
                                            text-align: left!important;
                                                           color: black;
                                            text-transform: capitalize;">
                                                    <strong>{{__('front.street_address')}}</strong>

                                                    : {!!$customerReview->shipping_address['addressLiv']!!}</div>
                                                @if ($customerReview->shipping_address['additionalAddressLiv'] != '' and  $customerReview->shipping_address['additionalAddressLiv'] != null)
                                                    <div class="item-currency-modal w-100" style="
                                            font-weight: 300!important ;
                                            margin: auto 10px!important;
                                            text-align: left!important;
                                            color: black;
                                            text-transform: capitalize;">
                                                        <strong>{{__('front.additional_address')}}</strong>

                                                        : {!! $customerReview->shipping_address['additionalAddressLiv'] !!}
                                                    </div>
                                                @endif
                                                <div class="item-currency-modal w-100" style="
                                            font-weight: 300!important ;
                                            margin: auto 10px!important;
                                            text-align: left!important;
                                            color: black;
                                            text-transform: capitalize;">
                                                    <strong>{{__('front.zipCodeLiv')}} </strong>
                                                    : {{$customerReview->shipping_address['zipCodeLiv']}}</div>
                                                @if (isset($customerReview->shipping_address['cityLiv']) and $customerReview->shipping_address['cityLiv'] != null)
                                                    <div class="item-currency-modal w-100" style="
                                            font-weight: 300!important ;
                                            margin: auto 10px!important;
                                            text-align: left!important;
                                            color: black;
                                            text-transform: capitalize;">
                                                        <strong> {{__('front.city')}}</strong>
                                                        : {{$customerReview->shipping_address['cityLiv']}}</div>
                                                @endif
                                                @if (isset($customerReview->shipping_address['reference']) and $customerReview->shipping_address['reference'] != null)
                                                    <div class="item-currency-modal w-100" style="    font-weight: 300!important;
    margin: auto 10px!important;
    text-align: left!important;
    color: black;
    text-transform: capitalize;">
                                                        {!!$customerReview->shipping_address['nameLiv']!!}
                                                    </div>
                                                @endif
                                                @if ($customerReview->shipping_address['phoneLiv'] != '')
                                                    <div class="item-currency-modal w-100" style="
                                                   font-weight: 300!important;
                                                   margin: auto 10px!important;
                                                   text-align: left!important;
                                                   color: black;
                                                   text-transform: capitalize;">
                                                        {!!$customerReview->shipping_address['phoneLiv']!!}</div>
                                                @endif
                                                @if ($customerReview->shipping_address['emailLiv'] != '')
                                                    <div class="item-currency-modal w-100" style="
                                              font-weight: 300!important;
    margin: auto 10px!important;
    text-align: left!important;
    color: black;
    text-transform: lowercase;">
                                                        {!!$customerReview->shipping_address['emailLiv']!!}</div>
                                                @endif

                                            </div>


                                        </div>
                                    </div>
                                @endif
                            @endisset
                        </div>
                        <div class="col-md-3"></div>
                        @if ($redirect != 'shipping')

                            <div class="col-md-5 " style="display: grid">
                                <table style="border: 0">
                                   
                                    <tr>
                                        <td>
                                            <div class="review-total-text">
                                                {{__('front.total')}}
                                            </div>
                                        </td>
                                        <td>
                                            <div class="inline-flex review-cart-total">
                                                <div class="unit-price" id="unit-price-grand-total">
                                                    @isset($customerReview->client_total)
                                                      
                                                            {{number_format((float)($customerReview->client_total), 2)}}

                                                    @else
                                                        0
                                                    @endisset
                                                </div>
                                                <div class="unit-currency" id="unit-currency-grand-total">
                                                    €
                                                </div>

                                            </div>
                                        </td>
                                    </tr>
                                </table>

                            </div>
                        @endif
                    </div>
                </div>
                @endif

            </div>


            {{--Mobile--}}
            <div class="d-block d-sm-none">
                @isset($customerReview)
                    @if (isset($customerReview->products) && $customerReview->products != null)
                        @foreach ($customerReview->products as $product)
                            <div class="inline-flex w-100" id="m{{$product['price']['ref']}}"
                                 style="padding: 0 0 20px 0;!important;">
                                <div class="cart-img">
                                    <img
                                        src="https://distrib.foreverliving.fr/450-450/media/image/produits/produit/{{$product['price']['ref']}}.png">
                                </div>
                                <div class="cart-details">
                                    <div class="cart-details-info">
                                        <div
                                            class="mobile-cart-product-name">{!!html_entity_decode($product['detail']['label']['fr']) !!}</div>
                                        <div class="mobile-cart-reference">item# {{$product['price']['ref']}}</div>
                                        <div class="inline-flex w-100">
                                            <div class="unit-price"
                                                 id="mobile-unit-pricxxx">{!!__('front.quantity')!!}
                                                : {{$product['qte']}}
                                            </div>

                                        </div>
                                        <div class="inline-flex">
                                            <div class="unit-price"
                                                 id="mobile-unit-price-{{$product['price']['ref']}}">{{$product['price']['prixTTC']}}</div>
                                            <div class="unit-currency"
                                                 id="mobile-unit-currency-{{$product['price']['ref']}}"> €
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        @endforeach
                    @endif
                @endisset
                @if ($redirect == 'shipping')
                    @isset($customerReview)
                        @if (isset($customerReview->shipping_address) && $customerReview->shipping_address != null)
                            <div class=border-transparent" style="margin: 20px 0">
                                <div class="inline-flex w-100">
                                    <div>
                                        <div class="item-currency-modal w-100" style="
                                            font-weight: 300!important ;
                                            margin: auto 10px!important;
                                            text-align: left!important;
                                                           color: black;
                                            text-transform: capitalize;">
                                            {!!__('front.shipping_address')!!}:
                                        </div>
                                        @if (isset($customerReview->shipping_address['reference']) and $customerReview->shipping_address['reference'] != '' and $customerReview->shipping_address['reference'] != null)
                                            <div class="item-currency-modal w-100" style="
                                            font-weight: 300!important ;
                                            margin: auto 10px!important;
                                            text-align: left!important;
                                                           color: black;
                                            text-transform: capitalize;">
                                                {!!$customerReview->shipping_address['nameLiv']!!}
                                            </div>
                                        @else
                                            <div class="item-currency-modal w-100" style="
                                            font-weight: 300!important ;
                                            margin: auto 10px!important;
                                            text-align: left!important;
                                                           color: black;
                                            text-transform: capitalize;">
                                                {!!$customerReview->shipping_address['nameLiv']!!}

                                            </div>
                                        @endif
                                        <div class="item-currency-modal w-100" style="
                                            font-weight: 300!important ;
                                            margin: auto 10px!important;
                                            text-align: left!important;
                                                           color: black;
                                            text-transform: capitalize;">
                                            <strong>{{__('front.street_address')}}</strong>

                                            : {!!$customerReview->shipping_address['addressLiv']!!}</div>
                                        @if ($customerReview->shipping_address['additionalAddressLiv'] != '' and $customerReview->shipping_address['additionalAddressLiv'] != null)
                                            <div class="item-currency-modal w-100" style="
                                            font-weight: 300!important ;
                                            margin: auto 10px!important;
                                            text-align: left!important;
                                            color: black;
                                            text-transform: capitalize;">
                                                <strong>{{__('front.additional_address')}}</strong>

                                                : {!! $customerReview->shipping_address['additionalAddressLiv'] !!}
                                            </div>
                                        @endif
                                        <div class="item-currency-modal w-100" style="
                                            font-weight: 300!important ;
                                            margin: auto 10px!important;
                                            text-align: left!important;
                                            color: black;
                                            text-transform: capitalize;">
                                            <strong>{{__('front.zipCodeLiv')}} </strong>
                                            : {{$customerReview->shipping_address['zipCodeLiv']}}</div>
                                        @if (isset($customerReview->shipping_address['cityLiv']) and $customerReview->shipping_address['cityLiv'] != null)
                                            <div class="item-currency-modal w-100" style="
                                            font-weight: 300!important ;
                                            margin: auto 10px!important;
                                            text-align: left!important;
                                            color: black;
                                            text-transform: capitalize;">
                                                <strong> {{__('front.city')}}</strong>
                                                : {{$customerReview->shipping_address['cityLiv']}}</div>
                                        @endif
                                        @if (isset($customerReview->shipping_address['reference']) and $customerReview->shipping_address['reference'] != null)
                                            <div class="item-currency-modal w-100" style="    font-weight: 300!important;
    margin: auto 10px!important;
    text-align: left!important;
    color: black;
    text-transform: capitalize;">
                                                {!!$customerReview->shipping_address['nameLiv']!!}
                                            </div>
                                        @endif
                                        @if ($customerReview->shipping_address['phoneLiv'] != '')
                                            <div class="item-currency-modal w-100" style="
                                                   font-weight: 300!important;
                                                   margin: auto 10px!important;
                                                   text-align: left!important;
                                                   color: black;
                                                   text-transform: capitalize;">
                                                {!!$customerReview->shipping_address['phoneLiv']!!}</div>
                                        @endif
                                        @if ($customerReview->shipping_address['emailLiv'] != '')
                                            <div class="item-currency-modal w-100" style="
                                              font-weight: 300!important;
    margin: auto 10px!important;
    text-align: left!important;
    color: black;
    text-transform: lowercase;">
                                                {!!$customerReview->shipping_address['emailLiv']!!}</div>
                                        @endif

                                    </div>


                                </div>
                            </div>
                        @endif
                    @endisset
                    @if ( session()->get('process.shipping_reference' , '') != '')
                        <div class="inline-flex w-100" style="padding: 0 0 20px 0;!important;">
                            <div class="cart-img">

                            </div>
                            <div class="cart-details">
                                <div class="cart-details-info">
                                    <div
                                        class="mobile-cart-product-name">{!!session()->get('process.nameLiv' , '')!!}</div>
                                    <div class="mobile-cart-reference">
                                        item# {{session()->get('process.shipping_reference')}}</div>

                                    <div class="inline-flex" style="visibility: hidden">
                                        <div class="unit-price">{{session()->get('process.amount')}}</div>
                                        <div class="unit-currency"> €</div>
                                    </div>
                                </div>
                                <div class="mobile-cart-price inline-flex" style="display: none;">
                                    <div class="unit-price">{{session()->get('process.amount')}}</div>
                                    <div class="unit-currency">€</div>

                                </div>
                            </div>
                        </div>

                    @endif
                @endif


                <div class="border-transparent-right w-100 p-2 text-center" style="background-color: black">
                    <div class="row">
                        <div class="col-md-5" style="display: grid">
                            <table style="border: 0">
                                <tr>
                                    <td>
                                        <div class="review-total-text">
                                            {{__('front.total')}}
                                        </div>
                                    </td>
                                    <td class="text-right">
                                        <div class="inline-flex cart-total">
                                            <div class="unit-price" id="unit-price-grand-total">
                                                @isset($customerReview->client_total)
                                                    {{number_format((float)($customerReview->client_total), 2)}}

                                                @else
                                                    0
                                                @endisset
                                            </div>
                                            <div class="unit-currency" id="unit-currency-grand-total">
                                                €
                                            </div>


                                        </div>
                                    </td>
                                </tr>
                            </table>

                        </div>
                    </div>
                </div>
            </div>

            <div class="d-none d-sm-block">
                <div class="btn-group-payment">

                    <div class="text-right">
                        @if ($redirect == 'shipping')
                            <a href="{{route('customer-review-shipping',['id'=>$customerReview->_id])}}"
                               class="btn-submit-yellow review-btn-cart" id="btn-cart">
                                {{__('front.proceed_to_shipping')}}
                            </a>
                        @else
                            <a href="{{route('payment',['id'=>$customerReview->_id])}}"
                               class="btn-submit-yellow review-btn-cart" id="btn-cart">
                                {{__('front.proceed_to_payment')}}
                            </a>
                        @endif

                    </div>
                </div>
            </div>
            <div class="d-block d-sm-none" style="min-height: inherit">
                <div class="btn-group-payment">
                    <div class="text-center">
                        <div class="col-md-12 p-0">
                        </div>
                        @if ($redirect == 'shipping')
                            <a href="{{route('customer-review-shipping',['id'=>$customerReview->_id])}}"
                               class="btn-submit-yellow review-btn-cart" id="btn-cart">
                                {{__('front.proceed_to_shipping')}}
                            </a>
                        @else
                            <a href="{{route('payment',['id'=>$customerReview->_id])}}"
                               class="btn-submit-yellow review-btn-cart" id="btn-cart">
                                {{__('front.proceed_to_payment')}}
                            </a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="cart-container" id="cart-container">


            <div class="cart-text-header">
                <h1 style="padding: 10px 0;font-size: 2rem">
                    {!! __('front.link-expired')  !!}

                </h1>
            </div>
        </div>
    @endif
    @include('frontend.layouts.partials.footer')

@endsection

@push('js')
    <script>
        $(document).ready(function () {
            if (window.location.hash.indexOf("error") > 0) {
                $('#error-message-hash').show()
            }
        })
    </script>

@endpush
