@extends('frontend.layouts.app')

@section('content')
    <!-- Modal content-->
    <div class="content-modal">
        <div class="modal-img">
            <div class="row bg-white m-0">
                <div class="d-none d-sm-block w-100">
                    <div class="w-100">
                        <div class="product-header">
                            <a href="{{route('products',$category->id)}}">{{html_entity_decode($category->label->{session()->get('current_locale')}) }}</a>
                            / {{html_entity_decode($product->detail->label->{session()->get('current_locale')}) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5">
                            <div class="image-padding-15">
                                <img class="product-image-detail"
                                     src="https://distrib.foreverliving.fr/450-450/media/image/produits/produit/{{$product->price->ref}}.png"
                                     alt="">
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="product-details">
                                <div class="item-name-modal">
                                    <b>{{ html_entity_decode($product->detail->label->{session()->get('current_locale')}) }}</b>
                                </div>
                                <div class="item-currency-modal">Ref: {{$product->price->ref}}</div>
                                <div class="item-details-modal">
                                    {!!  $product->detail->description->{session()->get('current_locale')} !!}
                                </div>
                                <div class="item-currency-modal-price">
                                    <b> {{number_format((float)($product->price->prixTTC), 2)}} €
                                        / {{number_format((float)($product->price->CC),3)}} CC </b></div>
                                <div class="add-to-cart-modal">
                                    {{--                                    @if ( $product->is_pack == 1)--}}
                                    {{--                                        <button class="btn-shop outline-0" id="bag" onclick="addItem2('{{$product->_id}}')">--}}
                                    {{--                                            <img aria-hidden="true" src="{{asset('images/Cart.svg')}}"--}}
                                    {{--                                                 alt="">--}}
                                    {{--                                            <span style="font-family: 'Roboto'; font-weight: 500"--}}
                                    {{--                                                  id="i-{{$product->_id}}">{{$product->getCurrentQuantity()}}</span>--}}
                                    {{--                                        </button>--}}
                                    {{--                                    @else--}}
                                    <button class="btn-shop outline-0" id="bag"
                                            onclick="addItem('{{$product->price->ref}}')"
                                            style="width: 100%;">
                                        <img aria-hidden="true" src="{{asset('images/Cart.svg')}}"
                                             alt="">
                                        <span style="font-family: 'Roboto'; font-weight: 500"
                                              id="i-{{$product->price->ref}}">{{getCurrentQuantity($product->price->ref)}}</span>
                                    </button>
                                    <button class="btn-submit-green"
                                            style="width: 100%;padding: 7px 0; margin: 3px 2px;"
                                            onclick="goBack()">
                                        {{__('front.return_to_list')}}
                                    </button>
                                    <button class="btn-submit-green"
                                            style="width: 100%;padding: 7px 0; margin: 3px 2px;"
                                            onclick="goCart()">
                                        {{__('front.proceed_to_cart')}}
                                    </button>
                                    {{--                                    @endif--}}
                                    <span class="out-stock-{{$product->price->ref}}"
                                          style="display: none">{{__('front.out_of_stock')}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1">
{{--                            <div class="share-product text-right cursor">--}}
{{--                                <a href="#">--}}
{{--                                    <span class="fa fa-share-alt fa-lg"></span>--}}
{{--                                </a>--}}
{{--                            </div>--}}
                        </div>
                    </div>
                </div>
                {{--phone view--}}
                <div class="d-block d-sm-none w-100">
                    <div class="w-100 inline-flex" style="margin-top:20px;">
                        <div class="product-header w-100 m-auto">
                            <a href="{{route('products',$category->id)}}">{{html_entity_decode($category->label->{session()->get('current_locale')}) }}</a>
                            / {{html_entity_decode($product->detail->label->{session()->get('current_locale')}) }}
                        </div>
                        {{--                        <div class="share-product text-right cursor">--}}
                        {{--                            <a href="#">--}}
                        {{--                                <span class="fa fa-share-alt fa-lg"></span>--}}
                        {{--                            </a>--}}
                        {{--                        </div>--}}
                    </div>
                    <div class="col-md-6 p-0">
                        <div class="image-padding-15">
                            <img class="product-image-detail"
                                 src="https://distrib.foreverliving.fr/450-450/media/image/produits/produit/{{$product->price->ref}}.png"
                                 alt="">
                        </div>
                    </div>
                    <div class="col-md-6 p-0">
                        <div class="product-details">
                            <div class="item-name-modal">
                                <b> {!! html_entity_decode($product->detail->label->{session()->get('current_locale')}) !!}</b>
                            </div>
                            <div class="item-currency-modal">Ref: {{$product->price->ref}}</div>
                            <div class="item-details-modal">
                                {!!  $product->detail->description->{session()->get('current_locale')}!!}
                            </div>
                            <div class="item-currency-modal-price">
                                <b> {{number_format((float)($product->price->prixTTC), 2)}} € / {{number_format((float)($product->price->CC),3)}}
                                    CC </b></div>
                            <div class="add-to-cart-modal">
                                {{--                                @if ( $product->is_pack == 1)--}}
                                {{--                                    <button class="btn-shop outline-0" id="bag" onclick="addItem2('{{$product->_id}}')">--}}
                                {{--                                        <img aria-hidden="true" src="{{asset('images/Cart.svg')}}" alt="">--}}
                                {{--                                        <span style="font-family: 'Roboto'; font-weight: 500" id="i-{{$product->_id}}">{{$product->getCurrentQuantity()}}</span>--}}
                                {{--                                    </button>--}}
                                {{--                                @else--}}
                                <button class="btn-shop outline-0" id="bag"
                                        onclick="addItem('{{$product->price->ref}}')"
                                        style="width: 100%;">
                                    <img aria-hidden="true" src="{{asset('images/Cart.svg')}}" alt="">
                                    <span style="font-family: 'Roboto'; font-weight: 500"
                                          id="i-{{$product->price->ref}}">{{getCurrentQuantity($product->price->ref)}}</span>
                                </button>
                                <button class="btn-submit-green"
                                        style="width: 100%;padding: 7px 0; margin: 5px 2px;"
                                        onclick="goBack()">
                                    {{__('front.return_to_list')}}
                                </button>
                                <button class="btn-submit-green"
                                        style="width: 100%;padding: 7px 0; margin: 5px 2px;"
                                        onclick="goCart()">
                                    {{__('front.proceed_to_cart')}}
                                </button>
                                {{--                                @endif--}}
                                <span class="out-stock-{{$product->price->ref}}"
                                      style="display: none">{{__('front.out_of_stock')}}</span>
                            </div>
                        </div>
                    </div>
                </div>
                {{--                <div class="col-md-1">--}}
                {{--                        <span class="fa fa-2x fa-arrow-circle-o-left cursor" data-dismiss="modal"--}}
                {{--                              style="padding: 17px;" id="back-to-products"></span>--}}
                {{--                </div>--}}
            </div>
        </div>

        <div class="product-body">
            <div class="business-profile-box top-5">
                <div class="business-profile-tabs">

                    <div class="business-profile-tab business-profile-tab-{{$product->price->ref}} active"
                         id="{{$product->price->ref}}"
                         target="description-{{$product->price->ref}}">{{__('front.description')}}
                    </div>
                    <div class="business-profile-tab business-profile-tab-{{$product->price->ref}}"
                         id="{{$product->price->ref}}"
                         target="advantages-{{$product->price->ref}}">{{__('front.advantages')}}
                    </div>
                    <div class="business-profile-tab business-profile-tab-{{$product->price->ref}}"
                         id="{{$product->price->ref}}"
                         target="ingredients-{{$product->price->ref}}">{{__('front.ingredients')}}
                    </div>
                    <div class="business-profile-tab business-profile-tab-{{$product->price->ref}}"
                         id="{{$product->price->ref}}"
                         target="packs-presentation-{{$product->price->ref}}">{{__('front.instructions')}}
                    </div>
                </div>
            </div>
        </div>

        <div class="business-profile-tab-content business-profile-tab-content-{{$product->price->ref}}"
             id="packs-presentation-{{$product->price->ref}}" style="">
{{--            {!!  $product->detail->conseils->{session()->get('current_locale')}!!}--}}
            {!!html_entity_decode( $product->detail->conseils->{session()->get('current_locale')})!!}

        </div>

        <div class="business-profile-tab-content business-profile-tab-content-{{$product->price->ref}} active"
             id="description-{{$product->price->ref}}" style="">
            <p style="text-align: center;">
{{--                {!!  $product->detail->description->{session()->get('current_locale')}!!}--}}
                {!!html_entity_decode($product->detail->description->{session()->get('current_locale')})!!}

            </p>
        </div>

        <div class="business-profile-tab-content business-profile-tab-content-{{$product->price->ref}}"
             id="advantages-{{$product->price->ref}}" style="">
            <p style="text-align: center;">
{{--                {!! strip_tags($product->detail->bénéfices->{session()->get('current_locale')}) !!}--}}
{{--                {!!  $product->detail->bénéfices->{session()->get('current_locale')}  !!}--}}
                {!!html_entity_decode($product->detail->bénéfices->{session()->get('current_locale')})!!}
            </p>
        </div>

        <div class="business-profile-tab-content span-unset business-profile-tab-content-{{$product->price->ref}}"
             id="ingredients-{{$product->price->ref}}" style="">
            <p style="text-align: center;">
{{--                {!! $product->detail->ingrédients->{session()->get('current_locale')}!!}--}}
                {!!html_entity_decode($product->detail->ingrédients->{session()->get('current_locale')})!!}

            </p>
        </div>

    </div>
@endsection
@push('js')
    <script>
        $('.business-profile-tab').on('click', function () {
            let id = $(this).attr('id');
            let target = $(this).attr('target');
            location.href = "#" + target;
            profileTab(target, id);
        });

        function profileTab(ref, id) {

            let tab = $('.business-profile-tab[target="' + ref + '"]');
            let target = $(tab).attr('target');

            $('.business-profile-tab-' + id).each(function (key, value) {
                $(value).removeClass('active');
            });
            $('.business-profile-tab-content-' + id).each(function (key, value) {
                $(value).removeClass('active');
            });

            $(tab).addClass('active');
            $('.business-profile-tab-content#' + target).addClass('active');
        }

        function goBack() {
            let url = window.location.protocol + '//' + window.location.hostname + '/product-list';
            window.location.href = url;
        }

        function goCart() {
            let url = window.location.protocol + '//' + window.location.hostname + '/cart';
            window.location.href = url;
        }

        function addItem(id) {

            var obj = new Object();
            obj.action = "verifStockRef";
            obj.env = "{{session()->get('process.country_detail.products_code')}}";
            obj.region = "{{session()->get('process.country_detail.region_code')}}";
            obj.typliv = "{{session()->get('process.country_detail.product_type')}}";
            obj.ref = id;
            let tempi = parseInt($('#i-' + id).html());
            if (isNaN(tempi)) {
                tempi = 0;
            }
            tempi++;
            obj.qte = tempi;

            var json = JSON.stringify(obj);

            $.ajax({
                url: 'https://webservice.foreverliving.fr/wsinscription.php',
                data: json,
                dataType: 'json',
                type: "POST",
                contentType: "application/json",

            }).done(function (result) {
                if (result.success) {
                    $('#out-stock-' + id).hide()
                    putSession(['item'], [id]);
                    $('#product-alert').hide();
                    if ($('#i-' + id).html() == ' ' || $('#i-' + id).html() == '') {
                        $('#i-' + id).html(1);
                    } else {

                        let iNum = parseInt($('#i-' + id).html());
                        $('#i-' + id).html(++iNum);
                    }
                    let iNum = 0;
                    let temp = parseInt($('.total-header-cart').html());
                    if (!isNaN(temp)) {
                        iNum = temp;
                    }
                    $('.total-header-cart').html(++iNum);
                } else {
                    $('.out-stock-' + id).show()
                }


            }).fail(function (data) {
            });


        }


        $("#shopping-cart").on('click', function () {
            location.href = '{{route('cart')}}';
        });

    </script>
@endpush
