@php($id = uniqid('form'))
<div class="right_col full" role="main">
    <div class="">
        <div class="page-title">
            <div>
                <h3>{{__('cms.Update')}}: {{$status->name}}</h3>
            </div>
        </div>

        <form class="form-horizontal form-label-left" id="{{$id}}" novalidate >
            @csrf
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel closed">
                        <div class="x_title">
                            <h2>{{__('cms.status_properties')}}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'name', 'id'=>'name', 'placeHolder'=>'Status', 'label'=>'Status','error'=>'This field is required', 'required'=>true, 'value'=>$status->name])

                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            @include('cms.layouts.blocks.input.buttons', ['formId'=>$id, 'submit'=>''.route('status.update', $status->id), 'type'=>'PUT'])
        </form>
    </div>
</div>
