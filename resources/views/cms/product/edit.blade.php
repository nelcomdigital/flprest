@php($id = uniqid('form'))
<div class="right_col full" role="main">
    <div class="">
        <div class="page-title">
            <div>
                <h3>{{__('cms.edit_product')}}</h3>
            </div>
        </div>

        <form class="form-horizontal form-label-left" id="{{$id}}" novalidate>
            @csrf
            @foreach( \App\Models\Locale::all() as $locale)

                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>{{__('cms.product_properties')}} - {{$locale->name}} </h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'name['.$locale->code.']', 'id'=>'name', 'placeHolder'=>'Name', 'label'=>'Name','error'=>'This field is required', 'required'=>true, 'value'=>$product->name[$locale->code]])

                                {{--                                @include('cms.layouts.blocks.input.ckeditor',['name'=>'short_description['.$locale->code.']', 'id'=>$id.'_short_description', 'label'=>'Short Description', 'required'=>false,  'placeHolder'=>'Short Description','error'=>'This field is required', 'value'=>$product->short_description[$locale->code]])--}}

                                @include('cms.layouts.blocks.input.ckeditor',['name'=>'description['.$locale->code.']', 'id'=>$id.'_description', 'label'=>'Description', 'required'=>false,  'placeHolder'=>'Description','error'=>'This field is required', 'value'=>$product->description[$locale->code]])

                                {{--                                @include('cms.layouts.blocks.input.ckeditor',['name'=>'advantages['.$locale->code.']', 'id'=>$id.'_advantages', 'label'=>'Advantages', 'required'=>false,  'placeHolder'=>'advantages','error'=>'This field is required', 'value'=>$product->advantages[$locale->code]])--}}

                                @include('cms.layouts.blocks.input.ckeditor',['name'=>'instructions['.$locale->code.']', 'id'=>$id.'_instructions', 'label'=>'Instructions', 'required'=>false,  'placeHolder'=>'instructions','error'=>'This field is required', 'value'=>$product->instructions[$locale->code]])

                                @include('cms.layouts.blocks.input.ckeditor',['name'=>'ingredients['.$locale->code.']', 'id'=>$id.'_ingredients', 'label'=>'Ingredients', 'required'=>false,  'placeHolder'=>'Ingredients','error'=>'This field is required', 'value'=>$product->ingredients[$locale->code]])

                                @if (isset($product->links[$locale->code]) and $product->links[$locale->code] != null)
                                    @include('cms.layouts.blocks.input.dynamic-inputs', [
                                            'add'=>'Youtube Link',
                                            'label'=>'Youtube Links',
                                            'inputs'=>[
                                               [
                                                    'name'=>'values['.$locale->code.']',
                                                    'type'=>'text',
                                                    'placeHolder' => 'video id',
                                                    'required' => false,
                                                    'key'=>'value'
                                               ]
                                            ],
                                            'values'=>[0=>$product->links[$locale->code]]
                                ])
                                @else
                                    @include('cms.layouts.blocks.input.dynamic-inputs', [
                                            'add'=>'Youtube Link',
                                            'label'=>'Youtube Links',
                                            'inputs'=>[
                                               [
                                                    'name'=>'values['.$locale->code.']',
                                                    'type'=>'text',
                                                    'placeHolder' => 'video id',
                                                    'key'=>'value'
                                               ]
                                            ]
                                ])
                                @endif


                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            @endforeach

            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>{{__('cms.product_utils')}}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            @include('cms.layouts.blocks.input.image',['name'=>'image', 'id'=>'image', 'label'=>'Main Photo', 'required'=>false,'value'=>$product->image, 'route'=>''.route('product.delete-image', $product->id)])

                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'reference', 'id'=>'reference', 'placeHolder'=>'Reference', 'label'=>'Reference','error'=>'This field is required', 'required'=>true, 'value'=>$product->reference])

                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'quantity', 'id'=>'quantity', 'placeHolder'=>'Quantity', 'label'=>'Quantity','error'=>'This field is required', 'required'=>true, 'value'=>$product->quantity])

                            {{--                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'price_label', 'id'=>'price_label', 'placeHolder'=>'Price Label', 'label'=>'Label','error'=>'This field is required', 'required'=>true, 'value'=>$product->price_label])--}}

                            @if ( $product->productCategory != null)
                                @include('cms.layouts.blocks.input.select',['name'=>'category_id', 'id'=>'category_id',
                                'label'=>'Parent', 'required'=>false, 'options'=>$productCategories, 'error'=>'This field is required',
                                'selected'=>$product->productCategory , 'multiple'=>false, 'value'=>$product->productCategory->name])
                            @else
                                @include('cms.layouts.blocks.input.select',['name'=>'category_id', 'id'=>'category_id', 'label'=>'Parent', 'required'=>false, 'options'=>$productCategories, 'error'=>'This field is required' , 'multiple'=>false, 'selected'=>$product->category])
                            @endif
                            @include('cms.layouts.blocks.input.flag',['fields'=>[
                        ['name'=> 'is_pack','label'=> 'Is Pack?'],
                        ], 'value'=>$product])
                            @include('cms.layouts.blocks.input.flag',['fields'=>[
                            ['name'=> 'is_active','label'=> 'Active?'],
                            ], 'value'=>$product])
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>{{__('cms.product_price_management')}} </h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            @include('cms.layouts.blocks.input.select',['name'=>'currency_id', 'id'=>'currency_id', 'label'=>'Parent', 'required'=>false, 'options'=>$currencies, 'error'=>'This field is required', 'selected'=>$product->currency, 'multiple'=>false, 'value'=>$product->currency->name])
                            @include('cms.layouts.blocks.input.dynamic-inputs', ['add'=>'add price', 'label'=>'Price List', 'inputs'=>[
                                                            ['name'=>'roles', 'type'=>'select', 'options'=>$roles],
                                                            ['name'=>'prices', 'type'=>'text', 'placeHolder' => 'price'],
                                                        ], 'values'=>$product->price_list])

                            {{--                            @include('cms.layouts.blocks.input.select',['name'=>'levels_allowed', 'id'=>'levels_allowed', 'label'=>'Levels Allowed', 'required'=>false, 'options'=>$roles, 'error'=>'This field is required' , 'multiple'=>true--}}
                            {{--                            //, 'selected'=>$product->role, 'value'=>$product->role->name--}}
                            {{--                            ])--}}

                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            @include('cms.layouts.blocks.input.buttons', ['formId'=>$id, 'submit'=>''.route('product.update', $product->id), 'type'=>'PUT'])
        </form>
    </div>
</div>
