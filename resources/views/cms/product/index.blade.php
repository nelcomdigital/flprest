
<div class="form-group">
    <div class="col-md-12">

        <button  type="submit" class="btn btn-success btn-save" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Submitting" data-url="{{route('products.inject')}}" data-type="GET" > {{__('cms.products_inject')}} </button>
    </div>
</div>


@include('cms.layouts.blocks.table.ajax-tables', ['route'=> route('product.index'), 'model'=>$model])
