<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<body>

<form action="{{route('cms.upload.venues.post')}}" method="post" enctype="multipart/form-data">
    @csrf

    <input type="file" name="csv" placeholder="CSV" >
    <input type="submit" name="Submit" >
</form>

</body>

</html>
