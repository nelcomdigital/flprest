<!-- top navigation -->
<div class="top_nav">
    <div class="nav_menu fixed">
        <nav>
            <ul class="nav navbar-nav navbar-right">
                <li class="">
                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        {{Auth::user()->name ?:'' }}
                        <i class=" fa fa-angle-down"></i>
                    </a>


                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                        <li><a href="javascript:;"> Profile</a></li>
                        <li><a href="{{route('cms.logout')}}"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                    </ul>
                </li>




            </ul>

        </nav>
    </div>
</div>
<!-- /top navigation -->
