@php($id = uniqid('form'))
<div class="right_col full" role="main">
    <div class="">
        <div class="page-title">
            <div>
                <h3>{{__('cms.general-configurations')}}</h3>
            </div>
        </div>

        <form class="form-horizontal form-label-left" id="{{$id}}" novalidate >
            @csrf
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>{{__('cms.general-configurations')}}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            {{-- <span class="section">Article Properties</span> --}}
                            @php($counter = 0 )
                            @foreach( $configs as $config)
                                @if ( $config->key =='logo' )
                                    @php($counter ++ )
                                    @include('cms.layouts.blocks.input.image',['name'=>'logo', 'id'=>'logo', 'label'=>'Logo', 'required'=>false,'value'=>$config->value, 'route'=>''.route('cms.general.configuration.delete.image', ['id'=>$config->id,'type'=>'logo'])])
                                    @break
                                @endif
                            @endforeach

                            @if ($counter == 0)
                                @include('cms.layouts.blocks.input.image',['name'=>'logo', 'id'=>'logo', 'label'=>'Logo', 'required'=>false])
                            @endif


                            @php($counter = 0 )
                            @foreach( $configs as $config)
                                @if ( $config->key =='favicon' )
                                    @php($counter ++ )
                                    @include('cms.layouts.blocks.input.image',['name'=>'favicon', 'id'=>'favicon', 'label'=>'Favicon', 'required'=>false, 'value'=>$config->value, 'route'=>''.route('cms.general.configuration.delete.image', ['id'=>$config->id,'type'=>'favicon'])])
                                    @break
                                @endif
                            @endforeach

                            @if ($counter == 0)
                                @php($counter ++ )
                                @include('cms.layouts.blocks.input.image',['name'=>'favicon', 'id'=>'favicon', 'label'=>'Favicon', 'required'=>false])
                            @endif

                            @php($counter = 0 )
                            @foreach( $configs as $config)
                                @if ( $config->key == 'background' )
                                    @php($counter ++ )
                                    @include('cms.layouts.blocks.input.image',['name'=>'background', 'id'=>'background', 'label'=>'Background', 'required'=>false,'value'=>$config->value , 'route'=>''.route('cms.general.configuration.delete.image', ['id'=>$config->id,'type'=>'background'])])
                                    @break
                                @endif
                            @endforeach

                            @if ($counter == 0)
                                @include('cms.layouts.blocks.input.image',['name'=>'background', 'id'=>'background', 'label'=>'Background', 'required'=>false])
                            @endif

                            @php($counter = 0 )
                            @foreach( $configs as $config)
                                @if ( $config->key == 'title' )
                                    @php($counter ++ )
                                    @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'title', 'id'=>'title', 'placeHolder'=>'CMS Title', 'label'=>'CMS Title','error'=>'This field is required', 'required'=>false, 'value'=>$config->value])
                                    @break
                                @endif
                            @endforeach

                            @if ($counter == 0)
                                @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'title', 'id'=>'title', 'placeHolder'=>'CMS Title', 'label'=>'CMS Title','error'=>'This field is required', 'required'=>false])
                            @endif

                            @php($counter = 0 )
                            @foreach( $configs as $config)
                                @if ( $config->key == 'maitainance' )
                                    @php($counter ++ )
                                        @include('cms.layouts.blocks.input.config-flag',['fields'=>[
                                            ['name'=> 'maitainance','label'=> 'Maitainance'],
                                            ], 'value'=>$config->value])
                                    @break
                                @endif
                            @endforeach
                            
                            @if ($counter == 0)
                                @include('cms.layouts.blocks.input.config-flag',['fields'=>[
                                    ['name'=> 'maitainance','label'=> 'Maitainance'],
                                ]])
                            @endif

                        </div>
                    </div>
                </div>
            </div>

            @include('cms.layouts.blocks.input.buttons', ['formId'=>$id, 'submit'=>route('cms.general.configuration.post'), 'type'=>'PUT'])
        </form>
    </div>
</div>
