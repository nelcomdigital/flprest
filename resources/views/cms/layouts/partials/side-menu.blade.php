<div class="col-md-3 left_col menu_fixed">
    <div class="left_col scroll-view">
        <div class="navbar nav_title h-100">
            <div class="nav toggle">
                <a id="menu_toggle">
                    <div class="burger-menu">
                        <i></i>
                    </div>
                    {{-- <i class="fa fa-bars"></i> --}}
                </a>
            </div>
            <a href="javascript:void(0);" class="site_title"><img src="{{asset('images/icons/Forever_TMblack.png')}}"></a>
        </div>

        <div class="clearfix"></div>

        <div class="nav toggle">
            {{--<a id="menu_toggle">--}}
                {{--<div class="burger-menu">--}}
                    {{--<i></i>--}}
                {{--</div>--}}
                {{-- <i class="fa fa-bars"></i> --}}
            {{--</a>--}}
        </div>

        <div class="clearfix"></div>

        <div class="profile clearfix">
            <div class="profile_pic">
                <a href="javascript:openTabByUrl('{{route('user.profile')}}', 'profile', 'Profile')">
                    <img src="@if(isset(Auth::user()->image) and Auth::user()->image != '') {{asset(Auth::user()->image)}} @else {{asset('images/Avatar.svg')}} @endif" alt="..." class="img-circle profile_img">
                </a>
            </div>
            <div class="profile_info">
                <a href="javascript:openTabByUrl('{{route('user.profile')}}', 'profile', 'Profile')">
                <h2>{{Auth::user()->name ?:'' }}</h2>
                </a>
                <span>Administrator</span>
            </div>
        </div>
        <!-- /menu profile quick info -->

        <br />
        @php($menu = getConfig('modules'))

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <ul class="nav side-menu">
                @foreach($menu as $k=>$m )
                <li><a>
                        <h3>
                            @if ( getMenuIcon(Str::slug($k)) != null )
                            {!! getMenuIcon(Str::slug($k)) !!}
                        @endif
                        {{__('cms.'.Str::slug($k))}}
                            {{--<span class="fa fa-plus"></span>--}}
                        </h3></a>

                    <ul class="nav child_menu">
                        @include('cms.layouts.blocks.menu.menu-item', ['menu'=>$m, 'key'=>$k])
                    </ul>
                </li>
                @endforeach
                </ul>
            </div>

        </div>
        <!-- /sidebar menu -->

        <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small">
           <a data-toggle="tooltip" data-placement="top" title="Settings" href="javascript:openTabByUrl('{{route('cms.general.configuration')}}','GeneralConfiguration','' )">
               <i class="fa fa-cog" aria-hidden="true"></i>
           </a>
            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <i class="fa fa-arrows-alt" aria-hidden="true"></i>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Logout" href="{{route('cms.logout')}}">
                <i class="fa fa-sign-out" aria-hidden="true"></i>
            </a>
        </div>
        <!-- /menu footer buttons -->
    </div>
</div>


@push('js')

    <script>
        var tabSet = null;
        $(function(){
            tabSet = $('#inter_tabs').scrollTabs();
        })

        var tabCounter = 0 ;

        function openTabBySlug(slug,name) {

            tabCounter = tabCounter++;
            id = slug+(tabCounter);
            if(slug.indexOf('?') > -1){
                slug1 = slug.replace('?', '-');
                slug1 = slug1.replace('#', '-');
                slug1 = slug1.replace('=', '-');
                slug1 = slug1.split('/').join('-');
                id = slug1+(tabCounter);
            }

            var tab =$('#'+id+'_tab').index();
            if  ( tab == -1 ){
                $.ajax({
                    type: 'GET', //THIS NEEDS TO BE GET
                    url: location.origin+'/cms/'+slug,
                    beforeSend: function(){
                        tabSet.addTab('<li role="presentation" id="'+id+'_tab" class="scroll_tab">' +
                            '<a href="javascript:openTab(\''+id+'_content\')" id="'+id+'_tab_a" class="tab-a" role="tab" data-toggle="tab" aria-expanded="true">'+name +
                            '</a><a href="javascript:refresh(\''+id+'_content\',\''+ location.origin+'/cms/'+slug+' \')" class="close-tab"><i class="fa fa-refresh"></i> </a>' +
                            '<a href="javascript:closeTab(\''+id+'\')" class="close-tab"><i class="fa fa-close"></i> </a>' +
                            '</li>');

                        var loader =
                                        '<div class="vertical-centered-box loader" id="'+id+'_loader">'+
                                        '<div class="content">'+
                                        '<div class="loader-circle"></div>'+
                                        '<div class="loader-line-mask">'+
                                        '<div class="loader-line"></div>'+
                                        '</div>'+
                                        '<img src="{{asset('images/mini-moovtoo-black.svg')}}">'+
                                        '</div>'+

                                        '</div>';

                        $('<div role="tabpanel" class="tab-pane" style="height:100%;"  id="'+id+'_content">\n' +
                            loader+
                            '</div>'
                        ).appendTo("#inter_tabs_frames");
                        openTab(id+'_content');

                    },
                    complete: function(){
                        // Handle the complete event
                    },
                    success: function (data) {
                        $('#'+id+'_loader').remove();
                        $('#'+id+'_content').append(data);
                        openTab(id+'_content');
                        // $('#'+id+'_loader').hide(200);
                        $('#'+id+'_tab').show(200);

                        // $('.select2').select2({
                        //     width: '100%'
                        // });
                        init_custom_select2(id+'_content');
                        $('.select2-tags').select2({
                            width: '100%',
                            allowClear: true,
                            tags: true,
                        });
                        reAdjust();
                    },
                    error: function(data) {
                        console.log(data);
                    }
                });
            }
        }
        function openTabByUrl(url,slug,name) {

            var tab =$('#'+slug+(++tabCounter)+'_tab').index();
            if  ( tab == -1 ){
                $.ajax({
                    type: 'GET', //THIS NEEDS TO BE GET
                    url: url,
                    beforeSend: function(){
                        tabSet.addTab('<li role="presentation" id="'+slug+tabCounter+'_tab" class="scroll_tab">' +
                            '<a href="javascript:openTab(\''+slug+tabCounter+'_content\')" id="'+slug+tabCounter+'_tab_a" class="tab-a" role="tab" data-toggle="tab" aria-expanded="true">'+name +
                           '<a href="javascript:closeTab(\''+slug+tabCounter+'\')" class="close-tab"><i class="fa fa-close"></i> </a>' +
                            '</li>');

                        var loader =
                                        '<div class="vertical-centered-box loader" id="'+slug+tabCounter+'_loader">'+
                                        '<div class="content">'+
                                        '<div class="loader-circle"></div>'+
                                        '<div class="loader-line-mask">'+
                                        '<div class="loader-line"></div>'+
                                        '</div>'+
                                        '<img src="{{asset('images/mini-moovtoo-black.svg')}}">'+
                                        '</div>'+

                                        '</div>';

                        $('<div role="tabpanel" class="tab-pane" style="height:100%;"  id="'+slug+tabCounter+'_content">\n' +
                            loader+
                            '</div>'
                        ).appendTo("#inter_tabs_frames");
                        openTab(slug+tabCounter+'_content');

                    },
                    success: function (data) {
                        $('#'+slug+tabCounter+'_content').append(data.view);
                        $('#'+slug+tabCounter+'_tab_a').html(data.title);
                        openTab(slug+tabCounter+'_content');
                        $('#'+slug+tabCounter+'_loader').hide(200);
                        $('#'+slug+tabCounter+'_tab').show(200);

                        // $('.select2').select2({
                        //     width: '100%'
                        // });
                        init_custom_select2(slug+tabCounter+'_content');
                        $('.select2-tags').select2({
                            width: '100%',
                            tags: true,
                        });
                        // init_custom_select2(slug+tabCounter+'_content');
                        // $('.select2-tags').select2({
                        //     width: '100%',
                        //     tags: true,
                        // });
                        reAdjust();
                    },
                    error: function(data) {
                    }
                });
            }
        }

        function openTab(id){
            $('.tab-pane').removeClass('active');
            $('#'+id).addClass('active');
            $('.scroll_tab_inner').find('li').removeClass('active');
            $('#'+id.replace('content', 'tab')).addClass('active');

            $('html, body').animate({
                scrollTop: $('#'+id).offset().top - 130
            }, 'slow');
        }

        function refresh(id,url) {

            $.ajax({
                type: 'GET', //THIS NEEDS TO BE GET
                url: url,
                beforeSend: function(){
                    var loader =
                                    '<div id="progress-loader" class="vertical-centered-box">'+
                                    '<div class="content">'+
                                    '<div class="loader-circle"></div>'+
                                    '<div class="loader-line-mask">'+
                                    '<div class="loader-line"></div>'+
                                    '</div>'+
                                    '<img src="{{asset('images/mini-moovtoo-black.svg')}}">'+
                                    '</div>'+

                                    '</div>';
                    $('#' + id).html(loader);

                },
                success: function (data) {

                    $('#progress-loader').remove();
                    $('#' + id).html(data);

                    // $('.select2').select2({
                    //     width: '100%'
                    // });
                    init_custom_select2(id);
                    $('.select2-tags').select2({
                        width: '100%',
                        tags: true
                    });
                },
                error: function (data) {
                    console.log(data);
                }
            });
        }
        function closeTab(slug){
            if($('#'+slug+'_tab').hasClass('active')){
                if($('#'+slug+'_tab').prev('.scroll_tab').length > 0){
                    id = $('#'+slug+'_tab').prev('.scroll_tab').attr('id');
                    openTab(id.replace('_tab', '_content'))
                }else if($('#'+slug+'_tab').next('.scroll_tab').length  > 0){
                    id = $('#'+slug+'_tab').next('.scroll_tab').attr('id');
                    openTab(id.replace('_tab', '_content'))
                }
            }
            $('#'+slug+'_tab').remove();
            $('#'+slug+'_content').remove();
            reAdjust();
        }
    </script>


@endpush


