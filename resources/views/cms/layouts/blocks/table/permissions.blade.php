@php($menu = getConfig('modules'))
<div class="item form-group">
    <label class="control-label col-sm-2 col-xs-12" for="permissions">Choose Permissions</label>
    <div class="col-md-10 col-sm-10">
        <table class="role-table">
            <tr>
                <th>{{('Module Name')}}</th>
                <th><input type="checkbox" class="role-col-check" data-target="add" />&nbsp;{{('Add')}}</th>
                <th><input type="checkbox" class="role-col-check" data-target="show" />&nbsp;{{('Show')}}</th>
                <th><input type="checkbox" class="role-col-check" data-target="edit" />&nbsp;{{('Edit')}}</th>
                <th><input type="checkbox" class="role-col-check" data-target="delete" />&nbsp;{{('Delete')}}</th>
                <th><input type="checkbox" class="role-col-check" data-target="status" />&nbsp;{{('Status')}}</th>
            </tr>
            @foreach($menu as $k => $m)
                @if(is_array($m) and !array_has($m, 'show_side_menu'))
                    <tr>
                        <td colspan="6" class="role-dropdown-toggle" data-target="{{str_slug($k)}}">
                            <input type="checkbox" class="role-parent-checkbox" data-parent-target="{{str_slug($k)}}" />&nbsp;<span>{{$k}}<i class="fa fa-angle-down"></i></span>
                        </td>
                    </tr>
                    @include('cms.layouts.blocks.permissions.table-root', ['parent'=>$m, 'parent_name'=>$k])
                @endif
            @endforeach
        </table>
    </div>
</div>