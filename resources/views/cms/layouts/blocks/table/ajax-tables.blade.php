@php($fields = $model->fields)
@php($translatables = $model->translatable)
@php($table = uniqid(substr(get_class($model), strrpos(get_class($model), '\\')+1)))
@php($tableId = uniqid())
<div class="container-fluid" id="{{$tableId}}_container" style="padding: 0;display:none;">
    <div class="col-md-12 padding-0">
        <table class="table ajax-table" id="{{$table}}" cellspacing="0" width="100%" style="margin-top:0!important;">
            <thead>
                <tr>
                @foreach($fields as $field)
                        <th>{{(array_has($field, 'name')) ? $field['name'] : $field['key']}}</th>
                @endforeach
                    <th>@lang('cms.actions')</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<div class="modal fade" id="deleteModal_{{$tableId}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-head-orange"></div>
            <div class="modal-body delete-modal-body">
                <img src="{{asset('images/Close.svg')}}" class="img-responsive">
                <h4>Are you sure you want to delete this <span id="delete-type"></span></h4>
                <div class="modal-btns">
                    <button type="button" class="btn" id="confirmDelete_{{$tableId}}">Yes</button>
                    <button type="button" class="btn" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="message-box_{{$tableId}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-head-orange"></div>
            <div class="modal-body delete-modal-body">
                <img src="{{asset('images/Close.svg')}}" class="img-responsive">
                <h4 id="message"></h4>
                <div class="modal-btns">
                    {{-- <button type="button" class="btn" id="confirmDelete">Yes</button> --}}
                    <button type="button" class="btn" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>

   <script>
        $(function() {
            $('#{{$table}}').dataTable({
                processing: false,
                serverSide: true,
                autoWidth:         true,
                paging:         true,
                pageLength: 50,

                bLengthChange: false,

                searching : true,
                order: [[ @if(isset($custom_order)) {{$custom_order , isset($custom_sort)? $custom_sort : 'desc'}}  @else 0,'asc' @endif]],
                ajax: '{{ $route}}?table=true{{isset($params)?("&".$params):""}}',
                columns: [
                        @foreach($fields as $field)
                            @if (array_has($field, 'type') and $field['type'] == 'photo')
                                { data:'{{$field['key']}}' @if(array_has($field,'width')) ,width:'{{$field['width']}}' @endif , render: function(data){
                                    if(data == null){
                                        data = "{{asset('images/avatar.jpg')}}"
                                    }else if(data.indexOf('graph.facebook') == -1 &&  data.indexOf('googleusercontent') == -1){
                                        data = "{{asset('')}}/"+data
                                    }
                                    return  '<img src="'+data+'" class="img-responsive user-img-list">';
                            }},

                            @elseif(array_has($field, 'type') and $field['type'] == 'sub_domains')
                                { data: '{{$field['key']}}', render: function(data){
                                    var s = '';
                                    $.each(data, function(key, value){
                                        s += '<p class="p-subdomain"><a class="sub-link" href="https://'+value.domain_name+'.'+ value.parent.domain_name+'" target="_blank">'+ value.domain_name +'.'+ value.parent.domain_name +'</a>'+ (value.is_published =="1"? "  (Published)": " (Not Published)") +' <span class="table-subdomain pull-right"><a href="javascript:void(0)" class="sub-domain-edit" id="'+value._id+'">Edit</a> | <a href="javascript:void(0)" class="sub-domain-delete" id="'+value._id+'">Remove</a></span></p>';
                                    })
                                    s += '<p class="p-subdomain-add"><span class="table-subdomain"><a href="javascript:void(0)" class="add-subdomain">Add sub domain</a></p>'
                                    return s;
                                }},
                            @elseif (array_has($field, 'type') and $field['type'] == 'function')
                        { data:'{{$field['key']}}' @if(array_has($field,'width'))  ,width:'{{$field['width']}}' @endif , render: function(data){
                                var text = "";
                                if(data != undefined){
                                    if(data.length > 0){
                                        var length = data.length;
                                        $.each(data, function(key, value){
                                            if(key == length-1){
                                                text += value.name;
                                            }else{
                                                text += value.name+', ';
                                            }
                                        })
                                    }
                                }
                                return  text;
                            }},

                        @elseif (array_has($field, 'type') and $field['type'] == 'object')
                            { data:'{{$field['key']}}' @if(array_has($field,'width'))  ,width:'{{$field['width']}}' @endif , render: function(data){
                                    let text = "";
                                    try {
                                        if(data.slug === 'submitted-for-review'){
                                            text =  '<p style="color: #ff9100">'+data.name+'</p>';
                                        }
                                        else
                                        text = data.name;
                                    }catch (e) {

                                    }
                                    return  text;
                                }},
                            @elseif (array_has($field, 'type') and $field['type'] == 'objects')
                                { data:'{{$field['key']}}' @if(array_has($field,'width'))  ,width:'{{$field['width']}}' @endif , render: function(data){
                                    var text = "";
                                    try{
                                        if(data != undefined){
                                            if(data.length > 0){
                                                var length = data.length;
                                                $.each(data, function(key, value){
                                                    if(key == length-1){
                                                        text += value.name;
                                                    }else{
                                                        text += value.name+', ';
                                                    }
                                                })
                                            }
                                        }
                                    }catch (e) {

                                    }

                                    return  text;
                                }},
                        @elseif (array_has($field, 'type') and $field['type'] == 'translation')
                        { data:'values' @if(array_has($field,'width'))  ,width:'{{$field['width']}}' @endif , render: function(data){
                                let text = '';
                                try{
                                    text =  (data != undefined)? data.{{$field["key"]}}['value'] :'';
                                }catch (e) {

                                }
                                return  text;
                            }},
                            @elseif ($field['key'] == 'module_id')
                                { data:'{{$field['key']}}' @if(array_has($field,'width')) ,width:'{{$field['width']}}' @endif , render: function(data){
                                        text = '';
                                        var modules = [
                                                @foreach(getModules() as $module)
                                            ['{{$module['slug']}}', '{{$module['name']}}'],
                                            @endforeach
                                        ];
                                        $.each(modules, function(key, value){
                                            if(value[0] == data){
                                                text = value[1];
                                            }
                                        })
                                        return text;
                                    }},
                            @elseif (array_has($field, 'type') and $field['type'] == 'modules')
                                { data:'{{$field['key']}}' @if(array_has($field,'width')) ,width:'{{$field['width']}}' @endif , render: function(data){

                                    text = '';
                                    var modules = {
                                        @foreach(getModules() as $module)
                                        '{{$module['slug']}}': '{{$module['name']}}',
                                        @endforeach
                                    };
                                    if(data != undefined) {
                                        if (data.length > 0) {
                                            var length = data.length;
                                            $.each(data, function (key, value) {
                                                console.log(value);
                                                if (key == length - 1) {
                                                        text += modules[value];
                                                } else {
                                                        text += modules[value] + ', ';

                                                }
                                            })
                                        }
                                    }
                                    return text;
                            }},
                            @else

                                { data: '{{$field['key']}}' @if(array_has($field,'width')) ,width:'{{$field['width']}}' @endif, render: function(data){

                                    return  (data != undefined)? data : '';
                            }},
                            @endif
                        @endforeach
                            { data: 'Action', name: 'Action',width:'120px',
                                defaultContent:
                                @if ( !isset($ordering) or $ordering )
                                '<a href="javascript:void(0)" class="record-order-up"><img src="'+'{{asset("images/arrow-up.svg")}}'+'" class="img-responsive" style="display: inline-block;max-width: 16px;"> </a>\n'
                                +'<a href="javascript:void(0)" class="record-order-down"><img src="'+'{{asset("images/arrow-down.svg")}}'+'" class="img-responsive" style="display: inline-block;max-width: 16px;"> </a>\n'+
                                @endif
                                @if ( !empty($translatables))
                                    '<a href="javascript:void(0)" class="record-translate"><img src="' + '{{asset("images/translate_table.svg")}}' + '" class="img-responsive" style="display: inline-block;max-width: 16px;"> </a>\n' +
                                @endif
                                '<a href="javascript:void(0)" class="record-show"><img src="'+'{{asset("images/view.svg")}}'+'" class="img-responsive" style="display: inline-block;max-width: 16px;"> </a>\n'

                                +'<a href="javascript:void(0)" class="record-edit"><img src="'+'{{asset("images/edit.svg")}}'+'" class="img-responsive" style="display: inline-block;max-width: 16px;"> </a>\n'
                                +'<a href="javascript:void(0)" class="record-delete"><img src="'+'{{asset("images/remove.svg")}}'+'" class="img-responsive" style="display: inline-block;max-width: 15px;margin-left:5px;"> </a>'
                            }
                ],
                preInit: function(e, settings ){
                    var div = $('#{{$table}}_wrapper').children('div').first();
                    $(div).hide();
                },
                initComplete: function(settings, json) {
                    var div = $('#{{$table}}_wrapper').children('div').first();
                    div.css('background-color','white');
                    div.css('padding','10px 5px ');
                    div.css('margin','15px 0');
                    div.css('width','100%');
                    div.css('display','block');

                    div.html(
                        ''
                    );

                    //Append Create Button
                    div.append(
                    '<button class="add-btn" id="add-button-{{$table}}">'+
                        '<div class="add-btn-title"><span>+</span>@lang("cms.add_new") @lang('cms.'.strtolower(substr(get_class($model), strrpos(get_class($model), '\\')+1))) </div>'+
                    '</button>'
                    );

                    $('#add-button-{{$table}} div').click(function () {
                        openTabByUrl( '{{$route}}/create{{isset($params)?("?".$params):""}}','{{substr(get_class($model), strrpos(get_class($model), '\\')+1)}}'
                            ,'{{str_slug(substr(get_class($model), strrpos(get_class($model), '\\')+1))}}' )
                    });

                    //Append Search
                    div.append(
                        '<div class="ajax-table-search">'+
                        '<input type="search" class="form-control input-sm " id="data-search_{{$tableId}}" placeholder="Search..." aria-controls="table" >'+
                        '</div>'
                    );

                    var table = $('#{{$table}}').DataTable();
                    $('#data-search_{{$tableId}}').keyup(function () {



                        @if ( strtolower(substr(get_class($model), strrpos(get_class($model), '\\')+1)) =='translation'
                        or strtolower(substr(get_class($model), strrpos(get_class($model), '\\')+1)) =='city' )
                            params = '?table=true&'+'_qq='+$(this).val();//meshe l7l
                            $('#{{$table}}').DataTable().ajax.url("{{$route}}"+params).load();
                        @else
                        table.search($(this).val()).draw();
                        @endif
                    });

                    //apply filter here
                    @if (method_exists($model,'filterRender'))
                        div.append({!! $model->filterRender($tableId) !!});
                    @endif

                    {{--div.append('</div>');--}}

                    $('.select2-filter-change').on('change', function(e) {
                        params = '?table=true&'+$('#{{$tableId}}_form').serialize();
                        $('#{{$table}}').DataTable().ajax.url("{{$route}}"+params).load();
                    });


                    init_custom_select2('{{$tableId}}_form');

                    var div = $('#{{$table}}_wrapper').children('div').eq(2).css('padding','0 18px');

                    $('#{{$tableId}}_container').parent().find('.loader').fadeOut(300);
                    $('#{{$tableId}}_container').fadeIn(300);
                    $('#{{$tableId}}_container').parent().find('.loader').remove();

                }
            });

            $('#{{$table}}').on('click', 'a.record-show', function (e) {

                 openTabByUrl('{{$route}}/'+$('#{{$table}}').DataTable().row( $(this).parents('tr') ).data()._id,
                     '{{substr(get_class($model), strrpos(get_class($model), '\\')+1)}}'
                     ,'{{str_slug(substr(get_class($model), strrpos(get_class($model), '\\')+1))}}' )
            } );

            $('#{{$table}}').on('click', 'a.record-edit', function (e) {
                openTabByUrl('{{$route}}/'+$('#{{$table}}').DataTable().row( $(this).parents('tr') ).data()._id+'/edit'
                    ,'{{substr(get_class($model), strrpos(get_class($model), '\\')+1)}}'
                    ,'{{str_slug(substr(get_class($model), strrpos(get_class($model), '\\')+1))}}' )
            } );

            $('#{{$table}}').on('click', 'a.record-order-up', function (e) {

                $.ajax({url: "{{$route}}/"+$('#{{$table}}').DataTable().row( $(this).parents('tr') ).data()._id+"/up",
                    success: function(result){
                        $('#{{$table}}').DataTable().ajax.reload(null, false );
                        console.log(result);
                    }
                });
            });
            $('#{{$table}}').on('click', 'a.record-order-down', function (e) {

                $.ajax({url: "{{$route}}/"+$('#{{$table}}').DataTable().row( $(this).parents('tr') ).data()._id+"/down",
                    success: function(result){
                    $('#{{$table}}').DataTable().ajax.reload(null, false );
                    console.log(result);
                    }
                });
            });


            $('#{{$table}}').on('click', 'a.record-translate', function (e) {
                openTabByUrl('{{$route}}/' + $('#{{$table}}').DataTable().row($(this).parents('tr')).data()._id + '/translate'
                    , '{{substr(get_class($model), strrpos(get_class($model), '\\')+1)}}'
                    , '{{str_slug(substr(get_class($model), strrpos(get_class($model), '\\')+1))}}')
            });


            // Delete a record
            $('#{{$table}}').on('click', 'a.record-delete', function(e) // <-- add an event variable
            {
                e.preventDefault();
                var id =$('#{{$table}}').DataTable().row( $(this).parents('tr') ).data()._id;
                $('#delete-type').html($('#{{$table}}').DataTable().row( $(this).parents('tr') ).data().name);
                $('#deleteModal_{{$tableId}}').modal('show');
                $('#confirmDelete_{{$tableId}}').on('click',function(){
                    $('#deleteModal_{{$tableId}}').modal('hide');
                    $.ajax({
                        url: "{{$route}}/"+id,
                        type:'DELETE',
                        dataType: 'json',
                        data: {
                            "_token": "{{ csrf_token() }}",
                        },
                        success: function(result){
                            if ( result.status == 'success')
                                $('#{{$table}}').DataTable().ajax.reload(null, false );
                            else{
                                $('#message-box_{{$tableId}}').find('#message').html(result.message);
                                $('#message-box_{{$tableId}}').modal('show');

                            }
                            // if(result.fail == "Category"){
                            //     $('#deleteModalFail').modal('show');
                            // }
                        },
                        error: function (data) {
                            $('#{{$table}}').DataTable().ajax.reload(null, false );
                            console.log(data);
                        }
                    });
                })
            });

            //Sub Domains
            $('#{{$table}}').on('click', 'a.sub-domain-edit', function (e) {
                openTabByUrl('{{$route}}/'+$(this).attr('id')+'/edit'
                    ,'{{substr(get_class($model), strrpos(get_class($model), '\\')+1)}}'
                    ,'{{str_slug(substr(get_class($model), strrpos(get_class($model), '\\')+1))}}' )
            } );
            $('#{{$table}}').on('click', 'a.add-subdomain', function (e) {
                openTabByUrl( '{{$route}}/create?domain_id='+$('#{{$table}}').DataTable().row( $(this).parents('tr') ).data()._id,'{{substr(get_class($model), strrpos(get_class($model), '\\')+1)}}'
                ,'{{str_slug(substr(get_class($model), strrpos(get_class($model), '\\')+1))}}' )
            });

        });
    </script>
