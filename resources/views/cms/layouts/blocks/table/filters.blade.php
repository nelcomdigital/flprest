@if(count($filters) > 0)
    <form id="{{$tableId}}_form" class="filter-form">
        <div class="filter-title">
            <span>Filter by</span>
        </div>
        @foreach($filters as  $filter)
            <div class="select2-filter">
                <select type="text" class="form-control select2 @if(array_has($filter, 'ajax_search') and $filter['ajax_search']) select2-ajax  @else  @endif select2-filter-change"
                        id="{{(array_has($filter, 'model'))? $filter['name'].'_id' : $filter['name']}}"
                        name="filters[{{(array_has($filter, 'model'))? $filter['name'].'_id' : $filter['name']}}]"
                        style="width:100%;margin: auto;"
                        @if(array_has($filter, 'ajax_search') and $filter['ajax_search'])
                        data-route="{{route('cms.ajax.select.search', $filter['name'])}}"
                        data-name="{{camel_case(str_singular($filter['name']))}}" @endif
                        @if(array_has($filter, 'ajax_search_only') and $filter['ajax_search_only'])
                        data-search="true" @endif
                >
                    <option value="">Select {{camel_case(str_singular($filter['name']))}}</option>
                    @if((array_has($filter, 'ajax_search') and !$filter['ajax_search']) or !array_has($filter, 'ajax_search'))
                        @if(array_has($filter, 'model'))

                            @foreach($filterValues[$filter['name']] as $val)
                                <option value="{{$val->id}}">{{$val->name[session()->get('current_locale')]}}</option>
                            @endforeach
                        @else
                            @foreach($filterValues[$filter['name']]  as $val)
                                <option value="{{$val['value']}}">{{$val['name']}}</option>
                            @endforeach
                        @endif
                    @endif
                </select>
            </div>
        @endforeach
    </form>
@endif



