@php($id = uniqid('form'))
<div class="right_col full" role="main">
    <div class="">
        <div class="page-title">
            <div>
                <h3>{{$model->name}} {{__('cms.translation')}}</h3>
            </div>
        </div>

        <form class="form-horizontal form-label-left" id="{{$id}}" novalidate >
            @csrf
{{--            @if ( $model->allLocales )--}}
                @php ( $locales = \App\Models\Locale::all())
            {{--@else--}}
                {{--@php ( $locales = $model->locales)--}}
            {{--@endif--}}
            @foreach($locales as $locale)
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>{{__('cms.'.$locale->name)}}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            @foreach( $model->translatable as $translate)
                                @php($loc = $locale->code)
                                @if($model->translations != null  and
                                    array_has($model->translations , $loc) and
                                    $model->translations[$loc] != null and
                                    array_has($model->translations[$loc], $translate )  )
                                    @include('cms.layouts.blocks.input.field',['type'=>'text',
                                    'name'=>$locale->code."[".$translate."]", 'id'=>$locale->code."[".$translate."]", 'label'=>$translate.' translation ','value'=>$model->translations[$loc][$translate] ])
                                @else
                                    @include('cms.layouts.blocks.input.field',['type'=>'text',
                                    'name'=>$locale->code."[".$translate."]", 'id'=>$locale->code."[".$translate."]", 'label'=>$translate.' translation '])
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            @endforeach

            @include('cms.layouts.blocks.input.buttons', ['formId'=>$id, 'submit' => $route, 'type'=>'POST'])
        </form>
    </div>
</div>
