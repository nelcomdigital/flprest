@if ( $menu != null )
    @if ( is_array($menu) and count($menu)> 0 )
        @foreach($menu as $k=>$m)
            @if  ( is_array($m) and Arr::has($m, 'show_side_menu'))
                    @if ( $m['show_side_menu'] == 1)
                        <li class="sub_menu"><a href="javascript:openTabBySlug('{{$m['slug']}}', '{{__('cms.'.Str::slug($k))}}')">
                                @if ( getMenuIcon($m['slug']) != null )
                                {!! getMenuIcon($m['slug']) !!}
                                @endif
                                {{__('cms.'.Str::slug($k))}}</a></li>
                    @endif
            @else
                    <li><a>@if ( getMenuIcon(Str::slug($k)) != null )
                                {!! getMenuIcon(Str::slug($k)) !!}
                            @endif
                            {{__('cms.'.Str::slug($k))}}
                            <span class="fa fa-plus"></span></a>
                        <ul class="nav child_menu">
                            @include('cms.layouts.blocks.menu.menu-item',['menu'=>$m,'key'=>$k])
                        </ul>
                    </li>
            @endif
        @endforeach
    @endif
@endif
