<ul class="reference-nested">
    <div class="reference-sub-category">
        @if(count($reference->children()) > 0)
            <span class="reference-sub-category-title">Sub Category</span>
            <ul>
                @foreach ($reference->children() as $value)
                    <li>
                        <input type="checkbox"
                               @isset($ref_object)
                               @if ( $ref_object->isReferenceRelated($root.$value->id) )  {{'checked'}} @endif
                               @endisset
                               value="{{$root . $value->id}}" name="reference_ids[]"> {{$value->name}}
                    </li>
                @endforeach
            </ul>
        @endif
    </div>
    @if ($reference!= null &&  count($reference->subReferences())> 0 )
        @foreach( $reference->subReferences() as $value)
            @isset($ref_object)
            @include('cms.layouts.blocks.references.leaf-reference', ['value'=>$value, 'ref_object'=>$ref_object ,
            'root'=>$root.$value->id.'|'])
            @else
                @include('cms.layouts.blocks.references.leaf-reference', ['value'=>$value, 'root'=>$root.$value->id.'|'])
            @endisset
        @endforeach
    @endif
</ul>


