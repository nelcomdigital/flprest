@if(count($value->children()) > 0)
    <li class="li-inline">
        <div class="sub-reference-title">
            <span class="rsub-reference-name">{{$value->name}}</span>
        </div>
        <ul class="reference-sub-list">
            @foreach ($value->children() as $val)
                <li>
                    <input type="checkbox"
                           @isset($ref_object)
                           @if ( $ref_object->isReferenceRelated($root.$val->id) )  {{'checked'}} @endif
                           @endisset
                           value="{{$root.$val->id}}" name="reference_ids[]"> {{$val->name}}
                </li>
            @endforeach
        </ul>
    </li>
@endif

@if ($value!= null &&  count($value->subReferences())> 0 )
    @foreach( $value->subReferences() as $v)
        @isset($ref_object)
        @include('cms.layouts.blocks.references.leaf-reference', ['value'=>$v, 'ref_object'=>$ref_object ,
        'root'=>$root.$value->id.'|'])
        @else
            @include('cms.layouts.blocks.references.leaf-reference', ['value'=>$v, 'root'=>$root.$value->id.'|'])
        @endisset
    @endforeach
@endif
