<li>
        <div class="reference-tab  @if ((isset($ref_object) and $ref_object->isReferenceRelated($reference->id)))  reference-active  @endif">
            <div class="reference-caret"><img src="{{asset("$reference->icon")}}"></div>
            <span class="">{{$reference->name}}</span>
            <span class="reference-tab-toggle"><i class="fa fa-plus"></i></span>
        </div>
    @isset($ref_object)
        @include('cms.layouts.blocks.references.sub-reference', ['reference'=>$reference, 'ref_object'=>$ref_object ,'root'=>$reference->id.'|'])
        @else
        @include('cms.layouts.blocks.references.sub-reference', ['reference'=>$reference ,'root'=>$reference->id.'|'])
    @endisset
    </li>
