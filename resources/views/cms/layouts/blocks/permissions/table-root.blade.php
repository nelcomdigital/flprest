@if(is_array($parent) and count($parent) > 0)
    @foreach($parent as $k => $mod)
        @if(is_array($mod) and array_has($mod, 'show_side_menu'))
            @include('cms.layouts.blocks.permissions.table-row', ['child'=>$mod, 'parent_name'=>$parent_name])
        @elseif(is_array($mod) and !array_has($m, 'show_side_menu'))
            <tr data-list="{{str_slug($parent_name)}}" style="display:none">
                <td colspan="6" class="role-dropdown-toggle" data-target="{{str_slug($k)}}">
                    <input type="checkbox" class="role-parent-checkbox role-parent-module-checkbox" data-parent-check="{{str_slug($parent_name)}}" data-parent-target="{{str_slug($k)}}"  />&nbsp;<span>{{$k}}<i class="fa fa-angle-down"></i></span>
                </td>
            </tr>
            @include('cms.layouts.blocks.permissions.table-root', ['parent'=>$mod, 'parent_name'=>$k])
        @endif

        {{-- @if($module->parentChildren->count() == 0 and $module->children()->count() == 0)

        @else
            <tr data-list="{{$parent->id}}" style="display:none">
                <td colspan="6" class="role-dropdown-toggle" data-target="{{$module->id}}">
                    <input type="checkbox" class="role-parent-checkbox role-parent-module-checkbox" data-parent-check="{{$parent->id}}" data-parent-target="{{$module->id}}"  />&nbsp;<span>{{$module->name}}<i class="fa fa-angle-down"></i></span>
                </td>
            </tr>
            @if($module->children()->count() > 0)
                @include('cms.layouts.partials.role-table-rows', ['children'=>$module->children(), 'mod'=>$module])
            @endif
            @if($module->parentChildren->count() > 0)
                @include('cms.layouts.partials.role-table-root', ['parent'=>$module])
            @endif
        @endif --}}
    @endforeach
@endif