<tr data-list="{{str_slug($parent_name)}}" style="display:none">
    <td><input type="checkbox" class="role-checkbox" data-target="{{$child['slug']}}" data-parent-check="{{str_slug($parent_name)}}" />
        &nbsp;<span>{{$child['name']}}</span></td>
    <td><input type="checkbox" name="permissions[{{$child['slug'] }}][add]" data-col-check="add" data-check="{{$child['slug']}}" value="1"
        @if (isset($role)) 
            @if( array_has($role->permissions, $child['slug']) && array_has($role->permissions[$child['slug']], 'add')) @if( $role->permissions[$child['slug']]['add'] == 1 ) checked @endif @endif
        @endif
        />
        &nbsp;</td>

    <td><input type="checkbox" name="permissions[{{$child['slug'] }}][show_all]" data-col-check="show"
            data-check="{{$child['slug']}}" value="1"
            @if (isset($role)) 
                @if( array_has($role->permissions, $child['slug']) && array_has($role->permissions[$child['slug']], 'show_all')) @if( $role->permissions[$child['slug']]['show_all'] == 1 ) checked @endif @endif
            @endif
        />
        &nbsp;<span class="role-permission-label">{{('All records')}}</span> <br />
        {{-- @if($child->user_related) --}}
            <input type="checkbox" name="permissions[{{$child['slug'] }}][show]" data-col-check="show"
            data-check="{{$child['slug']}}" value="1"
            @if (isset($role)) 
                @if( array_has($role->permissions, $child['slug']) && array_has($role->permissions[$child['slug']], 'show')) @if( $role->permissions[$child['slug']]['show'] == 1 ) checked @endif @endif
            @endif
        />
            <span class="role-permission-label">{{('Only user record')}}</span>
        {{-- @endif --}}
    </td>

    <td><input type="checkbox" name="permissions[{{$child['slug'] }}][edit_all]" data-col-check="edit"
            data-check="{{$child['slug']}}" value="1"
            @if (isset($role)) 
                @if( array_has($role->permissions, $child['slug']) && array_has($role->permissions[$child['slug']], 'edit_all')) @if(  $role->permissions[$child['slug']]['edit_all'] == 1 ) checked @endif @endif
            @endif
        />
        &nbsp;<span class="role-permission-label">{{('All records')}} </span><br />
        {{-- @if($child->user_related) --}}
            <input type="checkbox" name="permissions[{{$child['slug'] }}][edit]" data-col-check="edit"
            data-check="{{$child['slug']}}" value="1"
            @if (isset($role)) 
                @if( array_has($role->permissions, $child['slug']) && array_has($role->permissions[$child['slug']], 'edit')) @if( $role->permissions[$child['slug']]['edit'] == 1 ) checked @endif @endif
            @endif
        />
            <span class="role-permission-label">{{('Only user record')}}</span>
        {{-- @endif --}}
    </td>

    <td><input type="checkbox" name="permissions[{{$child['slug'] }}][delete_all]" data-col-check="delete"
            data-check="{{$child['slug']}}" value="1"
            @if (isset($role))
                @if( array_has($role->permissions, $child['slug']) && array_has($role->permissions[$child['slug']], 'delete_all')) @if( $role->permissions[$child['slug']]['delete_all'] == 1 ) checked @endif @endif
            @endif
        />
        &nbsp;<span class="role-permission-label">{{('All records')}}</span> <br />
        {{-- @if($child->user_related) --}}
            <input type="checkbox" name="permissions[{{$child['slug'] }}][delete]"
            data-col-check="delete" data-check="{{$child['slug']}}" value="1"
            @if (isset($role)) 
                @if( array_has($role->permissions, $child['slug']) && array_has($role->permissions[$child['slug']], 'delete')) @if( $role->permissions[$child['slug']]['delete'] == 1 ) checked @endif @endif
            @endif
        />
            <span class="role-permission-label">{{('Only user record')}}</span>
        {{-- @endif --}}
    </td>

    <td><input type="checkbox" name="permissions[{{$child['slug'] }}][status]" data-col-check="status"
            data-check="{{$child['slug']}}" value="1"
            @if (isset($role)) 
                @if( array_has($role->permissions, $child['slug']) && array_has($role->permissions[$child['slug']], 'status')) @if( $role->permissions[$child['slug']]['status'] == 1) checked @endif @endif
            @endif
        /> </td>
</tr>
