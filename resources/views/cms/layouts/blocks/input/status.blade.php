<div class="item form-group">
    <label class="control-label col-sm-2 col-xs-12">{{__('cms.Status')}}@if(isset($required) and $required)<span class="required">*</span> @endif</label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <select class="select2 form-control @if(isset($required) and $required) required @endif" name="status_id" id="status_id"  data-error-message="This field is required"  >
            @php($options = \App\Models\Status::all())
            <option value="">Select Status</option>
            @if(isset($options))
                @foreach ($options as $value)
                    <option value="{{$value->id}}"
                    @if (isset($selected) and $selected != null)
                        {{ ($selected->id == $value->id)? 'selected' : ''}}
                        @endif
                    >
                        {{$value->name}}</option>
                @endforeach
            @endif
        </select>
    </div>
</div>
<script>
    // $('.select2').select2({
    //     width: '100%',
    // });
</script>
