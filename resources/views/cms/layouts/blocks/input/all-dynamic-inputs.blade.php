@php($uId = uniqid())

<div class="item form-group">
    <label class="control-label col-sm-12 col-xs-12" >{{__('cms.'.str_slug($label))}}</label>
    <div >
        <div class="dynamic-inputs col-md-12">
            @if(isset($values) and count($values) > 0)
                @foreach ($values as $value)
                    @php($counter = rand(1,1000))
                    <div class="all-inline-inputs">
                        @foreach ($value as $val)
                            @if($inputs[$loop->index]['type'] == 'select')
                                <div class="item form-group">
                                    <label class="control-label col-sm-2 col-xs-12" for="{{$inputs[$loop->index]['name']}}">{{__('cms.'.str_slug($inputs[$loop->index]['label']))}} @if(isset($required) and
                                     $required)<span class="required">*</span> @endif
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="select2 form-control  select2-hidden-accessible" name="{{$inputs[$loop->index]['name']}}[{{$counter}}]">

                                    @if ( array_has($inputs[$loop->index] , 'options'))
                                        @foreach ($inputs[$loop->index]['options'] as $opt)
                                            <option value="{{$opt->id}}" @if($opt->id == $val) selected @endif>{{$opt->name}}</option>
                                        @endforeach
                                    @else
                                        @foreach ($inputs[$loop->index]['custom_options'] as $opt)
                                            <option value="{{$opt['value']}}" @if($opt['value'] == $val) selected @endif>{{$opt['name']}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                    </div>
                                </div>
                            @elseif($inputs[$loop->index]['type'] == 'ckeditor')
                                @php( $ckId = uniqid($id))
                                <div class="item form-group">
                                    <label class="control-label col-sm-2 col-xs-12" for="{{$inputs[$loop->index]['name']}}">{{__('cms.'.str_slug($inputs[$loop->index]['label']))}} @if(isset($required) and
                                     $required)<span class="required">*</span> @endif
                                    </label>
                                    <div class="col-md-10 col-sm-6 col-xs-12">
                                    <textarea id="{{$ckId}}" name="{{$inputs[$loop->index]['name']}}[{{$counter}}]" class="form-control col-md-12 col-xs-12 ckeditor" placeholder="{{$inputs[$loop->index]['placeHolder']}}">{!!$val ?: ''!!}</textarea>
                                    </div>
                                </div>
                                <script>CKEDITOR.replace({{$ckId}});</script>

                            @elseif($inputs[$loop->index]['type'] == 'ajax')
                                @php( $slctId = uniqid($id))
                                <div class="item form-group">
                                    <label class="control-label col-sm-2 col-xs-12">{{__('cms.'.str_slug($inputs[$loop->index]['label']))}}@if(isset($inputs[$loop->index]['required']) and $inputs[$loop->index]['required'])<span class="required">*</span> @endif</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select type="text" class="select2 select2-ajax form-control @if(isset($inputs[$loop->index]['required']) and $inputs[$loop->index]['required']) required @endif"
                                                @isset($inputs[$loop->index]['model']) data-route="{{route('cms.select.ajax', $inputs[$loop->index]['model'])}}" @endif data-name="{{$label}}"
                                                name="{{$inputs[$loop->index]['name']}}[{{$counter}}]" id="{{$slctId}}" @if((isset($inputs[$loop->index]['multiple']) and $inputs[$loop->index]['multiple']) || (isset($inputs[$loop->index]['selected']) and is_array($inputs[$loop->index]['selected']))) multiple @endif @isset ($inputs[$loop->index]['error']) data-error-message="{{$inputs[$loop->index]['error']}}" @endisset >
                                            @if(!isset($inputs[$loop->index]['multiple']) or !$inputs[$loop->index]['multiple'])
                                                <option value="">Select {{$inputs[$loop->index]['label']}}</option>
                                            @endif

                                                @if((isset($inputs[$loop->index]['multiple']) and $inputs[$loop->index]['multiple']) and is_array($val))
                                                    @foreach ($val as $v)
                                                        <option value="{{$v['_id']}}" selected>{{$v['name']}}</option>
                                                    @endforeach
                                                @else
                                                    <option value="{{$v->id}}" selected>{{$v->name}}</option>
                                                @endif

                                        </select>
                                    </div>
                                </div>

                            @elseif($inputs[$loop->index]['type'] == 'image')
                                <div class="item form-group">
                                    <label class="control-label col-sm-2 col-xs-12" for="{{$inputs[$loop->index]['name']}}">{{__('cms.'.str_slug($inputs[$loop->index]['label']))}} @if(isset($required) and
                                     $required)<span class="required">*</span> @endif
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        @if(isset($val) and $val != null)
                                            @php( $imId = uniqid($id))
                                            <div class="input-image show @if(isset($inputs[$loop->index]['icon']) and $inputs[$loop->index]['icon']) icon-image @endif">
                                                <img src="{{(str_contains($val, 'graph.facebook') or str_contains($val, 'googleusercontent'))? $value : asset($val)}}" id="{{$imId.'_'.$inputs[$loop->index]['name']}}-display" />
                                                <a href="javascript:void(0)" class="image-fav delete-object-image" data-id="{{$imId.'_'.$inputs[$loop->index]['name']}}" data-route="{{route('dossier.delete-image', $imId)}}"><img src="{{asset("images/deletemodifypic.png")}}"> </a>
                                            </div>
                                        @else
                                            @php( $imId = uniqid($id))
                                            <div class="input-image @if(isset($inputs[$loop->index]['icon']) and $inputs[$loop->index]['icon']) icon-image @endif">
                                                <img src="" id="{{$imId.'_'.$inputs[$loop->index]['name']}}-display" />
                                                <a href="javascript:void(0)" class="image-fav delete-selected-image" data-id="{{$imId.'_'.$inputs[$loop->index]['name']}}"><img src="{{asset("images/deletemodifypic.png")}}"> </a>
                                            </div>
                                        @endif
                                        <div class="image-input-box">
                                            <a href="javascript:void(0)" class="popup_selector" data-inputid="{{$imId.'_'.$inputs[$loop->index]['name']}}" data-type="image">{{__('cms.browse_image')}}</a>
                                            <input class="form-control col-md-7 col-xs-12" type="text" id="{{$imId.'_'.$inputs[$loop->index]['name']}}" name="{{$inputs[$loop->index]['name']}}[{{$counter}}]" value="{{$val ?: ''}}" readonly>
                                        </div>
                                    </div>
                                </div>
                            @elseif ($inputs[$loop->index]['type'] == 'text' and array_has($inputs[$loop->index],'class' ) and  $inputs[$loop->index]['class'] == 'time')
                                    @php( $tmId = uniqid($id))
                                <div class="item form-group">
                                    <label class="control-label col-sm-2 col-xs-12" for="{{$inputs[$loop->index]['name']}}">{{__('cms.'.str_slug($inputs[$loop->index]['label']))}} @if(isset($required) and
                                     $required)<span class="required">*</span> @endif
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input class="form-control col-md-7 col-xs-12  update-time "
                                           type="{{$inputs[$loop->index]['type']}}" name="{{$inputs[$loop->index]['name']}}[{{$counter}}]"
                                           placeholder="{{$inputs[$loop->index]['placeHolder']}}" id ={{$tmId}}  value="{{$val}}">
                                    </div>
                                </div>
                                @else
                                <div class="item form-group">
                                    <label class="control-label col-sm-2 col-xs-12" for="{{$inputs[$loop->index]['name']}}">{{__('cms.'.str_slug($inputs[$loop->index]['label']))}} @if(isset($required) and
                                     $required)<span class="required">*</span> @endif
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input class="form-control col-md-7 col-xs-12 "
                                           type="{{$inputs[$loop->index]['type']}}" name="{{$inputs[$loop->index]['name']}}[{{$counter}}]"
                                           placeholder="{{$inputs[$loop->index]['placeHolder']}}"  value="{{$val}}">
                                    </div>
                                </div>
                            @endif
                            <div class="delete-input" onclick="$(this).parent().remove();">-</div>
                        @endforeach
                    </div><hr>
                @endforeach
            @endif
        </div>
        <div class="add-input add-input_{{$uId}}">
            <div class="plus">+</div><span>{{$add}}</span>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('.add-input_{{$uId}}').on('click', function(){
        var div = '<div class="all-inline-inputs">';
        var id = '{{$uId}}';
        var ckId = randNum();
        var slctId = randNum();
        var tmId = randNum();
        var counter = randNum();

        @foreach($inputs as $input)
                @if($input['type'] == 'select')
            div  += '<div class="item form-group">\n'+
            '<label class="control-label col-sm-2 col-xs-12" for="{{$inputs[$loop->index]['name']}}">\n'+
            '{{__('cms.'.str_slug($inputs[$loop->index]['label']))}} @if(isset($required) and$required)<span class="required">*</span> @endif</label>\n';
        div  += '<div class="col-md-6 col-sm-6 col-xs-12">\n' +
            '<select class="select2_'+id+'" name="{{$input['name']}}['+counter+']">'+
            '<option value="">Select</option>'+
                @if ( array_has($input , 'options'))

                        @foreach($input['options'] as $opt)
                            '<option class="col-md-12" value="{{$opt->id}}">{{$opt->name}}</option>'+
                        @endforeach
                        @else
                        @foreach($input['custom_options'] as $opt)
                            '<option class="col-md-12" value="{{$opt['value']}}">{{$opt['name']}}</option>'+
                        @endforeach
                        @endif
                    '</select></div></div>';
        @elseif($input['type']  == 'ckeditor')
            div  += '<div class="item form-group">\n'+
            '<label class="control-label col-sm-2 col-xs-12" for="{{$inputs[$loop->index]['name']}}">\n'+
            '{{__('cms.'.str_slug($inputs[$loop->index]['label']))}} @if(isset($required) and$required)<span class="required">*</span> @endif</label>\n';
        div  += '<div class="col-md-10 col-sm-6 col-xs-12">\n' +
            '<textarea id="'+ckId+'_ck" name="{{$input['name']}}['+counter+']" class="form-control col-md-12 col-xs-12 ckeditor" placeholder="{{$input['placeHolder']}}"></textarea></div></div>';


        @elseif($input['type']  == 'image')
            div  += '<div class="item form-group">\n'+
            '<label class="control-label col-sm-2 col-xs-12" for="{{$inputs[$loop->index]['name']}}">\n'+
            '{{__('cms.'.str_slug($inputs[$loop->index]['label']))}} @if(isset($required) and$required)<span class="required">*</span> @endif</label>\n';
        div  += '<div class="col-md-6 col-sm-6 col-xs-12">\n' +
            '<div class="image-input-box">\n' +
            '    <a href="javascript:void(0)" class="popup_selector" data-inputid="'+ckId+'_img" data-type="image">{{__('cms.browse_image')}}</a>\n' +
            '    <input type="text" id="'+ckId+'_img" name="{{$input['name']}}['+counter+']" readonly >\n' +
            '</div></div></div>';


         @elseif($input['type']  == 'ajax')
             div  += '<div class="item form-group">\n'+
             '<label class="control-label col-sm-2 col-xs-12">{{__('cms.'.str_slug($inputs[$loop->index]['label']))}}\n'+
             '@if(isset($inputs[$loop->index]['required']) and $inputs[$loop->index]['required'])<span class="required">*</span> @endif</label>\n';
        div  += '<div class="col-md-6 col-sm-6 col-xs-12">\n' +
            '<select type="text" class="select2 select2-ajax form-control '+slctId+'_slct @if(isset($inputs[$loop->index]['required']) and $inputs[$loop->index]['required']) required @endif"\n'+
        '@isset($inputs[$loop->index]['model']) data-route="{{route('cms.select.ajax', $inputs[$loop->index]['model'])}}" @endif data-name="{{$inputs[$loop->index]['name']}}"\n'+
        'name="{{$inputs[$loop->index]['name']}}['+counter+']@if((isset($inputs[$loop->index]['multiple']) and $inputs[$loop->index]['multiple']))[]@endif"  id="temp_'+randNum()+' " @if((isset($inputs[$loop->index]['multiple']) and $inputs[$loop->index]['multiple']) || (isset($inputs[$loop->index]['selected']) and is_array($inputs[$loop->index]['selected']))) multiple @endif @isset ($inputs[$loop->index]['error']) data-error-message="{{$inputs[$loop->index]['error']}}" @endisset >\n'+
                @if(!isset($inputs[$loop->index]['multiple']) or !$inputs[$loop->index]['multiple'])
            '<option value="">Select {{$inputs[$loop->index]['label']}}</option>\n'+
                @endif

                @if (isset($inputs[$loop->index]['selected']) and $inputs[$loop->index]['selected'] != null)
                @if((isset($inputs[$loop->index]['multiple']) and $inputs[$loop->index]['multiple']))
                @foreach ($inputs[$loop->index]['selected'] as $value)
            '<option value="{{$value->id}}" selected>{{$value->name}}</option>\n'+
        @endforeach
        @else
        '<option value="{{$inputs[$loop->index]['selected']->id}}" selected>{{$inputs[$loop->index]['selected']->name}}</option>\n'+
        @endif
        @endif

        '</select></div></div>' ;


        @elseif ($inputs[$loop->index]['type'] == 'text' and array_has($inputs[$loop->index],'class')and $inputs[$loop->index]['class'] == 'time')
            div  += '<div class="item form-group">\n'+
                '<label class="control-label col-sm-2 col-xs-12" for="{{$inputs[$loop->index]['name']}}">\n'+
                '{{__('cms.'.str_slug($inputs[$loop->index]['label']))}} @if(isset($required) and$required)<span class="required">*</span> @endif</label>\n';
            div  += '<div class="col-md-6 col-sm-6 col-xs-12">\n' +
            '<input class="form-control col-md-7 col-xs-12 update-time "  id="'+tmId+'_time" type="{{$input['type']}}" name="{{$input['name']}}['+counter+']" placeholder="{{$input['placeHolder']}}" @if ( array_has($input, 'default') ) value={{$input['default']}} @endif>' +
            '</div></div>';
        @else
            div  += '<div class="item form-group">\n'+
                '<label class="control-label col-sm-2 col-xs-12" for="{{$inputs[$loop->index]['name']}}">\n'+
                '{{__('cms.'.str_slug($inputs[$loop->index]['label']))}} @if(isset($required) and$required)<span class="required">*</span> @endif</label>\n';
            div  += '<div class="col-md-6 col-sm-6 col-xs-12">\n' +
            '<input class="form-control col-md-7 col-xs-12 "  type="{{$input['type']}}" name="{{$input['name']}}['+counter+']" placeholder="{{$input['placeHolder']}}" @if ( array_has($input, 'default') ) value={{$input['default']}} @endif>' +
            '</div></div>';
            {{--div += '<input type="{{$input['type']}}" name="{{$input['name']}}[]" placeholder="{{$input['placeHolder']}}" @if ( array_has($input, 'default') ) value={{$input['default']}} @endif>'--}}
        @endif
                @endforeach
            div += '<div class="delete-input" onclick="$(this).parent().remove();">-</div>';
        div += '</div><hr>';

        $parentDiv = $(this).parent().find('.dynamic-inputs');
        $parentDiv.append(div);
        init_custom_select_one_class(slctId+'_slct');
        init_time_picker(tmId+'_time');
        $('.select2_'+id).select2({
            width: '100%'
        });
        CKEDITOR.replace(ckId+'_ck');
    })


</script>
