@php($uId = uniqid())
<div class="item form-group">
    <label class="control-label col-sm-2 col-xs-12" >{{__('cms.'.str_slug($label))}}</label>
    <div class="col-md-8 col-sm-8 col-xs-12">
        <div class="dynamic-inputs">
            @if(isset($values) and count($values) > 0)
                @foreach ($values as $value)
                    <div class="inline-inputs">
                        @if ( is_array($value))
                            @foreach ($value as $val)
                                @if($inputs[$loop->index]['type'] == 'select')
                                    <select class="select2" name="{{$inputs[$loop->index]['name']}}[]" @if(isset($inputs[$loop->index]['required']) and $inputs[$loop->index]['required']) required="required" @endif>
                                        @if ( array_has($inputs[$loop->index] , 'options'))
                                            @foreach ($inputs[$loop->index]['options'] as $opt)
                                                <option value="{{$opt->id}}" @if(  $opt->id == $val['_id'] )selected @endif>{{$opt->name}}</option>
                                            @endforeach
                                        @else
                                            @foreach ($inputs[$loop->index]['custom_options'] as $opt)
                                                <option value="{{$opt['value']}}" @if($opt['value'] == $val) selected @endif>{{$opt['name']}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                @elseif($inputs[$loop->index]['type'] == 'ckeditor')
                                    @php( $ckId = uniqid($id))
                                    <textarea id="{{$ckId}}"  name="{{$inputs[$loop->index]['name']}}" class="form-control col-md-7 col-xs-12 ckeditor" placeholder="{{$inputs[$loop->index]['placeHolder']}}" >@if(isset($inputs[$loop->index]['value'])){!!$inputs[$loop->index]['value']!!}@endif</textarea>
                                    <script>CKEDITOR.replace({{$ckId}});</script>
                                @else
                                    <input type="{{$inputs[$loop->index]['type']}}" name="{{$inputs[$loop->index]['name']}}[]" placeholder="{{$inputs[$loop->index]['placeHolder']}}" value="{{$val}}" @if(isset($inputs[$loop->index]['required']) and $inputs[$loop->index]['required']) required="required" @endif>
                                @endif
                                <div class="delete-input" onclick="$(this).parent().remove();">-</div>
                            @endforeach
                        @else

                        @endif

                    </div>
                @endforeach
            @endif
        </div>
        <div class="add-input add-input_{{$uId}}">
            <div class="plus">+</div><span>{{$add}}</span>
        </div>
    </div>
</div>
<script>
    $('.add-input_{{$uId}}').on('click', function(){
        var div = '<div class="inline-inputs">';
        var id = '{{$uId}}';
        var ckId = Math.floor(Math.random() * Math.floor(1000));

        @foreach($inputs as $input)
            @if($input['type'] == 'select')
                div +=
                    '<select class="select2_'+id+'" name="{{$input['name']}}[]" @if(isset($inputs[$loop->index]['required']) and $inputs[$loop->index]['required']) required="required" @endif>'+
                        '<option value="">Select</option>'+
                        @if ( array_has($input , 'options'))

                            @foreach($input['options'] as $opt)
                            '<option value="{{$opt->id}}">{{$opt->name}}</option>'+
                            @endforeach
                        @else
                                @foreach($input['custom_options'] as $opt)
                                '<option value="{{$opt['value']}}">{{$opt['name']}}</option>'+
                                @endforeach
                        @endif

                    '</select>';
        @elseif($input['type']  == 'ckeditor')
         div += ' <textarea id="'+ckId+'_ck" name="{{$input['name']}}" class="form-control col-md-7 col-xs-12 ckeditor" placeholder="{{$input['placeHolder']}}">@if(isset($input['value'])){!!$input['value']!!}@endif</textarea>'

        {{--div += ' <textarea id="{{$ckId}}" name="{{$input['name']}}" class="form-control col-md-7 col-xs-12 ckeditor" >@if(isset($value)){!!$value!!}@endif</textarea>'--}}
        @else
                div += '<input type="{{$input['type']}}" name="{{$input['name']}}[]" placeholder="{{$input['placeHolder']}}" @if ( array_has($input, 'default') ) value={{$input['default']}} @endif @if(isset($inputs[$loop->index]['required']) and $inputs[$loop->index]['required']) required="required" @endif>'
        @endif
        @endforeach
        div += '<div class="delete-input" onclick="$(this).parent().remove();">-</div>';
        div += '</div>';

        $parentDiv = $(this).parent().find('.dynamic-inputs');
        $parentDiv.append(div);

        $('.select2_'+id).select2({
            width: '100%'
        });
        CKEDITOR.replace(ckId+'_ck');
    })

</script>
