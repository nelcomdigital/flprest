@php($ckId = uniqid($id))
<div class="item form-group">
    <label class="control-label col-sm-2 col-xs-12" for="{{$name}}">{{__('cms.'.str_slug($label))}} @if(isset($required) and $required) <span class="required">*</span> @endif
    </label>
    <div class="col-md-10 col-sm-10 col-xs-12">
        <textarea id="{{$ckId}}" name="{{$name}}" @isset ($error) data-error-message="{{$error}}" @endisset  class="form-control col-md-7 col-xs-12 ckeditor" placeholder="{{$placeHolder}}"  @if(isset($required) and $required) required @endif>@if(isset($value)){!!$value!!}@endif</textarea>
    </div>
</div>
<script>
    CKEDITOR.replace('{{$ckId}}');
</script>
