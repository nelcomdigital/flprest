<div class="item form-group">
    @foreach ($fields as  $field)
            <label class="control-label col-sm-2 col-xs-12" for="{{$field['name']}}">{{__('cms.'.str_slug($field['label']))}} </label>
            <div class="day-switch" style="margin: 12px;">
                <label class="switch">
                    <input type="checkbox" class="time-switch" value="1"  name="{{$field['name']}}"
                            @if(isset($value) and $value ) checked @endif>
                    <span class="slider round"></span>
                </label>
            </div>
    @endforeach
</div>

