<div class="item form-group">
    <label class="control-label col-sm-2 col-xs-12" for="{{$name}}">{{__('cms.'.str_slug($label))}} @if(isset($required) and $required)<span class="required">*</span> @endif
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="{{$type}}" id="{{$id}}" name="{{$name}}" @if(isset($required) and $required) required="required" @endif   @if(isset($readOnly) and $readOnly) readonly @endif  @if(isset($disabled) and $disabled) disabled @endif  @isset ($error) data-error-message="{{$error}}" @endisset class="form-control col-md-7 col-xs-12" @isset($placeHolder) placeholder="{{$placeHolder}}" @endisset @if(isset($value)) value="{{$value}}" @endif @if(isset($readonly) and $readonly) readonly @endif>
    </div>
</div>
