<div class="item form-group">
    <label class="control-label col-sm-2 col-xs-12" for="{{$name}}">{{__('cms.'.str_slug($label))}} @if(isset($required) and $required) <span class="required">*</span> @endif
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <textarea id="{{$id}}" @if(isset($required) and $required) required="required" @endif @isset ($error) data-error-message="{{$error}}" @endisset  name="{{$name}}" class="form-control col-md-7 col-xs-12" placeholder="{{$placeHolder}}" >@if(isset($value)) {!! $value !!} @endif</textarea>
    </div>
</div>
