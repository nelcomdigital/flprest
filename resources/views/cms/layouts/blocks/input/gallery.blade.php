@php($gId = uniqid())
<div class="item form-group">
    <label class="control-label col-sm-12" for="{{$name}}">{{$label}}</label>
    <div class="col-md-12">
        <div class="image-galley-div">
            <div class="gallery-item" id="gallery-inputs_{{$gId}}">
                @if(isset($value))
                    @foreach ($value as $val)
                        <div class="gallery-image">
                            <img src="{{asset($val)}}" />
                            <a href="javascript:void(0)" class="image-fav delete-gallery-img" data-route="{{$route}}" data-image="{{$val}}" data-gallery="{{$gId}}"><img src="{{asset("images/deletemodifypic.png")}}"> </a>
                        </div>
                    @endforeach
                @endif
                <label class="file">
                    <img src="{{asset('images/gallery.jpg')}}">
                    <p>{{__('cms.add-photos')}}</p>
                    <input type="file" class="gallery-input" name="{{$name}}[]" id="{{$id}}" data-id="{{$gId}}" aria-label="File browser example" multiple>
                </label>
            </div>
        </div>
    </div>
</div>