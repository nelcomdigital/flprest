@isset($module)

    @include('cms.layouts.blocks.input.select',['module'=>$module, 'name'=>'domains[]', 'id'=>'domains', 'label'=>'Domains', 'required'=>true, 'placeholder'=>'Domains', 'options'=>$domains, 'multiple'=>true , 'selected'=>(isset($selected_domains))? $selected_domains : null ])
    @include('cms.layouts.blocks.input.select',['module'=>$module, 'name'=>'countries[]', 'id'=>'countries', 'label'=>'Countries', 'required'=>true, 'options'=>$countries, 'multiple'=>true, 'selected'=>(isset($selected_countries))? $selected_countries : null])
    @include('cms.layouts.blocks.input.select',['module'=>$module, 'name'=>'locales[]', 'id'=>'locales', 'label'=>'Locales', 'required'=>true, 'options'=>$locales, 'multiple'=>(isset($selected_locales))? true : false, 'selected'=>(isset($selected_locales))? $selected_locales : null])
@else

    @include('cms.layouts.blocks.input.select',['name'=>'domains[]', 'id'=>'domains', 'label'=>'Domains', 'required'=>true, 'placeholder'=>'Domains', 'options'=>$domains, 'multiple'=>true , 'selected'=>(isset($selected_domains))? $selected_domains : null ])
    @include('cms.layouts.blocks.input.select',['name'=>'countries[]', 'id'=>'countries', 'label'=>'Countries', 'required'=>true, 'options'=>$countries, 'multiple'=>true, 'selected'=>(isset($selected_countries))? $selected_countries : null])
    @include('cms.layouts.blocks.input.select',['name'=>'locales[]', 'id'=>'locales', 'label'=>'Locales', 'required'=>true, 'options'=>$locales, 'multiple'=>(isset($selected_locales))? true : false, 'selected'=>(isset($selected_locales))? $selected_locales : null])
@endisset
