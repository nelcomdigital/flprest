<div class="item form-group">
    <label class="control-label col-sm-2 col-xs-12">{{__('cms.'.str_slug($label))}}@if(isset($required) and $required)<span class="required">*</span> @endif</label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <select class="select2 form-control @if(isset($required) and $required) required @endif" name="{{$name}}" id="{{$id}}" @if((isset($multiple) and $multiple) || (isset($selected) and is_array($selected))) multiple @endif @isset ($error) data-error-message="{{$error}}" @endisset >
            @if(!isset($multiple) or !$multiple)
                <option value="">Select {{$label}}</option>
            @endif
            @if(isset($options))
                @foreach ($options as $value)
                    <option value="{{$value->id}}"
                    @if (isset($selected) and $selected != null)
                        @if((isset($multiple) and $multiple))
                            @foreach ($selected as $slct)
                                {{ ($slct->id == $value->id)? 'selected' : ''}}
                                @endforeach
                            @else
                            {{ ($selected->id == $value->id)? 'selected' : ''}}
                            @endif
                        @endif
                    >{{$value->name}}</option>
                @endforeach
            @endif
            @if(isset($custom_options))
                @foreach ($custom_options as $value)
                    <option value="{{(array_has($value, 'value'))? $value['value'] : $value['slug']}}"
                    @if (isset($selected) and $selected != null)
                        @if((isset($multiple) and $multiple))
                            @foreach ($selected as $slct)
                                @if(array_has($value, 'value'))
                                    {{ ($slct == $value['value'])? 'selected' : ''}}
                                    @else
                                    {{ ($slct == $value['slug'])? 'selected' : ''}}
                                    @endif
                                @endforeach
                            @else
                            @if(array_has($value, 'value'))
                                {{ ($selected == $value['value'])? 'selected' : ''}}
                                @else
                                {{ ($selected == $value['slug'])? 'selected' : ''}}
                                @endif
                            @endif
                        @endif
                    > {!!$value['name'] !!}</option>
                @endforeach
            @endif
        </select>
    </div>
</div>
<div class="item form-group">
    <label class="control-label col-sm-2 col-xs-12">{{__('cms.'.str_slug($sub_label))}}@if(isset($sub_required) and $sub_required)<span class="required">*</span> @endif</label>

    <div class="col-md-6 col-sm-6 col-xs-12">
        <select class="select2 form-control @if(isset($sub_required) and $sub_required) required @endif" name="{{$sub_name}}" id="{{$sub_id}}" @if(isset($sub_multiple) and $sub_multiple ) multiple @endif @isset ($sub_error) data-error-message="{{$sub_error}}" @endisset >
            @if(!isset($sub_multiple) or !$sub_multiple)
                <option value="">Select {{$sub_label}}</option>
            @endif
            @if(isset($sub_options))
                @foreach ($sub_options as $value)
                    <option value="{{$value->id}}"
                        @if (isset($sub_selected) and $sub_selected != null)
                            @if((isset($sub_multiple) and $sub_multiple))
                                @foreach ($sub_selected as $key=>$slct)
                                    {{ ($slct == $value->id)? 'selected' : ''}}
                                @endforeach
                            @elseif((!isset($sub_multiple) or !$sub_multiple) )
                                    @if(is_array($sub_selected) and sizeof($sub_selected)>0)
                                {{ ($sub_selected[0] == $value->id)? 'selected' : ''}}
                                    @else
                                    {{ ($sub_selected == $value->id)? 'selected' : ''}}
                                    @endif
                                @endif
                        @endif
                        >{{$value->name}}</option>
                @endforeach
            @endif

        </select>
    </div>
</div>


<script>
    // $('.select2').select2({
    //     width: '100%',
    // });

        $('#{{$id}}').on('change', function() {
            var id = $(this).val();
            var dataString = '?id=' + id;
            $("#{{$sub_id}}").val(null);
            $("#{{$sub_id}}").trigger('change');
            $('#{{$sub_id}}').select2({
                width: '100%',
                placeholder: '{{$sub_label}}',
                ajax: {
                    url: '{{$route}}' + dataString,
                    dataType: 'json',
                    processResults: function (data) {
                        return {
                            results: $.map(data, function (key) {
                                return {id: key._id, text:  key.name};
                            })
                        };
                    },
                    cache: true
                }
            });
        });
</script>
