<div class="clearfix"></div>
<div class="ln_solid"></div>
<div class="form-group">
    <div class="col-md-12">
        @isset($submit)
            <button type="submit" class="btn btn-success btn-save" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Submitting" data-form="{{$formId}}" data-url="{{$submit}}" data-type="{{$type}}">{{__('cms.submit')}}</button>
        @endisset
    </div>
</div>
