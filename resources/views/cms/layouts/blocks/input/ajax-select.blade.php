<div class="item form-group">
    <label class="control-label col-sm-2 col-xs-12">{{__('cms.'.str_slug($label))}}@if(isset($required) and $required)<span class="required">*</span> @endif</label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <select type="text" class="select2 select2-ajax form-control @if(isset($required) and $required) required @endif"
        @isset($model) data-route="{{route('cms.select.ajax', $model)}}" @endif data-name="{{$label}}"
        name="{{$name}}" id="{{$id}}" @if((isset($multiple) and $multiple) || (isset($selected) and is_array($selected))) multiple @endif @isset ($error) data-error-message="{{$error}}" @endisset >
            @if(!isset($multiple) or !$multiple)
                <option value="">Select {{$label}}</option>
            @endif

            @if (isset($selected) and $selected != null)
                @if((isset($multiple) and $multiple))
                    @foreach ($selected as $value)
                        <option value="{{$value->id}}" selected>{!! mb_convert_encoding($value->name, "UTF-8", "HTML-ENTITIES") !!}</option>
                    @endforeach
                @else
                    <option value="{{$selected->id}}" selected> {!! mb_convert_encoding($selected->name, "UTF-8", "HTML-ENTITIES") !!}</option>
                @endif
            @endif

        </select>
    </div>
</div>

