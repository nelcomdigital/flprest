<div class="item form-group">
    <label class="control-label col-sm-2 col-xs-12">{{__('cms.'.str_slug($label))}}@if(isset($required) and $required)<span class="required">*</span> @endif</label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <select class="select2 form-control @if(isset($required) and $required) required @endif" name="{{$name}}" id="{{$id}}" @if((isset($multiple) and $multiple) || (isset($selected) and is_array($selected))) multiple @endif @isset ($error) data-error-message="{{$error}}" @endisset >
            @if(!isset($multiple) or !$multiple)
                <option value="">Select {{$label}}</option>
            @endif
            @if(isset($options))
                @foreach ($options as $value)
                    <option value="{{$value->id}}"
                    @if (isset($selected) and $selected != null)
                        @if((isset($multiple) and $multiple))
                            @foreach ($selected as $slct)
                                {{ ($slct->id == $value->id)? 'selected' : ''}}
                                @endforeach
                            @else
                            {{ ($selected->id == $value->id)? 'selected' : ''}}
                            @endif
                        @endif
                    >
                        @if( is_string($value->name)) {{$value->name}} @else @if ( Arr::has($value->name, session()->get('current_locale','fr'))){{$value->name[session()->get('current_locale','fr')]}} @else {{Arr::first($value->name)}} @endif @endif </option>
                @endforeach
            @endif
            @if(isset($custom_options))
                @foreach ($custom_options as $value)
                    <option value="{{(Arr::has($value, 'value'))? $value['value'] : $value['slug']}}"
                    @if (isset($selected) and $selected != null)
                        @if((isset($multiple) and $multiple))
                            @foreach ($selected as $slct)
                                @if(Arr::has($value, 'value'))
                                    {{ ($slct == $value['value'])? 'selected' : ''}}
                                    @else
                                    {{ ($slct == $value['slug'])? 'selected' : ''}}
                                    @endif
                                @endforeach
                            @else
                            @if(Arr::has($value, 'value'))
                                {{ ($selected == $value['value'])? 'selected' : ''}}
                                @else
                                {{ ($selected == $value['slug'])? 'selected' : ''}}
                                @endif
                            @endif
                        @endif
                    > {!!$value['name'] !!}</option>
                @endforeach
            @endif
        </select>
    </div>
</div>
