@php($imId = uniqid('file'))
<div class="item form-group">
    <label class="control-label col-sm-2 col-xs-12" for="{{$name}}">{{__('cms.'.Str::slug($label))}} @if(isset($required) and
        $required)<span class="required">*</span> @endif
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        
        <div class="image-input-box">
            <a href="javascript:void(0)" class="popup_selector" data-inputid="{{$imId.'_'.$name}}"
               data-type="file">{{__('cms.browse_file')}}</a>
            <input type="text" id="{{$imId.'_'.$name}}" name="{{$name}}" value="@isset($value){{$value}}@endisset"
                   readonly @isset ($error) data-error-message="{{$error}}" @endisset >
        </div>
    </div>
</div>

