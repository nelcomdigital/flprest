<div class="item form-group">
    <label class="control-label col-sm-2 col-xs-12">{{__('cms.'.str_slug($label))}}@if(isset($required) and $required)<span class="required">*</span> @endif</label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <select class="select2 select2-tags form-control @if(isset($required) and $required) required @endif" name="{{$name}}" id="{{$id}}" @if((isset($multiple) and $multiple) || (isset($selected) and is_array($selected))) multiple @endif @isset ($error) data-error-message="{{$error}}" @endisset >
            @if(!isset($multiple) or !$multiple)
                <option value="">Select {{$label}}</option>
            @endif
            @if(isset($options))
                @foreach ($options as $value)
                    <option value="{{$value->id}}" 
                        @if (isset($selected) and $selected != null)
                            @if((isset($multiple) and $multiple))
                                @foreach ($selected as $slct)
                                    {{ ($slct->id == $value->id)? 'selected' : ''}}
                                @endforeach
                            @else
                                {{ ($selected->id == $value->id)? 'selected' : ''}}
                            @endif
                        @elseif( isset($module ) and getFieldConfig($module, $name) != null )
                            @php($c_select = getFieldConfig($module, $name) )
                            @foreach ($c_select as $slct)
                                {{ ($slct == $value->id)? 'selected' : ''}}
                                @endforeach
                        @endif
                        > {!!$value->name !!}</option>
                @endforeach
            @endif
        </select>
    </div>
</div>
<script>


</script>
