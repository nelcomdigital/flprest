<div class="item form-group">
    <div class="col-md-12">
        <div class="timing-tabs">
            @foreach ($fields as  $field)
                    <div class="day-name">
                        {{$field['label']}}
                    </div>
                    <div class="day-switch">
                        <label class="switch">
                            <input type="checkbox" class="time-switch" value="1"  name="{{$field['name']}}"
                                   @if(isset($value) and $value->{$field['name']} ) checked @endif>
                            <span class="slider round"></span>
                        </label>
                    </div>
            @endforeach
        </div>
    </div>
</div>
