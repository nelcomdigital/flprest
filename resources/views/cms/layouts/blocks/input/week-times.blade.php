@php($tId = uniqid())
<div class="item form-group">
    <div class="col-md-12">
        <div class="timing-tabs" id="{{$tId}}">
            @foreach ($days as $key => $day)
                <div class="row time-row" id="time-{{$key+1}}">
                    <div class="day-name">
                        {{$day}}
                    </div>
                    <div class="day-switch">
                        <label class="switch">
                            <input type="checkbox" class="time-switch" value="1"  name="closed[{{$key+1}}]" data-id="{{$key+1}}" @if(isset($value) and isset($value[$key+1]) and !isset($value[$key+1]['closed'])) checked @endif>
                            <span class="slider round"></span>
                        </label>
                    </div>
                    <div class="day-times" @if(isset($value) and isset($value[$key+1]) and !isset($value[$key+1]['closed'])) style="display:inline-flex;" @endif>
                        <div class="times">
                            @if(isset($value) and isset($value[$key+1]) and !isset($value[$key+1]['closed']))
                                @foreach ($value[$key+1] as $time)
                                    <div class="inline-time">
                                        <div class="input-time start-time">
                                            <input type="text" class="{{$tId}}_update-time" value="{{$time['start_time']}}" name="start_time[{{$key+1}}][]"/>
                                        </div>
                                        <div class="time-seperate">-</div>
                                        <div class="input-time end-time">
                                            <input type="text" class="{{$tId}}_update-time" value="{{$time['end_time']}}" name="end_time[{{$key+1}}][]"/>
                                        </div>
                                        <div class="delete-time" @if(count($value[$key+1]) > 1 ) style="display:block" @endif>
                                            <i class="fa fa-close"></i>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <div class="inline-time">
                                    <div class="input-time start-time">
                                        <input type="text" class="{{$tId}}_update-time" value="9:00" name="start_time[{{$key+1}}][]"/>
                                    </div>
                                    <div class="time-seperate">-</div>
                                    <div class="input-time end-time">
                                        <input type="text" class="{{$tId}}_update-time" value="19:00" name="end_time[{{$key+1}}][]"/>
                                    </div>
                                    <div class="delete-time">
                                        <i class="fa fa-close"></i>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="add-time" data-id="{{$key+1}}" @if(isset($value) and isset($value[$key+1]) and count($value[$key+1]) == 3  and !isset($value[$key+1]['closed'])) style="display:none" @endif>
                            {{__('cms.add_hours')}}
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
<script>
    $(function(){
        $('.{{$tId}}_update-time').clockpicker({
            autoclose: true,
            placement: 'top',
            align: 'left',
            donetext: 'Done'
        });
    })
</script>