@php($dId = uniqid())
<div class="item form-group">
    <label class="control-label col-sm-2 col-xs-12" for="{{$name}}">{{__('cms.'.str_slug($label))}} @if(isset($required) and
        $required)<span class="required">*</span> @endif
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="text" class="form-control has-feedback-left picker-date_{{$dId}}" name="{{$name}}" id="{{$id}}" placeholder="{{$placeHolder}}" aria-describedby="inputSuccess2Status" @isset ($error) data-error-message="{{$error}}" @endisset @if(isset($required) and $required) required="required" @endif
        >
        <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
    </div>
</div>
<script>
    $('.picker-date_{{$dId}}').daterangepicker({
        singleDatePicker: true,
        timePicker: true,
        @if(isset($value) and $value != null)
        startDate: '{{$value}}',
        @else
        startDate: moment(),
        @endif
        singleClasses: "picker_1",
        autoUpdateInput: true,
        locale: {
            format: 'dd-MM-YYYY HH:mm'
        }
    }, function(start, end, label) {
        console.log(start.toISOString(), end.toISOString(), label);
    });
</script>
