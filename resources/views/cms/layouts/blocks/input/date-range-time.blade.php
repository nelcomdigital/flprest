@php($uId = uniqid())
<div class="item form-group">
    <label class="control-label col-sm-12" >{{__('cms.'.str_slug($label))}}</label>
</div>
<div class="row">
    <div class="dynamic-time-inputs">
        <div class="time-range-input">
            <div class="col-sm-6">
                <div class="item form-group">
                    <label class="control-label col-sm-4 col-xs-12" >{{__('cms.start_date')}}</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control has-feedback-left picker-date" name="start_date[]" id="start_date" placeholder="Start Date" aria-describedby="inputSuccess2Status">
                        <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="item form-group">
                    <label class="control-label col-sm-4 col-xs-12" >{{__('cms.end_date')}}</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control has-feedback-left picker-date" name="end_date[]" id="end_date" placeholder="End Date" aria-describedby="inputSuccess2Status">
                        <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <label class="control-label col-sm-12">
        <div class="add-input add-time-input">
            <div class="plus">+</div><span>{{$add}}</span>
        </div>
    </label>
</div>
<script>
    $('.picker-date').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_1",
        autoUpdateInput: true
    }, function(start, end, label) {
        console.log(start.toISOString(), end.toISOString(), label);
    });
</script>