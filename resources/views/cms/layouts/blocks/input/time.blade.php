@php($tId = uniqid())
<div class="item form-group">
    <div class="col-md-12">
        <div class="timing-tabs" id="{{$tId}}">
            <div class="full-tab">
                <div class="full-tab-head" onclick="openTimeTab(this ,1)" data-target="1">
                    <span class="full-tab-title">{{__('cms.Time')}}</span>
                    <span class="full-tab-arrow"><i class="fa fa-angle-down"></i></span>
                </div>
                <div class="full-tab-content" data-id="1">
                    <div class="timing-div">
                        @if(isset($value) and array_has($value, '1'))
                            @foreach ($value[1] as $val)
                                <div class="time-item">
                                    <div class="input-time start-time">
                                        <label>
                                            <span class="glyphicon glyphicon-time"></span>
                                            {{__('cms.start_time')}}
                                        </label>
                                        <input type="text" class="update-time" name="start_time[1][]" value="{{$val['start_time']}}"/>
                                    </div>
                                    <div class="input-time end-time">
                                        <label>
                                            <span class="glyphicon glyphicon-time"></span>
                                            {{__('cms.end_time')}}
                                        </label>
                                        <input type="text" class="update-time" name="end_time[1][]" value="{{$val['end_time']}}"/>
                                    </div>
                                    <i class="fa fa-close close-time"></i>
                                </div>
                            @endforeach
                        @else
                            <div class="time-item">
                                <div class="input-time start-time">
                                    <label>
                                        <span class="glyphicon glyphicon-time"></span>
                                        {{__('cms.start_time')}}
                                    </label>
                                    <input type="text" class="update-time" name="start_time[1][]"/>
                                </div>
                                <div class="input-time end-time">
                                    <label>
                                        <span class="glyphicon glyphicon-time"></span>
                                        {{__('cms.end_time')}}
                                    </label>
                                    <input type="text" class="update-time" name="end_time[1][]"/>
                                </div>
                                <i class="fa fa-close close-time"></i>
                            </div>
                        @endif
                    </div>
                    <div class="add-input add-time-btn" data-id="{{$tId}}" data-target="1">
                        <div class="plus">+</div><span class="black">{{__('cms.add_timing')}}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function(){
        $('.update-time').clockpicker({
            autoclose: true,
            placement: 'top',
            align: 'left',
            donetext: 'Done'
        });
    })
</script>