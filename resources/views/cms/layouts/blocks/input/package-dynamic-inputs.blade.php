@php($uId = uniqid())

<div class="item form-group">
    <label class="control-label col-sm-12 col-xs-12">Timeline</label>
    <div id="{{$uId}}_main_container" class="col-md-12">

    </div>

    <div class="col-md-12">
        <br/>
        <div class="add-input-new" onclick="addDay('{{$uId}}')">
            <div class="plus">+</div>
            <span>Day</span>
        </div>
    </div>
</div>
<script>
    let packageTimeLineCounter{{$uId}} = 1;
    let packageDayCounter{{$uId}} = 1;

    function addDay(id, data = null) {
        let rand = packageDayCounter{{$uId}}++;
        let val = '';
        let description = '';
        if (data != null) {
            val = data['day'];
            try {
                description = data['description'];
            } catch (e) {

            }
        }

        let container =
            '<div class="dynamic-input" id="' + rand + '_day_inputs">\n' +
            '   <div class="item form-group">\n' +
            '       <label class="control-label col-md-2 col-xs-12" for="day_name">Day Title<span class="required">*</span></label>\n' +
            '       <div class="col-md-6 col-sm-6 col-xs-12">\n' +
            '           <input class="form-control col-md-7 col-xs-12 " type="text" name="object[' + rand + '][day]" placeholder="Day Name"  required value="' + val + '"/>\n' +
            '       </div>\n' +
            '<div class="col-md-3">' +
            '   <div class="delete-input-new" onclick="removeDay(' + rand + ')">- Day</div>\n' +
            '   </div>\n' +
            '   </div>\n' +
            '<div class="item form-group">\n' +
            '<label class="control-label col-sm-2 col-xs-12" for="timeline_time">Description<span class="required">*</span></label>\n' +
            '<div class="col-md-6 col-sm-6 col-xs-12">\n' +
            '<textarea  class="form-control col-md-7 col-xs-12 " name="object[' + rand + '][description]" placeholder="" >' + description + '</textarea>\n' +
            '</div>' +
            '</div>' +
            '<div id="' + rand + '_day_content" class="col-md-12"></div> ' +
            '   <div class = "add-input-new" onclick="addTimeLine(\'' + id + '\',' + rand + ')"> <div class= "plus" > + Add TimeLine </div>' +
            '</div> ' +
            '</div><hr><hr>';

        $('#' + id + '_main_container').append(container);
        if (data != null) {
            for (let t in data['timeline']) {
                addTimeLine(id, rand, data['timeline'][t]);
            }
        } else
            addTimeLine(id, rand);


    }

    function addTimeLine(id, rand, data = null) {
        let inRand = packageTimeLineCounter{{$uId}}++;
        let time = '';
        let description = '';
        let venues = '';
        let cities = '';
        let notes = '';
        let location = '';
        if (data != null) {
            time = data['time'] !== undefined ? data['time'] : '';
            description = data['description'] !== undefined ? data['description'] : '';
            notes = data['notes'] !== undefined ? data['notes'] : '';
            location = data['location'] !== undefined ? data['location'] : '';

            try {
                for (v in data['venues']) {
                    venues += '<option value="' + data['venues'][v]['_id'] + '" selected >' + data['venues'][v]['name'] + '</option>';
                }
            } catch (e) {

            }

            try {
                for (c in data['cities']) {
                    cities += '<option value="' + data['cities'][c]['_id'] + '" selected >' + data['cities'][c]['name'] + '</option>';
                }
            } catch (e) {

            }

        }
        let container =
            '<div class="all-inline-inputs" id="' + rand + '_' + inRand + '_timeline">\n' +

            '<div class="item form-group">\n' +
            '<label class="control-label col-sm-2 col-xs-12" for="timeline_time">Time<span class="required">*</span></label>\n' +
            '<div class="col-md-6 col-sm-6 col-xs-12">\n' +
            '<input class="form-control col-md-7 col-xs-12  update-time" type="time" name="object[' + rand + '][' + inRand + '][time]" placeholder="Time" id="' + rand + '_' + inRand + '_time" value="' + time + '"/>\n' +
            '</div>' +
            '<div class="col-md-2"><div class="delete-input-new" onclick="removeTimeLine(' + rand + ',' + inRand + ')">- Timeline</div></div>' +
            '</div>' +
            '<div class="item form-group">\n' +
            '<label class="control-label col-sm-2 col-xs-12" for="timeline_time">Description<span class="required">*</span></label>\n' +
            '<div class="col-md-6 col-sm-6 col-xs-12">\n' +
            '<textarea  class="form-control col-md-7 col-xs-12 " name="object[' + rand + '][' + inRand + '][description]" placeholder="" id="' + rand + '_' + inRand + '_description" >' + description + '</textarea>\n' +
            '</div>' +
            '</div>' +
            '<div class="item form-group">' +
            '<label class="control-label col-sm-2 col-xs-12">Venues</label>' +
            '<div class="col-md-6 col-sm-6 col-xs-12">' +
            '<select type="text" class="select2 select2-ajax form-control" data-route="{{route('cms.select.published.ajax', 'Venue')}}" data-name="name" name="object[' + rand + '][' + inRand + '][venues][]" id="' + rand + '_' + inRand + '_venues" multiple >' +
            venues +
            '</select>' +
            '</div>' +
            '</div>' +
            '<div class="item form-group">' +
            '<label class="control-label col-sm-2 col-xs-12">Cities</label>' +
            '<div class="col-md-6 col-sm-6 col-xs-12">' +
            '<select type="text" class="select2 select2-ajax form-control" data-route="{{route('cms.select.ajax', 'City')}}" data-name="name" name="object[' + rand + '][' + inRand + '][cities][]" id="' + rand + '_' + inRand + '_cities" multiple >' +
            cities +
            '</select>' +
            '</div>' +
            '</div>' +
            '<div class="item form-group">\n' +
            '<label class="control-label col-sm-2 col-xs-12" for="timeline_time">Notes</label>\n' +
            '<div class="col-md-6 col-sm-6 col-xs-12">\n' +
            '<textarea  class="form-control col-md-7 col-xs-12 " name="object[' + rand + '][' + inRand + '][notes]" placeholder="Notes" >' + notes + '</textarea>\n' +
            '</div>' +
            '</div>' +
            '   <div class="item form-group">\n' +
            '       <label class="control-label col-sm-2 col-xs-12" for="day_name">Locations</label>\n' +
            '       <div class="col-md-6 col-sm-6 col-xs-12">\n' +
            '           <input class="form-control col-md-7 col-xs-12 " type="text" name="object[' + rand + '][' + inRand + '][location]" placeholder="Location"  value="' + location + '"/>\n' +
            '       </div>\n' +
            '   </div>\n<hr>' +
            '</div>';
        $('#' + rand + '_day_content').append(container);
        init_custom_select_one(rand + '_' + inRand + '_cities');
        init_custom_select_one(rand + '_' + inRand + '_venues');
        init_time_picker(rand + '_' + inRand + '_time');
        $('.select2_' + id).select2({
            width: '100%'
        });


    }


    function init_custom_select_one_class(id) {

        $('.' + id).each(function (key, value) {
            if ($(value).hasClass('select2-ajax') && $(value).attr('data-route') !== undefined) {
                let route = $(value).attr('data-route');
                let name = $(value).attr('data-name');
                if ($(value).attr('data-flag') !== undefined) {
                    let flag = $(id).attr('data-search');
                    route = route + '?search=' + flag;
                }
                $(value).select2({
                    width: '100%',
                    placeholder: 'Select ' + name,
                    ajax: {
                        url: route,
                        dataType: 'json',
                        processResults: function (data) {
                            results = [];
                            if (data.label != null) {
                                results.push({id: "", text: 'Select ' + data.label})
                            } else {
                                results.push({id: "", text: 'Select ' + name})
                            }
                            data = $.map(data.data, function (obj) {
                                return {id: obj['_id'], text: obj['name']};
                            })
                            $.each(data, function (key, value) {
                                results.push(value)
                            })
                            return {
                                results: results
                            };
                        },
                        cache: true
                    }
                });
            }
        });


    };

    function init_custom_select_one(id) {
        let route = $('#' + id).attr('data-route');
        let name = $('#' + id).attr('data-name');
        if ($('#' + id).attr('data-flag') !== undefined) {
            let flag = $(id).attr('data-search');
            route = route + '?search=' + flag;
        }
        $('#' + id).select2({
            width: '100%',
            placeholder: 'Select ' + name,
            ajax: {
                url: route,
                dataType: 'json',
                processResults: function (data) {
                    results = [];
                    if (data.label != null) {
                        results.push({id: "", text: 'Select ' + data.label})
                    } else {
                        results.push({id: "", text: 'Select ' + name})
                    }
                    data = $.map(data.data, function (obj) {
                        return {id: obj['_id'], text: obj['name']};
                    })
                    $.each(data, function (key, value) {
                        results.push(value)
                    })
                    return {
                        results: results
                    };
                },
                cache: true
            }
        });

    };


    function removeDay(rand) {
        $('#' + rand + '_day_inputs').remove();
    }

    function removeTimeLine(rand, inRand) {
        $('#' + rand + '_' + inRand + '_timeline').remove();
    }

        @isset( $timeline)
        @foreach( $timeline as $time)
    let data{{$loop->index}} = {
            day: '{{$time['day']}}',
            description: '@if(array_has($time,'description')){{$time['description']}}@else {{''}} @endif',
            timeline: [
                @foreach( $time['timeline'] as $line)
                @if ( $loop->index > 0 ), @endif
                {
                    time: '{{$line['time']}}',
                    description: '{{$line['description']}}',
                    notes: '{{$line['notes']}}',
                    location: '{{$line['location']}}',
                    venues: [
                        @foreach( $line['venues'] as $venue )
                        @if ( $loop->index > 0 ), @endif
                        {
                            _id: '{{$venue['_id']}}',
                            name: '{{$venue ['name']}}'
                        }
                        @endforeach
                    ],
                    cities: [
                        @foreach( $line['cities'] as $city )
                        @if ( $loop->index > 0 ), @endif
                        {
                            _id: '{{$city['_id']}}',
                            name: '{{$city ['name']}}'
                        }
                        @endforeach
                    ]
                }
                @endforeach
            ]
        };
    addDay('{{$uId}}', data{{$loop->index}});
    @endforeach

    @endisset
</script>
