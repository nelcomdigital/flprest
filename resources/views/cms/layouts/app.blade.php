<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>@if(isset($settings['title'])) {{ $settings['title']}}
        @else{{ config('app.name', 'Laravel') }}@endif</title>

    <link rel="shortcut icon" type="image/x-icon" href="@if(isset($settings['favicon'])) {{asset($settings['favicon'])}} @else  {{asset('images/favicon.ico')}} @endif">

    <!-- Bootstrap -->
    <link href="{{asset('vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{asset('vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- Select2 -->
    <link href="{{asset('vendors/select2/dist/css/select2.min.css')}}" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="{{asset('vendors/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">
    <!-- bootstrap-datetimepicker -->
    <link href="{{asset('vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css')}}" rel="stylesheet">
    <!-- jQuery custom content scroller -->
    <link href="{{asset('vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css')}}" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jq-3.3.1/jszip-2.5.0/dt-1.10.18/af-2.3.2/b-1.5.4/b-colvis-1.5.4/b-flash-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.5.0/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.min.css"/>

    <link href="{{asset('css/cms/scrolltabs.css')}}" rel="stylesheet">
    <link href="{{asset('css/colorbox.css')}}" rel="stylesheet">
    <link href="{{asset('css/cms/clockpicker.css')}}" rel="stylesheet">
    <link href="{{asset('css/cms/loader.less')}}" rel="stylesheet/less">
    <!-- Fine Uploader JS file
    ====================================================================== -->
    <script src="{{ asset('js/fine-uploader.js')}}"></script>
    <link href="{{ asset('css/filebrowser.css') }}" rel="stylesheet">
    <link href="{{asset('css/cms/cms.css')}}" rel="stylesheet">

    <link href="{{ asset('css/fine-uploader-new.css')}}" rel="stylesheet">





</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        @include('cms.layouts.partials.header')
        @include('cms.layouts.partials.side-menu')

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="col-md-12 padding-0">
                <div class="" role="tabpanel" data-example-id="togglable-tabs">
                    <ul id="inter_tabs" class="nav nav-tabs bar_tabs fixed" role="tablist">

                    </ul>
                    <div id="inter_tabs_frames" class="tab-content">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->

        @include('cms.layouts.partials.footer')
    </div>
</div>

<div class="modal fade" id="closeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-head-orange"></div>
            <div class="modal-body delete-modal-body">
                <h4>Are you sure you want to close the page?</h4>
                <div class="modal-btns">
                    <button type="button" class="btn" id="confirmClose">Yes</button>
                    <button type="button" class="btn" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- jQuery -->
<script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/less.js/2.5.1/less.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>

{{--<script type="text/javascript" src="https://cdn.datatables.net/v/bs-3.3.7/jq-3.3.1/jszip-2.5.0/dt-1.10.18/af-2.3.2/b-1.5.4/b-colvis-1.5.4/b-flash-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.5.0/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.min.js"></script>--}}
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<!-- Bootstrap -->
 <script src="{{asset('vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/cms/gallery-albums.js')}}"></script>

<script src="{{asset('js/cms/clockpicker.js')}}"></script>
<script src="{{asset('js/cms/cms.js')}}"></script>
<script src="{{asset('js/cms/jquery-functions.js')}}"></script>
<script src="{{ asset('js/dbfilebrowser.js') }}"></script>
<script src="{{asset('js/cms/custom.js')}}"></script>
<script src="{{asset('js/cms/jquery.scrolltabs.js')}}"></script>
<script src="{{asset('js/cms/jquery.mousewheel.js')}}"></script>

<!-- validator -->
<script src="{{asset('vendors/validator/validator.js')}}"></script>
<!-- Select2 -->
<script src="{{asset('vendors/select2/dist/js/select2.full.min.js')}}"></script>
<!-- bootstrap-daterangepicker -->
<script src="{{asset('vendors/moment/min/moment.min.js')}}"></script>
<script src="{{asset('vendors/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<!-- bootstrap-datetimepicker -->
<script src="{{asset('vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')}}"></script>
<!-- jQuery custom content scroller -->
<script src="{{asset('vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js')}}"></script>


<script src="{{asset('packages/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.colorbox-min.js')}}"></script>

<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
<script>jQ = jQuery.noConflict(true);</script>
<script src="{{ asset('js/filebrowser.js') }}"></script>

<meta name="_token_images" content="{{ csrf_token() }}">

<script>

    //elfinder
    function processSelectedFile(e,t){
        $("#"+t).val(e.replace('public/','storage/'));
        var type = $('#'+t).parent().find('.popup_selector').attr('data-type');
        if(type != "" && type != undefined && type == "image"){
            // document.getElementById(t+'-display').src = e.replace('public/','/public/storage/');
            document.getElementById(t+'-display').src = e.replace('public/','storage/');
            $('#'+t+'-display').parent().show();
        }
    }
    $(document).on("click",".popup_selector",function(e){
        e.preventDefault();
        var t=$(this).attr("data-inputid");
        var n="/elfinder/popup/";
        var type = $(this).attr('data-type');
        var r=n+t;

        if(type != "" && type != undefined){
            r += "?type="+type;
        }

        $.colorbox({href:r,fastIframe:true,iframe:true,width:"70%",height:"70%"})
    });


    var hidWidth;
    var scrollBarWidths = 40;

     function widthOfList (){
        var itemsWidth = 0;
        $('.scroll_tabs_container li').each(function(){
            var itemWidth = $(this).outerWidth();
            itemsWidth+=itemWidth;
        });
        return itemsWidth;
    };

     function widthOfHidden(){
        return (($('.scroll_tabs_container').outerWidth())-widthOfList()-getLeftPosi())-scrollBarWidths;
    };

     function getLeftPosi (){
        return $('.scroll_tabs_container').position().left;
    };

     function reAdjust (){
        if ($('.scroll_tabs_container').outerWidth() < widthOfList()) {
            $('.scroll_tab_right_button').show();
        }
        else {
            $('.scroll_tab_right_button').hide();
        }

        if (getLeftPosi()<0) {
            $('.scroll_tab_left_button').show();
        }
        else {
            $('.scroll_tab_left_button').hide();
        }
    }

    reAdjust();

    $(window).on('resize',function(e){
        reAdjust();
    });

    $('.scroll_tab_right_button').click(function() {

        $('.scroll_tab_left_button').fadeIn('slow');
        $('.scroll_tab_right_button').fadeOut('slow');

        $('.scroll_tabs_container').animate({left:"+="+widthOfHidden()+"px"},'slow',function(){

        });
    });

    $('.scroll_tab_left_button').click(function() {

        $('.scroll_tab_right_button').fadeIn('slow');
        $('.scroll_tab_left_button').fadeOut('slow');

        $('.scroll_tabs_container').animate({left:"-="+getLeftPosi()+"px"},'slow',function(){

        });
    });

</script>



@stack('js')

</body>
</html>
