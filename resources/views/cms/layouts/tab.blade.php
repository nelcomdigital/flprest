<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>CMS Moovtoo</title>

    <!-- Bootstrap -->
    <link href="{{asset('vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{asset('vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- Select2 -->
    <link href="{{asset('vendors/select2/dist/css/select2.min.css')}}" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="{{asset('vendors/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">
    <!-- bootstrap-datetimepicker -->
    <link href="{{asset('vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css')}}" rel="stylesheet">

    <link href="{{asset('css/cms/cms.css')}}" rel="stylesheet">
    <link href="{{asset('css/colorbox.css')}}" rel="stylesheet">

</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        @yield('content')
    </div>
</div>

<!-- jQuery -->
<script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap -->
<script src="{{asset('vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- validator -->
<script src="{{asset('vendors/validator/validator.js')}}"></script>
<!-- Select2 -->
<script src="{{asset('vendors/select2/dist/js/select2.full.min.js')}}"></script>
<!-- bootstrap-daterangepicker -->
<script src="{{asset('vendors/moment/min/moment.min.js')}}"></script>
<script src="{{asset('vendors/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<!-- bootstrap-datetimepicker -->    
<script src="{{asset('vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')}}"></script>

<script src="{{asset('packages/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.colorbox-min.js')}}"></script>
<script src="{{asset('js/cms/cms.js')}}"></script>

<script>
    // $(function(){
    //     $('.select2').select2({
    //         width: '100%'
    //     });
    // })

    //elfinder
    function processSelectedFile(e,t){
        $("#"+t).val(e);
        document.getElementById(t+'-display').src = e.replace('public','/public/storage');
        $('#'+t+'-display').parent().show();
    }
    $(document).on("click",".popup_selector",function(e){
        e.preventDefault();
        var t=$(this).attr("data-inputid");
        var n="/elfinder/popup/";
        var r=n+t;
        $.colorbox({href:r,fastIframe:true,iframe:true,width:"70%",height:"70%"})
    })
</script>

@stack('js')

</body>
</html>
