<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <style>
        input{
            width: 500px;
            margin-bottom: 20px;
        }
    </style>
</head>
<body>

<form action="{{route('cms.uploader')}}" method="post" enctype="multipart/form-data">
    @csrf
    <input type="text" name="collection_name" placeholder="Collection Name( table name )" required>
    <br/>
    <input type="text" name="collection_fields" placeholder="Collection Fields separated by , in the same order of csv file " required>
    <br/>
    <input type="file" name="csv_file" required>
    <br/>
    <input type="submit" name="Export" >
</form>

</body>

</html>
