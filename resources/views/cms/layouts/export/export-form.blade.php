<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<body>

<form action="{{route('cms.post.export.backup')}}" method="post" enctype="multipart/form-data">
    @csrf
    <table style="border: 0">
        <tr>
            <td><input type="checkbox" name="database" ></td>
            <td>Database {{ $name != null ? $name :'' }}</td>
            <td><input type="checkbox" name="storage" ></td>
            <td>Storage</td>
        </tr>
    </table>

    <input type="submit" name="Export" >
</form>

</body>

</html>
