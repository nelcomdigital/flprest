@php($id = uniqid('form'))
<div class="right_col full" role="main">
    <div class="">
        <div class="page-title">
            <div>
                <h3>{{__('cms.update')}}: {{$user->name}}</h3>
            </div>
        </div>
        <form class="form-horizontal form-label-left" id="{{$id}}" novalidate >
            @csrf
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel closed">
                        <div class="x_title">
                            <h2>{{__('cms.user_properties')}}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            
                            @include('cms.layouts.blocks.input.image',['name'=>'photo', 'id'=>'photo', 'label'=>'User Image', 'required'=>false, 'value'=>$user->photo, 'route'=>''.route('user.delete-image', $user->id)])

                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'name', 'id'=>'name', 'placeHolder'=>'Full Name', 'label'=>'Full Name','error'=>'This field is required', 'required'=>true, 'value'=>$user->name])

                            @include('cms.layouts.blocks.input.field',['type'=>'email', 'name'=>'email', 'id'=>'email', 'label'=>'Email', 'placeHolder'=>'Email', 'required'=>true, 'error'=>'This field is required', 'value'=>$user->email, 'readonly'=>true])

                            @include('cms.layouts.blocks.input.status',['module'=>'user', 'required'=>true,'selected'=>$user->status])
                            
                            @include('cms.layouts.blocks.input.select',['name'=>'roles[]', 'id'=>'roles', 'label'=>'Roles', 'required'=>true, 'options'=>$roles, 'multiple'=>true, 'error'=>'This field is required', 'selected'=>$user->roles])
                                
                        </div>
                    </div>
                </div>
            </div>
            @include('cms.layouts.blocks.input.buttons', ['formId'=>$id, 'submit' => ''.route('user.update', $user->id), 'type'=>'PUT'])
        </form>
    </div>
</div>