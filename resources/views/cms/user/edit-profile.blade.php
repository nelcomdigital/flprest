@php($id = uniqid('form'))
<div class="right_col full" role="main">
    <div class="">
        <div class="page-title">
            <div>
                <h3>{{__('cms.update')}}: {{$user->name}}</h3>
            </div>
        </div>

        <form class="form-horizontal form-label-left" id="{{$id}}" novalidate >
            @csrf
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>{{__('cms.user_properties')}}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'name', 'id'=>'name', 'placeHolder'=>'User Name', 'label'=>'User Name', 'required'=>true,'error'=>'This field is required',  'value'=>$user->name])
                            @include('cms.layouts.blocks.input.image',['name'=>'image', 'id'=>'image', 'label'=>'User Image', 'required'=>false, 'value'=>$user->image, 'route'=>''.route('user.delete-image', $user->id)])
                            @include('cms.layouts.blocks.input.field',['type'=>'password', 'name'=>'old_password', 'id'=>'old_password', 'placeHolder'=>'Current Password', 'label'=>'Current Password'])
                            @include('cms.layouts.blocks.input.field',['type'=>'password', 'name'=>'password', 'id'=>'password', 'placeHolder'=>'Password', 'label'=>'Password'])
                            @include('cms.layouts.blocks.input.field',['type'=>'password', 'name'=>'password_confirmation', 'id'=>'password_confirmation', 'placeHolder'=>'Confirm Password', 'label'=>'Confirm Password'])
                        </div>
                    </div>
                </div>
            </div>



            @include('cms.layouts.blocks.input.buttons', ['formId'=>$id, 'submit'=>''.route('user.update-profile', $user->id), 'type'=>'Post'])
        </form>

    </div>
</div>