@php($id = uniqid('form'))
<div class="right_col full" role="main">
    <div class="">
        <div class="page-title">
            <div>
                <h3>{{__('cms.edit_region')}}</h3>
            </div>
        </div>

        <form class="form-horizontal form-label-left" id="{{$id}}" novalidate>
            @csrf
            @foreach( \App\Models\Locale::all() as $locale)
                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>{{__('cms.region_properties')}} - {{$locale->name}} </h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'name['.$locale->code.']', 'id'=>'name', 'placeHolder'=>'Name', 'label'=>'Name','error'=>'This field is required', 'required'=>true, 'value'=>$region->name[$locale->code]])
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            @endforeach
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>{{__('cms.region_parent')}}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            @include('cms.layouts.blocks.input.select',['name'=>'country_id', 'id'=>'country_id', 'label'=>'Country', 'required'=>true, 'options'=>$countries, 'error'=>'This field is required', 'multiple'=>false, 'selected'=>(isset($region->country) ?$region->country : null)])

                        </div>
                    </div>
                </div>
            </div>

            @include('cms.layouts.blocks.input.buttons', ['formId'=>$id, 'submit' => ''.route('region.update', $region->id), 'type'=>'PUT'])
        </form>
    </div>
</div>
