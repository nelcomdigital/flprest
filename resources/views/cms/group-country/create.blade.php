@php($id = uniqid('form'))
<div class="right_col full" role="main">
    <div class="">
        <div class="page-title">
            <div>
                <h3>{{__('cms.create_group_country')}}</h3>
            </div>
        </div>

        <form class="form-horizontal form-label-left" id="{{$id}}" novalidate >
            @csrf
            @foreach( \App\Models\Locale::all() as $locale)
                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>{{__('cms.group_country_properties')}} - {{$locale->name}} </h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'name['.$locale->code.']', 'id'=>'name', 'placeHolder'=>'Name', 'label'=>'Name','error'=>'This field is required', 'required'=>true])


                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            @endforeach


            @include('cms.layouts.blocks.input.buttons', ['formId'=>$id, 'submit' => ''.route('group-country.store'), 'type'=>'POST'])
        </form>
    </div>
</div>
