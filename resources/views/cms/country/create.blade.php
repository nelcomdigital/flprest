@php($id = uniqid('form'))
<div class="right_col full" role="main">
    <div class="">
        <div class="page-title">
            <div>
                <h3>{{__('cms.create_country')}}</h3>
            </div>
        </div>

        <form class="form-horizontal form-label-left" id="{{$id}}" novalidate >
            @csrf
            @foreach( \App\Models\Locale::all() as $locale)
                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>{{__('cms.country_properties')}} - {{$locale->name}} </h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'name['.$locale->code.']', 'id'=>'name', 'placeHolder'=>'Name', 'label'=>'Name','error'=>'This field is required', 'required'=>true])
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            @endforeach
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>{{__('cms.country_group')}}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            @include('cms.layouts.blocks.input.select',['name'=>'group_country_id', 'id'=>'group_country_id', 'label'=>'Group Country', 'required'=>false, 'options'=>$groupCountries, 'error'=>'This field is required', 'multiple'=>false])
                            @include('cms.layouts.blocks.input.flag',['fields'=>[
                       ['name'=> 'has_postal','label'=> 'Postal Code Required'],
                       ]])
                            @include('cms.layouts.blocks.input.flag',['fields'=>[
                        ['name'=> 'has_id_number','label'=> 'Id Number Required'],
                        ]])
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>{{__('cms.country_parameter')}}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'products_code', 'id'=>'product_code', 'placeHolder'=>'Product code', 'label'=>'Product code','error'=>'This field is required', 'required'=>true,])
                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'country_code', 'id'=>'country_code', 'placeHolder'=>'Country code', 'label'=>'Country code','error'=>'This field is required', 'required'=>true,])
                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'region_code', 'id'=>'region_code', 'placeHolder'=>'Region code', 'label'=>'Region code','error'=>'This field is required', 'required'=>true])
                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'min_cc', 'id'=>'min_cc', 'placeHolder'=>'Minimum CC sponser', 'label'=>'Minimum CC sponser','error'=>'This field is required', 'required'=>true])
                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'product_type', 'id'=>'product_type', 'placeHolder'=>'Product type', 'label'=>'Product type','error'=>'This field is required', 'required'=>true])
                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'pack_product', 'id'=>'pack_product', 'placeHolder'=>'Fbo', 'label'=>'Fbo','error'=>'This field is required', 'required'=>true])
                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'relay_delivery', 'id'=>'relay_delivery', 'placeHolder'=>'Relay Delivery', 'label'=>'Relay Delivery','error'=>'This field is required', 'required'=>false])
                            {{-- @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'relay_delivery_description', 'id'=>'relay_delivery_description', 'placeHolder'=>'Relay Delivery Description', 'label'=>'Relay Delivery Description','error'=>'This field is required', 'required'=>false]) --}}
                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'home_delivery', 'id'=>'home_delivery', 'placeHolder'=>'Home Delivery', 'label'=>'Home Delivery','error'=>'This field is required', 'required'=>false])
                            {{-- @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'home_delivery_description', 'id'=>'home_delivery_description', 'placeHolder'=>'Home Delivery Description', 'label'=>'Home Delivery Description','error'=>'This field is required', 'required'=>false]) --}}
                            @include('cms.layouts.blocks.input.status',['module'=>'country' , 'required'=>true])

                        </div>
                    </div>
                </div>
            </div>

            @include('cms.layouts.blocks.input.buttons', ['formId'=>$id, 'submit' => ''.route('country.store'), 'type'=>'POST'])
        </form>
    </div>
</div>
