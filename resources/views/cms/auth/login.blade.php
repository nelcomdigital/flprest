<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@if(isset($settings['cms_title'])) {{ $settings['cms_title']}}
        @else{{ config('app.name', 'Laravel') }}@endif</title>

    <link rel="shortcut icon" type="image/x-icon" href="@if(isset($settings['favicon'])) {{asset($settings['favicon'])}} @else  {{asset('images/login_logo.svg')}} @endif">

    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

    <link href="{{ asset('css/cms/main.css') }}" rel="stylesheet">
    <link href="{{ asset('css/inputfile.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jq-3.3.1/jszip-2.5.0/dt-1.10.18/af-2.3.2/b-1.5.4/b-colvis-1.5.4/b-flash-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.5.0/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.min.css"/>

    {{--<script src="{{ asset('js/app.js') }}" defer></script>--}}
    @stack('css')
</head>
<body>
<div id="app">
    <div class="row" style="padding: 0;margin: 0%;height: 100vh">
        <div class="col-md-6 flex-center"  style="@if(isset($settings['background'])) background-image : url('{{asset($settings['background'])}}'); background-size: 100% 100%; margin-bottom: 11px; @else background-image: url('{{asset('images/loginbg.png')}}'); background-size: cover; @endif backround-repeat: no-repeat; ">

            <div class="support-box">
{{--                <div class="login-join">--}}
{{--                    <img src="{{asset('images/conversation.svg')}}" style="width: 60px;height: 50px">--}}
{{--                    <div class="login-join-text">--}}
{{--                        {{__('cms.join_conversation')}}--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="login-blog">--}}
{{--                    <img src="{{asset('images/blog.svg')}}" style="width: 60px;height: 50px">--}}
{{--                    <div class="login-blog-text">--}}
{{--                        {{__('cms.check_blog')}}--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>
        </div>
        <div class="col-md-6 ">
            <div class="flex-center">
                <div class="login-cms-form">
                    <img  class="login-cms-img" src="@if(isset($settings['logo']) and $settings['logo'] != null) {{asset($settings['logo'])}} @else {{asset('images/flp-mobile-login.png')}} @endif">
                    <div class="login-cms-title">
                        {{__('cms.welcome_platform')}}
                    </div>
                    <form method="POST" action="{{route('cms.login')}}">
                        @csrf
                        <div class="login-cms-email">
                            <img src="{{asset('images/email.svg')}}" class="login-cms-icon">
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}"  placeholder="Email" required autofocus>

                            @if ($errors->has('email'))
                                <span class="login-error">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif

                            @if ($errors->has('custom_error') )

                                <span class="login-error">
                                        <strong>{{ $errors->first('custom_error') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="login-cms-password">
                            <img src="{{asset('images/password.svg')}}" class="login-cms-icon">
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required>

                            @if ($errors->has('password'))
                                <span class="login-error" >
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>


                        <div class="login-cms-submit-box">
                            <button type="submit" class="btn login-cms-submit">
                                {{__('cms.login')}}
                            </button>
                            <div class="login-cms-remember-me">

                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                <label class="form-check-label" for="remember">
                                    {{__('cms.remember_me')}}
                                </label>
                            </div>
                        </div>
                        {{--<div class="login-cms-forgot">--}}
                            {{--@if (Route::has('password.request'))--}}
                                {{--<a href="{{ route('cms.password.request') }}">--}}
                                    {{--{{alias('Forgot Your Password?')}}--}}
                                {{--</a>--}}
                            {{--@endif--}}
                        {{--</div>--}}

                    </form>

                </div>

            </div>
        </div>
    </div>
</div>

<footer class="login-footer">
    <ul class="login-footer-list">
        <li>
            <a class="footer-link" href="http://nelcomdigital.com/" target="_blank">© 2019 Nelcom Digital</a>
        </li>
{{--        <li>--}}
{{--            <a class="footer-link"  href="#">About</a>--}}
{{--        </li>--}}
    </ul>
</footer>


<script
        src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E="
        crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs-3.3.7/jq-3.3.1/jszip-2.5.0/dt-1.10.18/af-2.3.2/b-1.5.4/b-colvis-1.5.4/b-flash-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.5.0/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>


</body>
</html>
