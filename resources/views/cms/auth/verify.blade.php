@extends('frontend.layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ alias('Verify Your Email Address') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ alias('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif

                    {{ alias('Before proceeding, please check your email for a verification link.') }}
                    {{ alias('If you did not receive the email') }}, <a href="{{ route('cms.verification.resend') }}">{{ alias('click here to request another') }}</a>.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
