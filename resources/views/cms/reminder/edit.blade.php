@php($id = uniqid('form'))
<div class="right_col full" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>{{__('cms.edit_reminder')}}</h3>
            </div>
        </div>
        <form class="form-horizontal form-label-left" id="{{$id}}" novalidate >
            @csrf
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel closed">
                        <div class="x_title">
                            <h2>{{__('cms.reminder_properties')}}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            {{-- <span class="section">Article Properties</span> --}}
                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'name', 'id'=>'name', 'placeHolder'=>'Name', 'label'=>'Name','error'=>'This field is required', 'required'=>true,'value'=>$reminder->name])
                            
                            {{-- @include('cms.layouts.blocks.input.select',['name'=>'reminder_type_id', 'id'=>'reminder_type_id', 'label'=>'Reminder Type', 'required'=>false, 'options'=>$reminderTypes, 'error'=>'This field is required', 'multiple'=>false, 'selected'=>(isset($reminder->reminderType) ?$reminder ->reminderType : null)]) --}}
                            @include('cms.layouts.blocks.input.field',['type'=>'color', 'name'=>'color', 'id'=>'color', 'placeHolder'=>'#000','value'=>$reminder->color==null ? '':$reminder->color, 'label'=>'Color','error'=>'This field is required', 'required'=>false])

                            @include('cms.layouts.blocks.input.date-with-time',['name'=>'date', 'id'=>'date', 'label'=>'Reminder Date', 'required'=>true, 'placeHolder'=>'Date','value'=>$reminder->date])

                        </div>
                    </div>
                </div>
            </div>

            @include('cms.layouts.blocks.input.buttons', ['formId'=>$id, 'submit' => ''.route('reminder.update', $reminder->id), 'type'=>'PUT'])
        </form>
    </div>
</div>