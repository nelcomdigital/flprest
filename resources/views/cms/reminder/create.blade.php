@php($id = uniqid('form'))
<div class="right_col full" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>{{__('cms.new_reminder')}}</h3>
            </div>
        </div>
        <form class="form-horizontal form-label-left" id="{{$id}}" novalidate >
            @csrf
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>{{__('cms.reminder_properties')}}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            {{-- <span class="section">Article Properties</span> --}}
                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'name', 'id'=>'name', 'placeHolder'=>'Name', 'label'=>'Name','error'=>'This field is required', 'required'=>true])
                        
                            {{-- @include('cms.layouts.blocks.input.select',['name'=>'reminder_type_id', 'id'=>'reminder_type_id', 'label'=>'Reminder Type', 'required'=>false, 'options'=>$reminderTypes, 'error'=>'This field is required','multiple'=>false]) --}}
                            @include('cms.layouts.blocks.input.field',['type'=>'color', 'name'=>'color', 'id'=>'color', 'placeHolder'=>'#000', 'label'=>'Color','error'=>'This field is required', 'required'=>false])

                            @include('cms.layouts.blocks.input.date-with-time',['name'=>'date', 'id'=>'date', 'label'=>'Date', 'required'=>true, 'placeHolder'=>'Date'])


                        </div>
                    </div>
                </div>
            </div>

            @include('cms.layouts.blocks.input.buttons', ['formId'=>$id, 'submit' => ''.route('reminder.store'), 'type'=>'POST'])
        </form>
    </div>
</div>