@php($id = uniqid('form'))
<div class="right_col full" role="main">
    <div class="">
        <div class="page-title">
            <div>
                <h3>{{__('cms.edit_city')}}</h3>
            </div>
        </div>

        <form class="form-horizontal form-label-left" id="{{$id}}" novalidate>
            @csrf
            @foreach( \App\Models\Locale::all() as $locale)
                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>{{__('cms.city_properties')}} - {{$locale->name}} </h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'name['.$locale->code.']', 'id'=>'name', 'placeHolder'=>'Name', 'label'=>'Name','error'=>'This field is required', 'required'=>true, 'value'=>$city->name[$locale->code]])
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            @endforeach
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>{{__('cms.city_zipcode')}}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            @include('cms.layouts.blocks.input.select',['name'=>'department_id', 'id'=>'department_id', 'label'=>'Parent', 'required'=>false, 'options'=>$departments, 'error'=>'This field is required', 'multiple'=>false, 'selected'=>(isset($city->department) ?$city->department : null)])
                            @include('cms.layouts.blocks.input.select',['name'=>'country_id', 'id'=>'country_id', 'label'=>'Country', 'required'=>false, 'options'=>$countries, 'error'=>'This field is required', 'multiple'=>false,'selected'=>(isset($city->country) ?$city->country : null)])

                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'zip_code', 'id'=>'zip_code', 'placeHolder'=>'zip code', 'label'=>'zip Code','error'=>'This field is required', 'required'=>true, 'value'=>$city->zip_code])
                            @include('cms.layouts.blocks.input.flag',['fields'=>[
                                ['name'=> 'cargo_tax','label'=> 'Cargo Tax']
                                ], 'value'=>$city])
                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'cargo_amount', 'id'=>'cargo_amount', 'placeHolder'=>'Cargo Amount', 'label'=>'Cargo Amount', 'value'=>$city->cargo_amount])

                            @include('cms.layouts.blocks.input.flag',['fields'=>[
                            ['name'=> 'iva_tax','label'=> 'IVA Tax'],
                            ], 'value'=>$city])
                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'iva_amount', 'id'=>'iva_amount', 'placeHolder'=>'IVA Amount', 'label'=>'IVA Amount', 'value'=>$city->iva_amount])
                        </div>
                    </div>

                </div>
            </div>
            @include('cms.layouts.blocks.input.buttons', ['formId'=>$id, 'submit'=>''.route('city.update', $city->id), 'type'=>'PUT'])
        </form>
    </div>
</div>
<script>
    if ($('[name="cargo_tax"]').prop('checked')) {
        $('[name="cargo_amount"]').prop('disabled', false);
    } else {
        $('[name="cargo_amount"]').prop('disabled', true);
    }
    $('[name="cargo_tax"]').on('click', function () {
        if ($('[name="cargo_tax"]').prop('checked')) {
            $('[name="cargo_amount"]').prop('disabled', false);
        } else {
            $('[name="cargo_amount"]').prop('disabled', true);
        }
    });

    if ($('[name="iva_tax"]').prop('checked')) {
        $('[name="iva_amount"]').prop('disabled', false);
    } else {
        $('[name="iva_amount"]').prop('disabled', true);
    }
    $('[name="iva_tax"]').on('click', function () {
        if ($('[name="iva_tax"]').prop('checked')) {
            $('[name="iva_amount"]').prop('disabled', false);
        } else {
            $('[name="iva_amount"]').prop('disabled', true);
        }
    });
</script>
