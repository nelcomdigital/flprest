@php($id = uniqid('form'))
<div class="right_col full" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>{{__('cms.edit_delivery_mode')}}</h3>
            </div>
        </div>
        <form class="form-horizontal form-label-left" id="{{$id}}" novalidate >
            @csrf
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel closed">
                        <div class="x_title">
                            <h2>{{__('cms.delivery_mode_properties')}}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                                   
                            @include('cms.layouts.blocks.input.select',['name'=>'user_type', 'id'=>'user_type', 'label'=>'User Type', 'required'=>true,'multiple'=>false, 'custom_options'=>[
                                ['value'=>'FBO', 'name'=>'FBO'],
                                ['value'=>'CLI', 'name'=>'CLI'],
                            ], 'selected'=>$deliveryMode->user_type])

                            @include('cms.layouts.blocks.input.select',['name'=>'country_id', 'id'=>'country_id', 'label'=>'Country', 'required'=>true, 'options'=>$countries, 'error'=>'This field is required', 'multiple'=>false,'selected'=>(isset($deliveryMode->country) ?$deliveryMode->country : null)])

                            @include('cms.layouts.blocks.input.ckeditor',['name'=>'relay_delivery_values1', 'id'=>'relay_delivery_values1', 'label'=>'Relay Delivery Values', 'placeHolder'=>'Relay Delivery Values', 'value'=>$deliveryMode->relay_delivery_values1])

                            @include('cms.layouts.blocks.input.ckeditor',
                            ['name'=>'home_delivery_values1', 'id'=>'home_delivery_values1', 'label'=>'Home Delivery Values', 'placeHolder'=>'Home Delivery Values', 'value'=>$deliveryMode->home_delivery_values1])


                            @include('cms.layouts.blocks.input.ckeditor',
                            ['name'=>'withdrawal_points1', 'id'=>'withdrawal_points1', 'label'=>'Withdrawal Points', 'placeHolder'=>'Withdrawal Points', 'value'=>$deliveryMode->withdrawal_points1])
                        


                        </div>
                    </div>
                </div>
            </div>

            @include('cms.layouts.blocks.input.buttons', ['formId'=>$id, 'submit' => ''.route('delivery-mode.update', $deliveryMode->id), 'type'=>'PUT'])
        </form>
    </div>
</div>