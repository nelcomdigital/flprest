@php($id = uniqid('form'))
<div class="right_col full" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>{{__('cms.new_delivery_mode')}}</h3>
            </div>
        </div>
        <form class="form-horizontal form-label-left" id="{{$id}}" novalidate >
            @csrf
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>{{__('cms.delivery_mode_properties')}}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            @include('cms.layouts.blocks.input.select',['name'=>'user_type', 'id'=>'user_type', 'label'=>'User Type', 'required'=>true,'multiple'=>false, 'custom_options'=>[
                                ['value'=>'FBO', 'name'=>'FBO'],
                                ['value'=>'CLI', 'name'=>'CLI'],
                            ]])
                            @include('cms.layouts.blocks.input.select',['name'=>'country_id', 'id'=>'country_id', 'label'=>'Country', 'required'=>true, 'options'=>$countries, 'error'=>'This field is required', 'multiple'=>false])
            
                            @include('cms.layouts.blocks.input.ckeditor',['name'=>'relay_delivery_values', 'id'=>'relay_delivery_values', 'label'=>'Relay Delivery Values', 'placeHolder'=>'Relay Delivery Values'])

                            @include('cms.layouts.blocks.input.ckeditor',['name'=>'home_delivery_values', 'id'=>'home_delivery_values', 'label'=>'Home Delivery Values', 'placeHolder'=>'Home Delivery Values'])

                            @include('cms.layouts.blocks.input.ckeditor',['name'=>'withdrawal_points', 'id'=>'withdrawal_points', 'label'=>'Withdrawal Points', 'placeHolder'=>'Withdrawal Points'])


                        </div>
                    </div>
                </div>
            </div>

            @include('cms.layouts.blocks.input.buttons', ['formId'=>$id, 'submit' => ''.route('delivery-mode.store'), 'type'=>'POST'])
        </form>
    </div>
</div>