@php($id = uniqid('form'))
<div class="right_col full" role="main">
    <div class="">
        <div class="page-title">
            <div>
                <h3>{{__('cms.create_main_menu')}}</h3>
            </div>
        </div>
        <form class="form-horizontal form-label-left" id="{{$id}}" novalidate >
            @csrf
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>{{__('cms.main-menu_properties')}}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'name', 'id'=>'name', 'placeHolder'=>'Page Name', 'label'=>'Page Name','error'=>'This field is required', 'required'=>true])
                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'cat_id', 'id'=>'cat_id', 'placeHolder'=>'Category ID', 'label'=>'Category ID','error'=>'This field is required', 'required'=>false])

                            @include('cms.layouts.blocks.input.select',['name'=>'locales[]', 'id'=>'locales', 'label'=>'Locale', 'required'=>true, 'options'=>\App\Models\Locale::all(), 'error'=>'This field is required','multiple'=>true])
                            @include('cms.layouts.blocks.input.select',['name'=>'countries[]', 'id'=>'Countries', 'label'=>'Country', 'required'=>true, 'options'=>\App\Models\Country::all(), 'error'=>'This field is required','multiple'=>true])
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel closed">
                        <div class="x_title">
                            <h2>{{__('cms.main-menu_media')}}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            @include('cms.layouts.blocks.input.image',['name'=>'image', 'id'=>'image', 'label'=>'Page Image', 'required'=>false])
            
            
                            @include('cms.layouts.blocks.input.ckeditor',['name'=>'description', 'id'=>'description', 'label'=>'Description', 'placeHolder'=>'Description'])

                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel closed">
                        <div class="x_title">
                            <h2>{{__('cms.main-menu_relations')}}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            @include('cms.layouts.blocks.input.select',['name'=>'parents[]', 'id'=>'parents', 'label'=>'parents', 'required'=>false, 'options'=>$mainCategories, 'error'=>'This field is required','multiple'=>true])
                            @include('cms.layouts.blocks.input.flag',['fields'=>[
                                ['name'=> 'is_parent','label'=> 'Is Parent'],
                                ]])
                                @include('cms.layouts.blocks.input.flag',['fields'=>[
                                ['name'=> 'is_active','label'=> 'Is Active'],
                                ]])
                                
                            @include('cms.layouts.blocks.input.select',['name'=>'accessed_by[]', 'id'=>'accessed_by', 'label'=>'Accessed By', 'required'=>false,'multiple'=>true, 'custom_options'=>[
                                ['value'=>'FBO', 'name'=>'FBO'],
                                ['value'=>'CLI', 'name'=>'CLI'],
                            ]])
                        </div>
                    </div>
                </div>
            </div>
            @include('cms.layouts.blocks.input.buttons', ['formId'=>$id, 'submit' => ''.route('main-menu.store'), 'type'=>'POST'])
        </form>
    </div>
</div>
