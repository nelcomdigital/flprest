@php($id = uniqid('form'))
<div class="right_col full" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>{{__('cms.new_contact')}}</h3>
            </div>
        </div>
        <form class="form-horizontal form-label-left" id="{{$id}}" novalidate >
            @csrf
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>{{__('cms.contact_properties')}}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'name', 'id'=>'name', 'placeHolder'=>'Full Name', 'label'=>'Full Name','error'=>'This field is required', 'required'=>true])
                            
                            @include('cms.layouts.blocks.input.field',['type'=>'email', 'name'=>'email', 'id'=>'email', 'label'=>'Email', 'placeHolder'=>'Email', 'required'=>true, 'error'=>'This field is required'])

                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'phone', 'id'=>'phone', 'placeHolder'=>'Phone Number', 'label'=>'Phone Number','error'=>'This field is required', 'required'=>false])

                            @include('cms.layouts.blocks.input.select',['name'=>'user_id', 'id'=>'user_id', 'label'=>'User', 'required'=>true, 'options'=>$users, 'error'=>'This field is required','multiple'=>false])

                            @include('cms.layouts.blocks.input.select',['name'=>'reminder_id', 'id'=>'reminder_id', 'label'=>'Reminder', 'required'=>false, 'options'=>$reminders, 'error'=>'This field is required','multiple'=>false])

                            @include('cms.layouts.blocks.input.image',['name'=>'photo', 'id'=>'photo', 'label'=>'User Image', 'required'=>false])

                            @include('cms.layouts.blocks.input.ckeditor',['name'=>'comment', 'id'=>$id.'_comment', 'label'=>'Comment', 'required'=>false,  'placeHolder'=>'Comment'])

                        </div>
                    </div>
                </div>
            </div>

            @include('cms.layouts.blocks.input.buttons', ['formId'=>$id, 'submit' => ''.route('contact.store'), 'type'=>'POST'])
        </form>
    </div>
</div>