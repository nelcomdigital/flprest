@php($id = uniqid('form'))
<div class="right_col full" role="main">
    <div class="">
        <div class="page-title">
            <div>
                <h3>{{__('cms.update')}}: {{$contact->name}}</h3>
            </div>
        </div>
        <form class="form-horizontal form-label-left" id="{{$id}}" novalidate >
            @csrf
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel closed">
                        <div class="x_title">
                            <h2>{{__('cms.contact_properties')}}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'name', 'id'=>'name', 'placeHolder'=>'Full Name', 'label'=>'Full Name','error'=>'This field is required', 'required'=>true, 'value'=>$contact->name])
                            
                            @include('cms.layouts.blocks.input.field',['type'=>'email', 'name'=>'email', 'id'=>'email', 'label'=>'Email', 'placeHolder'=>'Email', 'required'=>true, 'error'=>'This field is required', 'value'=>$contact->email, 'readonly'=>true])
                            
                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'phone', 'id'=>'phone', 'placeHolder'=>'Phone Number', 'label'=>'Phone Number','error'=>'This field is required', 'required'=>false, 'value'=>$contact->phone])

                            @include('cms.layouts.blocks.input.select',['name'=>'user_id', 'id'=>'user_id', 'label'=>'User', 'required'=>true, 'options'=>$users, 'error'=>'This field is required', 'multiple'=>false, 'selected'=>(isset($contact->user) ?$contact->user : null)])
                            
                            @include('cms.layouts.blocks.input.select',['name'=>'reminder_id', 'id'=>'reminder_id', 'label'=>'Reminder', 'required'=>false, 'options'=>$reminders, 'error'=>'This field is required', 'multiple'=>false, 'selected'=>(isset($contact->reminder) ?$contact->reminder : null)])
 
                            @include('cms.layouts.blocks.input.image',['name'=>'photo', 'id'=>'photo', 'label'=>'User Image', 'required'=>false, 'value'=>$contact->photo, 'route'=>''.route('contact.delete-image', $contact->id)])

                            @include('cms.layouts.blocks.input.ckeditor',['name'=>'comment', 'id'=>$id.'_comment', 'label'=>'Comment', 'required'=>false,  'placeHolder'=>'Comment','value'=>$contact->comment])


                        </div>
                    </div>
                </div>
            </div>
            @include('cms.layouts.blocks.input.buttons', ['formId'=>$id, 'submit' => ''.route('contact.update', $contact->id), 'type'=>'PUT'])
        </form>
    </div>
</div>