@php($id = uniqid('form'))
<div class="right_col full" role="main">
    <div class="">
        <div class="page-title">
            <div>
                <h3>{{__('cms.edit_business_file')}}</h3>
            </div>
        </div>
        <form class="form-horizontal form-label-left" id="{{$id}}" novalidate >
            @csrf
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel closed">
                        <div class="x_title">
                            <h2>{{__('cms.business_file_properties')}}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'name', 'id'=>'name', 'placeHolder'=>'Business File Name', 'label'=>'Business File Name','error'=>'This field is required', 'required'=>true, 'value'=>$businessFile->name])
                            @include('cms.layouts.blocks.input.select',['name'=>'businessCategories[]',
                                'id'=>'businessCategories', 'label'=>'BusinessCategory', 'required'=>true,
                                'options'=>\App\Models\BusinessCategory::all(), 'error'=>'This field is required',
                                'multiple'=>true,'selected'=>$businessFile->businessCategories])
                            @include('cms.layouts.blocks.input.file',['name'=>'file', 'id'=>'file', 'label'=>'Page Image', 'required'=>false,'value'=>$businessFile->file])

                         </div>
                    </div>
                </div>
            </div>

            @include('cms.layouts.blocks.input.buttons', ['formId'=>$id, 'submit' => ''.route('business-file.update', $businessFile->id), 'type'=>'PUT'])
        </form>
    </div>
</div>
