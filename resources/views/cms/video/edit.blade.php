@php($id = uniqid('form'))
<div class="right_col full" role="main">
    <div class="">
        <div class="page-title">
            <div>
                <h3>{{__('cms.edit_video')}}</h3>
            </div>
        </div>
        <form class="form-horizontal form-label-left" id="{{$id}}" novalidate >
            @csrf
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel closed">
                        <div class="x_title">
                            <h2>{{__('cms.video_and_article_properties')}}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'name', 'id'=>'name', 'placeHolder'=>'Page Name', 'label'=>'Page Name','error'=>'This field is required', 'required'=>true, 'value'=>$video->name])
                            
                            @include('cms.layouts.blocks.input.select',['name'=>'categories[]', 'id'=>'categories', 'label'=>'Categories', 'required'=>true, 'options'=>$categories, 'multiple'=>true, 'error'=>'This field is required', 'selected'=>$video->categories])

                            @include('cms.layouts.blocks.input.select',['name'=>'menus[]', 'id'=>'menus', 'label'=>'Main Menu', 'required'=>true, 'options'=>$menus, 'multiple'=>true, 'error'=>'This field is required', 'selected'=>$video->menus])

                            @include('cms.layouts.blocks.input.select',['name'=>'locales[]',
                            'id'=>'locales', 'label'=>'Locale', 'required'=>true,
                            'options'=>\App\Models\Locale::all(), 'error'=>'This field is required',
                            'multiple'=>true,'selected'=>$video->locales])
                            
                            @include('cms.layouts.blocks.input.select',['name'=>'countries[]',
                            'id'=>'countries', 'label'=>'Country', 'required'=>true,
                            'options'=>\App\Models\Country::all(), 'error'=>'This field is required',
                            'multiple'=>true,'selected'=>$video->countries])
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel closed">
                        <div class="x_title">
                            <h2>{{__('cms.video_and_article_content')}}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            @include('cms.layouts.blocks.input.image',['name'=>'image', 'id'=>'image', 'label'=>'Image', 'required'=>false,'value'=>$video->image, 'route'=>''.route('video.delete-image', $video->id)])

                            @include('cms.layouts.blocks.input.image',['name'=>'thumbnail', 'id'=>'thumbnail', 'label'=>'Thumbnail', 'required'=>false,'value'=>$video->thumbnail, 'route'=>''.route('video.delete-image', $video->id)])

                            @include('cms.layouts.blocks.input.video',['name'=>'video', 'id'=>'video', 'label'=>'Video','placeHolder'=>'Maximum Size:600x400 px','value'=>$video->video,'required'=>false])
                            
                            @include('cms.layouts.blocks.input.ckeditor',['name'=>'short_description', 'id'=>'short_description', 'label'=>'Short Description', 'placeHolder'=>'Short Description', 'value'=>$video->short_description])
                            
                            @include('cms.layouts.blocks.input.ckeditor',['name'=>'description', 'id'=>'description', 'label'=>'Description', 'placeHolder'=>'Description', 'value'=>$video->description])

                        </div>
                    </div>
                </div>
            </div>
            @include('cms.layouts.blocks.input.buttons', ['formId'=>$id, 'submit' => ''.route('video.update', $video->id), 'type'=>'PUT'])
        </form>
    </div>
</div>
