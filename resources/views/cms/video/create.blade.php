@php($id = uniqid('form'))
<div class="right_col full" role="main">
    <div class="">
        <div class="page-title">
            <div>
                <h3>{{__('cms.create_video_or_article')}}</h3>
            </div>
        </div>
        <form class="form-horizontal form-label-left" id="{{$id}}" novalidate >
            @csrf
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>{{__('cms.video_and_article_properties')}}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'name', 'id'=>'name', 'placeHolder'=>'Name', 'label'=>'Name','error'=>'This field is required', 'required'=>true])

                            @include('cms.layouts.blocks.input.select',['name'=>'categories[]', 'id'=>'categories', 'label'=>'Categories', 'required'=>true, 'options'=>$categories, 'multiple'=>true, 'error'=>'This field is required'])

                            @include('cms.layouts.blocks.input.select',['name'=>'menus[]', 'id'=>'menus', 'label'=>'Menus', 'required'=>true, 'options'=>$menus, 'multiple'=>true, 'error'=>'This field is required'])

                            @include('cms.layouts.blocks.input.select',['name'=>'locales[]', 'id'=>'locales', 'label'=>'Locale', 'required'=>true, 'options'=>\App\Models\Locale::all(), 'error'=>'This field is required','multiple'=>true])

                            @include('cms.layouts.blocks.input.select',['name'=>'countries[]', 'id'=>'Countries', 'label'=>'Country', 'required'=>true, 'options'=>\App\Models\Country::all(), 'error'=>'This field is required','multiple'=>true])
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>{{__('cms.video_and_article_content')}}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            @include('cms.layouts.blocks.input.image',['name'=>'image', 'id'=>'image', 'label'=>'Image', 'required'=>false])
                            
                            @include('cms.layouts.blocks.input.image',['name'=>'thumbnail', 'id'=>'thumbnail', 'label'=>'Thumbnail', 'required'=>false])
                            
                            @include('cms.layouts.blocks.input.video',['name'=>'video', 'id'=>'video', 'label'=>'Video','placeHolder'=>'Maximum Size:600x400 px','required'=>false])
                        
                            @include('cms.layouts.blocks.input.ckeditor',['name'=>'short_description', 'id'=>'short_description', 'label'=>'Short Description', 'placeHolder'=>'Short Description'])
                            
                            @include('cms.layouts.blocks.input.ckeditor',['name'=>'description', 'id'=>'description', 'label'=>'Description', 'placeHolder'=>'Description'])

                        </div>
                    </div>
                </div>
            </div>
            @include('cms.layouts.blocks.input.buttons', ['formId'=>$id, 'submit' => ''.route('video.store'), 'type'=>'POST'])
        </form>
    </div>
</div>
