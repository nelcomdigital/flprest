@php($id = uniqid('form'))
<div class="right_col full" role="main">
    <div class="">
        <div class="page-title">
            <div>
                <h3>{{__('cms.create_product_category')}}</h3>
            </div>
        </div>
        <form class="form-horizontal form-label-left" id="{{$id}}" novalidate >
            @csrf
            @foreach( \App\Models\Locale::all() as $locale)
                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel ">
                            <div class="x_title">
                                <h2>{{__('cms.product_category_properties')}} - {{ $locale->name }}</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                {{-- <span class="section">Article Properties</span> --}}
                                @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'name['.$locale->code.']', 'id'=>'name', 'placeHolder'=>'Category Name', 'label'=>'Name','error'=>'This field is required', 'required'=>true])
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            @endforeach
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel ">
                        <div class="x_title">
                            <h2>{{__('cms.product_category_parent')}}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a></li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            @include('cms.layouts.blocks.input.image',['name'=>'banner_image', 'id'=>'banner_image', 'label'=>'Banner Image', 'required'=>true])

                            @include('cms.layouts.blocks.input.image',['name'=>'banner_image_mobile', 'id'=>'banner_image_mobile', 'label'=>'Banner Image Mobile', 'required'=>true])

                            @include('cms.layouts.blocks.input.select',['name'=>'countries[]', 'id'=>'countries', 'label'=>'Countries', 'required'=>false, 'options'=>$countries, 'multiple'=>true])
                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'reference_id', 'id'=>'Reference', 'placeHolder'=>'Reference', 'label'=>'Reference','error'=>'This field is required', 'required'=>false])

{{--                            @include('cms.layouts.blocks.input.select',['name'=>'countries[]', 'id'=>'countries', 'label'=>'Parent Countries', 'required'=>true, 'options'=>$countries, 'error'=>'This field is required', 'multiple'=>true])--}}

{{--                            @include('cms.layouts.blocks.input.select',['name'=>'parent_id', 'id'=>'parent_id', 'label'=>'Parent', 'required'=>false, 'options'=>$productCategories, 'error'=>'This field is required','multiple'=>false])--}}
                            @include('cms.layouts.blocks.input.flag',['fields'=>[
                                                        ['name'=> 'is_active','label'=> 'Active?'],
                                                        ]])
                            @include('cms.layouts.blocks.input.image',['name'=>'icon', 'id'=>'icon', 'label'=>'App Icon', 'required'=>true])

                            @include('cms.layouts.blocks.input.flag',['fields'=>[
                                                        ['name'=> 'for_app','label'=> 'For App'],
                                                        ]])
                            {{--                            @include('cms.layouts.blocks.input.select',['name'=>'parent_id', 'id'=>'parent_id', 'label'=>'Parent', 'required'=>false, 'options'=>$productCategories, 'error'=>'This field is required','multiple'=>false])--}}
{{--                            @include('cms.layouts.blocks.input.select',['name'=>'levels_allowed[]', 'id'=>'levels_allowed', 'label'=>'Levels Allowed', 'required'=>false, 'options'=>$roles, 'error'=>'This field is required' , 'multiple'=>true])--}}
                            @include('cms.layouts.blocks.input.select',['name'=>'levels_allowed[]', 'id'=>'levels_allowed', 'label'=>'Levels Allowed', 'required'=>false, 'options'=>$roles, 'error'=>'This field is required' , 'multiple'=>true])

                        </div>
                    </div>
                </div>
            </div>
            @include('cms.layouts.blocks.input.buttons', ['formId'=>$id, 'submit' => ''.route('product-category.store'), 'type'=>'POST'])
        </form>
    </div>
</div>
