@php($id = uniqid('form'))
<div class="right_col full" role="main">
    <div class="">
        <div class="page-title">
            <div>
                <h3>{{__('cms.edit_order')}}</h3>
            </div>
        </div>
        <form class="form-horizontal form-label-left" id="{{$id}}" novalidate >
            @csrf
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>{{__('cms.order_properties')}}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            {{-- <span class="section">Article Properties</span> --}}
                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'name', 'id'=>'name', 'placeHolder'=>'Order Reference', 'label'=>'Order Reference','error'=>'This field is required', 'required'=>true,'value'=>$order->name])
                            @include('cms.layouts.blocks.input.select',['name'=>'users', 'id'=>'users', 'label'=>'Customer', 'required'=>false, 'options'=>$users, 'multiple'=>true,'selected'=>$order->users])


                            @include('cms.layouts.blocks.input.status',['module'=>'order' , 'required'=>true,'selected'=>$order->status])

                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'payment', 'id'=>'payment', 'placeHolder'=>'Payment', 'label'=>'Payment', 'required'=>false,'value'=>$order->payment])
                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'discount', 'id'=>'discount', 'placeHolder'=>'Discounts', 'label'=>'Discounts','required'=>false,'value'=>$order->discount])
                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'total_products', 'id'=>'total_products', 'placeHolder'=>'Total Products', 'label'=>'Total Products', 'required'=>false,'value'=>$order->total_products])
                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'tax', 'id'=>'tax', 'placeHolder'=>'Tax', 'label'=>'Tax','required'=>false,'value'=>$order->tax])
                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'total', 'id'=>'total', 'placeHolder'=>'TOTAL', 'label'=>'TOTAL','required'=>false,'value'=>$order->total])
                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'total_paid', 'id'=>'total_paid', 'placeHolder'=>'TOTAL Paid ', 'label'=>'TOTAL Paid','required'=>false,'value'=>$order->total_paid])

                        </div>
                    </div>
                </div>
            </div>

            @include('cms.layouts.blocks.input.buttons', ['formId'=>$id, 'submit' => ''.route('order.update', $order->id), 'type'=>'PUT'])
        </form>
        </form>
    </div>
</div>