@php($id = uniqid('form'))
<div class="right_col full" role="main">
    <div class="">
        <div class="page-title">
            <div>
                <h3>{{__('cms.create_internal-notification')}}</h3>
            </div>
        </div>

        <form class="form-horizontal form-label-left" id="{{$id}}" novalidate >
            @csrf
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel closed ">
                        <div class="x_title">
                            <h2>{{__('cms.internal-notification_properties')}}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'name', 'id'=>'name', 'placeHolder'=>'Name', 'label'=>' Name','error'=>'This field is required', 'required'=>true])
                            @include('cms.layouts.blocks.input.textarea',['type'=>'text', 'name'=>'message', 'id'=>'name', 'placeHolder'=>'message', 'label'=>' message','error'=>'This field is required', 'required'=>true])
                            @include('cms.layouts.blocks.input.date',['name'=>'start_date', 'id'=>'start_date', 'label'=>'Start Date', 'required'=>true, 'placeHolder'=>'Date'])
                            @include('cms.layouts.blocks.input.date',['name'=>'end_date', 'id'=>'end_date', 'label'=>'End Date', 'required'=>true, 'placeHolder'=>'Date'])
                            <div class="row">
                                <div class="col-md-12">{{__('cms.user')}}</div>
                                <div class="col-md-4">
                                    @include('cms.layouts.blocks.input.flag',['fields'=>[
                                                        ['name'=> 'FBO','label'=> 'FBO'],
                                                        ]])
                                </div>
                                <div class="col-md-4">
                                    @include('cms.layouts.blocks.input.flag',['fields'=>[
                                                        ['name'=> 'CLI','label'=> 'CLI'],
                                                        ]])
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 control-label">{{__('cms.user-notification-platform')}}</div>
                                <div class="col-md-4">
                                    @include('cms.layouts.blocks.input.flag',['fields'=>[
                                                        ['name'=> 'android','label'=> 'Android'],
                                                        ]])
                                </div>
                                <div class="col-md-4">
                                    @include('cms.layouts.blocks.input.flag',['fields'=>[
                                                        ['name'=> 'ios','label'=> 'IOS'],
                                                        ]])
                                </div>
                            </div>
                            @include('cms.layouts.blocks.input.status',['module'=>'internal-notification', 'required'=>true])

                        </div>
                    </div>
                </div>
            </div>

            @include('cms.layouts.blocks.input.buttons', ['formId'=>$id, 'submit' => ''.route('internal-notification.store'), 'type'=>'POST'])
        </form>
    </div>
</div>
