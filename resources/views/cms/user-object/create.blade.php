@php($id = uniqid('form'))
<div class="right_col full" role="main">
    <div class="">
        <div class="page-title">
            <div>
                <h3>{{__('cms.create_user-object')}}</h3>
            </div>
        </div>
        <form class="form-horizontal form-label-left" id="{{$id}}" novalidate >
            @csrf
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>{{__('cms.user-object_properties')}}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'user_id', 'id'=>'user_id', 'placeHolder'=>'User ID', 'label'=>'User ID','error'=>'This field is required', 'required'=>false])
                            
                            @include('cms.layouts.blocks.input.select',['name'=>'object_type', 'id'=>'object_type', 'label'=>'Type', 'required'=>false, 'custom_options'=>[
                                ['value'=>'0', 'name'=>'personal'],
                                ['value'=>'1', 'name'=>'family'],
                                ['value'=>'2', 'name'=>'financial'],
                            ]])

                            @include('cms.layouts.blocks.input.ckeditor',['name'=>'short_term_description', 'id'=>'short_term_description', 'label'=>'Short Term Description', 'placeHolder'=>'Short Term Description'])
                            
                            @include('cms.layouts.blocks.input.ckeditor',['name'=>'long_term_description', 'id'=>'long_term_description', 'label'=>'Long Term Description', 'placeHolder'=>'Long Term Description'])

                        </div>
                    </div>
                </div>
            </div>
            @include('cms.layouts.blocks.input.buttons', ['formId'=>$id, 'submit' => ''.route('user-object.store'), 'type'=>'POST'])
        </form>
    </div>
</div>
