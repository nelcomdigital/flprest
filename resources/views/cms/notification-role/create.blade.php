@php($id = uniqid('form'))
<div class="right_col full" role="main">
    <div class="">
        <div class="page-title">
            <div>
                <h3>{{__('cms.create_notification-role')}}</h3>
            </div>
        </div>

        <form class="form-horizontal form-label-left" id="{{$id}}" novalidate >
            @csrf
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel closed ">
                        <div class="x_title">
                            <h2>{{__('cms.notification-role_properties')}}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            {{-- <span class="section">Venue Properties</span> --}}
                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'name', 'id'=>'name', 'placeHolder'=>'Name', 'label'=>' Name','error'=>'This field is required', 'required'=>true])
                            {{-- @include('cms.layouts.blocks.input.field',['type'=>'number', 'name'=>'num', 'id'=>'num', 'placeHolder'=>'Num (Unique)', 'label'=>'Num (Unique)','error'=>'This field is required', 'required'=>true]) --}}
                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'module', 'id'=>'module', 'placeHolder'=>'module', 'label'=>'module','error'=>'This field is required', 'required'=>true])
                            @include('cms.layouts.blocks.input.textarea',['type'=>'text', 'name'=>'message', 'id'=>'name', 'placeHolder'=>'message', 'label'=>' message','error'=>'This field is required', 'required'=>false])
                            {{-- @include('cms.layouts.blocks.input.image',['name'=>'icon', 'id'=>'icon', 'label'=>'icon','placeholder'=>__('cms.size.notification.icon') , 'required'=>false]) --}}

                        </div>
                    </div>
                </div>
            </div>

            @include('cms.layouts.blocks.input.buttons', ['formId'=>$id, 'submit' => ''.route('notification-role.store'), 'type'=>'POST'])
        </form>
    </div>
</div>
