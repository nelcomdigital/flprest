@php($id = uniqid('form'))
<div class="right_col full" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>{{__('cms.edit_locale')}}</h3>
            </div>
        </div>
        <form class="form-horizontal form-label-left" id="{{$id}}" novalidate >
            @csrf
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel closed">
                        <div class="x_title">
                            <h2>{{__('cms.locale_properties')}}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            {{-- <span class="section">Article Properties</span> --}}
                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'name', 'id'=>'name', 'placeHolder'=>'Name', 'label'=>'Name','error'=>'This field is required', 'required'=>true,'value'=>$locale->name])
                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'code', 'id'=>'code', 'placeHolder'=>'Code', 'label'=>'Symbol','required'=>false,'value'=>$locale->code])

                        </div>
                    </div>
                </div>
            </div>

            @include('cms.layouts.blocks.input.buttons', ['formId'=>$id, 'submit' => ''.route('locale.update', $locale->id), 'type'=>'PUT'])
        </form>
    </div>
</div>
