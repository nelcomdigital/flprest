@php($id = uniqid('form'))
<div class="right_col full" role="main">
    <div class="">
        <div class="page-title">
            <div>
                <h3>Create Alias</h3>
            </div>
        </div>

        <form class="form-horizontal form-label-left" id="{{$id}}" novalidate >
            @csrf
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Alias Properties</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'key', 'id'=>'name', 'placeHolder'=>'Key ', 'label'=>'Key','error'=>'This field is required', 'required'=>true])
                            @foreach($locales as $locale)
                                @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'value['.$locale->code.']', 'id'=>'value['.$locale->code.']', 'placeHolder'=>'Key in '.$locale->name, 'label'=>'Key in '.$locale->name,'error'=>'This field is required', 'required'=>false])
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>


            @include('cms.layouts.blocks.input.buttons', ['formId'=>$id, 'submit' => ''.route('translation.store'), 'type'=>'POST'])
        </form>
    </div>
</div>
