
<div class="form-group">
    <div class="col-md-12">

        <button  type="submit" class="btn btn-success btn-save" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Submitting" data-url="{{route('translation.inject')}}" data-type="GET" > {{__('cms.missing_translations')}} ( {{$missingTranslations}} )</button>
        <button  type="submit" class="btn btn-success btn-save" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Submitting" data-url="{{route('translation.export')}}" data-type="GET" > {{__('cms.export_translations')}} </button>

    </div>
</div>

@include('cms.layouts.blocks.table.ajax-tables', ['route'=> route('translation.index'),'ordering'=>false, 'model'=>$model])
