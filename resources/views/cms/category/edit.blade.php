@php($id = uniqid('form'))
<div class="right_col full" role="main">
    <div class="">
        <div class="page-title">
            <div>
                <h3>{{__('cms.edit_category')}}</h3>
            </div>
        </div>
        <form class="form-horizontal form-label-left" id="{{$id}}" novalidate >
            @csrf
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel closed">
                        <div class="x_title">
                            <h2>{{__('cms.category_properties')}}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'name', 'id'=>'name', 'placeHolder'=>'Page Name', 'label'=>'Page Name','error'=>'This field is required', 'required'=>true, 'value'=>$category->name])

                            @include('cms.layouts.blocks.input.select',['name'=>'module_ids[]', 'id'=>'modules', 'multiple'=>true, 'label'=>'Module', 'required'=>false, 'custom_options'=>$modules , 'selected'=>$category->modules()])

                            @include('cms.layouts.blocks.input.select',['name'=>'locales[]',
                            'id'=>'locales', 'label'=>'Locale', 'required'=>true,
                            'options'=>\App\Models\Locale::all(), 'error'=>'This field is required',
                            'multiple'=>true,'selected'=>$category->locales])

                            @include('cms.layouts.blocks.input.select',['name'=>'countries[]',
                            'id'=>'countries', 'label'=>'Country', 'required'=>true,
                            'options'=>\App\Models\Country::all(), 'error'=>'This field is required',
                            'multiple'=>true,'selected'=>$category->countries])
                        </div>
                    </div>
                </div>
            </div>
            @include('cms.layouts.blocks.input.buttons', ['formId'=>$id, 'submit' => ''.route('category.update', $category->id), 'type'=>'PUT'])
        </form>
    </div>
</div>
