@php($id = uniqid('form'))
<div class="right_col full" role="main">
    <div class="">
        <div class="page-title">
            <div>
                <h3>{{__('cms.edit_page')}}</h3>
            </div>
        </div>
        <form class="form-horizontal form-label-left" id="{{$id}}" novalidate >
            @csrf
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel closed">
                        <div class="x_title">
                            <h2>{{__('cms.page_properties')}}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'name', 'id'=>'name', 'placeHolder'=>'Page Name', 'label'=>'Page Name','readonly'=>true,'error'=>'This field is required', 'required'=>true, 'value'=>$page->name])
                            @include('cms.layouts.blocks.input.select',['name'=>'locales[]',
                            'id'=>'locales', 'label'=>'Locale', 'required'=>true,
                            'options'=>\App\Models\Locale::all(), 'error'=>'This field is required',
                            'multiple'=>true,'selected'=>$page->locales])
                            @include('cms.layouts.blocks.input.image',['name'=>'image', 'id'=>'image', 'label'=>'Page Image', 'required'=>false,'value'=>$page->image, 'route'=>''.route('page.delete-image', $page->id)])
                            @include('cms.layouts.blocks.input.video',['name'=>'video', 'id'=>'video', 'label'=>'video','value'=>$page->video, 'route'=>''.route('page.delete-image', $page->id),'required'=>false])
                            @include('cms.layouts.blocks.input.textarea',['type'=>'text', 'name'=>'text', 'id'=>'text', 'placeHolder'=>'Text', 'label'=>'Text','value'=>$page->text])
                            @include('cms.layouts.blocks.input.ckeditor',['name'=>'description', 'id'=>$id.'_description', 'label'=>'Description', 'required'=>false,  'placeHolder'=>'Description', 'value'=>$page->description])
                            @include('cms.layouts.blocks.input.dynamic-inputs', ['add'=>'Add Text', 'label'=>'Text List', 'inputs'=>[
                                ['name'=>'texts', 'type'=>'text', 'placeHolder' => 'Text'],
                            ], 'values'=>$page->text_list])
                            @include('cms.layouts.blocks.input.field',['type'=>'field', 'name'=>'field', 'id'=>'field', 'placeHolder'=>'Field', 'label'=>'Field','value'=>$page->field])
                            @include('cms.layouts.blocks.input.flag',['fields'=>[
                                                                    ['name'=> 'CLI','label'=> 'CLI'],
                                                                    ], 'value'=>$page])
                            @include('cms.layouts.blocks.input.flag',['fields'=>[
                                                        ['name'=> 'FBO','label'=> 'FBO'],
                                                        ], 'value'=>$page])
                         </div>
                    </div>
                </div>
            </div>

            @include('cms.layouts.blocks.input.buttons', ['formId'=>$id, 'submit' => ''.route('page.update', $page->id), 'type'=>'PUT'])
        </form>
    </div>
</div>
