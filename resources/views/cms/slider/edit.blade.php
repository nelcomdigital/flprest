@php($id = uniqid('form'))
<div class="right_col full" role="main">
    <div class="">
        <div class="page-title">
            <div>
                <h3>{{__('cms.edit_slider')}}</h3>
            </div>
        </div>
        <form class="form-horizontal form-label-left" id="{{$id}}" novalidate >
            @csrf
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel closed">
                        <div class="x_title">
                            <h2>{{__('cms.edit_slider_properties')}}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'name', 'id'=>'name', 'placeHolder'=>'Slider Name', 'label'=>'Slider Title','error'=>'This field is required', 'required'=>true, 'value'=>$slider->name])
                            @include('cms.layouts.blocks.input.image',['name'=>'image', 'id'=>'image', 'label'=>'Slider Image', 'required'=>false,'value'=>$slider->image, 'route'=>''.route('slider.delete-image', $slider->id)])
                            @include('cms.layouts.blocks.input.image',['name'=>'image_mobile', 'id'=>'image_mobile', 'label'=>'Slider Mobile Image','placeHolder'=>'Maximum Size:720x1280 px','value'=>$slider->image_mobile, 'route'=>''.route('slider.delete-image', $slider->id),'required'=>false])
                            @include('cms.layouts.blocks.input.select',['name'=>'locale_id', 'id'=>'locale', 'label'=>'Locale', 'required'=>true, 'options'=>$locales, 'multiple'=>false, 'selected'=>(isset($slider->locale))? $slider->locale : null])
                            @include('cms.layouts.blocks.input.select',['name'=>'countries[]', 'id'=>'countries', 'label'=>'Country', 'required'=>true, 'options'=>\App\Models\Country::all(), 'error'=>'This field is required', 'multiple'=>true, 'selected'=>$slider->countries])
                            @include('cms.layouts.blocks.input.select',['name'=>'main_menu_id', 'id'=>'main_menu_id', 'label'=>'Main Menu', 'required'=>true, 'options'=>$mainMenus, 'multiple'=>false, 'selected'=>(isset($slider->mainMenu))? $slider->mainMenu : null])

                            @include('cms.layouts.blocks.input.flag',['fields'=>[
                            ['name'=> 'is_active','label'=> 'Active'],
                            ], 'value'=>$slider])

                        </div>
                    </div>
                </div>
            </div>

            @include('cms.layouts.blocks.input.buttons', ['formId'=>$id, 'submit' => ''.route('slider.update', $slider->id), 'type'=>'PUT'])
        </form>
    </div>
</div>
