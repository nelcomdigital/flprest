@php($id = uniqid('form'))
<div class="right_col full" role="main">
    <div class="">
        <div class="page-title">
            <div>
                <h3>{{__('cms.create_slider')}}</h3>
            </div>
        </div>
        <form class="form-horizontal form-label-left" id="{{$id}}" novalidate>
            @csrf
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>{{__('cms.slider_properties')}} </h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'name', 'id'=>'name', 'placeHolder'=>'Slider Title', 'label'=>'Slider Title','error'=>'This field is required', 'required'=>true])
                            @include('cms.layouts.blocks.input.image',['name'=>'image', 'id'=>'image', 'label'=>'Image', 'required'=>false])
                            @include('cms.layouts.blocks.input.image',['name'=>'image_mobile', 'id'=>'image_mobile', 'label'=>'Mobile Image','placeHolder'=>'Maximum Size:600x400 px','required'=>false])
                            @include('cms.layouts.blocks.input.select',['name'=>'locale_id', 'id'=>'locale', 'label'=>'Locale', 'required'=>true, 'options'=>$locales, 'multiple'=>false])
                            @include('cms.layouts.blocks.input.select',['name'=>'countries[]', 'id'=>'countries', 'label'=>'country', 'required'=>true, 'options'=>\App\Models\Country::all(), 'error'=>'This field is required', 'multiple'=>true])
                            @include('cms.layouts.blocks.input.select',['name'=>'main_menu_id', 'id'=>'main_menu_id', 'label'=>'Main Menu', 'required'=>false, 'options'=>$mainMenus, 'error'=>'This field is required','multiple'=>false])
                            @include('cms.layouts.blocks.input.flag',['fields'=>[
                            ['name'=> 'is_active','label'=> 'Active'],
                            ]])
{{--                            @include('cms.layouts.blocks.input.select',['name'=>'locales[]', 'id'=>'locales', 'label'=>'Locales', 'required'=>true, 'options'=>$locales, 'multiple'=>(isset($selected_locales))? true : false, 'selected'=>(isset($selected_locales))? $selected_locales : null])--}}

{{--                            @include('cms.layouts.blocks.input.select',['name'=>'locale_id', 'id'=>'locale_id', 'label'=>'Locale', 'required'=>false, 'options'=>$locale, 'error'=>'This field is required','multiple'=>false])--}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            @include('cms.layouts.blocks.input.buttons', ['formId'=>$id, 'submit' => ''.route('slider.store'), 'type'=>'POST'])
        </form>
    </div>
</div>
