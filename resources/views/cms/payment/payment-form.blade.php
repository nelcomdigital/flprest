<html lang="{{session()->get('current_locale')}}">
<body>
<form id="formPayment" action="https://paiement.systempay.fr/vads-payment/" name="formPayment"  method="POST">
    @foreach($params as $key=> $param)
        <input type="hidden" name="{{$key}}" value="{{$param}}">
    @endforeach

</form>
<script type="application/javascript">
    //Once the page is loaded, submit the form
    setTimeout(function(){ document.getElementById('formPayment').submit(); }, 200);

</script>
</body>

</html>


