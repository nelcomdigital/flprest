@php($id = uniqid('form'))
<div class="right_col full" role="main">
    <div class="">
        <div class="page-title">
            <div>
                <h3>{{__('cms.create_producer')}}</h3>
            </div>
        </div>
        <form class="form-horizontal form-label-left" id="{{$id}}" novalidate >
            @csrf
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel ">
                        <div class="x_title">
                            <h2>{{__('cms.producer_properties')}}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            {{-- <span class="section">Article Properties</span> --}}
                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'name', 'id'=>'name', 'placeHolder'=>'Name', 'label'=>'Name','error'=>'This field is required', 'required'=>true])

                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'commercial_name', 'id'=>'commercial_name', 'placeHolder'=>'Commercial Name', 'label'=>'Commercial Name','required'=>false])

                            @include('cms.layouts.blocks.input.date',['name'=>'date_created', 'id'=>'date_created', 'label'=>'Date Created', 'required'=>true, 'placeHolder'=>'Date Created'])

                            @include('cms.layouts.blocks.input.date',['name'=>'date_updated', 'id'=>'date_updated', 'label'=>'Date Updated', 'required'=>false, 'placeHolder'=>'Date Updated'])

                            @include('cms.layouts.blocks.input.image',['name'=>'image', 'id'=>'image', 'label'=>'Profile', 'required'=>false])

                            @include('cms.layouts.blocks.input.image',['name'=>'cover_image', 'id'=>'cover_image', 'label'=>'Cover', 'required'=>false])

                            @include('cms.layouts.blocks.input.status',['module'=>'producer', 'required'=>false])

                            @include('cms.layouts.blocks.input.loggers',['module'=>'producer'])

                        </div>
                    </div>
                </div>
            </div>

            @include('cms.layouts.blocks.input.buttons', ['formId'=>$id, 'submit' => ''.route('producer.store'), 'type'=>'POST'])
        </form>
    </div>
</div>