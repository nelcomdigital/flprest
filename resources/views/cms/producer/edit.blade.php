@php($id = uniqid('form'))
<div class="right_col full" role="main">
    <div class="">
        <div class="page-title">
            <div>
                <h3>{{__('cms.edit_producer')}}</h3>
            </div>
        </div>
        <form class="form-horizontal form-label-left" id="{{$id}}" novalidate >
            @csrf
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>{{__('cms.producer_properties')}}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            {{-- <span class="section">Article Properties</span> --}}
                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'name', 'id'=>'name', 'placeHolder'=>'Name', 'label'=>'Name','error'=>'This field is required', 'required'=>true,'value'=>$producer->name])

                            @include('cms.layouts.blocks.input.field',['type'=>'text', 'name'=>'commercial_name', 'id'=>'commercial_name', 'placeHolder'=>'Commercial Name', 'label'=>'Commercial Name','required'=>false,'value'=>$producer->commercial_name])

                            @include('cms.layouts.blocks.input.date',['name'=>'date_created', 'id'=>'date_created', 'label'=>'Date Created', 'required'=>true, 'placeHolder'=>'Date Created','value'=>$producer->date_created])

                            @include('cms.layouts.blocks.input.date',['name'=>'date_updated', 'id'=>'date_updated', 'label'=>'Date Updated', 'required'=>false, 'placeHolder'=>'Date Updated','value'=>$producer->date_updated])

                            @include('cms.layouts.blocks.input.image',['name'=>'image', 'id'=>'image', 'label'=>'Profile', 'required'=>false,'value'=>$producer->image, 'route'=>''.route('producer.delete-image', $producer->id)])

                            @include('cms.layouts.blocks.input.image',['name'=>'cover_image', 'id'=>'cover_image', 'label'=>'Cover', 'required'=>false,'value'=>$producer->cover_image, 'route'=>''.route('producer.delete-image', $producer->id)])

                            @include('cms.layouts.blocks.input.status',['module'=>'producer', 'required'=>false,'selected'=>$producer->status])

                            @include('cms.layouts.blocks.input.loggers', ['selected_countries'=>$producer->countries, 'selected_domains'=>$producer->domains, 'selected_locales'=>$producer->locales])

                        </div>
                    </div>
                </div>
            </div>

            @include('cms.layouts.blocks.input.buttons', ['formId'=>$id, 'submit'=>''.route('producer.update', $producer->id), 'type'=>'PUT'])
        </form>
    </div>
</div>