<?php

namespace App\Console\Commands;

use App\Jobs\SendFCMNotification;
use App\Mail\ErrorMail;
use App\Mail\sponsee36HourNotification;
use App\Mail\sponseeAfter72HourNotification;
use App\Mail\sponser36HourNotification;
use App\Mail\sponserAfter72HourNotification;
use App\Mail\sponserLessThan2CC;
use App\Models\Meeting;
use App\Models\NotificationRole;
use App\Models\Order;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class MeetingNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:MeetingNotification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'MeetingNotification';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $meetings = Meeting::query()->where('started', '!=', 1)->get();
        foreach ($meetings as $meeting) {
            $start = Carbon::createFromFormat('d/m/Y H:i', $meeting->start_date)->timezone('UTC');
//            $end = Carbon::createFromFormat('d/m/Y H:i', $meeting->end_date)->timezone('UTC');
            $now = Carbon::now()->addHours(2);
            if ($start > $now) {
                $dif = $start->diffInHours($now);
                if ($dif <= 24 && $meeting->not_24 != 1) {
                    $notification = NotificationRole::query()
                        ->where('num', 5)
                        ->first();
                    $other_meeting_id = $meeting->id;
                    $message = $notification->message;
                    $order_id = null;
                    $training_id = null;
                    $event_id = null;
                    $rdv_id = null;
                    $participation_date = null;
                    SendFCMNotification::dispatch([
                        'to' => $meeting->userNum,
                        'from' => null,
                        'name' => $notification->name,
                        'message' => $message,
                        'module' => $notification->module,
                        'order_id' => $order_id,
                        'training_id' => $training_id,
                        'event_id' => $event_id,
                        'rdv_id' => $rdv_id,
                        'other_meeting_id' => $other_meeting_id,
                        'participation_date' => $participation_date,
                    ]);

                    $meeting->not_24 = 1;
                    $meeting->save();


                } elseif ($dif <= 1 && $meeting->not_1 != 1) {
                    $notification = NotificationRole::query()
                        ->where('num', 6)
                        ->first();
                    $other_meeting_id = $meeting->id;
                    $message = $notification->message;
                    $order_id = null;
                    $training_id = null;
                    $event_id = null;
                    $rdv_id = null;
                    $participation_date = null;
                    SendFCMNotification::dispatch([
                        'to' => $meeting->userNum,
                        'from' => null,
                        'name' => $notification->name,
                        'message' => $message,
                        'module' => $notification->module,
                        'order_id' => $order_id,
                        'training_id' => $training_id,
                        'event_id' => $event_id,
                        'rdv_id' => $rdv_id,
                        'other_meeting_id' => $other_meeting_id,
                        'participation_date' => $participation_date,
                    ]);

                    $meeting->not_1 = 1;
                    $meeting->started = 1;
                    $meeting->save();
                }
            }

        }
    }
}
