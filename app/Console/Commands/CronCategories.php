<?php

namespace App\Console\Commands;

use App\Mail\sponsee36HourNotification;
use App\Mail\sponserLessThan2CC;
use App\Models\Order;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class CronCategories extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:categories';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

       $orders  =   Order::query()->where('status','temporary')->get();

        $now = Carbon::now();
       foreach ( $orders as $order ){
           $dateto = Carbon::parse($order->created_at)->addRealHours(36);
           $datefrom = Carbon::parse($order->created_at)->addRealHours(37);

           if ( Carbon::parse($order->created_at)->addHours(72)->lessThan( $now )){
               $order->delete();
           }
           if ( $datefrom->greaterThan($now) && $dateto->lessThan($now)){
               $user = User::query()->where('_id',$order->user_id)->first();
               App::setLocale($order->language);
               usleep(200);
               Mail::to($user->email)->send(new sponsee36HourNotification($order));
           }

       }
    }
}
