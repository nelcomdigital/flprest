<?php

namespace App\Console\Commands;

use App\Models\Article;
use App\Providers\Modules\Manifest;
use Illuminate\Console\Command;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Schema;
use Infinety\Config\Rewrite;

class module extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:module {module}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create New Module';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $module = $this->argument('module');

        $result = $this->splitCamelCase($module);

        $params = array();
        //1.
        $params['name'] ='';
        $params['table']= '';
        $params['slug']= '';
        foreach ($result as $k=>$r){
            if ( $k == count($result)-1 ){
                $params['name'] .=ucfirst(str_singular($r));
                $params['table'].= str_plural(strtolower($r));
                $params['slug'] .= str_slug($r);
            }else{
                $params['name'] .=ucfirst($r);
                $params['table'].= strtolower($r).'_';
                $params['slug'] .= str_slug($r).'-';
            }
        }
        $this->line("Creating ".$params['name']."...");

        //2.
        $v = $this->ask("Set parent ( -1 |-2 not mentioned | separated by . )");
        $params['parent']= $v;

        //3.
        $tableName = $this->ask("Set table Name (0 for ".$params['table'].",-1 for no table )");
        if ( $tableName == -1 ){
            $params['table']= -1;
        }else if ( $tableName == 0){
            // do nothing
        }else
            $params['table']= $tableName;

        //4.

        $controller = $this->choice("Need Controller?",['no','yes']);

        if ( $controller == 'yes')
            $params['controller'] = $params['name'].'Controller';
        else
            $params['controller'] = 0;
        //5.
        $views = $this->choice("Need Views?",['no','yes']);
        $params['views'] = $views=="yes"?1:0;

        $views = $this->choice("Need Routes?",['no','yes']);
        $params['service'] = $views=="yes"?1:0;

        //6.
        $views = $this->choice("Need Model?",['no','yes']);
        if ( $views == 'yes')
            $params['model'] = strtolower($params['name']);
        else
            $params['model'] = 0;

        //7.
        $params['show_side_menu']= $this->choice("Show in side menu?",['no','yes'])=='yes'?1:0;

        //8.
//        $params['soft_delete']= $this->choice("Allow SoftDelete",['no','yes'])=='yes'?1:0;

        //9.
        $migration = $this->choice("Need Migration?",['no','yes'])=='yes'?1:0;
        if ( $migration== 1 ){
            do{
                $ans = $this->ask("Enter values\n >key-fillable(0,1)-translatable(0,1)");
                try{
                    $ansList= explode('-',$ans);
                    $params['migration'][]= [
                        'key'=>$ansList[0],
                        'fillable'=>$ansList[1],
                        'translatable'=>$ansList[2],
                    ];
                }catch (\Exception $e){

                }
            }while($ans != -1);
        }

        //1.
        $this->line("Setting configurations...");

        if( $params['parent'] != -2 ) {
            if ($params['parent'] == -1) {
                setConfig("modules" . '.' . $params['name'], $params);
            } else {
                setConfig("modules." . $params['parent'] . '.' . $params['name'], $params);
            }
        }
        //2.
        // $this->line("Creating Table...");
        // if ( $params['table'] == -1){
        //     $this->line("There is no table...");
        // }else{
        //     Schema::create($params['table'], function(Blueprint $table) use ($params)
        //     {

        //     });
        //     $this->line("Created Successfully...");
        // }
        if ( $params['model'] !== 0){
            $this->line("Creating Model...");
            $this->model($params);
            $this->line("Model Created Successfully...");
        }
        if ( $params['controller'] !== 0 ){
            $this->line("Creating Controller...");
            $this->controller($params);
            $this->line("Controller Created Successfully...");
        }
        if ( $params['views'] == 1 ){
            $this->line("Creating Views...");
            $this->views($params);
            $this->line("Views Created Successfully...");
        }
        if ( $params['service'] == 1 ){
            $this->line("Creating Service...");
            $service= $this->service($params);
            Manifest::addProvider($service);
            $this->line("Service Created Successfully...");
        }

    }

    protected function views($params)
    {
        if (!is_dir(resource_path("views\\cms\\".$params['slug']))) {
            mkdir(resource_path("views\cms\\".$params['slug']));
        }
        file_put_contents(resource_path("views\cms\\".$params['slug']."\\index.blade.php"),'');
        file_put_contents(resource_path("views\cms\\".$params['slug']."\\edit.blade.php"),'');
        file_put_contents(resource_path("views\cms\\".$params['slug']."\\create.blade.php"),'');
        file_put_contents(resource_path("views\cms\\".$params['slug']."\\show.blade.php"),'');

    }

    protected function service($params)
    {
        $path = "App\Http\Controllers\CMS\\".$params['controller'];
        $modelTemplate = str_replace(
            ['{{name}}','{{slug}}','{{ControllerPath}}'],
            [$params['name'],$params['slug'],$path],
            file_get_contents(resource_path("stubs/cms/service.stub"))
        );
        if (!is_dir(app_path("Providers\Modules"))) {
            mkdir(app_path("Providers\Modules"));
        }
        file_put_contents(app_path("Providers\Modules\\".$params['name']."ServiceProvider.php"), $modelTemplate);
        return "\App\Providers\Modules\\".$params['name']."ServiceProvider";
    }

    protected function controller($params)
    {
        $modelTemplate = str_replace(
            ['{{name}}'],
            [$params['name']],
            file_get_contents(resource_path("stubs/cms/controller.stub"))
        );
        if (!is_dir(app_path("Http\Controllers\CMS"))) {
            mkdir(app_path("Http\Controllers\CMS"));
        }
        file_put_contents(app_path("Http\Controllers\CMS\\".$params['controller'].".php"), $modelTemplate);
    }


    protected function model($params)
    {
        $fillable = '';
        $translatable = '';
        try {
            foreach ($params['migration'] as $item) {
                if ($item['fillable'] == 1) {
                    if ($fillable == '') {
                        $fillable .= '"' . $item['key'] . '"';
                    } else {
                        $fillable .= ',' . '"' . $item['key'] . '"';
                    }
                }
                if ($item['translatable'] == 1) {
                    if ($translatable == '') {
                        $translatable .= '"' . $item['key'] . '"';
                    } else {
                        $translatable .= ',' . '"' . $item['key'] . '"';
                    }
                }
            }
        }catch (\Exception $ex){

        }
        $modelTemplate = str_replace(
            ['{{name}}','{{slug}}','{{fillable}}','{{translatable}}'],
            [$params['name'],$params['slug'],$fillable, $translatable],
            file_get_contents(resource_path("stubs/cms/model.stub"))
        );

        if (!is_dir(app_path("Models"))) {
            mkdir(app_path("Models"));
        }
        file_put_contents(app_path("Models\\".$params['name'].".php"), $modelTemplate);
    }


    protected function splitCamelCase($input)
    {
        return preg_split(
            '/(^[^A-Z]+|[A-Z][^A-Z]+)/',
            $input,
            -1, /* no limit for replacement count */
            PREG_SPLIT_NO_EMPTY /*don't return empty elements*/
            | PREG_SPLIT_DELIM_CAPTURE /*don't strip anything from output array*/
        );
    }
}
