<?php

namespace App\Console\Commands;

use App\Mail\sponsee36HourNotification;
use App\Mail\sponseeAfter72HourNotification;
use App\Mail\sponser36HourNotification;
use App\Mail\sponserAfter72HourNotification;
use App\Mail\sponserLessThan2CC;
use App\Models\Order;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class CronJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:crons';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $orders  = Order::query()->where('status','temporary')->get();

        $now = Carbon::now();
        Log::info("Entered Cron");

        foreach ( $orders as $order ){
            $date = Carbon::parse($order->created_at);
            if ( Carbon::parse($order->created_at)->addHours(72)->lessThan( $now )){
                $user = User::query()->where('_id',$order->user_id)->first();
                $url = 'https://webservice.foreverliving.fr/wsinscription.php';
                $ch = curl_init($url);
                $jsonData = array("action" => "droitDeParrainer",
                    "id_parrain"=> str_replace('-','',$order->distributor_number),
                );
                $jsonDataEncoded = json_encode($jsonData);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                $sponser = curl_exec($ch);
                $sponser = json_decode($sponser);


                if ( $sponser != null && $sponser->success){
                    try{
                        Mail::to($user->email)->send(new sponseeAfter72HourNotification([
                            's_first_name'=>$sponser->parrain->prenom,
                            's_last_name'=>$sponser->parrain->nom,
                            's_phone'=>$sponser->parrain->tel,
                            's_mail'=>$sponser->parrain->email,
                        ]));
                    }catch (\Exception $exception){

                    }

                    try{
                        Mail::to($sponser->parrain->email)->send(new sponserAfter72HourNotification([
                            's_first_name'=>$user->first_name,
                            's_last_name'=>$user->last_name,
                            's_phone'=>$user->phone_number,
                            's_mail'=>$user->email,
                        ]));

                    }catch (\Exception $exception){

                    }
                }

                $order->delete();
            }
            if (  $date->diffInHours($now) > 36 && $order->email_sent == null ){
                $order->email_sent = 1;
                $order->save();
                $user = User::query()->where('_id',$order->user_id)->first();
                App::setLocale($order->language== null ? "fr":$order->language);
                usleep(200);
//                Mail::to($user->email)->send(new sponsee36HourNotification($order));
                $url = 'https://webservice.foreverliving.fr/wsinscription.php';
                $ch = curl_init($url);
                $jsonData = array("action" => "droitDeParrainer",
                    "id_parrain"=> str_replace('-','',$order->distributor_number),
                );
                $jsonDataEncoded = json_encode($jsonData);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                $sponser = curl_exec($ch);
                $sponser = json_decode($sponser);


                if ( $sponser != null && $sponser->success){
                    try{
                        Mail::to($user->email)->send(new sponsee36HourNotification([
                            'id'=>$order->_id,
                            's_first_name'=>$sponser->parrain->prenom,
                            's_last_name'=>$sponser->parrain->nom,
                            's_phone'=>$sponser->parrain->tel,
                            's_mail'=>$sponser->parrain->email,
                        ]));
                    }catch (\Exception $exception){

                    }

                    try{
                        Mail::to($sponser->parrain->email)->send(new sponser36HourNotification([
                            'id'=>$order->_id,
                            's_first_name'=>$user->first_name,
                            's_last_name'=>$user->last_name,
                            's_phone'=>$user->phone_number,
                            's_mail'=>$user->email,
                        ]));

                    }catch (\Exception $exception){

                    }
                }

            }

        }
    }
}
