<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Schema;
use Jenssegers\Mongodb\Schema\Blueprint;

class gen_table extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:gen_table';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $data = getConfig('modules');
        $tables = array();
        $this->getTables($tables,$data);

        foreach ($tables as $table){
            if (Schema::hasTable($table)) {
                $this->line("$table exists");
            }else{
                Schema::create($table, function(Blueprint $table){
                    $table->unique('name');
                });
                $this->line("$table created Successfully...");
            }
        }
        return;
    }
    function getTables(&$tables , $arr){
        if ( is_array($arr) && Arr::has($arr, 'table') ){
            if ( $arr['table'] != -1){
                $tables[]= $arr['table'];
            }
        }else if ( is_array($arr) ){
            foreach ($arr as $k=>$v){
                $this->getTables($tables , $v);
            }
        }
    }
}
