<?php

namespace App\Providers\Modules;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class ProducerServiceProvider extends ServiceProvider {

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {

    }

    public function boot(Router $router)
    {
//                    $router->group(['prefix'=>'cms','middleware'=>['web','roles']], function ($router) {
//                    $router->resource('producer','App\Http\Controllers\CMS\ProducerController');
//                    $router->get('producer/{id}/up', 'App\Http\Controllers\CMS\ProducerController@up')->name('producer.up');
//                    $router->get('producer/{id}/down', 'App\Http\Controllers\CMS\ProducerController@down')->name('producer.down');
//                    $router->get('producer/{id}/translate', 'App\Http\Controllers\CMS\ProducerController@translate')->name('producer.translate');
//                    $router->post('producer/{id}/translate', 'App\Http\Controllers\CMS\ProducerController@translateStore')->name('producer.translate.post');
//                    //add custom routes
//                    $router->get('producer/{id}/delete-image', 'App\Http\Controllers\CMS\ProducerController@deleteImage')->name('producer.delete-image');

//                });
    }

}
