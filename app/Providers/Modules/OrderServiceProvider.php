<?php

namespace App\Providers\Modules;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class OrderServiceProvider extends ServiceProvider {

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {

    }

    public function boot(Router $router)
    {
                    $router->group(['prefix'=>'cms','middleware'=>['web','roles']], function ($router) {
                    $router->resource('order','App\Http\Controllers\CMS\OrderController');
                    $router->get('order/{id}/up', 'App\Http\Controllers\CMS\OrderController@up')->name('order.up');
                    $router->get('order/{id}/down', 'App\Http\Controllers\CMS\OrderController@down')->name('order.down');
                    $router->get('order/{id}/translate', 'App\Http\Controllers\CMS\OrderController@translate')->name('order.translate');
                    $router->post('order/{id}/translate', 'App\Http\Controllers\CMS\OrderController@translateStore')->name('order.translate.post');
                    //add custom routes

                });
    }

}
