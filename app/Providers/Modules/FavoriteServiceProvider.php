<?php

namespace App\Providers\Modules;

use Illuminate\Support\ServiceProvider;

class FavoriteServiceProvider extends ServiceProvider {

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {

    }

    public function boot()
    {
      $domain =  parse_url(config('app.url'), PHP_URL_HOST);
        $this->app['router']->group(['prefix'=>'cms', 'middleware'=>['web','roles']], function ($router) {
          $router->resource('favorites','App\Http\Controllers\CMS\FavoriteController');
          $router->get('favorites/{id}/up', 'App\Http\Controllers\CMS\FavoriteController@up')->name('favorites.up');
          $router->get('favorites/{id}/down', 'App\Http\Controllers\CMS\FavoriteController@down')->name('favorites.down');
          $router->get('favorites/{id}/translate', 'App\Http\Controllers\CMS\FavoriteController@translate')->name('favorites.translate');
          $router->post('favorites/{id}/translate', 'App\Http\Controllers\CMS\FavoriteController@translateStore')->name('favorites.translate.post');
          //add custom routes
          $router->get('favorites/{id}/delete-image', 'App\Http\Controllers\CMS\FavoriteController@deleteImage')->name('favorites.delete-image');
      });
    }

}
