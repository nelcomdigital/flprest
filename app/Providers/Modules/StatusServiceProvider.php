<?php

namespace App\Providers\Modules;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class StatusServiceProvider extends ServiceProvider {

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {

    }

    public function boot(Router $router)
    {
                    $router->group(['prefix'=>'cms','middleware'=>['web','roles']], function ($router) {
                    $router->resource('status','App\Http\Controllers\CMS\StatusController');
                    $router->get('status/{id}/up', 'App\Http\Controllers\CMS\StatusController@up')->name('status.up');
                    $router->get('status/{id}/down', 'App\Http\Controllers\CMS\StatusController@down')->name('status.down');
                    $router->get('status/{id}/translate', 'App\Http\Controllers\CMS\StatusController@translate')->name('status.translate');
                    $router->post('status/{id}/translate', 'App\Http\Controllers\CMS\StatusController@translateStore')->name('status.translate.post');
                    //add custom routes

                });
    }

}
