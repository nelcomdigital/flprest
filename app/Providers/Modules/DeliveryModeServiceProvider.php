<?php

namespace App\Providers\Modules;

use Illuminate\Support\ServiceProvider;

class DeliveryModeServiceProvider extends ServiceProvider {

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {

    }

    public function boot()
    {
      $domain =  parse_url(config('app.url'), PHP_URL_HOST);
        $this->app['router']->group(['prefix'=>'cms', 'middleware'=>['web','roles']], function ($router) {
          $router->resource('delivery-mode','App\Http\Controllers\CMS\DeliveryModeController');
          $router->get('delivery-mode/{id}/up', 'App\Http\Controllers\CMS\DeliveryModeController@up')->name('delivery-mode.up');
          $router->get('delivery-mode/{id}/down', 'App\Http\Controllers\CMS\DeliveryModeController@down')->name('delivery-mode.down');
          $router->get('delivery-mode/{id}/translate', 'App\Http\Controllers\CMS\DeliveryModeController@translate')->name('delivery-mode.translate');
          $router->post('delivery-mode/{id}/translate', 'App\Http\Controllers\CMS\DeliveryModeController@translateStore')->name('delivery-mode.translate.post');
          //add custom routes
          $router->get('delivery-mode/{id}/delete-image', 'App\Http\Controllers\CMS\DeliveryModeController@deleteImage')->name('delivery-mode.delete-image');
      });
    }

}
