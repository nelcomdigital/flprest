<?php

namespace App\Providers\Modules;

use Illuminate\Support\ServiceProvider;

class InternalNotificationServiceProvider extends ServiceProvider {

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {

    }

    public function boot()
    {
        $domain =  parse_url(config('app.url'), PHP_URL_HOST);
        $this->app['router']->group(['prefix'=>'cms', 'middleware'=>['web','roles']], function ($router) {
            $router->resource('internal-notification','App\Http\Controllers\CMS\InternalNotificationController');
            $router->get('internal-notification/{id}/up', 'App\Http\Controllers\CMS\InternalNotificationController@up')->name('notification-role.up');
            $router->get('internal-notification/{id}/down', 'App\Http\Controllers\CMS\InternalNotificationController@down')->name('notification-role.down');
            $router->get('internal-notification/{id}/translate', 'App\Http\Controllers\CMS\InternalNotificationController@translate')->name('notification-role.translate');
            $router->post('internal-notification/{id}/translate', 'App\Http\Controllers\CMS\InternalNotificationController@translateStore')->name('notification-role.translate.post');
            //add custom routes
            $router->get('internal-notification/{id}/delete-image', 'App\Http\Controllers\CMS\InternalNotificationController@deleteImage')->name('notification-role.delete-image');
        });
    }

}
