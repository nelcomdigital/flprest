<?php

namespace App\Providers\Modules;

use Illuminate\Support\ServiceProvider;

class MainMenuServiceProvider extends ServiceProvider {

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {

    }

    public function boot()
    {
      $domain =  parse_url(config('app.url'), PHP_URL_HOST);
        $this->app['router']->group(['prefix'=>'cms', 'middleware'=>['web','roles']], function ($router) {
          $router->resource('main-menu','App\Http\Controllers\CMS\MainMenuController');
          $router->get('main-menu/{id}/up', 'App\Http\Controllers\CMS\MainMenuController@up')->name('main-menu.up');
          $router->get('main-menu/{id}/down', 'App\Http\Controllers\CMS\MainMenuController@down')->name('main-menu.down');
          $router->get('main-menu/{id}/translate', 'App\Http\Controllers\CMS\MainMenuController@translate')->name('main-menu.translate');
          $router->post('main-menu/{id}/translate', 'App\Http\Controllers\CMS\MainMenuController@translateStore')->name('main-menu.translate.post');
          //add custom routes
          $router->get('main-menu/{id}/delete-image', 'App\Http\Controllers\CMS\MainMenuController@deleteImage')->name('main-menu.delete-image');
      });
    }

}
