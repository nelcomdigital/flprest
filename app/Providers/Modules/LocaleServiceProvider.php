<?php

namespace App\Providers\Modules;

use Illuminate\Support\ServiceProvider;

class LocaleServiceProvider extends ServiceProvider {

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {

    }

    public function boot()
    {
     
        $this->app['router']->group(['prefix'=>'cms','middleware'=>['web']], function ($router) {
          $router->resource('locale','App\Http\Controllers\CMS\LocaleController');
          $router->get('locale/{id}/up', 'App\Http\Controllers\CMS\LocaleController@up')->name('locale.up');
          $router->get('locale/{id}/down', 'App\Http\Controllers\CMS\LocaleController@down')->name('locale.down');
          $router->get('locale/{id}/translate', 'App\Http\Controllers\CMS\LocaleController@translate')->name('locale.translate');
          $router->post('locale/{id}/translate', 'App\Http\Controllers\CMS\LocaleController@translateStore')->name('locale.translate.post');
          //add custom routes
          $router->get('locale/{id}/delete-image', 'App\Http\Controllers\CMS\LocaleController@deleteImage')->name('locale.delete-image');
      });
    }

}
