<?php

namespace App\Providers\Modules;

use Illuminate\Support\ServiceProvider;

class NotificationRoleServiceProvider extends ServiceProvider {

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {

    }

    public function boot()
    {
      $domain =  parse_url(config('app.url'), PHP_URL_HOST);
        $this->app['router']->group(['prefix'=>'cms', 'middleware'=>['web','roles']], function ($router) {
          $router->resource('notification-role','App\Http\Controllers\CMS\NotificationRoleController');
          $router->get('notification-role/{id}/up', 'App\Http\Controllers\CMS\NotificationRoleController@up')->name('notification-role.up');
          $router->get('notification-role/{id}/down', 'App\Http\Controllers\CMS\NotificationRoleController@down')->name('notification-role.down');
          $router->get('notification-role/{id}/translate', 'App\Http\Controllers\CMS\NotificationRoleController@translate')->name('notification-role.translate');
          $router->post('notification-role/{id}/translate', 'App\Http\Controllers\CMS\NotificationRoleController@translateStore')->name('notification-role.translate.post');
          //add custom routes
          $router->get('notification-role/{id}/delete-image', 'App\Http\Controllers\CMS\NotificationRoleController@deleteImage')->name('notification-role.delete-image');
      });
    }

}
