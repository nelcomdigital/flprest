<?php

namespace App\Providers\Modules;

use Illuminate\Support\ServiceProvider;

class AnnouncementServiceProvider extends ServiceProvider {

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {

    }

    public function boot()
    {
      $domain =  parse_url(config('app.url'), PHP_URL_HOST);
        $this->app['router']->group(['prefix'=>'cms', 'middleware'=>['web','roles']], function ($router) {
          $router->resource('announcement','App\Http\Controllers\CMS\AnnouncementController');
          $router->get('announcement/{id}/up', 'App\Http\Controllers\CMS\AnnouncementController@up')->name('announcement.up');
          $router->get('announcement/{id}/down', 'App\Http\Controllers\CMS\AnnouncementController@down')->name('announcement.down');
          $router->get('announcement/{id}/translate', 'App\Http\Controllers\CMS\AnnouncementController@translate')->name('announcement.translate');
          $router->post('announcement/{id}/translate', 'App\Http\Controllers\CMS\AnnouncementController@translateStore')->name('announcement.translate.post');
          //add custom routes
          $router->get('announcement/{id}/delete-image', 'App\Http\Controllers\CMS\AnnouncementController@deleteImage')->name('announcement.delete-image');
      });
    }

}
