<?php

namespace App\Providers\Modules;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class ProductServiceProvider extends ServiceProvider
{
    
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
    
    }
    
    public function boot(Router $router)
    {
        $router->group(['prefix' => 'cms', 'middleware' => ['web', 'roles']], function ($router) {
            $router->resource('product', 'App\Http\Controllers\CMS\ProductController');
            $router->get('product/{id}/up', 'App\Http\Controllers\CMS\ProductController@up')->name('product.up');
            $router->get('product/{id}/down', 'App\Http\Controllers\CMS\ProductController@down')->name('product.down');
            $router->get('product/{id}/translate', 'App\Http\Controllers\CMS\ProductController@translate')->name('product.translate');
            $router->post('product/{id}/translate', 'App\Http\Controllers\CMS\ProductController@translateStore')->name('product.translate.post');
            $router->post('product/store', 'App\Http\Controllers\CMS\ProductController@store')->name('product.store');
            //add custom routes
            $router->get('product/{id}/delete-image', 'App\Http\Controllers\CMS\ProductController@deleteImage')->name('product.delete-image');
            
        });
    }
    
}
