<?php

namespace App\Providers\Modules;

class Manifest {
    public static function providers() {
        $jsonString = file_get_contents(base_path("custom-config/providers.php"));
        $data = eval('?>'.$jsonString);
        return  $data;
   }

   public static function addProvider($value){
       $jsonString = file_get_contents(base_path("custom-config/providers.php"));
       $data = eval('?>'.$jsonString);
       $data[]=$value;

       $file = "<?php \n\n return [\n\n";
       foreach ($data as $d){
           $file.=$d."::class,\n";
       }
       $file.="\n];";

       file_put_contents(base_path("custom-config/providers.php"), $file);

       return $data;
   }
}
