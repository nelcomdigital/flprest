<?php

namespace App\Providers\Modules;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class RegionServiceProvider extends ServiceProvider {

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {

    }

    public function boot(Router $router)
    {
                    $router->group(['prefix'=>'cms','middleware'=>['web','roles']], function ($router) {
                    $router->resource('region','App\Http\Controllers\CMS\RegionController');
                    $router->get('region/{id}/up', 'App\Http\Controllers\CMS\RegionController@up')->name('region.up');
                    $router->get('region/{id}/down', 'App\Http\Controllers\CMS\RegionController@down')->name('region.down');
                    $router->get('region/{id}/translate', 'App\Http\Controllers\CMS\RegionController@translate')->name('region.translate');
                    $router->post('region/{id}/translate', 'App\Http\Controllers\CMS\RegionController@translateStore')->name('region.translate.post');
                    //add custom routes

                });
    }

}
