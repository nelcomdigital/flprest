<?php

namespace App\Providers\Modules;

use App\User;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class RoleServiceProvider extends ServiceProvider {

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {

    }

    public function boot(Router $router)
    {
            $router->group(['prefix'=>'cms','middleware'=>['web','roles']], function ($router) {
                $router->resource('role','App\Http\Controllers\CMS\RoleController');
                $router->get('role/{id}/up', 'App\Http\Controllers\CMS\RoleController@up')->name('role.up');
                $router->get('role/{id}/down', 'App\Http\Controllers\CMS\RoleController@down')->name('role.down');
                $router->get('role/{id}/translate', 'App\Http\Controllers\CMS\RoleController@translate')->name('role.translate');
                $router->post('role/{id}/translate', 'App\Http\Controllers\CMS\RoleController@translateStore')->name('role.translate.post');
                //add custom routes

            });
    }

}
