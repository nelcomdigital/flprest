<?php

namespace App\Providers\Modules;

use Illuminate\Support\ServiceProvider;

class BusinessCategoryServiceProvider extends ServiceProvider {

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {

    }

    public function boot()
    {
      $domain =  parse_url(config('app.url'), PHP_URL_HOST);
        $this->app['router']->group(['prefix'=>'cms', 'middleware'=>['web','roles']], function ($router) {
          $router->resource('business-category','App\Http\Controllers\CMS\BusinessCategoryController');
          $router->get('business-category/{id}/up', 'App\Http\Controllers\CMS\BusinessCategoryController@up')->name('business-category.up');
          $router->get('business-category/{id}/down', 'App\Http\Controllers\CMS\BusinessCategoryController@down')->name('business-category.down');
          $router->get('business-category/{id}/translate', 'App\Http\Controllers\CMS\BusinessCategoryController@translate')->name('business-category.translate');
          $router->post('business-category/{id}/translate', 'App\Http\Controllers\CMS\BusinessCategoryController@translateStore')->name('business-category.translate.post');
          //add custom routes
          $router->get('business-category/{id}/delete-image', 'App\Http\Controllers\CMS\BusinessCategoryController@deleteImage')->name('business-category.delete-image');
      });
    }

}
