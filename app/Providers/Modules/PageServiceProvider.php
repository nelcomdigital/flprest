<?php

namespace App\Providers\Modules;

use Illuminate\Support\ServiceProvider;

class PageServiceProvider extends ServiceProvider {

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {

    }

    public function boot()
    {
        $this->app['router']->group(['prefix'=>'cms','middleware'=>['web','roles']], function ($router) {
          $router->resource('page','App\Http\Controllers\CMS\PageController');
          $router->get('page/{id}/up', 'App\Http\Controllers\CMS\PageController@up')->name('page.up');
          $router->get('page/{id}/down', 'App\Http\Controllers\CMS\PageController@down')->name('page.down');
          $router->get('page/{id}/translate', 'App\Http\Controllers\CMS\PageController@translate')->name('page.translate');
          $router->post('page/{id}/translate', 'App\Http\Controllers\CMS\PageController@translateStore')->name('page.translate.post');
          //add custom routes
          $router->get('page/{id}/delete-image', 'App\Http\Controllers\CMS\PageController@deleteImage')->name('page.delete-image');
      });
    }

}
