<?php

namespace App\Providers\Modules;

use Illuminate\Support\ServiceProvider;

class CityServiceProvider extends ServiceProvider
{
    
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
    
    }
    
    public function boot()
    {
        $this->app['router']->group(['prefix' => 'cms', 'middleware' => ['web', 'roles']], function ($router) {
            $router->resource('city', 'App\Http\Controllers\CMS\CityController');
            $router->get('city/{id}/up', 'App\Http\Controllers\CMS\CityController@up')->name('city.up');
            $router->get('city/{id}/down', 'App\Http\Controllers\CMS\CityController@down')->name('city.down');
            $router->get('city/{id}/translate', 'App\Http\Controllers\CMS\CityController@translate')->name('city.translate');
            $router->post('city/{id}/translate', 'App\Http\Controllers\CMS\CityController@translateStore')->name('city.translate.post');
            //add custom routes
            $router->get('city/{id}/delete-image', 'App\Http\Controllers\CMS\CityController@deleteImage')->name('city.delete-image');
        });
    }
    
}
