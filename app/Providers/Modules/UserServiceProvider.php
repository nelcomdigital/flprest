<?php

namespace App\Providers\Modules;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class UserServiceProvider extends ServiceProvider
{

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {

    }

    public function boot(Router $router)
    {
        $router->group(['prefix' => 'cms', 'middleware' => ['web', 'roles']], function ($router) {
            $router->resource('user', 'App\Http\Controllers\CMS\UserController');
            $router->get('user/{id}/up', 'App\Http\Controllers\CMS\UserController@up')->name('user.up');
            $router->get('user/{id}/down', 'App\Http\Controllers\CMS\UserController@down')->name('user.down');
            $router->get('user/{id}/translate', 'App\Http\Controllers\CMS\UserController@translate')->name('user.translate');
            $router->post('user/{id}/translate', 'App\Http\Controllers\CMS\UserController@translateStore')->name('user.translate.post');
            //add custom routes
            $router->get('user/{id}/delete-image', 'App\Http\Controllers\CMS\UserController@deleteImage')->name('user.delete-image');
            //add custom routes
            $router->get('profile', 'App\Http\Controllers\CMS\UserController@editProfile')->name('user.profile');
            $router->post('profile/update', 'App\Http\Controllers\CMS\UserController@updateProfile')->name('user.update-profile');


        });
    }

}
