<?php

namespace App\Providers\Modules;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class CurrencyServiceProvider extends ServiceProvider {

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {

    }

    public function boot(Router $router)
    {
                    $router->group(['prefix'=>'cms','middleware'=>['web','roles']], function ($router) {
                    $router->resource('currency','App\Http\Controllers\CMS\CurrencyController');
                    $router->get('currency/{id}/up', 'App\Http\Controllers\CMS\CurrencyController@up')->name('currency.up');
                    $router->get('currency/{id}/down', 'App\Http\Controllers\CMS\CurrencyController@down')->name('currency.down');
                    $router->get('currency/{id}/translate', 'App\Http\Controllers\CMS\CurrencyController@translate')->name('currency.translate');
                    $router->post('currency/{id}/translate', 'App\Http\Controllers\CMS\CurrencyController@translateStore')->name('currency.translate.post');
                    //add custom routes

                });
    }

}
