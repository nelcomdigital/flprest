<?php

namespace App\Providers\Modules;

use Illuminate\Support\ServiceProvider;

class VideoServiceProvider extends ServiceProvider {

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {

    }

    public function boot()
    {
      $domain =  parse_url(config('app.url'), PHP_URL_HOST);
        $this->app['router']->group(['prefix'=>'cms', 'middleware'=>['web','roles']], function ($router) {
          $router->resource('video','App\Http\Controllers\CMS\VideoController');
          $router->get('video/{id}/up', 'App\Http\Controllers\CMS\VideoController@up')->name('video.up');
          $router->get('video/{id}/down', 'App\Http\Controllers\CMS\VideoController@down')->name('video.down');
          $router->get('video/{id}/translate', 'App\Http\Controllers\CMS\VideoController@translate')->name('video.translate');
          $router->post('video/{id}/translate', 'App\Http\Controllers\CMS\VideoController@translateStore')->name('video.translate.post');
          //add custom routes
          $router->get('video/{id}/delete-image', 'App\Http\Controllers\CMS\VideoController@deleteImage')->name('video.delete-image');
      });
    }

}
