<?php

namespace App\Providers\Modules;

use Illuminate\Support\ServiceProvider;

class ContactServiceProvider extends ServiceProvider {

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {

    }

    public function boot()
    {
      $domain =  parse_url(config('app.url'), PHP_URL_HOST);
        $this->app['router']->group(['prefix'=>'cms', 'middleware'=>['web','roles']], function ($router) {
          $router->resource('contact','App\Http\Controllers\CMS\ContactController');
          $router->get('contact/{id}/up', 'App\Http\Controllers\CMS\ContactController@up')->name('contact.up');
          $router->get('contact/{id}/down', 'App\Http\Controllers\CMS\ContactController@down')->name('contact.down');
          $router->get('contact/{id}/translate', 'App\Http\Controllers\CMS\ContactController@translate')->name('contact.translate');
          $router->post('contact/{id}/translate', 'App\Http\Controllers\CMS\ContactController@translateStore')->name('contact.translate.post');
          //add custom routes
          $router->get('contact/{id}/delete-image', 'App\Http\Controllers\CMS\ContactController@deleteImage')->name('contact.delete-image');
      });
    }

}
