<?php

namespace App\Providers\Modules;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class ProductCategoryServiceProvider extends ServiceProvider
{
    
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
    
    }
    
    public function boot(Router $router)
    {
        $router->group(['prefix' => 'cms', 'middleware' => ['web', 'roles']], function ($router) {
            $router->resource('product-category', 'App\Http\Controllers\CMS\ProductCategoryController');
            $router->get('product-category/{id}/up', 'App\Http\Controllers\CMS\ProductCategoryController@up')->name('product-category.up');
            $router->get('product-category/{id}/down', 'App\Http\Controllers\CMS\ProductCategoryController@down')->name('product-category.down');
            $router->get('product-category/{id}/translate', 'App\Http\Controllers\CMS\ProductCategoryController@translate')->name('product-category.translate');
            $router->post('product-category/{id}/translate', 'App\Http\Controllers\CMS\ProductCategoryController@translateStore')->name('product-category.translate.post');
            $router->post('product-category/store', 'App\Http\Controllers\CMS\ProductCategoryController@store')->name('product-category.translate.post');
            //add custom routes
            $router->get('product-category/{id}/delete-image', 'App\Http\Controllers\CMS\ProductCategoryController@deleteImage')->name('product-category.delete-image');
            
        });
    }
    
}
