<?php

namespace App\Providers\Modules;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class CountryServiceProvider extends ServiceProvider {

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {

    }

    public function boot(Router $router)
    {
                    $router->group(['prefix'=>'cms','middleware'=>['web','roles']], function ($router) {
                    $router->resource('country','App\Http\Controllers\CMS\CountryController');
                    $router->get('country/{id}/up', 'App\Http\Controllers\CMS\CountryController@up')->name('country.up');
                    $router->get('country/{id}/down', 'App\Http\Controllers\CMS\CountryController@down')->name('country.down');
                    $router->get('country/{id}/translate', 'App\Http\Controllers\CMS\CountryController@translate')->name('country.translate');
                    $router->post('country/{id}/translate', 'App\Http\Controllers\CMS\CountryController@translateStore')->name('country.translate.post');
                    //add custom routes

                });
    }

}
