<?php

namespace App\Providers\Modules;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class DepartmentServiceProvider extends ServiceProvider {

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {

    }

    public function boot(Router $router)
    {
            $router->group(['prefix'=>'cms','middleware'=>['web','roles']], function ($router) {
            $router->resource('department','App\Http\Controllers\CMS\DepartmentController');
            $router->get('department/{id}/up', 'App\Http\Controllers\CMS\DepartmentController@up')->name('department.up');
            $router->get('department/{id}/down', 'App\Http\Controllers\CMS\DepartmentController@down')->name('department.down');
            $router->get('department/{id}/translate', 'App\Http\Controllers\CMS\DepartmentController@translate')->name('department.translate');
            $router->post('department/{id}/translate', 'App\Http\Controllers\CMS\DepartmentController@translateStore')->name('department.translate.post');
            //add custom routes
        });
    }

}
