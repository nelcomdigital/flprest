<?php

namespace App\Providers\Modules;

use Illuminate\Support\ServiceProvider;

class BusinessFileServiceProvider extends ServiceProvider {

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {

    }

    public function boot()
    {
      $domain =  parse_url(config('app.url'), PHP_URL_HOST);
        $this->app['router']->group(['prefix'=>'cms', 'middleware'=>['web','roles']], function ($router) {
          $router->resource('business-file','App\Http\Controllers\CMS\BusinessFileController');
          $router->get('business-file/{id}/up', 'App\Http\Controllers\CMS\BusinessFileController@up')->name('business-file.up');
          $router->get('business-file/{id}/down', 'App\Http\Controllers\CMS\BusinessFileController@down')->name('business-file.down');
          $router->get('business-file/{id}/translate', 'App\Http\Controllers\CMS\BusinessFileController@translate')->name('business-file.translate');
          $router->post('business-file/{id}/translate', 'App\Http\Controllers\CMS\BusinessFileController@translateStore')->name('business-file.translate.post');
          //add custom routes
          $router->get('business-file/{id}/delete-image', 'App\Http\Controllers\CMS\BusinessFileController@deleteImage')->name('business-file.delete-image');
      });
    }

}
