<?php

namespace App\Providers\Modules;

use Illuminate\Support\ServiceProvider;

class GroupServiceProvider extends ServiceProvider {

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {

    }

    public function boot()
    {
      $domain =  parse_url(config('app.url'), PHP_URL_HOST);
        $this->app['router']->group(['prefix'=>'cms', 'middleware'=>['web','roles']], function ($router) {
          $router->resource('group','App\Http\Controllers\CMS\GroupController');
          $router->get('group/{id}/up', 'App\Http\Controllers\CMS\GroupController@up')->name('group.up');
          $router->get('group/{id}/down', 'App\Http\Controllers\CMS\GroupController@down')->name('group.down');
          $router->get('group/{id}/translate', 'App\Http\Controllers\CMS\GroupController@translate')->name('group.translate');
          $router->post('group/{id}/translate', 'App\Http\Controllers\CMS\GroupController@translateStore')->name('group.translate.post');
          //add custom routes
          $router->get('group/{id}/delete-image', 'App\Http\Controllers\CMS\GroupController@deleteImage')->name('group.delete-image');
      });
    }

}
