<?php

namespace App\Providers\Modules;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class SliderServiceProvider extends ServiceProvider {

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {

    }

    public function boot(Router $router)
    {
                    $router->group(['prefix'=>'cms','middleware'=>['web','roles']], function ($router) {
                    $router->resource('slider','App\Http\Controllers\CMS\SliderController');
                    $router->get('slider/{id}/up', 'App\Http\Controllers\CMS\SliderController@up')->name('slider.up');
                    $router->get('slider/{id}/down', 'App\Http\Controllers\CMS\SliderController@down')->name('slider.down');
                    $router->get('slider/{id}/translate', 'App\Http\Controllers\CMS\SliderController@translate')->name('slider.translate');
                    $router->post('slider/{id}/translate', 'App\Http\Controllers\CMS\SliderController@translateStore')->name('slider.translate.post');
                    //add custom routes
                    $router->get('slider/{id}/delete-image', 'App\Http\Controllers\CMS\SliderController@deleteImage')->name('slider.delete-image');

                });
    }

}
