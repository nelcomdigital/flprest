<?php

namespace App\Providers\Modules;

use Illuminate\Support\ServiceProvider;

class UserObjectServiceProvider extends ServiceProvider {

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {

    }

    public function boot()
    {
      $domain =  parse_url(config('app.url'), PHP_URL_HOST);
        $this->app['router']->group(['prefix'=>'cms', 'middleware'=>['web','roles']], function ($router) {
          $router->resource('user-object','App\Http\Controllers\CMS\UserObjectController');
          $router->get('user-object/{id}/up', 'App\Http\Controllers\CMS\UserObjectController@up')->name('user-object.up');
          $router->get('user-object/{id}/down', 'App\Http\Controllers\CMS\UserObjectController@down')->name('user-object.down');
          $router->get('user-object/{id}/translate', 'App\Http\Controllers\CMS\UserObjectController@translate')->name('user-object.translate');
          $router->post('user-object/{id}/translate', 'App\Http\Controllers\CMS\UserObjectController@translateStore')->name('user-object.translate.post');
          //add custom routes
          $router->get('user-object/{id}/delete-image', 'App\Http\Controllers\CMS\UserObjectController@deleteImage')->name('user-object.delete-image');
      });
    }

}
