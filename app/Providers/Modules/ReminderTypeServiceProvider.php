<?php

namespace App\Providers\Modules;

use Illuminate\Support\ServiceProvider;

class ReminderTypeServiceProvider extends ServiceProvider {

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {

    }

    public function boot()
    {
      $domain =  parse_url(config('app.url'), PHP_URL_HOST);
        $this->app['router']->group(['prefix'=>'cms', 'middleware'=>['web','roles']], function ($router) {
          $router->resource('reminder-type','App\Http\Controllers\CMS\ReminderTypeController');
          $router->get('reminder-type/{id}/up', 'App\Http\Controllers\CMS\ReminderTypeController@up')->name('reminder-type.up');
          $router->get('reminder-type/{id}/down', 'App\Http\Controllers\CMS\ReminderTypeController@down')->name('reminder-type.down');
          $router->get('reminder-type/{id}/translate', 'App\Http\Controllers\CMS\ReminderTypeController@translate')->name('reminder-type.translate');
          $router->post('reminder-type/{id}/translate', 'App\Http\Controllers\CMS\ReminderTypeController@translateStore')->name('reminder-type.translate.post');
          //add custom routes
          $router->get('reminder-type/{id}/delete-image', 'App\Http\Controllers\CMS\ReminderTypeController@deleteImage')->name('reminder-type.delete-image');
      });
    }

}
