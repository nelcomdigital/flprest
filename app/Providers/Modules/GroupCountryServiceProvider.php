<?php

namespace App\Providers\Modules;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class GroupCountryServiceProvider extends ServiceProvider {

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {

    }

    public function boot(Router $router)
    {
                    $router->group(['prefix'=>'cms','middleware'=>['web','roles']], function ($router) {
                    $router->resource('group-country','App\Http\Controllers\CMS\GroupCountryController');
                    $router->get('group-country/{id}/up', 'App\Http\Controllers\CMS\GroupCountryController@up')->name('group-country.up');
                    $router->get('group-country/{id}/down', 'App\Http\Controllers\CMS\GroupCountryController@down')->name('group-country.down');
                    $router->get('group-country/{id}/translate', 'App\Http\Controllers\CMS\GroupCountryController@translate')->name('group-country.translate');
                    $router->post('group-country/{id}/translate', 'App\Http\Controllers\CMS\GroupCountryController@translateStore')->name('group-country.translate.post');
                    //add custom routes

                });
    }

}
