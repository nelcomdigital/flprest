<?php

namespace App\Providers\Modules;

use Illuminate\Support\ServiceProvider;

class ReminderServiceProvider extends ServiceProvider {

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {

    }

    public function boot()
    {
      $domain =  parse_url(config('app.url'), PHP_URL_HOST);
        $this->app['router']->group(['prefix'=>'cms', 'middleware'=>['web','roles']], function ($router) {
          $router->resource('reminder','App\Http\Controllers\CMS\ReminderController');
          $router->get('reminder/{id}/up', 'App\Http\Controllers\CMS\ReminderController@up')->name('reminder.up');
          $router->get('reminder/{id}/down', 'App\Http\Controllers\CMS\ReminderController@down')->name('reminder.down');
          $router->get('reminder/{id}/translate', 'App\Http\Controllers\CMS\ReminderController@translate')->name('reminder.translate');
          $router->post('reminder/{id}/translate', 'App\Http\Controllers\CMS\ReminderController@translateStore')->name('reminder.translate.post');
          //add custom routes
          $router->get('reminder/{id}/delete-image', 'App\Http\Controllers\CMS\ReminderController@deleteImage')->name('reminder.delete-image');
      });
    }

}
