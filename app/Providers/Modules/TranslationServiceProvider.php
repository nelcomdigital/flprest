<?php

namespace App\Providers\Modules;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class TranslationServiceProvider extends ServiceProvider {

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {

    }

    public function boot(Router $router)
    {
                    $router->group(['prefix'=>'cms','middleware'=>['web','roles']], function ($router) {
                    $router->resource('translation','App\Http\Controllers\CMS\TranslationController');
                    $router->get('translation/{id}/up', 'App\Http\Controllers\CMS\TranslationController@up')->name('translation.up');
                    $router->get('translation/{id}/down', 'App\Http\Controllers\CMS\TranslationController@down')->name('translation.down');
                    $router->get('translation/{id}/translate', 'App\Http\Controllers\CMS\TranslationController@translate')->name('translation.translate');
                    $router->post('translation/{id}/translate', 'App\Http\Controllers\CMS\TranslationController@translateStore')->name('translation.translate.post');
                    $router->get('inject/translation', 'App\Http\Controllers\CMS\TranslationController@inject')->name('translation.inject');
                    $router->get('export/translation', 'App\Http\Controllers\CMS\TranslationController@Export')->name('translation.export');
                    //add custom routes



        });
    }

}
