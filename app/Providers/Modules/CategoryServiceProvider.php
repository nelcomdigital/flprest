<?php

namespace App\Providers\Modules;

use Illuminate\Support\ServiceProvider;

class CategoryServiceProvider extends ServiceProvider {

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {

    }

    public function boot()
    {
      $domain =  parse_url(config('app.url'), PHP_URL_HOST);
        $this->app['router']->group(['prefix'=>'cms', 'middleware'=>['web','roles']], function ($router) {
          $router->resource('category','App\Http\Controllers\CMS\CategoryController');
          $router->get('category/{id}/up', 'App\Http\Controllers\CMS\CategoryController@up')->name('category.up');
          $router->get('category/{id}/down', 'App\Http\Controllers\CMS\CategoryController@down')->name('category.down');
          $router->get('category/{id}/translate', 'App\Http\Controllers\CMS\CategoryController@translate')->name('category.translate');
          $router->post('category/{id}/translate', 'App\Http\Controllers\CMS\CategoryController@translateStore')->name('category.translate.post');
          //add custom routes
          $router->get('category/{id}/delete-image', 'App\Http\Controllers\CMS\CategoryController@deleteImage')->name('category.delete-image');
      });
    }

}
