<?php

namespace App\Providers;
use App\Models\Country;
use App\Models\Domain;
use App\Models\GeneralConfiguration;
use App\Models\Locale;

use Illuminate\Support\ServiceProvider;

class GlobalTemplateServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(['cms.layouts.blocks.input.loggers','cms.layouts.partials.header'], function ($view) {
            $view->with('locales', $this->getLocales());
            $view->with('countries', $this->getCountries());
            $view->with('domains', $this->getAllDomains());
        });
        view()->composer(['cms.role.create','cms.role.edit'], function ($view) {
            $view->with('domains', $this->getAllDomains());
        });
        view()->composer(['cms.auth.login'], function ($view) {
            $view->with('settings', $this->getSettings());
        });
        view()->composer(['cms.layouts.app'], function ($view) {
            $view->with('settings', $this->getSettings());
        });

        view()->composer(['cms.layouts.blocks.input.week-times',
            'frontend.business.profile.services.venues.layouts.week-times'], function ($view) {
            $view->with('days', [
                                __('cms.monday'),
                                __('cms.tuesday'),
                                __('cms.wednesday'),
                                __('cms.thursday'),
                                __('cms.friday'),
                                __('cms.saturday'),
                                __('cms.sunday'),
                            ]
                        );
        });

    }
    private function getSettings(){
        $setting = array();
        $settings = GeneralConfiguration::all();
        foreach ($settings as $set) {
            $setting [$set->key] = $set->value;
        }
        return $setting;
    }

    private function getCountries(){
        return Country::query()->where('is_active', '1')->orderBy('name', 'asc')->get();
    }
    
    private function getAllDomains(){
        return Domain::query()->get();
    }

    private function getLocales(){
        return Locale::query()->orderBy('name')->get();
    }
}
