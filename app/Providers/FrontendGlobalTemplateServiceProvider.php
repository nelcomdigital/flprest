<?php

namespace App\Providers;

use App\Models\ProductCategory;
use App\Models\Slider;
use Illuminate\Support\ServiceProvider;

class FrontendGlobalTemplateServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

        view()->composer(['frontend.product-list'], function ($view) {
            $view->with('slider', $this->getSlider());
        });
        view()->composer(['frontend.layouts.partials.header'], function ($view) {
            $view->with('cart', $this->getCart());
        });
    }



    private function getSlider(){
        return Slider::query()->whereHas('locale',function($q){
            $q->where('code',session()->get('current_locale','fr'));
        })->where('is_active','1')->first();
    }

    private function getCart(){
        $items= session()->get('process.items');
        $count = 0;
        if ($items != null)
            foreach ( $items as $k =>$v ){
                $count+= $v;
            }
        return $count==0? '': $count;
    }
}
