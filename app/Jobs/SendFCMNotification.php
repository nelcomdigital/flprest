<?php

namespace App\Jobs;

use App\Models\Notification;
use App\Models\MobUser;
use App\Notifications;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use LaravelFCM\Facades\FCM;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;

class SendFCMNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data;


    public function __construct($data)
    {
        //
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $this->sendNotification($this->data);
    }

    public function sendNotification($data){
        // if ($data['from'] != null)
        //     $fromUser = User::find($data['from']);
        // else
        $fromUser = 'admin';
        $toUser =  MobUser::query()->where('num', $data['to'])->first();

        $notificationData = array();

        $notificationData['user_id'] = $toUser->num;
        $notificationData['module'] = $data['module'];
        $notificationData['name'] = $data['name'];
        $notificationData['message'] = $data['message'];
        $notificationData['order_id'] = $data['order_id'];
        $notificationData['training_id'] = $data['training_id'];
        $notificationData['event_id'] = $data['event_id'];
        $notificationData['rdv_id'] = $data['rdv_id'];
        $notificationData['other_meeting_id'] = $data['other_meeting_id'];
        $notificationData['podcast_id'] = $data['podcast_id'];
        $notificationData['participation_date'] = $data['participation_date'];
        $notificationData['related_id'] = 'admin';

        $not = Notification::create($notificationData);

        if ($not != null) {
            try {
                $optionBuiler = new OptionsBuilder();
                $optionBuiler->setTimeToLive(60 * 20);

                $notificationBuilder = new PayloadNotificationBuilder(env('APP_NAME',""));
                $notificationBuilder->setBody($data['message'])
                    ->setSound('default');

                $dataBuilder = new PayloadDataBuilder();
                $dataBuilder->addData(['a_data' => $notificationData]);

                $option = $optionBuiler->build();
                $notification = $notificationBuilder->build();
                $data = $dataBuilder->build();

                //$token = User::;
                $tokens = $toUser->devices ;
                foreach ($tokens as $tob){
                    if(strcasecmp($tob['platform'], 'android') == 0){
                        FCM::sendTo($tob['token'], $option, null, $data);
                    } else {
                        FCM::sendTo($tob['token'], $option, $notification, $data);
                    }

                }

                return true;
            } catch (\Exception $e) {
                dd($e->getMessage());
                return false;
            }
        }
    }
}
