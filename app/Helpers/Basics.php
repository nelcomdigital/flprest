<?php
/**
 * Created by PhpStorm.
 * User: Rabee
 * Date: 16/04/2019
 * Time: 08:09
 */


if (!function_exists('getConfig')) {

    function getConfig($key, $default= null)
    {
        $kkey = array_merge(explode('.',$key));

        $file = Arr::first ($kkey);

        unset($kkey[0]);
//        try{
            $jsonString = file_get_contents(base_path("custom-config/$file.json"));
            $data = json_decode($jsonString, true);
            $exist = true;
            if ( empty($kkey)){
                return $data;
            }
            foreach ($kkey as $k ){

                if (Arr::has($data, $k) ){
                    $data = $data[$k];
                }else{
                    $exist = false;
                    break;
                }
            }
            return $exist ? $data: $default;
//        }catch (Exception $e){
//            return $default;
//        }
    }
}

if (!function_exists('getModules')) {

    function getInside(&$modules , $steps,$slug = null  ){
        if ( array_has($steps,'show_side_menu') ){
            if ( $slug == null )
            $modules[] = $steps;
            else
                if ( $steps['slug'] == $slug ){
                    $modules[]= $steps;
                }
        }else{
            foreach ($steps as $k => $v ){
                getInside($modules , $v,$slug );
            }
        }
    }

    function getModules($slug = null )
    {
        try{
            $jsonString = file_get_contents(base_path("custom-config/modules.json"));
            $data = json_decode($jsonString, true);
            $modules = array();
            getInside($modules , $data,$slug);
            if ( $slug != null )
                return $modules[0];
            return $modules;
        }catch (Exception $e){
        }
    }
}

if (!function_exists('setConfig')) {

    function stepInside(&$arr , &$key ,$inKey,$value){

        if ( empty($key)  ){
            $arr[$inKey] = $value;
        }else{

            $k = array_shift($key);
          if ( $arr!=null && in_array($k,array_keys($arr),true )){
              stepInside($arr[$k] , $key,$inKey, $value);
          }else{
              $arr[$k]= null;
              stepInside($arr[$k],$key,$inKey,$value);
          }
        }
    }

    function setConfig( $key, $value, $update=false )
    {
        $key = explode('.',$key);

        if ( $update ){
            if ( getConfig($key , null) != null )
                return false;
        }
        $file  = array_shift($key);
        $jsonString = file_get_contents(base_path("custom-config/$file.json"));
        $data = json_decode($jsonString, true);


        stepInside($data ,$key,array_pop($key),$value);
        $newJsonString = json_encode($data);
        file_put_contents(base_path("custom-config/$file.json"), stripslashes($newJsonString));

        return true;

    }
}

if ( !function_exists( 'getFieldConfig') ){

   function getFieldConfig($module , $field_name ){
       $config = \App\Models\Configuration::query()
           ->where('user_id',auth()->id())
           ->where('relations','exists',[$module])
           ->where('field_name',$field_name)
           ->first();
       if ( $config == null )
           return null;
       else{
           return $config->selected_values;
       }
   }

}

if ( !function_exists( 'getStatuses') ){

    function getStatuses($module){
        $statues = \App\Models\Status::query()
            ->whereIn('module_ids',[$module])
            ->get();
        return $statues;
    }

}

if ( !function_exists( 'getMenuIcon') ){

    function getMenuIcon($slug){
            $jsonString = strtolower(file_get_contents(base_path("custom-config/menu-icons.php")));
            $data = eval('?>'.$jsonString);
        try{
            return $data[$slug];
        }catch (Exception $e){
            return null;
        }

    }

}

if ( !function_exists( 'transformValues') ){

     function transformValues($array, $name, $value){
        $res = [];
        foreach ( $array as $arr ){
            $res [] = ['value'=> $arr[$value], 'name'=>$arr[$name]];
        }
        return $res;
    }

}

if ( !function_exists( 'asset_thumb') ){

     function asset_thumb($path, $width = null, $height = null, $scale = null, $secure = null){
        $params = "?";
        if($width != null){
            $params .= "w=".$width;
        }
        if($height != null){
            if($params != "?"){
                $params .= "&";
            }
            $params .= "h=".$height;
        }
        if($scale != null){
            if($params != "?"){
                $params .= "&";
            }
            $params .= "fit=".$scale;
        }
        if($params != '?'){
            $path .= $params;
        }
        return app('url')->asset('images/'.$path, $secure);
    }

}

if  ( !function_exists( 'explodeTree')){

    function explodeTree($array)
    {
        if(!is_array($array)) return false;
        $returnArr = array();
        foreach ($array as $val) {
            // Get parent parts and the current leaf
            $parts	= explode('|',$val);
            array_shift($parts);
            // Build parent structure
            // Might be slow for really deep and large structures
            $parentArr = &$returnArr;
            foreach ($parts as $part) {
                $r = \App\Models\Reference::query()->where('_id',$part)->first();
                if (!isset($parentArr[$r->name])) {
                    $parentArr[$r->name] = array();
                } elseif (!is_array($parentArr[$r->name])) {

                    $parentArr[$r->name] = array();

                }
                $parentArr = &$parentArr[$r->name];
            }

        }
        return $returnArr;
    }
}

if  ( !function_exists( 'displayReferences')){
    function displayReferences(&$string,  $venue , $array  ){
        if ( !is_array($array) or count( $array ) == 0)
            return;
        else{
            $counter = 0;
            foreach ( $array as $k =>$v ){

                if (is_array($v) && sizeof($v)>0)
                    $string.="<br/>".$k.": ";
                else{

                    if ($counter == 0)
                        $string.=$k;
                    else{
                        $string .=", ".$k;
                    }

                }
                $counter ++ ;

                displayReferences($string, $venue, $v );
            }

        }
    }

}

if  ( !function_exists( 'displayReferencesSub')){
    function displayReferencesSub(&$string,  $venue , $array ,$counter = 0  ){
        if ( !is_array($array) or count( $array ) == 0 or substr_count ($string, '<div>' ) > 4 or $counter>3 )
            return;

        else{
//            $counter = 0;
            foreach ( $array as $k =>$v ){
                if (is_array($v) && sizeof($v)>0)
                    $string.='';
                else {
                    $string .= "<div>" . $k . "</div>";
                }
                if ($counter >= 3){
//                    $string .= "<div>" . '...' . "</div>";
                    break;
                }
                $counter++;
                displayReferencesSub($string, $venue, $v , $counter );
            }

        }
    }

}

if  ( !function_exists( 'truncateText')){
    function truncateText($s, $l, $e = '...', $isHTML = false)
    {
        $i = 0;
        $tags = array();
        if ($isHTML) {
            preg_match_all('/<[^>]+>([^<]*)/', $s, $m, PREG_OFFSET_CAPTURE | PREG_SET_ORDER);
            foreach ($m as $o) {
                if ($o[0][1] - $i >= $l)
                    break;
                $t = substr(strtok($o[0][0], " \t\n\r\0\x0B>"), 1);
                if ($t[0] != '/')
                    $tags[] = $t;
                elseif (end($tags) == substr($t, 1))
                    array_pop($tags);
                $i += $o[1][1] - $o[0][1];
            }
        }
        return substr($s, 0, $l = min(strlen($s), $l + $i)) . (count($tags = array_reverse($tags)) ? '
    
    ' : '') . (strlen($s) > $l ? $e : '');
    }

}

if  ( !function_exists( 'js_asset')){
    function js_asset(){
        return asset('/').'/';
    }

}

if  ( !function_exists( 'b_url')){
    function b_url(){
        return url('/').'/';
    }

}

if ( !function_exists( 'dataHelper')){

    function dataHelper($model, $conditions = array() , $relations=array(), $relations_embd=array() ){

        $m  = new $model();
        $query = $m::query();
        $selection =array();
        foreach ( $m->fields as $field ) {
            if (Arr::has($field, 'type') && ($field['type'] == 'sub_domains' ||$field['type'] == 'object' ||
                    $field['type'] == 'objects')) {
                if (method_exists($m, $field['key']))
                    $query->with($field['key']);
            }
        }

        if ( !empty($conditions) ){
            //['module_id','=', ['venue']]
            foreach ($conditions as $condition){
                if ( sizeof($condition) == 2 ){
                    if(is_array($condition[1])){
                        $query->whereIn($condition[0],$condition[1]);
                    }else{
                        if ($condition[0] == 'name' ){
                            $query->where(function ($query) use($condition) {
                                $query->where($condition[0],$condition[1]);
                                if ($condition[0] == 'name' )
                                    $query->orWhere('translations.'.session()->get('cms.locale').'.'.$condition[0],$condition[1]);
                            });
                        }else{
                            $query->where($condition[0],$condition[1]);
                        }
                    }
                }elseif ( sizeof($condition) == 3 ){
                    if(is_array($condition[2]) && $condition[1]!= "near"){
                        if($condition[1] == "="){
                            $query->whereIn($condition[0],$condition[2]);
                        }elseif ($condition[1]== "like"){
                            $vs = $condition[2];
                            $k = $condition[0];
                            $query->where(function ($query) use($k ,$vs) {
                                for ($i = 0; $i < count($vs); $i++){
                                    $query->orwhere($k, 'like',  '%' . $vs[$i] .'%');
                                }
                            });
                        }else if ( $condition[1]== "all"){
                            if ($condition[0] == 'name' ){
                                $query->where(function ($query) use($condition) {
                                    $query->where($condition[0],$condition[1],$condition[2]);
                                    if ($condition[0] == 'name' )
                                        $query->orWhere('translations.'.session()->get('cms.locale').'.'.$condition[0],$condition[1],$condition[2]);
                                });
                            }else{
                                $query->where($condition[0],$condition[1],$condition[2]);
                            }
                        }
                        else{
                            $query->whereNotIn($condition[0],$condition[2]);
                        }
                    }else{
                        if ( $condition[1]== "like" ){
                            if ($condition[0] == 'name' ){
                                $query->where(function ($query) use($condition) {
                                    $query->where($condition[0],$condition[1],'%'.$condition[2].'%');
                                    if ($condition[0] == 'name' )
                                        $query->orWhere('translations.'.session()->get('cms.locale').'.'.$condition[0],$condition[1],'%'.$condition[2].'%');
                                });
                            }else{
                                $query->where($condition[0],$condition[1],'%'.$condition[2].'%');

                            }

                        }else if ( $condition[1]== "near" ){
                            $query->where('location', 'near', [
                                '$geometry' => [
                                    'type' => 'Point',
                                    'coordinates' => [
                                        floatval($condition[2][1]),
                                        floatval($condition[2][0]),
                                    ]
                                ],
                                '$maxDistance' => intval($condition[2][2]),
                            ]);
                        } else
                            if ($condition[0] == 'name' ){
                                $query->where(function ($query) use($condition) {
                                    $query->where($condition[0],$condition[1],$condition[2]);
                                    if ($condition[0] == 'name' )
                                        $query->orWhere('translations.'.session()->get('cms.locale').'.'.$condition[0],$condition[1],$condition[2]);
                                });
                            }else{
                                $query->where($condition[0],$condition[1],$condition[2]);
                            }

                    }

                } elseif(sizeof($condition) == 4){
                    if ( $condition[0] == 'or'){
                        if(is_array($condition[2])){
                            if($condition[1] == "="){
                                $query->whereIn($condition[0],$condition[2]);
                            }elseif ($condition[1]== "like"){
                                $vs = $condition[2];
                                $k = $condition[0];
                                $query->where(function ($query) use($k ,$vs) {
                                    for ($i = 0; $i < count($vs); $i++){
                                        $query->orwhere($k, 'like',  '%' . $vs[$i] .'%');
                                    }
                                });
                            }else if ( $condition[1]== "all"){
                                if ($condition[0] == 'name' ){
                                    $query->where(function ($query) use($condition) {
                                        $query->where($condition[0],$condition[1],$condition[2]);
                                        $query->orWhere('translations.'.session()->get('cms.locale').'.'.$condition[0],$condition[1],$condition[2]);
                                    });
                                }else{
                                    $query->where($condition[0],$condition[1],$condition[2]);
                                }

                            }
                            else{
                                $query->whereNotIn($condition[0],$condition[2]);
                            }
                        }else{
                            if ( $condition[1]== "like" ){
                                $query->where($condition[0],$condition[1],'%'.$condition[2].'%');
                            }else if ( $condition[1]== "near" ){
                                $query->where('location', 'near', [
                                    '$geometry' => [
                                        'type' => 'Point',
                                        'coordinates' => [
                                            floatval($condition[2][1]),
                                            floatval($condition[2][0]),
                                        ],

                                    ],
                                    '$maxDistance' => intVal($condition[2][2]),
                                ]);
                            } else
                                if ($condition[0] == 'name' ){
                                    $query->where(function ($query) use($condition) {
                                        $query->where($condition[0],$condition[1],$condition[2]);

                                        $query->orWhere('translations.'.session()->get('cms.locale').'.'.$condition[0],$condition[1],$condition[2]);
                                    });
                                }else{
                                    $query->where($condition[0],$condition[1],$condition[2]);
                                }

                        }
                    }else{
                        $ids = array();
                        foreach ($condition['relations'] as $key => $rel){
                            $relatedM = new $rel['model'];
                            if($key == 0){
                                $ids = $relatedM::query()->where($rel['name'].'_id', array_last($condition))->pluck('_id');
                            }else {
                                $ids = $relatedM::query()->whereIn($rel['name'].'_id', $ids)->pluck('_id');
                            }
                        }
                        if($condition['operator'] == "in"){
                            $query->whereIn($condition['key'], $ids);
                        }
                    }

                }
            }
        }


        if(!empty($relations)){

            foreach ($relations as  $relation ) {
                if($relation['operator'] == "none"){
                    $query->doesnthave($relation['name']);
                }else{
                    $query->whereHas($relation['name'], function($q) use($relation ){
                        switch ($relation['operator']){
                            case 'in':
                                $q->whereIn($relation['key'],$relation['value']);
                                break;
                            case 'not in':
                                $q->whereNotIn($relation['key'],$relation['value']);
                                break;
                            default:
                                $q->where($relation['key'], $relation['operator'], $relation['value']);
                        }
                    });
                }
            }

        }
        if(!empty($relations_embd )){
            switch ($relations_embd['operator']){
                case 'in':
                    $query->whereIn($relations_embd['key'],$relations_embd['value']);
                    break;
                case 'not in':
                    $query->whereNotIn($relations_embd['key'],$relations_embd['value']);
                    break;
                default:
                    $query->where($relations_embd['key'],$relations_embd['operator'],$relations_embd['value']);
            }
        }

        return $query;
    }

}

if  ( !function_exists( 'b_url')){
    function b_url(){
        return url('/').'/';
    }

}

if ( !function_exists('getCurrentQuantity') ){
     function getCurrentQuantity($id){
        return session()->get('process.items.'.$id,'');
    }
}
