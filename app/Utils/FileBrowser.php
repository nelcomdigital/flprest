<?php
/**
 * Created by PhpStorm.
 * User: Rabee
 * Date: 02/04/2019
 * Time: 12:19
 */

namespace App\Utils;


class FileBrowser
{


    /**
     * Scan for files.
     *
     * @param $dir
     * @return array
     */
    public function scan($dir)
    {
        $old_dir = $dir;
        $dir = base_path($dir);

        $files = [];
        if (file_exists($dir)) {
            foreach (scandir($dir) as $f) {
                if (!$f || $f[0] == '.') {
                    continue; // ignore hidden files
                }
                if (is_dir($dir . '/' . $f)) {
                    // folder
                    $files[] = [
                        'name'  => $f,
                        'type'  => 'folder',
                        'path'  => $old_dir . '/' . $f,
                        'items' => $this->scan($old_dir . '/' . $f) // recursively get the contents of the folder
                    ];
                } else {
                    // file
                    $files[] = [
                        'name' => $f,
                        'type' => "file",
                        'path' => $old_dir . '/' . $f,
                        'size' => filesize($dir . '/' . $f)
                    ];
                }
            }
        }
        return $files;
    }

}
