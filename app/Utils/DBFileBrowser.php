<?php
/**
 * Created by PhpStorm.
 * User: Rabee
 * Date: 02/04/2019
 * Time: 12:19
 */

namespace App\Utils;


use App\Models\Gallery;
use App\Models\GalleryAlbum;
use Illuminate\Support\Facades\Log;

class DBFileBrowser
{


    /**
     * Scan for files.
     *
     * @param $dir
     * @return array
     */
    public function scan($dir)
    {
        $old_dir = $dir;
        $dir = base_path('public/storage/'.$dir);

        $files = [];
        if (file_exists($dir)) {
            foreach (scandir($dir) as $f) {
                if (!$f || $f[0] == '.') {
                    continue; // ignore hidden files
                }
                if (is_dir($dir . '/' . $f)) {
                    // folder
                    $album = GalleryAlbum::query()->where('path','storage/'.$old_dir . '/' . $f)->first();
                    $files[] = [
                        'name'  => $f,
                        'type'  => 'folder',
                        'path'  => $old_dir . '/' . $f,
                        'edit'=>$album!= null ?route('gallery-album.edit',$album->id):'#',
                        'object'=>$album,
                        'items' => $this->scan($old_dir . '/' . $f) // recursively get the contents of the folder
                    ];
                } else {
                    // file
                    $files[] = [
                        'name' => $f,
                        'type' => "file",
                        'path' => $old_dir . '/' . $f,
                        'edit'=>'#',
                        'object'=>Gallery::query()->where('path',$old_dir . '/' . $f)->first(),
                        'size' => filesize($dir . '/' . $f)
                    ];
                }
            }
        }
        return $files;
    }

}
