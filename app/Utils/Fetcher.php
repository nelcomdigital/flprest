<?php
/**
 * Created by PhpStorm.
 * User: Rabee
 * Date: 17/05/2019
 * Time: 10:00
 */

namespace App\Utils;


use App\Models\Api;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

trait Fetcher
{

    public function fetch(Request $request, $version, $slug, $domain = null, $data = array() ){

        $api = Api::query()->where('version',trim($version))->where('slug',trim($slug))->first();

        $messages = array();
        if ( $api == null || $api->method != $request->method() ){
            if ( $request->acceptsJson()){
                return response()->json(['status'=>'error', 'message'=>'apis request not exist.']);
            }else{
                return response(['status'=>'error', 'message'=>'apis request not exist.']);
            }
        }

        if ( $api->allow_loggers == '1' ){
            try{
                if($request->is('api*')){
                    if ( $request->header('locale') == null){
                        $messages['locale'] ="This field is required";
                    }else {
                        $locale = $request->header('locale');
                    }
                }else{
                    if ( session()->get('front.locale', null) == null){
                        $messages['locale'] ="This field is required";
                    }else {
                        $locale = session()->get('front.locale', null);
                    }
                }

            }catch (\Exception $exception ){
                $messages['locale'] ="This field is required";
            }

            try{
                if($request->is('api*')){
                    if ( $request->header('country') == null ){
                        $messages['country'] ="This field is required";
                    }else{
                        $country = $request->header('country');
                    }
                }else{
                    if ( session()->get('front.country', null) == null ){
                        $messages['country'] ="This field is required";
                    }else{
                        $country = strtoupper(session()->get('front.country'));
                    }
                }

            }catch (\Exception $exception ){
                $messages['country'] ="This field is required";

            }
            if($request->is('api*')){
                $domain = $request->header('domain', 'moovtoo.com');
            }else{
                $domain = array_first(explode('.', str_replace('http://','',str_replace('https://','',request()->url()))));
                if($domain == 'moovtoo' || $domain == "nelcomlab"){
                    $domain = "moovtoo.com";
                }
            }
        }

        if ( $request->is('api*') && $api->type != 'api'){
            if ( $request->acceptsJson()){
                return response()->json(['status'=>'error', 'message'=>'apis request not allowed.']);
            }else{
                return response(['status'=>'error', 'message'=>'apis request not allowed.']);
            }
        }

        if ( $api->authenticated == '1' &&
            !($request->has('access_token')
                || $request->header('access_token') ) ){
            if ( $request->acceptsJson()){
                return response()->json(['status'=>'error', 'message'=>'this request needs auth request']);
            }else{
                return response(['status'=>'error', 'message'=>'this request needs auth request.']);
            }

        }
        if( $api->authenticated == '1' )
        {
            if ( Auth::guard('api')->user() == null ){
                if ( $request->acceptsJson()){
                    return response()->json(['status'=>'error', 'message'=>'cannot fetch user']);
                }else{
                    return response(['status'=>'error', 'message'=>'cannot fetch user']);
                }
            }
        }


        foreach ( $api->arguments as $argument ){
            // if ( $argument['required'] == '1' && !$request->has($argument['key'])){
            if ( $argument['required'] == '1' && !array_has($data, $argument['key'])){
                $messages[$argument['key']] ="This field is required";
            }
        }

        foreach ( $api->conditions as $relation ){
            if ( str_contains($relation['value'],'@') && !str_contains($relation['value'],'=')){
                $key = str_replace('@','',array_first( explode(' ',$relation['value'])));
                // if( !$request->has($key))
                if( !array_has($data, $key))
                    $messages[$key] ="This field is required";
            }
        }

        foreach ( $api->relations as $relation ){
            if ( str_contains($relation['value'],'@') && !str_contains($relation['value'],'=')){
                $key = str_replace('@','',array_first( explode(' ',$relation['value'])));
                // if( !$request->has($key))
                if( !array_has($data, $key))
                    $messages[$key] ="This field is required";
            }
        }

        foreach ( $api->relations_embd as $relation ){
            if ( str_contains($relation['value'],'@') && !str_contains($relation['value'],'=')){
                $key = str_replace('@','',array_first( explode(' ',$relation['value'])));
                // if( !$request->has($key))
                if( !array_has($data, $key))
                    $messages[$key] ="This field is required";
            }
        }

        if ( !empty($messages)){
            if ( $request->acceptsJson()){
                return response()->json(['status'=>'error', 'message'=>$messages]);
            }else{
                return response(['status'=>'error', 'message'=>$messages]);
            }
        }

        $m = getModules($api->module_id);

        if ( $m['slug']== 'user' ){
            $c = 'App\User';
            $model = new $c();
        }else{
            $c = 'App\Models\\'.$m['name'];
            $model = new $c();
        }
        if ( $api->method == 'GET'){
            $conditions = array();
            foreach ( $api->conditions as $condition ){
                if ( str_contains($condition['value'],'@') && !str_contains($condition['value'],'=')){
                    $key = str_replace('@','',array_first( explode(' ',$condition['value'])));
                    $conditions[] = [$condition['name'],$condition['operator'],$request->input($key)];
                }elseif ( str_contains($condition['value'],'@') && str_contains($condition['value'],'=')){
                    $key = str_replace('@','',array_first( explode(' ',$relation['value'])));
                    $value = trim(array_last( explode('=',$condition['value'])));
                    // if($request->has($key)){
                    if(array_has($data, $key)){
                        $condition['value'] = $data[$key];
                        $conditions[] = $condition;
                    }else if($value != "none"){
                        $conditions[] = [$condition['name'],$condition['operator'],$value];
                    }
                }else{
                    if(isset($condition['value'])){
                        $conditions [] = $condition;
                    }else{
                        $condition['value'] = $request->input($condition['name']);
                    }
                }
            }

            $relations = array();
            foreach ( $api->relations as $relation ){
                if ( str_contains($relation['value'],'@') && !str_contains($relation['value'],'=')){
                    $key = str_replace('@','',array_first( explode(' ',$relation['value'])));
                    $relation['value'] = $request->input($key);
                    $relations[] = $relation;
                }elseif ( str_contains($relation['value'],'@') && str_contains($relation['value'],'=')){
                    $key = str_replace('@','',array_first( explode(' ',$relation['value'])));
                    $value = trim(array_last( explode('=',$relation['value'])));
                    // if($request->has($key)){
                        if(array_has($data, $key)){
                        $relation['value'] = $data[$key];
                        $relations[] = $relation;
                    }else if($value != "none"){
                        $relation['value'] = $value;
                        $relations[] = $relation;
                    }
                }else{
                    if(isset($relation['value'])){
                        $relations [] = $relation;
                    }else{
                        $relation['value'] = $request->input($relation['key']);
                        $relations [] = $relation;
                    }
                }
            }

            $relations_embd = array();
            foreach ( $api->relations_embd as $relation ){
                if ( str_contains($relation['value'],'@') && !str_contains($relation['value'],'=')){
                    $key = str_replace('@','',array_first( explode(' ',$relation['value'])));
                    $relation['value'] = $request->input($key);
                    $relations_embd[] = $relation;
                }elseif ( str_contains($relation['value'],'@') && str_contains($relation['value'],'=')){
                    $key = str_replace('@','',array_first( explode(' ',$relation['value'])));
                    $value = trim(array_last( explode('=',$relation['value'])));
                    // if($request->has($key)){
                    if(array_has($data, $key)){
                        $relation['value'] = $data[$key];
                        $relations_embd[] = $relation;
                    }else if($value != "none"){
                        $relation['value'] = $value;
                        $relations_embd[] = $relation;
                    }
                }else{
                    if(isset($relation['value'])){
                        $relations_embd [] = $relation;
                    }else{
                        $relation['value'] = $request->input($relation['key']);
                        $relations_embd [] = $relation;
                    }
                }
            }
            try{
                $limit = $api->limit ==null ? 0: ((int)$api->limit);
            }catch (\Exception $e){
                $limit = 0;
            }

            $force_session = ($api->allow_loggers == "1");
            $order = [$api->order_by, $api->order_type];
            $external_loggers = null ;
            if ( $force_session ){
                // if($request->is('api*')){
                //     $external_loggers['locale']= $locale;
                //     $external_loggers['country']= $country;
                //     $external_loggers['domain']= $domain;
                // }else{
                    $external_loggers['locale']= $locale;
                    $external_loggers['country']= $country;
                    $external_loggers['domain']= $domain;
                // }
            }

            $paginate = ($api->paginate == "1");

            if ( $request->is('api*')){
                if($paginate){
                    return response()->json(['status'=>'success', 'data'=>
                    dataHelper($model , $conditions , $order , $limit , $relations , $relations_embd , $force_session , $external_loggers, false, $paginate)]);
                }else{
                    return response()->json(['status'=>'success', 'data'=>
                    dataHelper($model , $conditions , $order , $limit , $relations , $relations_embd , $force_session , $external_loggers, false, $paginate)->get()]);
                }
            }else{
                if($paginate){
                    return dataHelper($model , $conditions , $order , $limit , $relations , $relations_embd , $force_session , $external_loggers, false, $paginate);
                }else{
                    return dataHelper($model , $conditions , $order , $limit , $relations , $relations_embd , $force_session , $external_loggers, false, $paginate)->get();
                }
            }

        }

    }
}
