<?php
namespace App\Utils;

class MongoDumper {
    private $_BACKUP_FOLDER = "";
    private $_CURRENT_DATE_TIME = "";
    private $current_dump_path = "";
    private $database = "";
    private $files_to_delete = array();
    private $debug = false;
    public function __construct($backup_folder) {
        $now = new \DateTime();
        $this->_BACKUP_FOLDER = rtrim($backup_folder, '/');
        $this->_CURRENT_DATE_TIME = $now->format('d-m-Y_H-i');
    }
    public function run($database, $debug = false) {
        $this->debug = ($debug === true);
        try {
            $this->current_dump_path = $this->_BACKUP_FOLDER ;
            $this->database = $database;
            $this->echo_if_debug("<p><strong>Backing up '" . $database . "' to '" . $this->current_dump_path . "'</strong></p>");
            $this->echo_if_debug("<ol>");
            $this->echo_if_debug("<li>Executing mongodump...</li>");
            $this->mongodump();
            $this->echo_if_debug("<li>Zipping files...</li>");
            $this->zip_files();
            $this->echo_if_debug("<li>Deleting dump folder...</li>");
//            $this->delete_dump_folder();
            $this->echo_if_debug("<li>Complete!</li>");
            $this->echo_if_debug("</ol>");
            return true;
        }
        catch (Exception $ex) {
            return false;
        }
    }
    private function echo_if_debug($string) {
        if ($this->debug) {
            echo $string;
        }
    }
    private function mongodump() {
        $command = "mongodump --db " . $this->database . " --out " . $this->current_dump_path.' -u '.env('DB_USERNAME')
            . ' -p '. env('DB_PASSWORD'). ' -h ' .env('DB_HOST');
        $results = shell_exec($command);
    }
    private function zip_files() {

        $database_dump_folder = $this->current_dump_path . "/" . $this->database;
        // Initialize archive object
        if ( !file_exists($database_dump_folder))
            mkdir($database_dump_folder);
        $files = glob($database_dump_folder.'/*');
        $this->create_zip( $files , $this->current_dump_path. "/" . $this->database.'.zip',true );
    }


    function create_zip($files = array(),$destination = '',$overwrite = false) {
        //if the zip file already exists and overwrite is false, return false
        if(file_exists($destination) && !$overwrite) { return false; }
        //vars
        $valid_files = array();
        //if files were passed in...
        if(is_array($files)) {
            //cycle through each file
            foreach($files as $file) {
                //make sure the file exists
                if(file_exists($file)) {
                    $valid_files[] = $file;
                }
            }
        }
        //if we have good files...
        if(count($valid_files)) {
            //create the archive
            $zip = new \ZipArchive();
            if($zip->open($destination, \ZipArchive::OVERWRITE|\ZipArchive::CREATE) !== true) {
                return false;
            }
            //add the files
            foreach($valid_files as $file) {
                $zip->addFile($file,array_last( explode('/',$file)));
            }
            //debug
            //echo 'The zip archive contains ',$zip->numFiles,' files with a status of ',$zip->status;

            //close the zip -- done!
            $zip->close();

            //check to make sure the file exists
            return file_exists($destination);
        }
        else
        {
            return false;
        }
    }

    private function delete_dump_folder() {
        $files = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator($this->current_dump_path, \FilesystemIterator::SKIP_DOTS),
            \RecursiveIteratorIterator::CHILD_FIRST
        );
        foreach ( $files as $file ) {
            $file->isDir() ? rmdir($file) : unlink($file);
        }
        rmdir($this->current_dump_path);
    }
}
