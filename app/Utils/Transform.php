<?php

namespace App\Utils;


use Illuminate\Support\Facades\Log;

trait Transform
{

    public function transform(){
        $module = new $this();
        $module->_id = $this->_id;
        foreach($this->fields as $field){
            $key = $field['key'];
            if(array_has($field, 'type') && $field['type'] == 'object'){
                $module->$key = ($this->$key)? $this->$key->name : '';
            }else if(array_has($field, 'type') && $field['type'] == 'function'){

                if ( $module->{$key}() == null )
                    $module->$key = array();
                else
                    $module->$key = $module->{$key}();
            }else if(array_has($field, 'type') && $field['type'] == 'objects'){
                $module->$key = ($this->$key)? $this->$key : [];
            }else if(array_has($field, 'type') && $field['type'] == 'modules'){
            $module->$key = $this->modules();
            }
            else{
                $module->$key = $this->$key;
            }
        }
        return $module;
    }

}
