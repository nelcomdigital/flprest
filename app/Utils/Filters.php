<?php
/**
 * Created by PhpStorm.
 * User: Rabee
 * Date: 14/05/2019
 * Time: 01:04
 */

namespace App\Utils;


use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;

trait Filters
{

    public function  filterSearch($searches, $force_session = false,$minimize = false){
        $filters = $this->filters;
        $conditions = array();
        $relations = array();
        $relations_embd = array();
        $order = ['name','asc'];
        $limit = 0;
        foreach ($filters as $filter ) {
            if ( Arr::has($filter , 'model')){
                $search = $filter['search'];
                if ( Arr::has($search , 'conditions') ){
                    foreach ( $search['conditions'] as $condition){
                        if ((( Arr::has($searches,$filter['name'].'_id')   &&  $searches[$filter['name'].'_id'] != null &&  $searches[$filter['name'].'_id'] != ' ' )||
                            ( Arr::has($searches,$filter['name'].'_ids')&&  $searches[$filter['name'].'_ids'] != null  &&  $searches[$filter['name'].'_ids'] != " " ) ) ) {
                            $condition[] = Arr::has($searches,$filter['name'].'_ids') ? $searches[$filter['name'].'_ids']:$searches[$filter['name'].'_id'];
                            $conditions[]= $condition ;
                        }else{
                            $condition[]= $condition;
                        }
                    }
                }


                if ( Arr::has($search , 'condition') && ( ( Arr::has($searches,$filter['name'].'_id')   &&  $searches[$filter['name'].'_id'] != null &&  $searches[$filter['name'].'_id'] != ' ' )||
                        ( Arr::has($searches,$filter['name'].'_ids')&&  $searches[$filter['name'].'_ids'] != null  &&  $searches[$filter['name'].'_ids'] != " " ) ) ) {
                        $search['condition'][] = Arr::has($searches,$filter['name'].'_ids') ? $searches[$filter['name'].'_ids']:$searches[$filter['name'].'_id'];
                        $conditions[]= $search['condition'] ;
                    }

                    if ( Arr::has( $search, 'relation')&&  ( ( Arr::has($searches,$filter['name'].'_id')  &&  $searches[$filter['name'].'_id'] != null  &&  $searches[$filter['name'].'_id'] != ' ' )||
                            ( Arr::has($searches,$filter['name'].'_ids') &&  $searches[$filter['name'].'_ids'] != null &&  $searches[$filter['name'].'_ids'] != " " ) )){
                        if ( $search['relation']['operator'] == 'in')
                            $search['relation']['value'] =  explode(',', Arr::has($searches,$filter['name'].'_ids') ? $searches[$filter['name'].'_ids']:$searches[$filter['name'].'_id']) ;
                        else
                            $search['relation']['value'] =   Arr::has($searches,$filter['name'].'_ids') ? $searches[$filter['name'].'_ids']:$searches[$filter['name'].'_id']; ;
                        $relations [] = $search['relation'];
                    }

                    if ( Arr::has( $search , 'relation_embd') && ( ( Arr::has($searches,$filter['name'].'_id')  &&  $searches[$filter['name'].'_id'] != null  &&  $searches[$filter['name'].'_id'] != ' ' )||
                            ( Arr::has($searches,$filter['name'].'_ids')&&  $searches[$filter['name'].'_ids'] != null  &&  $searches[$filter['name'].'_ids'] != " " ) ) ){
                        if ( $search['relation_embd']['operator'] == 'in')
                            $search['relation_embd']['value'] =  explode(',', Arr::has($searches,$filter['name'].'_ids') ? $searches[$filter['name'].'_ids']:$searches[$filter['name'].'_id']) ;
                        else
                            $search['relation_embd']['value'] =   array_has($searches,$filter['name'].'_ids') ? $searches[$filter['name'].'_ids']:$searches[$filter['name'].'_id']; ;
                        $relations_embd []=  $search['relation_embd'];
                    }
                }


        }


        $query = $this::query();

        foreach ( $this->fields as $field ) {
            if (Arr::has($field, 'type') && ($field['type'] == 'sub_domains' ||$field['type'] == 'object' ||
                    $field['type'] == 'objects')) {
                if (method_exists($this, $field['key']))
                    $query->with($field['key']);
            }
        }


        if ( !empty($conditions) ){
            foreach ($conditions as $condition){
                if ( sizeof($condition) == 2 ){
                    if(is_array($condition[1])){
                        $query->whereIn($condition[0],$condition[1]);
                    }else{
                            $query->where($condition[0],$condition[1]);
                    }
                }elseif ( sizeof($condition) == 3 ){
                    if(is_array($condition[2]) && $condition[1]!= "near"){
                        if($condition[1] == "="){
                            $query->whereIn($condition[0],$condition[2]);
                        }elseif ($condition[1]== "like"){
                            $vs = $condition[2];
                            $k = $condition[0];
                            $query->where(function ($query) use($k ,$vs) {
                                for ($i = 0; $i < count($vs); $i++){
                                    $query->orwhere($k, 'like',  '%' . $vs[$i] .'%');
                                }
                            });
                        }else if ( $condition[1]== "all"){
                                $query->where($condition[0],$condition[1],$condition[2]);
                        }
                        else{
                            $query->whereNotIn($condition[0],$condition[2]);
                        }
                    }else{
                        if ( $condition[1]== "like" ){
                                $query->where($condition[0],$condition[1],'%'.$condition[2].'%');
                        }else if ( $condition[1]== "near" ){
                            $query->where('location', 'near', [
                                '$geometry' => [
                                    'type' => 'Point',
                                    'coordinates' => [
                                        floatval($condition[2][1]),
                                        floatval($condition[2][0]),
                                    ]
                                ],
                                '$maxDistance' => intval($condition[2][2]),
                            ]);
                        } else
                                $query->where($condition[0],$condition[1],$condition[2]);

                    }

                } elseif(sizeof($condition) == 4){
                    if ( $condition[0] == 'or'){
                        if(is_array($condition[2])){
                            if($condition[1] == "="){
                                $query->whereIn($condition[0],$condition[2]);
                            }elseif ($condition[1]== "like"){
                                $vs = $condition[2];
                                $k = $condition[0];
                                $query->where(function ($query) use($k ,$vs) {
                                    for ($i = 0; $i < count($vs); $i++){
                                        $query->orwhere($k, 'like',  '%' . $vs[$i] .'%');
                                    }
                                });
                            }else if ( $condition[1]== "all"){
                                    $query->where($condition[0],$condition[1],$condition[2]);
                            }
                            else{
                                $query->whereNotIn($condition[0],$condition[2]);
                            }
                        }else{
                            if ( $condition[1]== "like" ){
                                $query->where($condition[0],$condition[1],'%'.$condition[2].'%');
                            }else if ( $condition[1]== "near" ){
                                $query->where('location', 'near', [
                                    '$geometry' => [
                                        'type' => 'Point',
                                        'coordinates' => [
                                            floatval($condition[2][1]),
                                            floatval($condition[2][0]),
                                        ],

                                    ],
                                    '$maxDistance' => intVal($condition[2][2]),
                                ]);
                            } else
                                    $query->where($condition[0],$condition[1],$condition[2]);
                        }
                    }else{
                        $ids = array();
                        foreach ($condition['relations'] as $key => $rel){
                            $relatedM = new $rel['model'];
                            if($key == 0){
                                $ids = $relatedM::query()->where($rel['name'].'_id', array_last($condition))->pluck('_id');
                            }else {
                                $ids = $relatedM::query()->whereIn($rel['name'].'_id', $ids)->pluck('_id');
                            }
                        }
                        if($condition['operator'] == "in"){
                            $query->whereIn($condition['key'], $ids);
                        }
                    }

                }
            }
        }

        if(!empty($relations)){

            foreach ($relations as  $relation ) {
                if($relation['operator'] == "none"){
                    $query->doesnthave($relation['name']);
                }else{
                    $query->whereHas($relation['name'], function($q) use($relation ){
                        switch ($relation['operator']){
                            case 'in':
                                $q->whereIn($relation['key'],$relation['value']);
                                break;
                            case 'not in':
                                $q->whereNotIn($relation['key'],$relation['value']);
                                break;
                            default:
                                $q->where($relation['key'], $relation['operator'], $relation['value']);
                        }
                    });
                }
            }

        }
        if(!empty($relations_embd )){
            switch ($relations_embd['operator']){
                case 'in':
                    $query->whereIn($relations_embd['key'],$relations_embd['value']);
                    break;
                case 'not in':
                    $query->whereNotIn($relations_embd['key'],$relations_embd['value']);
                    break;
                default:
                    $query->where($relations_embd['key'],$relations_embd['operator'],$relations_embd['value']);
            }
        }

        if(app()->request->has('order')){
            $tOrder = app()->request->get('order')[0];
            $field = $this->fields[$tOrder['column']];
            if(Arr::has($field, 'type') and $field['type'] == "object"){
                $query->orderBy($field['key'].'.name', $tOrder['dir']);
            }
        }else if(!empty($order)){
            $query->orderBy($order[0], $order[1]);
        }


        return $query;

    }


    public function filterRender($tableId){

        $filters = $this->filters;
        $filterValues = array();
        foreach ($filters as $filter ){
            if ( Arr::has($filter , 'model')){
                if(!Arr::has($filter, 'ajax_search') or (Arr::has($filter, 'ajax_search') and !$filter['ajax_search'])){
                    if ( Arr::has($filter , 'conditions'))
                        $conditions = $filter['conditions'];
                    else
                        $conditions = array();
                    if ( Arr::has( $filter, 'relations'))
                        $relations = $filter['relations'];
                    else
                        $relations = array();
                    if ( Arr::has( $filter , 'relations_embd') )
                        $relations_embd = $filter['relations_embd'];
                    else
                        $relations_embd = array();
                    $filterValues[$filter['name']] =
                        dataHelper($filter['model'], $conditions , $relations,$relations_embd)->get();
                }else{
                    $filterValues[$filter['name']] = [];
                }
            }else{
                if(Arr::has($filter, 'type') and $filter['type'] == 'module'){
                    $filterValues[ $filter['name'] ] = transformValues( call_user_func($filter['values']), 'name','slug') ;
                }else{
                    $filterValues[ $filter['name'] ] = $filter['options'];
                }
            }
        }

        return json_encode( view('cms.layouts.blocks.table.filters', compact('filters','filterValues', 'tableId'))->render() );

    }

}
