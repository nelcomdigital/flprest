<?php
/**
 * Created by PhpStorm.
 * User: Rabee
 * Date: 27/06/2019
 * Time: 12:55
 */

namespace App\Utils;


trait ClientAnalytics
{
    function with($array){
        $result = array();
        foreach ( $array as $arr ){
            $result[] = [
                'category' => $arr[0],
                'action' => $arr[1],
                'label'=>$arr[2],
                'value'=>1
            ];
        }
        if ( sizeof($result) > 0){
            request()->session()->flash('send_ga_event',
                $result);
        }

    }

}
