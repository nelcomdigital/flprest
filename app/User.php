<?php

namespace App;


use App\Models\Role;
use App\Utils\Transform;
use GeoIp2\Record\Country;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Log;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


class User extends Eloquent implements
    AuthenticatableContract, AuthorizableContract, CanResetPasswordContract
{

    use Authenticatable, Authorizable, Notifiable, CanResetPassword, Transform;

    public $fields = [
        ['key' => 'photo', 'name' => 'Photo', 'type' => 'photo', 'width' => '40px'],
        ['key' => 'name', 'name' => 'Full Name'],
        ['key' => 'status', 'name' => 'Status', 'type' => 'object'],
        ['key' => 'roles', 'name' => 'Roles', 'type' => 'objects'],
        ['key' => 'created_at', 'name' => 'Date Created'],
    ];
    public $filters = [
//        [
//            'name' => 'status',
//            'model' => 'App\Models\Status',
//            'conditions' => [
//                ['key' => 'module_id', 'value' => 'user']
//            ]
//        ],
//        [
//            'name' => 'roles',
//            'model' => 'App\Models\Role',
//        ],
    ];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'country_id', 'phone', 'position', 'provider', 'status_id', 'is_approved'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $with = [
        'roles', 'status',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function status()
    {
        return $this->belongsTo('App\Models\Status');
    }

    public function verifyUser()
    {
        return $this->hasOne('App\Models\VerifyUser');
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function comments()
    {
        return $this->hasMany(Comments::class, 'user_id');
    }

    public function trips()
    {
        return $this->hasMany(Trip::class);
    }

    public function isAdmin()
    {
        foreach ($this->roles()->get() as $role) {
            if ($role->slug == 'admin' || $role->slug == 'super-admin' || $role->slug == 'editor-in-chief' || $role->slug == 'editor') {
                return true;
            }
        }

        return false;
    }
    public function isUser(){
        foreach ($this->roles()->get() as $role) {
            if ($role->slug == 'l1' ||$role->slug == 'l2' ||$role->slug == 'l3' ||$role->slug == 'l4' ||$role->slug == 'l5' ) {
                return true;
            }
        }

        return false;
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function isTripModerator()
    {
        $roles = array();
        foreach ($this->roles()->get() as $role) {
            if ($role->slug == 'trip-organizer') {
                $roles[] = "trip-organizer";
            } else if ($role->slug == 'trip-manager') {
                $roles [] = "trip-manager";
            }
        }

        if (sizeof($roles) == 0)
            return null;
        return $roles;
    }

    public function getUserRoles()
    {
        $roles = array();
        $r = $this->roles()->get();
        foreach ( $r as $role) {
            if ($role->slug == 'venue-owner') {
                $roles[] = "venue-owner";
            } else if ($role->slug == 'event-owner') {
                $roles [] = "event-owner";
            } else if ($role->slug == 'trip-manager') {
                $roles[] = "trip-manager";
            } else if ($role->slug == 'activity-owner') {
                $roles [] = "activity-owner";
            }
        }

        if (sizeof($roles) == 0)
            return [];
        return $roles;
    }

    public function authorizeRoles($roles)
    {
        // if (is_array($roles)) {
        //     return $this->hasAnyRole($roles) ||
        //         abort(401, 'This action is unauthorized.');
        // }
        // return $this->hasRole($roles) ||
        //     abort(401, 'This action is unauthorized.');
        return true;
    }

    public function getKey()
    {
        return $this->id;
    }


    public function authorAttributes()
    {
        return [
            'name' => $this->name,
            'email' => $this->email,
            'avatar' => ($this->photo != null) ? (str_contains($this->photo, 'facebook') or str_contains($this->photo, 'google')) ? $this->photo : asset($this->photo) : asset('images/avatar.jpg'), // optional
        ];
    }


    public function isReferenceRelated($id)
    {
        if ($this->reference_ids == null) {
            return false;
        }
        if (str_contains(str_replace(' ', '', implode('|', $this->reference_ids)),
            str_replace(' ', '', $id))) {
            return true;
        }

        return false;

    }

    public function userPermissions()
    {
        $roles = auth()->user()->roles;
        $domainsAllowed = array();

        $modules = getModules();
        $result = [];

        foreach ($roles as $role) {
            if ($role->slug == 'super-admin') {
                $permissions = array();
                foreach ($modules as $module) {
                    $rm = array();
                    $rm['slug'] = $module['slug'];
                    $rm['show'] = true;
                    $rm['add'] = true;
                    $rm['edit'] = true;
                    $rm['delete'] = true;
                    $rm['edit_all'] = true;
                    $rm['delete_all'] = true;
                    $rm['show_all'] = true;
                    $permissions[] = $rm;
                }
                $result['permissions'] = $permissions;
                $result['permission_roles'] = $roles;
                $result['domains_allowed'] = ['cms', 'business'];
                return $result;
            }
            if ($role->redirect != null) {

                $domainsAllowed [] = $role->redirect->domain_name;
            }

        }

        $result['permission_roles'] = $roles;
        $result['domains_allowed'] = $domainsAllowed;
        return $result;
    }

    public function checkFavorite($model, $object)
    {
        if ($this->favorites != null) {
            if (array_has($this->favorites, $model)) {
                $favs = $this->favorites;
                if (in_array($object, $favs[$model])) {
                    return true;
                } else {

                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function updateFavorite($model, $object)
    {
        if ($this->favorites != null) {
            if (array_has($this->favorites, $model)) {
                $favs = $this->favorites;
                if (in_array($object, $favs[$model])) {
                    $favs[$model] = array_diff($favs[$model], [$object]);
                    $this->favorites = $favs;
                    $this->save();
                    return false;
                } else {
                    $favs[$model][] = $object;
                    $this->favorites = $favs;
                    $this->save();
                    return true;
                }
            } else {
                $favs = $this->favorites;
                $favs[$model][] = $object;
                $this->favorites = $favs;
                $this->save();
                return true;
            }
        } else {
            $favs = [$model => [$object]];
            $this->favorites = $favs;
            $this->save();
            return true;
        }
    }

    public function getFavorites()
    {
//article package activity
        $result = array();
        if ($this->favorites != null) {
            $favs = $this->favorites;
            if (array_has($favs, 'Venue') && is_array($favs['Venue'])) {
                foreach ($favs['Venue'] as $id) {
                    $venue =Venue::query()->with('country')->with('city')->where('_id', $id)->first();
                    if ( $venue != null)
                        $result['Venue'][] = $venue->toArray();
                }
            }

        }
        if ($this->favorites != null) {
            $favs = $this->favorites;
            if (array_has($favs, 'Article') && is_array($favs['Article'])) {
                foreach ($favs['Article'] as $id) {
                    $article = Article::query()->where('_id', $id)->first();
                    if ( $article != null )
                        $result['Article'][] = $article->toArray();
                }
            }

        }
        if ($this->favorites != null) {
            $favs = $this->favorites;
            if (array_has($favs, 'Activity') && is_array($favs['Activity'])) {
                foreach ($favs['Activity'] as $id) {
                    $trip = Activity::query()->with('city')->where('_id', $id)->first();
                    if ( $trip != null )
                        $result['Activity'][] = $trip->toArray();
                }
            }

        }
        if ($this->favorites != null) {
            $favs = $this->favorites;
            if (array_has($favs, 'Event') && is_array($favs['Event'])) {
                foreach ($favs['Event'] as $id) {
                    $event = Event::query()->with('city')->where('_id', $id)->first();
                    if ( $event != null )
                        $result['Event'][] = $event->toArray();
                }
            }

        }
        if ($this->favorites != null) {
            $favs = $this->favorites;
            if (array_has($favs, 'Package') && is_array($favs['Package'])) {
                foreach ($favs['Package'] as $id) {
                    $package = Trip::query()->with('city')->where('_id', $id)->first();
                    if ( $package != null )
                        $result['Package'][] = $package->toArray();
                }
            }

        }


        return $result;

    }


}
