<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class sponseePurchaseOrder extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $data;
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address = 'noreply@foreverliving.fr';
//        if (session()->get('front.locale') == 'en'){
//            $subject = 'Your purchase order';
//        } else {
//            $subject = 'Tu orden de compra';
//        }
        $subject = __('front.sponsee.purchase_order');

        $name = 'Forever Living';
        return $this->view('frontend.emails.sponsee-purchase-order')
            ->from($address, $name)
            ->bcc("inscription@foreverliving.fr")
            ->subject($subject)
            ->with(['order'=>$this->data
            ]);
    }
}
