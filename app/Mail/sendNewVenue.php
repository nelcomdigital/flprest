<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class sendNewVenue extends Mailable
{
    use Queueable, SerializesModels;

    public $venue;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($venue)
    {
        $this->venue = $venue;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = [
            'venue' => $this->venue
        ];

        return $this->subject('New Venue added')
            ->view('frontend.emails.business.new-venue', $data);
    }

}
