<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class sponseeMoreThan2CC extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $data;
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address = 'noreply@foreverliving.fr';
//        if (session()->get('front.locale') == 'en') {
//            $subject = 'Congratulations! You are now a FOREVER preferred Customer';
//        }else{
//            $subject = '¡Felicitaciones! Ahora eres un Distributor FOREVER';
//        }
        $subject = __('front.sponsee.morethan_2cc');

        $name = 'Forever Living';
        return $this->view('frontend.emails.sponsee-welcome-more-than-2cc-and-pack')
            ->from($address, $name)
            ->bcc("inscription@foreverliving.fr")
            ->subject($subject)
            ->with(['reference'=>$this->data['reference'],
                'password'=>$this->data['password'],
                'd_first_name'=>$this->data['d_first_name'],
                'd_last_name'=>$this->data['d_last_name'],
                'd_phone'=>$this->data['d_phone'],
                'd_mail'=>$this->data['d_mail']
            ]);
    }
}
