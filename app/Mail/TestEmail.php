<?php

namespace App\Mail;

use http\Env\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TestEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }
    
    /**
     * Build the message.
     *
     * @param Request $request
     * @return $this
     */
    public function build()
    {
        $address = 'fouad.saoudi94@gmail.com';
        $subject = 'This is a demo!';
        $name = 'Fouad Saoudi';
    
        $headerData = [
            'category' => 'category',
            'unique_args' => [
                'variable_1' => 'abc'
            ]
        ];
    
        $header = $this->asString($headerData);
        
        $this->withSwiftMessage(function ($message) use ($header) {
            $message->getHeaders()
                ->addTextHeader('X-SMTPAPI', $header);
        });
    
        return $this->subject($subject)->view('frontend.emails.test');
    }
    private function asJSON($data)
    {
        $json = json_encode($data);
        $json = preg_replace('/(["\]}])([,:])(["\[{])/', '$1$2 $3', $json);
        
        return $json;
    }
    
    
    private function asString($data)
    {
        $json = $this->asJSON($data);
        
        return wordwrap($json, 76, "\n   ");
    }
}
