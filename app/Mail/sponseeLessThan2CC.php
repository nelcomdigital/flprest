<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class sponseeLessThan2CC extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $data;
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address = 'noreply@foreverliving.fr';
        $subject = __('front.sponsee.lessThan2cc_notification_title');
        $name = 'Forever Living';
        return $this->view('frontend.emails.sponsee-welcome-less-than-2cc-products')
            ->from($address, $name)
            ->bcc("inscription@foreverliving.fr")
            ->subject($subject)
            ->with(['reference'=>$this->data['reference'],
                'password'=>$this->data['password'],
                'd_first_name'=>$this->data['d_first_name'],
                'd_last_name'=>$this->data['d_last_name'],
                'd_phone'=>$this->data['d_phone'],
                'd_mail'=>$this->data['d_mail']
            ]);
    }
}
