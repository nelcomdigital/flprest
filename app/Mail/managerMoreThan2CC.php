<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class managerMoreThan2CC extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $data;
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address = 'noreply@foreverliving.fr';
//        if (session()->get('front.locale') == 'en'){
//            $subject = 'A Preferred Customer has joined your group';
//        } else {
//            $subject = 'Un Cliente Preferido se ha unido a tu grupo';
//        }
        $subject = __('front.manager.more_2cc');

        $name = 'Forever Living';
        return $this->view('frontend.emails.manager-more-than-2cc-products')
            ->from($address, $name)
            ->bcc("inscription@foreverliving.fr")
            ->subject($subject)
            ->with([
                's_name'=>$this->data['s_name'],
                'reference'=>$this->data['reference'],
                's_first_name'=>$this->data['s_first_name'],
                'd_first_name'=>$this->data['d_first_name'],
                's_last_name'=>$this->data['s_last_name'],
                'd_last_name'=>$this->data['d_last_name'],
                's_address'=>$this->data['s_address'],
                's_zip_code'=>$this->data['s_zip_code'],
                's_city'=>$this->data['s_city'],
                's_phone'=>$this->data['s_phone'],
                's_mail'=>$this->data['s_mail'],
            ]);
    }
}
