<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class GroupMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected  $email;
    protected  $title;
    protected  $bodyMessage;
    protected  $attach;
    protected  $photo;

    public function __construct($email , $title, $bodyMessage, $attach,$photo)
    {
        //
        $this->email = $email;
        $this->title = $title;
        $this->bodyMessage = $bodyMessage;
        $this->attach = $attach;
        $this->photo = $photo;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address = 'noreply@foreverliving.fr';

        $subject = __('front.mail.group_mail_subject');
        $name = 'Forever Living';

        return $this->view('frontend.emails.group')
            ->from($address, $name)
            ->bcc("inscription@foreverliving.fr")
            ->subject($subject)
            ->with(['title'=> $this->title , 'bodyMessage'=> $this->bodyMessage , 'email'=>$this->email ] )
            ->attach(asset($this->attach))
            ->attach(asset($this->photo));

    }
}
