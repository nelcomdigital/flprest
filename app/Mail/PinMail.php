<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PinMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected  $email;
    protected  $pin;

    public function __construct($email , $pin)
    {
        //
        $this->email = $email;
        $this->pin = $pin;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address = 'noreply@foreverliving.fr';

        $subject = __('front.mail.pinmail_subject');
        $name = 'Forever Living';

        return $this->view('frontend.emails.pin')
            ->from($address, $name)
            ->bcc("inscription@foreverliving.fr")
            ->subject($subject)
            ->with(['pin'=> $this->pin , 'email'=>$this->email ] );
    }
}
