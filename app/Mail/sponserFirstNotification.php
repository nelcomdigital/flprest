<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class sponserFirstNotification extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $data;
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address = 'noreply@foreverliving.fr';
        $subject = __('front.email.sponser_72h_email_title');
        $name = 'Forever Living';
        return $this->view('frontend.emails.sponser-first-notification')
            ->from($address, $name)
            ->bcc("inscription@foreverliving.fr")
            ->subject($subject)
            ->with([
                'id'=>$this->data['id'],
                's_first_name'=>$this->data['s_first_name'],
                's_last_name'=>$this->data['s_last_name'],
                's_phone'=>$this->data['s_phone'],
                's_mail'=>$this->data['s_mail'],
            ]);
    }
}
