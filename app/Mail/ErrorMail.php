<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ErrorMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected  $data;

    public function __construct($data)
    {
        //
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address = 'noreply@foreverliving.fr';
//        if (session()->get('front.locale') == 'en'){
//            $subject = 'Thanks for buying';
//        } else {
//            $subject = 'Tu código PIN';
//        }
        $subject = "Error Mail";
        $name = 'Forever Living';

        return $this->view('frontend.emails.error_mail')
            ->from($address, $name)
            ->subject($subject)
            ->with(['data'=> $this->data ] );
    }
}
