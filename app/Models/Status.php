<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Model;
use App\Utils\Sortable;
use App\Utils\SortableTrait;

use App\Utils\Filters;
use App\Utils\Relations;
use App\Utils\Transform;


class Status extends Model implements Sortable
{
    //
   use SortableTrait,Filters,Relations,Transform;
    public $sortable = [
        'order_column_name' => 'order',
        'sort_when_creating' => true,
    ];

    public $translatable=[];

    public $allLocales = true;

    public $fillable=["name"];

    public $fields=[
        ['key'=>'name','name'=>'Name','width'=>'30%'],
        ['key'=>'modules', 'name'=>'Module', 'type'=>'modules'],
    ];

    // protected $with=['module'];

    public $filters = [

    ];
    public $languageSession = false;

    public function toArray()
    {
        $params =  parent::toArray();
        if ( count ($this->translatable) > 0 ){
            foreach ( $this->translatable as $t ){
                $v = $this->translateAttribute( $t );
                if ( $v!= null )
                 $params[$t] = $v;
            }
        }

        return $params;
    }

    public function __get($key){

        $c = get_called_class();
        if ( $c != null ){
            if(in_array($key, $this->translatable))  {
                $v = $this->translateAttribute( $key );
                return $v == null ? parent::__get($key): $v;

            }
        }

        return parent::__get($key);
    }


function translateAttribute($key) {

        $domain = array_first(explode('.', str_replace('http://','',str_replace('https://','',request()->url()))));
        if ( $domain == 'cms'){
            $loc = session()->get('cms.locale','en');
        }else{
            $loc = session()->get('front.locale','en');
        }
            try{
                return $this->translations[$loc][$key];
            }catch (\Exception $exception){

            }
            return null;
    }

    public function modules(){
        $modules = getModules();
        $m =array();
        if ( $this->module_ids != null )
        foreach($modules as $module){
            if(in_array($module['slug'] , $this->module_ids)){
                 $m[] = $module['slug'];
            }
        }
        return $m;
    }

}
