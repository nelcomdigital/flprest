<?php

namespace App\Models;

use App\Utils\Sortable;
use App\Utils\SortableTrait;
use Illuminate\Support\Arr;
use Jenssegers\Mongodb\Eloquent\Model as Model;
use App\Utils\Filters;
use App\Utils\Relations;
use App\Utils\Transform;



class GroupCountry extends Model implements Sortable
{
    //
   use SortableTrait,Filters,Relations,Transform;
    public $sortable = [
        'order_column_name' => 'order',
        'sort_when_creating' => true,
    ];

    public $fields = [
        ['key' => 'order', 'name' => 'Order', 'width' => '30%'],
        ['key' => 'name', 'name' => 'Name']
    ];

    public $fillable = [];

    public $filters = [

    ];
    public $translatable=['name','slug'];
    public $languageSession = false;

    public function toArray()
    {
        $params = parent::toArray();
        if (count($this->translatable) > 0) {
            foreach ($this->translatable as $t) {
                $v = $this->translateAttribute($t);
                if ($v != null)
                    $params[$t] = $v;
            }
        }

        return $params;
    }

    public function __get($key)
    {

        $c = get_called_class();
        if ($c != null) {
            if (in_array($key, $this->translatable)) {
                $v = $this->translateAttribute($key);
                return $v == null ? parent::__get($key) : $v;

            }
        }

        return parent::__get($key);
    }


    function translateAttribute($key)
    {
        try {
            return $this->$key[session()->get('current_locale', 'fr')];
        } catch (\Exception $exception) {

        }
        return null;
    }


    public function countries(){
        return $this->hasMany(Country::class)->orderBy('order','asc');
    }


}
