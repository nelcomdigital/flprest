<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Model;
use App\Utils\Sortable;
use App\Utils\SortableTrait;

use App\Utils\Filters;
use App\Utils\Relations;
use App\Utils\Transform;


class Producer extends Model implements Sortable
{
    //
   use SortableTrait,Filters,Relations,Transform;
    public $sortable = [
        'order_column_name' => 'order',
        'sort_when_creating' => true,
    ];

    public $translatable=[];

    public $fillable=["name"];

    public $fields=[
        ['key'=>'order','name'=>'Order'],
        ['key'=>'name','name'=>'Name','width'=>'30%'],
        ['key'=>'status','name'=>'Status','type'=>'object'],
        ['key'=>'date_created','name'=>'Date Created'],
    ];

    public $filters = [

    ];

    public $languageSession = false;

    public function toArray()
    {
        $params =  parent::toArray();
        if ( count ($this->translatable) > 0 ){
            foreach ( $this->translatable as $t ){
                $v = $this->translateAttribute( $t );
                if ( $v!= null )
                 $params[$t] = $v;
            }
        }

        return $params;
    }

    public function __get($key){

        $c = get_called_class();
        if ( $c != null ){
            if(in_array($key, $this->translatable))  {
                $v = $this->translateAttribute( $key );
                return $v == null ? parent::__get($key): $v;

            }
        }

        return parent::__get($key);
    }


function translateAttribute($key) {

        $domain = array_first(explode('.', str_replace('http://','',str_replace('https://','',request()->url()))));
        if ( $domain == 'cms'){
            $loc = session()->get('cms.locale','en');
        }else{
            $loc = session()->get('front.locale','en');
        }
            try{
                return $this->translations[$loc][$key];
            }catch (\Exception $exception){

            }
            return null;
    }
    public function domains(){
        return $this->embedsMany(Domain::class);
    }

    public function locales(){
        return $this->embedsMany(Locale::class);
    }

    public function countries(){
        return $this->embedsMany(Country::class);
    }
    public function status(){
        return $this->belongsTo(Status::class);
    }

}
