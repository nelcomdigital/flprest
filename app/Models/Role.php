<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Model;
use App\Utils\Sortable;
use App\Utils\SortableTrait;

use App\Utils\Filters;
use App\Utils\Relations;
use App\Utils\Transform;


class Role extends Model implements Sortable
{
    //
   use SortableTrait,Filters,Relations,Transform;
    public $sortable = [
        'order_column_name' => 'order',
        'sort_when_creating' => true,
    ];

    public $translatable=['name','slug'];

    public $fillable=[];

    public $fields=[
        ['key'=>'name','name'=>'Name','width'=>'30%'],
    ];

    public $filters = [

    ];
    public $languageSession = false;

    public function toArray()
    {
        $params = parent::toArray();
        if (count($this->translatable) > 0) {
            foreach ($this->translatable as $t) {
                $v = $this->translateAttribute($t);
                if ($v != null)
                    $params[$t] = $v;
            }
        }

        return $params;
    }

    public function __get($key)
    {

        $c = get_called_class();
        if ($c != null) {
            if (in_array($key, $this->translatable)) {
                $v = $this->translateAttribute($key);
                return $v == null ? parent::__get($key) : $v;

            }
        }

        return parent::__get($key);
    }


    function translateAttribute($key)
    {
        try {
            return $this->$key[session()->get('current_locale', 'fr')];
        } catch (\Exception $exception) {

        }
        return null;
    }


    public function users(){
        return $this->belongsToMany('App\User');
    }

    public function redirect(){
        return $this->belongsTo(Domain::class , 'domain_id');
    }


}
