<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Model;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;
use App\User;
use App\Models\Contact;



class Group extends Model implements Sortable
{
    //
    use SortableTrait;

    public $sortable = [
        'order_column_name' => 'order',
        'sort_when_creating' => true,
    ];

    public $translatable=['name','slug'];

    public $fillable=["name"];

    public $fields=[['key'=>'name']];

        public $filters = [

    ];
    public $languageSession = false;

    public function toArray()
    {
        $params =  parent::toArray();
        if ( count ($this->translatable) > 0 ){
            foreach ( $this->translatable as $t ){
                $v = $this->translateAttribute( $t );
                if ( $v!= null )
                 $params[$t] = $v;
            }
        }

        return $params;
    }

    public function __get($key){

            $c = get_called_class();
            if ( $c != null ){
                if(in_array($key, $this->translatable))  {
                    $v = $this->translateAttribute( $key );
                    return $v == null ? parent::__get($key): $v;

                }
            }

            return parent::__get($key);
        }


    function translateAttribute($key) {

        $domain = array_first(explode('.', str_replace('http://','',str_replace('https://','',request()->url()))));
        if ( $domain == 'cms'){
            $loc = session()->get('cms.locale','en');
        }else{
            $loc = session()->get('front.locale','en');
        }
            try{
                return $this->translations[$loc][$key];
            }catch (\Exception $exception){

            }
            return null;
    }

    public function domains(){
        return $this->embedsMany(Domain::class);
    }

    public function locales(){
        return $this->embedsMany(Locale::class);
    }

    public function countries(){
        return $this->embedsMany(Country::class);
    }
    
    // public function contacts(){
    //     $contacts=array();
    //     if (is_array($this->contact_ids ) && $this->contact_ids != null){
    //         $contacts = Contact::query()->whereIn('_id',$this->contact_ids)->get();
    //         return $contacts;
    //     }
    //     else{
    //         return $contacts;
    //     }

    // }
    public function contacts()
    {
        
        return $this->belongsToMany(Contact::class, 'contacts');
    }

    public function user(){
    return $this->belongsTo(User::class,'user_id');
    }

}
