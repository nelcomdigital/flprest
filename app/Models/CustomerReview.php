<?php

namespace App\Models;

use App\Utils\Sortable;
use App\Utils\SortableTrait;
use Jenssegers\Mongodb\Eloquent\Model as Model;


class CustomerReview extends Model implements Sortable
{
    //
    use SortableTrait;

    public $sortable = [
        'order_column_name' => 'order',
        'sort_when_creating' => true,
    ];

    public $translatable = [];


    public $filters = [

    ];
    public $languageSession = false;

    public function toArray()
    {
        $params = parent::toArray();
        if (count($this->translatable) > 0) {
            foreach ($this->translatable as $t) {
                $v = $this->translateAttribute($t);
                if ($v != null)
                    $params[$t] = $v;
            }
        }

        return $params;
    }

    public function __get($key)
    {

        $c = get_called_class();
        if ($c != null) {
            if (in_array($key, $this->translatable)) {
                $v = $this->translateAttribute($key);
                return $v == null ? parent::__get($key) : $v;

            }
        }

        return parent::__get($key);
    }





}
