<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Model;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;


class Category extends Model implements Sortable
{
    //
    use SortableTrait;

    public $sortable = [
        'order_column_name' => 'order',
        'sort_when_creating' => true,
    ];
    public $fields = [
        ['key' => 'order', 'name' => 'Order', 'width' => '30%'],
        ['key' => 'name', 'name' => 'Name']
    ];
    public $fillable = [];

    public $filters = [

    ];
    public $translatable=['name','slug'];
    public $languageSession = false;

    public function toArray()
    {
        $params =  parent::toArray();
        if ( count ($this->translatable) > 0 ){
            foreach ( $this->translatable as $t ){
                $v = $this->translateAttribute( $t );
                if ( $v!= null )
                 $params[$t] = $v;
            }
        }

        return $params;
    }

    public function __get($key){

            $c = get_called_class();
            if ( $c != null ){
                if(in_array($key, $this->translatable))  {
                    $v = $this->translateAttribute( $key );
                    return $v == null ? parent::__get($key): $v;

                }
            }

            return parent::__get($key);
        }


    function translateAttribute($key) {

        $domain = array_first(explode('.', str_replace('http://','',str_replace('https://','',request()->url()))));
        if ( $domain == 'cms'){
            $loc = session()->get('cms.locale','en');
        }else{
            $loc = session()->get('front.locale','en');
        }
            try{
                return $this->translations[$loc][$key];
            }catch (\Exception $exception){

            }
            return null;
    }

    public function domains(){
        return $this->embedsMany(Domain::class);
    }

    public function locales(){
        return $this->embedsMany(Locale::class);
    }

    public function countries(){
        return $this->embedsMany(Country::class);
    }
    
    public function videos(){
        return $this->belongsToMany(Video::class,'video_ids');
    }
    
    public function modules(){
        $modules = getModules();
        $m =array();
        if ( $this->module_ids != null )
        foreach($modules as $module){
            if(in_array($module['slug'] , $this->module_ids)){
                 $m[] = $module['slug'];
            }
        }
        return $m;
    }

}
