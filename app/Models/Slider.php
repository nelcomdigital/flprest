<?php

namespace App\Models;

use App\Utils\Filters;
use App\Utils\Relations;
use App\Utils\Sortable;
use App\Utils\SortableTrait;
use App\Utils\Transform;
use Jenssegers\Mongodb\Eloquent\Model as Model;


class Slider extends Model implements Sortable
{
    //
    use SortableTrait, Filters, Relations, Transform;
    public $sortable = [
        'order_column_name' => 'order',
        'sort_when_creating' => true,
    ];

    public $translatable = [];

    public $fillable = [];
    protected $appends=['active'];
    public $fields = [
        ['key' => 'name', 'name' => 'Name', 'width' => '30%'],
        ['key' => 'created_at', 'name' => 'Date Created'],
        ['key' => 'active', 'name' => 'Active'],
    ];

    public $filters = [

    ];
    public $languageSession = false;
    public function getActiveAttribute(){
        try{
            if (isset($this->is_active) && $this->is_active ){
                return 'true';

            }else{
                return'false';
            }
        }catch (\Exception $e){
            return'false';
        }
    }
    public function toArray()
    {
        $params = parent::toArray();
        if (count($this->translatable) > 0) {
            foreach ($this->translatable as $t) {
                $v = $this->translateAttribute($t);
                if ($v != null)
                    $params[$t] = $v;
            }
        }

        return $params;
    }

    public function __get($key)
    {

        $c = get_called_class();
        if ($c != null) {
            if (in_array($key, $this->translatable)) {
                $v = $this->translateAttribute($key);
                return $v == null ? parent::__get($key) : $v;

            }
        }

        return parent::__get($key);
    }


    function translateAttribute($key)
    {
        try {
            return $this->$key[session()->get('current_locale', 'fr')];
        } catch (\Exception $exception) {

        }
        return null;
    }

    public function domains()
    {
        return $this->embedsMany(Domain::class);
    }

    public function locale()
    {
        return $this->belongsTo(Locale::class,'locale_id');
    }
    public function mainMenu()
    {
        return $this->belongsTo(MainMenu::class,'main_menu_id');
    }

    public function countries()
    {
        return $this->embedsMany(Country::class);
    }


}
