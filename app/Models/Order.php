<?php

namespace App\Models;

use App\User;
use App\Utils\Sortable;
use App\Utils\SortableTrait;
use Jenssegers\Mongodb\Eloquent\Model as Model;
use App\Utils\Filters;
use App\Utils\Relations;
use App\Utils\Transform;


class Order extends Model implements Sortable
{
    //
   use SortableTrait,Filters,Relations,Transform;
    public $sortable = [
        'order_column_name' => 'order',
        'sort_when_creating' => true,
    ];

    public $translatable=[];

    public $fillable=[];

    public $fields=[
        ['key'=>'order','name'=>'Order'],
        ['key'=>'user','name'=>'Name', 'type'=>'object'],
        ['key'=>'status','name'=>'Status'],
        ['key'=>'total','name'=>'Total'],
    ];

    public $filters = [

    ];
    public $languageSession = false;


    public function status(){
        return $this->belongsTo(Status::class);
    }
    public function user(){
        return $this->BelongsTo(User::class,'user_id');
    }

    public function getProductFromId( $id ){
        return Product::query()->find($id);
    }

}
