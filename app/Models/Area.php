<?php

namespace App\Models;

use App\Utils\Sortable;
use App\Utils\SortableTrait;
use Jenssegers\Mongodb\Eloquent\Model as Model;
use App\Utils\Filters;
use App\Utils\Relations;
use App\Utils\Transform;



class Area extends Model implements Sortable
{
    //
   use SortableTrait,Filters,Relations,Transform;
    public $sortable = [
        'order_column_name' => 'order',
        'sort_when_creating' => true,
    ];

    public $translatable=[];

    public $allLocales = true;

    public $fillable=["name"];

    public $fields=[
        ['key'=>'name'],
        ['key'=>'is_top','name'=>'Flag','id'=>'is_top'],
        ['key'=>'city','name'=>'City','type'=>'object'],
    ];

    public $filters = [
//        [
//            'name' => 'country',
//            'model' => Country::class,
//            'search'=>[
//                'condition'=>['key'=>'city_id', 'operator'=>'in', 'relations'=>[
//                        ['name'=>'country', 'model'=>Region::class],
//                        ['name'=>'region', 'model'=>Department::class],
//                        ['name'=>'department', 'model'=>City::class]
//                    ]
//                ]
//            ]
//        ],
//        [
//            'name' => 'region',
//            'model' => Region::class,
//            'ajax_search' => true,
//            'search'=>[
//                'condition'=>['key'=>'city_id', 'operator'=>'in', 'relations'=>[
//                        ['name'=>'region', 'model'=>Department::class],
//                        ['name'=>'department', 'model'=>City::class],
//                    ]
//                ]
//            ]
//        ],
//        [
//            'name' => 'department',
//            'model' => Department::class,
//            'ajax_search' => true,
//            'search'=>[
//                'condition'=>['key'=>'city_id', 'operator'=>'in', 'relations'=>[
//                        ['name'=>'department', 'model'=>City::class],
//                    ]
//                ]
//            ]
//        ],
//        [
//            'name' => 'city',
//            'model' => City::class,
//            'ajax_search' => true,
//            'search'=>[
//                'relation'=>['name'=>'city','key'=>'_id','operator'=>'in']
//            ]
//        ],
    ];
    public $languageSession = false;

    public function toArray()
    {
        $params =  parent::toArray();
        if ( count ($this->translatable) > 0 ){
            foreach ( $this->translatable as $t ){
                $v = $this->translateAttribute( $t );
                if ( $v!= null )
                 $params[$t] = $v;
            }
        }

        return $params;
    }

    public function __get($key){

        $c = get_called_class();
        if ( $c != null ){
            if(in_array($key, $this->translatable))  {
                $v = $this->translateAttribute( $key );
                return $v == null ? parent::__get($key): $v;

            }
        }

        return parent::__get($key);
    }


function translateAttribute($key) {

        $domain = array_first(explode('.', str_replace('http://','',str_replace('https://','',request()->url()))));
        if ( $domain == 'cms'){
            $loc = session()->get('cms.locale','en');
        }else{
            $loc = session()->get('front.locale','en');
        }
            try{
                return $this->translations[$loc][$key];
            }catch (\Exception $exception){

            }
            return null;
    }

    public function city(){
        return $this->belongsTo(City::class);
    }

}
