<?php

namespace App\Models;

use App\Utils\Sortable;
use App\Utils\SortableTrait;
use Jenssegers\Mongodb\Eloquent\Model as Model;


class Page extends Model implements Sortable
{
    //
    use SortableTrait;

    public $sortable = [
        'order_column_name' => 'order',
        'sort_when_creating' => true,
    ];

    public $translatable=[];

    public $fillable=["name"];

    public $fields=[
        ['key'=>'name','name'=>'Name','width'=>'30%'],
        ['key'=>'FBO','name'=>'FBO','width'=>'30%'],
        ['key'=>'CLI','name'=>'CLI','width'=>'30%'],
//        ['key'=>'locales','name'=>'Locales','type'=>'objects'],
    ];

    public $filters = [

    ];
    public $languageSession = false;

    public function toArray()
    {
        $params =  parent::toArray();
        if ( count ($this->translatable) > 0 ){
            foreach ( $this->translatable as $t ){
                $v = $this->translateAttribute( $t );
                if ( $v!= null )
                 $params[$t] = $v;
            }
        }

        return $params;
    }

    public function __get($key){

            $c = get_called_class();
            if ( $c != null ){
                if(in_array($key, $this->translatable))  {
                    $v = $this->translateAttribute( $key );
                    return $v == null ? parent::__get($key): $v;

                }
            }

            return parent::__get($key);
        }


    function translateAttribute($key) {

        $domain = array_first(explode('.', str_replace('http://','',str_replace('https://','',request()->url()))));
        if ( $domain == 'cms'){
            $loc = session()->get('cms.locale','en');
        }else{
            $loc = session()->get('front.locale','en');
        }
            try{
                return $this->translations[$loc][$key];
            }catch (\Exception $exception){

            }
            return null;
    }

    public function locales(){
        return $this->embedsMany(Locale::class);
    }



}
