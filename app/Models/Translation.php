<?php
/**
 * Created by PhpStorm.
 * User: Rabee
 * Date: 22/05/2019
 * Time: 01:56
 */

namespace App\Models;


use App\Utils\Filters;
use App\Utils\Relations;
use App\Utils\Transform;
use Jenssegers\Mongodb\Eloquent\Model as Model;


class Translation extends Model
{
    //
    use Filters,Relations,Transform;


    public $translatable=[];

    public $fillable=["name"];

    protected $dates=['created_at'];

    public $fields=[];

    public function __construct($attributes = array())
    {
        $locales = Locale::all();
        $fields []=['key'=>'key','name'=>'Name','width'=>'30%'];
        foreach ($locales as $locale){
            $fields[] = ['key'=>$locale->code , 'name'=> $locale->name , 'type'=> 'translation'];
        }
        $this->fields = $fields;
        return parent::__construct($attributes);
    }



    // protected $with=['module'];

    public $filters = [
    ];

    public $languageSession = false;

    public function toArray()
    {
        $params =  parent::toArray();
        if ( count ($this->translatable) > 0 ){
            foreach ( $this->translatable as $t ){
                $v = $this->translateAttribute( $t );
                if ( $v!= null )
                 $params[$t] = $v;
            }
        }

        return $params;
    }

    public function __get($key){

        $c = get_called_class();
        if ( $c != null ){
            if(in_array($key, $this->translatable))  {
                $v = $this->translateAttribute( $key );
                return $v == null ? parent::__get($key): $v;

            }
        }

        return parent::__get($key);
    }


function translateAttribute($key) {

        $domain = array_first(explode('.', str_replace('http://','',str_replace('https://','',request()->url()))));
        if ( $domain == 'cms'){
            $loc = session()->get('cms.locale','en');
        }else{
            $loc = session()->get('front.locale','en');
        }
            try{
                return $this->translations[$loc][$key];
            }catch (\Exception $exception){

            }
            return null;
    }



}
