<?php

namespace App\Models;

use App\Utils\Filters;
use App\Utils\Relations;
use App\Utils\Sortable;
use App\Utils\SortableTrait;
use App\Utils\Transform;
use Illuminate\Support\Facades\Log;
use Jenssegers\Mongodb\Eloquent\Model as Model;


class ProductCategory extends Model implements Sortable
{
    //
    use SortableTrait, Filters, Relations, Transform;
    public $sortable = [
        'order_column_name' => 'order',
        'sort_when_creating' => true,
    ];

    public $translatable = ['name','slug'];

    public $fillable = ["name"];

    protected $appends=['active'];
    
    public $fields = [
        ['key' => 'order', 'name' => 'Order', 'width' => '5%'],
        ['key' => 'name', 'name' => 'Name', 'width' => '100%'],
        ['key' => 'active', 'name' => 'Active'],
    ];

    public $filters = [

    ];

    public function toArray()
    {
        $params = parent::toArray();
        if (count($this->translatable) > 0) {
            foreach ($this->translatable as $t) {
                $v = $this->translateAttribute($t);

                if ($v == null)
                    $params[$t] = "";
                else{
                    $params[$t] = $v;
                }
            }
        }

        return $params;
    }
    public function getActiveAttribute(){
        try{
            if (isset($this->is_active) && $this->is_active ){
                return 'true';

            }else{
                return'false';
            }
        }catch (\Exception $e){
            return'false';
        }
    }
    public function __get($key)
    {

        $c = get_called_class();
        if ($c != null) {
            if (in_array($key, $this->translatable)) {
                $v = $this->translateAttribute($key);
                return $v == null ? parent::__get($key) : $v;

            }
        }

        return parent::__get($key);
    }


    function translateAttribute($key)
    {
        try {
            return $this->$key[session()->get('current_locale', 'fr')];
        } catch (\Exception $exception) {

        }
        return null;
    }

    public function parent()
    {
        return $this->BelongsTo(ProductCategory::class,  'parent_id');
    }

    public function role(){
        return $this->belongsTo(Role::class,'role_id');
    }

    public function products(){
        return $this->belongsTo(Product::class);
    }

    public function checkSlug($slug){
        $slugs = $this->slug;
        foreach ($slugs as $k=>$v){
            if ( $slug == $v)
                return true;
        }
        return false;
    }

    public function countries(){
        return $this->belongsToMany(Country::class);
    }

}
