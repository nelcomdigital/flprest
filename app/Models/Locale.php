<?php

namespace App\Models;

use App\Utils\Sortable;
use App\Utils\SortableTrait;
use Jenssegers\Mongodb\Eloquent\Model as Model;


class Locale extends Model implements Sortable
{
    //
    use SortableTrait;

    public $sortable = [
        'order_column_name' => 'order',
        'sort_when_creating' => true,
    ];

    public $fillable=[];
    
    public $fields=[
        ['key'=>'name','name'=>'Name','width'=>'30%'],
        ['key'=>'code','name'=>'Code','width'=>'30%'],
    ];
    
    
    public $filters = [

    ];
    
    
}
