<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Model;

use App\Utils\Filters;
use App\Utils\Relations;
use App\Utils\Sortable;
use App\Utils\SortableTrait;
use App\Utils\Transform;

class BusinessFile extends Model implements Sortable
{
    //
    use SortableTrait;

    public $sortable = [
        'order_column_name' => 'order',
        'sort_when_creating' => true,
    ];

    public $translatable=[];

    public $fillable=[];

    public $fields = [
        ['key' => 'name', 'name' => 'Name', 'width' => '100%'],

    ];
//    protected $with=['status'];

    public $filters = [

    ];
    public $languageSession = false;
    
    public function businessCategories()
    {
        return $this->belongsToMany(BusinessCategory::class);
    }
}
