<?php

namespace App\Models;

use Google_Client;
use Jenssegers\Mongodb\Eloquent\Model as Model;
use App\Utils\Sortable;
use App\Utils\SortableTrait;

use App\Utils\Filters;
use App\Utils\Relations;
use App\Utils\Transform;


class Product extends Model implements Sortable
{
    //
    use SortableTrait,Filters,Relations,Transform;
    public $sortable = [
        'order_column_name' => 'order',
        'sort_when_creating' => true,
    ];

    public $translatable=['name','slug','short_description'
        ,'description','ingredients','advantages','instructions'];

//    protected $appends=['price_label'];

    public $fillable=["name","description","content","meta_title","meta_description"];

    public $fields=[
        ['key'=>'order','width'=>'5%'],
        ['key'=>'name','name'=>'Name'],
        ['key'=>'reference','name'=>'Reference'],
//        ['key'=>'quantity','name'=>'Quantity'],
        ['key'=>'category','name'=>'Category','type'=>'object'],
//        ['key'=>'date_created','name'=>'Date Created'],
    ];

    public $filters = [
        [
            'name' => 'categories',
            'model' => ProductCategory::class,
        ],
    ];
    public $languageSession = false;

    public function toArray()
    {
        $params = parent::toArray();
        if (count($this->translatable) > 0) {
            foreach ($this->translatable as $t) {
                $v = $this->translateAttribute($t);

                if ($v == null)
                    $params[$t] = "";
                else{
                    $params[$t] = $v;
                }
            }
        }

        return $params;
    }

    public function __get($key)
    {

        $c = get_called_class();
        if ($c != null) {
            if (in_array($key, $this->translatable)) {
                $v = $this->translateAttribute($key);
                return $v == null ? parent::__get($key) : $v;

            }
        }

        return parent::__get($key);
    }


    function translateAttribute($key)
    {
        try {
            return $this->$key[session()->get('current_locale', 'fr')];
        } catch (\Exception $exception) {

        }
        return null;
    }

    public function status(){
        return $this->belongsTo(Status::class);
    }

    public function category(){
        return $this->belongsTo(ProductCategory::class, 'category_id');
    }
    public function currency(){
        return $this->belongsTo(Currency::class , 'currency_id');
    }

    //price_label getPriceLabelAttribute
    public function getPriceLabelAttribute(){
        try{
            return $this->price .' '.$this->currency->symbol;
        }catch (\Exception $e){
            return $this->price . '/ '.' xxx cc';
        }

    }

    public function getCurrentQuantity(){
        return session()->get('process.items.'.$this->id,'');
    }

    public function getPrice(){
        foreach ( $this->price_list as $price ){
            if ( $price['role']['slug'] =='l1' ){
                if ( session()->get('process.tax_validate',true) )
                    return $price['price'] * ((100+$this->iva_tax)/100);
                else
                    return $price['price'];
            }
        }
        return 0;
    }

    public function getCC(){
        foreach ( $this->price_list as $price ){
            if ( $price['role']['slug'] =='l1' ){
                return number_format($price['price']/250,3);
            }
        }
        return 0;
    }

    public function getTotalPrice(){
        $total = 0;
        foreach ( $this->price_list as $price ){
            if ( $price['role']['slug'] =='l1' ){
                if ( session()->get('process.tax_validate',true) )
                    $total += $price['price']* ((100+$this->iva_tax)/100);
                else
                    $total += $price['price'];
            }
        }
        return $total;
    }

    public function getVideoData($videoID)
    {
        try{
            $apikey = 'AIzaSyArTw46LtR--XL4sBE1SWvEdfPZr3mSKts';
            $client = new Google_Client();
            $client->setApplicationName('ForEverLiving');
            $client->setDeveloperKey($apikey);
            $youtube = new \Google_Service_YouTube($client);
            $videoData = $youtube->videos->listVideos('snippet,statistics', [
                'id' => $videoID,
            ]);
            $title = $videoData->getItems()[0]->getSnippet()->getTitle();
            $published = $videoData->getItems()[0]->getSnippet()->getPublishedAt();
            $views = $videoData->getItems()[0]->getStatistics()->getViewCount();

            return array(
                'title' => $title,
                'published' => $published,
                'views' =>$views
            );
        }catch (\Exception $e){
            dd($e->getTrace());
        }
    }
}
