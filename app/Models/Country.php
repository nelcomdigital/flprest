<?php

namespace App\Models;

use App\Utils\Filters;
use App\Utils\Relations;
use App\Utils\Sortable;
use App\Utils\SortableTrait;
use App\Utils\Transform;
use Illuminate\Support\Arr;
use Jenssegers\Mongodb\Eloquent\Model as Model;


class Country extends Model implements Sortable
{
    //
   use SortableTrait,Filters,Relations,Transform;
    public $sortable = [
        'order_column_name' => 'order',
        'sort_when_creating' => true,
    ];

    public $fields = [
        ['key' => 'order', 'name' => 'Order', 'width' => '30%'],
        ['key' => 'name', 'name' => 'Name'],
        ['key' => 'country_code', 'name' => 'Country Code']
    ];

    public $fillable = [];

    public $filters = [

    ];
    public $translatable=['name','slug','home_delivery','relay_delivery','home_delivery_description','relay_delivery_description'];
    public $languageSession = false;

    public function toArray()
    {
        $params = parent::toArray();
        if (count($this->translatable) > 0) {
            foreach ($this->translatable as $t) {
                $v = $this->translateAttribute($t);

                if ($v == null)
                    $params[$t] = "";
                else{
                    $params[$t] = $v;
                }
            }
        }

        return $params;
    }

    public function __get($key)
    {

        $c = get_called_class();
        if ($c != null) {
            if (in_array($key, $this->translatable)) {
                $v = $this->translateAttribute($key);
                return $v == null ? parent::__get($key) : $v;

            }
        }

        return parent::__get($key);
    }


    function translateAttribute($key)
    {
        try {
            return $this->$key[session()->get('current_locale', 'fr')];
        } catch (\Exception $exception) {

        }
        return null;
    }


    public function locales(){
        return $this->embedsMany(Locale::class );
    }

    public function primary(){
        return $this->belongsTo( Locale::class , 'main_language');
    }

    public function city(){
        return $this->hasMany(City::class);
    }

    public function group_country(){
        return $this->belongsTo(GroupCountry::class);
    }
    
    public function status()
    {
        return $this->belongsTo(Status::class);
    }

}
