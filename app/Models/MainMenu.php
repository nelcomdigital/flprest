<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Model;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;


class MainMenu extends Model implements Sortable
{
    //
    use SortableTrait;

    public $sortable = [
        'order_column_name' => 'order',
        'sort_when_creating' => true,
    ];
    public $translatable=[
        'name'
    ];

    public $fillable=["name"];

    public $filters = [

    ];
    protected $appends=['parent','active'];
    public $fields = [
        ['key' => 'order', 'name' => 'Order', 'width' => '5%'],
        ['key' => 'name', 'name' => 'Title', 'width' => '30%'],
        ['key'=>'parent','name'=>'parent'],
        ['key' => 'active', 'name' => 'Active'],
        ['key' => 'created_at', 'name' => 'Date Created'],
    ];
    public $languageSession = false;
    public function getParentAttribute(){
        try{
            if (is_array($this->parent_ids ) && $this->parent_ids != null){
                return MainMenu::query()->whereIn('_id',$this->parent_ids)->first()->name;

            }else{
                return'';
            }
        }catch (\Exception $e){
            return'';
        }
    }
    public function getActiveAttribute(){
        try{
            if (isset($this->is_active) && $this->is_active ){
                return 'true';

            }else{
                return'false';
            }
        }catch (\Exception $e){
            return'false';
        }
    }
    public function toArray()
    {
        $params =  parent::toArray();
        if ( count ($this->translatable) > 0 ){
            foreach ( $this->translatable as $t ){
                $v = $this->translateAttribute( $t );
                if ( $v!= null )
                 $params[$t] = $v;
            }
        }

        return $params;
    }

    public function __get($key){

            $c = get_called_class();
            if ( $c != null ){
                if(in_array($key, $this->translatable))  {
                    $v = $this->translateAttribute( $key );
                    return $v == null ? parent::__get($key): $v;

                }
            }

            return parent::__get($key);
        }


    function translateAttribute($key) {

        $domain = array_first(explode('.', str_replace('http://','',str_replace('https://','',request()->url()))));
        if ( $domain == 'cms'){
            $loc = session()->get('cms.locale','en');
        }else{
            $loc = session()->get('front.locale','en');
        }
            try{
                return $this->translations[$loc][$key];
            }catch (\Exception $exception){

            }
            return null;
    }

    public function domains(){
        return $this->embedsMany(Domain::class);
    }

    public function locales(){
        return $this->embedsMany(Locale::class);
    }

    public function countries(){
        return $this->embedsMany(Country::class);
    }
    public function parents(){
        $parents=array();
        if (is_array($this->parent_ids ) && $this->parent_ids != null){
            $parents=MainMenu::query()->whereIn('_id',$this->parent_ids)->get();
            return $parents;
        }
        else{
            return $parents;
        }

    }
    
    public function videos(){
        return $this->belongsToMany(Video::class,'video_ids');
    }

}
