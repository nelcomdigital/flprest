<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Crypt;
use File;
use SoapClient;
use SoapFault;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function thirdPartyLogin($data)
    {
        $secretKey = 'R=&878hbm4F*sSa=Cmb5UH@k@A*455';
        $apiUser = 'FLPAPINELCOM';
        $idKey = 'flp_xXBuBHnLTV0i';

        $ur = env('REST_URL');

        // if (strpos($ur, 'ppapirest') !== false) {
//            $date = gmdate('Y-m-dH');
        // } else {
             $date = date('Y-m-dH', strtotime('+2 hours'));
        // }

        $api_key = $idKey . '.' . sha1($apiUser . '-' . $date . '-' . $secretKey);


        $url = $ur . 'login' . '?api_key=' . $api_key;
        $ch = curl_init($url);
        $jsonData = [
            'login' => $data['email'],
            'pwd' => $data['password']
        ];
        $jsonDataEncoded = json_encode($jsonData);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        $result = json_decode($result);
        return $result;
    }

    public function handleAccessResponse($params)
    {

        $result = $this->thirdPartyLogin($params);
        if (isset($result->success) && $result->success) {
            $result = (array)$result;
            $result['token'] = $this->createToken($result['infos']->num, $result['infos']->flag);
            return [$result, 200];
        } else {
            return [(array)$result, 500];
        }
    }

    public function createToken($num, $type, $pin = null)
    {
        if ($pin != null)
            return Crypt::encryptString(uniqid('token') . ',' . $num . ','
                . uniqid('token') . ',' . $type . ',' . uniqid('token') . ',' . $pin . ',' . uniqid('token'));

        return Crypt::encryptString(uniqid('token') . ',' . $num . ','
            . uniqid('token') . ',' . $type . ',' . uniqid('token'));

    }

    public function thirdPartyFunc($type, $api, $data)
    {
        $secretKey = 'R=&878hbm4F*sSa=Cmb5UH@k@A*455';
        $apiUser = 'FLPAPINELCOM';
        $idKey = 'flp_xXBuBHnLTV0i';

        $ur = env('REST_URL');

        // if (strpos($ur, 'ppapirest') !== false) {
//            $date = gmdate('Y-m-dH');
        // } else {
             $date = date('Y-m-dH', strtotime('+2 hours'));
        // }

        $api_key = $idKey . '.' . sha1($apiUser . '-' . $date . '-' . $secretKey);

        $url = $ur . $api . '?api_key=' . $api_key;
        $ch = curl_init($url);
        $jsonDataEncoded = json_encode($data);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $type);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        $result = json_decode($result);
        return $result;
    }

    public function thirdPartyFuncTEST($type, $api, $data)
    {
        $secretKey = 'R=&878hbm4F*sSa=Cmb5UH@k@A*455';
        $apiUser = 'FLPAPINELCOM';
        $idKey = 'flp_xXBuBHnLTV0i';

        $ur = env('REST_URL');

        // if (strpos($ur, 'ppapirest') !== false) {
//            $date = gmdate('Y-m-dH');
        // } else {
             $date = date('Y-m-dH', strtotime('+2 hours'));
        // }

        $api_key = $idKey . '.' . sha1($apiUser . '-' . $date . '-' . $secretKey);

        $url = $ur . $api . '?api_key=' . $api_key;
        $ch = curl_init($url);
        $jsonDataEncoded = json_encode($data);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $type);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        $result = json_decode($result);
        dd($result);
        return $result;
    }

    public function thirdPartyFuncDiscount($type, $api, $data)
    {
        $ur = env('TOOLS');
        $url = $ur . $api;

        // $url = 'https://pptools.flpbis.com/api/'.$api;
        $ch = curl_init($url);
        $jsonDataEncoded = json_encode($data);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $type);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        $result = json_decode($result);
        return $result;
    }

    public function thirdPartyFuncPpapifl($type, $api, $data)
    {

        $apiV2SecretKey = 'jh=6*d87yH5YrS8&k!$k36Z3M&Rd!N';
        $apiV2User = 'FLAPIV2TNG';
        $url = env('APP_URL');
        // if (strpos($url, 'pprest') !== false) {
//            $date = gmdate('Y-m-dH');
        // } else {
             $date = date('Y-m-dH', strtotime('+2 hours'));
        // }

        $token = sha1($apiV2User . '-' . $date . '-' . $apiV2SecretKey);

        $ur = env('APIFL');
        $url = $ur . $api . "?api_key=" . $token;

        // $url = 'https://ppapifl.foreverliving.fr/api/'.$api."?api_key=".$token;
        $ch = curl_init($url);
        $jsonDataEncoded = json_encode($data);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $type);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);

        $result = json_decode($result);
        return $result;
    }

    public function thirdPartyFuncPpapiflFormData($type, $api, $data)
    {

        $apiV2SecretKey = 'jh=6*d87yH5YrS8&k!$k36Z3M&Rd!N';
        $apiV2User = 'FLAPIV2TNG';

        $url = env('APP_URL');
        // if (strpos($url, 'pprest') !== false) {
//            $date = gmdate('Y-m-dH');
        // } else {
             $date = date('Y-m-dH', strtotime('+2 hours'));
        // }
        $token = sha1($apiV2User . '-' . $date . '-' . $apiV2SecretKey);

        $ur = env('APIFL');
        $url = $ur . $api . "?api_key=" . $token;

        $headers = array(
            "Content-Type: application/x-www-form-urlencoded",
        );

        $convertedData = http_build_query($data);

        // $url = 'https://ppapifl.foreverliving.fr/api/'.$api."?api_key=".$token;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $type);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $convertedData);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);

        $result = json_decode($result);
        return $result;
    }

    public function thirdPartyFuncMobil($type, $api, $data)
    {

        $ur = env('MOBILVTE');
        $url = $ur . $api;

        // $url = 'https://ppapiflpmobilvte.flpbis.com/api/'.$api;
        $ch = curl_init($url);
        $jsonDataEncoded = json_encode($data);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $type);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        $result = json_decode($result);
        return $result;
    }

    public function thirdPartyDevapifl($type, $api, $data)
    {
        $ur = env('APIFL');
        $url = $ur . $api;

        // $url = 'https://devapifl.foreverliving.fr/api/'.$api;

        $ch = curl_init($url);
        $jsonDataEncoded = json_encode($data);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $type);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        $result = json_decode($result);
        return $result;
    }

    public function thirdPartyFuncRemise($type, $api, $data)
    {

        $apiV2SecretKey = 'jh=6*d87yH5YrS8&k!$k36Z3M&Rd!N';
        $apiV2User = 'FLAPIV2TNG';

        $url = env('APP_URL');
        // if (strpos($url, 'pprest') !== false) {
//            $date = gmdate('Y-m-dH');
        // } else {
             $date = date('Y-m-dH', strtotime('+2 hours'));
        // }
        $token = sha1($apiV2User . '-' . $date . '-' . $apiV2SecretKey);

        $ur = env('REMISE');
        $url = $ur . $api . "?api_key=" . $token;

        // $url = 'https://ppapieremise.flpbis.com/api/.$api;
        $ch = curl_init($url);
        $jsonDataEncoded = json_encode($data);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $type);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        $result = json_decode($result);
        return $result;
    }

    public function copyFileToPublic($path1)
    {
        try {
            $reqPath = str_replace('storage/', '', $path1);
            $path = storage_path('app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . $reqPath);
            $folders = explode('/', $path);
            $publicPath = public_path('storage/'.end($folders));

            if (File::exists($publicPath)) {
            } else {
                File::copy($path, $publicPath);
            }

            return 'storage/'.end($folders);
        }catch(\Exception $e){
            return $path1;
        }

    }

    public function sendDoListMessage($email, $pin, $firstName, $lastName)
    {
        try {

            ini_set("soap.wsdl_cache_enabled", "0");

            // AUTHENTICATION
                $AccountId = 388;

                // Wsdl contract url
                $proxywsdl_auth = "http://api.emt.dolist.net/V3/AuthenticationService.svc?wsdl";
                $location_auth = "http://api.emt.dolist.net/V3/AuthenticationService.svc/soap1.1";

                // Proxy generation
                $client_auth = new SoapClient($proxywsdl_auth, array('trace' => 1, 'location' => $location_auth));
                $request_auth = array(
                    'authenticationRequest' => array(
                        'AuthenticationKey' => 'SITmDchgvCcsBZpOXLLs5unEnlKTYZekum+PRbr4Dw4tqbuU2Qs04zbtbhQiqV13EQN815+sfw+4rT1M7U683Q==',
                        'AccountID' => $AccountId
                    )
                );

                // Authentication request
                $result_auth = $client_auth->GetAuthenticationToken($request_auth);

                if (!is_null($result_auth->GetAuthenticationTokenResult) and $result_auth->GetAuthenticationTokenResult != '') {
                    if ($result_auth->GetAuthenticationTokenResult->Key != '') {
                        // echo "Authentification réussie.<br/>";

                        $proxywsdl = "http://api.emt.dolist.net/V3/MessageService.svc?wsdl";
                          $client = new SoapClient($proxywsdl,
                                                  array('soap_version' => SOAP_1_1,
                                                        'trace' => 1,
                                                        'location'=>"http://api.emt.dolist.net/V3/MessageService.svc/soap1.1"
                                    ));

                          $message = array('Data'=>utf8_encode('<?xml version="1.0" encoding="utf-8"?>
                                                  <emtroot>
                                                  <MDP>    <PRENOM>'.$firstName.'</PRENOM>    <NOM>'.$lastName.'</NOM>    <TOKEN>'.$pin.'</TOKEN>  </MDP>
                                                  </emtroot>'),
                        'MessageContentType'=>'EmailMultipart',
                        'Recipient'=>$email,
                        'TemplateID'=>90,
                        );

                        // Create token
                        $token = array(
                            'AccountID' => $AccountId,
                            'Key' => $result_auth->GetAuthenticationTokenResult->Key
                        );

                        $sendMessageParam = array('token' => $token,
                        'message'=>$message);

                        // Send message
                        $ticket = $client->SendMessage($sendMessageParam);
                        // echo "Message sent : Ticket -> ";
                        // echo $ticket->SendMessageResult;
                    }
                }
            }
            // Error management
            catch(SoapFault $fault) {
            // echo  $fault;
            $detail=$fault->detail;
            // echo "<h3>Error</h3>";
            // echo "Message : ".$detail->ServiceException->Message;
            // echo "<br>
            //   ";
            //   echo "Description : ".$detail->ServiceException->Description;
            //   echo "<br>";
            }

    }

}
