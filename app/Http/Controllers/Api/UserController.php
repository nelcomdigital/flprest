<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\CustomerReview;
use App\Models\InternalNotification;
use App\Models\Notification;
use App\Models\Page;
use App\Models\Status;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\Models\Country;
use App\Models\MobUser;
use App\Mail\PinMail;
use Illuminate\Support\Facades\Mail;


class UserController extends Controller
{
    //

    public function login(Request $request)
    {
        $params = $this->validate($request, [
            'email' => 'required',
            'password' => 'required'
        ]);
        $result = $this->handleAccessResponse($params);

        if (isset($result[0]['success']) && $result[0]['success']) {
            return response()->json($result[0], $result[1]);
        } else {
            return response()->json($result[0], $result[1]);
        }

    }

    public function deviceToken(Request $request)
    {
        $params = $this->validate($request,
            ['device_token' => 'required|min:3', 'device_platform' => 'required|min:3']);
        try {
            $user = MobUser::query()->where('num', $request->user_id)->first();
            if ($user == null) {
                $user = new MobUser();
                $user->num = $request->user_id;
                $user->save();
            }
            $devices = $user->devices;
            if ($devices != null) {
                $tokens = array_column($devices, 'token');
                if (!in_array($params['device_token'], $tokens)) {
                    $devices[] = [
                        'platform' => $params['device_platform'],
                        'token' => $params['device_token']
                    ];
                }
            } else {
                $devices[] = [
                    'platform' => $params['device_platform'],
                    'token' => $params['device_token']
                ];
            }
            $user->devices = $devices;
            $user->save();
            return response()->json(['user' => $user], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => __('app.error_message'), 'code_message' => $e->getMessage()], 500);
        }

    }
    public function getForceUser(Request $request,$num)
    {
        $user = MobUser::query()->where('num', $num)->first();

        return response()->json(['user' => $user], 200);


    }

    public function removeDeviceToken(Request $request)
    {
        $params = $this->validate($request,
            ['device_token' => 'required|min:3']);
        try {
            $user = MobUser::query()->where('num', $request->user_id)->first();
            if ($user == null) {
                $user = new MobUser();
                $user->num = $request->user_id;
                $user->save();
            }
            $devices = $user->devices;
            $newDevices = array();
            if ($devices != null) {
                foreach ($devices as $device) {
                    if ($device['token'] != $params['device_token']) {
                        $newDevices[] = [
                            'platform' => $device['platform'],
                            'token' => $device['token']
                        ];
                    }
                }

            }
            $user->devices = $newDevices;
            $user->save();

            return response()->json(['user' => $user], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => __('app.error_message'), 'code_message' => $e->getMessage()], 500);
        }

    }

    public function profile(Request $request)
    {

        $result = $this->thirdPartyFunc('POST', 'getProfil', [
            "flag" => $request->type,
            "num" => $request->user_id
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function sponsorInfo(Request $request)
    {

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $result = $this->thirdPartyFunc('POST', 'infoParrain', [
            "flag" => $request->type,
            "num" => $request->user_id,
            'env' => $country->products_code
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function setProfile(Request $request)
    {
        $this->validate($request, [
            'phoneNumber' => 'required',
            'address' => 'required',
            'zipCode' => 'required',
            'city' => 'required',
        ]);
        $result = $this->thirdPartyFunc('POST', 'setProfil', [
            "flag" => $request->type,
            "num" => $request->user_id,
            "phoneNumber" => $request->input('phoneNumber'),
            "address" => $request->input('address'),
            "additionalAddress" => $request->has('additionalAddress') ? $request->input('additionalAddress') : '',
            "zipCode" => $request->input('zipCode'),
            "city" => $request->input('city'),
            "gourvernorat" => $request->has('gourvernorat') ? $request->input('gourvernorat') : '',
            "wilaya" => $request->has('wilaya') ? $request->input('wilaya') : '',
            "email" => $request->has('email') ? $request->input('email') : '',
            "comForever" => $request->has('comForever') ? $request->input('comForever') : '',
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function forgotPassword($email)
    {
        $result = $this->thirdPartyFunc('POST', 'forgetPassword', [
            "email" => $email,
        ]);

        if (isset($result->success) && $result->success) {

            $profileResult = $this->thirdPartyFunc('POST', 'getProfil', [
                "flag" => $result->flag,
                "num" => $result->num
            ]);
            if (isset($profileResult->success) && $profileResult->success) {

                if (isset($result->success) && $result->success) {
                    $pin = rand(1000, 9999);
                    $result->token = $this->createToken($result->num, $result->flag, $pin);
                    // send email with the pin to user
                    $this->sendDoListMessage($email, $pin, $profileResult->profil->firstName, $profileResult->profil->lastName);
                    // Mail::to($email)->send(new PinMail($email,$pin));
                    return response()->json($result, 200);
                } else {
                    return response()->json($result, 500);
                }
            } else {
                return response()->json($profileResult, 500);
            }
    } else {
        return response()->json($result, 500);
    }
    }

    public function resetPassword(Request $request)
    {
        $this->validate($request, [
            'password' => 'required',
            'pin' => 'required'
        ]);
        if ($request->server_pin != $request->pin) {

            return response()->json(['message' => 'pin dont match'], 500);

        }
        $result = $this->thirdPartyFunc('POST', 'resetPassword', [
            "flag" => $request->type,
            "num" => $request->user_id,
            "password" => $request->input('password')
        ]);
        if (isset($result->success) && $result->success) {
            $result = (array)$result;
            $result['token'] = $this->createToken($request->user_id, $request->type);
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function getDiscount(Request $request)
    {
        $this->validate($request, [
            'country_code' => 'required',
        ]);

        // if (!$request->headers->has('country-code')) {
        //     $country = Country::query()->where('slug.fr', 'metropole')->first();
        // } else {
        //     $country_code = $request->header('country-code');
        //     $country = Country::query()->where('country_code', $country_code)->first();
        // }

        $result = $this->thirdPartyFuncDiscount('GET', 'directDiscount', [
            'country' => $request->country_code,
            "num" => $request->user_id,
        ]);
        if (isset($result->success) && $result->success) {
            if (!isset($result->rate)) {
                $result->rate = 0.0;
            }
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function getListeArticles(Request $request)
    {


        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }
        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $result = $this->thirdPartyFunc('POST', 'getListeArticles', [
            'env' => $request->headers->has('country-code') ? $country->products_code : "",
            "region" => $request->headers->has('country-code') ? $country->region_code : "",
            "typLiv" => $request->headers->has('country-code') ? $country->product_type : "P",
            "flag" => $request->type,
            "rate" => $request->has('rate') ? $request->input('rate') : '0.0',
            "num" => $request->user_id,
            "notAllRef" => $request->has('notAllRef') ? $request->input('notAllRef') : '0',
        ]);

        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json([$result,
                'env' => $request->headers->has('country-code') ? $country->products_code : "",
                "region" => $request->headers->has('country-code') ? $country->region_code : "",
                "typLiv" => $request->headers->has('country-code') ? $country->product_type : "P",
                "flag" => $request->type,
                "rate" => $request->has('rate') ? $request->input('rate') : '0.0',
                "num" => $request->user_id,
            ], 500);

        }
    }

    public function getArticleAuto(Request $request)
    {
        $this->validate($request, [
            'rate' => 'required',
        ]);

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }


        $result = $this->thirdPartyFunc('POST', 'getArticleAuto', [
            'env' => $country->products_code,
            "region" => $country->region_code,
            "typLiv" => $country->product_type,
            "flag" => $request->type,
            "rate" => $request->input('rate'),
            "num" => $request->user_id,
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function getFraisLivraison(Request $request)
    {
        $this->validate($request, [
            // 'rate'=>'required',
        ]);
        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }


        $result = $this->thirdPartyFunc('POST', 'getFraisLivraison', [
            "flag" => $request->type,
            "num" => $request->user_id,
            'env' => $country->products_code,
            "region" => $country->region_code,
            "modeLiv" => $request->has('modeLiv') ? $request->input('modeLiv') : null,
            "typLiv" => $country->product_type,
            "totalTTC" => $request->has('totalTTC') ? $request->input('totalTTC') : null,
            "totalCC" => $request->has('totalCC') ? $request->input('totalCC') : null,
            "countryLiv" => $country->products_code,
            "zipCodeLiv" => $request->has('zipCodeLiv') ? $request->input('zipCodeLiv') : null,
            "expediteur" => $request->has('expediteur') ? $request->input('expediteur') : null,
            "option" => $request->has('option') ? $request->input('option') : null,

        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function getFlagTest(Request $request)
    {

        return response()->json(['type' => strtoupper($request->type)], 200);

    }

    public function isSub(Request $request)
    {
        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $result = $this->thirdPartyFunc('POST', 'isSub', [
            "flag" => $request->type,
            "numFBO" => $request->user_id,
            'env' => $country->products_code,
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function corporatePage(Request $request, $page)
    {
        $flag = $request->type;

        $page = Page::query()
            ->where('slug', 'like', $page . '%');
        if ($flag == 'FBO') {
            $page = $page->where('FBO', '1');
        }
        if ($flag == 'CLI') {
            $page = $page->where('CLI', '1');
        }
        $page = $page->first();
        $result = array();
        if ($page != null) {
            $result['name'] = $page->name;
            $result['title'] = $page->text;
            $result['description'] = $page->description;
        }
        return $result;
    }

    public function getInternalNotifications(Request $request)
    {
        $notifications = InternalNotification::query();

        $platform = $request->header('platform');
        $type = $request->type;
        $user_id = $request->user_id;
        $now = Carbon::now('Europe/Paris')->toDateString();
        $status = Status::query()->where('slug', 'active')->first();

        $notifications = $notifications
            ->where('status_id', $status->_id)
            ->where('end_date', '>=', $now)
            ->where('end_date', '>=', $now)
            ->where('used_user_ids', '!=', $user_id);

        if ($platform == 'ios') {
            $notifications = $notifications->where('ios', '1');
        }

        if ($platform == 'android') {
            $notifications = $notifications->where('android', '1');
        }

        if ($type == 'FBO') {
            $notifications = $notifications->where('FBO', '1');
        } else {
            $notifications = $notifications->where('CLI', '1');
        }
        $notifications = $notifications->get();

        foreach ($notifications as $notification) {
            $ids = $notification->used_user_ids;
            $ids[] = $user_id;
            $notification->used_user_ids = $ids;
            $notification->save();

            $notificationData['user_id'] = $user_id;
            $notificationData['module'] = 'internal';
            $notificationData['name'] = $notification->name;
            $notificationData['message'] = $notification->message;
            $notificationData['order_id'] = null;
            $notificationData['training_id'] = null;
            $notificationData['event_id'] = null;
            $notificationData['rdv_id'] = null;
            $notificationData['other_rdv_id'] = null;
            $notificationData['participation_date'] = null;
            $notificationData['related_id'] = 'admin';
            $not = Notification::create($notificationData);
        }

        return response()->json(['notifications' => $notifications], 200);

    }

}
