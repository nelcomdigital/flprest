<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Country;
use App\Mail\PinMail;
use Exception;
use Illuminate\Support\Facades\Mail;


class NestorController extends Controller
{

    //Joseph added | It should probably be added to another controller
    public function getNestorServices(Request $request)
    {

        $api = "nestor/services";

        $result =  $this->thirdPartyFuncPpapifl('GET', $api, []);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    //Joseph added | It should probably be added to another controller
    public function getNestorCommandes(Request $request)
    {
        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $api = "nestor/commandes";
        $api .= "/$request->user_id";
        $api .= "/$country->products_code";

        $result =  $this->thirdPartyFuncPpapifl('GET', $api, []);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    //Joseph added | It should probably be added to another controller
    public function createNestorTicket(Request $request)
    {

        $this->validate($request, [
            'id_service' => 'required',
        ]);

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }
      
        $data = [
            "ct_num" => $request->user_id,
            "countryFbo" => $country->country_code,
            "id_service" => (int)($request->id_service),
            "category" => 999998,
            "numCmde" => $request->has('numCmde') ? ((int)($request->input('numCmde'))) : '',
            "object" => $request->has('object') ? $request->input('object') : '',
            "message" => $request->has('message') ? $request->input('message') : '',
        ];

        if($request->has('attachment')){
            $attachments = $request->attachment;
            $data["attachment"]  = $attachments;
        }
        if($request->has('name_attachment')){
            $name_attachments = $request->name_attachment;
            $data["name_attachment"] = $name_attachments;
        }
       

        $api = "nestor/ticket-create";
        $result =  $this->thirdPartyFuncPpapiflFormData('POST', $api, $data);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }
}
