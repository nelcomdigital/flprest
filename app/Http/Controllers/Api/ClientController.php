<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Jobs\SendFCMNotification;
use App\Models\Meeting;
use App\Models\MobUser;
use App\Models\NotificationRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use App\Models\Country;
use App\Mail\PinMail;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;


class ClientController extends Controller
{
    //

    public function authCreateClient(Request $request)
    {

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }


        $result = $this->thirdPartyFuncMobil('GET', 'authCreateClient', [
            'env' => $country->products_code,
            "numFBO" => $request->user_id,
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function getClientList(Request $request)
    {

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }


        $result = $this->thirdPartyFuncMobil('GET', 'getClientList', [
            'env' => $country->products_code,
            "numFBO" => $request->user_id,
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function addClient(Request $request)
    {
        // $this->validate($request, [
        //     'civilite'=>'required',
        //     'firstName'=>'required',
        //     'lastName'=>'required',
        //     'birthdayDate'=>'required',
        //     'comment'=>'required',
        //     'accessShop'=>'required',
        //     'address'=>'required',
        //     'zipCode'=>'required',
        //     'phoneNumber'=>'required',
        //     'foreverCommunication'=>'required',
        //     'rgpd'=>'required',
        // ]);


        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $result = $this->thirdPartyFuncMobil('POST', 'addClient', [
            'env' => $country->products_code,
            "country" => $request->has('country') ? $request->input('country') : '',
            "numFBO" => $request->user_id,
            "numClient" => $request->has('numClient') ? $request->input('numClient') : '',
            "civilite" => $request->has('civilite') ? $request->input('civilite') : '',
            "firstName" => $request->has('firstName') ? $request->input('firstName') : '',
            "lastName" => $request->has('lastName') ? $request->input('lastName') : '',
            "birthdayDate" => $request->has('birthdayDate') ? $request->input('birthdayDate') : '',
            "comment" => $request->has('comment') ? $request->input('comment') : '',
            "accessShop" => $request->has('accessShop') ? $request->input('accessShop') : '',
            "address" => $request->has('address') ? $request->input('address') : '',
            "additionalAddress" => $request->has('additionalAddress') ? $request->input('additionalAddress') : '',
            "zipCode" => $request->has('zipCode') ? $request->input('zipCode') : '',
            "city" => $request->has('city') ? $request->input('city') : '',
            "phoneNumber" => $request->has('phoneNumber') ? $request->input('phoneNumber') : '',
            "email" => $request->has('email') ? $request->input('email') : '',
            "foreverCommunication" => $request->has('foreverCommunication') ? $request->input('foreverCommunication') : '',
            "rgpd" => $request->has('rgpd') ? $request->input('rgpd') : '',

        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function setClient(Request $request)
    {
        // $this->validate($request, [
        // 'numClient'=>'required',
        // 'civilite'=>'required',
        // 'firstName'=>'required',
        // 'lastName'=>'required',
        // 'birthdayDate'=>'required',
        // 'comment'=>'required',
        // 'accessShop'=>'required',
        // 'address'=>'required',
        // 'zipCode'=>'required',
        // 'phoneNumber'=>'required',
        // 'foreverCommunication'=>'required',
        // 'rgpd'=>'required',
        // 'country'=>'required',
        // 'city'=>'required',
        // ]);


        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }


        $result = $this->thirdPartyFuncMobil('PUT', 'setClient', [
            'env' => $country->products_code,
            "country" => $request->has('country') ? $request->input('country') : '',
            "numFBO" => $request->user_id,
            "numClient" => $request->has('numClient') ? $request->input('numClient') : '',
            "civilite" => $request->has('civilite') ? $request->input('civilite') : '',
            "firstName" => $request->has('firstName') ? $request->input('firstName') : '',
            "lastName" => $request->has('lastName') ? $request->input('lastName') : '',
            "birthdayDate" => $request->has('birthdayDate') ? $request->input('birthdayDate') : '',
            "comment" => $request->has('comment') ? $request->input('comment') : '',
            "accessShop" => $request->has('accessShop') ? $request->input('accessShop') : '',
            "address" => $request->has('address') ? $request->input('address') : '',
            "additionalAddress" => $request->has('additionalAddress') ? $request->input('additionalAddress') : '',
            "zipCode" => $request->has('zipCode') ? $request->input('zipCode') : '',
            "city" => $request->has('city') ? $request->input('city') : '',
            "phoneNumber" => $request->has('phoneNumber') ? $request->input('phoneNumber') : '',
            "email" => $request->has('email') ? $request->input('email') : '',
            "foreverCommunication" => $request->has('foreverCommunication') ? $request->input('foreverCommunication') : '',
            "rgpd" => $request->has('rgpd') ? $request->input('rgpd') : '',

        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function deleteClient(Request $request)
    {
        $this->validate($request, [
            'numClient' => 'required',

        ]);

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }


        $result = $this->thirdPartyFuncMobil('DELETE', 'deleteClient', [
            'env' => $country->products_code,
            "numFBO" => $request->user_id,
            "numClient" => $request->input('numClient'),
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function getDeliveryAddress(Request $request)
    {
        $this->validate($request, [
            'numClient' => 'required',

        ]);

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }


        $result = $this->thirdPartyFuncMobil('GET', 'getDeliveryAddress', [
            'env' => $country->products_code,
            "numFBO" => $request->user_id,
            "numClient" => $request->input('numClient'),
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function addDeliveryAddress(Request $request)
    {
        $this->validate($request, [
            'numClient' => 'required',
            'nomAdr' => 'required',
            'contact' => 'required',
            'email' => 'required|email',
            'address' => 'required',
            'zipCode' => 'required',
            'phoneNumber' => 'required',

        ]);

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }
        $result = $this->thirdPartyFuncMobil('POST', 'addDeliveryAddress', [
            "numClient" => $request->input('numClient'),
            'env' => $country->products_code,
            "nomAdr" => $request->input('nomAdr'),
            "contact" => $request->input('contact'),
            "email" => $request->input('email'),
            "address" => $request->input('address'),
            "additionalAddress" => $request->input('additionalAddress'),
            "zipCode" => $request->input('zipCode'),
            "city" => $country->name['fr'],
            "country" => $country->country_code,
            "phoneNumber" => $request->input('phoneNumber'),
            "region" => $country->region_code,
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function setDeliveryAddress(Request $request)
    {
        $this->validate($request, [
            'idAdr' => 'required',
            'numClient' => 'required',
            'nomAdr' => 'required',
            'contact' => 'required',
            'email' => 'required|email',
            'address' => 'required',
            'zipCode' => 'required',
            'phoneNumber' => 'required',

        ]);

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }


        $result = $this->thirdPartyFuncMobil('PUT', 'setDeliveryAddress', [
            'env' => $country->products_code,
            "idAdr" => $request->input('idAdr'),
            "numClient" => $request->input('numClient'),
            "nomAdr" => $request->input('nomAdr'),
            "contact" => $request->input('contact'),
            "email" => $request->input('email'),
            "address" => $request->input('address'),
            "additionalAddress" => $request->input('additionalAddress'),
            "zipCode" => $request->input('zipCode'),
            "city" => $country->name['fr'],
            "country" => $country->country_code,
            "phoneNumber" => $request->input('phoneNumber'),
            "region" => $country->region_code,
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function deleteDeliveryAddress(Request $request)
    {
        $this->validate($request, [
            'idAdr' => 'required',
        ]);

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }


        $result = $this->thirdPartyFuncMobil('DELETE', 'deleteDeliveryAddress', [
            'env' => $country->products_code,
            "idAdr" => $request->input('idAdr'),
            "numClient" => $request->input('numClient'),
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function getSalesMeeting(Request $request)
    {

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }


        $result = $this->thirdPartyFuncMobil('GET', 'getReunionVente', [
            'env' => $country->products_code,
            "numFBO" => $request->user_id,
            "canal" => "ALL",
            "idReunion" => "0",
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function addSalesMeeting(Request $request)
    {

        $this->validate($request, [
            'numClient' => 'required',
            'email' => 'required|email',
            'address' => 'required',
            'zipCode' => 'required',
            'phoneNumber' => 'required',
            'dateReunion' => 'required',

        ]);

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }


        $result = $this->thirdPartyFuncMobil('POST', 'addReunionVente', [
            'env' => $country->products_code,
            "numFBO" => $request->user_id,
            "idReunion" => 0,
            "numClient" => $request->input('numClient'),
            "email" => $request->input('email'),
            "address" => $request->input('address'),
            "additionalAddress" => $request->input('additionalAddress'),
            "zipCode" => $request->input('zipCode'),
            "city" => $country->name['fr'],
            "country" => $country->country_code,
            "phoneNumber" => $request->input('phoneNumber'),
            "region" => $country->region_code,
            "dateReunion" => $request->input('dateReunion'),
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function setSalesMeeting(Request $request)
    {

        $this->validate($request, [
            'numClient' => 'required',
            'email' => 'required|email',
            'address' => 'required',
            'zipCode' => 'required',
            'phoneNumber' => 'required',
            'dateReunion' => 'required',
            'idReunion' => 'required',

        ]);

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }


        $result = $this->thirdPartyFuncMobil('PUT', 'setReunionVente', [
            'env' => $country->products_code,
            "numFBO" => $request->user_id,
            "idReunion" => $request->input('idReunion'),
            "numClient" => $request->input('numClient'),
            "email" => $request->input('email'),
            "address" => $request->input('address'),
            "additionalAddress" => $request->input('additionalAddress'),
            "zipCode" => $request->input('zipCode'),
            "city" => $country->name['fr'],
            "country" => $country->country_code,
            "phoneNumber" => $request->input('phoneNumber'),
            "region" => $country->region_code,
            "dateReunion" => $request->input('dateReunion'),
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function deleteSalesMeeting(Request $request)
    {

        $this->validate($request, [
            'idReunion' => 'required',

        ]);

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }


        $result = $this->thirdPartyFuncMobil('DELETE', 'deleteReunionVente', [
            'env' => $country->products_code,
            "numFBO" => $request->user_id,
            "idReunion" => $request->input('idReunion'),
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function getMeetingParticipant(Request $request)
    {

        $this->validate($request, [
            'idReunion' => 'required',

        ]);

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }


        $result = $this->thirdPartyFuncMobil('GET', 'getParticipantReunion', [
            'env' => $country->products_code,
            "numFBO" => $request->user_id,
            "idReunion" => $request->input('idReunion'),
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function addMeetingParticipant(Request $request)
    {
        $this->validate($request, [
            'idReunion' => 'required',
            'numClient' => 'required',

        ]);
        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }
        $result = $this->thirdPartyFuncMobil('POST', 'addParticipantReunion', [
            'env' => $country->products_code,
            "numFBO" => $request->user_id,
            "idReunion" => $request->input('idReunion'),
            "numClient" => $request->input('numClient'),
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function deleteMeetingParticipant(Request $request)
    {
        $this->validate($request, [
            'idInvite' => 'required',
            'numClient' => 'required',

        ]);
        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }
        $result = $this->thirdPartyFuncMobil('DELETE', 'deleteParticipantReunion', [
            'env' => $country->products_code,
            "idInvite" => $request->input('idInvite'),
            "numClient" => $request->input('numClient'),
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function addOrUpdateOtherMeeting(Request $request)
    {
        try {
            $this->validate($request, [
//                'userNum' => 'required',
                'name' => 'required',
                'all_day' => 'required',
                'start_date' => 'required',
//                'end_date' => 'required',
            ]);
            if (!$request->headers->has('country-code')) {
                $country = Country::query()->where('slug.fr', 'metropole')->first();
            } else {
                $country_code = $request->header('country-code');
                $country = Country::query()->where('country_code', $country_code)->first();
            }

            if ($request->has('meeting_id') && $request->input('meeting_id') != null && $request->input('meeting_id') != '') {
                $meeting = Meeting::find($request->input('meeting_id'));
            } else {
                $meeting = new Meeting();
            }
            $meeting->name = $request->input('name');
            $meeting->userNum = $request->user_id;
            $meeting->start_date = $request->input('start_date');
            $meeting->end_date = $request->input('end_date');
            $meeting->all_day = $request->input('all_day');
            $meeting->detail = $request->input('detail');
            $meeting->save();

            return response()->json($meeting, 200);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);

        }


    }


    public function deleteOtherMeeting(Request $request, $id)
    {
        try {

            $meeting = Meeting::find($id);
            $meeting->delete();
            return response()->json('success', 200);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);

        }


    }

    public function GetUserOtherMeetings(Request $request)
    {
        try {
            $meetings = Meeting::query()->where('userNum', $request->user_id)->get();
            return response()->json(['meetings' => $meetings], 200);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);

        }


    }

    public function getUserOtherMeetingByID(Request $request, $id)
    {

        try {
            $meetings = Meeting::find($id);
            return response()->json(['meeting' => $meetings], 200);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);

        }


    }

}
