<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\CustomerReview;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\Models\Country;
use App\Mail\PinMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\App;


class SaleController extends Controller
{
    //
    public function addVente(Request $request)
    {
        $this->validate($request, [
            'numClient' => 'required',
            'mtRemise' => 'required',
            'percentRemise' => 'required',
            'idReunion' => 'required',
            'country' => 'required',
        ]);
        $country = Country::query()->where('country_code', $request->input('country'))->first();

        $clientResult = $this->thirdPartyFuncMobil('GET', 'getClientList', [
            'env' => $country->products_code,
            "numFBO" => $request->user_id,
        ]);
        $clientSelected = null;
        if ($clientResult->success and $clientResult->success) {
            if (isset($clientResult->clientList) && $clientResult->clientList != null) {
                foreach ($clientResult->clientList as $client) {
                    if ($client->numClient == $request->input('numClient')) {
                        $clientSelected = $client;
                    }
                }
            }
        }
        
        $result = $this->thirdPartyFuncMobil('POST', 'createVente', [
            "numFBO" => $request->user_id,
            'env' => $country->products_code,
            "country" => $clientSelected->country,
            "region" => $country->region_code,
            "numClient" => $request->input('numClient'),
            "mtRemise" => $request->input('mtRemise'),
            "percentRemise" => $request->input('percentRemise'),
            "idReunion" => $request->input('idReunion'),

        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function addSaleLine(Request $request)
    {
        $this->validate($request, [
            'idVente' => 'required',
            'qte' => 'required',
            'mtRemise' => 'required',
            'percentRemise' => 'required',
            'ref' => 'required',
        ]);


        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $result = $this->thirdPartyFuncMobil('POST', 'addLigneVente', [
            "idVente" => $request->input('idVente'),
            'env' => $country->products_code,
            "ref" => $request->input('ref'),
            "qte" => $request->input('qte'),
            "mtRemise" => $request->input('mtRemise'),
            "percentRemise" => $request->input('percentRemise'),


        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function setRemiseGeneral(Request $request)
    {
        $this->validate($request, [
            'idVente' => 'required',
            'mtRemise' => 'required',
            'percentRemise' => 'required',
        ]);


        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $result = $this->thirdPartyFuncMobil('PUT', 'setRemiseGeneral', [
            "idVente" => $request->input('idVente'),
            'env' => $country->products_code,
            "mtRemise" => $request->input('mtRemise'),
            "percentRemise" => $request->input('percentRemise'),
        ]);

        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function addComment(Request $request)
    {
        $this->validate($request, [
            'idVente' => 'required',
        ]);


        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $result = $this->thirdPartyFuncMobil('PUT', 'setCommentaire', [
            "idVente" => $request->input('idVente'),
            'env' => $country->products_code,
            "comment" => $request->input('comment'),
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function addShippingCosts(Request $request)
    {
        $this->validate($request, [
            'idVente' => 'required',
            'fraisPort' => 'required',
        ]);


        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $result = $this->thirdPartyFuncMobil('PUT', 'setFraisPort', [
            "idVente" => $request->input('idVente'),
            'env' => $country->products_code,
            "fraisPort" => $request->input('fraisPort'),
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function setPayment(Request $request)
    {
        $this->validate($request, [
            'idVente' => 'required',
            'idModePaiement' => 'required',
        ]);


        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $result = $this->thirdPartyFuncMobil('PUT', 'setPaiement', [
            "idVente" => $request->input('idVente'),
            'env' => $country->products_code,
            "idModePaiement" => $request->input('idModePaiement'),
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function setDelivery(Request $request)
    {
        $this->validate($request, [
            'idVente' => 'required',
            'firstName' => 'required',
            'lastName' => 'required',
            'email' => 'required|email',
            'address' => 'required',
            'zipCode' => 'required',
            'city' => 'required',
            'country' => 'required',
            'phoneNumber' => 'required',
        ]);


        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $result = $this->thirdPartyFuncMobil('PUT', 'setLivraison', [
            "idVente" => $request->input('idVente'),
            'env' => $country->products_code,
            "firstName" => $request->input('firstName'),
            "lastName" => $request->input('lastName'),
            "email" => $request->input('email'),
            "address" => $request->input('address'),
            "additionalAddress" => $request->input('additionalAddress'),
            "zipCode" => $request->input('zipCode'),
            "city" => $request->input('city'),
            "country" => $request->input('country'),
            "phoneNumber" => $request->input('phoneNumber'),
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function getSalesList(Request $request)
    {
        $this->validate($request, [
            'canal' => 'required',

        ]);


        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $result = $this->thirdPartyFuncMobil('GET', 'getVenteList', [
            "numFBO" => $request->user_id,
            'env' => $country->products_code,
            "canal" => $request->input('canal'),
            "canalDate" => $request->has('canalDate') ? $request->input('canalDate') : 'all',
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function getSalesDoc(Request $request)
    {

        $this->validate($request, [
            'canal' => 'required',
            'idVente' => 'required',
        ]);

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $result = $this->thirdPartyFuncMobil('GET', 'getDocVente', [
            "numFBO" => $request->user_id,
            'env' => $country->products_code,
            "canal" => $request->input('canal'),
            "idVente" => $request->input('idVente'),
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function getSalesDetail(Request $request)
    {
        $this->validate($request, [
            'idVente' => 'required',
        ]);


        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $result = $this->thirdPartyFuncMobil('GET', 'getVenteDetail', [
            "numFBO" => $request->user_id,
            'env' => $country->products_code,
            "idVente" => $request->input('idVente'),

        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function deleteSales(Request $request)
    {
        $this->validate($request, [
            'idVente' => 'required',

        ]);


        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $result = $this->thirdPartyFuncMobil('DELETE', 'deleteVente', [
            "numFBO" => $request->user_id,
            'env' => $country->products_code,
            "idVente" => $request->input('idVente'),

        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function setSalesPay(Request $request)
    {
        $this->validate($request, [
            'idVente' => 'required',

        ]);


        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $result = $this->thirdPartyFuncMobil('PUT', 'setVentePay', [
            "numFBO" => $request->user_id,
            'env' => $country->products_code,
            "idVente" => $request->input('idVente'),

        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function setSalesDelivery(Request $request)
    {
        $this->validate($request, [
            'idVente' => 'required',
        ]);


        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $result = $this->thirdPartyFuncMobil('PUT', 'setVenteDelivery', [
            "numFBO" => $request->user_id,
            'env' => $country->products_code,
            "idVente" => $request->input('idVente'),

        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function signerVente(Request $request)
    {
        $this->validate($request, [
            'idVente' => 'required',
            'applyTVA' => 'required',
            'generateFAC' => 'required',
            'numFac' => 'required',
            'signature' => 'required',

        ]);
        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }
        $result = $this->thirdPartyFuncMobil('POST', 'signerVente', [
            "numFBO" => $request->user_id,
            'env' => $country->products_code,
            "idVente" => $request->input('idVente'),
            "applyTVA" => $request->input('applyTVA'),
            "generateFAC" => $request->input('generateFAC'),
            "numFac" => $request->input('numFac'),
            "signature" => $request->input('signature'),
            "dateSign" => $request->has('dateSign') ? ($request->input('dateSign')) : '',
            "lieuSign" => $request->has('lieuSign') ? ($request->input('lieuSign')) : '',

        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function getSalesListeArticles(Request $request)
    {

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $result = $this->thirdPartyFuncDiscount('GET', 'directDiscount', [
            'country' => $country->country_code,
            "num" => $request->user_id,
        ]);
        if (isset($result->success) && $result->success) {
            if (!isset($result->rate)) {
                $rate = 0.0;
            } else {
                $rate = $result->rate;
            }
        } else {
            $rate = 0.0;
        }
        if (!$request->has('notAllRef') || $request->input('notAllRef') == '0') {
            $rate = 0.0;
        }

        $result = $this->thirdPartyFuncMobil('GET', 'getListeArticles', [
            'env' => $country->products_code,
            "region" => $country->region_code,
            "rate" => $rate,
            "notAllRef" => $request->has('notAllRef') ? $request->input('notAllRef') : '0',
        ]);

        if (isset($result->success) && $result->success) {
            $products = $this->getProducts($result);
            return response()->json(['products' => $products], 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function getModePayment(Request $request)
    {

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }
        $result = $this->thirdPartyFuncMobil('GET', 'getModePaiement', [
            'env' => $country->products_code,
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function getViewSalesDetail(Request $request)
    {
        $numFBO = $request->input('numFBO');
        $env = $request->input('env');
        $idVente = $request->input('idVente');
        $comment = $request->input('comment');

        App::setLocale($request->input('language'));
        if ($comment != null && $comment != 0) {
            $comment = 1;
        } else {
            $comment = 0;
        }
        $result = $this->thirdPartyFuncMobil('GET', 'getVenteDetail', [
            "numFBO" => $numFBO,
            'env' => $env,
            "idVente" => $idVente,
        ]);
        //        dd($result);
        if (isset($result->success) && $result->success) {
            return view('frontend.layouts.partials.details-web', compact('result', 'env', 'comment'));
        } else {
            return response()->json($result, 500);
        }
    }

    public function addWebComment(Request $request)
    {

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $result = $this->thirdPartyFuncMobil('PUT', 'setCommentaire', [
            "idVente" => $request->idVente,
            'env' => $country->products_code,
            "comment" => $request->comment,
        ]);

        if (isset($result->success) && $result->success) {
            return redirect()->route('vente.commentSuccess', 'true');
        } else {
            return redirect()->route('vente.commentSuccess', 'false');
        }
    }

    public function commentSuccess(Request $request, $rr)
    {
        if ($rr) {
            return response()->json($rr, 200);
        } else {
            return response()->json($rr, 500);
        }
    }

    public function getProducts($result)
    {

        $productPrices = $result->listeArticles;
        $refs = [];
        foreach ($productPrices as $price) {
            $refs[$price->ref] = $price;
        }

        //api for product details from carel
        $ts = time();
        $token = hash('sha512', 'tng-2019' . $ts . env('TOKEN_KEY'));
        $ur = env('DISTRIB');
        $url = $ur . 'products/list.api?ts=' .
            $ts . '&token=' . $token;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);

        $result = json_decode($result);
        try {
            $productDetails = $result->data;
        } catch (\Exception $exception) {
            $productDetails = [];
        }
        // Log::info(Carbon::now()->timestamp);


        //mixing carel api and stephan api
        $display = [];
        foreach ($productDetails as $details) {
            $i = $details->ref_produit;


            if (array_key_exists($i, $refs)) {
                $display[$i] = ['price' => $refs[$i], 'detail' => $details];
            }
        }
        // Log::info(Carbon::now()->timestamp);

        return array_values($display);
    }

    public function getRefProduct($result, $ref)
    {

        $productPrices = $result->listeArticles;
        $refs = [];
        foreach ($productPrices as $price) {
            if ($ref == $price->ref) {
                $refs[$price->ref] = $price;
            }
        }

        //api for product details from carel
        $ts = time();
        $token = hash('sha512', 'tng-2019' . $ts . env('TOKEN_KEY'));
        $ur = env('DISTRIB');
        $url = $ur . 'products/list.api?ts=' .
            $ts . '&token=' . $token;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);

        $result = json_decode($result);
        try {
            $productDetails = $result->data;
        } catch (\Exception $exception) {
            $productDetails = [];
        }
        // Log::info(Carbon::now()->timestamp);


        //mixing carel api and stephan api
        $display = [];
        foreach ($productDetails as $details) {
            $i = $details->ref_produit;


            if (array_key_exists($i, $refs)) {
                $display[$i] = ['price' => $refs[$i], 'detail' => $details];
            }
        }
        // Log::info(Carbon::now()->timestamp);

        return array_values($display);
    }

    //Everything below this, Joseph added
    //I don't know if this is right. The base url has to be https://ppapieremise.flpbis.com/api/
    //So it needs to be added to env
    public function verifyPromoCode(Request $request)
    {
        $this->validate($request, [
            'mtCommande' => 'required',
            'idVente' => 'required',
            'codeRemise' => 'required',
            'listProducts' => 'required',
        ]);

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $sponsorResult = $this->thirdPartyFunc('POST', 'infoParrain', [
            "flag" => $request->type,
            "num" => $request->user_id,
            'env' => $country->products_code
        ]);

        $params = [
            "numFBO" => $sponsorResult->infosParrain->numFBO,
            'env' => $country->products_code,
            "region" => $country->region_code,
            "idClient" => $request->user_id,
            "idVente" => $request->input('idVente'),
            "mtCommande" => $request->input('mtCommande'),
            "codeRemise" => $request->input('codeRemise'),
            "type" => strtoupper($request->type),
        ];

        $products = $request->input('listProducts');
        $listProduct = json_decode($products);

        $params['listProducts'] = $listProduct;

        $result = $this->thirdPartyFuncRemise('POST', 'verifCode', $params);

        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function addOffre(Request $request)
    {
        $this->validate($request, [
            'nomOffre' => 'required',
            'dateEmission' => 'required',
            'dateLimiteUtilisation' => 'required',
            'idParamsRemise' => 'required',
            'mtMiniCmde' => 'required',
            'futurClient' => 'required',
        ]);

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $result = $this->thirdPartyFuncRemise('POST', 'addOffre', [
            "numFBO" => $request->user_id,
            'env' => $country->products_code,
            "nomOffre" => $request->input('nomOffre'),
            "dateEmission" => $request->input('dateEmission'),
            "dateLimiteUtilisation" => $request->input('dateLimiteUtilisation'),
            "idParamsRemise" => $request->input('idParamsRemise'),
            "mtRemise" => $request->input('mtRemise'),
            "txRemise" => $request->input('txRemise'),
            "mtMiniCmde" => $request->input('mtMiniCmde'),
            "id_params_condition" => $request->has('idParamsCondition') ? ($request->input('idParamsCondition')) : '',
            "qteMiniCmde" => $request->has('qteMiniCmde') ? ($request->input('qteMiniCmde')) : '',
            "refMiniCmde" => $request->has('refMiniCmde') ? ($request->input('refMiniCmde')) : '',
            "listClient" => $request->has('listClient') ? ($request->input('listClient')) : '',
            "codeRemise" => $request->input('codeRemise'),
            "futurClient" => $request->input('futurClient'),
            "onlyFutur" => $request->has('onlyFutur') ? ($request->input('onlyFutur')) : '',
            "messageClient" => $request->has('messageClient') ? ($request->input('messageClient')) : '',
        ]);

        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function getOffre(Request $request)
    {
        $this->validate($request, [
            'action' => 'required',
        ]);

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $result = $this->thirdPartyFuncRemise('POST', 'getOffre', [
            "numFBO" => $request->user_id,
            'env' => $country->products_code,
            "action" => $request->input('action'),
        ]);

        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function getOffreDetail(Request $request)
    {
        $this->validate($request, [
            'idOffre' => 'required',
        ]);

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $result = $this->thirdPartyFuncRemise('POST', 'getOffreDetail', [
            'env' => $country->products_code,
            "idOffre" => $request->input('idOffre'),
        ]);

        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function setOffre(Request $request)
    {
        $this->validate($request, [
            'idOffre' => 'required',
            'dateLimiteUtilisation' => 'required',
            'mtMiniCmde' => 'required',
            'futurClient' => 'required',
        ]);

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $result = $this->thirdPartyFuncRemise('PUT', 'setOffre', [
            'env' => $country->products_code,
            "idOffre" => $request->input('idOffre'),
            "dateLimiteUtilisation" => $request->input('dateLimiteUtilisation'),
            "mtMiniCmde" => $request->input('mtMiniCmde'),
            "refMiniCmde" => $request->has('refMiniCmde') ? ($request->input('refMiniCmde')) : '',
            "qteMiniCmde" => $request->has('qteMiniCmde') ? ((int)($request->input('qteMiniCmde'))) : 0,
            "futurClient" => $request->input('futurClient'),
            "onlyFutur" => $request->has('onlyFutur') ? ($request->input('onlyFutur')) : '',
        ]);

        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function deleteOffre(Request $request)
    {
        $this->validate($request, [
            'idOffre' => 'required',
        ]);

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $result = $this->thirdPartyFuncRemise('DELETE', 'deleteOffre', [
            'env' => $country->products_code,
            "numFBO" => $request->user_id,
            "idOffre" => $request->input('idOffre'),
        ]);

        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function addCodes(Request $request)
    {
        $this->validate($request, [
            'listClient' => 'required',
            'idOffre' => 'required',
        ]);

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $result = $this->thirdPartyFuncRemise('POST', 'addCodes', [
            "numFBO" => $request->user_id,
            'env' => $country->products_code,
            "listClient" => $request->input('listClient'),
            "idOffre" => $request->input('idOffre'),
        ]);

        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function getProductsClient(Request $request)
    {

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $result = $this->thirdPartyFuncRemise('POST', 'getProductsClient', [
            'env' => $country->products_code,
        ]);

        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function getParamsRemise(Request $request)
    {

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $result = $this->thirdPartyFuncRemise('POST', 'getParamsRemise', [
            'env' => $country->products_code,
        ]);

        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function getConditionClients(Request $request)
    {

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $result = $this->thirdPartyFuncRemise('POST', 'getConditionClients', [
            'env' => $country->products_code,
        ]);

        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function getOfferClients(Request $request)
    {
        // $this->validate($request, [
        //     'idOffre' => 'required',
        //     'qte' => 'required',
        //     'mois' => 'required',
        // ]);

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $result = $this->thirdPartyFuncRemise('POST', 'getClients', [
            "numFBO" => $request->user_id,
            'env' => $country->products_code,
            "idParamsCondition" => $request->has('idParamsCondition') ? ((int)($request->input('idParamsCondition'))) : '',
            "idOffre" => $request->has('idOffre') ? ($request->input('idOffre')) : '',
            "refBuy" => $request->has('refBuy') ? ((int)($request->input('refBuy'))) : '',
            "qte" => $request->has('qte') ? ($request->input('qte')) : '',
            "mois" => $request->has('mois') ? ($request->input('mois')) : '',
        ]);

        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }


    public function saveClientIdCart(Request $request)
    {

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }
        $type= $request->input('type'); // client/prospect
        $numFBO = $request->user_id;
        $env = $country->products_code;
        $numClient = $request->input('numClient');
        $customerReview = new CustomerReview();
        $customerReview->numClient = $numClient;
        $customerReview->numFBO = $numFBO;
        $customerReview->country = $country->toArray();
        $customerReview->status = 'ongoing';
        if($type == 'prospect'){
            $customerReview->type = 'prospect'; 
        }else{
            $customerReview->type = 'client'; 
        }
        $customerReview->save();

        return response()->json(["success" => true, 'object_id' => $customerReview->_id], 200);
    }

    public function addClientSaleLine(Request $request)
    {
        $this->validate($request, [
            'qte' => 'required',
            'ref' => 'required',
            'object_id' => 'required',
        ]);

        $total = 0;
        $ClientTotal = 0;
        $marge = 0;
        $totalFboCC = 0;
        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }
        $result = $this->thirdPartyFuncDiscount('GET', 'directDiscount', [
            'country' => $country->country_code,
            "num" => $request->user_id,
        ]);
        if (isset($result->success) && $result->success) {
            if (!isset($result->rate)) {
                $rate = 0.0;
            } else {
                $rate = $result->rate;
            }
        } else {
            $rate = 0.0;
        }

        $customerReview = CustomerReview::find($request->input('object_id'));

        if ($customerReview->numOrder == null) {
            if($customerReview->type == 'client'){
                $clientResult = $this->thirdPartyFuncMobil('GET', 'getClientList', [
                    'env' => $customerReview->country['products_code'],
                    "numFBO" => $customerReview->numFBO,
                ]);
                $clientSelected = null;
                if ($clientResult->success and $clientResult->success) {
                    if (isset($clientResult->clientList) && $clientResult->clientList != null) {
                        foreach ($clientResult->clientList as $client) {
                            if ($client->numClient == $customerReview->numClient) {
                                $clientSelected = $client;
                            }
                        }
                    }
                }
                $customerReview->client = $clientSelected;
    
                //check if there is no client what value we shuld take
                $region = Country::query()->where('country_code', $clientSelected->country)->first()->region_code;
                $result = $this->thirdPartyFunc('POST', 'createCmde', [
                    "flag" => 'CLI',
                    "env" => "FRA",
                    "num" => $customerReview->numClient,
                    "region" => $region,
                    "typLiv" => "P",
                    "rate" => '0',
                    "country" => $clientSelected->country,
                    "origine" => "link",
                ]);
                $id = $result->numOrder;
                $customerReview->numOrder = $id;
                $customerReview->save();
            }else{
                
                $customerReview->client = null;
                //check if there is no client what value we shuld take
                $region = Country::query()->where('country_code', 'FRA')->first()->region_code;
                $result = $this->thirdPartyFunc('POST', 'createCmdeInvite', [
                    "numClient" => 0,
                    "numFBO" =>  $customerReview->numFBO,
                    "env" => "FRA",
                    "region" => $region,
                    "typLiv" => "P",
                    "rate" => '0',
                    "country" => 'FRA',
                    "origine" => "link",
                ]);

                $id = $result->id_vente;
                $customerReview->numOrder = $id;
                $customerReview->save();
            }
          
        }


        $products = $customerReview->products;
        $found = false;
        $newProducts = array();
        if ($products != null && count($products) > 0) {
            foreach ($products as $product) {


                $result = $this->thirdPartyFuncMobil('GET', 'getListeArticles', [
                    'env' => $country->products_code,
                    "region" => $country->region_code,
                    "numFBO" => $customerReview->numFBO,
                    "rate" => $rate,
                ]);
                if ($product['ref'] == $request->input('ref')) {
                    $found = true;
                    if ($request->input('qte') != '0' && $request->input('qte') != 0) {

                        $searchProducts = null;
                        if (isset($result->success) && $result->success) {

                            $searchProducts = $this->getRefProduct($result, $request->input('ref'));
                        }
                        if ($searchProducts != null && count($searchProducts) > 0) {
                            $ClientTotal = $ClientTotal + ((float)$request->input('qte') * $searchProducts[0]['price']->prixTTC);
                            $total = $total + ((float)$request->input('qte') * $searchProducts[0]['price']->prixTTCFBO);
                            $totalFboCC = $totalFboCC + ((float)$request->input('qte') * $searchProducts[0]['price']->FBOCC);
                            $newProducts[] = [
                                'ref' => $request->input('ref'),
                                'qte' => $request->input('qte'),
                                'price' => $searchProducts[0]['price'],
                                'detail' => $searchProducts[0]['detail'],
                            ];
                        }
                    }
                } else {
                    $searchProducts = null;
                    if (isset($result->success) && $result->success) {
                        $searchProducts = $this->getRefProduct($result, $product['ref']);
                    }

                    if ($searchProducts != null && count($searchProducts) > 0) {
                        $ClientTotal = $ClientTotal + ((float)$product['qte'] * $searchProducts[0]['price']->prixTTC);
                        $total = $total + ((float)$product['qte'] * $searchProducts[0]['price']->prixTTCFBO);
                        $totalFboCC = $totalFboCC + ((float)$request->input('qte') * $searchProducts[0]['price']->FBOCC);

                        $newProducts[] = [
                            'ref' => $product['ref'],
                            'qte' => $product['qte'],
                            'price' => $searchProducts[0]['price'],
                            'detail' => $searchProducts[0]['detail'],
                        ];
                    }
                }
            }
        }
        if (!$found) {
            if ($request->input('qte') != '0' && $request->input('qte') != 0) {
                $result = $this->thirdPartyFuncMobil('GET', 'getListeArticles', [
                    'env' => $country->products_code,
                    "region" => $country->region_code,
                    "numFBO" => $customerReview->numFBO,
                    "rate" => $rate,
                ]);
                $searchProducts = null;
                if (isset($result->success) && $result->success) {
                    $searchProducts = $this->getRefProduct($result, $request->input('ref'));
                }
                if ($searchProducts != null && count($searchProducts) > 0) {
                    $ClientTotal = $ClientTotal + ((float)$request->input('qte') * $searchProducts[0]['price']->prixTTC);
                    $total = $total + ((float)$request->input('qte') * $searchProducts[0]['price']->prixTTCFBO);
                    $totalFboCC = $totalFboCC + ((float)$request->input('qte') * $searchProducts[0]['price']->FBOCC);

                    $newProducts[] = [
                        'ref' => $request->input('ref'),
                        'qte' => $request->input('qte'),
                        'price' => $searchProducts[0]['price'],
                        'detail' => $searchProducts[0]['detail'],
                    ];
                }
            }
        }
        $marge = $ClientTotal - $total;
        $customerReview->products = $newProducts;
        $customerReview->total = '' . $total;
        $customerReview->total_fbo_cc = '' . $totalFboCC;
        $customerReview->client_total = '' . $ClientTotal;
        $customerReview->marge = '' . $marge;
        $customerReview->save();
        foreach ($newProducts as $product) {
            if($customerReview->type == 'client'){
                $result = $this->thirdPartyFunc('POST', 'ajouteLigneCmde', [
                    "flag" => 'CLI',
                    "num" => $customerReview->numClient,
                    "env" => "FRA",
                    "numOrder" => $customerReview->numOrder,
                    "qte" => $product['qte'],
                    "ref" => $product['ref'],
                    "perso" => "",
                ]);
            }else{
                $result = $this->thirdPartyFunc('POST','ajouteLigneCmdeInvite', [
                    "env" =>"FRA",
                    "region" => $country->region_code,
                    "id_vente" =>$customerReview->numOrder,
                    "qte" => $product['qte'],
                    "ref" => $product['ref'],
                    "origine"=>"link"
                ]);
            }
    
          
        }

        return response()->json(["success" => true, "msgErreur" => "", "numErreur" => 0, "result" => $customerReview], 200);
    }

    public function addClientShippingData(Request $request)
    {
        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }
        $this->validate($request, [
            'object_id' => 'required',
        ]);
        $customerReview = CustomerReview::find($request->input('object_id'));

        $array = [
            "flag" => $request->type,
            'env' => $country->products_code,
            "typLiv" => $request->input('typLiv'),
            "numOrder" => $request->input('numOrder'),
            "numPtRelais" => $request->input('numPtRelais'),
            "typePtRelais" => $request->input('typePtRelais'),
            "nameLiv" => $request->input('nameLiv'),
            "addressLiv" => $request->input('addressLiv'),
            "additionalAddressLiv" => $request->input('additionalAddressLiv'),
            "zipCodeLiv" => $request->input('zipCodeLiv'),
            "infosLiv" => $request->input('infosLiv'),
            "phoneLiv" => $request->input('phoneLiv'),
            "emailLiv" => $request->input('emailLiv'),
            "cityLiv" => $request->input('cityLiv'),
            "countryLiv" => $request->input('countryLiv'),
            "gareLiv" => $request->input('gareLiv'),
            "modeLiv" => $request->input('modeLiv'),
            "codeCity" => $request->input('codeCity'),
            "prestataire" => $request->input('prestataire'),

        ];
        $customerReview->shipping_data = $array;
        $customerReview->save();
        return response()->json(["success" => true, "msgErreur" => "", "numErreur" => 0, "result" => $customerReview], 200);
    }

    public function getSalesClientDetail(Request $request)
    {
        $this->validate($request, [
            'object_id' => 'required',
        ]);

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }
        $customerReview = CustomerReview::find($request->input('object_id'));

        $customerReview->save();
    
        if($customerReview->type == 'client'){
            $order = $this->thirdPartyFunc('POST', 'getPanier', [
                "flag" => "CLI",
                "env" => "FRA",
                "numOrder" =>$customerReview->numOrder,
            ]);
        }else{
            $order = $this->thirdPartyFunc('POST', 'getPanierInvite', [
                "env" => "FRA",
                "id_vente" => $customerReview->numOrder,
            ]);
        }
        
      
       
        $link = route('customer-review', ['id' => $customerReview->_id]);
        $customerReview->link=$link;
        $customerReview->save();
        $remiseObject=null;
        $customerReview->pure_client_total= $customerReview->client_total ;
        if (isset($order->panierDetail) && $order->panierDetail != null){
            foreach ($order->panierDetail as $product){
                if ($product->typeProduit == 'R'){
                    $remiseObject= [
                        'name'=>$product->designation,
                        'ref'=>$product->ref,
                        'prixTTC'=>$product->prixTTC,
                    ];
                    $price=$customerReview->client_total;
                    $customerReview->client_total= $price + $product->prixTTC ;
                }
                 
            }
        }
        $customerReview->remiseObject= $remiseObject;
       
        return response()->json(["success" => true, "msgErreur" => "", "numErreur" =>  $order, "result" => $customerReview,'link'=> $link], 200);
    }
    public function addLigneVente(Request $request)  {
                $this->validate($request, [
            'object_id' => 'required',
        ]);

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }
        $customerReview = CustomerReview::find($request->input('object_id'));
        
    }

    public function clientLinkCheckout(Request $request)
    {
        $this->validate($request, [
            'object_id' => 'required',
        ]);

        $customerReview = CustomerReview::find($request->input('object_id'));
        $id = $customerReview->numOrder;
    
        $result = $this->thirdPartyFunc('POST', 'getPromo', [
            "flag" => 'CLI',
            "env" => "FRA",
            "numOrder" => $id,
            "isForClient" => '1',
            "num" => $customerReview->numClient,
        ]);
        $result = $this->thirdPartyFunc('POST', 'ctrlPanier', [
            "flag" => "CLI",
            "env" => "FRA",
            "numOrder" => $id,
        ]);

        $result = $this->thirdPartyFunc('POST', 'getPanier', [
            "flag" => "CLI",
            "env" => "FRA",
            "numOrder" => $id,
        ]);

        $link = route('customer-review', ['id' => $customerReview->_id]);
        $customerReview->link = $link;

        $result->link = $link;

        $products = $result->panierDetail;
        $array = array();
        foreach ($products as $product) {

            foreach ($customerReview->products as $p) {
                if ($p['ref'] == $product->ref) {
                    if (isset($p['price']['prixTTCFBO'])) {
                        $product->prixTTCFBO = $p['price']['prixTTCFBO'];
                    }
                }
            }
            $array[] = $product;
        }

        $total = 0;
        $ClientTotal = 0;
        $newProducts = array();
        foreach ($products as $product) {
            $found = false;

            foreach ($customerReview->products as $p) {

                if ($p['ref'] == $product->ref) {
                    $found = true;

                    $priceA = [
                        'ref' => $p['price']['ref'],
                        'prixTTC' => $product->prixTTCRemise,
                    ];

                    if (isset($p['price']['prixTTCFBO']) && $p['price']['prixTTCFBO'] != null) {
                        $priceA['prixTTCFBO'] = $p['price']['prixTTCFBO'];
                    }
                    if (isset($p['price']['designation']) && $p['price']['designation'] != null) {
                        $priceA['designation'] = $p['price']['designation'];
                    }
                    if (isset($p['price']['tauxTVA']) && $p['price']['tauxTVA'] != null) {
                        $priceA['tauxTVA'] = $p['price']['tauxTVA'];
                    }
                    if (isset($p['price']['prixHT']) && $p['price']['prixHT'] != null) {
                        $priceA['prixHT'] = $p['price']['prixHT'];
                    }
                    if (isset($p['price']['prixHT']) && $p['price']['prixHT'] != null) {
                        $priceA['prixHT'] = $p['price']['prixHT'];
                    }
                    $newProducts[] = [
                        'ref' => $product->ref,
                        'qte' => $product->qte,
                        'price' => $priceA,
                        'detail' => $p['detail'],
                    ];
                    //                    $ClientTotal = $ClientTotal + ((float)$product->qte * (float)$product->prixTTCRemise);
                    if (isset($p['price']['prixTTC']) && $p['price']['prixTTC'] != null) {
                        $ClientTotal = $ClientTotal + ((float)$product->qte * (float)$p['price']['prixTTC']);
                    }
                    if (isset($p['price']['prixTTCFBO']) && $p['price']['prixTTCFBO'] != null) {
                        $total = $total + ((float)$product->qte * (float)$p['price']['prixTTCFBO']);
                    }
                }
            }
            if (!$found) {
                $newProducts[] = [
                    'ref' => $product->ref,
                    'qte' => $product->qte,
                    'price' => [
                        'ref' => $product->ref,
                        'prixTTC' => $product->prixTTCRemise,
                    ],
                    'detail' => [
                        'label' => [
                            'fr' => $product->designation
                        ]
                    ],
                ];
                $ClientTotal = $ClientTotal + ((float)$product->qte * $product->prixTTC);
                $total = $total + ((float)$product->qte * $product->prixTTC);
            }
        }
        if ($newProducts != null) {

            $customerReview->products = $newProducts;
        }
        $customerReview->total = $total;
        $customerReview->client_total = '' . $ClientTotal;
        $customerReview->save();

        $value = $result->panierHead[0];
        $value->total_fbo_cc = $customerReview->total_fbo_cc != null ? '' . $customerReview->total_fbo_cc : 0;
        $value->total_fbo_price = $customerReview->total != null ? '' . $customerReview->total : 0;
        $value->total_fbo_client = $customerReview->client_total != null ? '' . $customerReview->client_total : 0;
        $result->panierHead = [$value];
        $result->panierDetail = $array;
        // return response()->json($result, 200);
        return response()->json(['link' => $link], 200);
    }
    public function addClientRemise(Request $request)
    {
        $this->validate($request, [
            'object_id' => 'required',
        ]);
        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }
        $customerReview = CustomerReview::find($request->input('object_id'));
        $id = $customerReview->numOrder;

        $result = $this->thirdPartyFuncDiscount('GET', 'directDiscount', [
            'country' => $country->country_code,
            "num" => $request->user_id,
        ]);
        if (isset($result->success) && $result->success) {
            if (!isset($result->rate)) {
                $rate = 0.0;
            } else {
                $rate = $result->rate;
            }
        } else {
            $rate = 0.0;
        }
    
        $result = $this->thirdPartyFunc('POST', 'getRemiseInvite', [
            "env" => "FRA",
            "id_vente" => $customerReview->numOrder,
            "origine"=> "link", 
            "montantRemise"=> $request->input('montantRemise'),
            "pourcentageRemise"=> $request->input('pourcentageRemise'), 
            "region" => $country->region_code,
        ]);
        if( !$result->success){
            return response()->json(['errors' => $result->msgErreur], 500);
        }
       

        if($customerReview->type == 'client'){
            $result = $this->thirdPartyFunc('POST', 'ajouteLigneCmde', [
                "flag" => 'CLI',
                "num" => $customerReview->numClient,
                "env" => "FRA",
                "numOrder" => $customerReview->numOrder,
                "qte" =>$result->Qte_remise,
                "ref" => $result->Ref_remise,
                "perso" => "",
            ]);
        }else{
            $result = $this->thirdPartyFunc('POST','ajouteLigneCmdeInvite', [
                "env" =>"FRA",
                "region" => $country->region_code,
                "id_vente" =>$customerReview->numOrder,
                "qte" =>$result->Qte_remise,
                "ref" => $result->Ref_remise,
                "origine"=>"link"
            ]);
        }
        if($customerReview->type == 'client'){
            $result = $this->thirdPartyFunc('POST', 'getPanier', [
                "flag" => "CLI",
                "env" => "FRA",
                "numOrder" =>$customerReview->numOrder,
            ]);
        }else{
            $result = $this->thirdPartyFunc('POST', 'getPanierInvite', [
                "env" => "FRA",
                "id_vente" => $customerReview->numOrder,
            ]);
        }

     
                      
        // return response()->json($result, 200);
        return response()->json(['result' => $result], 200);
    }
    
}
