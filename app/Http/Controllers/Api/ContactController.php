<?php

namespace App\Http\Controllers\Api;

use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\Contact;
use App\Models\Group;
use App\Models\ReminderType;
use App\Models\Reminder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use App\Mail\GroupMail;
use Illuminate\Support\Facades\Mail;


class ContactController extends Controller
{


    public function addContact (Request $request) {
        if(Contact::query()->where('user_id',$request->user_id)->count()> 100){
            return response()->json(['message'=>'out of limit'], 500);
        }
        $params = $this->validate($request, [
            'phone'=>'required',
            'name'=>'required',
            'email'=>'required|email',
        ]);
        $contact = new Contact();
        $contact->name = $params['name'];
        $contact->phone = $params['phone'];
        $contact->email = $params['email'];
        $contact->user_id = $request->user_id;
        if ($request->has('photo')) {
            $fileName = $this->uploadAny($request->input("photo"), 'contacts','.png');
            $contact->photo = 'storage/'.$fileName;
        }
        if ( $request->has('comment'))
        $contact->comment = $request->input('comment');
        if ( $request->has('reminder_id'))
        $contact->reminder_id = $request->input('reminder_id');
        $contact->save();

        return response()->json(['contact'=>$contact], 200);


    }

    public function getContacts (Request $request) {

        if(Contact::query()->where('user_id',$request->user_id)->count() == 0){
            return response()->json(['message'=>'no contacts found'], 500);
        }
        $contacts = Contact::query()->with('reminder')->where('user_id', $request->user_id)
            ->orderBy('name','asc')->get();


        return response()->json(['contacts'=>$contacts], 200);



    }
    public function updateContact (Request $request) {

        if (Contact::query()->where('_id', $request->input('contact_id'))
        ->where('user_id', $request->user_id)->count() == 0 ) {
            return response()->json(['message'=>'no contact found'], 500);
        }

        $params = $this->validate($request, [
            'contact_id'=>'required',
        ]);
        $contact = Contact::query()->find($params['contact_id']);
        if ( $request->has('name'))
        $contact->name = $request->input('name');
        if ( $request->has('phone'))
        $contact->phone = $request->input('phone');
        if ( $request->has('email'))
        $contact->email = $request->input('email');
        if ( $request->has('reminder_id'))
        $contact->reminder_id = $request->input('reminder_id');
        if ( $request->has('comment'))
        $contact->comment = $request->input('comment');
        if ($request->has('photo')) {
            $fileName = $this->uploadAny($request->input("photo"), 'contacts','.png');
            $contact->photo = 'storage/'.$fileName;
        }

        $contact->save();

        return response()->json(['contact'=>$contact], 200);


    }
    public function deleteContact (Request $request) {

        if (Contact::query()->where('_id', $request->input('contact_id'))
        ->where('user_id', $request->user_id)->count() == 0 ) {
            return response()->json(['message'=>'no contact found'], 500);
        }
        $params = $this->validate($request, [
            'contact_id'=>'required',
        ]);
        $contact = Contact::query()->find($params['contact_id']);
        $contact->delete();
        return response()->json(['message'=>'contact deleted'], 200);


    }
    public function addGroup (Request $request) {
        $params = $this->validate($request, [
            'name'=>'required',
        ]);
        $group = new Group();
        $group->name = $params['name'];
        $group->user_id = $request->user_id;
        $group->save();

        if ($request->has('subject')) {
            $group->subject = $request->input('subject');
        }else{
            $group->subject = null;
        }
        if ($request->has('message')) {
            $group->message = $request->input('message');
        }else{
            $group->message = null;
        }
        if ($request->has('photo')) {
            $fileName = $this->uploadAny($request->input("photo"), 'groups','.png');
            $group->photo = 'storage/'.$fileName;
        }else{
            $group->photo = null;
        }
        if ($request->has('attachment')) {
            $fileName = $this->uploadAny($request->input("attachment"), 'groupAttachments', '.pdf');
            $group->attachment = 'storage/'.$fileName;
        }else{
            $group->attachment = null;
        }
        $group->save();
        $contacts = [] ;
        if ($request->has('contact_ids')) {
            $group->contacts()->attach($request->input('contact_ids'));
            foreach ($request->input('contact_ids') as $contact_id) {
                $contact = Contact::query()->where('_id', $contact_id)->first();
                array_push($contacts, $contact);
            }
            $emails = [];
            foreach ($contacts as $contact) {
                $email = $contact->email;
                array_push($emails, $email);
            }

            foreach ($emails as $email) {
                Mail::to($email)->send(new GroupMail($email, $group->subject, $group->message, $group->attachment, $group->photo));
            }
        }


        $group->save();
        return response()->json(['group'=>$group], 200);
    }
    public function updateGroup (Request $request) {
        $this->validate($request, [
            'group_id'=>'required',
        ]);
        if (Group::query()->where('_id', $request->input('group_id'))
                ->where('user_id', $request->user_id)->count() == 0) {
            return response()->json(['message'=>'no groups found'], 500);
        }
        $group = Group::query()->find($request->input('group_id'));

        if($request->has('name'))
            $group->name = $request->input('name');

        if ($request->has('contact_ids')) {
            $group->contacts()->detach();
            $group->contact_ids = [];
            $group->save();
            $group->contacts()->attach($request->input('contact_ids'));
            $group->save();
        }
        if ($request->has('subject'))
            $group->subject = $request->input('subject');

        if ($request->has('message'))
            $group->message = $request->input('message');

        if ($request->has('photo')) {
            $fileName = $this->uploadAny($request->input("photo"), 'groups','.png');
            $group->photo = 'storage/'.$fileName;
        }
        if ($request->has('attachment')) {
            $fileName = $this->uploadAny($request->input("attachment"), 'groupAttachments','.pdf');
            $group->attachment = 'storage/'.$fileName;
        }
        $group->save();
        $group1 = Group::query()->with('contacts')->where('_id', $group->_id)->first();
        return response()->json(['group'=>$group1], 200);
    }
    public function sendGroupMessage (Request $request) {
        $this->validate($request, [
            'contact_ids'=>'required',
            'subject'=>'required',
            'message'=>'required',
        ]);
        $contacts = [] ;
        foreach($request->input('contact_ids') as $contact_id ){
            $contact = Contact::query()->where('_id',$contact_id)->first();
            array_push($contacts,$contact);
        }
        $emails = [];
        foreach ($contacts as $contact) {
            $email = $contact->email;
            array_push($emails, $email);
        }
        if ($request->has('attachment')) {
            $fileName = $this->uploadAny($request->input("attachment"), 'messagesAttachments', '.pdf');
            $attachment = 'storage/'.$fileName;
        }
        $subject = $request->input('subject');
        $bodyMessage = $request->input('message');
        foreach($emails as $email){
            Mail::to($email)->send(new GroupMail($email, $subject,$bodyMessage,$attachment));
        }

        return response()->json(['message'=>'success'], 200);
    }
    public function addToGroup (Request $request) {
        if (Group::query()->where('_id', $request->input('group_id'))
        ->where('user_id', $request->user_id)->count() == 0) {
            return response()->json(['message'=>'no groups found'], 500);
        }
        $params = $this->validate($request, [
            'group_id'=>'required',
            'contact_ids'=>'required',
        ]);
        $group = Group::query()->find($params['group_id']);
        $group->contacts()->attach($request->input('contact_ids'));
        $group->save();
        return response()->json(['group'=>$group], 200);
    }
    public function getGroups (Request $request) {
        if (Group::query()->where('user_id', $request->user_id)->count() == 0) {
            return response()->json(['groups'=>[]], 500);
        }
        $groups = Group::query()->with('contacts')->where('user_id', $request->user_id)->get();
        return response()->json(['groups'=>$groups], 200);
    }

    public function groupDetail (Request $request) {
        if (Group::query()->where('_id', $request->input('group_id'))->count() == 0 ) {
            return response()->json(['message'=>'no groups found'], 500);
        }
        $params = $this->validate($request, [
            'group_id'=>'required',
        ]);
        $group = Group::query()->with('contacts')->find($params['group_id']);
        return response()->json(['group'=>$group], 200);
    }

    public function deleteGroup (Request $request) {

        if (Group::query()->where('_id', $request->input('group_id'))->count() == 0 ) {
            return response()->json(['message'=>'no groups found'], 500);
        }
        $params = $this->validate($request, [
            'group_id'=>'required',
        ]);
        $group = Group::query()->where('_id',$params['group_id'])->first();
        $group->delete();
        return response()->json(['message'=>'group deleted'], 200);


    }
    public function getReminders (Request $request) {
        if (Reminder::query()->get()->count() == 0) {
            return response()->json(['message'=>'no reminders found'], 500);
        }
        $reminders = Reminder::query()->get();
        return response()->json(['reminders'=>$reminders], 200);
    }

    public function addReminder (Request $request) {

        $params = $this->validate($request, [
            'name'=>'required',
            'date'=>'required',
            'color'=>'required',
        ]);
        $reminder = new Reminder();
        $reminder->name = $params['name'];
        $reminder->date = $params['date'];
        $reminder->color = $params['color'];
        $reminder->save();

        return response()->json(['reminder'=>$reminder], 200);
    }
    // public function getReminderTypes (Request $request) {

    //     if (ReminderType::query()->get()->count() == 0) {
    //         return response()->json(['message'=>'no reminder types found'], 500);
    //     }
    //     $reminderTypes = ReminderType::query()->get();
    //     return response()->json(['reminderTypes'=>$reminderTypes], 200);
    // }
    // public function deleteReminderType (Request $request) {

    //     if (ReminderType::query()->where('_id', $request->input('reminder_type_id'))->get()->count() == 0) {
    //         return response()->json(['message'=>'no reminder type found'], 500);
    //     }
    //     $params = $this->validate($request, [
    //         'reminder_type_id'=>'required',
    //     ]);
    //     $reminderType = ReminderType::query()->where('_id', $params['reminder_type_id'])->first();
    //     $reminderType->delete();
    //     return response()->json(['message'=>'reminder Type deleted'], 200);


    // }
    public function deleteReminder (Request $request) {

        if (Reminder::query()->where('_id', $request->input('reminder_id'))->get()->count() == 0) {
            return response()->json(['message'=>'no reminder type found'], 500);
        }
        $params = $this->validate($request, [
            'reminder_id'=>'required',
        ]);
        $reminder = Reminder::query()->where('_id', $params['reminder_id'])->first();
        $reminder->delete();
        return response()->json(['message'=>'reminder deleted'], 200);


    }

    public function addReminderToContact (Request $request) {

        if (Contact::query()->where('_id', $request->input('contact_id'))
        ->where('user_id', $request->user_id)->count() == 0 ) {
            return response()->json(['message'=>'no contact found'], 500);
        }
        $params = $this->validate($request, [
            'contact_id'=>'required',
            'reminder_id'=>'required',
        ]);
        $contact = Contact::query()->where('_id',$params['contact_id'])->first();
        $contact->reminder_id = $params['reminder_id'];
        $contact->save();
        return response()->json(['contact'=>$contact], 200);
    }



    public function uploadAny($file, $folder,$ex){
        $file = base64_decode($file);
        $file_name = Str::random(25).$ex; //generating unique file name;
        if (!Storage::disk('public')->exists($folder))
        {
            Storage::disk('public')->makeDirectory($folder);
        }
        $result = false;
        if($file!=""){ // storing image in storage/app/public Folder
            $result = Storage::disk('public')->put($folder.'/'.$file_name,$file);

        }
        if ( $result )
            return $folder.'/'.$file_name;
        else
            return null;
    }
}
