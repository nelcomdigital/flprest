<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Country;
use App\Mail\PinMail;
use Exception;
use Illuminate\Support\Facades\Mail;


class FormationController extends Controller
{
    //

    public function getFormations(Request $request)
    {

        $api = "agenda";
        $api .= "/$request->user_id";
        $api .= "/formations";

        $result =  $this->thirdPartyFuncPpapifl('GET', $api, [
            "Ct_num" => $request->user_id,
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function getEvents(Request $request)
    {

        $api = "agenda";
        $api .= "/$request->user_id";
        $api .= "/events";

        $result =  $this->thirdPartyFuncPpapifl('GET', $api, [
            "Ct_num" => $request->user_id,
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function eventCategories(Request $request)
    {

        $api = "events/categories";
        $result =  $this->thirdPartyFuncPpapifl('GET', $api, []);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function eventsCategoriesInfo(Request $request)
    {
        $params = $this->validate($request, [
            'category_id' => 'required',
            'country_iso' => 'required',
        ]);

        $api = "events/themes";
        $api .= "/$request->category_id";
        $api .= "/$request->country_iso";

        $result =  $this->thirdPartyFuncPpapifl('GET', $api, []);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function formationCategories()
    {

        $api = "formations/categories";
        $result =  $this->thirdPartyFuncPpapifl('GET', $api, []);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 400);
        }
    }


    public function formationsThemes(Request $request)
    {
        try {
            $api = "formations/themes";
            $result =  $this->thirdPartyFuncPpapifl('GET', $api, []);
            if (isset($result->success) && $result->success) {
                return response()->json($result, 200);
            } else {
                return response()->json($result, 400);
            }
        } catch (\Exception $e) {
            return   response()->json($e->getMessage(), 500);
        }
    }


    public function formationsCategoriesInfo(Request $request)
    {
        $params = $this->validate($request, [
            'category_id' => 'required',
            'country_iso' => 'required',
        ]);

        $api = "formations/categories";
        $api .= "/$request->category_id";
        $api .= "/$request->country_iso";

        $result =  $this->thirdPartyFuncPpapifl('GET', $api, []);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function getFormationsLabels(Request $request)
    {
        $api = "formations_initiales/labels";
        $result =  $this->thirdPartyFuncPpapifl('GET', $api, []);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }
    public function getFormationsLabelById(Request $request)
    {
        $params = $this->validate($request, [
            'formation_id' => 'required',
        ]);

        $api = "formations_initiales";
        $api .= "/$request->formation_id";

        $result =  $this->thirdPartyFuncPpapifl('GET', $api, []);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 400);
        }
    }

    public function getPodcastsThemes(Request $request)
    {
        $api = "podcasts/themes";
        $result =  $this->thirdPartyFuncPpapifl('GET', $api, []);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function getPodcastsThemeById(Request $request)
    {
        $this->validate($request, [
            'theme_id' => 'required',
        ]);
        $api = "podcasts/themes";
        $api .= "/$request->theme_id";
        $result =  $this->thirdPartyFuncPpapifl('GET', $api, []);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function formationsInscription(Request $request)
    {

        $this->validate($request, [
            'number_invitations' => 'required',
            'handicap_isWheelchair' => 'required',
            'handicap_isVisuallyImpaired' => 'required',
            'handicap_isMute' => 'required',
            'event_formation_id' => 'required',
        ]);
        $profile =  $this->thirdPartyFunc('POST', 'getProfil', [
            "flag" => $request->type, "num" => $request->user_id
        ]);
        $api = "formations/inscription";
        $result =  $this->thirdPartyFuncPpapifl('POST', $api, [
            "fbo_num " => $profile->profil->num,
            "fbo_firstname " => $profile->profil->firstName,
            "fbo_lastname " => $profile->profil->lastName,
            "fbo_email " => $profile->profil->email,
            "number_invitations " => $request->number_invitations,
            "handicap_isWheelchair " => $request->handicap_isWheelchair,
            "handicap_isVisuallyImpaired " => $request->handicap_isVisuallyImpaired,
            "handicap_isMute " => $request->handicap_isMute,
            "event_formation_id " => $request->event_formation_id,
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function eventsInscription(Request $request)
    {

        $this->validate($request, [
            'number_invitations' => 'required',
            'handicap_isWheelchair' => 'required',
            'handicap_isVisuallyImpaired' => 'required',
            'handicap_isMute' => 'required',
            'event_formation_id' => 'required',
        ]);
        $profile =  $this->thirdPartyFunc('POST', 'getProfil', [
            "flag" => $request->type, "num" => $request->user_id
        ]);
        $api = "events/inscription";
        $result =  $this->thirdPartyFuncPpapiflFormData('POST', $api, [
            "fbo_num" => $profile->profil->num,
            "fbo_firstname" => $profile->profil->firstName,
            "fbo_lastname" => $profile->profil->lastName,
            "fbo_email" => $profile->profil->email,
            "number_invitations" => $request->number_invitations,
            "handicap_isWheelchair" => $request->handicap_isWheelchair,
            "handicap_isVisuallyImpaired" => $request->handicap_isVisuallyImpaired,
            "handicap_isMute" => $request->handicap_isMute,
            "event_formation_id" => $request->event_formation_id,
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }


    public function formationsByCategoryAndCountryIso(Request $request)
    {

        $api = "formations/categories";
        $api .= "/$request->category_id";
        $api .= "/$request->country_iso";


        $result =  $this->thirdPartyFuncPpapifl('GET', $api, []);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }
    // changing to post 
    public function webinairesByCountry(Request $request)
    {

        try {
            $params = $this->validate($request, [

                'country_iso' => 'required',
            ]);

            $api = "formations/webinaires";
            $api .= "/$request->country_iso";


            $result =  $this->thirdPartyFuncPpapifl('GET', $api, []);
            if (isset($result->success) && $result->success) {
                return response()->json($result, 200);
            } else {
                return response()->json($result, 400);
            }
        } catch (\Exception $e) {
            return   response()->json($e->getMessage(), 500);
        }
    }


    public function productAssociation(Request $request)
    {

        $this->validate($request, [
            'ref' => 'required',
            'site' => 'required',
            'remise' => 'required',
        ]);

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $api = "produit-association";
        $result =  $this->thirdPartyFuncPpapifl('GET', $api, [
            "fbo_num" => $request->user_id,
            "country" => $country->products_code,
            "region" => $country->region_code,
            "ref" => $request->ref,
            "site" => $request->site,
            "remise" => $request->remise,
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function formationsTest()
    {

        return response()->json(["result" => "test"], 200);
    }

    public function webinairesOfTheMonthByCountry(Request $request)
    {

        $params = $this->validate($request, [

            'country_iso' => 'required',
        ]);

        $api = "formations/webinaires/month";
        $api .= "/$request->country_iso";


        $result =  $this->thirdPartyFuncPpapifl('GET', $api, []);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 400);
        }
    }

    public function webinairesById(Request $request)
    {
        $params = $this->validate($request, [

            'webinaire_id' => 'required',
        ]);

        $api = "formations/webinaires";
        $api .= "/$request->webinaire_id";

        $result =  $this->thirdPartyFuncPpapifl('GET', $api, []);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 400);
        }
    }
    public function webinairesCountries(Request $request)
    {

        $api = "formations/webinaires/countries";


        $result =  $this->thirdPartyFuncPpapifl('GET', $api, []);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 400);
        }
    }

    public function formationByID(Request $request)
    {
        $params = $this->validate($request, [

            'formation_id' => 'required',
        ]);

        $api = "formations_initiales";
        $api .= "/$request->formation_id";

        $result =  $this->thirdPartyFuncPpapifl('GET', $api, []);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 400);
        }
    }

    public function eventByID(Request $request)
    {
        $params = $this->validate($request, [

            'event_id' => 'required',
        ]);

        $api = "events/replay";
        $api .= "/$request->event_id";

        $result =  $this->thirdPartyFuncPpapifl('GET', $api, []);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 400);
        }
    }
}
