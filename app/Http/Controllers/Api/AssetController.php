<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\CustomerReview;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\Models\MainMenu;
use App\Models\Slider;
use App\Models\Favorite;
use App\Models\MobUser;
use App\Models\ProductCategory;
use App\Models\DeliveryMode;
use App\Models\Category;
use App\Models\Status;
use App\Models\Country;
use App\Models\GeneralConfiguration;
use App\Models\Video;
use App\Models\BusinessCategory;
use App\Models\BusinessFile;
use App\Models\NotificationRole;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use App\Mail\paymentSuccessNotification;
use App\Jobs\SendFCMNotification;
use App\Models\Notification;
use App\Models\Meeting;
use Illuminate\Support\Facades\Validator;

class AssetController extends Controller
{
    //
    public function getMainMenu(Request $request)
    {

        $mainMenu = MainMenu::query()
            ->where('is_parent', '1')
            ->where('is_active', '1')
            ->whereIn('accessed_by', [$request->type])
            ->orderBy('order')
            ->get();


        return response()->json(['main-menu' => $mainMenu], 200);
    }

    public function BusinessCategories(Request $request)
    {
        try {

            $categories = BusinessCategory::all();
            return response()->json(['status' => 'success', 'data' => $categories]);
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'msg' => $e->getMessage()]);
        }
    }

    public function BusinessFiles(Request $request, $category_id)
    {
        try {

            // $category = BusinessCategory::query()->where('_id',$category_id)->first();
            $files = BusinessFile::query()->whereIn('business_category_ids', [$category_id])->get();

            return response()->json(['status' => 'success', 'files' => $files]);
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'msg' => $e->getMessage()]);
        }
    }

    public function checkVersion(Request $request)
    {

        if ($request->header('version') == null) {
            return response()->json(['status' => 'error', 'message' => 'version_required']);
        }
        if ($request->header('platform') == null) {
            return response()->json(['status' => 'error', 'message' => 'platform_required']);
        }

        $version = $request->header('version');
        $platform = $request->header('platform');

        $versionAndroid = env('APPLICATION_VERSION_ANDROID', null);
        $versionIos = env('APPLICATION_VERSION_IOS', null);
        if ($platform == 'ios') {
            if ($version < $versionIos) {
                return response()->json(['update' => 1], 200);
            } else {
                return response()->json(['update' => 0], 200);
            }
        }
        if ($platform == 'android') {
            if ($version < $versionAndroid) {
                return response()->json(['update' => 1], 200);
            } else {
                return response()->json(['update' => 0], 200);
            }
        }
        return response()->json(['status' => 'platform unavailable'], 500);
    }

    public function checkURL(Request $request)
    {

        $APIFL = env('APIFL');
        $MOBILVTE = env('MOBILVTE');
        $REST_URL = env('REST_URL');
        $API_BANK_URL = env('API_BANK_URL');
        $DISTRIB = env('DISTRIB');
        $TOOLS = env('TOOLS');
        return response()->json(['APIFL' => $APIFL, 'DISTRIB' => $DISTRIB, 'TOOLS' => $TOOLS, 'MOBILVTE' => $MOBILVTE, 'REST_URL' => $REST_URL, 'API_BANK_URL' => $API_BANK_URL], 200);
    }

    public function getMainMenuById(Request $request, $mainMenuID)
    {

        if (MainMenu::query()->where('_id', $mainMenuID)->where('is_active', '1')->count() == 0) {
            return response()->json(['message' => 'No Menu found'], 500);
        }

        $mainMenu = MainMenu::query()->where('_id', $mainMenuID)->where('is_active', '1')->first();

        return response()->json(['main-menu' => $mainMenu], 200);
    }

    public function checkMaintainance(Request $request)
    {


        $conf = GeneralConfiguration::query()->where('key', 'maitainance')
            ->first();

        $value = false;
        if ($conf != null) {
            if ($conf->value == '1') {
                $value = true;
            }
        }


        return response()->json(['maintainance' => $value], 200);
    }

    public function getCountryDelivery(Request $request)
    {
        $this->validate($request, [
            'country_code' => 'required',
        ]);

        $country = Country::query()->where('country_code', $request->country_code)->first();
        if ($country != null) {
            $deliveryMode = DeliveryMode::query()->where('country_id', $country->id)
                ->where('user_type', strtoupper($request->type))->first();
            $ModeLivraision = array();
            if (isset($deliveryMode->relay_delivery_values1) and $deliveryMode->relay_delivery_values1 != null)
                array_push($ModeLivraision, ['name' => 'Livraison en relai', 'description' => strip_tags($deliveryMode->relay_delivery_values1)]);

            if (isset($deliveryMode->home_delivery_values1) and $deliveryMode->home_delivery_values1 != null)
                array_push($ModeLivraision, ['name' => 'Livraison à domicile', 'description' => strip_tags($deliveryMode->home_delivery_values1)]);

            if (isset($deliveryMode->withdrawal_points1) and $deliveryMode->withdrawal_points1 != null)
                array_push($ModeLivraision, ['name' => 'Point de retrait', 'description' => strip_tags($deliveryMode->withdrawal_points1)]);

            return response()->json(['ModeLivraision' => $ModeLivraision], 200);
        } else {
            return response()->json(['Message' => 'country not found'], 500);
        }
    }

    public function getSubMainMenu(Request $request, $id)
    {

        $mainMenu = MainMenu::query()
            ->where('parent_ids', $id)
            ->where('is_active', '1')
            ->get();

        return response()->json(['main-menu' => $mainMenu], 200);
    }

    public function getCountries(Request $request)
    {
        $status = Status::query()->where('slug', 'active')->first();
        $countries = Country::query()->where('status_id', $status->_id)->orderBy('order', 'asc')->get();
        return response()->json(['countries' => $countries], 200);
    }

    public function getMainMenuSlider(Request $request, $mainMenuID)
    {

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
            $country_code = $country->country_code;
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }


        $slider = Slider::query()->where('countries.country_code', $country_code)
            ->where('main_menu_id', $mainMenuID)
            ->orderBy('created_at', 'desc')
            ->get();

        return response()->json(['slider' => $slider], 200);
    }

    public function getProductCategories(Request $request)
    {


        $productCategories = ProductCategory::query()->where('is_active', '1')->get();

        return response()->json(['ProductCategories' => $productCategories], 200);
    }

    public function getCategoriesList(Request $request)
    {
        $ts = time();
        $token = hash('sha512', 'tng-2019' . $ts . env('TOKEN_KEY'));
        $ur = env('DISTRIB');
        $url = $ur . 'products/categories/list.api?ts=' . $ts . '&token=' . $token;
        // $url = 'https://ppdistrib.foreverliving.fr/products/categories/list.api?ts=' . $ts . '&token=' . $token;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result);
        if ($result->success) {
            try {
                return response()->json($result, 200);
            } catch (\Exception $exception) {
                return [];
            }
        } else {
            return [];
        }
    }

    public function getProductByCategories(Request $request)
    {
        $this->validate($request, [
            'category_id' => 'required',
        ]);

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $productCategory = ProductCategory::query()->where('_id', $request->input('category_id'))
            ->where('is_active', '1')
            ->first();
        if ($productCategory == null) {
            return response()->json('wrong category_id!', 500);
        }
        //api prices from stephan
        $result = $this->thirdPartyFunc('POST', 'getListeArticles', [
            'env' => $request->headers->has('country-code') ? $country->products_code : "",
            "region" => $request->headers->has('country-code') ? $country->region_code : "",
            "typLiv" => "P",
            "flag" => $request->type,
            "rate" => $request->has('rate') ? $request->input('rate') : '0.0',
            "num" => $request->user_id,
        ]);

        if (isset($result->success) && !$result->success) {
            return response()->json([
                $result,
                'env' => $request->headers->has('country-code') ? $country->products_code : "",
                "region" => $request->headers->has('country-code') ? $country->region_code : "",
                "typLiv" => "P",
                "flag" => $request->type,
                "rate" => $request->has('rate') ? $request->input('rate') : '0.0',
                "num" => $request->user_id,
            ], 500);
        }

        $favorite = Favorite::query()->where('num', $request->user_id)->where('env', $country->products_code)->first();
        if (isset($result->success) && $result->success) {
            if ($favorite != null)
                $products = $this->getProducts($result->listeArticles, explode(',', $favorite->ref), $productCategory->reference_id);
            else {
                $products = $this->getProductsNoRef($result->listeArticles, $productCategory->reference_id);
            }
            return response()->json(['rate' => $result->rate, 'products' => $products], 200);
        } else {
            return response()->json($result, 500);
        }
    }


    public function getAllProducts(Request $request)
    {
        // Log::info(Carbon::now()->timestamp);

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }
        //api prices from stephan
        $result = $this->thirdPartyFunc('POST', 'getListeArticles', [
            'env' => $request->headers->has('country-code') ? $country->products_code : "",
            "region" => $request->headers->has('country-code') ? $country->region_code : "",
            "typLiv" => $request->headers->has('country-code') ? $country->product_type : "",
            "flag" => $request->type,
            "rate" => $request->has('rate') ? $request->input('rate') : '0.0',
            "num" => $request->user_id,
        ]);

        // Log::info(Carbon::now()->timestamp);

        $favorite = Favorite::query()->where('num', $request->user_id)->where('env', $country->products_code)->first();
        if (isset($result->success) && $result->success) {
            if ($favorite != null)
                $products = $this->getProducts($result->listeArticles, explode(',', $favorite->ref));
            else {
                $products = $this->getProductsNoRef($result->listeArticles);
            }
            return response()->json(['rate' => $result->rate, 'products' => $products], 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function verifStockRef(Request $request)
    {
        $this->validate($request, [
            'qte' => 'required',
            'numOrder' => 'required',
            'ref' => 'required',
        ]);
        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }
        $result = $this->thirdPartyFunc('POST', 'verifStockRef', [
            "env" => $country->products_code,
            "flag" => $request->type,
            "qte" => $request->qte,
            "numOrder" => $request->numOrder,
            "ref" => $request->ref,
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function createCommand(Request $request)
    {
        $this->validate($request, [
            'rate' => 'required',
            'country' => 'required',
            'typLiv' => 'required',
        ]);
        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }
        if (strtoupper($request->type) == 'FBO') {
            $result = $this->thirdPartyFunc('POST', 'createCmde', [
                "num" => $request->user_id,
                "flag" => strtoupper($request->type),
                "env" => $country->products_code,
                "region" => $country->region_code,
                "typLiv" => $request->typLiv,
                "rate" => $request->rate,
                "country" => $request->country,
                "origine" => 'flpmobil',
            ]);
        } else {
            $result = $this->thirdPartyFunc('POST', 'createCmde', [
                "flag" => strtoupper($request->type),
                "env" => $country->products_code,
                "num" => $request->user_id,
                "region" => $country->region_code,
                "typLiv" => $request->typLiv,
                "rate" => $request->rate,
                "country" => $request->country,
                "origine" => 'FLPMOBIL_CLIENT',
            ]);
        }
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function addCommand(Request $request)
    {
        $this->validate($request, [
            'qte' => 'required',
            'numOrder' => 'required',
            'ref' => 'required',
            //            'perso'=>'required',
        ]);
        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }
        if ($request->perso != null) {
            $result = $this->thirdPartyFunc('POST', 'ajouteLigneCmde', [
                "flag" => strtoupper($request->type),
                "num" => $request->user_id,
                "env" => $country->products_code,
                "numOrder" => $request->numOrder,
                "qte" => $request->qte,
                "ref" => $request->ref,
                "perso" => $request->perso,
            ]);
        } else {
            $result = $this->thirdPartyFunc('POST', 'ajouteLigneCmde', [
                "flag" => strtoupper($request->type),
                "num" => $request->user_id,
                "env" => $country->products_code,
                "numOrder" => $request->numOrder,
                "qte" => $request->qte,
                "ref" => $request->ref,
                "perso" => "",
            ]);
        }

        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function deleteCommand(Request $request)
    {
        $this->validate($request, [
            'numOrder' => 'required',
        ]);
        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }
        $result = $this->thirdPartyFunc('DELETE', 'deleteCmde', [
            "flag" => $request->type,
            "env" => $country->products_code,
            "numOrder" => $request->numOrder,
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function getCart(Request $request)
    {
        $this->validate($request, [
            'numOrder' => 'required',
        ]);
        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }
        $result = $this->thirdPartyFunc('POST', 'getPanier', [
            "flag" => strtoupper($request->type),
            "env" => $country->products_code,
            "numOrder" => $request->numOrder,
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function requestPayment(Request $request, $id)
    {

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }
        $result = $this->thirdPartyFunc('POST', 'valideCmde', [
            "flag" => strtoupper($request->type),
            "env" => $country->products_code,
            "numOrder" => $id,
        ]);

        $user = $this->thirdPartyFunc('POST', 'getProfil', [
            "flag" => $request->type,
            "num" => $request->user_id
        ]);

        $bank_url = env('API_BANK_URL');

        $vads_site_id = '55754771';

        if (strtoupper($request->type) == 'CLI') {
            $vads_site_id = '16173012';
            $bank_url = env('API_CLIENT_BANK_URL');
        }

        if ($result->success and $user->success) {

            $params = [
                'vads_action_mode' => 'INTERACTIVE',
                'vads_amount' => $result->amount,
                'vads_available_languages' => 'fr;en;de;es;it;pt;nl;sv',
                'vads_ctx_mode' => env('CTX_MODE'),
                'vads_currency' => '978',
                'vads_cust_id' => $request->user_id,
                'vads_order_id' => $result->numOrder,
                'vads_order_info' => 'CMD',
                'vads_order_info2' => strtoupper($request->type),
                'vads_order_info3' => $country->products_code,
                'vads_page_action' => 'PAYMENT',
                'vads_payment_config' => 'SINGLE',
                'vads_return_mode' => 'GET',
                'vads_site_id' => $vads_site_id,
                'vads_trans_date' => Carbon::now()->format('YmdHis'),
                'vads_trans_id' => $result->transactionId,
                'vads_url_cancel' => route('payment.status', ['type' => 'cancel']),
                'vads_url_error' => route('payment.status', ['type' => 'error']),
                'vads_url_refused' => route('payment.status', ['type' => 'refused']),
                'vads_url_success' => route('payment.status', ['type' => 'success']),
                'vads_shop_url' => $bank_url,
                // 'vads_shop_url' => 'https://distrib.foreverliving.fr?origin=pprest.foreverliving.fr',
                'vads_validation_mode' => '0',
                'vads_version' => 'V2',
                'vads_firstName' => $user->profil->firstName,
                'vads_lastName' => $user->profil->lastName,
                'vads_email' => $user->profil->email,

            ];
            $signature = $this->getSignature($params, strtoupper($request->type) == 'CLI');
            $params['signature'] = $signature;
            $token = Crypt::encryptString(json_encode($params));
            return response()->json(['url' => route('payment.url', ['token' => $token])], 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function paymentUrl($token)
    {
        $decrypted = Crypt::decryptString($token);
        $params = json_decode($decrypted);
        return view('cms.payment.payment-form', compact('params'));
    }

    function getSignature($params, $isClient)
    {
        /**
         * Function that computes the signature.
         * $params : table containing the fields to send in the payment form.
         * $key : TEST or PRODUCTION key
         */
        //Initialization of the variable that will contain the string to encrypt
        $contenu_signature = "";
        // Sorting fields alphabetically
        ksort($params);
        foreach ($params as $name => $value) {
            // Recovery of vads_ fields
            if (substr($name, 0, 5) == 'vads_') {
                // Concatenation with "+"
                $contenu_signature .= $value . "+";
            }
        }
        // Adding the key at the end
        // $contenu_signature .= '2138633189841587';
        if ($isClient == true) {
            $contenu_signature .= env('CLIENT_MERCHANT_KEY');
        } else {
            $contenu_signature .= env('MERCHANT_KEY');
        }


        // Applying SHA-1 algorithm
        $signature = sha1($contenu_signature);
        return $signature;
    }

    public function paymentStatus($type)
    {

        if ($type == 'success')
            return response(['status' => 'success']);
        if ($type == 'cancel')
            return response(['status' => 'cancel']);
        if ($type == 'refused')
            return response(['status' => 'refused']);

        return response(['status' => 'error']);
    }

    public function paymentServer(Request $request)
    {
        Log::info($request->all());
        $payment = $request->all();
        $sign = $this->getSignature($request->all(), $payment['vads_order_info2'] == 'CLI');


        if ($request->signature != $sign || $request->vads_result != "00") {
            return response()->json(['status' => 'failed']);
        }
        $result = $this->thirdPartyFunc('POST', 'finCmde', [
            "flag" => $payment['vads_order_info2'],
            "env" => $payment['vads_order_info3'],
            'numOrder' => $payment['vads_order_id'],
            'typePaiement' => 'CB',
            'infoPaiement' => [
                'responseCode' => $payment['vads_result'],
                'transactionId' => $payment['vads_trans_id'] . "",
                'authorisationId' => $payment['vads_auth_number'],
                'paymentCertificate' => $payment['vads_payment_certificate'],
                'paymentDate' => substr($payment['vads_trans_date'], 0, 8),
                'paymentTime' => substr($payment['vads_trans_date'], 8, 4),
                'amount' => $payment['vads_amount'],
                'cardNumber' => $payment['vads_card_number']
            ]
        ]);
        Log::info(json_encode($result));
        if ($result->success) {
            Mail::to($payment['vads_email'])->send(new paymentSuccessNotification([
                'first_name' => $payment['vads_firstName'],
                'last_name' => $payment['vads_lastName'],
                'mail' => $payment['vads_email'],
                'num_order' => $payment['vads_order_id'],
            ]));
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }


    public function getPromo(Request $request)
    {
        $this->validate($request, [
            'numOrder' => 'required',
        ]);
        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }
        $result = $this->thirdPartyFunc('POST', 'getPromo', [
            "flag" => strtoupper($request->type),
            "env" => $country->products_code,
            "numOrder" => $request->numOrder,
            "isForClient" => $request->has('isForClient') ?
                $request->input('isForClient') : '0',
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function ctrlCart(Request $request)
    {
        $this->validate($request, [
            'numOrder' => 'required',
        ]);
        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }
        $result = $this->thirdPartyFunc('POST', 'ctrlPanier', [
            "flag" => strtoupper($request->type),
            "env" => $country->products_code,
            "numOrder" => $request->numOrder,
        ]);

        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function validCommand(Request $request)
    {
        $this->validate($request, [
            'numOrder' => 'required',
        ]);
        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }
        $result = $this->thirdPartyFunc('POST', 'valideCmde', [
            "flag" => strtoupper($request->type),
            "env" => $country->products_code,
            "numOrder" => $request->numOrder,
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function getAllAddressLiv(Request $request)
    {

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }
        $result = $this->thirdPartyFunc('POST', 'getAllAddressLiv', [
            "flag" => strtoupper($request->type),
            "env" => $country->products_code,
            "num" => $request->user_id,
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function createAddressLiv(Request $request)
    {
        $this->validate($request, [
            'nomAdr' => 'required',
        ]);

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }
        $result = $this->thirdPartyFunc('POST', 'addAddressLiv', [
            "flag" => strtoupper($request->type),
            "env" => $country->products_code,
            "num" => $request->user_id,
            "nomAdr" => $request->nomAdr,
            "contact" => $request->contact,
            "email" => $request->email,
            "address" => $request->address,
            "additionalAddress" => $request->additionalAddress,
            "zipCode" => $request->zipCode,
            "city" => $request->city,
            "country" => $request->country,
            "phoneNumber" => $request->phoneNumber,

        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function updateAddressLiv(Request $request)
    {
        $this->validate($request, [
            'idAdr' => 'required',
        ]);

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }
        $result = $this->thirdPartyFunc('PUT', 'setAddressLiv', [
            "flag" => strtoupper($request->type),
            "env" => $country->products_code,
            "num" => $request->user_id,
            "idAdr" => $request->idAdr,
            "nomAdr" => $request->nomAdr,
            "contact" => $request->contact,
            "email" => $request->email,
            "address" => $request->address,
            "additionalAddress" => $request->additionalAddress,
            "zipCode" => $request->zipCode,
            "city" => $request->city,
            "country" => $request->country,
            "phoneNumber" => $request->phoneNumber,

        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function getAllCmde(Request $request)
    {

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }
        $result = $this->thirdPartyFunc('POST', 'getAllCmde', [
            "flag" => strtoupper($request->type),
            "env" => $country->products_code,
            "num" => $request->user_id,

        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function getCmde(Request $request)
    {
        $this->validate($request, [
            'env' => 'required',
            'numOrder' => 'required',
        ]);
        // if (!$request->headers->has('country-code')) {
        //     $country = Country::query()->where('slug.fr', 'metropole')->first();

        // } else {
        //     $country_code = $request->header('country-code');
        //     $country = Country::query()->where('country_code', $country_code)->first();
        // }

        $result = $this->thirdPartyFunc('POST', 'getCmde', [
            "flag" => strtoupper($request->type),
            "env" => $request->input('env'),
            "num" => $request->user_id,
            "numOrder" => (int)$request->input('numOrder')
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function getAllCmdeClient(Request $request)
    {
        $this->validate($request, [
            'canal' => 'required',
        ]);
        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }
        $result = $this->thirdPartyFunc('POST', 'getAllCmdeClient', [
            "flag" => strtoupper($request->type),
            "env" => $country->products_code,
            "num" => $request->user_id,
            "canal" => $request->input('canal')

        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function getVenteCmdeClient(Request $request)
    {
        $this->validate($request, [
            'numClient' => 'required',
        ]);
        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }
        $result = $this->thirdPartyFuncMobil('GET', 'getVenteCmdeClient', [
            "flag" => strtoupper($request->type),
            "env" => $country->products_code,
            "numFBO" => $request->user_id,
            "numClient" => $request->numClient,
            "year" => $request->has('year') ? $request->input('year') : 'anné en cours',
            "month" => $request->has('month') ? $request->input('month') : 'mois en cours',

        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function deleteAddressLiv(Request $request)
    {
        $this->validate($request, [
            'idAdr' => 'required',
        ]);

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }
        $result = $this->thirdPartyFunc('DELETE', 'deleteAddressLiv ', [
            "flag" => strtoupper($request->type),
            "env" => $country->products_code,
            "num" => $request->user_id,
            "idAdr" => $request->idAdr,

        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function getVedios(Request $request)
    {

        $this->validate($request, [
            'main_menu_id' => 'required',
        ]);

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
            $country_code = $country->country_code;
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $vedios = Video::query()
            ->where('countries.country_code', $country_code)
            ->where('main_menu_ids', $request->main_menu_id)
            ->get();

        return response()->json(['vedios' => $vedios], 200);
    }

    public function getVedio(Request $request, $id)
    {

        $vedio = Video::query()
            ->where('_id', $id)
            ->first();
        return response()->json(['vedio' => $vedio], 200);
    }

    public function getVediosByCategory(Request $request)
    {

        $this->validate($request, [
            'main_menu_id' => 'required',
            'category_id' => 'required',
        ]);

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
            $country_code = $country->country_code;
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $vedios = Video::query()->where('countries.country_code', $country_code)
            ->where('main_menu_ids', $request->main_menu_id)
            ->where('category_ids', $request->category_id)
            ->get();

        return response()->json(['vedios' => $vedios], 200);
    }

    public function getVedioCategories(Request $request)
    {

        $categories = Category::query()->where('module_ids', 'video')->get();

        return response()->json(['categories' => $categories], 200);
    }


    public function chronopost(Request $request)
    {

        $secretKey = 'R=&878hbm4F*sSa=Cmb5UH@k@A*455';
        $apiUser = 'FLPAPINELCOM';
        $idKey = 'flp_xXBuBHnLTV0i';

        $date = gmdate('Y-m-dH');

        $api_key = $idKey . '.' . sha1($apiUser . '-' . $date . '-' . $secretKey);

        $key = "5352876b-9490-42a2-aade-d36c9b6d92ca";
        $bckUrl = env('APP_URL') . "/api/back-check-point?";
        // $url = "https://ppapipointrelais.foreverliving.fr/api/pointRelaisChronopost?api_key=".$api_key;
        $url = "https://posfra.flpbis.com/chronopost/index.php?bckUrl=$bckUrl&key=$key";

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }


        $url .= "&env=$country->products_code";

        if ($request->paysLiv != null) {
            $paysLiv = urlencode($request->paysLiv);
            $url .= "&paysLiv=$paysLiv";
            // $url .= "&country=$paysLiv";
        }
        if ($request->adresse != null) {
            $adresse = urlencode($request->adresse);
            $url .= "&adresse=$adresse";
        }
        if ($request->cp != null) {
            $cp = urlencode($request->cp);
            $url .= "&cp=$cp";
            // $url .= "&zipcode=$cp";
        }
        if ($request->ville != null) {
            $ville = urlencode($request->ville);
            $url .= "&ville=$ville";
            // $url .= "&city=$ville";
        }
        return response()->json(['url' => $url], 200);
    }

    public function checkPoint(Request $request)
    {

        // return response()->json($request->all(), 200);
        return response()->json([
            'chrono' => $request->parcelShopId,
            'chrono_ad1liv' => $request->ad1liv,
            'chrono_ad2liv' => $request->ad2liv,
            'chrono_info' => $request->infosLiv,
            'idTnt' => $request->idTnt,
            'chrono_zip' => $request->cpliv,
            'chrono_city' => $request->villiv,
            'country' => $request->pays
        ], 200);
    }

    public function uploadProfileImage(Request $request)
    {
        $this->validate($request, [
            'fbo_avatar' => 'required',
        ]);

        $data = [
            "ct_num" => $request->user_id,
            "fbo_avatar" => $request->input('fbo_avatar'),
        ];

        $result = $this->thirdPartyFuncPpapiflFormData(
            'POST',
            'profiles/' . $request->user_id . '/picture',
            $data
        );
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function getProfileImage(Request $request)
    {

        $result = $this->thirdPartyFuncPpapifl('GET', 'profiles/' . $request->user_id . '/picture', [
            "ct_num" => $request->user_id,
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function setDelivery(Request $request)
    {

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $result = $this->thirdPartyFunc('PUT', 'setLivraison', [

            "flag" => $request->type,
            'env' => $country->products_code,
            "typLiv" => $request->input('typLiv'),
            "numOrder" => $request->input('numOrder'),
            "numPtRelais" => $request->input('numPtRelais'),
            "typePtRelais" => $request->input('typePtRelais'),
            "nameLiv" => $request->input('nameLiv'),
            "addressLiv" => $request->input('addressLiv'),
            "additionalAddressLiv" => $request->input('additionalAddressLiv'),
            "zipCodeLiv" => $request->input('zipCodeLiv'),
            "infosLiv" => $request->input('infosLiv'),
            "phoneLiv" => $request->input('phoneLiv'),
            "emailLiv" => $request->input('emailLiv'),
            "cityLiv" => $request->input('cityLiv'),
            "countryLiv" => $request->input('countryLiv'),
            "gareLiv" => $request->input('gareLiv'),
            "modeLiv" => $request->input('modeLiv'),
            "codeCity" => $request->input('codeCity'),
            "prestataire" => $request->input('prestataire'),


        ]);


        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function getCustomerCheckout(Request $request, $id)
    {


        $customerReview = CustomerReview::find($id);

        $result = [
            "success" => true,
            "numErreur" => 0,
            "msgErreur" => "",
            "panierHead" => [
                $customerReview->panierHead
            ],
        ];

        if ($customerReview == null) {
            $customerReview = new CustomerReview();
        }
        $customerReview->client_id = $numClient;
        $customerReview->fbo_id = $numFBO;
        $customerReview->fbo_id = $numFBO;
        $customerReview->infosFBO = $result1->infosFBO;
        $customerReview->infosClient = $result1->listCommande[0]->infosClient;
        $customerReview->panierHead = $result1->listCommande[0]->panierHead;
        $customerReview->panierDetail = $result1->listCommande[0]->panierDetail;
        $customerReview->panierDetail = $result1->listCommande[0]->panierDetail;
        $customerReview->shipping_address = [

            "flag" => $request->type,
            'env' => $country->products_code,
            "typLiv" => $request->input('typLiv'),
            "numOrder" => $request->input('numOrder'),
            "numPtRelais" => $request->input('numPtRelais'),
            "typePtRelais" => $request->input('typePtRelais'),
            "nameLiv" => $request->input('nameLiv'),
            "addressLiv" => $request->input('addressLiv'),
            "additionalAddressLiv" => $request->input('additionalAddressLiv'),
            "zipCodeLiv" => $request->input('zipCodeLiv'),
            "infosLiv" => $request->input('infosLiv'),
            "phoneLiv" => $request->input('phoneLiv'),
            "emailLiv" => $request->input('emailLiv'),
            "cityLiv" => $request->input('cityLiv'),
            "countryLiv" => $request->input('countryLiv'),
            "gareLiv" => $request->input('gareLiv'),
            "modeLiv" => $request->input('modeLiv'),
            "codeCity" => $request->input('codeCity'),
            "prestataire" => $request->input('prestataire'),


        ];
        $customerReview->save();
        return response()->json(["success" => true, 'object' => $customerReview], 200);
    }

    public function getNbClient(Request $request)
    {
        $this->validate($request, [
            'year' => 'required',
        ]);

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $result = $this->thirdPartyFuncMobil('GET', 'getNbClient', [
            "env" => $country->products_code,
            "numFBO" => $request->user_id,
            "month" => $request->month,
            "year" => $request->year,
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function getNbNewClient(Request $request)
    {
        $this->validate($request, [
            'year' => 'required',
        ]);

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $result = $this->thirdPartyFuncMobil('GET', 'getNbNewClient', [
            "env" => $country->products_code,
            "numFBO" => $request->user_id,
            "month" => $request->month,
            "year" => $request->year,
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function getMontantMoyVente(Request $request)
    {
        $this->validate($request, [
            'year' => 'required',
        ]);

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $result = $this->thirdPartyFuncMobil('GET', 'getMontantMoyVente', [
            "env" => $country->products_code,
            "numFBO" => $request->user_id,
            "month" => $request->month,
            "year" => $request->year,
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function getMontantMoyBoutique(Request $request)
    {
        $this->validate($request, [
            'year' => 'required',
        ]);

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $result = $this->thirdPartyFuncMobil('GET', 'getMontantMoyBoutique', [
            "env" => $country->products_code,
            "numFBO" => $request->user_id,
            "month" => $request->month,
            "year" => $request->year,
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function getNbClientCmd(Request $request)
    {
        $this->validate($request, [
            'year' => 'required',
        ]);

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $result = $this->thirdPartyFuncMobil('GET', 'getNbClientCmd', [
            "env" => $country->products_code,
            "numFBO" => $request->user_id,
            "month" => $request->month,
            "year" => $request->year,
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function getTopProduitVente(Request $request)
    {
        $this->validate($request, [
            'year' => 'required',
        ]);

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $result = $this->thirdPartyFuncMobil('GET', 'getTopProduitVente', [
            "env" => $country->products_code,
            "numFBO" => $request->user_id,
            "month" => $request->month,
            "year" => $request->year,
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function getCC(Request $request)
    {
        $this->validate($request, [
            'year' => 'required',
        ]);

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $result = $this->thirdPartyFuncMobil('GET', 'getCC', [
            "env" => $country->products_code,
            "numFBO" => $request->user_id,
            "month" => $request->month,
            "year" => $request->year,
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function getCCBoutique(Request $request)
    {
        $this->validate($request, [
            'year' => 'required',
        ]);

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $result = $this->thirdPartyFuncMobil('GET', 'getCCBoutique', [
            "env" => $country->products_code,
            "numFBO" => $request->user_id,
            "month" => $request->month,
            "year" => $request->year,
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function addFrais(Request $request)
    {
        $this->validate($request, [
            'idTypeFrais' => 'required',
            'libelle' => 'required',
            'mtFrais' => 'required',
            'dateFrais' => 'required',
        ]);

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $result = $this->thirdPartyFuncMobil('POST', 'addFrais', [
            "env" => $country->products_code,
            "numFBO" => $request->user_id,
            "idTypeFrais" => $request->idTypeFrais,
            "libelle" => $request->libelle,
            "mtFrais" => $request->mtFrais,
            "dateFrais" => $request->dateFrais,
            "description" => $request->description,
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function setFrais(Request $request)
    {
        $this->validate($request, [
            'idFrais' => 'required',
            'idTypeFrais' => 'required',
            'libelle' => 'required',
            'mtFrais' => 'required',
            'dateFrais' => 'required',
        ]);

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $result = $this->thirdPartyFuncMobil('PUT', 'setFrais', [
            "env" => $country->products_code,
            "numFBO" => $request->user_id,
            "idFrais" => $request->idFrais,
            "idTypeFrais" => $request->idTypeFrais,
            "libelle" => $request->libelle,
            "mtFrais" => $request->mtFrais,
            "dateFrais" => $request->dateFrais,
            "description" => $request->description,
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function getFrais(Request $request)
    {
        $this->validate($request, [
            'year' => 'required',
        ]);

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $result = $this->thirdPartyFuncMobil('GET', 'getFrais', [
            "env" => $country->products_code,
            "numFBO" => $request->user_id,
            "month" => $request->month,
            "year" => $request->year,
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function deleteFrais(Request $request)
    {
        $this->validate($request, [
            'idFrais' => 'required',
        ]);

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $result = $this->thirdPartyFuncMobil('DELETE', 'deleteFrais', [
            "env" => $country->products_code,
            "numFBO" => $request->user_id,
            "idFrais" => $request->idFrais,
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function getListFrais(Request $request)
    {

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $result = $this->thirdPartyFuncMobil('GET', 'getListFrais', [
            "env" => $country->products_code,
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function getBalance(Request $request)
    {
        $this->validate($request, [
            'year' => 'required',
        ]);

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $result = $this->thirdPartyFuncMobil('GET', 'getBalance', [
            "env" => $country->products_code,
            "numFBO" => $request->user_id,
            "month" => $request->month,
            "year" => $request->year,
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function getCmdDetail(Request $request)
    {
        $this->validate($request, [
            'idVente' => 'required',
        ]);

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $result = $this->thirdPartyFuncMobil('GET', 'getCmdDetail', [
            "env" => $country->products_code,
            "numFBO" => $request->user_id,
            "idVente" => $request->idVente
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function getBalanceDetail(Request $request)
    {
        $this->validate($request, [
            'year' => 'required',
        ]);

        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $result = $this->thirdPartyFuncMobil('GET', 'getBalanceDetail', [
            "env" => $country->products_code,
            "numFBO" => $request->user_id,
            "month" => $request->month,
            "year" => $request->year,
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function searchProducts(Request $request)
    {
        $this->validate($request, [
            'search' => 'required',
            'rate' => 'required',
        ]);
        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }
        $result = $this->thirdPartyFunc('POST', 'searchProducts', [
            "flag" => $request->type,
            "env" => $country->products_code,
            "region" => $country->region_code,
            "search" => $request->search,
            "num" => $request->user_id,
            "rate" => $request->rate
        ]);
        $favorite = Favorite::query()->where('num', $request->user_id)->where('env', $country->products_code)->first();
        if (isset($result->success) && $result->success) {
            if ($favorite != null)
                $products = $this->getProducts($result->listeArticles, explode(',', $favorite->ref));
            else {
                $products = $this->getProductsNoRef($result->listeArticles);
            }
            return response()->json(['products' => $products], 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function searchContacts(Request $request)
    {
        $this->validate($request, [
            'search' => 'required',
        ]);
        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $result = $this->thirdPartyFuncMobil('GET', 'searchContact', [
            "env" => $country->products_code,
            "numFBO" => $request->user_id,
            "search" => $request->search
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function getArticlesByRef(Request $request)
    {
        $this->validate($request, [
            'search' => 'required',
            'rate' => 'required',
        ]);
        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $result = $this->thirdPartyFunc('POST', 'getArticlesByRef', [
            "flag" => $request->type,
            "env" => $country->products_code,
            "region" => $country->region_code,
            "search" => $request->search,
            "num" => $request->user_id,
            "rate" => $request->rate
        ]);
        $favorite = Favorite::query()->where('num', $request->user_id)->where('env', $country->products_code)->first();

        if (isset($result->success) && $result->success) {
            if ($favorite != null) {
                $products = $this->getProducts($result->listeArticles, explode(',', $favorite->ref));
            } else {
                $products = $this->getProductsNoRef($result->listeArticles);
            }
            return response()->json(['products' => $products], 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function searchAddress(Request $request)
    {
        $this->validate($request, [
            'search' => 'required',
        ]);
        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $result = $this->thirdPartyFunc('POST', 'searchAddress', [
            "flag" => $request->type,
            "env" => $country->products_code,
            "search" => $request->search,
            "num" => $request->user_id,
        ]);
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function searchGeneral(Request $request)
    {
        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $favorite = Favorite::query()->where('num', $request->user_id)
            ->where('env', $country->products_code)->first();

        $result = $this->thirdPartyFuncMobil('GET', 'searchGeneral', [
            "flag" => $request->type,
            "env" => $country->products_code,
            "numFBO" => $request->has('numFBO') ? $request->input('numFBO') : '',
            "search" => $request->has('search') ? $request->input('search') : '',
            "ref" => $request->has('ref') ? $request->input('ref') : '',
        ]);
        if (isset($result->success) && $result->success) {

            if ($favorite != null) {
                $products = $this->getProducts($result->listArticles, explode(',', $favorite->ref));
            } else {
                $products = $this->getProductsNoRef($result->listArticles);
            }
            return response()->json(['products' => $products], 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function addFavorite(Request $request)
    {
        $this->validate($request, [
            'ref' => 'required',
        ]);
        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }
        $favorite = Favorite::query()->where('num', $request->user_id)->where('env', $country->products_code)->first();
        if ($favorite == null) {
            $favorite = new Favorite();
            $favorite->num = $request->user_id;
            $favorite->ref = $request->ref;
            $favorite->env = $country->products_code;
            $favorite->region = $country->region_code;
            $favorite->save();
            return response()->json($favorite, 200);
        } else {
            if (isset($favorite->ref) and $favorite->ref != "") {
                $arrayRef = explode(',', $favorite->ref);
                if (in_array($request->ref, $arrayRef)) {
                    return response()->json("ref exists", 500);
                }
                array_push($arrayRef, $request->ref);
                $favorite->ref = implode(",", $arrayRef);
            } else {
                $favorite->ref = $request->ref;
            }
            $favorite->env = $country->products_code;
            $favorite->region = $country->region_code;
            $favorite->save();
            return response()->json($favorite, 200);
        }
    }

    public function deleteFavorite(Request $request)
    {
        $this->validate($request, [
            'ref' => 'required',
        ]);
        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }
        $favorite = Favorite::query()->where('num', $request->user_id)->where('env', $country->products_code)->first();
        if ($favorite == null) {
            return response()->json("not found", 500);
        } else {
            $arrayRef = explode(',', $favorite->ref);
            if (in_array($request->ref, $arrayRef)) {
                $newRef = array_diff($arrayRef, [$request->ref]);
                $favorite->ref = implode(",", $newRef);
                $favorite->save();
                return response()->json($favorite, 200);
            } else {
                return response()->json("ref not found", 500);
            }
        }
    }

    public function getFavorite(Request $request)
    {
        $this->validate($request, [
            'rate' => 'required',
        ]);
        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }
        $favorite = Favorite::query()->where('num', $request->user_id)
            ->where('env', $country->products_code)->first();

        $result = $this->thirdPartyFunc('POST', 'getArticlesByRef', [
            "flag" => $request->type,
            "env" => $country->products_code,
            "region" => $country->region_code,
            "search" => $favorite->ref,
            "num" => $request->user_id,
            "rate" => $request->rate
        ]);
        if (isset($result->success) && $result->success) {
            if ($favorite != null)
                $products = $this->getProducts($result->listeArticles, explode(',', $favorite->ref));
            else {
                $products = $this->getProductsNoRef($result->listeArticles);
            }
            return response()->json(['products' => $products], 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function getProducts($listeArticles, $fRefs, $id = null)
    {

        $productPrices = $listeArticles;
        $refs = [];
        foreach ($productPrices as $price) {
            $refs[$price->ref] = $price;
        }

        if ($id != null) {
            $idsArray = explode(',', $id);
        } else {
            $idsArray = ["id" => null];
        }

        $display = [];

        foreach ($idsArray as $cId) {
            //api for product details from carel
            $ts = time();
            $token = hash('sha512', 'tng-2019' . $ts . env('TOKEN_KEY'));

            $ur = env('DISTRIB');
            $url = $ur . 'products/list.api?ts=' .
                $ts . '&token=' . $token;
            if ($cId != null)
                $url .= "&category_id=$cId";

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_POST, 0);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $result = curl_exec($ch);
            curl_close($ch);
            $result = json_decode($result);
            try {
                $productDetails = $result->data;
            } catch (\Exception $exception) {
                $productDetails = [];
            }

            //mixing carel api and stephan api
            foreach ($productDetails as $details) {
                $i = $details->ref_produit;
                if (in_array($i, $fRefs)) {
                    $details->is_favorite = true;
                } else {
                    $details->is_favorite = false;
                }
                if (array_key_exists($i, $refs)) {
                    $display[$i] = ['price' => $refs[$i], 'detail' => $details];
                }
            }
        }
        return array_values($display);
    }

    public function getProductsNoRef($listeArticles, $id = null)
    {

        $productPrices = $listeArticles;
        $refs = [];
        foreach ($productPrices as $price) {
            $refs[$price->ref] = $price;
        }

        if ($id != null) {
            $idsArray = explode(',', $id);
        } else {
            $idsArray = ["id" => null];
        }

        $display = [];

        foreach ($idsArray as $cId) {
            //api for product details from carel
            $ts = time();
            $token = hash('sha512', 'tng-2019' . $ts . env('TOKEN_KEY'));
            $ur = env('DISTRIB');
            $url = $ur . 'products/list.api?ts=' .
                $ts . '&token=' . $token;
            if ($cId != null)
                $url .= "&category_id=$cId";

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_POST, 0);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $result = curl_exec($ch);
            curl_close($ch);

            $result = json_decode($result);
            try {
                $productDetails = $result->data;
            } catch (\Exception $exception) {
                $productDetails = [];
            }

            //mixing carel api and stephan api
            foreach ($productDetails as $details) {
                $i = $details->ref_produit;
                if (array_key_exists($i, $refs)) {
                    $details->is_favorite = false;
                    $display[$i] = ['price' => $refs[$i], 'detail' => $details];
                }
            }
        }
        return array_values($display);
    }

    public function sendNotification(Request $request)
    {
        $this->validate($request, [
            'message' => 'required',
        ]);
        try {
            $users = MobUser::query()->get();
            // $user = MobUser::query()->where('num','330000072712')->first();
            foreach ($users as $user) {
                SendFCMNotification::dispatch([
                    'to' => $user->num,
                    'from' => null,
                    'message' => $request->message,
                ]);
            }
            return response()->json(['success' => true], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => __('app.error_message'), 'code_message' => $e->getMessage()], 500);
        }
    }

    public function validateNotif(
        Request $request,
        array $rules,
        array $messages = [],
        array $customAttributes = []
    ) {
        $validator = $this->getValidationFactory()
            ->make(
                $request->all(),
                $rules,
                $messages,
                $customAttributes
            );
        if ($validator->fails()) {
            $errors = (new \Illuminate\Validation\ValidationException($validator))->errors();
            throw new \Illuminate\Http\Exceptions\HttpResponseException(response()->json(
                [
                    'success' => false,
                    'message' => "Some fields are missing!",
                    'errors' => $errors
                ],
                \Illuminate\Http\JsonResponse::HTTP_UNPROCESSABLE_ENTITY
            ));
        }
    }

    public function pushNotification(Request $request)
    {
        $fieldRequiredString = str_replace('[attribute]', ':attribute', __("app.field_required"));
        $fieldIsNumString = str_replace('[attribute]', ':attribute', __("app.field_is_number"));

        $this->validateNotif(
            $request,
            [
                'userNum' => 'required',
                'messageEnum' => 'required',
            ],
            [
                'userNum.required' => $fieldRequiredString,
                'messageEnum.required' => $fieldRequiredString,
            ]
        );

        if (!$request->hasHeader('Accept') || $request->header('Accept') != "application/json") {
            return response()->json(['success' => false, "message" => "The Accept header needs to be: application/json"], 500);
        }

        // try {
        $user = MobUser::query()->where('num', $request->input('userNum'))->first();
        if ($user != null) {
            $notification = NotificationRole::query()
                ->where('num', (int)$request->input('messageEnum'))
                ->first();
            $message = null;

            if (!$request->headers->has('country-code')) {
                $country = Country::query()->where('slug.fr', 'metropole')->first();
            } else {
                $country_code = $request->header('country-code');
                $country = Country::query()->where('country_code', $country_code)->first();
            }

            if ($user->agenda_flag == "0" && $notification->module == "Agenda") {
                return response()->json(['success' => false, "message" => __("app.notifs_turned_off_module")], 200);
            } else if ($user->boutique_flag == "0" && $notification->module == "Boutique") {
                return response()->json(['success' => false, "message" => __("app.notifs_turned_off_module")], 200);
            } else if ($user->formation_flag == "0" && $notification->module == "Formation") {
                return response()->json(['success' => false, "message" => __("app.notifs_turned_off_module")], 200);
            } else if ($user->event_flag == "0" && $notification->module == "Événement") {
                return response()->json(['success' => false, "message" => __("app.notifs_turned_off_module")], 200);
            } else if ($user->podcast_flag == "0" && $notification->module == "Podcast") {
                return response()->json(['success' => false, "message" => __("app.notifs_turned_off_module")], 200);
            }

            if ($notification != null) {

                $order_id = null;
                $training_id = null;
                $event_id = null;
                $rdv_id = null;
                $other_meeting_id = null;
                $podcast_id = null;
                $participation_date = null;
                switch ($notification->name) {
                    case 'YOUR_ORDER_SHIPPED':
                        $this->validateNotif(
                            $request,
                            [
                                'order_id' => 'required|numeric',
                                'flag' => 'required',
                            ],
                            [
                                'order_id.required' => $fieldRequiredString,
                                'order_id.numeric' => $fieldIsNumString,
                                'flag.required' => $fieldRequiredString,
                            ]
                        );
                        $order_id = $request->input('order_id');
                        $message = str_replace('[order_id]', $request->input('order_id'), $notification->message);

                        $result = $this->thirdPartyFunc('POST', 'getCmde', [
                            "flag" => strtoupper($request->flag),
                            "env" => $country->products_code,
                            "num" => $user->num,
                            "numOrder" => (int)$order_id
                        ]);
                        if (isset($result->success) && $result->success) {
                        } else {
                            return response()->json(['success' => false, "message" =>
                            str_replace('[attribute]', 'order', __("app.id_not_found"))], 500);
                        }

                        break;
                    case 'AGENDA_REMINDER_SALE_TOMOROW':
                        $this->validateNotif(
                            $request,
                            [
                                'rdv_id' => 'required|numeric',
                            ],
                            [
                                'rdv_id.required' => $fieldRequiredString,
                                'rdv_id.numeric' => $fieldIsNumString,
                            ]
                        );
                        $rdv_id = $request->input('rdv_id');
                        $message = $notification->message;
                        break;
                    case 'AGENDA_SALE_TO_BEGIN':
                        $this->validateNotif(
                            $request,
                            [
                                'rdv_id' => 'required|numeric',
                            ],
                            [
                                'rdv_id.required' => $fieldRequiredString,
                                'rdv_id.numeric' => $fieldIsNumString,
                            ]
                        );
                        $rdv_id = $request->input('rdv_id');
                        $message = $notification->message;
                        break;
                    case 'CREATE_A_SALE':
                        $this->validateNotif(
                            $request,
                            [
                                'rdv_id' => 'required|numeric',
                            ],
                            [
                                'rdv_id.required' => $fieldRequiredString,
                                'rdv_id.numeric' => $fieldIsNumString,
                            ]
                        );
                        $rdv_id = $request->input('rdv_id');
                        $message = $notification->message;
                        break;
                    case 'APPOINTMENT_SCHEDULED_TOMORROW':
                        $this->validateNotif(
                            $request,
                            [
                                'other_meeting_id' => 'required',
                            ],
                            [
                                'other_meeting_id.required' => $fieldRequiredString,
                            ]
                        );

                        $other_meeting_id = $request->input('other_meeting_id');

                        try {
                            $meetings = Meeting::find($other_meeting_id);
                            if (!isset($meetings)) {
                                return response()->json(['success' => false, "message" =>
                                str_replace('[attribute]', 'meeting', __("app.id_not_found"))], 500);
                            }
                        } catch (\Exception $e) {
                            return response()->json(['success' => false, "message" =>
                            str_replace('[attribute]', 'meeting', __("app.id_not_found"))], 500);
                        }

                        $message = $notification->message;
                        break;
                    case 'APPOINTMENT_TO_START':
                        $this->validateNotif(
                            $request,
                            [
                                'other_meeting_id' => 'required',
                            ],
                            [
                                'other_meeting_id.required' => $fieldRequiredString,
                            ]
                        );

                        $other_meeting_id = $request->input('other_meeting_id');

                        try {
                            $meetings = Meeting::find($other_meeting_id);
                            if (!isset($meetings)) {
                                return response()->json(['success' => false, "message" =>
                                str_replace('[attribute]', 'meeting', __("app.id_not_found"))], 500);
                            }
                        } catch (\Exception $e) {
                            return response()->json(['success' => false, "message" =>
                            str_replace('[attribute]', 'meeting', __("app.id_not_found"))], 500);
                        }

                        $message = $notification->message;
                        break;
                    case 'REGISTERED_TOMMOROW_TRAINING':
                        $this->validateNotif(
                            $request,
                            [
                                'training_id' => 'required|numeric',
                            ],
                            [
                                'training_id.required' => $fieldRequiredString,
                                'training_id.numeric' => $fieldIsNumString,
                            ]
                        );
                        $training_id = $request->input('training_id');
                        $message = str_replace('[training_id]', $request->input('training_id'), $notification->message);
                        break;
                    case 'TRAINING_TO_BEGIN':
                        $this->validateNotif(
                            $request,
                            [
                                'training_id' => 'required|numeric',
                            ],
                            [
                                'training_id.required' => $fieldRequiredString,
                                'training_id.numeric' => $fieldIsNumString,
                            ]
                        );
                        $training_id = $request->input('training_id');
                        $message = str_replace('[training_id]', $request->input('training_id'), $notification->message);
                        break;
                    case 'DONT_FORGET_EVENT':
                        $this->validateNotif(
                            $request,
                            [
                                'event_id' => 'required|numeric',
                            ],
                            [
                                'event_id.required' => $fieldRequiredString,
                                'event_id.numeric' => $fieldIsNumString,
                            ]
                        );
                        $event_id = $request->input('event_id');
                        $message = str_replace('[event_id]', $request->input('event_id'), $notification->message);
                        // return response()->json(['message'=>$message], 200);
                        break;
                    case 'EVENT_TO_START':
                        $this->validateNotif(
                            $request,
                            [
                                'event_id' => 'required|numeric',
                            ],
                            [
                                'event_id.required' => $fieldRequiredString,
                                'event_id.numeric' => $fieldIsNumString,
                            ]
                        );
                        $event_id = $request->input('event_id');
                        $message = str_replace('[event_id]', $request->input('event_id'), $notification->message);
                        break;
                    case 'GIVE_EVENT_OPINION':
                        $this->validateNotif(
                            $request,
                            [
                                'event_id' => 'required|numeric',
                                'participation_date' => 'required',
                            ],
                            [
                                'event_id.required' => $fieldRequiredString,
                                'event_id.numeric' => $fieldIsNumString,
                                'participation_date.required' => $fieldRequiredString,
                            ]
                        );

                        $event_id = $request->input('event_id');
                        $participation_date = $request->input('participation_date');

                        $message = str_replace('[event_id]', $request->input('event_id'), $notification->message);
                        $message = str_replace('[participation_date]', $request->input('participation_date'), $message);
                        break;
                    case 'GIVE_TRAINING_OPINION':
                        $this->validateNotif(
                            $request,
                            [
                                'training_id' => 'required|numeric',
                                'participation_date' => 'required',
                            ],
                            [
                                'training_id.required' => $fieldRequiredString,
                                'training_id.numeric' => $fieldIsNumString,
                                'participation_date.required' => $fieldRequiredString,
                            ]
                        );
                        $training_id = $request->input('training_id');
                        $participation_date = $request->input('participation_date');
                        $message = str_replace('[training_id]', $request->input('training_id'), $notification->message);
                        $message = str_replace('[participation_date]', $request->input('participation_date'), $message);
                        break;
                    case 'NEW_PODCAST':
                        $this->validateNotif(
                            $request,
                            [
                                'podcast_id' => 'required|numeric',
                            ],
                            [
                                'podcast_id.required' => $fieldRequiredString,
                                'podcast_id.numeric' => $fieldIsNumString
                            ]
                        );
                        $podcast_id = $request->input('podcast_id');
                        $message = $notification->message;
                        break;
                    case 'CUSTOMER_ORDER':
                        $this->validateNotif(
                            $request,
                            [
                                'order_id' => 'required|numeric',
                            ],
                            [
                                'order_id.required' => $fieldRequiredString,
                                'order_id.numeric' => $fieldIsNumString
                            ]
                        );
                        $order_id = $request->input('order_id');
                        $message = $notification->message;
                        break;
                    case 'SUB_END':
                        $message = $notification->message;
                        break;
                    default:
                        return response()->json(['success' => false], 500);
                }
                // return response()->json(['message'=>$message], 200);
                SendFCMNotification::dispatch([
                    'to' => $user->num,
                    'from' => null,
                    'name' => $notification->name,
                    'message' => $message,
                    'module' => $notification->module,
                    'order_id' => $order_id,
                    'training_id' => $training_id,
                    'event_id' => $event_id,
                    'rdv_id' => $rdv_id,
                    'other_meeting_id' => $other_meeting_id,
                    'podcast_id' => $podcast_id,
                    'participation_date' => $participation_date,
                ]);

                return response()->json(['success' => true], 200);
            } else {
                return response()->json(['success' => false, 'message' => 'notification not found'], 500);
            }
        } else {
            return response()->json(['success' => false, 'message' => 'user not found'], 500);
        }

        // } catch (\Exception $e) {
        //     return response()->json(['message'=>__('app.error_message'),'code_message'=>$e->getMessage()], 500);
        // }

    }

    public function saveOtherRDVNotif(Request $request)
    {
        $this->validate($request, [
            'messageEnum' => 'required',
            'other_rdv_id' => 'required',
            'participation_date' => 'required',
        ]);

        $notificationData = array();

        $notification = NotificationRole::query()
            ->where('num', (int)$request->input('messageEnum'))
            ->first();

        $notificationData['user_id'] = $request->user_id;
        $notificationData['module'] = $notification->module;
        $notificationData['name'] = $notification->name;
        $notificationData['message'] = $notification->message;
        $notificationData['order_id'] = null;
        $notificationData['training_id'] = null;
        $notificationData['event_id'] = null;
        $notificationData['rdv_id'] = null;
        $notificationData['other_rdv_id'] = $request->other_rdv_id;
        $notificationData['participation_date'] = $request->participation_date;
        $notificationData['related_id'] = 'admin';

        $not = Notification::create($notificationData);

        return response()->json(['success' => true, 'notification' => $not], 200);
    }

    public function getNotifications(Request $request)
    {

        $notifications = Notification::query()
            ->where('user_id', $request->user_id)
            ->whereIn('module', ["Podcast", "Boutique"])
            ->where('created_at', '>', Carbon::now()->addDays(-30))
            ->orderByDesc('created_at')
            ->get();

        if ($notifications != null) {
            foreach ($notifications as $n) {
                if ($n->is_checked != 1) {
                    $n->is_checked = 1;
                    $n->save();
                }
            }
        }

        return response()->json(['success' => true, 'notification' => $notifications], 200);
    }

    public function deleteNotification(Request $request)
    {

        $this->validate($request, [
            'notification_id' => 'required',
        ]);

        $notification_deletion = Notification::query()
            ->where('_id', $request->notification_id)
            ->delete();

        if ($notification_deletion == 0) {
            return response()->json(['message' => __('app.error_message')], 500);
        }

        return response()->json(['success' => true], 200);
    }

    public function checkNewNotifications(Request $request)
    {
        try {
            $notifications = Notification::query()->where('user_id', $request->user_id)->where('is_checked', '!=', 1)->orderBy('created_at', 'desc')->get();

            return response()->json(['count' => count($notifications)], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => __('app.error_message'), 'code_message' => $e->getMessage()], 500);
        }
    }

    public function changeNotificationFlags(Request $request)
    {

        $user = MobUser::query()->where('num', $request->user_id)->first();


        try {

            if ($request->has('agenda_flag')) {
                $user->agenda_flag = $request->input('agenda_flag');
            }

            if ($request->has('boutique_flag')) {
                $user->boutique_flag = $request->input('boutique_flag');
            }

            if ($request->has('formation_flag')) {
                $user->formation_flag = $request->input('formation_flag');
            }

            if ($request->has('event_flag')) {
                $user->event_flag = $request->input('event_flag');
            }

            if ($request->has('podcast_flag')) {
                $user->podcast_flag = $request->input('podcast_flag');
            }


            if (is_null($user->agenda_flag)) {
                $user->agenda_flag = "1";
            }

            if (is_null($user->boutique_flag)) {
                $user->boutique_flag = "1";
            }

            if (is_null($user->formation_flag)) {
                $user->formation_flag = "1";
            }

            if (is_null($user->event_flag)) {
                $user->event_flag = "1";
            }

            if (is_null($user->podcast_flag)) {
                $user->podcast_flag = "1";
            }

            $user->save();

            $flags = [
                'agenda_flag' => $user->agenda_flag,
                'boutique_flag' => $user->boutique_flag,
                'formation_flag' => $user->formation_flag,
                'event_flag' => $user->event_flag,
                'podcast_flag' => $user->podcast_flag,
            ];

            return response()->json(['flags' => $flags], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => __('app.error_message'), 'code_message' => $e->getMessage()], 500);
        }
    }

    public function getNotificationFlags(Request $request)
    {

        $user = MobUser::query()->where('num', $request->user_id)->first();

        $agenda_flag = $user->agenda_flag;
        $boutique_flag = $user->boutique_flag;
        $formation_flag = $user->formation_flag;
        $event_flag = $user->event_flag;
        $podcast_flag = $user->podcast_flag;

        if (is_null($agenda_flag)) {
            $user->agenda_flag = "1";
        }

        if (is_null($boutique_flag)) {
            $user->boutique_flag = "1";
        }

        if (is_null($formation_flag)) {
            $user->formation_flag = "1";
        }

        if (is_null($event_flag)) {
            $user->event_flag = "1";
        }

        if (is_null($podcast_flag)) {
            $user->podcast_flag = "1";
        }

        $user->save();

        $flags = [
            'agenda_flag' => $user->agenda_flag,
            'boutique_flag' => $user->boutique_flag,
            'formation_flag' => $user->formation_flag,
            'event_flag' => $user->event_flag,
            'podcast_flag' => $user->podcast_flag,
        ];

        return response()->json(['success' => true, 'flags' => $flags], 200);
    }

    public function getFacture(Request $request)
    {
        $this->validate($request, [
            'numOrder' => 'required',
        ]);
        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $result = $this->thirdPartyFunc(
            'POST',
            'getFacture',
            [
                "flag" => $request->type,
                "env" => $country->products_code,
                "num" => $request->user_id,
                "numOrder" => $request->numOrder,
            ]
        );
        if (isset($result->success) && $result->success) {
            return response()->json(['facture' => $result], 200);
        } else {
            return response()->json($result, 500);
        }
    }

    public function verifCmde(Request $request)
    {
        $this->validate($request, [
            'numOrder' => 'required',
        ]);
        if (!$request->headers->has('country-code')) {
            $country = Country::query()->where('slug.fr', 'metropole')->first();
        } else {
            $country_code = $request->header('country-code');
            $country = Country::query()->where('country_code', $country_code)->first();
        }

        $result = $this->thirdPartyFunc(
            'POST',
            'verifCmde',
            [
                "flag" => $request->type,
                "env" => $country->products_code,
                "numOrder" => $request->numOrder,
            ]
        );
        if (isset($result->success) && $result->success) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 500);
        }
    }
}
