<?php

namespace App\Http\Controllers\Api;

use App\Models\Product;
use App\Models\ProductCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    //
    public function categories () {
//        try{
            $categories = ProductCategory::query()->where('is_active','1')
                ->orderBy('order')
                ->get()->each(function($item){
                    $item->products = $item->products();
                });
            return response()->json(['status'=>'success','data'=>$categories]);
//        }catch ( \Exception $exception){
//            return response()->json(['status'=>'error']);
//        }

    }


    public function products ($id) {
        try{
            $products = Product::query()
                ->where('category_id',$id)
                ->where('is_active','1')->get();
            return response()->json(['status'=>'success','data'=>$products]);
        }catch ( \Exception $exception){
            return response()->json(['status'=>'error']);
        }

    }


}
