<?php

namespace App\Http\Controllers\Api;

use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\UserObject;
use App\Models\Group;
use App\Models\ReminderType;
use App\Models\Reminder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;


class UserObjectController extends Controller
{


    public function addUserObject (Request $request) {
        if (UserObject::query()->where('object_type', $request->input('object_type'))
        ->where('user_id', $request->user_id)->count() != 0) {

            $obj = UserObject::query()->where('object_type', $request->input('object_type'))
            ->where('user_id', $request->user_id)->first();
            $userObject = UserObject::query()->find($obj->_id);
            if ($request->has('object_type') and $request->object_type != null) {
                $userObject->object_type = $request->object_type;
            }
            if ($request->has('short_term_description')) {
                $userObject->short_term_description = $request->short_term_description;
            }
            if ($request->has('long_term_description')) {
                $userObject->long_term_description = $request->long_term_description;
            }
            $userObject->save();

            return response()->json(['userObject'=>$userObject], 200);
            
        }

            $params = $this->validate($request, [
                'object_type'=>'required',
            ]);
            $userObject = new UserObject();
            $userObject->object_type = $params['object_type'];        
            if ($request->has('short_term_description') and $request->short_term_description != null) {
                $userObject->short_term_description = $request->short_term_description;
            }
            if ($request->has('long_term_description') and $request->long_term_description != null) {
                $userObject->long_term_description = $request->long_term_description;
            }
            $userObject->user_id = $request->user_id;
        
            $userObject->save();

        return response()->json(['userObject'=>$userObject], 200);
    }
    

    public function getUserObject (Request $request) {
        
        $params = $this->validate($request, [
            'object_type'=>'required',
        ]);

        $userObjects = UserObject::query()->where('user_id', $request->user_id)
        ->where('object_type', $params['object_type'])->get();

        return response()->json(['userObjects'=>$userObjects], 200);

    }
    public function deleteUserObject (Request $request) {

        if (UserObject::query()->where('object_type', $request->input('object_type'))
        ->where('user_id', $request->user_id)->count() == 0 ) {
            return response()->json(['message'=>'no userObject found'], 500);
        }
        $params = $this->validate($request, [
            'object_type'=>'required',
        ]);
        
        $obj = UserObject::query()->where('object_type', $request->input('object_type'))
        ->where('user_id', $request->user_id)->first();

        $userObject = UserObject::query()->find($obj->_id);
        $userObject->delete();
        return response()->json(['message'=>'User Object deleted'], 200);


    }


}
