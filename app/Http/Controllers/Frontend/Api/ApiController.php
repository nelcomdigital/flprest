<?php

namespace App\Http\Controllers\Frontend\Api;

use App\Mail\ErrorMail;
use App\Mail\managerLessThan2CC;
use App\Mail\managerMoreThan2CC;
use App\Mail\sponsee36HourNotification;
use App\Mail\sponseeAfter72HourNotification;
use App\Mail\sponseeLessThan2CC;
use App\Mail\sponseeMoreThan2CC;
use App\Mail\sponserAfter72HourNotification;
use App\Mail\sponserLessThan2CC;
use App\Mail\sponserMoreThan2CC;
use App\Models\City;
use App\Models\Country;
use App\Models\Department;
use App\Models\GroupCountry;
use App\Models\Order;
use App\Models\Region;
use App\Models\Translation;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use MongoDB\Operation\Count;

class ApiController extends Controller
{
    //

    public function confirmPayment( $code ){

        $order = Order::query()->where('code',$code)->first();
        $order->status = 'paid';
        $order->save();
        $user = User::query()->where('_id',$order->user_id)->first();
        $m = $user->email;
        $res = $this->createServerOrder($order);
        $pass = null;
        if ( $res != null ){
            $pass = $res['pwdFBO'];
            $user->reference = $res['numFBO'];
            $user->password = bcrypt($res['pwdFBO']);
            $user->save();
        }

//        try{
        if ( $order->points < 2 && !$order->is_pack ){

            $url = 'https://webservice.foreverliving.fr/wsinscription.php';
            $ch = curl_init($url);
            $jsonData = array("action" => "droitDeParrainer",
                "id_parrain"=> str_replace('-','',$order->distributor_number),
            );
            $jsonDataEncoded = json_encode($jsonData);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $sponser = curl_exec($ch);
            $sponser = json_decode($sponser);



            if ( $sponser !=null && $sponser->success ){
                try{
                    Mail::to($m)->send(new sponseeLessThan2CC(
                        ['reference'=>$user->reference,
                            'password'=>$pass,
                            'd_first_name'=>$sponser->parrain->prenom,
                            'd_last_name'=>$sponser->parrain->nom,
                            'd_phone'=>$sponser->parrain->tel,
                            'd_mail'=>$sponser->parrain->email
                        ]
                    ));
                }catch (\Exception $exception){

                }
                try{
                    $city = City::query()->where('_id',$user->city_id)->first();
                    Mail::to($sponser->parrain->email)->send(new sponserLessThan2CC([
                        's_name'=>$user->name,
                        'reference'=>$user->reference,
                        's_first_name'=>$user->first_name,
                        's_last_name'=>$user->last_name,
                        's_address'=>$user->street_address,
                        's_zip_code'=>$user->zip_code,
                        's_city'=>$city!= null ? $city->name[$order->language]:'',
                        's_phone'=>$user->phone_number,
                        's_mail'=>$user->email,
                    ]));
                }catch (\Exception $exception){

                }
                try{
                    $city = City::query()->where('_id',$user->city_id)->first();
                    Mail::to($sponser->manager->email)->send(new managerLessThan2CC([
                        's_name'=>$user->name,
                        'reference'=>$user->reference,
                        's_first_name'=>$user->first_name,
                        'd_first_name'=>$sponser->parrain->prenom.' '.$sponser->parrain->nom,
                        's_last_name'=>$user->last_name,
                        'd_last_name'=>$order->distributor_number,
                        's_address'=>$user->street_address,
                        's_zip_code'=>$user->zip_code,
                        's_city'=>$city!= null ? $city->name[$order->language]:'',
                        's_phone'=>$user->phone_number,
                        's_mail'=>$user->email,
                    ]));
                }catch (\Exception $exception){

                }
            }

        }
        else
        {
            $url = 'https://webservice.foreverliving.fr/wsinscription.php';
            $ch = curl_init($url);
            $jsonData = array("action" => "droitDeParrainer",
                "id_parrain"=> str_replace('-','',$order->distributor_number),
            );
            $jsonDataEncoded = json_encode($jsonData);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $sponser = curl_exec($ch);
            $sponser = json_decode($sponser);
            if ( $sponser !=null ){

                try{
                    Mail::to($m)->send(new sponseeMoreThan2CC(
                        ['reference'=>$user->reference,
                            'password'=>$pass,
                            'd_first_name'=>$sponser->parrain->prenom,
                            'd_last_name'=>$sponser->parrain->nom,
                            'd_phone'=>$sponser->parrain->tel,
                            'd_mail'=>$sponser->parrain->email
                        ]
                    ));
                }catch (\Exception $exception){

                }
                try{
                    $city = City::query()->where('_id',$user->city_id)->first();
                    Mail::to($sponser->parrain->email)->send(new sponserMoreThan2CC([
                        's_name'=>$user->name,
                        'reference'=>$user->reference,
                        's_first_name'=>$user->first_name,
                        's_last_name'=>$user->last_name,
                        's_address'=>$user->street_address,
                        's_zip_code'=>$user->zip_code,
                        's_city'=>$city!= null ? $city->name[$order->language]:'',
                        's_phone'=>$user->phone_number,
                        's_mail'=>$user->email,
                    ]));
                }catch (\Exception $exception){

                }
                try{
                    $city = City::query()->where('_id',$user->city_id)->first();
                    Mail::to($sponser->manager->email)->send(new managerMoreThan2CC([
                        's_name'=>$user->name,
                        'reference'=>$user->reference,
                        's_first_name'=>$user->first_name,
                        'd_first_name'=>$sponser->parrain->prenom. ' '.$sponser->parrain->nom,
                        's_last_name'=>$user->last_name,
                        'd_last_name'=>$order->distributor_number,
                        's_address'=>$user->street_address,
                        's_zip_code'=>$user->zip_code,
                        's_city'=>$city!= null ? $city->name[$order->language]:'',
                        's_phone'=>$user->phone_number,
                        's_mail'=>$user->email,
                    ]));
                }catch (\Exception $exception){

                }

            }

        }

        return response()->json(['status'=>'success']);


    }

    public function createServerOrder($order){


        $user = User::query()->where('_id',$order->user_id)->first();
        $city = City::query()->where('_id',$user->city_id)->first();

        $address = $order->address;
        $country = Country::query()->where('_id', $user->country_id)->first();
        $scity = City::query()->where('_id',$address['city_id'])->first();
        $url = 'https://webservice.foreverliving.fr/wsinscription.php';
        $ch = curl_init($url);
        $jsonData = array("action" => "finCommandeInc",
            "env"=> $country->products_code,
            "region"=> $country->region_code,
            "typliv"=>$country->product_type,
            "numCommande"=>$order->code."",
            "totalTTC"=>number_format($order->total,2),
            "typePaiement"=>"CB",
            "infoFBO"=>[
                "numParrain"=>str_replace('-','',$user->distributor_number),
                "civilite"=>$user->gender =="male"? "M":"Mme",
                "nom"=>trim($user->last_name),
                "prenom"=>trim($user->first_name),
                "adresse"=>$user->street_address,
                "complement"=>$user->additional_address,
                "cp"=>$user->zip_code,
                "ville"=>$city->name[$order->language],
                "paysFBO"=>$country->delivery_code,
                "wilaya"=>'',
                "gouvernorat"=>'',
                "quartier"=>'',
                "dateNaissance"=>Carbon::parse($user->date_of_birth)->format('d/m/Y'),
                'gsm'=>$user->phone_number,
                'email'=>$user->email,
                'cin'=>'',
                'urlScan'=>''
            ],
            'livraison'=>[
                'pointRelais'=>$address['shipping_title'] == 'DOM'?"":$address['check_point'] ,
                'typePtRelais'=>$address['shipping_title'] == 'DOM'?
                    ($country->products_code == 'FRA'?"CHRONO2":"")
                    :$address['chrono_idTnt'] ,
                'nomLiv'=>$address['name'],
                'ad1Liv'=>$address['shipping_title'] == 'DOM'?$address['street']:$address['chrono_ad1liv'],
                'ad2liv'=>$address['shipping_title'] == 'DOM'? $address['additional_street']:$address['chrono_ad2liv'],
                'cpLiv'=>$address['shipping_title'] == 'DOM'?$address['zip_code']:$address['chrono_zip'] ,
                'infosLiv'=>$address['shipping_title'] == 'DOM'?'':$address['chrono_info'],
                'gsmLiv'=>$address['phone_number'],
                'emaiLiv'=>$address['email'],
                'villeLiv'=>$address['shipping_title'] == 'DOM'?($scity==null ?$address['city_id']: $scity->name[$order->language]):$address['chrono_city'],
                'paysLiv'=>$country->delivery_code,
            ],
            'infoCB'=>[
                "response_code"=> "00",
                "transaction_id"=> $order->code,
                "authorisation_id"=> $order->code,
                "payment_certificate"=> 'SINGLE',
                "payment_date"=>substr( $order->created_at,0,8),
                "payment_time"=> substr( $order->created_at,8,4),
                "amount"=> $order->amount,
                "card_number"=> "XXXx"
            ]
        );

        $detail = [];

        foreach ( $order->details as $d){
            $detail[] =[
                'ref'=>$d['product_id'],
                'qte'=>$d['quantity']
            ];
        }
        if ( $address['check_point'] != '' ||$address['shipping_reference']){
            $detail[]=[
                'ref'=>$address['shipping_reference'],
                'qte'=>1
            ];
        }


        $jsonData['commande'] = $detail;

        $jsonDataEncoded = json_encode($jsonData);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $res = curl_exec($ch);
        $result = json_decode($res, true);
        try{
            if ( $result['success'] ) {
                return $result;
            }else{
                try{
                    Log::info('error');
                    Log::info($result);
                    Mail::to('inscription@foreverliving.fr')
                        ->send(new ErrorMail(
                            json_encode($jsonData).'-----------'.json_encode($result)));

                }catch (\Exception $e){

                }
                return null;
            }

        }catch (\Exception $e){
            try{
                Mail::to('inscription@foreverliving.fr')
                    ->send(new ErrorMail(
                        json_encode($jsonData).' ----------- '. $e->getMessage() ));
                Log::info('exception');
                Log::info($e->getMessage());
            }catch (\Exception $e){

            }
            return null;
        }

    }

    public function test(){
        for($i=0; $i< 10000; $i++ ){
            $cities = City::query()->skip($i*500)->limit(500)->get();
            foreach ($cities as $city){
                $n=[];
                foreach ($city->name as $k =>$v){
                    $x = str_replace('‚','é',$v);
                    $x = str_replace('?¡','Œ',$x);
                    $x = str_replace('Š','è',$x);
                    $x = str_replace('ˆ','ê',$x);
                    $x = str_replace('‰','ë',$x);
                    $x = str_replace('…','à',$x);
                    $x = str_replace('ƒ','â',$x);
                    $x = str_replace('?’','ï',$x);
                    $x = str_replace('“','ô',$x);
                    $x = str_replace('‡','ç',$x);
                    $x = str_replace('?ƒ','É',$x);
                    $n[$k] = $x;
                }
                $city->name = $n;
                $city->save();
            }
        }
    }

    public function connect($type,$cat = null, $region = null , $letter = null ){

        if ( $type == "cat"){
            $ts = time();
            $token = hash('sha512', 'tng-2019' . $ts . env('TOKEN_KEY'));
            $ur = env('DISTRIB');
            $url = $ur.'products/categories/list.api?ts='.$ts.'&token='.$token;
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $result = curl_exec($ch);
            curl_close($ch);

            return $result;
        }else if ( $type == "product" ){
            $ts = time();
            $token = hash('sha512', 'tng-2019' . $ts . env('TOKEN_KEY'));
            $ur = env('DISTRIB');
            if ( $cat == null )
                $url = $ur.'products/list.api?ts='.$ts.'&token='.$token.'&category_id=1';
            else
                $url = $ur.'products/list.api?ts='.$ts.'&token='.$token.'&category_id='.$cat;
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_POST, 0);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $result = curl_exec($ch);
            curl_close($ch);

            $result = json_decode($result);

            return $result->data->{4}->description->fr;

        }else if ($type == "price"){
            $url = 'https://webservice.foreverliving.fr/wsinscription.php';
            $ch = curl_init($url);
            $jsonData = array("action" => "getListeArticlesInc",
                "env"=> $cat,
                "region"=> $region,
                "typliv"=>$letter);
            $jsonDataEncoded = json_encode($jsonData);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $result = curl_exec($ch);

            curl_close($ch);
            return  $result;
        }else{
            $url = 'https://webservice.foreverliving.fr/wsinscription.php';
            $ch = curl_init($url);
            $jsonData = array("action" => "verifStockRef",
                "env"=> "FRA",
                "region"=> "101",
                "typliv"=>"R",
                "ref"=>15,
                "qte"=>2);



            $jsonDataEncoded = json_encode($jsonData);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $result = curl_exec($ch);

            curl_close($ch);
            return  $result;
        }


    }

    public function checkEmail($email){
        return response()->json(['status'=>'success']);
        $test = DB::connection('sqlsrv')->table('tbl_Clientes')
            ->where('Email_Cli',$email)
            ->count();
        return response()->json(['status'=>$test == 0?'success':'failed']);
    }

    public function checkDistributor($number){
        return response()->json(['status'=>'success']);
        if ( !Str::contains($number,'-')){
            $number = str_split($number , 3);
            $number = implode('-',$number);
        }
        $test = DB::connection('sqlsrv')->table('tbl_Clientes')
            ->where('Cod_Cli',$number)
            ->where('Validado_Cli',1)
            ->where('Bloqueado_Cli',0)
            ->count();
        return response()->json(['status'=>$test == 1?'success':'failed']);
    }

    public function getClient($number){
        return response()->json(['status'=>'success']);
        $test = DB::connection('sqlsrv')->table('tbl_Clientes')
            ->where('Cod_Cli',$number)->first();
        return response()->json(['status'=>$test]);
    }
}
