<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\Role;
use App\Models\Status;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        if (auth()->user() != null) {
            $domain = array_first(explode('.', str_replace('http://','',str_replace('https://','',request()->url()))));
            if ( $domain == 'cms')
                return redirect()->route('cms.home');
            return redirect()->to(mroute('home') );
        }
        $title = " Moovtoo Login";
        $countries = Country::query()->orderBy('name', 'asc')->get();
        return view('frontend.auth.login', compact('countries', 'title'));
    }

    public function redirectPath()
    {
        if (method_exists($this, 'redirectTo')) {
            return $this->redirectTo();
        }

        $domain = array_first(explode('.', str_replace('http://','',str_replace('https://','',request()->url()))));
        if ( $domain == 'cms')
            return property_exists($this, 'redirectTo') ?  $this->redirectTo : '/';
        return property_exists($this, 'redirectTo') ? (mroute('').'/'. $this->redirectTo) : '/';
    }

    public function login(Request $request)
    {

        $this->validateLogin($request);
        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        // $user = User::withTrashed()->where('email', $request->input('email'))->first();
        $user = User::query()->where('email', $request->input('email'))->first();
        $userRoles = $user->getUserRoles();

        if ($user != null and $user->deleted_at != null) {
            return response()->json(['custom_error' => __('front.label.100266')]);
        }
        $sub = array_first(explode('.', str_replace('http://', '', str_replace('https://', '', request()->url()))));

        if ($this->attemptLogin($request)) {
            if ($sub == 'business' || $sub == 'join') {
                if (in_array('event-owner', $userRoles )
                    ||in_array('venue-owner', $userRoles)
                    ||in_array('trip-manager', $userRoles)
                    ||in_array('activity-owner', $userRoles)){
                    return response()->json(['success' => true, 'redirect' => 'business']);
                } else{
                    return response()->json(['custom_error' => __('front.label.100267')]);
                }
            } else {
                $user->last_login = $request->input('last_login');
                $user->last_login_provider = $request->input('last_login_provider');
                $user->save();
                return $this->sendLoginResponse($request);
            }
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.

        $this->incrementLoginAttempts($request);

        if ($sub == 'business') {
            return response()->json(['custom_error' => __('front.label.100267')]);
        } else {
            return $this->sendFailedLoginResponse($request);
        }


    }


    public
    function redirectToProvider(Request $request,$cu,$lo, $provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public
    function handleProviderCallback(Request $request,$provider)
    {
        if ($provider == 'facebook') {
            $user = Socialite::driver('facebook')->stateless()->user();
        } else if ($provider == 'google') {
            $user = Socialite::driver('google')->stateless()->user();
        }

        $authUser = $this->findOrCreateUser($user, $provider);
        Auth::login($authUser);
        // dd($response->user);
        // Session::put('user' , collect($response->user) );
        // Session::put('token' , $response->token );
        // $authUser = $this->findOrCreateUser($user, $provider);
        // Auth::login($authUser);
        $domain = array_first(explode('.', str_replace('http://','',str_replace('https://','',request()->url()))));
        if ( $domain == 'cms')
            return redirect()->route('cms.home');

        return redirect()->to(mroute('home') );
    }

    public function findOrCreateUser($user, $provider)
    {
        $status = Status::query()->where('slug', 'active')->first();
        $authUser = User::query()->where('provider_id', $user->id)
            ->orWhere('email', $user->email)->first();
        if ($authUser != null) {
            return $authUser;
        }

        if ($provider == 'facebook') {
            if ($user->email == null)
                $user->email = $user->id . '@nelcomlab.club';

            $user = User::create([
                'name' => $user->name,
                'email' => $user->email,
                'provider' => $provider,
                'provider_id' => $user->id,
                'photo' => $user->avatar_original,
                'status_id' => $status->_id,
                'is_approved' => 1,
            ]);

            $role = Role::query()->where('slug', 'user')->first();
            $user->roles()->attach([$role->id]);
        }
        else if ( $provider == 'google'){
            if ( $user->email == null )
                $user->email = $user->id.'@nelcomlab.club';

            $user =  User::create([
                'name'     => $user->name,
                'email'    => $user->email,
                'provider' => $provider,
                'provider_id' => $user->id,
                'photo'=>$user->avatar_original,
                'status_id'=>$status->_id,
                'is_approved'=>1,
            ]);
            $role = Role::query()->where('slug', 'user')->first();
            $user->roles()->attach([$role->id]);
        }
        return $user;
    }

    protected
    function authenticated(Request $request, $user)
    {

        $status = Status::query()->where('_id', auth()->user()->status_id)->first();

        if ($status->slug == 'active' and $user->is_approved == 1) {
            return json_encode(['success' => $user, 'route' => route('home')]);
        } elseif ($user->status->name == 'inactive' or $user->is_approved == 0) {
            Auth::logout();
            return json_encode(['custom_error' => __('front.label.100264')]);
        } else {
            Auth::logout();
            return json_encode(['custom_error' => __('front.label.100265')]);
        }


    }

    public
    function logout(Request $request)
    {
        $this->guard()->logout();

//        $request->session()->invalidate();

        return redirect()->back();
    }
}
