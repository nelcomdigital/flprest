<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\User;
use App\Models\Role;
use App\Models\Status;
use App\Models\VerifyUser;
use Jenssegers\Agent\Agent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendWelcomIndividualEmail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        if ( $data['email']!= ''){
            $user = User::query()->where('email',$data['email'] )->first();
            if ( $user != null and $user->deleted_at != null ){
                return Validator::make($data, [
                    'name' => ['required', 'string', 'max:255'],
                    'email' => ['required', 'string', 'email', 'max:255'],
                    'password' => ['required', 'string', 'min:6', 'confirmed'],
                ]);
            }
        }

        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        if ( $data['email']!= ''){
            // $user = User::withTrashed()->where('email',$data['email'] )->first();
            $user = User::query()->where('email',$data['email'] )->first();
            if ( $user != null and $user->deleted_at == null ){
                return false;
            }else if ( $user != null and $user->deleted_at != null ){
                 $user->forceDelete();
            }
        }
        $agent = new Agent();
        if($agent->isMobile()){
            $provider = "Mobile";
        }else if($agent->isTablet()){
            $provider = "Tablet";
        }else{
            $provider = "Moovtoo";
        }

        $user =  User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'country_id' => $data['country_id'],
            'phone' => $data['phone'],
            'provider' => $provider,
            'status_id' => Status::query()->where('slug', 'active')->first()->id,
        ]);
        $role = Role::query()->where('slug', 'user')->first();
        $user->roles()->attach([$role->id]);
        return $user;
    }

    public function register(Request $request,$cu,$lo)
    {
        $user = $this->create($request->except('_method', '_token'));
        if(!$user){
            return json_encode(['custom_error' => __("front.a_user_already_exists_with_this_email")]);
        }
        // Auth::login($customer);
        $verifyUser = VerifyUser::create([
            'user_id' => $user->id,
            'token' => str_random(40)
        ]);
        $user->register_date = $request->input('register_date');
        $user->last_login = $request->input('last_login');
        $user->register_provider = $request->input('register_provider');
        $user->last_login_provider = $request->input('last_login_provider');
        $user->save();
        Mail::to($user->email)->send(new SendWelcomIndividualEmail($user));
        
        return json_encode(['success' => __("front.thank_you_for_creating_your_moovtoo_account_!_to_activate_it_click_on_the_link_in_your_email")]);
    }
    public function verifyUser($cu, $lo, $token)
    {
        $verifyUser = VerifyUser::where('token', $token)->first();
        if(isset($verifyUser) ){
            $user = $verifyUser->user;
            if(!$user->is_approved) {
                $verifyUser->user->is_approved = 1;
                $verifyUser->user->save();
                $status = __("front.your_email_is_verified_you_can_login");
            }else{
                $status = __("front.your_email_has_already_been_verified_you_can_login");
            }
        }else{
            return redirect()->to(mroute('login'))->with('verify_message', __("front.sorry_your_email_cannot_be_identified"));
        }
        return redirect()->to(mroute('login'))->with('verify_message', $status);
    }
}
