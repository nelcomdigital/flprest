<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Status;
use Illuminate\Support\Facades\Crypt;


use App\Events\UpdateStock;
use App\Http\Controllers\Controller;
use App\Http\Middleware\Roles;
use App\Mail\BuyLaterMail;
use App\Mail\ErrorMail;
use App\Mail\managerLessThan2CC;
use App\Mail\managerMoreThan2CC;
use App\Mail\PinMail;
use App\Mail\sponseeFirstNotification;
use App\Mail\sponseeLessThan2CC;
use App\Mail\sponseeMoreThan2CC;
use App\Mail\sponseePurchaseOrder;
use App\Mail\sponserFirstNotification;
use App\Mail\sponserLessThan2CC;
use App\Mail\sponserMoreThan2CC;
use App\Models\City;
use App\Models\Country;
use App\Models\CustomerReview;
use App\Models\Department;
use App\Models\GroupCountry;
use App\Models\Order;
use App\Models\Page;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\Region;
use App\Models\Role;
use App\User;
use Carbon\Carbon;
use Closure;
use DOMDocument;
use Google_Client;
use Google_Service_YouTube_Video;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Mail\TestEmail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use App\Models\MobUser;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('');
    }

    public function index(Request $request, $id = null)
    {
        $groupCountries = GroupCountry::query()->orderBy('order', 'asc')->with('countries')->limit(1)->get();
        $welcome = Page::query()->where('slug', 'like', 'welcome-%')
            ->where('locales.code', session()->get('current_locale'))->first();
        $pages = Page::query()->where('slug', 'like', 'home-slider-%')->where('locales.code', session()->get('current_locale'))->get()->toArray();
        $preId = $id;
        if ($id != null)
            session()->remove('process');
        if ($welcome != null)
            return view('frontend.home', compact('groupCountries', 'pages', 'welcome', 'preId'));
        return view('frontend.home', compact('groupCountries', 'pages', 'preId'));
    }

    public function main()
    {
        return view('frontend.main');
    }

    public function checkInternalUser($email)
    {
        $user = User::query()->where('email', $email)->first();
        if ($user == null) return "true";
        $count = Order::query()->where('user_id', $user->_id)->count();
        if ($count == 0) return "true";

        return "false";
    }

    public function getProducts($id)
    {
        if (session()->get('process.country_detail.products_code') != null) {
            $url = 'https://webservice.foreverliving.fr/wsinscription.php';
            $ch = curl_init($url);
            $jsonData = array("action" => "getListeArticlesInc",
                "env" => session()->get('process.country_detail.products_code'),
                "region" => session()->get('process.country_detail.region_code'),
                "typliv" => session()->get('process.country_detail.product_type')
            );
            $jsonDataEncoded = json_encode($jsonData);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $result = curl_exec($ch);
            $result = json_decode($result);
            $productPrices = $result->listeArticles;
            $refs = [];
            foreach ($productPrices as $price) {
                $refs[$price->ref] = $price;
            }
            curl_close($ch);
        } else {
            $url = 'https://webservice.foreverliving.fr/wsinscription.php';
            $ch = curl_init($url);
            $jsonData = array("action" => "getListeArticlesInc",
                "env" => "FRA",
                "region" => "101",
                "typliv" => "P"
            );
            $jsonDataEncoded = json_encode($jsonData);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $result = curl_exec($ch);
            $result = json_decode($result);
            $productPrices = $result->listeArticles;
            $refs = [];
            foreach ($productPrices as $price) {
                $refs[$price->ref] = $price;
            }
            curl_close($ch);
        }


        $ts = time();
        $token = hash('sha512', 'tng-2019' . $ts . env('TOKEN_KEY'));
        $ur = env('DISTRIB');
        $url = $ur . 'products/list.api?ts=' . $ts . '&token=' . $token . '&category_id=' . $id;

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);

        $result = json_decode($result);
        try {
            $productDetails = $result->data;
        } catch (\Exception $exception) {
            $productDetails = [];
        }


        $display = [];
        foreach ($productDetails as $details) {
            $i = $details->ref_produit;
//                if ( array_key_exists( $i , $refs ) ){
//                    $display[$i] = ['price' =>  $refs[$i], 'detail'=> $details];
//                }
            if (array_key_exists($i, $refs)) {
                if ($i == "980" && array_key_exists('981', $refs)) {
                    $p980 = $refs[$i];
                    $p981 = $refs["981"];
                    $p['ref'] = $p980->ref;
                    $p['designation'] = $p980->designation;
                    $p['CC'] = $p980->CC + $p981->CC;
                    $p['prixTTC'] = $p980->prixTTC + $p981->prixTTC;
                    $p['prixHT'] = $p980->prixHT + $p981->prixHT;
                    $p['tauxTVA'] = "0.00";
                    $display[$i] = ['price' => (object)$p, 'detail' => $details];
                } else {
                    $display[$i] = ['price' => $refs[$i], 'detail' => $details];
                }
            }
        }
        return $display;
    }

    public function getProductsList($ids)
    {
        if (session()->get('process.country_detail.products_code') != null) {
            $url = 'https://webservice.foreverliving.fr/wsinscription.php';
            $ch = curl_init($url);
            $jsonData = array("action" => "getListeArticlesInc",
                "env" => session()->get('process.country_detail.products_code'),
                "region" => session()->get('process.country_detail.region_code'),
                "typliv" => session()->get('process.country_detail.product_type')
            );
            $jsonDataEncoded = json_encode($jsonData);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $result = curl_exec($ch);
            $result = json_decode($result);
            $productPrices = $result->listeArticles;
            $refs = [];
            foreach ($productPrices as $price) {
                $refs[$price->ref] = $price;
            }
            curl_close($ch);
        } else {
            $url = 'https://webservice.foreverliving.fr/wsinscription.php';
            $ch = curl_init($url);
            $jsonData = array("action" => "getListeArticlesInc",
                "env" => "FRA",
                "region" => "101",
                "typliv" => "P"
            );
            $jsonDataEncoded = json_encode($jsonData);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $result = curl_exec($ch);
            $result = json_decode($result);
            $productPrices = $result->listeArticles;
            $refs = [];
            foreach ($productPrices as $price) {
                $refs[$price->ref] = $price;
            }
            curl_close($ch);
        }

        $ts = time();
        $token = hash('sha512', 'tng-2019' . $ts . env('TOKEN_KEY'));
        $ur = env('DISTRIB');
        $url = $ur . 'products/list.api?ts=' . $ts . '&token=' . $token;

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);

        $result = json_decode($result);
        $productDetails = $result->data;

        $display = [];
        foreach ($productDetails as $details) {
//            if ( array_key_exists( $details->ref_produit , $refs )  && in_array($details->ref_produit, $ids) ){
//                $display[$details->ref_produit] = ['price' =>  $refs[$details->ref_produit], 'detail'=> $details];
//            }
            if (array_key_exists($details->ref_produit, $refs)
                && in_array($details->ref_produit, $ids)) {

                if ($details->ref_produit == "980" && array_key_exists('981', $refs)) {
                    $p980 = $refs[$details->ref_produit];
                    $p981 = $refs["981"];
                    $p['ref'] = $p980->ref;
                    $p['designation'] = $p980->designation;
                    $p['CC'] = $p980->CC + $p981->CC;
                    $p['prixTTC'] = $p980->prixTTC + $p981->prixTTC;
                    $p['prixHT'] = $p980->prixHT + $p981->prixHT;
                    $p['tauxTVA'] = "0.00";
                    $display[$details->ref_produit] = ['price' => (object)$p,
                        'detail' => $details];
                } else {
                    $display[$details->ref_produit] = ['price' => $refs[$details->ref_produit],
                        'detail' => $details];
                }


            }
        }
        return $display;
    }

    private function calculateTotal($productDetails)
    {
        $total = 0.0;
        $taxGroups = [];
        foreach ($productDetails as $detail) {
            $taxGroups [$detail['price']->tauxTVA][] = $detail;
        }
        foreach ($taxGroups as $tg => $products) {
            $sp = 0.0;
            foreach ($products as $k => $p) {
                if ($tg == "0.00") {
                    $sp += getCurrentQuantity($p['price']->ref) * $p['price']->prixTTC;
                } else {
                    $sp += getCurrentQuantity($p['price']->ref) * $p['price']->prixHT;
                }

            }
            $vat = ((float)number_format($sp * $tg / 100, 2, '.', ''));
            $total += $sp + $vat;
        }
        return $total;
    }

    public function calculateTotalAjax()
    {
        $products = $this->getProductsList(array_keys(session()->get('process.items')));
        return $this->calculateTotal($products);
    }

    private function getCategories()
    {
        $ts = time();
        $token = hash('sha512', 'tng-2019' . $ts . env('TOKEN_KEY'));
        $ur = env('DISTRIB');
        $url = $ur . 'products/categories/list.api?ts=' . $ts . '&token=' . $token;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result);
        if ($result->success) {
            try {
                return $result->data;
            } catch (\Exception $exception) {
                return [];
            }
        } else {
            return [];
        }
    }

    public function products(Request $request, $slug = null)
    {
        if (!session()->has('process')) {
            return redirect()->route('welcome');
        }
        $country_id = session()->get('process.country');
        $country = Country::query()->where('_id', $country_id)->first();
        session()->put('process.country_detail', $country->toArray());
        $categories = $this->getCategories();

        if ($slug != null) {
            $category = $categories->{$slug};
            $products = $this->getProducts($slug);
            $page = Page::query()->where('slug', 'like', 'product-list-header-%')
                ->where('locales.code', session()->get('current_locale'))->first();
            return view('frontend.product-list', compact('products', 'page', 'slug', 'category', 'categories'));
        } else {
            $category = Arr::first($categories);
            $products = $this->getProducts($category->id);
            $page = Page::query()->where('slug', 'like', 'product-list-header-%')
                ->where('locales.code', session()->get('current_locale'))->first();
            $slug = $category->id;
            return view('frontend.product-list', compact('products', 'page', 'slug', 'category', 'categories'));
        }

    }

    public function product(Request $request, $id = null)
    {

        if (!session()->has('process')) {
            return redirect()->route('welcome');
        }
        if (!$request->has('product')) {
            return redirect()->route('products');
        }

        $product = json_decode($request->product);
        $category = json_decode($request->category);
        return view('frontend.product-detail', compact('product', 'category'));
    }

    public function cart(Request $request, $order_id = null)
    {
        if (!session()->has('process') && $order_id == null) {
            return redirect()->route('welcome');
        }
        if ($order_id != null) {
            session()->remove('process');
            $order = Order::query()->find($order_id);
            if ($order == null)
                return redirect()->route('welcome');
            $user = User::query()->where('_id', $order->user_id)->first();

            try {
                session()->put('process.first_name', explode(' ', $user->name)[0]);
            } catch (\Exception $e) {

            }
            try {
                session()->put('process.last_name',
                    str_replace(explode(' ', $user->name)[0], '', $user->name));
            } catch (\Exception $e) {

            }

            session()->put('process.order_id', $order_id);
            session()->put('process.distributor_number', $user->distributor_number);
            session()->put('process.distributor_name', $user->distributor_name);
            session()->put('process.email', $user->email);
            session()->put('process.date_of_birth', $user->date_of_birth);
            session()->put('process.phone_number', $user->phone_number);
            session()->put('process.genderType', $user->gender);
            session()->put('current_locale', $order->language);
            session()->put('process.zip_code', $user->zip_code);
            session()->put('process.city', $user->city_id);
            session()->put('process.country', $user->country_id);
            try {
                $country_id = session()->get('process.country');
                $country = Country::query()->where('_id', $country_id)->first();
                session()->put('process.country_detail', $country->toArray());
            } catch (\Exception $e) {

            }
            session()->put('process.street_address', $user->street_address);
            session()->put('process.additional_address', $user->additional_address);
            session()->put('process.additional_address', $user->additional_address);
            session()->put('process.done', true);
            $details = $order->details;
            foreach ($details as $detail) {
                session()->put('process.items.' . $detail['product_id'], $detail['quantity']);
            }
            App::setLocale(session()->get('current_locale', $order->language));
        }
        if (session()->get('process.items') != null) {
            $products = $this->getProductsList(array_keys(session()->get('process.items')));
            $total = $this->calculateTotal($products);
            $totalCC = 0;
            foreach ($products as $product) {
                $totalCC += $product['price']->CC * getCurrentQuantity($product['price']->ref);
            }
            $page = Page::query()->where('slug', 'like', 'cart-footer%')
                ->where('locales.code', session()->get('current_locale'))->first();
            if ($order_id != null) {
                $order = Order::query()->find($order_id);
                return view('frontend.cart', compact('products', 'order_id', 'total', 'totalCC', 'order', 'page'));
            }
            if (session()->has('process.order_id')) {
                $order = Order::query()->find(session()->get('process.order_id'));
                return view('frontend.cart', compact('products', 'order_id', 'total', 'totalCC', 'order', 'page'));
            }
            return view('frontend.cart', compact('products', 'total', 'totalCC', 'page', 'order_id'));
        }
        return redirect()->back();
    }

    public function putSession(Request $request)
    {
        $keys = $request->input('key');
        $values = $request->input('value');
        $negative = $request->input('negative');
        foreach ($keys as $k => $v) {
            if ($v == 'item') {
                $item = session()->get('process.items.' . $values[$k], null);
                if ($item == null) {
                    session()->put('process.items.' . $values[$k], 1);
                } else {
                    $q = session()->get('process.items.' . $values[$k]);
                    if ($negative == null)
                        session()->put('process.items.' . $values[$k], ++$q);
                    elseif ($negative == 1)
                        session()->put('process.items.' . $values[$k], --$q);
                    elseif ($negative == 2)
                        session()->remove('process.items.' . $values[$k]);
                }
            } else {
                session()->put('process.' . $v, $values[$k]);
            }
        }
        return;
    }

    public function putSessionQty(Request $request)
    {
        $key = $request->input('key');
        $value = $request->input('value');
        $negative = $request->input('negative');
        $item = session()->get('process.items.' . $key, null);
        if ($item == null) {
            session()->put('process.items.' . $key, 1);
        } else {
            $q = session()->get('process.items.' . $key);
            if ($negative == null)
                session()->put('process.items.' . $key, $value);
            elseif ($negative == 1)
                session()->put('process.items.' . $key, $value);
            elseif ($negative == 2)
                session()->remove('process.items.' . $key);
        }

        return $this->calculateTotalAjax();
    }

    public function reviewCart(Request $request, $order_id = null)
    {
        if (!session()->has('process')) {
            return redirect()->route('welcome');
        }
        if (session()->get('process.items') != null) {

            $products = $this->getProductsList(array_keys(session()->get('process.items')));
            $total = $this->calculateTotal($products);
            $totalCC = 0;
            $pack = false;
            foreach ($products as $product) {
                $totalCC += $product['price']->CC * getCurrentQuantity($product['price']->ref);
                if ($product['price']->ref == session()->get('process.country_detail.pack_product')) {
                    $pack = true;
                }

            }
            $total += session()->get('process.shipping_amount', 0);
            $page = Page::query()->where('slug', 'cart-footer')
                ->where('locales.code', session()->get('current_locale'))->first();

            $user = User::query()->where('email', session()->get('process.email'))->first();
            if ($user->distributor_number != null)
                session()->put('process.distributor_number', $user->distributor_number);
            if (session()->get('process.distributor_number', null) == null)
                return redirect()->route('welcome');
            $em = $user->email;
            $na = $user->name;
            $pin = $this->generatePIN();
            $user->pin_code = $pin;
            $user->save();

            try {
                Mail::to($em)->send(new PinMail(['name' => $na, 'pin' => $pin]));
            } catch (\Exception $e) {

            }


            return view('frontend.layouts.partials.blocks.cart.review-order', compact('products', 'page', 'total',
                'totalCC', 'pack'));
        }
        return redirect()->back();
    }

    public function address(Request $request, $order_id = null)
    {
        if (!session()->has('process')) {
            return redirect()->route('welcome');
        }

        $temp = User::query()->where('email', session()->get('process.email'))->first();
        if ($temp == null) {
            $user = new User();
        } else {
            $user = User::query()->where('email', session()->get('process.email'))->first();
        }
//        if ( $user->reference == null )
//            $user->reference = $this->createTempCode();
        $user->name = session()->get('process.first_name') . ' ' . session()->get('process.last_name');
        $user->first_name = session()->get('process.first_name');
        $user->last_name = session()->get('process.last_name');
        $user->email = session()->get('process.email');
        $user->distributor_number = session()->get('process.distributor_number');

        $user->date_of_birth = session()->get('process.date_of_birth');
        $user->phone_number = session()->get('process.phone_number');
        $user->gender = session()->get('process.genderType');
        $user->zip_code = session()->get('process.zip_code');
        $user->city_id = session()->get('process.city');
        $user->country_id = session()->get('process.country');
        $user->street_address = session()->get('process.street_address');
        $user->additional_address = session()->get('process.additional_address');

        $user->save();
        $user->roles()->attach(Role::query()->where('slug', 'l1')->first()->_id);
        $user->save();
        $city = City::query()->where('_id', session()->get('process.city'))->first();
        if ($city != null)
            session()->put('process.city_name', $city->name[session()->get('current_locale')]);

        $country = Country::query()->where('_id', session()->get('process.country'))->first();
        if ($country != null) {
            $group_country = GroupCountry::query()->where('_id', $country->group_country_id)->first();
            if ($group_country != null) {
                session()->put('process.group_code', $group_country->group_code);
            }
            session()->put('process.products_code', $country->products_code);
            session()->put('process.country_name', $country->name[session()->get('current_locale')]);

        }

        $products = $this->getProductsList(array_keys(session()->get('process.items')));
        $total = 0;
        $totalCC = 0;
        foreach ($products as $product) {
            $total += $product['price']->prixTTC * getCurrentQuantity($product['price']->ref);
            $totalCC += $product['price']->CC * getCurrentQuantity($product['price']->ref);
        }

        return view('frontend.layouts.partials.blocks.cart.shipping-address', compact('order_id', 'total', 'totalCC', 'city'));

    }


    public function buyWithin72Hours(Request $request)
    {

        usleep(500);
        if (session()->has('process.order_id')) {
            return redirect()->route('cart', session()->get('process.order_id'));
        }

        $temp = User::query()->where('email', session()->get('process.email'))->first();
        if ($temp == null) {
            $user = new User();
        } else {
            $user = $temp;
        }
        $user->name = session()->get('process.first_name') . ' ' . session()->get('process.last_name');
        $user->first_name = session()->get('process.first_name');
        $user->last_name = session()->get('process.last_name');
        $user->email = session()->get('process.email');
        $user->distributor_number = session()->get('process.distributor_number');
        $user->distributor_name = session()->get('process.distributor_name');
        $user->date_of_birth = session()->get('process.date_of_birth');
        $user->phone_number = session()->get('process.phone_number');
        $user->gender = session()->get('process.genderType');
        $user->city_id = session()->get('process.city');
        $user->country_id = session()->get('process.country');
        $user->street_address = session()->get('process.street_address');
        $user->additional_address = session()->get('process.additional_address');
        $user->zip_code = session()->get('process.zip_code');
        $user->roles()->attach(Role::query()->where('slug', 'l1')->first()->_id);
        $user->save();

        $user = User::query()->where('_id', $user->_id)->first();

        $items = session()->get('process.items');
        $products = $this->getProductsList(array_keys(session()->get('process.items')));
        $details = [];
        $total = 0;


        foreach ($products as $product) {
            $iTotal = $items[$product['price']->ref] * $product['price']->prixTTC;
            $details[] = [
                'product_id' => $product['price']->ref,
                'price' => $product['price']->prixTTC,
                'quantity' => $items[$product['price']->ref],
                'total' => $iTotal
            ];
            $total += $iTotal;
        }
        $order = new Order();
        $order->user_id = $user->_id;
        $order->distributor_number = session()->get('process.distributor_number');
        $order->total = $total;
        $order->language = session()->get('current_locale');
        $order->details = $details;
        $order->status = 'temporary';
        $order->save();

        session()->put('process.order_id', $order->_id);

        $url = 'https://webservice.foreverliving.fr/wsinscription.php';
        $ch = curl_init($url);
        $jsonData = array("action" => "droitDeParrainer",
            "id_parrain" => str_replace('-', '', $order->distributor_number),
        );
        $jsonDataEncoded = json_encode($jsonData);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $sponser = curl_exec($ch);
        $sponser = json_decode($sponser);


        if ($sponser != null && $sponser->success) {
            try {
                Mail::to($user->email)->send(new sponseeFirstNotification([
                    'id' => $order->_id,
                    's_first_name' => $sponser->parrain->prenom,
                    's_last_name' => $sponser->parrain->nom,
                    's_phone' => $sponser->parrain->tel,
                    's_mail' => $sponser->parrain->email,
                ]));
            } catch (\Exception $exception) {

            }

            try {
                Mail::to($sponser->parrain->email)->send(new sponserFirstNotification([
                    'id' => $order->_id,
                    's_first_name' => $user->first_name,
                    's_last_name' => $user->last_name,
                    's_address' => $user->street_address,
                    's_phone' => $user->phone_number,
                    's_mail' => $user->email,
                ]));

            } catch (\Exception $exception) {

            }
        }


        session()->remove('process');

        $page = Page::query()->where('slug', 'like', 'thank-you-72%')
            ->where('locales.code', session()->get('current_locale'))->first();

        return view('frontend.layouts.partials.blocks.cart.Thank-you72', compact('page'));


    }

    public function thankYou(Request $request)
    {
        return view('frontend.layouts.partials.blocks.cart.Thank-you');
    }

    public function switchLanguage($lang)
    {
        session()->put('current_locale', $lang);
        App::setLocale(session()->get('current_locale', $lang));
        return redirect()->route('welcome');
    }

    private function generatePIN($digits = 4)
    {
        $i = 0; //counter
        $pin = ""; //our default pin is blank.
        while ($i < $digits) {
            //generate a random number between 0 and 9.
            $pin .= mt_rand(0, 9);
            $i++;
        }
        return $pin;
    }

    public function selectCountry($id = null)
    {
        if ($id != null) {
            $country = Country::query()->where('_id', $id)->first();
            if ($country != null) {
                $needPostalCode = $country->has_postal == "1";
                $needIdNumber = $country->has_id_number == "1";
                $regions = Region::query()->where('country_id', $id)->get();
                return response()->json(['regions' => $regions, 'need_postal_code' => $needPostalCode, 'need_id_number' => $needIdNumber]);
            }
        }
        return response()->json(['message' => 'error']);
    }

    public function selectRegion($id = null)
    {
        if ($id != null) {
            $region = Region::query()->where('_id', $id)->first();
            if ($region != null) {
                $departments = Department::query()->where('region_id', $id)->get();
                return response()->json(['departments' => $departments]);
            }
        }
        return response()->json(['message' => 'error']);
    }

    public function selectDepartment($id = null)
    {
        if ($id != null) {
            $department = Department::query()->where('_id', $id)->first();
            if ($department != null) {
                $cities = City::query()->where('department_id', $id)->get();
                return response()->json(['cities' => $cities]);
            }
        }
        return response()->json(['message' => 'error']);
    }

    public function selectCity(Request $request)
    {
        $zip_code = $request->zip_code;
        $country_id = $request->id;
        $cities = City::query()->where('country_id', $country_id)->where('zip_code', $zip_code)->get();
//        dd($zip_code);
        if ($cities->isEmpty())
            return response()->json(['status' => false]);
        return response()->json(['status' => true, "response" => $cities]);
    }

    public function selectCityReview($country, $zip_code)
    {
        $cities = City::query()->where('country_id', $country)->where('zip_code', $zip_code)->get();
        if ($cities->isEmpty())
            return response()->json(['status' => false]);
        return response()->json(['status' => true, "response" => $cities]);
    }

    public function confirmPayment(Request $request)
    {


        return response()->json(['status' => 'success']);
    }

    public function successPayment(Request $request, $id)
    {
        $payment = $request->all();
        $sign = $this->getSignature($request->all(), true);


        if ($request->signature != $sign || $request->vads_result != "00") {
            return response()->json(['status' => 'failed']);
        }
        $customerReview = CustomerReview::query()->where('numOrder', $request->vads_order_id)->orWhere('numOrder', (int)$request->vads_order_id)->first();
        $customerReview->status = 'paid';
        $customerReview->save();
        $result = $this->thirdPartyFunc('POST', 'finCmde', [
            "flag" => $payment['vads_order_info2'],
            "env" => $payment['vads_order_info3'],
            'numOrder' => $payment['vads_order_id'],
            'typePaiement' => 'CB',
            'infoPaiement' => [
                'responseCode' => $payment['vads_result'],
                'transactionId' => $payment['vads_trans_id'] . "",
                'authorisationId' => $payment['vads_auth_number'],
                'paymentCertificate' => $payment['vads_payment_certificate'],
                'paymentDate' => substr($payment['vads_trans_date'], 0, 8),
                'paymentTime' => substr($payment['vads_trans_date'], 8, 4),
                'amount' => $payment['vads_amount'],
                'cardNumber' => $payment['vads_card_number']
            ]
        ]);
        $customerReview->status = 'paid';
        $customerReview->save();
        return view('frontend.thank-you-customer', ['id' => $id]);
    }

    public function failPayment()
    {
        return response(['status' => 'error']);

    }

    public function checkPoint(Request $request, $id)
    {

        $customerReview = CustomerReview::find($id);
        $country = Country::query()->where('country_code', $request->pays)->first();
        $array = [
            "flag" => 'CLI',
            'env' => $country->products_code,
            "typLiv" => $country->product_type,
            "numOrder" => $customerReview->numOrder,
            "numPtRelais" => '',
            "typePtRelais" => $request->idTnt,
            "nameLiv" => $request->ad1liv,
            "addressLiv" => $request->ad1liv,
            "additionalAddressLiv" => $request->ad2liv,
            "zipCodeLiv" => $request->cpliv,
            "infosLiv" => '',
            "emailLiv" => $customerReview->client != null ? $customerReview->client['email'] : '',
            "phoneLiv" => $customerReview->client != null ? $customerReview->client['phoneNumber'] : '',
            "cityLiv" => $request->villiv,
            "countryLiv" => $request->pays,
            "gareLiv" => '',
            "modeLiv" => '',
            "codeCity" => '',
            "prestataire" => '',

        ];
        $customerReview->shipping_address = $array;
        $customerReview->save();
        echo '<span style="font-family: \'Roboto\';font-size: 22px">' . __('front.selected_check_point') . '</span><br/><br/>';


        foreach ($request->all() as $k => $v) {
            echo ' <input type="hidden" id="' . $k . '" value="' . $v . '"/>';
            if ($k == 'stringInformations') {
                echo '<span style="font-family: \'Roboto\';">' . $v . '</span><br/>';
            }
            if ($k == 'order') {
                echo ' <input type="hidden" id="shipping_order" value="' . $v . '"/>';
            }
        }
        return;
    }

    public function saveAddress(Request $request, $id)
    {

        $customerReview = CustomerReview::find($id);
        $country = Country::query()->where('country_code', $request->shipping_country)->first();
        $array = [
            "flag" => 'CLI',
            'env' => $country->products_code,
            "typLiv" => $country->product_type,
            "numOrder" => $customerReview->numOrder,
            "numPtRelais" => '',
            "typePtRelais" => 'CHRONO2',
            "nameLiv" => $request->shipping_title,
            "addressLiv" => $request->shipping_street_address,
            "additionalAddressLiv" => $request->shipping_additional_address,
            "zipCodeLiv" => $request->shipping_zip_code,
            "infosLiv" => '',
            "emailLiv" => $request->shipping_email != null ? $request->shipping_email : '',
            "phoneLiv" => $request->shipping_phone_number != null ? $request->shipping_phone_number : '',
            "cityLiv" => $request->shipping_city,
            "countryLiv" => $request->shipping_country,
            "gareLiv" => '',
            "modeLiv" => '',
            "codeCity" => '',
            "prestataire" => '',

        ];
        $customerReview->shipping_address = $array;
        $customerReview->save();
        return response()->json(["success" => true, "msgErreur" => "", "numErreur" => 0], 200);

    }

    public function checkCheckPoint()
    {

        return session()->get('process.chrono', '');
    }

    public function checkPin(Request $request)
    {
        $temp = User::query()->where('email', session()->get('process.email'))->first();
        if ($temp == null) {
            return 'error';
        } else {
            $user = $temp;
        }
        if ($request->pin != $user->pin_code) {
            return 'error';
        } else {
            return 'success';
        }
    }

    public function checkCartServer($products)
    {
        $url = 'https://webservice.foreverliving.fr/wsinscription.php';
        $ch = curl_init($url);
        $jsonData = array(
            "action" => "ctrlPanier",
            "env" => session()->get('process.country_detail.products_code'),
            "region" => session()->get('process.country_detail.region_code'),
            "typliv" => session()->get('process.country_detail.product_type'),
            "numCommande" => 0
        );
        $details = [];
        foreach ($products as $d) {
            $details[] = [
                'ref' => $d['price']->ref,
                'qte' => getCurrentQuantity($d['price']->ref)
            ];
        }
        $jsonData['commande'] = $details;

        $jsonDataEncoded = json_encode($jsonData);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        $result = json_decode($result);
        if ($result->success) {
            return $result->numCommande;
        } else {
            return null;
        }
    }

    public function createServerOrder($order, $payment)
    {


        $user = User::query()->where('_id', $order->user_id)->first();
        $city = City::query()->where('_id', $user->city_id)->first();

        $address = $order->address;
        $country = Country::query()->where('_id', $user->country_id)->first();
        $scity = City::query()->where('_id', $address['city_id'])->first();
        $url = 'https://webservice.foreverliving.fr/wsinscription.php';
        $ch = curl_init($url);
        $jsonData = array("action" => "finCommandeInc",
            "env" => $country->products_code,
            "region" => $country->region_code,
            "typliv" => $country->product_type,
            "numCommande" => $order->code . "",
            "totalTTC" => number_format($order->total, 2),
            "typePaiement" => "CB",
            "infoFBO" => [
                "numParrain" => str_replace('-', '', $user->distributor_number),
                "civilite" => $user->gender == "male" ? "M" : "Mme",
                "nom" => trim($user->last_name),
                "prenom" => trim($user->first_name),
                "adresse" => $user->street_address,
                "complement" => $user->additional_address,
                "cp" => $user->zip_code,
                "ville" => $city->name[$order->language],
                "paysFBO" => $country->delivery_code,
                "wilaya" => '',
                "gouvernorat" => '',
                "quartier" => '',
                "dateNaissance" => Carbon::parse($user->date_of_birth)->format('d/m/Y'),
                'gsm' => $user->phone_number,
                'email' => $user->email,
                'cin' => '',
                'urlScan' => ''
            ],
            'livraison' => [
                'pointRelais' => $address['shipping_title'] == 'DOM' ? "" : $address['check_point'],
                'typePtRelais' => $address['shipping_title'] == 'DOM' ?
                    ($country->delivery_code == 'FRA' ? "CHRONO2" : "")
                    : $address['chrono_idTnt'],
                'nomLiv' => $address['name'],
                'ad1Liv' => $address['shipping_title'] == 'DOM' ? $address['street'] : $address['chrono_ad1liv'],
                'ad2liv' => $address['shipping_title'] == 'DOM' ? $address['additional_street'] : $address['chrono_ad2liv'],
                'cpLiv' => $address['shipping_title'] == 'DOM' ? $address['zip_code'] : $address['chrono_zip'],
                'infosLiv' => $address['shipping_title'] == 'DOM' ? '' : $address['chrono_info'],
                'gsmLiv' => $address['phone_number'],
                'emaiLiv' => $address['email'],
                'villeLiv' => $address['shipping_title'] == 'DOM' ? ($scity == null ? $address['city_id'] : $scity->name[$order->language]) : $address['chrono_city'],
                'paysLiv' => $country->delivery_code,
            ],
            'infoCB' => [
                "response_code" => $payment['vads_result'],
                "transaction_id" => $payment['vads_trans_id'] . "",
                "authorisation_id" => $payment['vads_auth_number'],
                "payment_certificate" => $payment['vads_payment_certificate'],
                "payment_date" => substr($payment['vads_trans_date'], 0, 8),
                "payment_time" => substr($payment['vads_trans_date'], 8, 4),
                "amount" => $payment['vads_amount'],
                "card_number" => $payment['vads_card_number']
            ]
        );

        $detail = [];

        foreach ($order->details as $d) {
            $detail[] = [
                'ref' => $d['product_id'],
                'qte' => $d['quantity']
            ];
        }
        if ($address['check_point'] != '' || $address['shipping_reference']) {
            $detail[] = [
                'ref' => $address['shipping_reference'],
                'qte' => 1
            ];
        }


        $jsonData['commande'] = $detail;

        $jsonDataEncoded = json_encode($jsonData);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $res = curl_exec($ch);
        $result = json_decode($res, true);

        return array("action" => "recFBO",
            "env" => $country->products_code,
            "numParrain" => str_replace('-', '', $user->distributor_number),
            "numCommande" => $order->code
        );


//        }

    }

    public function getCustomerReview(Request $request, $id)
    {
        $customerReview = CustomerReview::query()
            ->where('_id', $id)
            ->where('status', '!=', 'paid')
            ->where('status', '!=', 'expired')
            ->where('created_at', '>', Carbon::now()->addDays(-1))
            ->first();
        $order = null;
        if ($customerReview != null) {
            $id = $customerReview->numOrder;
            $order = $this->thirdPartyFunc('POST', 'getPanier', [
                "flag" => "CLI",
                "env" => "FRA",
                "numOrder" => $id,
            ]);
            if (!$order->success) {
                $region = Country::query()->where('country_code', 'FRA')->first()->region_code;
                $params = [
                    "numClient" => 0,
                    "numFBO" => $customerReview->numFBO,
                    "env" => "FRA",
                    "region" => $region,
                    "typLiv" => "P",
                    "rate" => 0,
                    "origine" => "link",
                    "id_vente" => $id,
                ];
                $orderClient = $this->thirdPartyFunc('POST','cmdeInviteValid', $params);
                if ($orderClient->success) {
                    $numOrder = $orderClient->id_vente;
                    $customerReview->numOrder=$numOrder;
                    $customerReview->save();

                   $newProducts= $customerReview->products;
                  
                    foreach ($newProducts as $product) {
                
                            $result = $this->thirdPartyFunc('POST', 'ajouteLigneCmde', [
                                "flag" => 'CLI',
                                "num" => $customerReview->numClient,
                                "env" => "FRA",
                                "numOrder" => $customerReview->numOrder,
                                "qte" => $product['qte'],
                                "ref" => $product['ref'],
                                "perso" => "",
                            ]);
                        
                      
                    }


                    $order = $this->thirdPartyFunc('POST', 'getPanier', [
                        "flag" => "CLI",
                        "env" => "FRA",
                        "numOrder" => $numOrder,
                    ]);
                }
            }
        }
        $redirect = 'shipping';
        return view('frontend.customer-review-order', ['customerReview' => $customerReview, 'order' => $order, 'redirect' => $redirect]);

    }

    public function getCustomerReviewShipping(Request $request, $id)
    {
        $customerReview = CustomerReview::query()
            ->where('_id', $id)
            ->where('status', '!=', 'paid')
            ->where('status', '!=', 'expired')
            ->where('created_at', '>', Carbon::now()->addDays(-1))
            ->first();
        $status = Status::query()->where('slug', 'active')->first();
        $countries = Country::query()->where('status_id', $status->_id)->orderBy('order', 'asc')->get();
        $order = null;
        $client = null;
        if ($customerReview != null) {
            $id = $customerReview->numOrder;
            $client = $customerReview->client;
            $order = $this->thirdPartyFunc('POST', 'getPanier', [
                "flag" => "CLI",
                "env" => "FRA",
                "numOrder" => $id,
            ]);
        }
        if ($client == null) {
          
            $client = [
                'firstName' => '',
                'lastName' => '',
                'country' => '',
                'zipCode' => '',
                'city' => '',
                'phoneNumber' => '',
                'additionalAddress' => '',
                'address' => '',
                'email' =>'',
            ];
        }
        return view('frontend.shipping-address',
            ['customerReview' => $customerReview,
                'order' => $order, 'client' => $client, 'countries' => $countries]);


    }

    public function getCustomerReviewCheckout(Request $request, $id)
    {
        $customerReview = CustomerReview::query()
            ->where('_id', $id)
            ->where('status', '!=', 'paid')
            ->where('status', '!=', 'expired')
            ->where('created_at', '>', Carbon::now()->addDays(-1))
            ->first();
        $id = $customerReview->numOrder;
        $shipping = $customerReview->shipping_address;
       
        $result = $this->thirdPartyFunc('PUT', 'setLivraison', $shipping);
        $order = $this->thirdPartyFunc('POST', 'getPanier', [
            "flag" => "CLI",
            "env" => "FRA",
            "numOrder" => $id,
        ]);
        $result1 = $this->thirdPartyFunc('POST', 'getPromo', [
            "flag" => 'CLI',
            "env" => "FRA",
            "numOrder" => $id,
            "isForClient" => '1',
            "num" => $customerReview->numClient,
        ]);
        $result = $this->thirdPartyFunc('POST', 'ctrlPanier', [
            "flag" => "CLI",
            "env" => "FRA",
            "numOrder" => $id,
        ]);

        $order = $this->thirdPartyFunc('POST', 'getPanier', [
            "flag" => "CLI",
            "env" => "FRA",
            "numOrder" => $id,
        ]);
        $customerReview->shipping_cost = $order->panierHead[0]->fraisLivraisonTTC;
        $customerReview->save();
        $total=0;

        if (isset($order->panierDetail) && $order->panierDetail != null){
            foreach ($order->panierDetail as $product){
                if ($product->typeProduit != 'R'){
                    $total += $product->qte  * $product->prixTTC;
                }else{
                    $total += $product->prixTTC;
                }
                 
            }
        }
        $customerReview->client_total=$total;
        $customerReview->save();

        $redirect = 'paymment';
        return view('frontend.customer-review-order', ['customerReview' => $customerReview, 'order' => $order, 'redirect' => $redirect]);

    }


    public function blockAllOldCustomerReview(Request $request)
    {
        $customerReview = CustomerReview::query()
            ->where('status', '!=', 'paid')
            ->where('status', '!=', 'expired')
            ->get();
        if ($customerReview != null) {
            foreach ($customerReview as $c) {
                $c->status = 'expired';
                $c->save();
            }
        }
        return response()->json(['numberOfTokens' => count($customerReview)], 200);


    }

    public function termsAndConditions(Request $request)
    {
        $page = page::query()->where('slug', 'cgv-client')->orWhere('slug', 'cgv-cli')->first();

        return view('frontend.terms', ['page' => $page]);

    }

    public function payment(Request $request, $id)
    {
        $customerReview = CustomerReview::find($id);
        $result = $this->thirdPartyFunc('POST', 'valideCmde', [
            "flag" => "CLI",
            "env" => 'FRA',
            "numOrder" => $customerReview->numOrder,
        ]);

        $vads_site_id = '16173012';
        $bank_url = env('API_CLIENT_BANK_URL');
        $params = [
            'vads_action_mode' => 'INTERACTIVE',
            'vads_amount' => ($customerReview->client_total) * 100,
            'vads_available_languages' => 'fr;en;de;es;it;pt;nl;sv',
            'vads_ctx_mode' => env('CTX_MODE'),
            'vads_currency' => '978',
            'vads_cust_id' => $customerReview->numClient,
            'vads_order_id' => $result->numOrder,
            'vads_order_info' => 'CMD',
            'vads_order_info2' => strtoupper('CLI'),
            'vads_order_info3' => $customerReview->country['products_code'],
            'vads_page_action' => 'PAYMENT',
            'vads_payment_config' => 'SINGLE',
            'vads_return_mode' => 'GET',
            'vads_site_id' => $vads_site_id,
            'vads_trans_date' => Carbon::now()->format('YmdHis'),
            'vads_trans_id' => $result->transactionId,
            'vads_url_cancel' => route('customer-review', ['id' => $customerReview->_id]),
            'vads_url_error' => route('customer-review', ['id' => $customerReview->_id]) . '#error',
            'vads_url_referral' => route('payment.process'),
            'vads_url_refused' => route('customer-review', ['id' => $customerReview->_id]) . '#error',
            'vads_url_success' => route('payment.success', ['id' => $customerReview->numOrder]),
            'vads_shop_url' => $bank_url,
            'vads_validation_mode' => '0',
            'vads_version' => 'V2',
            'vads_firstName' => $customerReview->client['firstName'],
            'vads_lastName' => $customerReview->client['lastName'],
            'vads_email' => $customerReview->client['email'],

        ];

        // dd($params);
        $signature = $this->getSignature($params, true);
        $params['signature'] = $signature;
        $token = Crypt::encryptString(json_encode($params));

        return redirect(route('payment.url', ['token' => $token]));

    }

    function getSignature($params, $isClient)
    {
        /**
         * Function that computes the signature.
         * $params : table containing the fields to send in the payment form.
         * $key : TEST or PRODUCTION key
         */
        //Initialization of the variable that will contain the string to encrypt
        $contenu_signature = "";
        // Sorting fields alphabetically
        ksort($params);
        foreach ($params as $name => $value) {
            // Recovery of vads_ fields
            if (substr($name, 0, 5) == 'vads_') {
                // Concatenation with "+"
                $contenu_signature .= $value . "+";
            }
        }
        // Adding the key at the end
        // $contenu_signature .= '2138633189841587';
        if ($isClient == true) {
            $contenu_signature .= env('CLIENT_MERCHANT_KEY');
        } else {
            $contenu_signature .= env('MERCHANT_KEY');
        }


        // Applying SHA-1 algorithm
        $signature = sha1($contenu_signature);
        return $signature;
    }

}
