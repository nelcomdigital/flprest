<?php

namespace App\Http\Controllers\CMS;

use App\Models\Locale;
use App\Models\Translation;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use League\Flysystem\Adapter\Local;
use Symfony\Component\Finder\Finder;


class TranslationController extends CmsController
{

    public function index(Request $request)
    {
        $model = new Translation();
        if ($request->ajax() && $request->has('table')) {
            if ($request->has('_qq') && $request->get('_qq') != '' ) {
                $translates = Translation::query()
                    ->where('key','like','%'.$request->get('_qq').'%');
                $locales = Locale::all();
                foreach ( $locales as $locale ){
                    $translates->orWhere('values.'.$locale->code.'.value', 'like','%'.$request->get('_qq').'%');
                }
                return datatables($translates->get())->make(true);
            }
            else
                return datatables(Translation::query())->make(true);
        }
        $missingTranslations = $this->findTranslations();
        return view('cms.translation.index', compact('model','missingTranslations'));

    }

    public function create(Request $request)
    {
        $locales = Locale::all();
        return ['view' => view('cms.translation.create', compact('locales'))->render(), 'title' => 'New Alias'];
    }

    public function store(Request $request)
    {
        $params = $request->except('_token', '_method');
        $translation = new Translation();
        $translation->key = $params['key'];
        $values = array();
        foreach ($params['value'] as $k => $v) {
            $values[$k] = array('value' => $v, 'status' => 0);
        }
        $translation->values = $values;
        $translation->save();
        if ( count($translation->translatable) > 0 ){
            $loc = Locale::query()->where('_id',$request->input('locales')[0])->first();
            $translations = array();
            foreach ($translation->translatable as $k){
                $translations[$loc->code][$k] = $params[$k];
            }
            $translation->translations = $translations;
            $translation->save();
        }
        return ['status' => 'success', 'module' => $translation];
    }

    public function show($id)
    {
        //TODO
    }

    public function edit($id)
    {
        $locales = Locale::all();
        $translation = dataHelper(Translation::class, [['_id', $id]], [], 0, [], [], false)->first();
        return ['view' => view('cms.translation.edit', compact( 'translation','locales'))->render(), 'title' => $translation->key];
    }

    public function update(Request $request, $id)
    {
        $params = $request->except('_token', '_method');
        $translation = Translation::query()->where('_id', $id)->first();;
        $translation->key = $params['key'];
        $values = array();
        foreach ($params['value'] as $k => $v) {
            if ( array_has($translation->values, $k) ){
                if( $translation->values[$k]['value'] == $v )
                    $status = $translation->values[$k]['status'];
                else
                    $status = 0;
                $values[$k] = array('value' => $v, 'status' => $status);
            }else
                $values[$k] = array('value' => $v, 'status' => 0);
        }
        $translation->values = $values;
        $translation->save();

        return ['status' => 'success', 'module' => $translation];
    }

    public function destroy($id)
    {
        $model = Translation::query()->where('_id', $id)->first();
        return $model->delete();
    }



    public function findTranslations($create = false)
    {

        $functions  = [
            'trans',
            'trans_choice',
            'Lang::get',
            'Lang::choice',
            'Lang::trans',
            'Lang::transChoice',
            '@lang',
            '@choice',
            '__',
            '$trans.get',
        ];

        $pattern =
            "[^\w|>]" .                          // Must not have an alphanum or _ or > before real method
            '(' . implode( '|', $functions ) . ')' .  // Must start with one of the functions
            "\(" .                               // Match opening parenthesis
            "[\'\"]" .                           // Match " or '
            '(' .                                // Start a new group to match:
            '[a-zA-Z0-9_-]+' .               // Must start with group
            "([.](?! )[^\1)]+)+" .             // Be followed by one or more items/keys
            ')' .                                // Close group
            "[\'\"]" .                           // Closing quote
            "[\),]";                            // Close parentheses or new parameter



        $finder = new Finder();
        $finder->in( base_path() )->exclude( 'storage' )->exclude('data')->exclude( 'vendor' )->name( '*.php' )->name( '*.twig' )->name( '*.vue' )->files();


        foreach ( $finder as $file ) {
            // Search the current file for the pattern
            if ( preg_match_all( "/$pattern/siU", $file->getContents(), $matches ) ) {

                foreach ( $matches[ 2 ] as $key ) {
                    if ( !( str_contains( $key, '::' ) && str_contains( $key, '.' ) )
                        || str_contains( $key, ' ' ) ) {
                        $stringKeys[] = $key;
                    }
                }
            }
        }
        // Remove duplicates
        $keys = array_unique( $stringKeys );


        if ( $create )
        foreach ( $keys as $key ) {
            $this->missingKey( $key );
        }

        // Return the number of found translations
        return count(  $keys  )- Translation::query()->count();
    }

    public function missingKey(  $key )
    {
        $translation = Translation::query()->where('key',$key)->get()->first();
        if ( $translation == null ){
            $translation = new Translation();
            $translation->key = $key;
            $translation->save();
        }

    }


    public function inject(){
        return $this->findTranslations(true);
    }

    public function export(){

        $translationsList = Translation::all();
        $locales = Locale::all();

        $translations = array();
        foreach ($locales as $locale ){
            $path      = resource_path('lang/'.$locale->code);

            if ( !file_exists( $path) ){
                mkdir($path , 0755);
            }

            foreach ($translationsList as $translation ){

                if (array_has($translation->values, $locale->code) && $translation->values[$locale->code] != null
                    && $translation->values[$locale->code]['value'] != null ){
                    $keys = explode('.',$translation->key);
                    $file = array_shift($keys);
                    $key = implode('.', $keys);
                    $translations[$file][$key] = $translation->values[$locale->code]['value'];
                }

            }

            foreach ($translations as $file => $keys ){
                $output = "<?php\n\nreturn " . var_export( $keys, true ) . ";" . \PHP_EOL;
                file_put_contents( $path.'/'.$file.'.php', $output );
            }

            $translations = array();
        }

    }



}
