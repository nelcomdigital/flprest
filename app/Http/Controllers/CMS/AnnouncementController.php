<?php

namespace App\Http\Controllers\CMS;

use App\Models\Announcement;
use App\Models\MobUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Jobs\SendFCMNotification;



class AnnouncementController extends Controller
{
   public function index(Request $request)
    {
        
        
        return view('cms.announcement.index', compact('model'));
    }

    public function create(Request $request)
    {
        //TODO
    }

    public function store(Request $request)
    {
        
        $params = $request->except('_token', '_method');

        // $users = MobUser::all();
        // foreach($users as $user){
            
        //     SendFCMNotification::dispatch([
        //         'to' => $user->num,
        //         'from' => null,
        //         'title' => $params['title'],
        //         'message' => $params['message'],
        //     ]);
        
        // }
        return ['status'=>'success'];

    }

    public function show($id)
    {
        //TODO
    }

    public function edit(Request $request, $id)
    {
        //TODO
    }

    public function update(Request $request, $id)
    {
        //TODO
    }

    public function destroy($id)
    {
        //TODO
    }

    public function up($id)
    {
        $this->parentUp(Announcement::find($id));
    }
    public function down($id)
    {
        $this->parentDown(Announcement::find($id));
    }

    public function translate($id)
    {
        //TODO
    }

    public function translateStore(Request $request, $id)
    {
            //TODO
    }
    
    public function deleteImage(Request $request, $id)
    {
        $module = Announcement::find($id);
        $image = $request->get('image');
        $module->$image = null;
        $module->save();

        return ['status'=>'Success'];
    }

}
