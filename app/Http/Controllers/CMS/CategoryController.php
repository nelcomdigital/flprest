<?php

namespace App\Http\Controllers\CMS;

use App\Models\Locale;
use App\Models\Country;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;


class CategoryController extends CmsController
{
    public function index(Request $request)
    {
        $model = new Category();
        if ($request->ajax() && $request->has('table')) {
            if ($request->has('filters')) {
                return datatables($model->filterSearch($request->input('filters')))->make(true);
            }
            return datatables(Category::query())->make(true);
            ;
        }

        return view('cms.category.index', compact('model'));
    }
    public function create(Request $request)
    {

        $modules = getModules();

        return ['view'=>view('cms.category.create', compact('modules'))->render(), 'title'=>'New Category'];
    }

    public function store(Request $request)
    {
        $params = $request->except('_token', '_method', 'locales', 'countries');
        $params['slug'] = Str::slug($request->input('name'));

        $category = new Category();
        foreach ($params as $key => $value) {
            $category->$key = $value;
        }
        $locales = $request->input('locales');
        foreach ($locales as $locale) {
            $category->locales()->associate(Locale::find($locale));
        }
        $countries = $request->input('countries');
        foreach ($countries as $country) {
            $category->countries()->associate(Country::find($country));
        }


        $category->save();


        return ['status'=>'success', 'category'=>$category];
    }

    public function show($id)
    {
        //TODO
    }

    public function edit($id)
    {
        $category = Category::query()->where('_id', $id)->first();
        $modules = getModules();
        return ['view'=>view('cms.category.edit', compact('category','modules'))->render(),
            'title'=>$category->name];
    }

    public function update(Request $request, $id)
    {
        $params = $request->except('_token', '_method','locales', 'countries');        
        $params['slug'] = Str::slug($request->input('name'));

        $category = Category::query()->where('_id', $id)->first();
        foreach ($params as $key => $value) {
            $category->$key = $value;
        }
        $locales = $request->input('locales');
        $category->locales()->delete();
        foreach ($locales as $locale) {
            $category->locales()->associate(Locale::find($locale));
        }
        $countries = $request->input('countries');
        $category->countries()->delete();
        foreach ($countries as $country) {
            $category->countries()->associate(Country::find($country));
        }

        $category->save();
        return ['status'=>'success', 'category'=>$category];
    }

    public function destroy($id)
    {
        $model = Category::query()->where('_id', $id)->first();
        return $model->delete();
    }

    public function up($id)
    {
        $this->parentUp(Category::find($id));
    }
    public function down($id)
    {
        $this->parentDown(Category::find($id));
    }

    public function translate($id)
    {
        //TODO
    }

    public function translateStore(Request $request, $id)
    {
        //TODO
    }
    
    public function deleteImage(Request $request, $id)
    {
        $module = Category::find($id);
        $image = $request->get('image');
        $module->$image = null;
        $module->save();

        return ['status'=>'Success'];
    }
}
