<?php

namespace App\Http\Controllers\CMS;

use App\Models\Favorite;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class FavoriteController extends Controller
{
   public function index(Request $request)
    {
        //TODO
    }

    public function create(Request $request)
    {
        //TODO
    }

    public function store(Request $request)
    {
        //TODO
    }

    public function show($id)
    {
        //TODO
    }

    public function edit(Request $request, $id)
    {
        //TODO
    }

    public function update(Request $request, $id)
    {
        //TODO
    }

    public function destroy($id)
    {
        //TODO
    }

    public function up($id)
    {
        $this->parentUp(Favorite::find($id));
    }
    public function down($id)
    {
        $this->parentDown(Favorite::find($id));
    }

    public function translate($id)
    {
        //TODO
    }

    public function translateStore(Request $request, $id)
    {
            //TODO
    }
    
    public function deleteImage(Request $request, $id)
    {
        $module = Favorite::find($id);
        $image = $request->get('image');
        $module->$image = null;
        $module->save();

        return ['status'=>'Success'];
    }

}
