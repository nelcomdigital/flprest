<?php

namespace App\Http\Controllers\CMS;

use App\Models\Country;
use App\Models\Domain;
use App\Models\Locale;
use App\Models\MainMenu;
use App\Models\Status;
use Illuminate\Http\Request;
use App\Models\Slider;
use Illuminate\Support\Str;


class SliderController extends CmsController
{

   public function index(Request $request)
    {
        $model = new Slider();
        if ( $request->ajax() && $request->has('table')){
            if($request->has('filters')){
                return datatables( $model->filterSearch($request->input('filters')) )->make(true);
            }
            return datatables(Slider::query())->make(true);;
        }

        return view('cms.slider.index',compact('model'));
    }
    public function create(Request $request)
    {
        $locales = Locale::all();
        $mainMenus = MainMenu::all();
        $statuses = Status::all();
        return ['view'=>view('cms.slider.create', compact('statuses','mainMenus', 'locales'))->render(), 'title'=>'New Slider'];

    }

    public function store(Request $request)
    {
        $params = $request->except('_token', '_method');
        $params['slug'] = Str::slug($request->input('name'));
        $params['is_active'] = $request->has('is_active')? '1':'0';

        $slider = new Slider();
        foreach($params as $key => $value){
            $slider->$key = $value;
        }
        $countries = $request->input('countries');
        foreach($countries as $country){
            $slider->countries()->associate(Country::find($country));
        }
        $slider->save();

        $slider->image = $this->copyFileToPublic($request->input('image'));
        $slider->image_mobile = $this->copyFileToPublic($request->input('image_mobile'));
        $slider->save();
        return ['status'=>'success', 'slider'=>$slider];

    }

    public function show($id)
    {
        //TODO
    }

    public function edit($id)
    {
        $locales = Locale::all();
        $statuses = Status::all();
        $mainMenus = MainMenu::all();
        $slider = Slider::query()->find($id);
        return ['view'=>view('cms.slider.edit', compact( 'statuses','mainMenus', 'slider', 'locales'))->render(),
            'title'=>$slider->name];
    }

    public function update(Request $request, $id)
    {
        $params = $request->except('_token', '_method');
        $params['slug'] = Str::slug($request->input('name'));
        $params['is_active'] = $request->has('is_active')? '1':'0';
        $slider = Slider::query()->where('_id', $id)->first();
        foreach($params as $key => $value){
            $slider->$key = $value;
        }

        $countries = $request->input('countries');
        $slider->countries()->delete();
        foreach($countries as $country){
            $slider->countries()->associate(Country::find($country));
        }
        $slider->save();
        $slider->image = $this->copyFileToPublic($request->input('image'));
        $slider->image_mobile = $this->copyFileToPublic($request->input('image_mobile'));
        $slider->save();
        return ['status'=>'success', 'slider'=>$slider];
    }

    public function destroy($id)
    {
        $model = Slider::query()->where('_id', $id)->first();
        return $model->delete();
    }

    public function up($id)
    {
        $this->parentUp(Slider::find($id));
    }
    public function down($id)
    {
        $this->parentDown(Slider::find($id));
    }
}
