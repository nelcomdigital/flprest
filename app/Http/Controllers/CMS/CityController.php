<?php

namespace App\Http\Controllers\CMS;

use App\Models\City;
use App\Models\Country;
use App\Models\Department;
use App\Models\Locale;
use App\Models\ProductCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use function GuzzleHttp\Promise\all;


class CityController extends Controller
{
   public function index(Request $request)
    {
//        $model = new City();
//        if ($request->ajax() && $request->has('table')) {
//            if ($request->has('filters')) {
//                return datatables($model->filterSearch($request->input('filters')))->make(true);
//            }
//            return datatables(City::query())->make(true);
//        }
//        return view('cms.city.index', compact('model'));
       $model = new City();
       if ($request->ajax() && $request->has('table')) {

           if ($request->has('_qq') && $request->get('_qq') != '' ) {
               $cities = City::query()->with('country')
                   ->where('zip_code','like','%'.$request->get('_qq').'%');
               $locales = Locale::all();
               foreach ( $locales as $locale ){
                   $cities->orWhere('values.'.$locale->code.'.value', 'like','%'.$request->get('_qq').'%');
               }
               return datatables($cities->get())->make(true);

               //try now ok up

           }
           else

               return datatables(City::query()->with('country'))->make(true);
       }
//       $missingTranslations = $this->findTranslations();
       return view('cms.city.index', compact('model'));
    }


    public function create(Request $request)
    {
        $city = City::all();
        $departments = Department::all();
        $countries = Country::all();

        return ['view' => view('cms.city.create', compact('city','departments','countries'))->render(), 'title' => 'New City'];
    }

    public function store(Request $request)
    {
        $params = $request->except('_token', '_method');
        foreach ($request->get('name') as $k => $value) {
            $slug [$k] = Str::slug($value);
        }
        $city = new City();
        $city->cargo_tax = $request->input('cargo_tax');
        $city->iva_tax = $request->input('iva_tax');
        $city->name = $request->get('name');
        $city->slug = $slug;
        $city->department()->associate($request->get('department_id'));
        $city->country()->associate($request->get('country_id'));
        if ($request->get('iva_tax') == "1"){
            $city->iva_amount = $request->get('iva_amount');
        } else {
            $city->iva_amount = "";
        }
        if ($request->get('cargo_tax') == "1"){
            $city->cargo_amount = $request->get('cargo_amount');
        } else{
            $city->cargo_amount = "";
        }
        $city->zip_code = $request->get('zip_code');
        //parent
        $city->save();
        return ['status' => 'success', 'city' => $city];
    }

    public function show($id)
    {
        //TODO
    }

    public function edit(Request $request, $id)
    {
        $city = City::query()->find($id);
        $departments = Department::all();
        $countries = Country::all();

        return ['view' => view('cms.city.edit', compact('city', 'departments','countries'))->render(),
            'title' => is_string($city->name) ?: $city->name[session()->get('current_locale')]];
    }

    public function update(Request $request, $id)
    {
        $params = $request->except('_token', '_method', 'country_id');
        foreach ($request->get('name') as $k => $value) {
            $slug [$k] = Str::slug($value);
        }
        $city = City::query()->where('_id', $id)->first();
        $city->cargo_tax = $request->input('cargo_tax');
        $city->iva_tax = $request->input('iva_tax');
        $city->name = $request->get('name');
        $city->slug = $slug;
        $city->department()->associate($request->get('department_id'));
        $city->country()->associate($request->get('country_id'));
        if ($request->get('iva_tax') == "1"){
            $city->iva_amount = $request->get('iva_amount');
        } else {
            $city->iva_amount = "";
        }
        if ($request->get('cargo_tax') == "1"){
            $city->cargo_amount = $request->get('cargo_amount');
        } else{
            $city->cargo_amount = "";
        }
        $city->zip_code = $request->get('zip_code');
        //parent
        $city->save();

        return ['status' => 'success', 'city-category' => $city];
    }

    public function destroy($id)
    {
        $model = City::query()->where('_id', $id)->first();
        return $model->delete();
    }

    public function up($id)
    {
        $this->parentUp(City::find($id));
    }
    public function down($id)
    {
        $this->parentDown(City::find($id));
    }

    public function translate($id)
    {
        //TODO
    }

    public function translateStore(Request $request, $id)
    {
            //TODO
    }

    public function deleteImage(Request $request, $id)
    {
        $module = City::find($id);
        $image = $request->get('image');
        $module->$image = null;
        $module->save();

        return ['status'=>'Success'];
    }

}
