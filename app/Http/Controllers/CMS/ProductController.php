<?php

namespace App\Http\Controllers\CMS;

use App\Models\Category;
use App\Models\Currency;
use App\Models\LinkType;
use App\Models\Locale;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\Role;
use App\Models\Tag;
use App\Models\Translation;
use Illuminate\Http\Request;
use Illuminate\Support\Str;


class ProductController extends CmsController
{
    public function index(Request $request)
    {
        $model = new Product();
        if ($request->ajax() && $request->has('table')) {
            $products = Product::query()->with('category');
            $locales = Locale::all();
            if ( $request->has('filters') && array_has($request->get('filters'),'categories_id') &&  $request->input('filters')['categories_id'] != ''){
                $products->Where('category_id',$request->input('filters')['categories_id']);
            }
            if ($request->has('_qq') && $request->get('_qq') != '' ) {
                $products->where('reference', 'like','%'.$request->get('_qq').'%');
                $req = $request->get('_qq');
                foreach ( $locales as $k =>$locale ){
                    $products->orWhere('name.'.$locale->code, 'like','%'.$req.'%');
                }

            }
            return datatables($products->get())->make(true);

        }
        return view('cms.product.index', compact('model'));
    }

    public function create(Request $request)
    {
        $productCategories = ProductCategory::all();
        $roles = Role::query()->whereNotIn('slug',['super-admin','admin'])->get();
        $currencies = Currency::all();
//        $statuses = Status::all();
        return ['view' => view('cms.product.create', compact('productCategories','roles','currencies'))->render(), 'title' => 'New Product'];
    }

    public function store(Request $request)
    {
        $params = $request->except('_token', '_method', 'productCategories','values');

        foreach ($request->get('name') as $k => $value) {
            $slug [$k] = Str::slug($value);
        }
        $product = new Product();
        $product->name = $request->get('name');
//        $product->short_description = $request->get('short_description');
        $product->description = $request->get('description');
        $product->ingredients = $request->get('ingredients');
        $product->instructions = $request->get('instructions');
//        $product->advantages = $request->get('advantages');
        $product->image = $request->get('image');
        $product->reference = $request->get('reference');
        $product->category_id = $request->input('category_id');
        $product->currency_id = $request->input('currency_id');
        $product->is_pack = $request->input('is_pack');
        $product->is_active = $request->input('is_active');
        $product->links = $request->input('values');
        $product->quantity = $request->get('quantity');
        //Price list
        $roles = $request->input('roles');
        $prices = $request->input('prices');
        $price_list = array();
        if($roles != null){
            foreach($roles as $key => $role){
                $price_list[] = [
                    'role' => Role::find($role)->toArray(),
                    'price' => $prices[$key],
                ];
            }
        }
        $product->price_list = $price_list;

        $product->save();
        return ['status' => 'success', 'product' => $product];
    }

    public function show($id)
    {
        //TODO
    }

    public function edit($id)
    {
        $productCategories = ProductCategory::query()->get();
        $product = Product::query()->where('_id',$id)->first();
        $currencies = Currency::all();
        $roles = Role::all();
        return ['view' => view('cms.product.edit', compact('productCategories','roles','currencies','product'))->render(),
            'title' =>$product->name[session()->get('current_locale')]];
    }

    public function update(Request $request, $id)
    {
        $params = $request->except('_token','_method', 'roles', 'prices', 'values');
        foreach ($request->get('name') as $k => $value) {
            $slug [$k] = Str::slug($value);
        }
        $product = Product::query()->where('_id', $id)->first();
        $product->name = $request->get('name');
//        $product->short_description = $request->get('short_description');
        $product->description = $request->get('description');
        $product->ingredients = $request->get('ingredients');
        $product->instructions = $request->get('instructions');
//        $product->advantages = $request->get('advantages');
        $product->image = $request->get('image');
        $product->reference = $request->get('reference');
        $product->category_id = $request->input('category_id');
        $product->currency_id = $request->input('currency_id');
        $product->is_pack = $request->input('is_pack');
        $product->is_active = $request->input('is_active');
        $product->links = $request->input('values');
        $product->quantity = $request->get('quantity');
        //Price list
        $roles = $request->input('roles');
        $prices = $request->input('prices');
        $price_list = array();
        if($roles != null){
            foreach($roles as $key => $role){
                $price_list[] = [
                    'role' => Role::find($role)->toArray(),
                    'price' => $prices[$key],
                ];
            }
        }
        $product->price_list = $price_list;
        $product->save();

        return ['status' => 'success', 'product' => $product];
    }

    public function destroy($id)
    {
        $model = Product::query()->where('_id', $id)->first();
        return $model->delete();
    }

    public function up($id)
    {
        $this->parentUp(Product::find($id));
    }

    public function down($id)
    {
        $this->parentDown(Product::find($id));
    }

    public function deleteImage()
    {

    }
}
