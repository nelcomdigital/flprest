<?php

namespace App\Http\Controllers\CMS;

use App\Models\Currency;
use App\Models\Locale;
use Illuminate\Http\Request;
use Illuminate\Support\Str;


class CurrencyController extends CmsController
{

   public function index(Request $request)
    {
        $model = new Currency();
        if ( $request->ajax() && $request->has('table')){
            if($request->has('filters')){
                return datatables( $model->filterSearch($request->input('filters')) )->make(true);
            }
            return datatables(Currency::query())->make(true);;
        }

        return view('cms.currency.index',compact('model'));
    }

    public function create(Request $request)
    {

        return ['view'=>view('cms.currency.create')->render(), 'title'=>'New Currency'];
    }


    public function store(Request $request)
    {
        $params = $request->except('_token', '_method');
        $params['slug'] = Str::slug($request->input('name'));
        $currency = new Currency();
        foreach($params as $key => $value){
            $currency->$key = $value;
        }
        $currency->save();

        return ['status'=>'success', 'currency'=>$currency];
    }

    public function show($id)
    {
        //TODO
    }

    public function edit($id)
    {
        $currency = Currency::query()->where('_id', $id)->first();
        return ['view'=>view('cms.currency.edit', compact('currency'))->render(), 'title'=>$currency->name];
    }

    public function update(Request $request, $id)
    {
        $params = $request->except('_token', '_method');
        $params['slug'] = Str::slug($request->input('name'));

        $currency = Currency::query()->where('_id', $id)->first();
        foreach($params as $key => $value){
            $currency->$key = $value;
        }
        $currency->save();
        return ['status'=>'success', 'currency'=>$currency];
    }

    public function destroy($id)
    {
        $model = Currency::query()->where('_id', $id)->first();
        return $model->delete();
    }

    public function up($id)
    {
        $this->parentUp(Currency::find($id));
    }
    public function down($id)
    {
        $this->parentDown(Currency::find($id));
    }

}
