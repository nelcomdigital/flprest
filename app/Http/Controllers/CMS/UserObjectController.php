<?php

namespace App\Http\Controllers\CMS;

use App\Models\UserObject;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use App\User;
use App\Models\Reminder;

class UserObjectController extends CmsController
{
    public function index(Request $request)
    {
        $model = new UserObject();
        if ($request->ajax() && $request->has('table')) {
            if ($request->has('filters')) {
                return datatables($model->filterSearch($request->input('filters')))->make(true);
            }
            return datatables(UserObject::query())->make(true);
            ;
        }

        return view('cms.user-object.index', compact('model'));
    }
    public function create(Request $request)
    {

        return ['view'=>view('cms.user-object.create', compact(''))->render(), 'title'=>'New UserObject'];
    }

    public function store(Request $request)
    {
        $params = $request->except('_token', '_method', 'locales');
        $userObject = new UserObject();
        foreach ($params as $key => $value) {
            $userObject->$key = $value;
        }

        $userObject->save();


        return ['status'=>'success', 'userObject'=>$userObject];
    }

    public function show($id)
    {
        //TODO
    }

    public function edit($id)
    {
        $userObject = UserObject::query()->where('_id', $id)->first();
        return ['view'=>view('cms.user-object.edit', compact('userObject'))->render(),
            'title'=>$userObject->name];
    }

    public function update(Request $request, $id)
    {
        $params = $request->except('_token', '_method');

        $userObject = UserObject::query()->where('_id', $id)->first();
        foreach ($params as $key => $value) {
            $userObject->$key = $value;
        }
        $userObject->save();
        return ['status'=>'success', 'userObject'=>$userObject];
    }

    public function destroy($id)
    {
        $model = UserObject::query()->where('_id', $id)->first();
        return $model->delete();
    }

    public function up($id)
    {
        $this->parentUp(UserObject::find($id));
    }
    public function down($id)
    {
        $this->parentDown(UserObject::find($id));
    }

    public function translate($id)
    {
        //TODO
    }

    public function translateStore(Request $request, $id)
    {
        //TODO
    }
    
    public function deleteImage(Request $request, $id)
    {
        $module = UserObject::find($id);
        $image = $request->get('image');
        $module->$image = null;
        $module->save();

        return ['status'=>'Success'];
    }
}
