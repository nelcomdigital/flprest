<?php

namespace App\Http\Controllers\CMS;

use App\Models\Country;
use App\Models\GroupCountry;
use App\Models\Region;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use MongoDB\Operation\Count;


class RegionController extends CmsController
{

   public function index(Request $request)
    {
        $model = new Region();
        if ( $request->ajax() && $request->has('table')){
            if($request->has('filters')){
                return datatables( $model->filterSearch($request->input('filters')) )->make(true);
            }
            return datatables(Region::query())->make(true);;
        }

        return view('cms.region.index',compact('model'));
    }

    public function create(Request $request)
    {

        $countries = Country::all();
        return ['view'=>view('cms.region.create',compact('countries'))->render(), 'title'=>'New Region'];
    }


    public function store(Request $request)
    {
        foreach ($request->get('name') as $k => $value) {
            $slug [$k] = Str::slug($value);
        }
        $region = new Region();
        $region->name = $request->input('name');
        $region->slug = $slug;
        $region->country()->associate($request->input('country_id'));
        $region->save();



        return ['status'=>'success', 'region'=>$region];
    }

    public function show($id)
    {
        //TODO
    }

    public function edit($id)
    {
        $countries = Country::all();
        $region = Region::query()->where('_id', $id)->first();
        return ['view'=>view('cms.region.edit', compact('region','countries'))->render(),
            'title'=>$region->name[session()->get('current_locale')]];
    }

    public function update(Request $request, $id)
    {
        foreach ($request->get('name') as $k => $value) {
            $slug [$k] = Str::slug($value);
        }
        $region = Region::query()->where('_id', $id)->first();
        $region->name = $request->input('name');
        $region->slug = $slug;
        $region->country()->associate($request->input('country_id'));
        $region->save();

        return ['status'=>'success', 'region'=>$region];
    }

    public function destroy($id)
    {
        $model = Region::query()->where('_id', $id)->first();
        return $model->delete();
    }

    public function up($id)
    {
        $this->parentUp(Region::find($id));
    }
    public function down($id)
    {
        $this->parentDown(Region::find($id));
    }

}
