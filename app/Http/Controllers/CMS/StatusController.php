<?php

namespace App\Http\Controllers\CMS;

use App\Models\Locale;
use App\Models\Status;
use Illuminate\Http\Request;
use Illuminate\Support\Str;


class StatusController extends CmsController
{

   public function index(Request $request)
   {
        $model = new Status();
        if ( $request->ajax() && $request->has('table')){
            if($request->has('filters')){
                return datatables( $model->filterSearch($request->input('filters')) )->make(true);
            }
            return datatables(Status::query())->make(true);;

        }

        return view('cms.status.index',compact('model'));
    }
    public function create(Request $request)
    {
        return ['view'=>view('cms.status.create')->render(), 'title'=>'New Status'];
    }

    public function store(Request $request)
    {
        $params = $request->except('_token','locales', 'domains', 'countries');
        $params['slug'] = Str::slug($request->input('name'));
        $status = new Status();
        foreach($params as $key => $value){
            $status->$key = $value;
        }
        $status->save();
        return ['status'=>'success', 'module'=>$status];
    }

    public function show($id)
    {
        //TODO
    }
    public function edit($id)
    {
        $status = Status::query()->find($id);
        return ['view'=>view('cms.status.edit', compact( 'status'))->render(), 'title'=>$status->name];
    }

    public function update(Request $request, $id)
    {
        $params = $request->except('_token', '_method');
        $params['slug'] = Str::slug($request->input('name'));
        $status = Status::query()->where('_id', $id)->first();
        foreach($params as $key => $value){
            $status->$key = $value;
        }
        $status->save();

        return ['status'=>'success', 'module'=>$status];
    }

    public function destroy($id)
    {
        $model = Status::query()->where('_id', $id)->first();
        return $model->delete();
    }

    public function up($id)
    {
        $this->parentUp(Status::find($id));
    }
    public function down($id)
    {
        $this->parentDown(Status::find($id));
    }
}
