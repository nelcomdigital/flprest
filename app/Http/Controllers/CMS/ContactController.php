<?php

namespace App\Http\Controllers\CMS;

use App\Models\Contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use App\User;
use App\Models\Reminder;


class ContactController extends CmsController
{
   public function index(Request $request)
    {
        $model = new Contact();
        if ( $request->ajax() && $request->has('table')){
            if($request->has('filters')){
                return datatables( $model->filterSearch($request->input('filters')) )->make(true);
            }
            return datatables(Contact::query())->make(true);;
        }

        return view('cms.contact.index',compact('model'));
    }
    public function create(Request $request)
    {
        $users = User::query()->get();
        $reminders =Reminder::query()->get();

        return ['view'=>view('cms.contact.create',compact('users','reminders'))->render(), 'title'=>'New Contact'];
    }

    public function store(Request $request)
    {
        $params = $request->except('_token', '_method', 'locales', 'texts');
        $params['slug'] = Str::slug($request->input('email'));
        $contact = new Contact();
        foreach ($params as $key => $value) {
            $contact->$key = $value;
        }

        $contact->save();


        return ['status'=>'success', 'contact'=>$contact];

    }

    public function show($id)
    {
        //TODO
    }

    public function edit($id)
    {
       $users = User::query()->get();
       $contact = Contact::query()->where('_id', $id)->first();
       $reminders =Reminder::query()->get();
        return ['view'=>view('cms.contact.edit', compact('contact','users','reminders'))->render(),
            'title'=>$contact->name];
    }

    public function update(Request $request, $id)
    {
        $params = $request->except('_token', '_method');
        $params['slug'] = Str::slug($request->input('email'));

        $contact = Contact::query()->where('_id', $id)->first();
        foreach ($params as $key => $value) {
            $contact->$key = $value;
        }
        $contact->save();
        return ['status'=>'success', 'contact'=>$contact];

    }

    public function destroy($id)
    {
        $model = Contact::query()->where('_id', $id)->first();
        return $model->delete();

    }

    public function up($id)
    {
        $this->parentUp(Contact::find($id));
    }
    public function down($id)
    {
        $this->parentDown(Contact::find($id));
    }

    public function translate($id)
    {
        //TODO
    }

    public function translateStore(Request $request, $id)
    {
            //TODO
    }
    
    public function deleteImage(Request $request, $id)
    {
        $module = Contact::find($id);
        $image = $request->get('image');
        $module->$image = null;
        $module->save();

        return ['status'=>'Success'];
    }

}
