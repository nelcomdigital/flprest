<?php

namespace App\Http\Controllers\CMS;

use App\Models\BusinessFile;
use App\Models\BusinessCategory;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class BusinessFileController extends Controller
{
    public function index(Request $request)
    {
        $model = new BusinessFile();
        if ($request->ajax() && $request->has('table')) {
            if ($request->has('filters')) {
                return datatables($model->filterSearch($request->input('filters')))->make(true);
            }
            return datatables(BusinessFile::query())->make(true);;
        }

        return view('cms.business-file.index', compact('model'));
    }

    public function create(Request $request)
    {
        return ['view' => view('cms.business-file.create')->render(), 'title' => 'New BusinessFile'];
    }


    public function store(Request $request)
    {

        $params = $request->except('_token', '_method', 'businessCategories');
        $params['slug'] = Str::slug($request->input('name'));
        $businessFile = new BusinessFile();
        foreach ($params as $key => $value) {
            $businessFile->$key = $value;
        }

        $businessFile->save();


        $businessFile->businessCategories()->attach($request->input('businessCategories'));
        $businessFile->file = $this->copyFileToPublic($request->input('file'));

        $businessFile->save();
        return ['status' => 'success', 'businessFile' => $businessFile];
    }

    public function show($id)
    {
        //TODO
    }

    public function edit(Request $request, $id)
    {
        $businessFile = BusinessFile::query()->find($id);

        return ['view' => view('cms.business-file.edit', compact('businessFile'))->render(), 'title' => $businessFile->name];
    }

    public function update(Request $request, $id)
    {
        $params = $request->except('_token', '_method', 'businessCategories');
        $params['slug'] = Str::slug($request->input('name'));

        $businessFile = BusinessFile::query()->where('_id', $id)->first();
        foreach ($params as $key => $value) {
            $businessFile->$key = $value;
        }


        $businessFile->save();

        $businessFile->businessCategories()->sync($request->input('businessCategories'));
        $businessFile->file = $this->copyFileToPublic($request->input('file'));

        $businessFile->save();

        return ['status' => 'success', 'businessFile' => $businessFile];
    }

    public function destroy($id)
    {
        $model = BusinessFile::query()->where('_id', $id)->first();
        return $model->delete();
    }

    public function up($id)
    {
        $this->parentUp(BusinessFile::find($id));
    }

    public function down($id)
    {
        $this->parentDown(BusinessFile::find($id));
    }

    public function deleteImage(Request $request, $id)
    {
        $module = BusinessFile::find($id);
        $image = $request->get('image');
        $module->$image = null;
        $module->save();

        return ['status' => 'Success'];
    }
}
