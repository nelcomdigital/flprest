<?php

namespace App\Http\Controllers\CMS;

use App\Models\Locale;
use App\Models\Country;
use App\Models\MainMenu;
use App\Models\Page;
use App\Models\Status;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Mpdf\Tag\Main;


class MainMenuController extends CmsController
{
    public function index(Request $request)
    {
        $model = new MainMenu();
        if ($request->ajax() && $request->has('table')) {
            if ($request->has('filters')) {
                return datatables($model->filterSearch($request->input('filters')))->make(true);
            }
            return datatables(MainMenu::query())->make(true);;
        }
        return view('cms.main-menu.index', compact('model'));
    }

    public function create(Request $request)
    {
        $statuses = Status::all();
        $mainCategories = MainMenu::all();
        return ['view' => view('cms.main-menu.create', compact('statuses', 'mainCategories'))->render(), 'title' => 'New main menu'];
    }

    public function store(Request $request){

        $params = $request->except('_token', '_method', 'locales', 'countries', 'parents','is_parent');
        $params['slug'] = Str::slug($request->input('name'));
        $menu = new MainMenu();
        foreach ($params as $key => $value) {
            $menu->$key = $value;
        }
        $locales = $request->input('locales');
        foreach ($locales as $locale) {
            $menu->locales()->associate(Locale::find($locale));
        }
        $ids = $request->input('parents');
        $menu->parent_ids = $ids;

        $countries = $request->input('countries');
        foreach ($countries as $country) {
            $menu->countries()->associate(Country::find($country));
        }
        $menu->is_parent = $request->input('is_parent');
        $menu->is_active = $request->is_active;
        $menu->save();
        if (count($menu->translatable) > 0) {
            $loc = Locale::query()->where('code', 'en')->first();
            $translations = array();
            foreach ($menu->translatable as $k) {
                $translations[$loc->code][$k] = $params[$k];
            }
            $translations[$loc->code]['is_primary'] = 1;
            $menu->translations = $translations;
            $menu->save();
        }
        return ['status' => 'success', 'menu' => $menu];
    }

    public function show($id)
    {
        //TODO
    }

    public function edit(Request $request, $id)
    {
        $statuses = Status::all();
        $menu = MainMenu::query()->find($id);
        $mainCategories = MainMenu::all();

        return ['view' => view('cms.main-menu.edit', compact('statuses', 'menu', 'mainCategories'))->render(), 'title' => $menu->name];

    }

    public function update(Request $request, $id)
    {
        $params = $request->except('_token', '_method', 'locales', 'countries', 'parents','is_parent');
//        $params['slug'] = Str::slug($request->input('name'));

        $main = MainMenu::query()->where('_id', $id)->first();
        foreach ($params as $key => $value) {
            $main->$key = $value;
        }

        $locales = $request->input('locales');
        $main->locales()->delete();
        foreach ($locales as $locale) {
            $main->locales()->associate(Locale::find($locale));
        }
        $ids = $request->input('parents');
        $main->parent_ids = $ids;
        $countries = $request->input('countries');
        $main->countries()->delete();
        foreach ($countries as $country) {
            $main->countries()->associate(Country::find($country));
        }
        $main->is_parent = $request->input('is_parent');
        $main->is_active = $request->is_active;
        $main->save();
        return ['status' => 'success', 'main' => $main];
    }

    public function destroy($id)
    {
        $model = MainMenu::query()->where('_id', $id)->first();
        return $model->delete();
    }

    public function up($id)
    {
        $this->parentUp(MainMenu::find($id));
    }

    public function down($id)
    {
        $this->parentDown(MainMenu::find($id));
    }

    public function translate($id)
    {
        $model = MainMenu::find($id);
        $route = route('main-menu.translate', $id);
        return ['view' => view('cms.layouts.blocks.translation.translation',
            compact('model', 'route'))->render(), 'title' => $model->name . '\'s Translate'];
    }

    public function translateStore(Request $request, $id)
    {
        $model = MainMenu::find($id);
        $params = $request->except('_token', '_method');
        $is_primary = 'en';
        foreach ($model->translations as $k => $translation) {
            if (array_key_exists('is_primary', $translation)) {
                $is_primary = $k;
            }
        }
        foreach ($params as $k => $value) {
            $count = MainMenu::query()->where('_id', $model->_id)
                ->where('locales.code', $k)->count();
            if ($count == 0) {
                $model->locales()->associate(Locale::query()->where('code', $k)->first());
            }
            if ($k == $is_primary) {
                $params[$k]["is_primary"] = 1;
            }
        }
        $model->translations = $params;
        return ['status' => 'success', 'venue' => $model->save()];

    }

    public function deleteImage(Request $request, $id)
    {
        $module = MainMenu::find($id);
        $image = $request->get('image');
        $module->$image = null;
        $module->save();

        return ['status' => 'Success'];
    }

}
