<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;



class ProfileController extends Controller
{
    //
    public function edit()
    {
        $user = Auth()->user();
        return ['view'=>view('cms.user.edit-profile', compact('user'))->render(), 'title'=>$user->name];
    }

    public function update(Request $request){
        $params = $request->except('_token','password','photo');

        $u = Auth()->user();
        if ($request->input('old_password') != null && !Hash::check( $request->input('old_password'),$u->password )) {
            return ['status' => 'error', 'messages' => ["old_password" => 'Password not match']];
        }else if($request->input('password') != null){
            $params['password'] =  Hash::make($request->input('password'));
        }
        if ( $request->has('photo') and $request->input('photo') != null ){
            $params['photo'] = $request->input('photo');
        }


        $this->updateUser($params);
        return ['status' => 'success'];
    }

    private function updateUser(array $params)
    {
        try {
            return Auth()->user()->update($params);
        } catch (\Exception $e) {
            return false;
        }
    }


}
