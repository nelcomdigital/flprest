<?php

namespace App\Http\Controllers\CMS;

use App\Models\Locale;
use App\User;
use App\Models\Status;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;


class UserController extends CmsController
{

   public function index(Request $request)
    {
        $model = new User();
        if ( $request->ajax() && $request->has('table')){
            if($request->has('filters')){
                return datatables( $model->filterSearch($request->input('filters'),false,false ) )->make(true);
            }
            return datatables(User::query())->make(true);

        }

        return view('cms.user.index',compact('model'));
    }
    public function create(){
        $statuses = Status::all();
        $roles = Role::all();
        return ['view'=>view('cms.user.create', compact('statuses', 'roles'))->render(), 'title'=>'New User'];
    }

    public function store(Request $request)
    {
        $params = $request->except('_token', '_method', 'roles', 'password', 'password_confirmation');
        $user = User::query()->where('email', $params['email'] )->first();
        if ( $user != null ){
            return ['status'=>'error', 'errors'=>[
                    'email' => 'Email already exists'
                ]
            ];
        }

        $params['slug'] = Str::slug($request->input('name'));
        $params['password'] = bcrypt($request->input('password'));
        $params['is_approved'] = true;
        $user = new User();
        foreach($params as $key => $value){
            $user->$key = $value;
        }
        $user->save();

        $user->roles()->attach($request->input('roles'));

        return ['status'=>'success', 'user'=>$user];
    }

    public function show($id)
    {
        //TODO
    }

    public function edit($id)
    {
        $statuses = Status::all();
        $roles = Role::all();
        $user = User::query()->find($id);
        return ['view'=>view('cms.user.edit', compact('statuses', 'roles', 'user'))->render(), 'title'=>$user->name];
    }

    public function update(Request $request, $id)
    {
        $params = $request->except('_token', '_method', 'roles', 'password', 'password_confirmation');


        $params['slug'] = Str::slug($request->input('name'));
        if($request->get('password') != null){
            $params['password'] = bcrypt($request->input('password'));
        }
        $user = User::query()->find($id);
        foreach($params as $key => $value){
            $user->$key = $value;
        }
        $user->save();

        $user->roles()->sync($request->input('roles'));

        return ['status'=>'success', 'user'=>$user];
    }

    public function destroy($id)
    {
        $model = User::query()->where('_id', $id)->first();
        return $model->delete();
    }

    public function up($id)
    {
        $this->parentUp(User::find($id));
    }
    public function down($id)
    {
        $this->parentDown(User::find($id));
    }

    public function deleteImage(Request $request, $id)
    {
        $user = User::find($id);
        $user = $request->get('image');
        $user->image = null;
        $user->save();

        return ['status'=>'Success'];
    }
    public function editProfile()
    {
        $user = Auth()->user();
        return ['view'=>view('cms.user.edit-profile', compact('user'))->render(), 'title'=>$user->name];
    }
    public function updateProfile(Request $request){
        $params = $request->except('_token','password');

        $u = Auth()->user();
        if ($request->input('old_password') != null && !Hash::check( $request->input('old_password'),$u->password )) {
            return ['status' => 'error', 'messages' => ["old_password" => 'Password not match']];
        }else if($request->input('password') != null){
            $params['password'] =  Hash::make($request->input('password'));
        }
//        if ( $request->has('image') and $request->input('image') != null ){
//            $params['image'] = $request->input('image');
//        }
////        $this->updateUser($params);
        foreach ($params as $key=>$value){
            $u->$key= $value;
        }
        $u->save();
        return ['status' => 'success'];
    }
    private function updateUser(array $params)
    {
        try {
            return Auth()->user()->update($params);
        } catch (\Exception $e) {
            return false;
        }
    }
}
