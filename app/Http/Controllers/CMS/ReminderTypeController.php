<?php

namespace App\Http\Controllers\CMS;

use App\Models\ReminderType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;


class ReminderTypeController extends Controller
{
   public function index(Request $request)
     {
        $model = new ReminderType();
        if ($request->ajax() && $request->has('table')) {
            if ($request->has('filters')) {
                return datatables($model->filterSearch($request->input('filters')))->make(true);
            }
            return datatables(ReminderType::query())->make(true);
            ;
        }

        return view('cms.reminder-type.index', compact('model'));
    }


    public function create(Request $request)
    {

        return ['view'=>view('cms.reminder-type.create')->render(), 'title'=>'New Reminder Type'];
    }


    public function store(Request $request)
    {
        $params = $request->except('_token', '_method');
        $params['slug'] = Str::slug($request->input('name'));
        $reminderType = new ReminderType();
        foreach ($params as $key => $value) {
            $reminderType->$key = $value;
        }
        $reminderType->save();

        return ['status'=>'success', 'reminderType'=>$reminderType];
    }


    public function show($id)
    {
        //TODO
    }

    public function edit(Request $request, $id)
    {
        $reminderType = ReminderType::query()->where('_id', $id)->first();
        return ['view'=>view('cms.reminder-type.edit', compact('reminderType'))->render(), 'title'=>$reminderType->name];
    }


    public function update(Request $request, $id)
    {
        $params = $request->except('_token', '_method');
        $params['slug'] = Str::slug($request->input('name'));

        $reminderType = ReminderType::query()->where('_id', $id)->first();
        foreach ($params as $key => $value) {
            $reminderType->$key = $value;
        }
        $reminderType->save();
        return ['status'=>'success', 'reminderType'=>$reminderType];
    }


    public function destroy($id)
    {
        $model = ReminderType::query()->where('_id', $id)->first();
        return $model->delete();

    }

    public function up($id)
    {
        $this->parentUp(ReminderType::find($id));
    }
    public function down($id)
    {
        $this->parentDown(ReminderType::find($id));
    }

    public function translate($id)
    {
        //TODO
    }

    public function translateStore(Request $request, $id)
    {
            //TODO
    }
    
    public function deleteImage(Request $request, $id)
    {
        $module = ReminderType::find($id);
        $image = $request->get('image');
        $module->$image = null;
        $module->save();

        return ['status'=>'Success'];
    }

}
