<?php


namespace App\Http\Controllers\CMS;

use App\Models\Category;
use App\Models\Country;
use App\Models\Locale;
use App\Models\ProductCategory;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Str;


class ProductCategoryController extends CmsController
{

    public function index(Request $request)
    {
//        $model = new ProductCategory();
//        if ($request->ajax() && $request->has('table')) {
//            if ($request->has('filters')) {
//                return datatables($model->filterSearch($request->input('filters')))->make(true);
//            }
//            return datatables(ProductCategory::query())->make(true);
//        }
//        return view('cms.product-category.index', compact('model'));

        $model = new ProductCategory();
        if ($request->ajax() && $request->has('table')) {
            if ($request->has('_qq') && $request->get('_qq') != '' ) {
                $products = ProductCategory::query();
                $locales = Locale::all();
                foreach ( $locales as $k =>$locale ){
                    if ( $k == 0)
                        $products->where('name.'.$locale->code, 'like','%'.$request->get('_qq').'%');
                    else
                        $products->orWhere('name.'.$locale->code, 'like','%'.$request->get('_qq').'%');
                }
                return datatables($products->get())->make(true);

            }else
                return datatables(ProductCategory::query())->make(true);
        }
        return view('cms.product-category.index', compact('model'));
    }

    public function create(Request $request)
    {
        $productCategories = ProductCategory::all();
        $countries = Country::all();
        $roles = Role::query()->whereNotIn('slug',['super-admin','admin'])->get();
        return ['view' => view('cms.product-category.create', compact('productCategories','roles', 'countries'))->render(), 'title' => 'New Product Category'];
    }

    public function store(Request $request)
    {
        $params = $request->except('_token', '_method', 'productCategories');

        foreach ($request->get('name') as $k => $value) {
            $slug [$k] = Str::slug($value);
        }
        $productCategory = new ProductCategory();
        $productCategory->name = $request->get('name');
        $productCategory->slug = $slug;
        $productCategory->is_active = $request->is_active;
        $productCategory->banner_image_mobile = $request->banner_image_mobile;
        $productCategory->icon = $request->icon;
        $productCategory->for_app = $request->get('for_app');
        $productCategory->banner_image = $request->get('banner_image');

        if ( $request->input('levels_allowed') != null ) {
            $ids = array();
            foreach ( $request->input('levels_allowed')  as $id ){
                $ids[] = $id;
            }
            $productCategory->levels_allowed = $ids;
        }else{
            $productCategory->levels_allowed = [];
        }

        //parent
        $productCategory->save();
        $productCategory->countries()->attach($request->get('countries'));

        return ['status' => 'success', 'productCategory' => $productCategory];
    }

    public function show($id)
    {
        //TODO
    }

    public function edit($id)
    {
        $productCategory = ProductCategory::query()->find($id);
        $productCategories = ProductCategory::query()->get();
        $countries = Country::all();
        $roles = Role::query()->whereNotIn('slug',['super-admin','admin'])->get();

        return ['view' => view('cms.product-category.edit', compact('roles','productCategory','countries', 'productCategories'))->render(),
            'title' => is_string($productCategory->name) ?: $productCategory->name[session()->get('current_locale')]];
    }

    public function update(Request $request, $id)
    {
        $params = $request->except('_token', '_method', 'productCategories', 'countries');
        foreach ($request->get('name') as $k => $value) {
            $slug [$k] = Str::slug($value);
        }

        $productCategory = ProductCategory::query()->where('_id', $id)->first();
        foreach ($params as $key => $value) {
            $productCategory->$key = $value;
        }
//        $productCategory->parent_id = $request->get('parent_id');

        $productCategory->is_active = $request->is_active;
        $productCategory->for_app = $request->get('for_app');
        $productCategory->banner_image = $request->get('banner_image');
        $productCategory->banner_image_mobile = $request->banner_image_mobile;
        $productCategory->icon = $request->icon;
        if ( $request->input('levels_allowed') != null ) {
            $ids = array();
            foreach ( $request->input('levels_allowed')  as $id ){
                $ids[] = $id;
            }
            $productCategory->levels_allowed = $ids;
        }else{
            $productCategory->levels_allowed = [];
        }

        $productCategory->countries()->detach();
        $productCategory->country_ids = [];
        $productCategory->save();

        $productCategory->countries()->attach($request->get('countries'));
        $productCategory->save();

//        if ($request->input('parent_product_category_id') != null)
//            $productCategory->productCategories()->sync($request->input('parent_product_category_id'));

        return ['status' => 'success', 'product-category' => $productCategory];
    }

    public function destroy($id)
    {
        $model = ProductCategory::query()->where('_id', $id)->first();
        return $model->delete();
    }

    public function up($id)
    {
        $this->parentUp(ProductCategory::find($id));
    }

    public function down($id)
    {
        $this->parentDown(ProductCategory::find($id));
    }

}
