<?php

namespace App\Http\Controllers\CMS;

use App\Models\Group;
use App\Models\Contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use App\Models\Locale;
use App\User;


class GroupController extends CmsController
{
   public function index(Request $request)
    {
        $model = new Group();
        if ( $request->ajax() && $request->has('table')){
            if($request->has('filters')){
                return datatables( $model->filterSearch($request->input('filters')) )->make(true);
            }
            return datatables(Group::query())->make(true);;
        }

        return view('cms.group.index',compact('model'));
    }

    public function create(Request $request)
    {
        
        $users = User::query()->get();

        $contacts = Contact::query()->get();
        return ['view' => view('cms.group.create', compact('contacts','users'))->render(), 'title' => 'New group'];
    }


    public function store(Request $request)
    {
        $params = $request->except('_token', '_method', 'locales', 'countries', 'contacts');
        $params['slug'] = Str::slug($request->input('name'));
        $group = new Group();
        foreach ($params as $key => $value) {
            $group->$key = $value;
        }        
        $ids = $request->input('contacts');
        $group->contact_ids = $ids;
        $group->save();
        if (count($group->translatable) > 0) {
            $loc = Locale::query()->where('code', 'en')->first();
            $translations = array();
            foreach ($group->translatable as $k) {
                $translations[$loc->code][$k] = $params[$k];
            }
            $translations[$loc->code]['is_primary'] = 1;
            $group->translations = $translations;
            $group->save();
        }
        return ['status' => 'success', 'group' => $group];

    }

    public function show($id)
    {
        //TODO
    }

    public function edit(Request $request, $id)
    {
        $contacts = Contact::query()->get();        
        $users = User::query()->get();
        $group = Group::query()->find($id);
        return ['view' => view('cms.group.edit', compact( 'group','contacts','users'))->render(), 'title' => $group->name];
    }

    public function update(Request $request, $id)
    {
        $params = $request->except('_token', '_method', 'locales', 'countries', 'contacts');
        $params['slug'] = Str::slug($request->input('name'));

        $group = Group::query()->where('_id', $id)->first();
        $ids = $request->input('contacts');
        $group->contact_ids = $ids;
        foreach ($params as $key => $value) {
            $group->$key = $value;
        }
        $group->save();
        return ['status' => 'success', 'group' => $group];
    }


    public function destroy($id)
    {
        $model = Group::query()->where('_id', $id)->first();
        return $model->delete();
    }

    public function up($id)
    {
        $this->parentUp(Group::find($id));
    }
    public function down($id)
    {
        $this->parentDown(Group::find($id));
    }

    public function translate($id)
    {
        //TODO
    }

    public function translateStore(Request $request, $id)
    {
            //TODO
    }
    
    public function deleteImage(Request $request, $id)
    {
        $module = Group::find($id);
        $image = $request->get('image');
        $module->$image = null;
        $module->save();

        return ['status'=>'Success'];
    }

}
