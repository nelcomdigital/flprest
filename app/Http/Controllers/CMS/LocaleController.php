<?php

namespace App\Http\Controllers\CMS;

use App\Models\Locale;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;


class LocaleController extends Controller
{
    public function index(Request $request)
    {
        $model = new Locale();
        if ( $request->ajax() && $request->has('table')){
            if($request->has('filters')){
                return datatables( $model->filterSearch($request->input('filters')) )->make(true);
            }
            return datatables(Locale::query())->make(true);;
        }
        
        return view('cms.locale.index',compact('model'));
    }
    
    public function create(Request $request)
    {
        
        return ['view'=>view('cms.locale.create')->render(), 'title'=>'New Locale'];
    }
    
    
    public function store(Request $request)
    {
        $params = $request->except('_token', '_method');
        $params['slug'] = Str::slug($request->input('name'));
        $locale = new Locale();
        foreach($params as $key => $value){
            $locale->$key = $value;
        }
        $locale->save();
        
        return ['status'=>'success', 'locale'=>$locale];
    }
    
    public function show($id)
    {
        //TODO
    }
    
    public function edit($id)
    {
        $locale = Locale::query()->where('_id', $id)->first();
        return ['view'=>view('cms.locale.edit', compact('locale'))->render(), 'title'=>$locale->name];
    }
    
    public function update(Request $request, $id)
    {
        $params = $request->except('_token', '_method');
        $params['slug'] = Str::slug($request->input('name'));
        
        $locale = Locale::query()->where('_id', $id)->first();
        foreach($params as $key => $value){
            $locale->$key = $value;
        }
        $locale->save();
        return ['status'=>'success', 'locale'=>$locale];
    }
    
    public function destroy($id)
    {
        $model = Locale::query()->where('_id', $id)->first();
        return $model->delete();
    }
    
    public function up($id)
    {
        $this->parentUp(Locale::find($id));
    }
    public function down($id)
    {
        $this->parentDown(Locale::find($id));
    }
}
