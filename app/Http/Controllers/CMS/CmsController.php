<?php

namespace App\Http\Controllers\CMS;

use App\Models\Area;
use App\Models\City;
use App\Models\Domain;
use App\Models\GeneralConfiguration;
use App\Models\LinkType;
use App\Models\Locale;
use App\Models\Product;
use App\Models\Region;
use App\Models\Country;
use App\Models\Department;
use App\Models\Status;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use App\Utils\MongoDumper;


class CmsController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function home(){
        return view('cms.layouts.app');
    }


    public function parentUp($model){
        return $model->moveOrderUp();
    }

    public function parentDown($model){
        return $model->moveOrderDown();
    }

    public function ajaxSelect(Request $request){
        if($request->has('country_id')){
            $regions = Region::query()->where('country_id', $request->get('country_id'));
            if($request->has('q') and $request->get('q') != ""){
                $regions = $regions->where('name', 'like', '%'.$request->get('q').'%');
            }
            $regions = $regions->orderBy('name', 'asc')->pluck('_id','name');
            return json_encode($regions);
        }else if($request->has('region_id')){
            $departments = Department::query()->where('region_id', $request->get('region_id'));
            if($request->has('q') and $request->get('q') != ""){
                $departments = $departments->where('name', 'like', '%'.$request->get('q').'%');
            }
            $departments = $departments->orderBy('name', 'asc')->pluck('_id','name');
            return json_encode($departments);
        }else if($request->has('city_id')){
            $areas = Area::query()->where('city_id', $request->get('city_id'));
            if($request->has('q') and $request->get('q') != ""){
                $areas = $areas->where('name', 'like', '%'.$request->get('q').'%');
            }
            $areas = $areas->orderBy('name', 'asc')->pluck('_id','name');
            return json_encode($areas);
        }else if ( $request->has('department_id')){
            $cities = City::query()->where('department_id', $request->get('department_id'));
            if($request->has('q') and $request->get('q') != ""){
                $cities = $cities->where('name', 'like', '%'.$request->get('q').'%');
            }
            $cities = $cities->orderBy('name', 'asc')->get()->toArray();
            return json_encode($cities);
        }else if ( $request->has('regions')){
            $regionIds = explode(',', $request->get('regions'));
            $departments = Department::query()->whereIn('region_id', $regionIds);
            if($request->has('q') and $request->get('q') != ""){
                $departments = $departments->where('name', 'like', '%'.$request->get('q').'%');
            }
            $departments = $departments->orderBy('name', 'asc')->pluck('_id','name');
            return json_encode($departments);
        }
    }

    public function ajaxSelectByModel( Request $request , $model){
        if($model == 'User'){
            $model = 'App\\'.$model;
        }else{
            $model = 'App\Models\\'.$model;
        }
        $model = new $model();
        $data = $model::query()->where('countries.code',Session::get('cms.country','LB'));
        if($request->has('q')  and $request->get('q') != ""){
            $data->where('name', 'like', '%'.$request->get('q').'%');
            $data->orWhere('translations.'.Session::get('cms.locale','en').'name', 'like',
                '%'.$request->get('q').'%');
        }
        return  json_encode(['data'=>$data->orderBy('name', 'asc')->get()->toArray(), 'label'=>null]);
    }

    public function ajaxSelectPublishedByModel( Request $request , $model){
        if($model == 'User'){
            $model = 'App\\'.$model;
        }else{
            $model = 'App\Models\\'.$model;
        }
        $model = new $model();
        $status = Status::query()->where('slug','published')->first();
        $data = $model::query()->where('countries.code',Session::get('cms.country','LB'));
        if($request->has('q')  and $request->get('q') != ""){
//            $data->where('name', 'like', '%'.$request->get('q').'%');
            $data->where('translations.'.Session::get('cms.locale','en').'.name', 'like',
                    '%'.$request->get('q').'%');
                $data->where('status_id', $status->_id);
        }
        return  json_encode(['data'=>$data->orderBy('name', 'asc')->get()->toArray(), 'label'=>null]);
    }

    public function ajaxSelectSearch(Request $request, $filter){
        $filter = str_singular($filter);
        $cntry = session()->get('cms.country', null);
        if($cntry != null and $cntry != 'GG'){
            $country = Country::query()->where('code', $cntry)->first();
        }else{
            $country = null;
        }

        if($filter == "region"){
            $data = Region::query();
            if($country != null){
                $data->where('country_id', $country->id);
            }
        }else if($filter == "department"){
            $data = Department::query();
            if($country != null){
                $ids = Region::query()->where('country_id', $country->id)->pluck('_id');
                $data->whereIn('region_id', $ids);
            }
        }else if($filter == "city"){
            $data = City::query();
            if($country != null){
                $ids = Region::query()->where('country_id', $country->id)->pluck('_id');
                $ids = Department::query()->whereIn('region_id', $ids)->pluck('_id');
                $data->whereIn('department_id', $ids);
            }
        }
        if($request->has('search') and $request->get('search') == "true"){
            if(!$request->has('q') or $request->get('q') == ""){
                return json_encode(['data'=>[], 'label'=>$filter]);
            }
        }
        if($request->has('q') and $request->get('q') != ""){
            $data = $data->where('name', 'like', '%'.$request->get('q').'%');
        }
        $data = $data->orderBy('name', 'asc')->get()->toArray();
        return json_encode(['data'=>$data, 'label'=>$filter]);
    }




    public function changeCountry($id){
        session()->put('cms.country', $id );
        return redirect()->back();
    }

    public function changeLocale($id){
        session()->put('cms.locale', $id );
        return redirect()->back();
    }

    public function generalConfiguration(){

        $configs = GeneralConfiguration::query()->where('user_id',null)
            ->orWhere('user_id',auth()->id())->get();

        return ['view'=> view( 'cms.layouts.partials.cms-configuration', compact('configs'))->render(),'title'=> 'General Configuration'];
    }
    public function generalConfigurationPost(Request $request){

        $params = $request->except('_token', '_method', '_method','maitainance');

        $conf = GeneralConfiguration::query()->where('key','maitainance')
                        ->first();
        if ( $conf == null ){
            $conf = new GeneralConfiguration();
            $conf->key = 'maitainance';
        }
        if ($request->get('maitainance') == "1"){
                $value = $request->get('maitainance');
        } else{
            $value = "";
        }
        $conf->value = $value;
        $conf->save();

        foreach($params as $key => $value){
           if ( $key !='language' ){
                $conf = GeneralConfiguration::query()->where('key', $key)
                ->first();
                if ($conf == null) {
                    $conf = new GeneralConfiguration();
                    $conf->key = $key;
                }
                $conf->value = $value;
                $conf->save();
           }else{
               $conf = GeneralConfiguration::query()->where('key',$key)
                   ->where('user_id',auth()->id())
                   ->first();
               if ( $conf == null ){
                   $conf = new GeneralConfiguration();
                   $conf->key = $key;
                   $conf->user_id = auth()->id();
               }
               $conf->value = $value;
               $conf->save();
           }
        }

        return ['status'=>'success'];
   }

    public function deleteGeneralConfigImage($id, $type )
    {
        $config = GeneralConfiguration::find($id);
        $config->$type = null;
        $config->save();

        return ['status'=>'Success'];
    }

    public function scanAlbums($model, $slug ){
        $Model = "App\Models\\".$model;
        $m = new $Model();
        $result = $m::query()->where('slug',$slug)->first();
        if ( $result != null
            && is_array( $result->albums)
            && sizeof( $result->albums) > 0  ){
            $albums = $result->albums;
            $newAlbums = array();
            foreach ( $albums as $album ){
                $images = $album['images'];
                $newImages = array();
                if (is_array( $images)) {
                    foreach ($images as $image) {
                        $image['url'] = asset('storage/' . $image['url']);
                        $newImages[] = $image;
                    }
                    $album['images'] = $newImages;
                    $newAlbums[] = $album;
                }
            }

            return response()->json( $newAlbums );
        }
        else return response()->json("no_data");
    }

    public function removeAlbumImage(Request $request){

        try{
            if ( $request->has('slug') && $request->has('model') ){
                Storage::disk('public')->delete($request->input('image') );

                $model = 'App\Models\\'.$request->input('model');
                $m = new $model();
                $object = $m::query()->where ( 'slug',$request->input('slug'))->first();


                if ( $object != null ) {
                    $newAlbums = array();
                    $oldAlbums = $object->albums;
                    foreach ( $oldAlbums as $album ){
                        $images = $album['images'];
                        $isExist = false;
                        foreach ( $images as $img ){
                            if ( $img['short_name'] == $request->input('image') ){
                                $isExist = true;
                                if ( !(array_has( $album , 'is_gallery') && $album['is_gallery'] == "1") ){
                                    try{
                                        Storage::disk('public')->delete($img['url']);
                                    }catch (\Exception $e){

                                    }
                                }
                                break;
                            }
                        }

                        if ( $isExist ){
                            $newImages = array();
                            foreach ( $images as $img ){
                                if ( $img['short_name'] != $request->input('image') ){
                                    $newImages [] =$img;
                                }
                            }
                            if ( sizeof( $newImages )> 0){
                                $album['images'] = $newImages;
                                $newAlbums[] = $album;
                            }

                        }else{
                            $newAlbums[]= $album;
                        }


                    }
                    $object->albums = $newAlbums;
                    $object->save();
                    return response()->json(['status'=>'success']);
                }
            }
        }catch ( \Exception $e){
            return response()->json(['status'=>'error']);
        }


    }


    public function uploadTempFile(Request $request){
        $image = $request->file('image');
        $slug = $request->input('slug');
        $model = $request->input('model');
        $album_name = $request->input('name');
        $temp_id = $request->input('temp_id');

        $result=null;
        if(isset($image)) {
            $currentDate = Carbon::now()->toDateString();
            $imageName = $slug . '-' . $currentDate . '-' . uniqid() . '.' . $image->getClientOriginalExtension();
            $postImage = Image::make($image)
                ->resize(1600, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })->stream('jpg', 100);
            if (Storage::disk('public')->put('galleries/' . $model . '/' . $slug . '/' . $album_name . '/' . $imageName, $postImage)) {
                $result = ['url' => asset('storage/galleries/' . $model . '/' . $slug . '/' . $album_name . '/' . $imageName),
                    'short_name' => $imageName,'path'=>'galleries/' . $model . '/' . $slug . '/' . $album_name . '/' . $imageName, 'temp_id' => $temp_id];
            }else{
                $result = ['url' => null,
                    'short_name' => $imageName, 'path'=>null,'temp_id' => $temp_id];
            }
        }
                return response()->json($result);

    }


    public function submitSaving(Request $request){
        $images = $request->input('images');
        $slug = $request->input('slug');
        $model = $request->input('model');
        $album_name = $request->input('name');


        $Model = 'App\Models\\'.$model;
        $m = new $Model();
        $object = $m::query()->where ( 'slug',$slug)->first();

        if(!Storage::disk('public')->exists('galleries'))
        {
            Storage::disk('public')->makeDirectory('galleries');
        }

        if(!Storage::disk('public')->exists('galleries/'.$model))
        {
            Storage::disk('public')->makeDirectory('galleries/'.$model);
        }

        if(!Storage::disk('public')->exists('galleries/'.$model.'/'.$object->slug))
        {
            Storage::disk('public')->makeDirectory('galleries/'.$model.'/'.$object->slug);
        }

        if(!Storage::disk('public')->exists('galleries/'.$model.'/'.$object->slug.'/'. $album_name))
        {
            Storage::disk('public')->makeDirectory('galleries/'.$model.'/'.$object->slug.'/'. $album_name);
            $newAlbum['name'] = $album_name;
            $newAlbum['images'] = [];
            $albums = $object->albums;
            $albums[] = $newAlbum;
            $object->save();
        }
        $albums = $object->albums;
        $found = false;
        if ( $albums == null ){
            $albums = [];
        }
        foreach ( $albums as $a ){
            if ( $a['name'] == $album_name){
                $found= true;
                break;
            }
        }
        if ( !$found ){
            $newAlbum['name'] = $album_name;
            $newAlbum['images'] = [];
            array_push($albums , $newAlbum);
            $object->albums = $albums;
            $object->save();
        }
        $object = $m::query()->where('slug', $slug)->first();

        if ($object != null) {
            $albums = $object->albums;
            $newAlbums = array();
            if (is_array($albums))
                foreach ($albums as $a) {
                    if ($a['name'] == $album_name) {
                        $oldImages = $a['images'];
                        foreach ($images as $image ){
                            $newImage ['url'] = $image['url'];
                            $newImage['short_name'] = $image['short_name'];
                            array_push($oldImages, $newImage);
                        }
                        $a['images'] = $oldImages;
                        $newAlbums [] = $a;
                    } else {
                        $newAlbums [] = $a;
                    }
                }

            $object->albums = $newAlbums;
            $object->save();
            return response()->json(['status'=>'success']);
        }
        return response()->json(['status'=>'error']);

    }

    public function getPhotographers(Request $request){
        if ( $request->has('q')){
            return ['data'=>dataHelper(User::class, [['name','like',$request->input('q')]],['name','asc'],0 ,
                [ ['name'=>'roles','key'=>'slug','operator'=>'=', 'value'=>"photographer"]],[],false)->get()];
        }else{
            return ['data'=>dataHelper(User::class, [],['name','asc'],0 ,
                [ ['name'=>'roles','key'=>'slug','operator'=>'=', 'value'=>"photographer"]],[],false)->get()];
        }

    }

    public function getGalleryAlbumsFiles(){
        return response()->json( GalleryAlbum::query()->where('slug','gallery-album')->first());
    }

    public function addGalleryAlbum(Request $request){
        $gallery_albums = GalleryAlbum::query()->where('slug','gallery-album')->first();

        $album_name = $request->input('album_name');
        $model = $request->input('model');
        $slug = $request->input('slug');

        $Model = "App\Models\\".$model;
        $m = new $Model();
        $result = $m::query()->where('slug',$slug)->first();
        $albums = $result->albums;

        $selected_album = null ;
        foreach ( $gallery_albums->albums as $al ){
            if ( $al['name'] == $album_name ){
                $selected_album = $al;
                $selected_album['is_gallery'] = "1";
            }
        }
        if ( $selected_album != null ){
            $albums [] = $selected_album;
            $result->albums = $albums;
            $result->save();
        }



    }
    public function updateAlbum( Request $request ){
        $oldAlbum = $request->input('old_album');
        $newAlbum = $request->input('new_album');
        $photographers = $request->input('photographers');
        $description = $request->input('description');
        $cities = $request->input('cities');
        $model = $request->input('model');
        $slug = $request->input('slug');


        $Model = "App\Models\\".$model;
        $m = new $Model();
        $result = $m::query()->where('slug',$slug)->first();

        $albums = $result->albums;


        if ( $oldAlbum != $newAlbum ){
            $newAlbums = array();

            if(!Storage::disk('public')->exists('galleries'))
            {
                Storage::disk('public')->makeDirectory('galleries');
            }

            if(!Storage::disk('public')->exists('galleries/'.$model))
            {
                Storage::disk('public')->makeDirectory('galleries/'.$model);
            }

            if(!Storage::disk('public')->exists('galleries/'.$model.'/'.$result->slug))
            {
                Storage::disk('public')->makeDirectory('galleries/'.$model.'/'.$result->slug);
            }

            if(!Storage::disk('public')->exists('galleries/'.$model.'/'.$result->slug.'/'. $newAlbum))
            {
                Storage::disk('public')->makeDirectory('galleries/'.$model.'/'.$result->slug.'/'. $newAlbum);
            }

            if (is_array($albums))
            foreach ( $albums as $album ){
                if ( $album['name'] == $oldAlbum ){
                    $album['name'] = $newAlbum;
                    $album['description'] = $description;
                    $pgs = explode(',',$cities);
                    $ps = array();
                    foreach ( $pgs as $pg){
                        try{
                            $p = City::query()->where('_id', $pg)->first();
                            $ps[] =  ['_id'=>$p->_id, 'name'=>$p->name];
                        }catch (\Exception $e){

                        }

                    }
                    $album['cities'] = $ps;
                    $newImages = array();
                    $images = $album['images'];
                    if ( is_array( $images ) && sizeof($images)> 0 ) {
                        foreach ($images as $image) {
                            $newPath = str_replace( $oldAlbum . '/', $newAlbum . '/', $image['url']);
                            if (Storage::disk('public')->move($image['url'], $newPath)) {
                                $image['url'] = $newPath;
                                $i['url'] = $newPath;

                            }
                            $newImages [] = $image;
                        }
                    }
                    $album['images'] = $newImages;
                }
                $newAlbums[] = $album;
            }
            $result->albums = $newAlbums;
            $result->save();
        }else{
            $newAlbums = array();
            if (is_array($albums))
            foreach ( $albums as $album ){
                if ( $album['name'] == $newAlbum ){
                    $album['description'] = $description;
                    $pgs = explode(',',$cities);
                    $ps = array();
                    foreach ( $pgs as $pg){
                        try{
                            $p = City::query()->where('_id', $pg)->first();
                            $ps[] =  ['_id'=>$p->_id, 'name'=>$p->name];
                        }catch (\Exception $e){

                        }

                    }
                    $album['cities'] = $ps;


                }
                $newAlbums[] = $album;
            }
            $result->albums = $newAlbums;
            $result->save();

        }




    }

    public function updateAlbumImage( Request $request ){
        $caption = $request->input('caption');
        $custom = $request->input('custom');
        $photographers = $request->input('photographers');
        $short_name = $request->input('short_name');
        $model = $request->input('model');
        $slug = $request->input('slug');


        $Model = "App\Models\\".$model;
        $m = new $Model();
        $result = $m::query()->where('slug',$slug)->first();

        $albums = $result->albums;

            $newAlbums = array();
        if (is_array($albums))
            foreach ( $albums as $album ){
                    $newImages = array();
                    $images = $album['images'];
                    if ( is_array( $images ) && sizeof($images)> 0 ) {
                        foreach ($images as $image) {
                            if ($image['short_name'] == $short_name ) {
                                $image['caption'] = $caption;
                                $image['custom'] = $custom;
                                $pgs = explode(',',$photographers);
                                $ps = array();
                                foreach ( $pgs as $pg){
                                    try{
                                        $p = User::query()->where('_id', $pg)->first();
                                        $ps[] =  ['_id'=>$p->_id, 'name'=>$p->name];
                                    }catch (\Exception $e){

                                    }

                                }
                                $image['photographers'] = $ps;

                            }
                            $newImages [] = $image;
                        }
                    }
                    $album['images'] = $newImages;
                $newAlbums[] = $album;
            }
            $result->albums = $newAlbums;
            $result->save();

    }



    public function exportBackup(){
        $date = Carbon::now()->format('Y-m-d-H');
        $path = storage_path('app/public/db_backups/dump-'.$date);
        if (! file_exists( $path))
        mkdir( $path);
        $dumper = new MongoDumper($path);
         if( $dumper->run(env('DB_DATABASE','Moovtoo'), false)){
             return 'dump-'.$date;
         }else{
             return null;
         }
    }

    public function exportBackupForm(Request $request){

        try{
            $files = scandir(storage_path('app/public/db_backups'), SCANDIR_SORT_DESCENDING);
            if ( $files == null || !is_array($files) || sizeof( $files ) == 0){
                $name = null;
            }else{
                $newest_file = $files[0];
                $name = $newest_file['name'];
            }
        }catch (\Exception $e){
            $name = null ;
        }

        return view('cms.layouts.export.export-form', compact('name'));
    }

    public function exportBackupPost ( Request $request ){
        if ( !$request->get('database') && !$request->get('storage') ){
            return ' nothing selected';
        }
        $export = $this->exportBackup();
        if ( $export == null )
            return 'error exporting DB';

        if ( $request->get('database')){
            $files = scandir(storage_path('app/public/db_backups'), SCANDIR_SORT_DESCENDING);

            if ( $files == null || !is_array($files) || sizeof( $files ) == 0){
                return 'something went wrong in db';
            }else{

                if (!$request->get('storage')){
                    return redirect()->to(asset('storage/db_backups/'.$export.'/'.env('DB_DATABASE').'.zip'));
                }
            }

        }
        if ( $request->get('storage')){

            $this->addzip(storage_path('app/public'), storage_path('app/public/db_backups_zip/all.zip'));
            return redirect()->to(asset('storage/db_backups_zip/all.zip'));
        }
    }

    private function create_zip($files = array(),$destination = '',$source,$overwrite = true) {
        //if the zip file already exists and overwrite is false, return false
        if(file_exists($destination) && !$overwrite) { return false; }
        //vars
        $valid_files = array();
        //if files were passed in...

        if(is_array($files)) {
            //cycle through each file
            foreach($files as $file) {
                //make sure the file exists
                if(file_exists($file)) {
                    $valid_files[] = $file;
                }
            }
        }
        //if we have good files...
        if(count($valid_files)) {
            //create the archive
            $zip = new \ZipArchive();
            if($zip->open($destination, \ZipArchive::OVERWRITE|\ZipArchive::CREATE) !== true) {
                return false;
            }
            echo 'zip created';
            $files = new \RecursiveIteratorIterator(
                new \RecursiveDirectoryIterator($source),
                \RecursiveIteratorIterator::LEAVES_ONLY
            );

            echo 'files sorted';
            foreach ($files as $name => $file)
            {
                // Skip directories (they would be added automatically)
                if (!$file->isDir() )
                {
                    // Get real and relative path for current file
                    $filePath = $file->getRealPath();

                    $relativePath = substr($filePath, strlen($source) + 1);

                    // Add current file to archive
                    if ( !str_contains($filePath,'db_backups_zip')){
                        $zip->addFile($filePath, $relativePath);
                        echo $filePath.'\n';
                    }

                }
            }
            //debug
            //echo 'The zip archive contains ',$zip->numFiles,' files with a status of ',$zip->status;
            echo 'closing zip';
            //close the zip -- done!
            $zip->close();

            echo 'file closed';
            //check to make sure the file exists
            return true;
        }
        else
        {
            return false;
        }
    }
    public function setLoggers(&$model, $domains, $locales, $countries){
        //loggers
        foreach($locales as $locale){
            $model->locales()->associate(Locale::find($locale));
        }
        foreach($domains as $domain){
            $model->domains()->associate(Domain::find($domain));
        }
        foreach($countries as $country){
            $model->countries()->associate(Country::find($country));
        }
        $model->save();
    }
    
    public function updateLoggers(&$model, $domains, $locales, $countries){
        //loggers
        // try{
            $model->locales()->delete();
            foreach($locales as $locale){
                $model->locales()->associate(Locale::find($locale));
            }
        // }catch (\Exception $e){

        // }
        // try{
            $model->domains()->delete();
            foreach($domains as $domain){
                $model->domains()->associate(Domain::find($domain));
            }
        // }catch (\Exception $e){

        // }
        // try{
            $model->countries()->delete();
            foreach($countries as $country){
                $model->countries()->associate(Country::find($country));
            }
        // }catch (\Exception $e){

        // }
        $model->save();
    }


    private function addzip($source, $destination) {
        $files_to_zip = glob($source . '/*');
        return $this->create_zip($files_to_zip, $destination,$source);

    }





    public function sample(){

        return;

    }

    public function uploaderForm(){
        return view('cms.layouts.uploader');
    }

    public function uploader( Request $request){

        $collectionName = $request->input('collection_name');
        try{
            DB::table($collectionName)->count();
        }catch (\Exception $e){
            echo "Wrong table Name";
            return;
        }



        $collectionFields = explode(',',$request->input('collection_fields') );


        $f = $request->file('csv_file');
        $ext = $f->getClientOriginalExtension();
        // check the file is a csv
        $result = [];
        if ($ext === 'csv') {
            if (($handle = fopen($f, 'r')) !== FALSE) {
                // necessary if a large csv file
                set_time_limit(0);

                $c = 0;
//                try{
//                    DB::connection('mongodb')->beginTransaction();
                    while (($data = fgetcsv($handle, 1000, ',')) !== FALSE) {

                        if ($c++ == 0) continue;
                        $q = [];
                        foreach ($collectionFields as $k=>$v){
                            if ( Str::contains($v,'-')){
                                $vv = explode('-',$v);
                                $q[$vv[0]][$vv[1]] = mb_convert_encoding($data[$k],"HTML-ENTITIES","UTF-8");
                            }else{
                                $q[$v] = $data[$k];
                            }

                        }
                        DB::connection('mongodb')->collection($collectionName)->insert([$q]);

                    }
//                }catch (\Exception $exception){
////                    DB::connection('mongodb')->rollBack();
//                }
//                DB::connection('mongodb')->commit();

                fclose($handle);
            }
        }
    }
}
