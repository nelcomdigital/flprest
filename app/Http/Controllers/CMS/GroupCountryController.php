<?php

namespace App\Http\Controllers\CMS;

use App\Models\Country;
use App\Models\GroupCountry;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use MongoDB\Operation\Count;


class GroupCountryController extends CmsController
{

   public function index(Request $request)
    {
        $model = new GroupCountry();
        if ( $request->ajax() && $request->has('table')){
            if($request->has('filters')){
                return datatables( $model->filterSearch($request->input('filters')) )->make(true);
            }
            return datatables(GroupCountry::query())->make(true);;
        }

        return view('cms.group-country.index',compact('model'));
    }

    public function create(Request $request)
    {

        return ['view'=>view('cms.group-country.create')->render(), 'title'=>'New Group Country'];
    }


    public function store(Request $request)
    {
        $params = $request->except('_token', '_method');
        foreach ($request->get('name') as $k => $value) {
            $slug [$k] = Str::slug($value);
        }
        $groupCountry = new GroupCountry();
        $groupCountry->name = $request->input('name');
        $groupCountry->slug = $slug;
        $groupCountry->save();

        return ['status'=>'success', 'groupCountry'=>$groupCountry];
    }

    public function show($id)
    {
        //TODO
    }

    public function edit($id)
    {
        $groupCountry = GroupCountry::query()->where('_id', $id)->first();
        return ['view'=>view('cms.group-country.edit', compact('groupCountry'))->render(),
            'title' => is_string($groupCountry->name) ?: $groupCountry->name[session()->get('current_locale')]];
    }

    public function update(Request $request, $id)
    {
        $params = $request->except('_token', '_method');
        $groupCountry = GroupCountry::query()->where('_id', $id)->first();
        foreach ($request->get('name') as $k => $value) {
            $slug [$k] = Str::slug($value);
        }
        $groupCountry->name = $request->input('name');
        $groupCountry->slug = $slug;
        $groupCountry->save();
        return ['status'=>'success', 'groupCountry'=>$groupCountry];
    }

    public function destroy($id)
    {
        $model = GroupCountry::query()->where('_id', $id)->first();
        return $model->delete();
    }

    public function up($id)
    {
        $this->parentUp(GroupCountry::find($id));
    }
    public function down($id)
    {
        $this->parentDown(GroupCountry::find($id));
    }

}
