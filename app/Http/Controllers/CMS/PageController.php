<?php

namespace App\Http\Controllers\CMS;

use App\Models\Page;
use App\Models\Status;
use App\Models\Locale;
use Illuminate\Http\Request;
use Illuminate\Support\Str;


class PageController extends CmsController
{
    public function index(Request $request)
    {
        $model = new Page();
        if ($request->ajax() && $request->has('table')) {
            if ($request->has('filters')) {
                return datatables($model->filterSearch($request->input('filters')))->make(true);
            }
            return datatables(Page::query())->make(true);;
        }

        return view('cms.page.index', compact('model'));
    }

    public function create(Request $request)
    {
        $statuses = Status::all();
        return ['view' => view('cms.page.create', compact('statuses'))->render(), 'title' => 'New Page'];
    }

    public function store(Request $request)
    {
        $params = $request->except('_token', '_method', 'locales', 'texts');
        $params['slug'] = Str::slug($request->input('name'));
        $page = new Page();
        foreach ($params as $key => $value) {
            $page->$key = $value;
        }
        $page->FBO=$request->input('FBO');
        $page->CLI=$request->input('CLI');
        //text List
        $texts = $request->input('texts');
        if ($texts != null) {
            $text_list = array();
            foreach ($texts as $key => $curr) {
                $text_list[] = [
                    'text' => $curr
                ];
            }
            $page->text_list = $text_list;
        }

        $locales = $request->input('locales');
        foreach ($locales as $locale) {
            $page->locales()->associate(Locale::find($locale));
        }

        $page->save();

        $page->image = $this->copyFileToPublic($request->input('image'));
        $page->video = $this->copyFileToPublic($request->input('video'));
        $page->save();
        return ['status' => 'success', 'page' => $page];

    }

    public function show($id)
    {
        //TODO
    }

    public function edit(Request $request, $id)
    {
        $statuses = Status::all();
        $page = Page::query()->find($id);
        return ['view' => view('cms.page.edit', compact('statuses', 'page'))->render(), 'title' => $page->name];

    }

    public function update(Request $request, $id)
    {
        $params = $request->except('_token', '_method', 'locales', 'texts');
        $params['slug'] = Str::slug($request->input('name'));

        $page = Page::query()->where('_id', $id)->first();
        foreach ($params as $key => $value) {
            $page->$key = $value;
        }
        $page->FBO=$request->input('FBO');
        $page->CLI=$request->input('CLI');
        //text List
        $texts = $request->input('texts');
        if ($texts != null) {
            $text_list = array();
            foreach ($texts as $key => $curr) {
                $text_list[] = [
                    'text' => $curr
                ];
            }
            $page->text_list = $text_list;
        }

        $locales = $request->input('locales');
        $page->locales()->delete();
        foreach ($locales as $locale) {
            $page->locales()->associate(Locale::find($locale));
        }

        $page->save();
        $page->image = $this->copyFileToPublic($request->input('image'));
        $page->video = $this->copyFileToPublic($request->input('video'));
        $page->save();
        return ['status' => 'success', 'page' => $page];
    }

    public function destroy($id)
    {
        $model = Page::query()->where('_id', $id)->first();
        return $model->delete();
    }

    public function up($id)
    {
        $this->parentUp(Page::find($id));
    }

    public function down($id)
    {
        $this->parentDown(Page::find($id));
    }

    public function deleteImage(Request $request, $id)
    {
        $module = Page::find($id);
        $image = $request->get('image');
        $module->$image = null;
        $module->save();

        return ['status' => 'Success'];
    }

}
