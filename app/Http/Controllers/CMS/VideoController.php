<?php

namespace App\Http\Controllers\CMS;

use App\Models\Video;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use App\Models\Locale;
use App\Models\Country;
use App\Models\Category;
use App\Models\MainMenu;



class VideoController  extends CmsController
{
    public function index(Request $request)
    {
        $model = new Video();
        if ($request->ajax() && $request->has('table')) {
            if ($request->has('filters')) {
                return datatables($model->filterSearch($request->input('filters')))->make(true);
            }
            return datatables(Video::query())->make(true);
            ;
        }

        return view('cms.video.index', compact('model'));
    }
    public function create(Request $request)
    {

        $categories = Category::query()->where('module_ids', 'video')->get();
        $menus = MainMenu::all();
        return ['view'=>view('cms.video.create', compact('categories','menus'))->render(), 'title'=>'New Video/Article'];
    }

    public function store(Request $request)
    {
        $params = $request->except('_token', '_method', 'locales', 'countries','categories','menus');
        $params['slug'] = Str::slug($request->input('name'));

        $video = new Video();
        foreach ($params as $key => $value) {
            $video->$key = $value;
        }
        $video->save();

        $video->categories()->attach($request->input('categories'));

        $video->menus()->attach($request->input('menus'));

        $locales = $request->input('locales');
        foreach ($locales as $locale) {
            $video->locales()->associate(Locale::find($locale));
        }
        $countries = $request->input('countries');
        foreach ($countries as $country) {
            $video->countries()->associate(Country::find($country));
        }


        $video->save();


        return ['status'=>'success', 'video'=>$video];
    }

    public function show($id)
    {
        //TODO
    }

    public function edit($id)
    {
        $video = Video::query()->where('_id', $id)->first();
        $categories = Category::query()->where('module_ids', 'video')->get();
        $menus = MainMenu::all();
        return ['view'=>view('cms.video.edit', compact('video','categories','menus'))->render(),
            'title'=>$video->name];
    }

    public function update(Request $request, $id)
    {
        $params = $request->except('_token', '_method','locales', 'countries','categories','menus');        
        $params['slug'] = Str::slug($request->input('name'));

        $video = Video::query()->where('_id', $id)->first();
        foreach ($params as $key => $value) {
            $video->$key = $value;
        }
        
        $video->categories()->sync($request->input('categories'));
        $video->menus()->sync($request->input('menus'));

        $locales = $request->input('locales');
        $video->locales()->delete();
        foreach ($locales as $locale) {
            $video->locales()->associate(Locale::find($locale));
        }
        $countries = $request->input('countries');
        $video->countries()->delete();
        foreach ($countries as $country) {
            $video->countries()->associate(Country::find($country));
        }

        $video->save();
        return ['status'=>'success', 'video'=>$video];
    }

    public function destroy($id)
    {
        $model = Video::query()->where('_id', $id)->first();
        return $model->delete();
    }

    public function up($id)
    {
        $this->parentUp(Video::find($id));
    }
    public function down($id)
    {
        $this->parentDown(Video::find($id));
    }

    public function translate($id)
    {
        //TODO
    }

    public function translateStore(Request $request, $id)
    {
        //TODO
    }
    
    public function deleteImage(Request $request, $id)
    {
        $module = Video::find($id);
        $image = $request->get('image');
        $module->$image = null;
        $module->save();

        return ['status'=>'Success'];
    }
}
