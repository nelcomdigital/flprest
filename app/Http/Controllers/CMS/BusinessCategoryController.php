<?php

namespace App\Http\Controllers\CMS;

use App\Models\BusinessCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class BusinessCategoryController extends Controller
{
    public function index(Request $request)
    {
        $model = new BusinessCategory();
        if ($request->ajax() && $request->has('table')) {
            if ($request->has('filters')) {
                return datatables($model->filterSearch($request->input('filters')))->make(true);
            }
            return datatables(BusinessCategory::query())->make(true);
            ;
        }

        return view('cms.business-category.index', compact('model'));
    }

    public function create(Request $request)
    {
        return ['view'=>view('cms.business-category.create')->render(), 'title'=>'New BusinessCategory'];
    }

    public function store(Request $request)
    {
        $params = $request->except('_token', '_method');
        $params['slug'] = Str::slug($request->input('name'));
        $businessCategory = new BusinessCategory();
        foreach ($params as $key => $value) {
            $businessCategory->$key = $value;
        }
        

        $businessCategory->save();


        return ['status'=>'success', 'businessCategory'=>$businessCategory];
    }

    public function show($id)
    {
        //TODO
    }

    public function edit(Request $request, $id)
    {
        $businessCategory = BusinessCategory::query()->find($id);
        return ['view'=>view('cms.business-category.edit', compact('businessCategory'))->render(), 'title'=>$businessCategory->name];
    }
    public function update(Request $request, $id)
    {
        $params = $request->except('_token', '_method');
        $params['slug'] = Str::slug($request->input('name'));

        $businessCategory = BusinessCategory::query()->where('_id', $id)->first();
        foreach ($params as $key => $value) {
            $businessCategory->$key = $value;
        }

        $businessCategory->save();
        return ['status'=>'success', 'businessCategory'=>$businessCategory];
    }

    public function destroy($id)
    {
        $model = BusinessCategory::query()->where('_id', $id)->first();
        return $model->delete();
    }

    public function up($id)
    {
        $this->parentUp(BusinessCategory::find($id));
    }
    public function down($id)
    {
        $this->parentDown(BusinessCategory::find($id));
    }

    public function deleteImage(Request $request, $id)
    {
        $module = BusinessCategory::find($id);
        $image = $request->get('image');
        $module->$image = null;
        $module->save();

        return ['status'=>'Success'];
    }
}
