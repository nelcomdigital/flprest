<?php

namespace App\Http\Controllers\CMS;

use App\Models\Country;
use App\Models\GroupCountry;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use MongoDB\Operation\Count;


class CountryController extends CmsController
{

   public function index(Request $request)
    {
        $model = new Country();
        if ( $request->ajax() && $request->has('table')){
            if($request->has('filters')){
                return datatables( $model->filterSearch($request->input('filters')) )->make(true);
            }
            return datatables(Country::query())->make(true);;
        }

        return view('cms.country.index',compact('model'));
    }

    public function create(Request $request)
    {

        $groupCountries = GroupCountry::all();
        return ['view'=>view('cms.country.create',compact('groupCountries'))->render(), 'title'=>'New Country'];
    }


    public function store(Request $request)
    {
        foreach ($request->get('name') as $k => $value) {
            $slug [$k] = Str::slug($value);
        }
        $country = new Country();
        $country->name = $request->input('name');
        $country->slug = $slug;
        $country->has_postal = $request->input('has_postal');
        $country->has_id_number = $request->input('has_id_number');
        $country->min_cc = $request->input('min_cc');
        $country->region_code = $request->input('region_code');
        $country->products_code = $request->input('products_code');
        $country->country_code = $request->input('country_code');
        $country->product_type = $request->input('product_type');
        $country->pack_product= $request->input('pack_product');        
        $country->relay_delivery= $request->input('relay_delivery');
        // $country->relay_delivery_description= $request->input('relay_delivery_description');
        $country->home_delivery= $request->input('home_delivery');
        // $country->home_delivery_description = $request->input('home_delivery_description');
    

        $country->group_country()->associate($request->input('group_country_id'));
        $country->status()->associate($request->input('status_id'));
        $country->save();



        return ['status'=>'success', 'country'=>$country];
    }

    public function show($id)
    {
        //TODO
    }

    public function edit($id)
    {
        $groupCountries = GroupCountry::all();
        $country = Country::query()->where('_id', $id)->first();
        return ['view'=>view('cms.country.edit', compact('country','groupCountries'))->render(),
            'title'=>$country->name[session()->get('current_locale')]];
    }

    public function update(Request $request, $id)
    {
        // foreach ($request->get('name') as $k => $value) {
        //     $slug [$k] = Str::slug($value);
        // }
        $country = Country::query()->where('_id', $id)->first();
        $country->name = $request->input('name');
        // $country->slug = $slug;
        
        $country->has_postal = $request->input('has_postal');
        $country->min_cc = $request->input('min_cc');
        $country->region_code = $request->input('region_code');
        $country->products_code = $request->input('products_code');
        $country->country_code = $request->input('country_code');
        $country->product_type = $request->input('product_type');
        $country->has_id_number = $request->input('has_id_number');
        $country->pack_product= $request->input('pack_product');
        $country->relay_delivery= $request->input('relay_delivery');
        // $country->relay_delivery_description= $request->input('relay_delivery_description');
        $country->home_delivery= $request->input('home_delivery');
        // $country->home_delivery_description= $request->input('home_delivery_description');
        $country->group_country()->associate($request->input('group_country_id'));
        $country->status()->associate($request->input('status_id'));
        $country->save();

        return ['status'=>'success', 'country'=>$country];
    }

    public function destroy($id)
    {
        $model = Country::query()->where('_id', $id)->first();
        return $model->delete();
    }

    public function up($id)
    {
        $this->parentUp(Country::find($id));
    }
    public function down($id)
    {
        $this->parentDown(Country::find($id));
    }

}
