<?php

namespace App\Http\Controllers\CMS;

use App\Models\Reminder;
use App\Models\ReminderType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;


class ReminderController extends  CmsController
{

   public function index(Request $request)
    {
        $model = new Reminder();
        if ( $request->ajax() && $request->has('table')){
            if($request->has('filters')){
                return datatables( $model->filterSearch($request->input('filters')) )->make(true);
            }
            return datatables(Reminder::query())->make(true);;
        }

        return view('cms.reminder.index',compact('model'));
    }

    public function create(Request $request)
    {
        $reminderTypes = ReminderType::query()->get();
        return ['view'=>view('cms.reminder.create', compact('reminderTypes'))->render(), 'title'=>'New Reminder'];
    }


    public function store(Request $request)
    {
        $params = $request->except('_token', '_method');
        $params['slug'] = Str::slug($request->input('name'));
        $reminder = new Reminder();
        foreach($params as $key => $value){
            $reminder->$key = $value;
        }
        $reminder->save();

        return ['status'=>'success', 'reminder'=>$reminder];
    }

    public function show($id)
    {
        //TODO
    }

    public function edit($id)
    {
        $reminder = Reminder::query()->where('_id', $id)->first();
        $reminderTypes = ReminderType::query()->get();
        return ['view'=>view('cms.reminder.edit', compact('reminder','reminderTypes'))->render(), 'title'=>$reminder->name];
    }

    public function update(Request $request, $id)
    {
        $params = $request->except('_token', '_method');
        $params['slug'] = Str::slug($request->input('name'));

        $reminder = Reminder::query()->where('_id', $id)->first();
        foreach($params as $key => $value){
            $reminder->$key = $value;
        }
        $reminder->save();
        return ['status'=>'success', 'reminder'=>$reminder];
    }

    public function destroy($id)
    {
        $model = Reminder::query()->where('_id', $id)->first();
        return $model->delete();
    }

    public function up($id)
    {
        $this->parentUp(Reminder::find($id));
    }
    public function down($id)
    {
        $this->parentDown(Reminder::find($id));
    }

}
