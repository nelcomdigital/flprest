<?php

namespace App\Http\Controllers\CMS;

use App\Models\Locale;
use App\Models\Order;
use App\Models\Status;
use App\User;
use Illuminate\Http\Request;


class OrderController extends CmsController
{

   public function index(Request $request)
    {
        $model = new Order();
        if ( $request->ajax() && $request->has('table')){
            if($request->has('filters')){
                return datatables( $model->filterSearch($request->input('filters')) )->make(true);
            }
            return datatables(Order::query()->with(['user']))->make(true);
        }

        return view('cms.order.index',compact('model'));
    }


}
