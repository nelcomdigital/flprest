<?php

namespace App\Http\Controllers\CMS;

use App\Models\Locale;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class RoleController extends CmsController
{

   public function index(Request $request)
    {
        $model = new Role();
        if ( $request->ajax() && $request->has('table')){
            if($request->has('filters')){
                return datatables( $model->filterSearch($request->input('filters')) )->make(true);
            }
            return datatables(Role::query())->make(true);;
        }


        return view('cms.role.index',compact('model'));
    }

    public function create(Request $request)
    {
        return ['view'=>view('cms.role.create')->render(), 'title'=>'New Role'];
    }

    public function store(Request $request)
    {
        $params = $request->except('_token', '_method');
        $params['slug'] = Str::slug($request->input('name'));
        $params['permissions'] = (array) $request->input('permissions');
        $role = new Role();
        foreach($params as $key => $value){
            $role->$key = $value;
        }
        $role->save();
        return ['status'=>'success', 'role'=>$role];
    }

    public function show($id)
    {
        //TODO
    }

    public function edit($id)
    {
        $role = Role::find($id);
        return ['view'=>view('cms.role.edit', compact('role'))->render(), 'title'=>$role->name];
    }

    public function update(Request $request, $id)
    {
        $params = $request->except('_token', '_method');
        $params['slug'] = Str::slug($request->input('name'));
        $params['permissions'] = (array) $request->input('permissions');
        $role = Role::query()->find($id);
        foreach($params as $key => $value){
            $role->$key = $value;
        }
        $role->save();

        return ['status'=>'success', 'role'=>$role];
    }

    public function destroy($id)
    {
        $model = Role::query()->where('_id', $id)->first();
        return $model->delete();
    }

    public function up($id)
    {
        $this->parentUp(Role::find($id));
    }
    public function down($id)
    {
        $this->parentDown(Role::find($id));
    }


}
