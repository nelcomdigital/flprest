<?php

namespace App\Http\Controllers\CMS;

use App\Models\DeliveryMode;
use App\Models\Country;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class DeliveryModeController extends CmsController
{
   public function index(Request $request)
    {
        $model = new DeliveryMode();
        if ($request->ajax() && $request->has('table')) {
            if ($request->has('filters')) {
                return datatables($model->filterSearch($request->input('filters')))->make(true);
            }
            return datatables(DeliveryMode::query())->make(true);
            ;
        }

        return view('cms.delivery-mode.index', compact('model'));

    }

    public function create(Request $request)
    {
     
        $countries = Country::all();

        return ['view'=>view('cms.delivery-mode.create',compact('countries'))->render(), 'title'=>'New Delivery Mode'];

    }


    public function store(Request $request)
    {

        $params = $request->except('_token', '_method');
        
        $deliveryMode = new DeliveryMode();
        foreach ($params as $key => $value) {
            $deliveryMode->$key = $value;
        }
        $deliveryMode->save();
        
        return ['status' => 'success', 'deliveryMode' => $deliveryMode];
    }

    public function show($id)
    {
        //TODO
    }

    public function edit($id)
    {
        $deliveryMode = DeliveryMode::query()->where('_id', $id)->first();
        $countries = Country::all();

        return ['view'=>view('cms.delivery-mode.edit', compact('deliveryMode','countries'))->render()];
    }

    public function update(Request $request, $id)
    {
        $params =  $request->except('_token', '_method');


        $deliveryMode = DeliveryMode::query()->where('_id', $id)->first();

        foreach($params as $key => $value){
            $deliveryMode->$key = $value;
        }
        $deliveryMode->save();


        return ['status'=>'success', 'deliveryMode'=>$deliveryMode];
    }

    public function destroy($id)
    {
        $model = DeliveryMode::query()->where('_id', $id)->first();
        return $model->delete();

    }

    public function up($id)
    {
        $this->parentUp(DeliveryMode::find($id));
    }
    public function down($id)
    {
        $this->parentDown(DeliveryMode::find($id));
    }

    public function translate($id)
    {
        //TODO
    }

    public function translateStore(Request $request, $id)
    {
            //TODO
    }
    
    public function deleteImage(Request $request, $id)
    {
        $module = DeliveryMode::find($id);
        $image = $request->get('image');
        $module->$image = null;
        $module->save();

        return ['status'=>'Success'];
    }

}
