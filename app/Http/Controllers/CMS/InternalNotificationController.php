<?php

namespace App\Http\Controllers\CMS;

use App\Models\InternalNotification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class InternalNotificationController extends CmsController
{
    public function index(Request $request){

        $model = new InternalNotification();
        if ( $request->ajax() && $request->has('table')){
            if($request->has('filters')){
                return datatables( $model->filterSearch($request->input('filters')) )->make(true);
            }
            return datatables(dataHelper(InternalNotification::class , [] , [] ,0 , [] , [] , false)->orderBy('num','asc'))->make(true);
        }
        return view('cms.internal-notification.index',compact('model'));
    }


    public function create(Request $request)
    {
        return ['view'=>view('cms.internal-notification.create')->render(), 'title'=>'New Role'];
    }

    public function store(Request $request)
    {
        // $this->validate($request, [
        //     'num'=>'required|unique:notification_roles',
        // ]);
        $params = $request->except('_token','num', '_method', 'locales', 'domains', 'countries','values', 'notes','currency','start_time', 'end_time');
        $params['slug'] = str_slug($request->input('name'));
        $role = new InternalNotification();
        foreach($params as $key => $value){
            $role->$key = $value;
        }
        $role->FBO=$request->input('FBO');
        $role->CLI=$request->input('CLI');
        $role->android=$request->input('android');
        $role->ios=$request->input('ios');
        $role->save();

        //loggers
        // $this->setLoggers($role, $request->input('domains'), $request->input('locales'), $request->input('countries'));

        return ['status'=>'success', 'role'=>$role];
    }

    public function show($id)
    {
        //TODO
    }

    public function edit($id)
    {
        $role = InternalNotification::find($id);
        return ['view'=>view('cms.internal-notification.edit', compact('role'))->render(), 'title'=>$role->name];
    }
    public function update(Request $request, $id)
    {
        $params = $request->except('_token','num', '_method', 'locales', 'domains', 'countries','values', 'notes','currency','start_time', 'end_time');
        $role = InternalNotification::find($id);
        $params['slug'] = str_slug($request->input('name'));

        foreach($params as $key => $value){
            $role->$key = $value;
        }
        $role->FBO=$request->input('FBO');
        $role->CLI=$request->input('CLI');
        $role->android=$request->input('android');
        $role->ios=$request->input('ios');
        $role->save();

        //loggers
        // $this->updateLoggers($role, $request->input('domains'), $request->input('locales'), $request->input('countries'));

        return ['status'=>'success', 'role'=>$role];
    }

    public function destroy($id)
    {
        $model = InternalNotification::query()->where('_id', $id)->first();
        return $model->delete();
    }

    public function up($id)
    {
        $this->parentUp(InternalNotification::find($id));
    }
    public function down($id)
    {
        $this->parentDown(InternalNotification::find($id));
    }

    public function translate($id)
    {

        $model = InternalNotification::find($id);
        $route = route('internal-notification.translate',$id);
        return ['view'=>view('cms.layouts.blocks.translation.translation',
            compact('model', 'route'))->render(), 'title'=>$model->name.'\'s Translate'];

    }

    public function translateStore(Request $request, $id)
    {
        $model = InternalNotification::find($id);
        $params = $request->except('_token', '_method');
        $model->translations = $params ;
        foreach ($params as $k => $value ){
            $count = InternalNotification::query()->where('_id',$model->_id)
                ->where('locales.code',$k)->count();
            if ( $count == 0 ){
                $model->locales()->associate(Locale::query()->where('code',$k)->first());
            }
        }
        return ['status'=>'success', 'role'=>$model->save()];

    }

    public function deleteImage(Request $request, $id)
    {
        $module = InternalNotification::find($id);
        $image = $request->get('image');
        $module->$image = null;
        $module->save();

        return ['status'=>'Success'];
    }

}
