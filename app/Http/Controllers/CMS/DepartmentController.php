<?php

namespace App\Http\Controllers\CMS;

use App\Models\Country;
use App\Models\Department;
use App\Models\GroupCountry;
use App\Models\Region;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use MongoDB\Operation\Count;


class DepartmentController extends CmsController
{

   public function index(Request $request)
    {
        $model = new Department();
        if ( $request->ajax() && $request->has('table')){
            if($request->has('filters')){
                return datatables( $model->filterSearch($request->input('filters')) )->make(true);
            }
            return datatables(Department::query())->make(true);;
        }

        return view('cms.department.index',compact('model'));
    }

    public function create(Request $request)
    {

        $regions = Region::all();
        return ['view'=>view('cms.department.create',compact('regions'))->render(), 'title'=>'New Department'];
    }


    public function store(Request $request)
    {
        foreach ($request->get('name') as $k => $value) {
            $slug [$k] = Str::slug($value);
        }
        $department = new Department();
        $department->name = $request->input('name');
        $department->slug = $slug;
        $department->region()->associate($request->input('region_id'));
        $department->save();



        return ['status'=>'success', 'department'=>$department];
    }

    public function show($id)
    {
        //TODO
    }

    public function edit($id)
    {
        $regions = Region::all();
        $department = Department::query()->where('_id', $id)->first();
        return ['view'=>view('cms.department.edit', compact('department','regions'))->render(),
            'title'=>$department->name[session()->get('current_locale')]];
    }

    public function update(Request $request, $id)
    {
        foreach ($request->get('name') as $k => $value) {
            $slug [$k] = Str::slug($value);
        }
        $department = Department::query()->where('_id', $id)->first();
        $department->name = $request->input('name');
        $department->slug = $slug;
        $department->region()->associate($request->input('region_id'));
        $department->save();

        return ['status'=>'success', 'department'=>$department];
    }

    public function destroy($id)
    {
        $model = Department::query()->where('_id', $id)->first();
        return $model->delete();
    }

    public function up($id)
    {
        $this->parentUp(Department::find($id));
    }
    public function down($id)
    {
        $this->parentDown(Department::find($id));

    }

}
