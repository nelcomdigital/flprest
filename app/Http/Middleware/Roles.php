<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class Roles
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $roleArray = array();

        $modules = getModules();
        if ( auth()->check() && !auth()->user()->isUser()) {
            $roles = auth()->user()->roles;
            foreach($roles as $role){
                if($role->slug == 'super-admin' ){
                    $permissions = array();
                    foreach ($modules as $module ){
                        $rm = array();
                        $rm['slug'] = $module['slug'];
                        $rm['show'] = true;
                        $rm['add'] = true;
                        $rm['edit'] = true;
                        $rm['delete'] = true;
                        $rm['edit_all'] = true;
                        $rm['delete_all'] = true;
                        $rm['show_all'] = true;
                        $permissions[] = $rm;
                    }
                    $request->role_array = ['super-admin'];
                    $role->permissions = $permissions;
                    $request->permission_roles = $roles;
                    return $next($request);
                }
                $roleArray[] = $role->slug;
            }

            $request->role_array= $roleArray;

            $request->permission_roles = $roles;


                return $next($request);

        }else if (auth()->check() && auth()->user()->isUser()){
            Auth::logout();
            return redirect()->route('welcome');
        }
        else{
            return redirect()->route('home');
        }
    }
}
