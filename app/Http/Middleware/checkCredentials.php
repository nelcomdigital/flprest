<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Str;

class checkCredentials
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->bearerToken();
//

        $decrypted = Crypt::decryptString($token);
        $credentails = explode(',',$decrypted);
//            $url = 'https://apirest.flpbis.com/api/Login';
//            $ch = curl_init($url);
//            $jsonData =[
//                'login'=>$credentails[3],
//                'pwd'=>$credentails[5]
//            ];
//            $jsonDataEncoded = json_encode($jsonData);
//            curl_setopt($ch, CURLOPT_POST, 1);
//            curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
//            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
//            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
//            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//            $result = curl_exec($ch);
//            $result = json_decode($result);
//            if ( $result->success ){
//
//                $request->user_id = $result->infos->Num;
//                return $next($request);
//            }else{
//                return response()->json($result,500);
//            }
        try{
            if ( $credentails[1] != null && $credentails[1] != '' && $credentails[3] != null && $credentails[3] != ''){

                if ( sizeof($credentails )== 7  && !Str::contains($request->url(),'password/reset') ){
                    return response()->json(['message'=>'unauthenticated'] ,422);
                }

                $request->user_id = $credentails[1];
                $request->type = $credentails[3];
                try{
                    if ( sizeof($credentails) ==7 )
                    $request->server_pin = $credentails[5];

                }catch(\Exception $e){

                }
                return $next($request);
            }else{
                return response()->json(['message'=>'unauthenticated'] ,422);
            }

        }catch (\Exception $e){
            return response()->json(['message'=>'unauthenticated'] ,422);
        }
//            return $next($request);


    }
}
