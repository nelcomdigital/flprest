<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Str;

class CheckNotifToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->header('Token');

        $secretKey = 'owPDxXzrwQN4P3kMouXJ6LlI0RewzW';

        $date = date('Y-m-dH');

        $rightToken = sha1($secretKey . '-' . $date);

        try{
            if ($token == $rightToken){
                return $next($request);
            }else{
                return response()->json(['success'=>false,'message'=>'unauthenticated'] ,422);
            }

        }catch (\Exception $e){
            return response()->json(['success'=>false, 'message'=>'unauthenticated'] ,422);
        }


    }
}
