<?php
/**
 * Created by PhpStorm.
 * User: Rabee
 * Date: 10/05/2019
 * Time: 13:19
 */
return [
   'administration'=>'<i class="fa fa-user"></i>',
   'configuration-management'=>'<i class="fa fa-wrench"></i>',
   'content-management'=>'<i class="fa fa-pencil"></i>',
   'business-management'=>'<i class="fa fa-suitcase"></i>',
   'ecommerce'=>'<i class="fa fa-cart-arrow-down"></i>',
   'user'=>'<i class="fa fa-users"></i>',
   'notification-center'=>'<i class="fa fa-bell"></i>',
];
