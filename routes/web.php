<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;


Route::get('/', 'Frontend\HomeController@index')->name('welcome');
Route::get('/ref/{id?}', 'Frontend\HomeController@index');
Route::post('main', 'Frontend\HomeController@main')->name('main');
Route::post('put-session', 'Frontend\HomeController@putSession')->name('session.put');
Route::post('save-address/{id}', 'Frontend\HomeController@saveAddress')->name('save-address');
Route::post('put-session-qty', 'Frontend\HomeController@putSessionQty')->name('session.put.qty');
Route::get('product-list/{slug?}', 'Frontend\HomeController@products')->name('products');
Route::post('product/{id?}', 'Frontend\HomeController@product')->name('product.detail');
Route::get('product/{id?}', 'Frontend\HomeController@product')->name('product.detail');
Route::get('cart/{order_id?}', 'Frontend\HomeController@cart')->name('cart');
Route::get('review/{order_id?}', 'Frontend\HomeController@reviewCart')->name('review');
Route::get('address/{order_id?}', 'Frontend\HomeController@address')->name('address');





Route::get('later-buy', 'Frontend\HomeController@buyWithin72Hours')->name('later-buy');
Route::get('thank-you', 'Frontend\HomeController@thankYou')->name('thank-you');
Route::get('change-lang/{lang}', 'Frontend\HomeController@switchLanguage')->name('switch-language');
//Route::get('select-city/{zip?}', 'Frontend\HomeController@selectCity')->name('select-city');
Route::get('select-city-review/{country?}/{zip?}', 'Frontend\HomeController@selectCityReview')->name('select-city-review');
Route::get('testEmail', 'Frontend\HomeController@testEmail')->name('testEmail');
Route::get('select-country/{id?}', 'Frontend\HomeController@selectCountry')->name('select.country');
Route::get('select-region/{id?}', 'Frontend\HomeController@selectRegion')->name('select.region');
Route::get('select-department/{id?}', 'Frontend\HomeController@selectDepartment')->name('select.department');
Route::get('select-city', 'Frontend\HomeController@selectCity')->name('select.city');
Route::get('back-check-point/{id}', 'Frontend\HomeController@checkPoint')->name('select.checkpoint');
Route::get('check-check-point', 'Frontend\HomeController@checkCheckPoint')->name('check.checkpoint');
Route::get('check-user/{email?}', 'Frontend\HomeController@checkInternalUser')->name('check.internal.user');
Route::get('calculate-total', 'Frontend\HomeController@calculateTotalAjax')->name('ajax.calculate.total');

Route::get('payment/{id}', 'Frontend\HomeController@payment')->name('payment');

Route::get('process-payment', 'Frontend\HomeController@confirmPayment')->name('payment.process');
Route::get('payment-success/{id}', 'Frontend\HomeController@successPayment')->name('payment.success');
Route::get('payment-cancel', 'Frontend\HomeController@failPayment')->name('payment.cancel');
Route::get('payment-error', 'Frontend\HomeController@failPayment')->name('payment.error');
Route::get('payment-refused', 'Frontend\HomeController@failPayment')->name('payment.refused');
Route::get('blockAllOldCustomerReview', 'Frontend\HomeController@blockAllOldCustomerReview')->name('blockAllOldCustomerReview');

Route::get('customer-review/{id}', 'Frontend\HomeController@getCustomerReview')->name('customer-review');
Route::get('customer-review-checkout/{id}', 'Frontend\HomeController@getCustomerReviewCheckout')->name('customer-review-checkout');
Route::get('customer-review-shipping/{id}', 'Frontend\HomeController@getCustomerReviewShipping')->name('customer-review-shipping');
Route::get('terms-conditions', 'Frontend\HomeController@termsAndConditions')->name('terms-conditions');

//Auth::routes();

//Route::get('__info', function (){
//    echo phpinfo();
//});


