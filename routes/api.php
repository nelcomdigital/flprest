<?php


use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::get('connect/{type}/{cat?}/{region?}/{letter?}', 'Frontend\Api\ApiController@connect');
Route::get('testtest', 'Frontend\Api\ApiController@test');
Route::get('check/email/{email?}', 'Frontend\Api\ApiController@checkEmail')->name('check.email');
Route::get('check/distributor/{number?}', 'Frontend\Api\ApiController@checkDistributor')->name('check.distributor');
Route::get('client', 'Frontend\Api\ApiController@getClient');
Route::get('product-inject', 'Frontend\Api\ApiController@getProducts')->name('products.inject');
Route::get('order/{code}/inject', 'Frontend\Api\ApiController@confirmPayment')->name('products.confirmPayment');


Route::get('vente/getViewSalesDetail', 'Api\SaleController@getViewSalesDetail');
Route::post('vente/setComment/webView', 'Api\SaleController@addWebComment')->name('vente.setComment');
Route::get('vente/comment-success/{rr}', 'Api\SaleController@commentSuccess')->name('vente.commentSuccess');

// FOR THE APPLICATION
Route::post('login', 'Api\UserController@login');

Route::get('password/forgot/{email}', 'Api\UserController@forgotPassword');
Route::get('countries', 'Api\AssetController@getCountries');

//Payment
Route::get('payment-url/{token}', 'Api\AssetController@paymentUrl')->name('payment.url');
Route::get('payment-status/{status}', 'Api\AssetController@paymentStatus')->name('payment.status');
Route::get('payment-validate', 'Api\AssetController@paymentServer')->name('payment.validate');
Route::get('back-check-point', 'Api\AssetController@checkPoint')->name('api.select.checkpoint');
Route::get('checkVersion', 'Api\AssetController@checkVersion');
Route::get('checkURL', 'Api\AssetController@checkURL');
Route::get('checkMaintainance', 'Api\AssetController@checkMaintainance');
Route::get('getForceUser/{num}', 'Api\UserController@getForceUser');

Route::post('send-notification', 'Api\AssetController@sendNotification');
Route::middleware(['notif-token'])->group(function () {
    Route::post('push-notification', 'Api\AssetController@pushNotification');
});


Route::middleware(['app-token'])->group(function () {
    Route::get('me', 'Api\UserController@profile');
    Route::get('sponsorInfo', 'Api\UserController@sponsorInfo');
    Route::post('device/token', 'Api\UserController@deviceToken');
    Route::post('device/remove-token', 'Api\UserController@removeDeviceToken');
    Route::post('me', 'Api\UserController@setProfile');
    Route::post('password/reset', 'Api\UserController@resetPassword');
    Route::get('isSub', 'Api\UserController@isSub');
    Route::post('upload-profile-image', 'Api\AssetController@uploadProfileImage');
    Route::get('get-profile-image', 'Api\AssetController@getProfileImage');
    Route::get('main-menu', 'Api\AssetController@getMainMenu');
    Route::get('sub-main-menu/{id}', 'Api\AssetController@getSubMainMenu');
    Route::post('get-country-delivery', 'Api\AssetController@getCountryDelivery');
    Route::get('get/main-menu/{id}', 'Api\AssetController@getMainMenuById');
    Route::get('/slider/{mainMenuID?}', 'Api\AssetController@getMainMenuSlider');
    Route::get('product-categories', 'Api\AssetController@getProductCategories');
    Route::post('products-by-category', 'Api\AssetController@getProductByCategories');
    Route::get('categories-list', 'Api\AssetController@getCategoriesList');
    Route::post('all/products', 'Api\AssetController@getAllProducts');
    Route::post('verifStockRef', 'Api\AssetController@verifStockRef');
    Route::post('createCommand', 'Api\AssetController@createCommand');
    Route::post('addCommand', 'Api\AssetController@addCommand');
    Route::post('deleteCommand', 'Api\AssetController@deleteCommand');
    Route::post('setLivraison', 'Api\AssetController@setDelivery');
    Route::get('getAllCmde', 'Api\AssetController@getAllCmde');
    Route::post('getCmde', 'Api\AssetController@getCmde');
    Route::post('getAllCmdeClient', 'Api\AssetController@getAllCmdeClient');
    Route::post('getVenteCmdeClient', 'Api\AssetController@getVenteCmdeClient');
    Route::post('getFacture', 'Api\AssetController@getFacture');
    Route::post('verifCmde', 'Api\AssetController@verifCmde');

    Route::post('getCart', 'Api\AssetController@getCart');
    Route::post('getPromo', 'Api\AssetController@getPromo');
    Route::post('ctrlCart', 'Api\AssetController@ctrlCart');
    Route::post('validCommand', 'Api\AssetController@validCommand');
    Route::post('getAllAddressLiv', 'Api\AssetController@getAllAddressLiv');
    Route::post('directDiscount', 'Api\UserController@getDiscount');

    Route::post('listeArticles', 'Api\UserController@getListeArticles');

    Route::post('addAddressLiv', 'Api\AssetController@createAddressLiv');
    Route::post('setAddressLiv', 'Api\AssetController@updateAddressLiv');
    Route::post('deleteAddressLiv', 'Api\AssetController@deleteAddressLiv');
    Route::post('getArticleAuto', 'Api\UserController@getArticleAuto');
    Route::post('getFraisLivraison', 'Api\UserController@getFraisLivraison');


    Route::post('addFavorite', 'Api\AssetController@addFavorite');
    Route::post('deleteFavorite', 'Api\AssetController@deleteFavorite');
    Route::post('getFavorite', 'Api\AssetController@getFavorite');

    //Contacts
    Route::post('saveContact', 'Api\ContactController@addContact');
    Route::post('updateContact', 'Api\ContactController@updateContact');
    Route::post('deleteContact', 'Api\ContactController@deleteContact');
    Route::get('getContacts', 'Api\ContactController@getContacts');
    //UserObject
    Route::post('save/userObject', 'Api\UserObjectController@addUserObject');
    Route::post('getUserObject', 'Api\UserObjectController@getUserObject');
    Route::post('deleteUserObject', 'Api\UserObjectController@deleteUserObject');
    //Videos
    Route::post('getVedios', 'Api\AssetController@getVedios');
    Route::get('vedio/{id}', 'Api\AssetController@getVedio');
    Route::get('vedioCategories', 'Api\AssetController@getVedioCategories');
    Route::post('VediosByCategory', 'Api\AssetController@getVediosByCategory');

    //Groups
    Route::post('createGroup', 'Api\ContactController@addGroup');
    Route::post('updateGroup', 'Api\ContactController@updateGroup');
    Route::post('sendGroupMessage', 'Api\ContactController@sendGroupMessage');
    Route::post('addToGroup', 'Api\ContactController@addToGroup');
    Route::get('getGroups', 'Api\ContactController@getGroups');
    Route::post('groupDetails', 'Api\ContactController@groupDetail');
    Route::post('deleteGroup', 'Api\ContactController@deleteGroup');
    //Reminder
    Route::post('addReminder', 'Api\ContactController@addReminder');
    Route::get('getReminders', 'Api\ContactController@getReminders');
    Route::post('deleteReminder', 'Api\ContactController@deleteReminder');
    Route::post('addReminderToContact', 'Api\ContactController@addReminderToContact');

    //Client
    Route::post('vente/authCreateClient', 'Api\ClientController@authCreateClient');
    Route::post('vente/getClientList', 'Api\ClientController@getClientList');
    Route::post('vente/addClient', 'Api\ClientController@addClient');
    Route::post('vente/setClient', 'Api\ClientController@setClient');
    Route::post('vente/deleteClient', 'Api\ClientController@deleteClient');
    Route::post('vente/getDeliveryAddress', 'Api\ClientController@getDeliveryAddress');
    Route::post('vente/addDeliveryAddress', 'Api\ClientController@addDeliveryAddress');
    Route::post('vente/setDeliveryAddress', 'Api\ClientController@setDeliveryAddress');
    Route::post('vente/deleteDeliveryAddress', 'Api\ClientController@deleteDeliveryAddress');
    Route::get('vente/getSalesMeeting', 'Api\ClientController@getSalesMeeting');
    Route::post('vente/addSalesMeeting', 'Api\ClientController@addSalesMeeting');
    Route::post('vente/setSalesMeeting', 'Api\ClientController@setSalesMeeting');
    Route::post('vente/deleteSalesMeeting', 'Api\ClientController@deleteSalesMeeting');
    Route::post('vente/getMeetingParticipant', 'Api\ClientController@getMeetingParticipant');
    Route::post('vente/addMeetingParticipant', 'Api\ClientController@addMeetingParticipant');
    Route::post('vente/deleteMeetingParticipant', 'Api\ClientController@deleteMeetingParticipant');

    Route::post('addOrUpdateOtherMeeting', 'Api\ClientController@addOrUpdateOtherMeeting');
    Route::get('deleteOtherMeeting/{id}', 'Api\ClientController@deleteOtherMeeting');
    Route::get('getAllUserMeetings', 'Api\ClientController@GetUserOtherMeetings');
    Route::get('getOtherMeeting/{id}', 'Api\ClientController@getUserOtherMeetingByID');
    Route::get('testdates', 'Api\ClientController@testdates');
    //Sale
    Route::post('vente/createVente', 'Api\SaleController@addVente');
    Route::post('vente/addLigneVente', 'Api\SaleController@addSaleLine');
    Route::post('vente/setRemiseGeneral', 'Api\SaleController@setRemiseGeneral');
    Route::post('vente/setComment', 'Api\SaleController@addComment');
    Route::post('vente/setFraisPort', 'Api\SaleController@addShippingCosts');
    Route::post('vente/setPaiement', 'Api\SaleController@setPayment');
    Route::post('vente/setLivraison', 'Api\SaleController@setDelivery');
    Route::post('vente/getVenteList', 'Api\SaleController@getSalesList');
    Route::post('vente/getSalesDoc', 'Api\SaleController@getSalesDoc');
    Route::post('vente/getVenteDetail', 'Api\SaleController@getSalesDetail');
    Route::post('vente/deleteVente', 'Api\SaleController@deleteSales');
    Route::post('vente/setVentePay', 'Api\SaleController@setSalesPay');
    Route::post('vente/setVenteDelivery', 'Api\SaleController@setSalesDelivery');
    Route::post('vente/listeArticles', 'Api\SaleController@getSalesListeArticles');
    Route::get('vente/getModePaiement', 'Api\SaleController@getModePayment');
    Route::post('vente/signerVente', 'Api\SaleController@signerVente');

    //Offer
    Route::post('offer/verifCode', 'Api\SaleController@verifyPromoCode');
    Route::post('offer/addOffre', 'Api\SaleController@addOffre');
    Route::get('offer/getOffre', 'Api\SaleController@getOffre');
    Route::post('offer/getOffreDetail', 'Api\SaleController@getOffreDetail');
    Route::post('offer/setOffre', 'Api\SaleController@setOffre');
    Route::post('offer/deleteOffre', 'Api\SaleController@deleteOffre');
    Route::post('offer/addCodes', 'Api\SaleController@addCodes');
    Route::get('offer/getProductsClient', 'Api\SaleController@getProductsClient');
    Route::get('offer/getParamsRemise', 'Api\SaleController@getParamsRemise');
    Route::get('offer/getConditionClients', 'Api\SaleController@getConditionClients');
    Route::post('offer/getOfferClients', 'Api\SaleController@getOfferClients');

    //Formation
    Route::get('agenda/formations', 'Api\FormationController@getFormations');
    Route::get('agenda/events', 'Api\FormationController@getEvents');
    Route::get('event/categories', 'Api\FormationController@eventCategories');
    Route::post('event/category-info', 'Api\FormationController@eventsCategoriesInfo');
    Route::post('formations/categories', 'Api\FormationController@formationCategories');
    Route::post('formation/category-info', 'Api\FormationController@formationsCategoriesInfo');
    Route::get('formation/labels', 'Api\FormationController@getFormationsLabels');
    Route::post('formation/label-by-id', 'Api\FormationController@getFormationsLabelById');
    Route::get('formation/podcasts/themes', 'Api\FormationController@getPodcastsThemes');
    Route::post('formation/podcasts/theme-by-id', 'Api\FormationController@getPodcastsThemeById');
    Route::post('formation/inscription', 'Api\FormationController@formationsInscription');
    Route::post('event/inscription', 'Api\FormationController@eventsInscription');
    Route::post('formation/product-association', 'Api\FormationController@productAssociation');


    Route::get('formations/{category_id}/{country_iso}', 'Api\FormationController@formationsByCategoryAndCountryIso');
    Route::get('formations/themes', 'Api\FormationController@formationsThemes');

    Route::post('formations/webinaires/country-iso', 'Api\FormationController@webinairesByCountry');

    Route::post('formations/webinaires/month/country_iso', 'Api\FormationController@webinairesOfTheMonthByCountry');
    Route::post('formations/webinaires/webinaire-id', 'Api\FormationController@webinairesById');
    Route::post('formations/formation-id', 'Api\FormationController@formationByID');
    Route::post('event/event-id', 'Api\FormationController@eventByID');
    Route::post('formations/webinaires/countries', 'Api\FormationController@webinairesCountries');
    Route::get('formations/webinaires/month-1/{country_iso}', 'Api\FormationController@webinairesOfTheMonthByCountry');



    Route::get('formationsTest', 'Api\FormationController@formationsTest');


    //Nestor
    Route::get('nestor/services', 'Api\NestorController@getNestorServices');
    Route::get('nestor/commandes', 'Api\NestorController@getNestorCommandes');
    Route::post('nestor/ticket-create', 'Api\NestorController@createNestorTicket');


    //Search
    Route::post('searchProducts', 'Api\AssetController@searchProducts');
    Route::post('searchContacts', 'Api\AssetController@searchContacts');
    Route::post('getArticlesByRef', 'Api\AssetController@getArticlesByRef');
    Route::post('searchAddress', 'Api\AssetController@searchAddress');
    Route::post('searchGeneral', 'Api\AssetController@searchGeneral');
    //Statistics
    Route::post('statistics/getNbClient', 'Api\AssetController@getNbClient');
    Route::post('statistics/getNbNewClient', 'Api\AssetController@getNbNewClient');
    Route::post('statistics/getMontantMoyVente', 'Api\AssetController@getMontantMoyVente');
    Route::post('statistics/getMontantMoyBoutique', 'Api\AssetController@getMontantMoyBoutique');
    Route::post('statistics/getNbClientCmd', 'Api\AssetController@getNbClientCmd');
    Route::post('statistics/getTopProduitVente', 'Api\AssetController@getTopProduitVente');
    Route::post('statistics/getCC', 'Api\AssetController@getCC');
    Route::post('statistics/getCCBoutique', 'Api\AssetController@getCCBoutique');
    Route::post('statistics/addFrais', 'Api\AssetController@addFrais');
    Route::post('statistics/setFrais', 'Api\AssetController@setFrais');
    Route::post('statistics/getFrais', 'Api\AssetController@getFrais');
    Route::post('statistics/deleteFrais', 'Api\AssetController@deleteFrais');
    Route::get('statistics/getListFrais', 'Api\AssetController@getListFrais');
    Route::post('statistics/getBalance', 'Api\AssetController@getBalance');
    Route::post('statistics/getCmdDetail', 'Api\AssetController@getCmdDetail');
    Route::post('statistics/getBalanceDetail', 'Api\AssetController@getBalanceDetail');


    //Payment
    Route::get('payment/{id}', 'Api\AssetController@requestPayment')->name('payment.request');
    //chronopost

    Route::post('chronopost', 'Api\AssetController@chronopost');


    Route::get('business/categories', 'Api\AssetController@BusinessCategories');
    Route::get('business/category-file/{category_id}', 'Api\AssetController@BusinessFiles');

    //Notifications
    Route::post('save-other-rdv-notif', 'Api\AssetController@saveOtherRDVNotif');
    Route::get('get-notifications', 'Api\AssetController@getNotifications');
    Route::post('delete-notification', 'Api\AssetController@deleteNotification');
    Route::get('check-new-notifications', 'Api\AssetController@checkNewNotifications');
    Route::post('change-notification-flags', 'Api\AssetController@changeNotificationFlags');
    Route::get('get-notification-flags', 'Api\AssetController@getNotificationFlags');

    Route::get('getFlag', 'Api\UserController@getFlagTest');
    Route::get('corporate-page/{page}', 'Api\UserController@corporatePage');

    Route::get('get-internal-notifications', 'Api\UserController@getInternalNotifications');

    Route::get('save-client-id-cart', 'Api\SaleController@saveClientIdCart');
    Route::post('client/addLigneClient', 'Api\SaleController@addClientSaleLine');
    Route::post('client/getClientDetail', 'Api\SaleController@getSalesClientDetail');
    Route::post('client/saveShippingData', 'Api\SaleController@addClientShippingData');
    Route::post('client/addRemise', 'Api\SaleController@addClientRemise');
    Route::post('client/checkout', 'Api\SaleController@clientLinkCheckout');
});
