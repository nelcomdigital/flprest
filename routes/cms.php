<?php
/**
 * Created by PhpStorm.
 * User: Rabee
 * Date: 04/05/2019
 * Time: 12:55
 */


Route::prefix('cms')->group(function () {
    Route::get('login', 'Auth\LoginController@showCmsLoginForm')->name('cms.login');
    Route::post('login', 'Auth\LoginController@login')->name('cms.login');
    Route::get('logout', 'Auth\LoginController@logout')->name('cms.logout');


});

Route::prefix('cms')->middleware(['auth','roles'])->group(function () {
    Route::get('/', 'CMS\CmsController@home')->name('cms.home');
    Route::get('/ajax-select', 'CMS\CmsController@ajaxSelect')->name('cms.ajax-select');
    Route::get('/ajax-select-search/{filter}', 'CMS\CmsController@ajaxSelectSearch')->name('cms.ajax.select.search');
    Route::get('/select-by-ajax/{model}', 'CMS\CmsController@ajaxSelectByModel')->name('cms.select.ajax');
    Route::get('/select-published-by-ajax/{model}', 'CMS\CmsController@ajaxSelectPublishedByModel')->name('cms.select.published.ajax');
    Route::get('/albums-scan/{model?}/{slug?}', 'CMS\CmsController@scanAlbums')->name('cms.scan.albums');
    Route::post('/temp-upload', 'CMS\CmsController@uploadTempFile')->name('cms.upload.albums');
    Route::post('/temp-submit', 'CMS\CmsController@submitSaving')->name('cms.submit.albums');
    Route::post('/delete-upload', 'CMS\CmsController@removeAlbumImage')->name('cms.delete.albums');
    Route::get('/gallery-modal/photographers', 'CMS\CmsController@getPhotographers')->name('cms.photographers.albums');
    Route::post('/gallery-update', 'CMS\CmsController@updateAlbum')->name('cms.update.albums');
    Route::post('/gallery-image-update', 'CMS\CmsController@updateAlbumImage')->name('cms.update.albums.images');
    Route::post('/get-gallery-albums', 'CMS\CmsController@addGalleryAlbum')->name('cms.post.albums.images');
    Route::get('/get-gallery-albums', 'CMS\CmsController@getGalleryAlbumsFiles')->name('cms.get.albums.images');
    Route::get('/csv-uploader', 'CMS\CmsController@uploaderForm')->name('cms.uploader.form');
    Route::post('/csv-uploader', 'CMS\CmsController@uploader')->name('cms.uploader');

    Route::get('/general-configuration', 'CMS\CmsController@generalConfiguration')->name('cms.general.configuration');
    Route::put('/general-configuration', 'CMS\CmsController@generalConfigurationPost')->name('cms.general.configuration.post');
    Route::get('/general-configuration/delete/{id}/{type}', 'CMS\CmsController@deleteGeneralConfigImage')->name('cms.general.configuration.delete.image');



});
