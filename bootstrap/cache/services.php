<?php return array (
  'providers' => 
  array (
    0 => 'Illuminate\\Auth\\AuthServiceProvider',
    1 => 'Illuminate\\Broadcasting\\BroadcastServiceProvider',
    2 => 'Illuminate\\Bus\\BusServiceProvider',
    3 => 'Illuminate\\Cache\\CacheServiceProvider',
    4 => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    5 => 'Illuminate\\Cookie\\CookieServiceProvider',
    6 => 'Illuminate\\Database\\DatabaseServiceProvider',
    7 => 'Illuminate\\Encryption\\EncryptionServiceProvider',
    8 => 'Illuminate\\Filesystem\\FilesystemServiceProvider',
    9 => 'Illuminate\\Foundation\\Providers\\FoundationServiceProvider',
    10 => 'Illuminate\\Hashing\\HashServiceProvider',
    11 => 'Illuminate\\Mail\\MailServiceProvider',
    12 => 'Illuminate\\Notifications\\NotificationServiceProvider',
    13 => 'Illuminate\\Pagination\\PaginationServiceProvider',
    14 => 'Illuminate\\Pipeline\\PipelineServiceProvider',
    15 => 'Illuminate\\Queue\\QueueServiceProvider',
    16 => 'Illuminate\\Redis\\RedisServiceProvider',
    17 => 'Illuminate\\Auth\\Passwords\\PasswordResetServiceProvider',
    18 => 'Illuminate\\Session\\SessionServiceProvider',
    19 => 'Illuminate\\Translation\\TranslationServiceProvider',
    20 => 'Illuminate\\Validation\\ValidationServiceProvider',
    21 => 'Illuminate\\View\\ViewServiceProvider',
    22 => 'Artesaos\\SEOTools\\Providers\\SEOToolsServiceProvider',
    23 => 'Barryvdh\\Elfinder\\ElfinderServiceProvider',
    24 => 'BeyondCode\\DumpServer\\DumpServerServiceProvider',
    25 => 'LaravelFCM\\FCMServiceProvider',
    26 => 'Fideloper\\Proxy\\TrustedProxyServiceProvider',
    27 => 'Intervention\\Image\\ImageServiceProvider',
    28 => 'Jenssegers\\Agent\\AgentServiceProvider',
    29 => 'Jenssegers\\Mongodb\\MongodbServiceProvider',
    30 => 'Jenssegers\\Mongodb\\MongodbQueueServiceProvider',
    31 => 'Illuminate\\Notifications\\NexmoChannelServiceProvider',
    32 => 'Laravel\\Passport\\PassportServiceProvider',
    33 => 'Illuminate\\Notifications\\SlackChannelServiceProvider',
    34 => 'Laravel\\Socialite\\SocialiteServiceProvider',
    35 => 'Laravel\\Tinker\\TinkerServiceProvider',
    36 => 'Carbon\\Laravel\\ServiceProvider',
    37 => 'niklasravnsborg\\LaravelPdf\\PdfServiceProvider',
    38 => 'NunoMaduro\\Collision\\Adapters\\Laravel\\CollisionServiceProvider',
    39 => 'SimpleSoftwareIO\\QrCode\\QrCodeServiceProvider',
    40 => 'Spatie\\Analytics\\AnalyticsServiceProvider',
    41 => 'Spatie\\CookieConsent\\CookieConsentServiceProvider',
    42 => 'Stevebauman\\Location\\LocationServiceProvider',
    43 => 'Yajra\\DataTables\\DataTablesServiceProvider',
    44 => 'App\\Providers\\AppServiceProvider',
    45 => 'App\\Providers\\AuthServiceProvider',
    46 => 'App\\Providers\\EventServiceProvider',
    47 => 'App\\Providers\\RouteServiceProvider',
    48 => 'Jenssegers\\Mongodb\\MongodbServiceProvider',
    49 => 'Barryvdh\\Elfinder\\ElfinderServiceProvider',
    50 => 'Stevebauman\\Location\\LocationServiceProvider',
    51 => 'Laravel\\Passport\\PassportServiceProvider',
    52 => 'Laravel\\Socialite\\SocialiteServiceProvider',
    53 => 'Jenssegers\\Mongodb\\MongodbQueueServiceProvider',
    54 => 'Jenssegers\\Mongodb\\Auth\\PasswordResetServiceProvider',
    55 => 'Yajra\\DataTables\\DataTablesServiceProvider',
    56 => 'App\\Providers\\GlobalTemplateServiceProvider',
    57 => 'App\\Providers\\FrontendGlobalTemplateServiceProvider',
    58 => 'Artesaos\\SEOTools\\Providers\\SEOToolsServiceProvider',
    59 => 'Spatie\\Analytics\\AnalyticsServiceProvider',
    60 => 'App\\Providers\\Modules\\SliderServiceProvider',
    61 => 'App\\Providers\\Modules\\OrderServiceProvider',
    62 => 'App\\Providers\\Modules\\ProductServiceProvider',
    63 => 'App\\Providers\\Modules\\ProductCategoryServiceProvider',
    64 => 'App\\Providers\\Modules\\ProducerServiceProvider',
    65 => 'App\\Providers\\Modules\\RoleServiceProvider',
    66 => 'App\\Providers\\Modules\\UserServiceProvider',
    67 => 'App\\Providers\\Modules\\CurrencyServiceProvider',
    68 => 'App\\Providers\\Modules\\StatusServiceProvider',
    69 => 'App\\Providers\\Modules\\TranslationServiceProvider',
    70 => 'App\\Providers\\Modules\\PageServiceProvider',
    71 => 'App\\Providers\\Modules\\LocaleServiceProvider',
    72 => 'App\\Providers\\Modules\\CityServiceProvider',
    73 => 'App\\Providers\\Modules\\CountryServiceProvider',
    74 => 'App\\Providers\\Modules\\ContactServiceProvider',
    75 => 'App\\Providers\\Modules\\GroupCountryServiceProvider',
    76 => 'App\\Providers\\Modules\\RegionServiceProvider',
    77 => 'App\\Providers\\Modules\\DepartmentServiceProvider',
    78 => 'App\\Providers\\Modules\\MainMenuServiceProvider',
    79 => 'App\\Providers\\Modules\\ReminderServiceProvider',
    80 => 'App\\Providers\\Modules\\GroupServiceProvider',
    81 => 'App\\Providers\\Modules\\ReminderTypeServiceProvider',
    82 => 'App\\Providers\\Modules\\UserObjectServiceProvider',
    83 => 'App\\Providers\\Modules\\CategoryServiceProvider',
    84 => 'App\\Providers\\Modules\\VideoServiceProvider',
    85 => 'App\\Providers\\Modules\\FavoriteServiceProvider',
    86 => 'App\\Providers\\Modules\\DeliveryModeServiceProvider',
    87 => 'App\\Providers\\Modules\\BusinessCategoryServiceProvider',
    88 => 'App\\Providers\\Modules\\BusinessFileServiceProvider',
    89 => 'App\\Providers\\Modules\\NotificationRoleServiceProvider',
    90 => 'App\\Providers\\Modules\\AnnouncementServiceProvider',
  ),
  'eager' => 
  array (
    0 => 'Illuminate\\Auth\\AuthServiceProvider',
    1 => 'Illuminate\\Cookie\\CookieServiceProvider',
    2 => 'Illuminate\\Database\\DatabaseServiceProvider',
    3 => 'Illuminate\\Encryption\\EncryptionServiceProvider',
    4 => 'Illuminate\\Filesystem\\FilesystemServiceProvider',
    5 => 'Illuminate\\Foundation\\Providers\\FoundationServiceProvider',
    6 => 'Illuminate\\Notifications\\NotificationServiceProvider',
    7 => 'Illuminate\\Pagination\\PaginationServiceProvider',
    8 => 'Illuminate\\Session\\SessionServiceProvider',
    9 => 'Illuminate\\View\\ViewServiceProvider',
    10 => 'Barryvdh\\Elfinder\\ElfinderServiceProvider',
    11 => 'BeyondCode\\DumpServer\\DumpServerServiceProvider',
    12 => 'Fideloper\\Proxy\\TrustedProxyServiceProvider',
    13 => 'Intervention\\Image\\ImageServiceProvider',
    14 => 'Jenssegers\\Mongodb\\MongodbServiceProvider',
    15 => 'Illuminate\\Notifications\\NexmoChannelServiceProvider',
    16 => 'Laravel\\Passport\\PassportServiceProvider',
    17 => 'Illuminate\\Notifications\\SlackChannelServiceProvider',
    18 => 'Carbon\\Laravel\\ServiceProvider',
    19 => 'niklasravnsborg\\LaravelPdf\\PdfServiceProvider',
    20 => 'Spatie\\Analytics\\AnalyticsServiceProvider',
    21 => 'Spatie\\CookieConsent\\CookieConsentServiceProvider',
    22 => 'Stevebauman\\Location\\LocationServiceProvider',
    23 => 'Yajra\\DataTables\\DataTablesServiceProvider',
    24 => 'App\\Providers\\AppServiceProvider',
    25 => 'App\\Providers\\AuthServiceProvider',
    26 => 'App\\Providers\\EventServiceProvider',
    27 => 'App\\Providers\\RouteServiceProvider',
    28 => 'Jenssegers\\Mongodb\\MongodbServiceProvider',
    29 => 'Barryvdh\\Elfinder\\ElfinderServiceProvider',
    30 => 'Stevebauman\\Location\\LocationServiceProvider',
    31 => 'Laravel\\Passport\\PassportServiceProvider',
    32 => 'Yajra\\DataTables\\DataTablesServiceProvider',
    33 => 'App\\Providers\\GlobalTemplateServiceProvider',
    34 => 'App\\Providers\\FrontendGlobalTemplateServiceProvider',
    35 => 'Spatie\\Analytics\\AnalyticsServiceProvider',
    36 => 'App\\Providers\\Modules\\SliderServiceProvider',
    37 => 'App\\Providers\\Modules\\OrderServiceProvider',
    38 => 'App\\Providers\\Modules\\ProductServiceProvider',
    39 => 'App\\Providers\\Modules\\ProductCategoryServiceProvider',
    40 => 'App\\Providers\\Modules\\ProducerServiceProvider',
    41 => 'App\\Providers\\Modules\\RoleServiceProvider',
    42 => 'App\\Providers\\Modules\\UserServiceProvider',
    43 => 'App\\Providers\\Modules\\CurrencyServiceProvider',
    44 => 'App\\Providers\\Modules\\StatusServiceProvider',
    45 => 'App\\Providers\\Modules\\TranslationServiceProvider',
    46 => 'App\\Providers\\Modules\\PageServiceProvider',
    47 => 'App\\Providers\\Modules\\LocaleServiceProvider',
    48 => 'App\\Providers\\Modules\\CityServiceProvider',
    49 => 'App\\Providers\\Modules\\CountryServiceProvider',
    50 => 'App\\Providers\\Modules\\ContactServiceProvider',
    51 => 'App\\Providers\\Modules\\GroupCountryServiceProvider',
    52 => 'App\\Providers\\Modules\\RegionServiceProvider',
    53 => 'App\\Providers\\Modules\\DepartmentServiceProvider',
    54 => 'App\\Providers\\Modules\\MainMenuServiceProvider',
    55 => 'App\\Providers\\Modules\\ReminderServiceProvider',
    56 => 'App\\Providers\\Modules\\GroupServiceProvider',
    57 => 'App\\Providers\\Modules\\ReminderTypeServiceProvider',
    58 => 'App\\Providers\\Modules\\UserObjectServiceProvider',
    59 => 'App\\Providers\\Modules\\CategoryServiceProvider',
    60 => 'App\\Providers\\Modules\\VideoServiceProvider',
    61 => 'App\\Providers\\Modules\\FavoriteServiceProvider',
    62 => 'App\\Providers\\Modules\\DeliveryModeServiceProvider',
    63 => 'App\\Providers\\Modules\\BusinessCategoryServiceProvider',
    64 => 'App\\Providers\\Modules\\BusinessFileServiceProvider',
    65 => 'App\\Providers\\Modules\\NotificationRoleServiceProvider',
    66 => 'App\\Providers\\Modules\\AnnouncementServiceProvider',
  ),
  'deferred' => 
  array (
    'Illuminate\\Broadcasting\\BroadcastManager' => 'Illuminate\\Broadcasting\\BroadcastServiceProvider',
    'Illuminate\\Contracts\\Broadcasting\\Factory' => 'Illuminate\\Broadcasting\\BroadcastServiceProvider',
    'Illuminate\\Contracts\\Broadcasting\\Broadcaster' => 'Illuminate\\Broadcasting\\BroadcastServiceProvider',
    'Illuminate\\Bus\\Dispatcher' => 'Illuminate\\Bus\\BusServiceProvider',
    'Illuminate\\Contracts\\Bus\\Dispatcher' => 'Illuminate\\Bus\\BusServiceProvider',
    'Illuminate\\Contracts\\Bus\\QueueingDispatcher' => 'Illuminate\\Bus\\BusServiceProvider',
    'cache' => 'Illuminate\\Cache\\CacheServiceProvider',
    'cache.store' => 'Illuminate\\Cache\\CacheServiceProvider',
    'memcached.connector' => 'Illuminate\\Cache\\CacheServiceProvider',
    'command.cache.clear' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.cache.forget' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.clear-compiled' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.auth.resets.clear' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.config.cache' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.config.clear' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.down' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.environment' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.key.generate' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.migrate' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.migrate.fresh' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.migrate.install' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.migrate.refresh' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.migrate.reset' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.migrate.rollback' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.migrate.status' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.optimize' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.optimize.clear' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.package.discover' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.preset' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.queue.failed' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.queue.flush' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.queue.forget' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.queue.listen' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.queue.restart' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.queue.retry' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.queue.work' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.route.cache' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.route.clear' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.route.list' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.seed' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'Illuminate\\Console\\Scheduling\\ScheduleFinishCommand' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'Illuminate\\Console\\Scheduling\\ScheduleRunCommand' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.storage.link' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.up' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.view.cache' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.view.clear' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.app.name' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.auth.make' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.cache.table' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.channel.make' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.console.make' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.controller.make' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.event.generate' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.event.make' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.exception.make' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.factory.make' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.job.make' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.listener.make' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.mail.make' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.middleware.make' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.migrate.make' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.model.make' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.notification.make' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.notification.table' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.observer.make' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.policy.make' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.provider.make' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.queue.failed-table' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.queue.table' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.request.make' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.resource.make' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.rule.make' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.seeder.make' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.session.table' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.serve' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.test.make' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.vendor.publish' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'migrator' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'migration.repository' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'migration.creator' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'composer' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'hash' => 'Illuminate\\Hashing\\HashServiceProvider',
    'hash.driver' => 'Illuminate\\Hashing\\HashServiceProvider',
    'mailer' => 'Illuminate\\Mail\\MailServiceProvider',
    'swift.mailer' => 'Illuminate\\Mail\\MailServiceProvider',
    'swift.transport' => 'Illuminate\\Mail\\MailServiceProvider',
    'Illuminate\\Mail\\Markdown' => 'Illuminate\\Mail\\MailServiceProvider',
    'Illuminate\\Contracts\\Pipeline\\Hub' => 'Illuminate\\Pipeline\\PipelineServiceProvider',
    'queue' => 'Jenssegers\\Mongodb\\MongodbQueueServiceProvider',
    'queue.worker' => 'Jenssegers\\Mongodb\\MongodbQueueServiceProvider',
    'queue.listener' => 'Jenssegers\\Mongodb\\MongodbQueueServiceProvider',
    'queue.failer' => 'Jenssegers\\Mongodb\\MongodbQueueServiceProvider',
    'queue.connection' => 'Jenssegers\\Mongodb\\MongodbQueueServiceProvider',
    'redis' => 'Illuminate\\Redis\\RedisServiceProvider',
    'redis.connection' => 'Illuminate\\Redis\\RedisServiceProvider',
    'auth.password' => 'Jenssegers\\Mongodb\\Auth\\PasswordResetServiceProvider',
    'auth.password.broker' => 'Jenssegers\\Mongodb\\Auth\\PasswordResetServiceProvider',
    'translator' => 'Illuminate\\Translation\\TranslationServiceProvider',
    'translation.loader' => 'Illuminate\\Translation\\TranslationServiceProvider',
    'validator' => 'Illuminate\\Validation\\ValidationServiceProvider',
    'validation.presence' => 'Illuminate\\Validation\\ValidationServiceProvider',
    'Artesaos\\SEOTools\\Contracts\\SEOTools' => 'Artesaos\\SEOTools\\Providers\\SEOToolsServiceProvider',
    'Artesaos\\SEOTools\\Contracts\\MetaTags' => 'Artesaos\\SEOTools\\Providers\\SEOToolsServiceProvider',
    'Artesaos\\SEOTools\\Contracts\\TwitterCards' => 'Artesaos\\SEOTools\\Providers\\SEOToolsServiceProvider',
    'Artesaos\\SEOTools\\Contracts\\OpenGraph' => 'Artesaos\\SEOTools\\Providers\\SEOToolsServiceProvider',
    'seotools' => 'Artesaos\\SEOTools\\Providers\\SEOToolsServiceProvider',
    'seotools.metatags' => 'Artesaos\\SEOTools\\Providers\\SEOToolsServiceProvider',
    'seotools.opengraph' => 'Artesaos\\SEOTools\\Providers\\SEOToolsServiceProvider',
    'seotools.twitter' => 'Artesaos\\SEOTools\\Providers\\SEOToolsServiceProvider',
    'fcm.client' => 'LaravelFCM\\FCMServiceProvider',
    'fcm.group' => 'LaravelFCM\\FCMServiceProvider',
    'fcm.sender' => 'LaravelFCM\\FCMServiceProvider',
    'agent' => 'Jenssegers\\Agent\\AgentServiceProvider',
    'Jenssegers\\Agent\\Agent' => 'Jenssegers\\Agent\\AgentServiceProvider',
    'Laravel\\Socialite\\Contracts\\Factory' => 'Laravel\\Socialite\\SocialiteServiceProvider',
    'command.tinker' => 'Laravel\\Tinker\\TinkerServiceProvider',
    'NunoMaduro\\Collision\\Contracts\\Provider' => 'NunoMaduro\\Collision\\Adapters\\Laravel\\CollisionServiceProvider',
    'qrcode' => 'SimpleSoftwareIO\\QrCode\\QrCodeServiceProvider',
  ),
  'when' => 
  array (
    'Illuminate\\Broadcasting\\BroadcastServiceProvider' => 
    array (
    ),
    'Illuminate\\Bus\\BusServiceProvider' => 
    array (
    ),
    'Illuminate\\Cache\\CacheServiceProvider' => 
    array (
    ),
    'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider' => 
    array (
    ),
    'Illuminate\\Hashing\\HashServiceProvider' => 
    array (
    ),
    'Illuminate\\Mail\\MailServiceProvider' => 
    array (
    ),
    'Illuminate\\Pipeline\\PipelineServiceProvider' => 
    array (
    ),
    'Illuminate\\Queue\\QueueServiceProvider' => 
    array (
    ),
    'Illuminate\\Redis\\RedisServiceProvider' => 
    array (
    ),
    'Illuminate\\Auth\\Passwords\\PasswordResetServiceProvider' => 
    array (
    ),
    'Illuminate\\Translation\\TranslationServiceProvider' => 
    array (
    ),
    'Illuminate\\Validation\\ValidationServiceProvider' => 
    array (
    ),
    'Artesaos\\SEOTools\\Providers\\SEOToolsServiceProvider' => 
    array (
    ),
    'LaravelFCM\\FCMServiceProvider' => 
    array (
    ),
    'Jenssegers\\Agent\\AgentServiceProvider' => 
    array (
    ),
    'Jenssegers\\Mongodb\\MongodbQueueServiceProvider' => 
    array (
    ),
    'Laravel\\Socialite\\SocialiteServiceProvider' => 
    array (
    ),
    'Laravel\\Tinker\\TinkerServiceProvider' => 
    array (
    ),
    'NunoMaduro\\Collision\\Adapters\\Laravel\\CollisionServiceProvider' => 
    array (
    ),
    'SimpleSoftwareIO\\QrCode\\QrCodeServiceProvider' => 
    array (
    ),
    'Jenssegers\\Mongodb\\Auth\\PasswordResetServiceProvider' => 
    array (
    ),
  ),
);