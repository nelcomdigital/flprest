var number_uploading_images = [];
var number_uploading_files = 0;
function createAlbum(id, model, slug) {

    let albums = $("#" + id + "_albums");
    var rand = Math.random().toString(36).substr(2, 9);

    let rightDiv = createRightDiv(id, model, slug, rand);
    let thumb_container = '<div class="col-md-8" id="' + id + '_albums_thumb"></div>';
    let div = '<div class="col-md-2 "><label class="file add-new-album">\n' +
        '                    <img src="/public/images/add_picture.svg" style="max-width: unset;margin-top: unset"/> \n' +
        '                    <input type="file" class="gallery-input" onchange="loadImages(\'' + rand + '_albums_input\', \'' + rand + '_album_name\',\'' + model + '\',\'' + slug + '\')"  id="' + rand + '_albums_input" multiple="multiple" >\n' +
        '                </label></div>';

    albums.html('');
    albums.html(rightDiv);
    albums.append(thumb_container);
    let tc = $('#' + id + '_albums_thumb');
    tc.html(div);
    init_custom_select_one(rand + '_album_cities');
}

function updateAlbum(id, model, slug, rand) {
    $('#album-update-progress').show();
    let formData = new FormData();
    formData.append('new_album', $('#' + rand + '_album_name').val());
    formData.append('old_album', $('#' + rand + '_album_name_hidden').val());
    formData.append('description', $('#' + rand + '_album_description').val());
    formData.append('cities', $('#' + rand + '_album_cities').val());
    formData.append('slug', slug);
    formData.append('model', model);
    formData.append('_token', $('meta[name="_token_images"]').attr('content'));
    let url = window.location.protocol+'//' + window.location.hostname +'/gallery-update';
    $.ajax({
        url: url,
        type: 'POST',
        data: formData,
        contentType: false,
        cache: false,
        processData: false,
        success: function (data) {
            console.log(data);
            scan_gallery_album(id, model, slug);

        },
        error: function (e) {

        }
    });
}

function updateImage(id, model, slug, rand, short_name) {
    let formData = new FormData();
    formData.append('caption', $('#' + rand + '_image_caption').val());
    // formData.append('custom', $('#' + rand + '_album_photographers_custom').val());
    formData.append('photographers', $('#' + rand + '_album_photographers').val());
    formData.append('short_name', short_name);
    formData.append('slug', slug);
    formData.append('model', model);
    formData.append('_token', $('meta[name="_token_images"]').attr('content'));
    let url = window.location.protocol+'//' + window.location.hostname +'/gallery-image-update';
    $.ajax({
        url: url,
        type: 'POST',
        data: formData,
        contentType: false,
        cache: false,
        processData: false,
        success: function (data) {
            console.log(data);
            closeImage(rand);

        },
        error: function (e) {

        }
    });
}

function createRightDiv(id, model, slug, rand, album = null, readonly = false) {

    let options = '';
    var album_temp_id = rand;

    if (album == null) {
        album = {'name': slug + '_' + album_temp_id, 'description': ''};
    } else {
        album = JSON.parse(replacy(album));
        try {
            for (pg of album.cities) {
                options += '<option value="' + pg._id + '" selected>' + pg.name + '</option>\n';
            }
        } catch (e) {

        }
        if (album.description == undefined) {
            album.description = '';
        }
    }

    let div =  '<div class="col-md-4 album-section" > ' +
        '<div class="control-label col-sm-12 col-xs-12 album-back" onclick="backAlbum(\'' + id + '\',\'' + model + '\',\'' + slug + '\')"><img src="public/images/album_back.svg" style="width: 30px;cursor: pointer"/></div>' +
        '<div class="control-label col-sm-12 col-xs-12" style="margin-bottom: 10px;">' +
        '  <label class="control-label col-sm-12 col-xs-12 album-label" for="album_name">Album Name</label>\n' +
        '    <div class="col-md-12 col-sm-12 col-xs-12">\n' +
        '        <input type="text" id="' + rand + '_album_name" class="form-control album-input" value="' + album.name + '" >\n' +
        '        <input type="hidden" id="' + rand + '_album_name_hidden" class="album-input" value="' + album.name + '" readonly>\n' +
        '    </div>\n' +
        '</div>\n'+
        '<div class="control-label col-sm-12 col-xs-12" style="margin-bottom: 10px;">' +
        '  <label class="control-label col-sm-12 col-xs-12 album-label" for="album_name">Album Description</label>\n' +
        '    <div class="col-md-12 col-sm-12 col-xs-12">\n' +
        '        <textarea type="text" id="' + rand + '_album_description" class="form-control album-input">' + album.description + '</textarea>\n' +
        '    </div>' +
        '</div>\n<br/>' ;
    if ( slug == 'gallery-album')
    div+=
        '                   <div class="control-label col-sm-12 col-xs-12" style="margin-bottom: 10px;">' +
        '                       <label class="modal-label col-sm-12 col-md-12 col-xl-12 album-label" for="">Cities</label>\n' +
        '                       <div class="col-md-12 col-sm-12">\n' +
        '                       <select type="text" class="select2 select2-ajax form-control" data-route="http://cms.nelcomlab.club/select-by-ajax/City" ' +
        '                           data-name="name" name="' + rand + '_album_cities" id="' + rand + '_album_cities"  multiple>\n' + options +
        '                       </select>\n' +
        '                   </div>\n<br/></div>' ;

        div+=
            '<div class="control-label col-sm-12 col-xs-12" style="margin-bottom: 10px;">' +
        '    <div class="col-md-12 col-sm-12 col-xs-12">\n' +
        '        <div class="btn btn-success btn-save" onclick="updateAlbum(\'' + id + '\',\'' + model + '\',\'' + slug + '\',\'' + rand + '\')">Update</div>' +
            '<br/>\n '+
            '<div class="lds-ring" style="display: none" id="album-update-progress"><div></div><div></div><div></div><div></div></div>\n' +
        '    </div>\n' +
        '    </div>\n' +
        '</div>';

        return  div;



}

function closeImage(rand) {
    $('#' + rand + '_image_modal').modal('hide');
}

function openImage(id, model, slug, rand, image) {

    let albums = $("#" + id + "_albums");
    dat = JSON.parse(replacy(image));
    if (dat.caption == undefined)
        dat.caption = '';
    if (dat.custom == undefined)
        dat.custom = '';

    let options = '';
    try {
        for (pg in dat.photographers) {
            options += '<option value="' + dat.photographers[pg]._id + '" selected>' + dat.photographers[pg].name + '</option>\n';
        }
    } catch (e) {

    }


    if ($('#' + rand + '_image_modal').length != 0) {
        $('#' + rand + '_image_modal').remove();
    }
        var div = '<div class="modal fade" id="' + rand + '_image_modal" style="width: 60%;margin: 20px auto" tabindex="-1" role="dialog" aria-labelledby="imageDisplay" aria-hidden="true">\n' +
            '    <div class="modal-content">\n' +
            '        <div class="container">\n' +
            '          <div class="modal-header">\n' +
            '            <button class="close" style="color:#ff9100; font-size:30px" type="button" onclick="closeImage(\'' + rand + '\')"><span aria-hidden="true">&times;</span></button>\n' +
            '            <h3 class="modal-title" id="myModalLabel"></h3>\n' +
            '          </div>\n' +
            '              <div class="modal-body" >' +
            '                   <div class="item form-group" style="text-align: left; margin-left: 13px">' +
            '                       <a href="' + dat.url + '" target="_blank"> <img src="' + dat.url + '" style="width: 50%;border:2px solid #ff9100; border-radius: 4px" ></a>' +
            '                   </div>' +
            '                   <div class="item form-group" >\n' +
            '                       <label class="modal-label col-sm-12 col-md-12 col-xl-12" for="">Caption</label>\n' +
            '                       <div class="modal-label col-md-12 col-sm-12 col-xs-12">\n' +
            '                           <textarea id="' + rand + '_image_caption"  class="form-control col-md-7 col-xs-12"  >' + dat.caption + '</textarea>\n' +
            '                       </div>\n' +
            '                   </div>' +
            '                   <div class="item form-group">' +
            '                       <label class="modal-label col-sm-12 col-md-12 col-xl-12" for="">Photographer</label>\n' +
            '                       <div class="col-md-12-sm-12" style="margin: 0 10px;">\n' +
            '                       <select type="text" class="select2 select2-ajax form-control" data-route="http://cms.nelcomlab.club/gallery-modal/photographers" ' +
            '                           data-name="name" name="' + rand + '_album_photographers" id="' + rand + '_album_photographers"  multiple>\n' + options +
            '                       </select>\n' +
            '                   </div>\n' +
            '          <div class="modal-footer">\n' +
            '        <div class="btn btn-success btn-save" onclick="updateImage(\'' + id + '\',\'' + model + '\',\'' + slug + '\',\'' + rand + '\',\'' + dat.short_name + '\')">Update</div>' +
            '          </div>\n' +
            '        </div>\n' +
            '    </div>\n' +
            '</div>';
        albums.append(div);

        init_custom_select_one(rand + '_album_photographers');



    $('#' + rand + '_image_modal').modal('show');

}

function openAlbum(id, data, album, model, slug) {


    let albums = $("#" + id + "_albums");
    var rand = Math.random().toString(36).substr(2, 9);

    let rightDiv = createRightDiv(id, model, slug, rand, album);
    let thumb_container = '<div class="col-md-8" id="' + id + '_albums_thumb"></div>';

    var div = '<div class="col-md-2 "><label class="file add-new-album">\n' +
        '                    <img src="/public/images/add_picture.svg" style="max-width: unset;margin-top: unset" />\n' +
        '                    <input type="file" class="gallery-input" onchange="loadImages(\'' + rand + '_albums_input\', \'' + rand + '_album_name\',\'' + model + '\',\'' + slug + '\')"  id="' + rand + '_albums_input" multiple="multiple" >\n' +
        '                </label></div>';

    albums.html('');
    albums.html(rightDiv);
    albums.append(thumb_container);
    let tc = $('#' + id + '_albums_thumb');
    dat = JSON.parse(replacy(album));
    for (let img of dat.images)
        createImage(id, model, slug, rand, tc, img);

    tc.append(div);
    init_custom_select_one(rand + '_album_cities');

}

function backAlbum(id, model, slug) {
    scan_gallery_album(id, model, slug);
}

function loadImages(id, album_id, model, slug) {

    if ($('#' + album_id).val() == '')
        return;
    number_uploading_files = $("#" + id).prop('files').length;
    for (let file in $("#" + id).prop('files')) {
        if (file == 'length' || file == 'item')
            continue;
        let rand = Math.random().toString(36).substr(2, 9);
        var reader = new FileReader();

        reader.onload = function (e) {
            createImageTemp($("#" + id).parent().parent().parent(), e.target.result, rand);
        }
        reader.readAsDataURL($("#" + id).prop('files')[file]);
        let url = window.location.protocol+'//' + window.location.hostname +'/temp-upload';
        let formData = new FormData();
        formData.append('image', $("#" + id).prop('files')[file]);
        formData.append('name', $('#' + album_id).val());
        formData.append('model', model);
        formData.append('slug', slug);
        formData.append('temp_id', rand);
        formData.append('_token', $('meta[name="_token_images"]').attr('content'));
        $.ajax({
            url: url,
            type: 'POST',
            data: formData,
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                console.log(data);
                submitSaving(id , model, slug, rand,album_id , data);

                let albums = $("#" + id).parent().parent().parent();
                $('#' + data.temp_id + '_tempImage').remove();
                createImage(id, model, slug, rand, albums, data);


            },
            error: function (e) {

            }
        });
    }

}
function submitSaving(id , model, slug, rand ,album_id, data){
    number_uploading_images.push(data);
    if ( number_uploading_images.length === number_uploading_files){
        let url = window.location.protocol+'//' + window.location.hostname +'/temp-submit';
        let formData = new FormData();
        for (let index in number_uploading_images){
            if ( number_uploading_images[index].url != null ){
                formData.append('images['+index+'][url]', number_uploading_images[index].path);
                formData.append('images['+index+'][short_name]', number_uploading_images[index].short_name);
            }
        }
        number_uploading_images = [];
        number_uploading_files = 0 ;
        formData.append('name', $('#' + album_id).val());
        formData.append('model', model);
        formData.append('slug', slug);
        formData.append('_token', $('meta[name="_token_images"]').attr('content'));
        $.ajax({
            url: url,
            type: 'POST',
            data: formData,
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                console.log(data);

            },
            error: function (e) {

            }
        });
    }
}

function createImage(id, model, slug, rand, albums, ob) {
    let name = ob.short_name;
    var tt = replacy(JSON.stringify(ob), false);
    name = name.replace('.', '_');
    let div = '<div class="col-md-2 open-image-album"><div  onclick="openImage(\'' + id + '\',\'' + model + '\',\'' + slug + '\',\'' + rand + '\',\'' + tt + '\')" style="background-image:url('+'\'' +  handleQuote(ob.url)  + '\' '+');' +
        'background-position: center; background-repeat: no-repeat;background-size: cover;width: 100%;height: 100%" id="' + slug + '_' + model + '_' + name + '"></div>' +
        '<div class="remove-image-album" onclick="removeImage(\'' + ob.short_name + '\',\'' + model + '\',\'' + slug + '\')"><i class="fa fa-remove"></i></div>' +
        '</div>';
    albums.prepend(div);


}

function createImageTemp(albums, result, id) {
    let div = '<div class="col-md-2 open-image-album temp" style="background-image:url(' + handleQuote(result)  +
        ');background-position: center; background-repeat: no-repeat;background-size: cover" id="' + id + '_tempImage">' +
        '<div class="lds-ring"><div></div><div></div><div></div><div></div></div>' +
        '</div>';
    albums.prepend(div);

}

function fillData(id, data, model, slug) {
    let albums = $("#" + id + "_albums");
    if (data === "qq-0no_dataqq-0") {
        let div = '<div class="col-md-2 add-new-album" onclick="createAlbum(\'' + id + '\',\'' + model + '\',\'' + slug + '\')" ><img src="/public/images/add_album.svg"/></div>';
        albums.html(div);
        if ( model != 'GalleryAlbum') {
            let divSelect = '<div class="col-md-2 add-new-album"> <div style="color:black;font-size:14px;position: absolute;top:20%;left: 9px;width: 90%;"><select class="select2" id="' + id + '-select-albums" ></select> </div>' +
                '<div class="btn btn-success btn-save" style="position: absolute;bottom:0;left: 9px;" onclick="addGalleryAlbum(\'' + id + '\',\'' + model + '\',\'' + slug + '\')">Add Gallery Album</div>' +
                '</div>';
            albums.append(divSelect);
        }
        init_custom_select_album(id+'-select-albums');

    } else {
        let div = '<div class="col-md-2 add-new-album" onclick="createAlbum(\'' + id + '\',\'' + model + '\',\'' + slug + '\')" > <img src="/public/images/add_album.svg"/> </div>';

        albums.html(div);
        if ( model != 'GalleryAlbum') {
            let divSelect = '<div class="col-md-2 add-new-album"> <div style="color:black;font-size:14px;position: absolute;top:20%;left: 9px;width: 90%;"> <select class="select2" id="' + id + '-select-albums" ></select> </div>' +
                '<div class="btn btn-success btn-save" style="position: absolute;bottom:0;left: 9px;" onclick="addGalleryAlbum(\'' + id + '\',\'' + model + '\',\'' + slug + '\')">Add Gallery Album</div>' +
                '</div>';
            albums.append(divSelect);
        }
        init_custom_select_album(id+'-select-albums');

        dat = JSON.parse(replacy(data));
        for (let album in dat) {
            // try{
            var tt = replacy(JSON.stringify(dat[album]), false);
            let img_url = '';
            try {
                img_url = dat[album].images[0].url;
            } catch (e) {

            }
            let div = '<div class="col-md-2 open-album" style="background-image:url(\'' +  handleQuote(img_url) + '\');background-position: center; background-repeat: no-repeat;background-size: cover" onclick="openAlbum(\'' + id + '\',\'' + data + '\',\'' + tt + '\',\'' + model + '\',\'' + slug + '\')" >\n' +
                '<p>' + dat[album].name + '</p>\n' +
                '</div>';
            albums.append(div);

        }
    }

}

function removeImage(image, model = 'undefined', slug = 'undefined') {
    let formData = new FormData();
    formData.append('image', image);
    formData.append('slug', slug);
    formData.append('model', model);
    formData.append('_token', $('meta[name="_token_images"]').attr('content'));
    let url = window.location.protocol+'//' + window.location.hostname +'/delete-upload';
    $.ajax({
        url: url,
        type: 'POST',
        data: formData,
        contentType: false,
        cache: false,
        processData: false,
        success: function (data) {
            console.log(data);
            let img = image.replace('.', '_');
            $('#' + slug + '_' + model + '_' + img).parent().remove();

        },
        error: function (e) {

        }
    });
}

function addGalleryAlbum( id, model = 'undefined', slug = 'undefined') {
    let formData = new FormData();
    formData.append('album_name', $('#' + id + '-select-albums').val());
    formData.append('slug', slug);
    formData.append('model', model);
    formData.append('_token', $('meta[name="_token_images"]').attr('content'));
    let url = window.location.protocol+'//' + window.location.hostname +'/get-gallery-albums';
    $.ajax({
        url: url,
        type: 'POST',
        data: formData,
        contentType: false,
        cache: false,
        processData: false,
        success: function (data) {
            console.log(data);
            scan_gallery_album(id, model, slug);
        },
        error: function (e) {

        }
    });
}

function scan_gallery_album(id, model, slug) {
    jQ(function ($) {
        let url = window.location.protocol+'//' + window.location.hostname +'/albums-scan/'+ model + "/" + slug;
        $.get(url, function (data) {

            fillData(id, replacy(JSON.stringify(data), false), model, slug);


        });
    });
}

function replacy(data, flag = true) {

    if (flag) {
        while (data.includes('qq-0'))
            data = data.replace('qq-0', '"');
        while (data.includes('qq-1'))
            data = data.replace('qq-1', '{');
        while (data.includes('qq-2'))
            data = data.replace('qq-2', '[');
        while (data.includes('qq-3'))
            data = data.replace('qq-3', ':');
        while (data.includes('qq-4'))
            data = data.replace('qq-4', "'");
    } else {
        while (data.includes('"'))
            data = data.replace('"', 'qq-0');
        while (data.includes('{'))
            data = data.replace('{', 'qq-1');
        while (data.includes('['))
            data = data.replace('[', 'qq-2');
        while (data.includes(':'))
            data = data.replace(':', 'qq-3');
        while (data.includes("'"))
            data = data.replace("'", 'qq-4');
    }
    return data;
}

function handleQuote(string){
    while (string.includes("'"))
        string = string.replace("'", "%27");
    return string;
}

function init_custom_select_album(id) {
    let url = window.location.protocol+'//' + window.location.hostname +'/get-gallery-albums';
    $('#'+id).select2({
        width: '100%',
        placeholder:'Select Album',
        ajax: {
            url: url,
            dataType: 'json',
            processResults: function (data) {
                results = [];
                results.push({id: "", text: 'Select Album'})

                data = $.map(data.albums, function(obj) {
                    return { id: obj['name'], text: obj['name'] };
                })
                $.each(data, function(key, value){
                    results.push(value)
                })
                return {
                    results: results
                };
            },
            cache: true
        }
    });

}

