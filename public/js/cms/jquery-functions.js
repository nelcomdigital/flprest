(function ($) {

    $.fn.collapse_link =  function() {
        $('.x_panel').each(function(key, value){
            var $BOX_PANEL1 = $(value),
                $ICON = $(value).find('.collapse-link').find('i'),
                $BOX_CONTENT = $BOX_PANEL1.find('.x_content');

            if (!$BOX_PANEL1.hasClass('closed')) {
                $BOX_CONTENT.slideToggle(200, function(){
                    $BOX_PANEL1.addClass('closed');
                });
                if($ICON.hasClass('fa-chevron-up')){
                    $ICON.toggleClass('fa-chevron-up fa-chevron-down');
                }
            }
        })

        var $BOX_PANEL = $(this).closest('.x_panel'),
            $ICON = $(this).find('i'),
            $BOX_CONTENT = $BOX_PANEL.find('.x_content');

        // fix for some div with hardcoded fix class
        if ($BOX_PANEL.hasClass('closed')) {
            $BOX_CONTENT.slideToggle(200, function(){
                $BOX_PANEL.toggleClass('closed');
            });
            $ICON.toggleClass('fa-chevron-up fa-chevron-down');
        }
    };

    function input_error(value, error = null) {
        if($(value).closest('.form-group').find('.help-block').length == 0){
            if(error != null)
                $(value).closest('.form-group').append('<span class="help-block">'+error+'</span>');
            else if ( $(value).attr('data-error-message') === undefined )
                $(value).closest('.form-group').append('<span class="help-block">This field is required.</span>');
            else
                $(value).closest('.form-group').append('<span class="help-block">'+ $(value).attr("data-error-message")+'</span>');
        }else{
            if(error != null)
                $(value).closest('.form-group').find('.help-block').html(error);
            else if ( $(value).attr('data-error-message') === undefined )
                $(value).closest('.form-group').find('.help-block').html('This field is required.');
            else
                $(value).closest('.form-group').find('.help-block').html($(value).attr("data-error-message"));
        }
        $(value).addClass('validate-error');
        $(value).parent().parent().parent().parent().addClass('validate-error');
    };

    $.fn.validateForm =  function() {
        var submit = true;
        var $form = $(this);

        $(this).find('.x_panel').each(function(key, value){
            $(value).removeClass('validate-error');
        });
        $(this).find('input[required], select.required, textarea[required]').each(function(key, value){
            if($(value).attr('type') == 'checkbox'){
                if(!value.checked){
                    input_error($(value));
                    submit = false;
                }else{
                    $(value).closest('.form-group').find('.help-block').remove();
                    $(value).removeClass('validate-error');
                }
            }else{
                if($(value).val() == "" || $(value).val() == null){
                    input_error($(value));
                    $(value).addClass('validate-error');
                    $(value).parent().parent().parent().parent().addClass('validate-error');
                    submit = false;
                }else if($(value).attr('type') == "email"){
                    var pattern = new RegExp(/^(("[\w-+\s]+")|([\w-+]+(?:\.[\w-+]+)*)|("[\w-+\s]+")([\w-+]+(?:\.[\w-+]+)*))(@((?:[\w-+]+\.)*\w[\w-+]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][\d]\.|1[\d]{2}\.|[\d]{1,2}\.))((25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\.){2}(25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\]?$)/i);
                    if(!pattern.test($(value).val())){
                        input_error($(value), 'Please enter a valid email');
                        submit = false;
                    }else{
                        $(value).closest('.form-group').find('.help-block').remove();
                        $(value).removeClass('validate-error');
                    }

                }else{
                    $(value).closest('.form-group').find('.help-block').remove();
                    $(value).removeClass('validate-error');
                }
            }
        })
        var old_password_error = false;
        var password_error = false;
        var confirm_password_error = false;
        $(this).find('input[type="password"]').each(function(key, value){
            if($(value).attr('name') == "old_password"){
                if($(value).val() != ""){
                    if($form.find('input[name="password"]').val() == ""){
                        input_error($form.find('input[name="password"]'));
                        submit = false;
                        password_error = true;
                    }
                    if($form.find('input[name="password_confirmation"]').val() == ""){
                        input_error($form.find('input[name="password_confirmation"]'));
                        submit = false;
                        confirm_password_error = true;
                    }
                }
            }else if($(value).attr('name') == "password"){
                if($(value).val() != ''){
                    if($form.find('input[name="old_password"]').val() == ""){
                        input_error($form.find('input[name="old_password"]'));
                        submit = false;
                        old_password_error = true;
                    }
                    if($form.find('input[name="password_confirmation"]').val() == ""){
                        input_error($form.find('input[name="password_confirmation"]'));
                        confirm_password_error = true;
                        submit = false;
                    }
                }
            }else if($(value).attr('name') == "password_confirmation"){
                if($(value).val() != ''){
                    if($form.find('input[name="old_password"]').val() == ""){
                        input_error($form.find('input[name="old_password"]'));
                        old_password_error = true;
                        submit = false;
                    }
                    if($form.find('input[name="password"]').val() == ""){
                        input_error($form.find('input[name="password"]'));
                        password_error = true;
                        submit = false;
                    }else if($(value).val() != $form.find('input[name="password"]').val()){
                        input_error($(value), 'Password does not match');
                        confirm_password_error = true;
                        submit = false;
                    }
                } 
            }
        })
        if(!old_password_error){
            $form.find('#old_password').closest('.form-group').find('.help-block').remove();
            $form.find('#old_password').removeClass('validate-error');
        }
        if(!password_error){
            $form.find('#password').closest('.form-group').find('.help-block').remove();
            $form.find('#password').removeClass('validate-error');
        }
        if(!confirm_password_error){
            $form.find('#password_confirmation').closest('.form-group').find('.help-block').remove();
            $form.find('#password_confirmation').removeClass('validate-error');
        }

        return submit;
    }

}(jQuery));

function openTimeTab(clicked, id) {
    target = $(clicked).parent().find('.full-tab-content[data-id="'+id+'"]');
    if($(target).css('display') == "none"){
        $(target).slideDown(500);
        $(target).find('full-tab-arrow').find('i').removeClass('fa-angle-down');
        $(target).find('full-tab-arrow').find('i').addClass('fa-angle-up');
    }else{
        $(target).slideUp(500);
        $(target).find('full-tab-arrow').find('i').addClass('fa-angle-down');
        $(target).find('full-tab-arrow').find('i').removeClass('fa-angle-up');
    }
};