// Custom Js
window.notSelected = [];
window.timeNum = 0;
window.galleryNum = 0;

$(document).on('click', '.delete-selected-image', function(){
    target = $(this).attr('data-id');
    $('#'+target+'-display').attr('src', '');
    $('#'+target+'-display').parent().hide();
    $('#'+target).val('');
})

$(document).on('click', '.delete-object-image', function(){
    target = $(this).attr('data-id');
    route = $(this).attr('data-route');

    $(this).toggleClass('delete-object-image delete-selected-image');
    $(this).parent().toggleClass('show');

    $('#'+target+'-display').attr('src', '');
    $('#'+target).val('');

    $.ajax({
        type: 'get',
        url: route,
        data: {
            image: target,
            _token : "{{ csrf_token() }}",
        },
        dataType:'json',
        success: function(data)
        {

        },
        error:function( data){

        }
    });
})

$(document).on('click', '.btn-save', function(e){
    e.preventDefault();
    target = $(this).attr('data-form');
    url = $(this).attr('data-url');
    type = $(this).attr('data-type');
    $form = $('#'+target);
    $btn = $(this);

    for (var i in CKEDITOR.instances) {
        CKEDITOR.instances[i].updateElement();
    };

    // // check if the input is valid
    if(! $form.validateForm()) return false;

    var data = new FormData();
    //Form data
    var form_data = $form.serializeArray();
    $.each(form_data, function (key, input) {
        data.append(input.name, input.value);
    });

    if(type == "put" || type == "PUT"){
        data.append('_method', type);
        type = "post";
    }

    // Display the key/value pairs
    // for (var pair of data.entries()) {
    //     console.log(pair[0]+ ', ' + pair[1]);
    // }

    $.ajax({
        type: type,
        processData: false,
        contentType: false,
        url: url,
        data: data,
        beforeSend: function(){
            $btn.prop('disabled', true);
            data = $btn.attr('data-loading-text');
            $btn.attr('data-loading-text', $btn.html());
            $btn.html(data);
        },
        complete: function(){
            $btn.prop('disabled', false);
            data = $btn.attr('data-loading-text');
            $btn.attr('data-loading-text', $btn.html());
            $btn.html(data);
        },
        success: function(data)
        {
            // console.log(data);
            if(data.status == 'success'){
                var slug = $('#'+target).closest('div.tab-pane')[0].id.replace('_content','')
                closeTab(slug,true);
            }else if(data.status == "error"){
                $.each(data.errors, function(key, value){
                    if($form.find('#'+key).closest('.form-group').find('.help-block').length == 0){
                        $form.find('#'+key).closest('.form-group').append('<span class="help-block">'+value+'</span>');
                    }else{
                        $form.find('#'+key).closest('.form-group').find('.help-block').html(value);
                    }
                    $form.find('#'+key).addClass('validate-error');
                })
            }
        },
        error:function( data){
            console.log(data);
        }
    });
})

$(document).on('click', '.role-dropdown-toggle span', function(){
    div = $(this).parent();
    target = $(div).attr('data-target');
    if($('[data-list="'+target+'"]').css('display') == 'none'){
        $('[data-list="'+target+'"]').show(400);
        $(div).find('i').removeClass('fa-angle-down');
        $(div).find('i').addClass('fa-angle-up');
    }else{
        $('[data-list="'+target+'"]').hide(400);
        closeRoleChildren($('[data-list="'+target+'"]'));
        $(div).find('i').removeClass('fa-angle-up');
        $(div).find('i').addClass('fa-angle-down');
    }
})
function closeRoleChildren(children){
    $(children).each(function(key, value){
        if($(value).find('.role-dropdown-toggle').length != 0){
            child = $('[data-list="'+$(value).find('.role-dropdown-toggle').attr('data-target')+'"]');
            $(child).each(function(key, value2){
                if($(value2).find('.role-dropdown-toggle').length != 0){
                    childTarget = $(value2).find('.role-dropdown-toggle').attr('data-target');
                    closeRoleChildren($('[data-list="'+childTarget+'"]'));
                }
            })
            if($(child).css('display') != "none"){
                $(child).hide(400);
                $(value).find('td').find('i').removeClass('fa-angle-up');
                $(value).find('td').find('i').addClass('fa-angle-down');
            }
        }else{
            if($(value).css('display') != "none"){
                $(value).hide(400);
                $(value).find('td').find('i').removeClass('fa-angle-up');
                $(value).find('td').find('i').addClass('fa-angle-down');
            }
        }
    })
}

$(document).on('change', '.role-parent-checkbox', function(){
    check(this);
})
$(document).on('change', '.role-checkbox', function(){
    childCheck(this);
})
$(document).on('change', '.role-col-check',function(){
    target = $(this).attr('data-target');
    div = this;
    $('input[data-col-check="'+target+'"]').each(function(key, value){
        if(div.checked){
            $(value).prop('checked',true);
        }else{
            $(value).prop('checked',false);
        }
    })
})

function check(checkbox){
    if(checkbox.checked) {
        var id = $(checkbox).attr('data-parent-target');
        $('input[data-parent-check="'+id+'"]').prop('checked',true);
        $.each($('input[data-parent-check="'+id+'"]'), function(key, value){
            $(value).prop('checked',true);
            if($(value).attr('data-parent-target') != "undefined"){
                check(value);
            }
            if($(value).attr('data-target') != "undefined"){
                childCheck(value);
            }
        })
    }
    else {
        var id = $(checkbox).attr('data-parent-target');
        $('input[data-parent-check="'+id+'"]').prop('checked',false);
        $.each($('input[data-parent-check="'+id+'"]'), function(key, value){
            $(value).prop('checked',false);
            if($(value).attr('data-parent-target') != "undefined"){
                check(value);
            }
            if($(value).attr('data-target') != "undefined"){
                childCheck(value);
            }
        })
    }
}

function childCheck(checkbox){
    if(checkbox.checked) {
        var id = $(checkbox).attr('data-target');
        $('input[data-check="'+id+'"]').prop('checked',true);
        $.each($('input[data-check="'+id+'"]'), function(key, value){
            $(value).prop('checked',true);
            if($(value).attr('data-target') != "undefined"){
                childCheck(value);
            }
        })
    }
    else {
        var id = $(checkbox).attr('data-target');
        $('input[data-check="'+id+'"]').prop('checked',false);
        $.each($('input[data-check="'+id+'"]'), function(key, value){
            $(value).prop('checked',false);
            if($(value).attr('data-target') != "undefined"){
                childCheck(value);
            }
        })
    }
}
$(document).on('click', '.close-time', function(){
    $(this).parent().remove();
})

//Week Times
$(document).on('click', '.add-time-btn', function(){
    id = $(this).attr('data-id');
    target = $(this).attr('data-target');
    div = $('#'+id).find('.full-tab-content[data-id="'+target+'"]');
    $(div).find('.timing-div').append(
        '<div class="time-item">'
        +'<div class="input-time start-time"><label><span class="glyphicon glyphicon-time"></span></span> Start Time</label><input id="start-time_'+window.timeNum+'" type="text" name="start_time['+target+'][]" /></div>'
        +'<div class="input-time end-time"><label><span class="glyphicon glyphicon-time"></span></span> End Time</label><input id="end-time_'+window.timeNum+'"  type="text" name="end_time['+target+'][]" /></div>'
        +'<i class="fa fa-close close-time"></i>'
        +'</div>'
    );
    $('#start-time_'+window.timeNum).clockpicker({
        autoclose: true,
        placement: 'top',
        align: 'left',
        donetext: 'Done'
    });
    $('#end-time_'+window.timeNum).clockpicker({
        autoclose: true,
        placement: 'top',
        align: 'left',
        donetext: 'Done'
    });
    window.timeNum++;
})

//Gallery
$(document).on('change','.gallery-input',function(){
    var files = $(this).get(0).files;
    var clone = $(this).clone();
    var input = $(this).parent();
    var id = $(this).attr('data-id');
    $('#gallery-inputs_'+id).find('.file').remove();
    $.each(files, function(key, value){
            var reader = new FileReader();
            var i = files.length-(key+1);
            $('#gallery-inputs_'+id).append(
                '<div class="gallery-image">'+
                '<img  id="gallery_image_'+(window.galleryNum)+'">'+
                '<a href="javascript:void(0)" class="image-fav delete-choosen-gallery-img" data-image="'+files[i].name+'" data-gallery="'+id+'"><img src="{{asset("images/deletemodifypic.png")}}"> </a>'+
                '</div>'
            );
            reader.onload = function (e) {
                $('#gallery_image_'+(window.galleryNum-(key+1))).attr('src',  e.target.result);
            }
            reader.readAsDataURL(value);
            window.galleryNum ++;
    })
    $(clone).attr('hidden', 'hidden');
    $(clone).attr('id', 'gallery_'+window.galleryNum);
    $('#gallery-inputs_'+id).append(clone);
    $(input).val('');
    $('#gallery-inputs_'+id).append(input);
})

$(document).on('click', '.delete-choosen-gallery-img', function(){
    var image = $(this).attr('data-image');
    var target = $(this).attr('data-gallery');

    if(window.notSelected[target] != undefined){
        window.notSelected[target].push(image);
    }else{
        window.notSelected[target] = [];
        window.notSelected[target].push(image);
    }
    $(this).parent().remove();
})

$(document).on('click', '.delete-gallery-img', function(){
    var image = $(this).attr('data-image');
    var route = $(this).attr('data-route');
    $(this).parent().remove();

    $.ajax({
        type: 'get',
        url: route,
        data: {
            image: image,
            _token : "{{ csrf_token() }}",
        },
        dataType:'json',
        success: function(data)
        {

        },
        error:function( data){

        }
    });
})


function ajaxSelect(form, parent, data, id, name, route, zip = false){
    url = route+'?'+parent+'='+data;
    if(name == "City"){
        $('#'+form).find('#'+id).select2({
            width: '100%',
            allowClear: true,
            placeholder: 'Select '+name,
            ajax: {
                url: url,
                dataType: 'json',
                processResults: function (data) {
                    return {
                        results: $.map(data, function(obj) {
                            return { id: obj['_id'], text: obj['name'], zip:obj['zip_code'] };
                        })
                    };
                },
                cache: true
            }
        });

    }else{
        $('#'+form).find('#'+id).select2({
            width: '100%',
            placeholder: 'Select '+name,
            allowClear: true,
            ajax: {
                url: url,
                dataType: 'json',
                processResults: function (data) {
                    return {
                        results: $.map(data, function(key, value) {
                            return { id: key, text: value };
                        })
                    };
                },
                cache: true
            }
        });

    }
    if(zip){
        $('#'+form).find('#'+id).on('select2:select', function(event) {
            var contact = event.params.data;
            var zip = contact.zip;
            if(zip != undefined && zip != "undefined" && zip != null){
                $('#postal_code').val(zip);
            }
        });
    }
}


function init_custom_select2(id) {
    $('#'+id+' .select2').each(function(key, value){
        if($(value).hasClass('select2-ajax') && $(value).attr('data-route') !== undefined){
            let route = $(value).attr('data-route');
            let name = $(value).attr('data-name');
            if($(value).attr('data-flag') !== undefined){
                let flag = $(value).attr('data-search');
                route = route+'?search='+flag;
            }
            $(value).select2({
                width: '100%',
                placeholder: 'Select '+name,
                allowClear: true,
                ajax: {
                    url: route,
                    dataType: 'json',
                    processResults: function (data) {
                        results = [];
                        if(data.label != null){
                            results.push({id: "", text: 'Select '+data.label})
                        }else{
                            results.push({id: "", text: 'Select '+name})
                        }
                        data = $.map(data.data, function(obj) {
                            return { id: obj['_id'], text: obj['name'] };
                        })
                        $.each(data, function(key, value){
                            results.push(value)
                        })
                        return {
                            results: results
                        };
                    },
                    cache: true
                }
            });

        }else{
            $(value).select2({
                width: "100%"
            });

        }
    })

    // $('.select2').each(function (i, obj) {
    //     if ($(obj).hasClass("select2-ajax") && $(obj).attr('data-route') !== undefined ){

    //         $(obj).val(null);
    //         $(obj).trigger('change');
    //         $(obj).select2({
    //             width: '100%',
    //             ajax: {
    //                 url: $(obj).attr('data-route') ,
    //                 dataType: 'json',
    //                 processResults: function (data) {
    //                     return {
    //                         results: $.map(data, function (obj) {
    //                             return {id: obj['_id'], text: obj['name']};
    //                         })
    //                     };
    //                 },
    //                 cache: true
    //             }
    //         });


    //     }else if(!$(obj).hasClass("select2-ajax")){

    //     }
    // });

};

function init_custom_select_one(id) {
            let route = $('#'+id).attr('data-route');
            let name = $('#'+id).attr('data-name');
            if($('#'+id).attr('data-flag') !== undefined){
                let flag = $(id).attr('data-search');
                route = route+'?search='+flag;
            }
            $('#'+id).select2({
                width: '100%',
                placeholder: 'Select '+name,
                ajax: {
                    url: route,
                    dataType: 'json',
                    processResults: function (data) {
                        results = [];
                        if(data.label != null){
                            results.push({id: "", text: 'Select '+data.label})
                        }else{
                            results.push({id: "", text: 'Select '+name})
                        }
                        data = $.map(data.data, function(obj) {
                            return { id: obj['_id'], text: obj['name'] };
                        })
                        $.each(data, function(key, value){
                            results.push(value)
                        })
                        return {
                            results: results
                        };
                    },
                    cache: true
                }
            });

};


function init_custom_select_one_class(id) {

    $('.'+id).each(function(key, value) {
        if ($(value).hasClass('select2-ajax') && $(value).attr('data-route') !== undefined) {
            let route = $(value).attr('data-route');
            let name = $(value).attr('data-name');
            if($(value).attr('data-flag') !== undefined){
                let flag = $(id).attr('data-search');
                route = route+'?search='+flag;
            }
            $(value).select2({
                width: '100%',
                placeholder: 'Select '+name,
                ajax: {
                    url: route,
                    dataType: 'json',
                    processResults: function (data) {
                        results = [];
                        if(data.label != null){
                            results.push({id: "", text: 'Select '+data.label})
                        }else{
                            results.push({id: "", text: 'Select '+name})
                        }
                        data = $.map(data.data, function(obj) {
                            return { id: obj['_id'], text: obj['name'] };
                        })
                        $.each(data, function(key, value){
                            results.push(value)
                        })
                        return {
                            results: results
                        };
                    },
                    cache: true
                }
            });
        }
    });


};

function generateBreadcrumbs(nextDir){
    let path = nextDir.split('/').slice(0);
    for(let i=1;i<path.length;i++){
        path[i] = path[i-1]+ '/' +path[i];
    }
    return path;
}

$(document).on('click', 'a.folders', function(e){
    e.preventDefault();
    filemanager = $('#'+$(this).attr('data-id')+'_content').find('.filemanager')
    let nextDir = $(this).attr('href');
    window.location.hash = $(this).attr('data-id')+'='+nextDir;
    currentPath = nextDir;
});

$(document).on('click', '.breadcrumbs a', function(e){
    e.preventDefault();
    let nextDir = $(this).attr('href');

    window.location.hash = $(this).attr('data-id')+'='+nextDir;
});
$(document).on('click', '.time-switch', function(){
    target = $(this).attr('data-id');
    if(this.checked){
        $('#time-'+target).find('.day-times').css('display', 'inline-flex');
    }else{
        $('#time-'+target).find('.day-times').hide(300);
    }
})
window.timeNum = 0;
$(document).on('click', '.add-time', function(){
    target = $(this).attr('data-id');
    div = $('#time-'+target).find('.day-times').find('.times');
    $(div).append(
        '<div class="inline-time">'+
            '<div class="input-time start-time">'+
                '<input type="text" class="'+window.timeNum+'_update-time start" value="9:00" name="start_time['+target+'][]"/>'+
            '</div>'+
            '<div class="time-seperate">-</div>'+
            '<div class="input-time end-time">'+
                '<input type="text" class="'+window.timeNum+'_update-time end" value="19:00" name="end_time['+target+'][]"/>'+
            '</div>'+
            '<div class="delete-time">'+
                '<i class="fa fa-close"></i>'+
            '</div>'+
        '</div>'
    );
    $('.'+window.timeNum+'_update-time').clockpicker({
        autoclose: true,
        placement: 'top',
        align: 'left',
        donetext: 'Done'
    });
    window.timeNum++;
    if($('#time-'+target).find('.day-times').find('.times').find('.inline-time').length == 3){
        $(this).hide();
    }
    if($('#time-'+target).find('.day-times').find('.times').find('.inline-time').length > 1){
        $(div).find('.inline-time').each(function(key, value){
            $(value).find('.delete-time').show();
        })
    }
})
$(document).on('click', '.delete-time', function(){
    div = $(this).parent().parent();
    $(this).parent().remove();
    if($('#time-'+target).find('.day-times').find('.times').find('.inline-time').length < 3){
        $(div).parent().find('.add-time').show();
    }
    if($('#time-'+target).find('.day-times').find('.times').find('.inline-time').length == 1){
        $(div).find('.inline-time').each(function(key, value){
            $(value).find('.delete-time').hide();
        })
    }
})
function init_time_picker(id) {
    $('#'+id).clockpicker({
        autoclose: true,
        placement: 'top',
        align: 'left',
        donetext: 'Done'
    });

}

