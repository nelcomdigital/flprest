let email = '', genderType = '', first_name = '', last_name = '', phone_number = '', date_of_birth = '',
    distributor_number = '', city = '', street_address = '', additional_address = '', zip_code = '',
    remember_me = false, cart_access = false, cantre_access = false, country = '';

function steps(n) {
    if (n === 1) {
        $('.header-right').hide();
        document.getElementById("tab1").style.display = 'block';
        document.getElementById("tab2").style.display = 'none';
        document.getElementById("tab3").style.display = 'none';
    } else if (n === 2) {
        distributor_number = document.getElementById("phone_number").value;
        if (distributor_number === '') {
            document.getElementById("help-register_phone_number").innerText = 'Please provide a mobile number';
        } else {
            if (document.getElementById("remember_me").checked) {
                remember_me = true;
            }
            document.getElementById("help-register_phone_number").innerText = '';
            document.getElementById("tab1").style.display = 'none';
            document.getElementById("tab2").style.display = 'block';
            document.getElementById("tab3").style.display = 'none';
        }
    } else if (n === 3) {
        let error = true;
        first_name = document.getElementById("first_name").value;
        last_name = document.getElementById("last_name").value;
        if (first_name === '' || last_name === '') {
            document.getElementById("help-name").innerText = 'Please provide a name';
            error = false;
        } else {
            document.getElementById("help-name").innerText = '';
            error = true;
        }

        email = document.getElementById("email").value;
        if (email === '') {
            document.getElementById("help-email").innerText = 'Please provide an email address';
            error = false;
        } else if (!email.includes('@') || !email.includes('.')) {
            document.getElementById("help-email").innerText = "Email should contain an '@' and '.com, .net, ...'";
            error = false;
        } else {
            document.getElementById("help-email").innerText = '';
            error = true;
        }

        date_of_birth = document.getElementById("birth_date").value;
        if (date_of_birth === '') {
            document.getElementById("help-date-of-birth").innerText = "Please provide a date of birth";
            error = false;
        } else {
            document.getElementById("help-date-of-birth").innerText = "";
            error = true;
        }

        phone_number = document.getElementById("reg_phone_number").value;
        if (phone_number === '') {
            document.getElementById("help-phone-number").innerText = 'Please provide a phone number';
            error = false;
        } else {
            document.getElementById("help-phone-number").innerText = '';
            error = true;
        }

        let ele = document.getElementsByName("gender");
        for (let i = 0; i < ele.length; i++) {
            if (ele[i].checked) {
                genderType = ele[i].value;
            }
        }
        if (genderType === '') {
            document.getElementById("help-gender").innerText = 'Please Provide a gender';
            error = false;
        } else {
            document.getElementById("help-gender").innerText = '';
        }

        if (error) {
            document.getElementById("tab1").style.display = 'none';
            document.getElementById("tab2").style.display = 'none';
            document.getElementById("tab3").style.display = 'block';
        }
    } else if (n === 4) {
        let addressError = true;
        city = document.getElementById("city").value;
        if (city === '') {
            document.getElementById("help-city").innerText = "Please provide a city";
            addressError = false;
        } else {
            document.getElementById("help-city").innerText = "";
            addressError = true;
        }

        street_address = document.getElementById("street_address").value;
        if (street_address === '') {
            document.getElementById("help-street-address").innerText = "Please provide a street address";
            addressError = false;
        } else {
            document.getElementById("help-street-address").innerText = "";
            addressError = true;
        }

        additional_address = document.getElementById("additional_address").value;

        zip_code = document.getElementById("zip_code").value;
        if (zip_code === '') {
            document.getElementById("help-zip-code").innerText = "Please provide a zip code";
            addressError = false;
        } else {
            document.getElementById("help-zip-code").innerText = "";
            addressError = true;
        }

        if (addressError) {
            document.getElementById("main-content").style.display = 'block';
            document.getElementById("header-content").style.display = 'none';
            document.getElementById("cus-name").innerText = "Hello, " + first_name + " " + last_name;
            cart_access = true;
            // let url = window.location.protocol + '//' + window.location.hostname + ':8000/main';
            // window.location.href = url;
        }
    }
    if (n === 5) {
        document.getElementById("main-content").style.display = 'none';
        document.getElementById("header-content").style.display = 'flex';
        cantre_access = false;
        cart_access = false;
    }
}

document.getElementById("shopping-cart").addEventListener('click', function () {
    if (cart_access) {
        document.getElementById('main-content').style.display = 'none';
        document.getElementById('cart-container').style.display = 'block';
    }
});

document.getElementById("cantre").addEventListener('click', function () {
    if (cantre_access) {

    }
});

document.getElementById("continue_shopping").addEventListener('click', function () {
    document.getElementById('main-content').style.display = 'block';
    document.getElementById('cart-container').style.display = 'none';
    cantre_access = true;
    cart_access = true;
});

let shippingName = document.getElementById("shipping-name");
let shippingCountry = document.getElementById("shipping-country");
let shippingCity = document.getElementById("shipping-city");
let shippingStreetAddress = document.getElementById("shipping-street-Address");
let shippingAdditionalAddress = document.getElementById("shipping-additional-address");
let shippingZipCode = document.getElementById("shipping-zip-code");
let shippingPhoneNumber = document.getElementById("shipping-phone-number");



document.getElementById('back-to-cart').addEventListener('click', function () {
    document.getElementById('cart-container').style.display = 'block';
    document.getElementById('shopping-address').style.display = 'none';
    cantre_access = false;
    cart_access = false;
});

document.getElementById('payment-back-to-cart').addEventListener('click', function () {
    document.getElementById('cart-container').style.display = 'block';
    document.getElementById('shopping-payment').style.display = 'none';
    document.getElementById("help-terms-and-policy").innerText = '';
    cantre_access = false;
    cart_access = false;
});

document.getElementById('btn-proceed-payment').addEventListener('click', function () {
    if ( shippingName.value && shippingCountry.value && shippingCity.value && shippingStreetAddress.value
         && shippingZipCode.value && shippingPhoneNumber.value){
        [first_name, last_name] = shippingName.value.split(" ");
        country = shippingCountry.value;
        city = shippingCity.value;
        street_address = shippingStreetAddress.value;
        additional_address = shippingAdditionalAddress.value;
        zip_code = shippingZipCode.value;
        phone_number = shippingPhoneNumber.value;
        document.getElementById('shopping-address').style.display = 'none';
        document.getElementById('shopping-payment').style.display = 'block';
        document.getElementById('help-address').innerText = '';

        shippingName.disabled = true;
        shippingCountry.disabled = true;
        shippingCity.disabled = true;
        shippingStreetAddress.disabled = true;
        shippingAdditionalAddress.disabled = true;
        shippingZipCode.disabled = true;
        shippingPhoneNumber.disabled = true;
        errorDisabled = true;
    } else {
        document.getElementById('help-address').innerText = 'Please fill all fields';
    }
    // }
});

document.getElementById('buy-within-72').addEventListener('click', function () {
    document.getElementById('thank-you2').style.display = 'block';
    document.getElementById('shopping-payment').style.display = 'none';
    document.getElementById("thank-you72-distributor").value = distributor_number;
    document.getElementById("thank-you72-name").value = first_name + " " + last_name;
});

document.getElementById("btn-card-submit").addEventListener('click', function (e) {
    e.preventDefault();
    let submitError = true;
    let policy = document.getElementById("policy").checked;
    let procedure = document.getElementById("procedure").checked;
    if (policy && procedure) {
        document.getElementById('thank-you').style.display = 'block';
        document.getElementById('shopping-payment').style.display = 'none';
        document.getElementById("help-terms-and-policy").innerText = '';
        document.getElementById("thank-you-distributor").value = distributor_number;
        document.getElementById("thank-you-name").value = first_name + " " + last_name;
    } else {
        document.getElementById("help-terms-and-policy").innerText = 'Please agree to the terms and policies first';
    }
});
let addClasses = document.getElementsByClassName('add');
let subClasses = document.getElementsByClassName('sub');
let i;
for (i = 0; i < addClasses.length; i++) {
    addClasses[i].addEventListener('click', function (e) {
        let target = $(this).attr('id');
        document.getElementById("quantity-" + target).value++;
        let AddUnitPrice = Number(document.getElementById("unit-price-" + target).innerText);
        let AddQuantity = Number(document.getElementById("quantity-" + target).value);
        let AddUnitTotalPriceCC = Number(document.getElementById("unit-price-total-" + target).innerText);
        let AddPrice_multiple = Number(AddUnitPrice) * Number(AddQuantity);
        document.getElementById("unit-price-total-" + target).innerText = '' + Number(AddPrice_multiple);

        let total = Number(document.getElementById("unit-price-grand-total").innerText);
        total += AddUnitPrice;
        document.getElementById("unit-price-grand-total").innerText = '' + total;
    }, false);
}
for (i = 0; i < subClasses.length; i++) {
    subClasses[i].addEventListener('click', function (e) {
        let target = $(this).attr('id');
        if (document.getElementById("quantity-" + target).value > 0) {
            document.getElementById("quantity-" + target).value--;
            let SubUnitPrice = Number(document.getElementById("unit-price-" + target).innerText);
            let SubQuantity = Number(document.getElementById("quantity-" + target).value);
            let SubUnitTotalPriceCC = Number(document.getElementById("unit-price-total-" + target).innerText);
            let SubPrice_multiple = Number(SubUnitPrice) * Number(SubQuantity);
            document.getElementById("unit-price-total-" + target).innerText = '' + Number(SubPrice_multiple);

            let total = Number(document.getElementById("unit-price-grand-total").innerText);
            total -= SubUnitPrice;
            document.getElementById("unit-price-grand-total").innerText = '' + total;

            e.stopPropagation();
            e.preventDefault();
        }
    }, false);
}

document.getElementById('delete-address').addEventListener('click', function () {
    shippingName.value = '';
    shippingCountry.value = '';
    shippingCity.value = '';
    shippingStreetAddress.value = '';
    shippingAdditionalAddress.value = '';
    shippingZipCode.value = '';
    shippingPhoneNumber.value = '';
});

let errorDisabled = true;
document.getElementById("edit-address").addEventListener('click', function () {
    errorDisabled = !errorDisabled;
    shippingName.disabled = errorDisabled;
    shippingCountry.disabled = errorDisabled;
    shippingCity.disabled = errorDisabled;
    shippingStreetAddress.disabled = errorDisabled;
    shippingAdditionalAddress.disabled = errorDisabled;
    shippingZipCode.disabled = errorDisabled;
    shippingPhoneNumber.disabled = errorDisabled;
});
