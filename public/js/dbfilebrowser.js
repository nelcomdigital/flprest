function init_gallery_album(id, path, model, hide = false){
    jQ(function($){

        let filemanager = $('#'+id+'_content').find('.filemanager'),
        breadcrumbs = filemanager.find('.breadcrumbs'),
        fileList = filemanager.find('.data');

        // Start by fetching the file data from the route with an AJAX request
        url = '/'+model+'?scan=true&path='+path;
        $.get(url, function(data) {

            let response = [data],
                currentPath = '',
                breadcrumbsUrls = [];

            let folders = [],
                files = [];

            let FilesBtn = $('#'+id+'_buttons').find('.qq-upload-button').clone(),
                FolderBtn = $('#'+id+'_buttons').find('.btn-create').clone();

                
            $(FolderBtn).css('display', 'flex')
            let addFolderBtn = $('<li class="add-file-btn"></li>')
            $(addFolderBtn).append(FolderBtn);
            
            $(FilesBtn).css('display', 'flex')
            let addFilesBtn = $('<li class="add-file-btn"></li>')
            $(addFilesBtn).append(FilesBtn);

            // Monitor changes on the URL.
            $(window).on('hashchange', function(){
                    if(window.location.hash.includes('=') && window.location.hash.slice(1).split('=')[0] == id){
                        // console.log(window.location.hash.slice(1).split('='));
                        goto(window.location.hash.slice(1).split('=')[1]);
                    }
            }).trigger('hashchange');

            goto(data.path);
            location.hash = data.path;

            // Search
            // filemanager.find('.search').click(function(){

            //     let search = $(this);

            //     search.find('span').hide();
            //     search.find('input[type=search]').show().focus();

            // });

            // filemanager.find('input').on('input', function(e){

            //     folders = [];
            //     files = [];

            //     let value = this.value.trim();

            //     if(value.length) {

            //         filemanager.addClass('searching');

            //         // Update the hash on every key stroke
            //         window.location.hash = 'search=' + value.trim();

            //     }

            //     else {

            //         filemanager.removeClass('searching');
            //         window.location.hash = (currentPath);

            //     }

            // }).on('keyup', function(e){

            //     let search = $(this);

            //     if(e.keyCode == 27) {

            //         search.trigger('focusout');

            //     }

            // }).focusout(function(e){

            //     // Cancel search
            //     let search = $(this);

            //     if(!search.val().trim().length) {

            //         window.location.hash = (currentPath);
            //         search.hide();
            //         search.parent().find('span').show();

            //     }

            // });

            // fileList.on('click', 'a.folders', function(e){
            // // function openFileList(nextDir){
            //     e.preventDefault();

            //     // let nextDir = $(this).find('a.folders').attr('href');
            //     let nextDir = $(this).attr('href');

            //     if(filemanager.hasClass('searching')) {

            //         // Build the breadcrumbs
            //         breadcrumbsUrls = generateBreadcrumbs(nextDir);

            //         filemanager.removeClass('searching');
            //         filemanager.find('input[type=search]').val('').hide();
            //         filemanager.find('span').show();
            //     }
            //     else {
            //         breadcrumbsUrls.push(nextDir);
            //     }

            //     window.location.hash = nextDir;
            //     goto(nextDir);
            //     currentPath = nextDir;
            // // }
            // });

            // breadcrumbs.on('click', 'a', function(e){
            // // function openBread(index){
            //     e.preventDefault();

            //     let index = breadcrumbs.find('a').index($(this));
            //     let nextDir;
            //     if(hide){
            //         nextDir = breadcrumbsUrls[index+2];
            //         breadcrumbsUrls.length = Number(index)+2;
            //     }else{
            //         nextDir = breadcrumbsUrls[index];
            //         breadcrumbsUrls.length = Number(index);
            //     }

            //     window.location.hash = nextDir;
            //     goto(nextDir);
            //     console.log(nextDir)
            // // }
            // });

            // Navigates to the given hash (path)
            function goto(hash) {

                // hash = decodeURIComponent(hash).slice(1).split('=');
                hash = decodeURIComponent(hash).split('=');

                if (hash.length) {
                    let rendered = '';

                    // if hash has search in it
                    if (hash[0] === 'search') {

                        filemanager.addClass('searching');
                        rendered = searchData(response, hash[1].toLowerCase());

                        if (rendered.length) {
                            currentPath = hash[0];
                            render(rendered);
                        }
                        else {
                            render(rendered);
                        }

                    }

                    // if hash is some path
                    else if (hash[0].trim().length) {

                        rendered = searchByPath(hash[0]);

                        if (rendered.length) {

                            currentPath = hash[0];
                            breadcrumbsUrls = generateBreadcrumbs(hash[0]);
                            render(rendered, hash);

                        }
                        else {
                            currentPath = hash[0];
                            breadcrumbsUrls = generateBreadcrumbs(hash[0]);
                            render(rendered, hash);
                        }

                    }

                    // if there is no hash
                    else {
                        currentPath = data.path;
                        breadcrumbsUrls.push(data.path);
                        render(searchByPath(data.path));
                    }
                }
            }

            // Splits a file path and turns it into clickable breadcrumbs
            function generateBreadcrumbs(nextDir){
                let path = nextDir.split('/').slice(0);
                for(let i=1;i<path.length;i++){
                    path[i] = path[i-1]+ '/' +path[i];
                }
                return path;
            }

            // Locates a file by path
            function searchByPath(dir) {
                let path = dir.split('/'),
                    demo = response,
                    flag = 0;

                for(let i=0;i<path.length;i++){
                    for(let j=0;j<demo.length;j++){
                        if(demo[j].name === path[i]){
                            flag = 1;
                            demo = demo[j].items;
                            break;
                        }
                    }
                }

                demo = flag ? demo : [];
                return demo;
            }

            // Recursively search through the file tree
            function searchData(data, searchTerms) {

                data.forEach(function(d){
                    if(d.type === 'folder') {

                        searchData(d.items,searchTerms);

                        if(d.name.toLowerCase().match(searchTerms)) {
                            folders.push(d);
                        }
                    }
                    else if(d.type === 'file') {
                        if(d.name.toLowerCase().match(searchTerms)) {
                            files.push(d);
                        }
                    }
                });
                return {folders: folders, files: files};
            }

            // Render the HTML for the file manager
            function render(data, hash = []) {

                let scannedFolders = [],
                    scannedFiles = [];

                if(Array.isArray(data)) {
                    data.forEach(function (d) {

                        if (d.type === 'folder') {
                            scannedFolders.push(d);
                        }
                        else if (d.type === 'file') {
                            scannedFiles.push(d);
                        }

                    });

                }
                else if(typeof data === 'object') {
                    scannedFolders = data.folders;
                    scannedFiles = data.files;
                }

                // Empty the old result and make the new one
                fileList.empty().hide();

                // if(!scannedFolders.length && !scannedFiles.length) {
                //     filemanager.find('.nothingfound').show();
                // }
                // else {
                //     filemanager.find('.nothingfound').hide();
                // }

                if(hash[0].split('/').length == 3 && hash[0].includes('Guide')){
                    div = $('#'+id+'_fine-uploader-manual-trigger');
                    $(div).hide(200);
                    $(div).parent().removeClass('col-md-3')
                    $(div).parent().addClass('col-md-0')
                    m = $(div).parent().parent().find('.col-md-9');
                    $(m).removeClass('col-md-9')
                    $(m).addClass('col-md-12')
                    if(scannedFolders.length) {
                        scannedFolders.forEach(function(f) {

                            let itemsLength = f.items.length,
                                name = escapeHTML(f.name),
                                icon = '<span class="icon folder"></span>';

                            if(itemsLength) {
                                // icon = '<span class="icon folder full"></span>';
                                icon = '<img src="http://nelcomlab.club/images/storage/'+f.items[1].path+'?w=100&h=100&fit=crop">'
                            }

                            if(itemsLength == 1) {
                                itemsLength += ' item';
                            }
                            else if(itemsLength > 1) {
                                itemsLength += ' items';
                            }
                            else {
                                itemsLength = 'Empty';
                            }

                            let folder = $('<li class="folders"><a href="'+ f.path+'" title="'+ f.path +'" class="folders" data-id="'+id+'">'+icon+'<span class="details">' + name + ' (' + itemsLength + ')</span></a>'
                                // +' <span class="">' + itemsLength + '</span></a>'
                                +'<div class="details-album-box">'
                                +(f.object!=null && f.object.user != null  ?'</span> <span class="details">'+f.object.user.name+'</span>':'')
                                +'<a href="javascript:void(0)" class="edit-gallery-album" data-route="'+f.edit+'"> <img src="http://nelcomlab.club/public/images/edit.svg" class="img-responsive" style="display: inline-block;max-width: 16px;"></a>'
                                +'<a href="javascript:void(0)" onclick="deleteImage(\''+f.name+'\',\''+f.path+'\')" ><img src="http://nelcomlab.club/public/images/remove.svg" class="img-responsive" style="display: inline-block;max-width: 16px;"></a>'
                                +'</div>'
                                +'</li>'
                            );
                            folder.appendTo(fileList);
                        });
                    }
                    $(fileList).append($(addFolderBtn))
                }else if(!hash[0].includes('Guide')){
                    if(scannedFolders.length) {
                        scannedFolders.forEach(function(f) {

                            let itemsLength = f.items.length,
                                name = escapeHTML(f.name),
                                icon = '<span class="icon folder"></span>';

                            if(itemsLength) {
                                // icon = '<span class="icon folder full"></span>';
                                icon = '<img src="http://nelcomlab.club/images/storage/'+f.items[1].path+'?w=100&h=100&fit=crop">'
                            }

                            if(itemsLength == 1) {
                                itemsLength += ' item';
                            }
                            else if(itemsLength > 1) {
                                itemsLength += ' items';
                            }
                            else {
                                itemsLength = 'Empty';
                            }

                            let folder = $('<li class="folders"><a href="'+ f.path+'" title="'+ f.path +'" class="folders" data-id="'+id+'">'+icon+'<span class="details">' + name + ' (' + itemsLength + ')</span></a>'
                                // +' <span class="">' + itemsLength + '</span></a>'
                                +'<div class="details-album-box">'
                                +(f.object!=null && f.object.user != null  ?'</span> <span class="details">'+f.object.user.name+'</span>':'')
                                +'<a href="javascript:void(0)" class="edit-gallery-album" data-route="'+f.edit+'"> <img src="http://nelcomlab.club/public/images/edit.svg" class="img-responsive" style="display: inline-block;max-width: 16px;"></a>'
                                +'<a href="javascript:void(0)" onclick="deleteImage(\''+f.name+'\',\''+f.path+'\')" ><img src="http://nelcomlab.club/public/images/remove.svg" class="img-responsive" style="display: inline-block;max-width: 16px;"></a>'
                                +'</div>'
                                +'</li>'
                            );
                            folder.appendTo(fileList);
                        });
                    }
                }
                if(hash[0].split('/').length > 3 && hash[0].includes('Guide')){
                    div = $('#'+id+'_fine-uploader-manual-trigger');
                    $(div).show(200);
                    $(div).parent().removeClass('col-md-0')
                    $(div).parent().addClass('col-md-3')
                    m = $(div).parent().parent().find('.col-md-12');
                    $(m).removeClass('col-md-12')
                    $(m).addClass('col-md-9')
                    if(scannedFiles.length) {
                        scannedFiles.forEach(function(f) {
    
                            let fileSize = bytesToSize(f.size),
                                name = escapeHTML(f.name),
                                fileType = name.split('.'),
                                icon = '<span class="icon file"></span>';
    
                            fileType = fileType[fileType.length-1];
    
                            if ( fileType !=='png' && fileType !=='jpg' && fileType !=='jpeg' && fileType !=='svg')
                            icon = '<span class="icon file f-'+fileType+'">.'+fileType+'</span>';
                            else{
                                icon = '<span class="icon"> <img src="http://nelcomlab.club/images/storage/'+f.path+'?w=100&h=100&fit=crop" style="padding-right: 10px"/></span>';
                            }
    
                            let file = $(
                                '<li class="files">' +
                                '<a href="http://nelcomlab.club/public/storage/'+f.path+'" title="'+ f.path +'" class="files" target="_blank">'+icon
                                // +'<span class="name">'+ name +'</span> '
                                +'<span class="details">'+name+' ('+fileSize+')</span></a>'
                                +'<div class="details-album-box">'
                                +(f.object!=null && f.object.user != null  ?'</span> <span class="details">'+f.object.user.name+'</span>':'')
                                +'<a href="javascript:void(0)" onclick="deleteImage(\''+f.name+'\',\''+f.path+'\')" ><img src="http://nelcomlab.club/public/images/remove.svg" class="img-responsive" style="display: inline-block;max-width: 16px;"></a>'
                                +'</div>'
                                +'</li>'
                            );
                            file.appendTo(fileList);
                        });
                    }
                    $(fileList).append($(addFilesBtn))
                }else if(!hash[0].includes('Guide')){
                    if(scannedFiles.length) {
                        scannedFiles.forEach(function(f) {
    
                            let fileSize = bytesToSize(f.size),
                                name = escapeHTML(f.name),
                                fileType = name.split('.'),
                                icon = '<span class="icon file"></span>';
    
                            fileType = fileType[fileType.length-1];
    
                            if ( fileType !=='png' && fileType !=='jpg' && fileType !=='jpeg' && fileType !=='svg')
                            icon = '<span class="icon file f-'+fileType+'">.'+fileType+'</span>';
                            else{
                                icon = '<span class="icon"> <img src="http://nelcomlab.club/images/storage/'+f.path+'?w=100&h=100&fit=crop" style="padding-right: 10px"/></span>';
                            }
    
                            let file = $(
                                '<li class="files">' +
                                '<a href="http://nelcomlab.club/public/storage/'+f.path+'" title="'+ f.path +'" class="files" target="_blank">'+icon
                                // +'<span class="name">'+ name +'</span> '
                                +'<span class="details">'+name+' ('+fileSize+')</span></a>'
                                +'<div class="details-album-box">'
                                +(f.object!=null && f.object.user != null  ?'</span> <span class="details">'+f.object.user.name+'</span>':'')
                                +'<a href="javascript:void(0)" onclick="deleteImage(\''+f.name+'\',\''+f.path+'\')" ><img src="http://nelcomlab.club/public/images/remove.svg" class="img-responsive" style="display: inline-block;max-width: 16px;"></a>'
                                +'</div>'
                                +'</li>'
                            );
                            file.appendTo(fileList);
                        });
                    }
                }

                // Generate the breadcrumbs
                let url = '';

                if(filemanager.hasClass('searching')){

                    url = '<span>Search results: </span>';
                    fileList.removeClass('animated');

                }
                else {

                    fileList.addClass('animated');
                    breadcrumbsUrls.forEach(function (u, i) {

                        let name = u.split('/');
                        // console.log(name);
                        if (i !== breadcrumbsUrls.length - 1) {
                            if ( hide ){
                                if(i>1){
                                    url += '<a href="'+u+'" data-id="'+id+'" data-index="'+(i+2)+'"><span class="folderName">' + name[name.length-1] + '</span></a> <span class="arrow">/</span>';
                                }
                            }else{
                                url += '<a href="'+u+'" data-id="'+id+'" data-index="'+i+'"><span class="folderName">' + name[name.length-1] + '</span></a> <span class="arrow">/</span>';
                            }
                            // url += '<span class="folderName">' + name[name.length-1] + '</span> <span class="arrow">/</span> ';

                        }
                        else {
                            url += '<span class="folderName">' + name[name.length-1] + '</span>';
                        }

                    });

                }

                breadcrumbs.text('').append(url);

                // Show the generated elements
                fileList.animate({'display':'inline-block'});
            }

            // Escape special html characters in names
            function escapeHTML(text) {
                return text.replace(/\&/g,'&amp;').replace(/\</g,'&lt;').replace(/\>/g,'&gt;');
            }

            // Convert file sizes from bytes to human readable units
            function bytesToSize(bytes) {
                let sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
                if (bytes == 0) return '0 Bytes';
                let i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
                return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
            }

        });
    });
}
